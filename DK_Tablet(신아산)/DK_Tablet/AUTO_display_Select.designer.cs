﻿namespace DK_Tablet
{
    partial class AUTO_display_Select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_In = new DevExpress.XtraEditors.SimpleButton();
            this.btn_DISPLAY = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_rework = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btn_In
            // 
            this.btn_In.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_In.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            this.btn_In.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.btn_In.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_In.Appearance.Options.UseBackColor = true;
            this.btn_In.Appearance.Options.UseFont = true;
            this.btn_In.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_In.Location = new System.Drawing.Point(1, 1);
            this.btn_In.Name = "btn_In";
            this.btn_In.Size = new System.Drawing.Size(255, 213);
            this.btn_In.TabIndex = 0;
            this.btn_In.Text = "입구";
            this.btn_In.Click += new System.EventHandler(this.btn_In_Click);
            // 
            // btn_DISPLAY
            // 
            this.btn_DISPLAY.Appearance.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_DISPLAY.Appearance.BackColor2 = System.Drawing.Color.White;
            this.btn_DISPLAY.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.btn_DISPLAY.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_DISPLAY.Appearance.Options.UseBackColor = true;
            this.btn_DISPLAY.Appearance.Options.UseFont = true;
            this.btn_DISPLAY.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_DISPLAY.Location = new System.Drawing.Point(260, 1);
            this.btn_DISPLAY.Name = "btn_DISPLAY";
            this.btn_DISPLAY.Size = new System.Drawing.Size(255, 213);
            this.btn_DISPLAY.TabIndex = 1;
            this.btn_DISPLAY.Text = "현황";
            this.btn_DISPLAY.Click += new System.EventHandler(this.btn_DISPLAY_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Orange;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.simpleButton1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton1.Location = new System.Drawing.Point(519, 1);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(255, 213);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "출구";
            this.simpleButton1.Click += new System.EventHandler(this.btn_Out_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            this.simpleButton2.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.simpleButton2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton2.Location = new System.Drawing.Point(1, 220);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(255, 213);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "상세\r\n현황";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btn_rework
            // 
            this.btn_rework.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.btn_rework.Appearance.BackColor2 = System.Drawing.Color.White;
            this.btn_rework.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.btn_rework.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_rework.Appearance.Options.UseBackColor = true;
            this.btn_rework.Appearance.Options.UseFont = true;
            this.btn_rework.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_rework.Location = new System.Drawing.Point(260, 220);
            this.btn_rework.Name = "btn_rework";
            this.btn_rework.Size = new System.Drawing.Size(255, 213);
            this.btn_rework.TabIndex = 1;
            this.btn_rework.Text = "재작업";
            this.btn_rework.Click += new System.EventHandler(this.btn_rework_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.simpleButton3.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton3.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.simpleButton3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton3.Location = new System.Drawing.Point(778, 1);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(255, 213);
            this.simpleButton3.TabIndex = 1;
            this.simpleButton3.Text = "노즐\r\n현황";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.simpleButton4.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 50F);
            this.simpleButton4.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton4.Location = new System.Drawing.Point(519, 220);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(255, 213);
            this.simpleButton4.TabIndex = 1;
            this.simpleButton4.Text = "커넥션\r\n현황";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // AUTO_display_Select
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1037, 436);
            this.Controls.Add(this.btn_rework);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.btn_DISPLAY);
            this.Controls.Add(this.btn_In);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_display_Select";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paint_In_Out_Select";
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton btn_In;
        public DevExpress.XtraEditors.SimpleButton btn_DISPLAY;
        public DevExpress.XtraEditors.SimpleButton simpleButton1;
        public DevExpress.XtraEditors.SimpleButton simpleButton2;
        public DevExpress.XtraEditors.SimpleButton btn_rework;
        public DevExpress.XtraEditors.SimpleButton simpleButton3;
        public DevExpress.XtraEditors.SimpleButton simpleButton4;
    }
}