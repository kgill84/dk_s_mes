using System;
using System.IO;
using System.IO.Ports;
using System.Collections;
using System.Windows.Forms;
using System.Xml; // for Application.StartupPath

namespace DK_Tablet
{
    /// <summary>
    /// Persistent settings
    /// </summary>
    class Settings_Xml
    {
        /// <summary> Port settings. </summary>
        public static bool Write(string it_scode, string print_type)
        {
            string sFilePath = Application.StartupPath;

            try
            {

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.NewLineOnAttributes = true;
                XmlWriter xmlWriter = XmlWriter.Create(sFilePath + @"/Settings_print_type.xml",settings); // 저장할 xml 파일명
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("root");
                xmlWriter.WriteElementString("ITEM"+it_scode.ToString(), print_type.ToString()); // 앞이 node, 뒤는 value 
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Flush(); // xml 파일을 쓴다.
                xmlWriter.Close(); // 반드시 닫아줍시다.
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public static bool reWrite(string it_scode, string print_type)
        {
            bool bFlag;
            string sFilePath = Application.StartupPath;

            try
            {
                if (File.Exists(sFilePath + @"/Settings_print_type.xml"))   //  xml 파일 존재 유무 검사
                {
                    //XmlTextReader xmlReadData = new XmlTextReader(sFilePath + @"/Settings_print_type.xml");    //  xml 파일 열기

                    //while (xmlReadData.Read())
                    //{
                        
                    //        MessageBox.Show(xmlReadData.Name.ToUpper().Trim());
                    //        MessageBox.Show(xmlReadData.ReadString().ToString().Trim()); 
                            
                        
                    //}
                    //xmlReadData.Close();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(sFilePath + @"/Settings_print_type.xml");
                    XmlElement root = xmldoc.DocumentElement;

                    // 노드 요소들
                    XmlNodeList nodes = root.ChildNodes;
                    bool check = false;
                    // 노드 요소의 값을 읽어 옵니다.
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Indent = true;
                    settings.NewLineOnAttributes = true;
                    XmlWriter xmlWriter = XmlWriter.Create(sFilePath + @"/Settings_print_type.xml"); // 저장할 xml 파일명
                    xmlWriter.WriteStartDocument();

                    xmlWriter.WriteStartElement("root");
                    foreach (XmlNode node in nodes)
                    {

                        if (node.Name.ToString().Equals("ITEM" + it_scode))
                        {
                            xmlWriter.WriteElementString("ITEM" + it_scode, print_type); // 앞이 node, 뒤는 value 
                            check = true;
                        }
                        else
                        {
                            xmlWriter.WriteElementString(node.Name.ToString().Trim(), node.InnerText.ToString().Trim()); // 앞이 node, 뒤는 value 

                        }

                    }
                    
                    if (!check)
                    {
                        xmlWriter.WriteElementString("ITEM" + it_scode, print_type); // 앞이 node, 뒤는 value                         
                    }
                        xmlWriter.WriteEndDocument();

                        xmlWriter.Flush(); // xml 파일을 쓴다.
                        xmlWriter.Close(); // 반드시 닫아줍시다.
                   
                }
                else // xml 파일이 존재 하지 않을 때 
                {
                    // 디폴트값으로 xml 파일을 기록해야하므로 저장 함수로 보낸다.
                    Write(it_scode, print_type);
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public static string read(string it_scode, string print_type)
        {
            bool bFlag;
            string sFilePath = Application.StartupPath;
            string print_type_str = "";
            try
            {
                if (File.Exists(sFilePath + @"/Settings_print_type.xml"))   //  xml 파일 존재 유무 검사
                {
                    
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(sFilePath + @"/Settings_print_type.xml");
                    XmlElement root = xmldoc.DocumentElement;

                    // 노드 요소들
                    XmlNodeList nodes = root.ChildNodes;
                    

                    foreach (XmlNode node in nodes)
                    {

                        if (node.Name.ToString().Equals("ITEM" + it_scode))
                        {
                            print_type_str = node.InnerText.ToString().Trim();
                        }
                    }                   

                }
                else // xml 파일이 존재 하지 않을 때 
                {
                    // 디폴트값으로 xml 파일을 기록해야하므로 저장 함수로 보낸다.
                    Write(it_scode, print_type);
                }

                return print_type_str;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return print_type_str;
            }
        }
	}
}
