﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Shipment_search_popup : Form
    {
        public string sh_snumb { get; set; }
        public string loc_code { get; set; }
        public Shipment_search_popup()
        {
            InitializeComponent();
        }

        private void Shipment_search_popup_Load(object sender, EventArgs e)
        {
            getdata();
        }
        private void getdata()
        {
            splashScreenManager1.ShowWaitForm();
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_SHIPMENT_SEARCH_CS", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "SHIPMENT_SEARCH");

                DataTable dt = ds.Tables["SHIPMENT_SEARCH"];

                gridControl1.DataSource = dt;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            sh_snumb = gridView1.GetRowCellDisplayText(gridView1.FocusedRowHandle, gridView1.Columns["SH_SNUMB"].FieldName).ToString();
            loc_code = gridView1.GetRowCellDisplayText(gridView1.FocusedRowHandle, gridView1.Columns["LOC_CODE"].FieldName).ToString();
            DialogResult = DialogResult.OK;
        }
    }
}
