﻿using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class Lot_Trace_LY : Form
    {
       
        string totalConn = "";
        string site_code = "";
        public bool popup_gubn = false;
        
        string lot_snumb = "";
        public Lot_Trace_LY()
        {
            
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string strConn = "";
            strConn = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strConn);

            SqlDataAdapter da = new SqlDataAdapter("USP_LASER_Y_SEARCH", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE_Y", "");
            da.SelectCommand.Parameters.AddWithValue("@LOT_NO_Y", "");
            

            DataSet ds = new DataSet();
            da.Fill(ds, "Y_BARCODE_CHK");
            DATA_ALL = ds.Tables["Y_BARCODE_CHK"];
            gridControl1.DataSource = DATA_ALL;
            
        }

       
            /*
            loginUserDetail.getUserData(site_code, Properties.Settings.Default.userID, "");

            btnGrade(this, int.Parse(loginUserDetail.user_level));
            */
        
        DataTable DATA_ALL = new DataTable();
        private void Lot_Trace_Load(object sender, EventArgs e)
        {
            btnSearch.PerformClick();
        }
        
       


        private void btnClose_Click(object sender, EventArgs e)
        {
            
                this.Close();
           
        }


        
    }
}
