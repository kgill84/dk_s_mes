﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DK_Tablet.Popup;

namespace DK_Tablet
{
    public partial class Fail_Input_Commit : Form
    {
        public string lost_code { get; set; }
        public string wc_group { get; set; }
        public string gubn { get; set; }
        public string sqty { get; set; }
        public Fail_Input_Commit()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            
        }
        public void set_btn()
        {
            splashScreenManager1.ShowWaitForm();
            string strCon;
            //string datetime = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT DISTINCT B.NUMB,B.FT_NAME FROM FAIL_TYPE_ROUTING A";
            sql = sql +" LEFT JOIN FAIL_TYPE B ON A.NUMB = B.NUMB";
        

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();
            int cnt = 1;
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Button btn = new Button();
                    btn.Tag = reader["NUMB"].ToString();
                    btn.Name = btn + reader["NUMB"].ToString();
                    string name = reader["FT_NAME"].ToString().Trim();
                    if (name.Length > 3)
                    {
                        btn.Text = name.Substring(0, 2) + "\r\n" + name.Substring(2, name.Length - 2);
                    }
                    else
                    {
                        btn.Text = reader["FT_NAME"].ToString().Trim();
                    }
                    btn.Dock = DockStyle.Fill;
                    btn.TextAlign = ContentAlignment.MiddleCenter;
                    btn.Margin = new System.Windows.Forms.Padding(2);
                    //btn.UseVisualStyleBackColor = true;
                    btn.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
                    btn.Click += new System.EventHandler(this.btn_fail_input);
                    int set_rowcount=0;
                    if (cnt == 7 || cnt == 13 || cnt == 19 || cnt == 25)
                    {
                        if (cnt % 6 != 0)
                        {
                            set_rowcount = (cnt / 6) + 1;
                            tableLayoutPanel2.RowCount = set_rowcount;
                            this.tableLayoutPanel2.Size = new System.Drawing.Size(679, 86 * set_rowcount);
                            for (int i = 0; i < set_rowcount; i++)
                            {
                                tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 84F));
                                this.ClientSize = new System.Drawing.Size(700, 77 + (82 * set_rowcount));
                            }
                        }
                    }
                    tableLayoutPanel2.Controls.Add(btn);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void Fail_Input_Load(object sender, EventArgs e)
        {
            set_btn();
            
        }
        private void btn_fail_input(object sender, EventArgs e)
        {

            Button btn = (Button)sender;
            lost_code = btn.Tag.ToString();

            

                Fail_Sqty_popup Fail_Sqty_popup = new Fail_Sqty_popup();
                Fail_Sqty_popup.fail_name = btn.Text.Replace("\r", "").Replace("\n", "");
                Fail_Sqty_popup.sqty = "1";

                if (Fail_Sqty_popup.ShowDialog() == DialogResult.OK)
                {
                    sqty = Fail_Sqty_popup.sqty;
                    DialogResult = DialogResult.OK;
                }
            
        }
    }
}
