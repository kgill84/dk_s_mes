﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DK_Tablet.Popup
{
    public partial class TEST_ELIPSE : DevExpress.XtraEditors.XtraForm
    {
        public TEST_ELIPSE()
        {
            InitializeComponent();
           
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //DrawEllipse();
            //this.roundButton1.cr = Color.Red;
            
            this.roundButton1.ForeColor = Color.Red;
        }
        private void DrawEllipse()
        {
            System.Drawing.Pen myPen;
            myPen = new System.Drawing.Pen(System.Drawing.Color.Red);
            System.Drawing.Graphics formGraphics = this.CreateGraphics();
            formGraphics.DrawEllipse(myPen, new Rectangle(0, 0, 50, 50));
            myPen.Dispose();
            formGraphics.Dispose();
            
        }
    }
}