﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class po_release_search : Form
    {
        public string IT_SCODE_str { get; set; }
        public string IT_SNAME_str { get; set; }
        public string IT_MODEL_str { get; set; }
        public string MO_SQUTY_str { get; set; }
        public string MO_SNUMB_str { get; set; }
        public string WC_CODE_str { get; set; }
        public string SITE_CODE_str { get; set; }
        public string ALC_CODE_str { get; set; }
        public string ME_SCODE_str { get; set; }
        public string IT_PKQTY_str { get; set; }
        public string MATCH_GUBN_str { get; set; }
        public string D_WEIGHT_str { get; set; }
        public string A_WEIGHT_str { get; set; }
        public string CARRIER_YN_str { get; set; }
        public string MAX_SQTY_str { get; set; }
        public string wc_code { get; set; }
        public string wc_group { get; set; }
        public po_release_search()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void po_realese_search_Load(object sender, EventArgs e)
        {
            getdata();
        }
        private void getdata()
        {
            splashScreenManager1.ShowWaitForm();
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_WORK_PLAN_SEARCH", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            try 
            { 
                DataSet ds = new DataSet();
                da.Fill(ds, "WORK_PLAN");

                DataTable dt = ds.Tables["WORK_PLAN"];

                dataGridView1.DataSource = dt;
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ColorChange();
                splashScreenManager1.CloseWaitForm();
            }
        }
        private void ColorChange()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //MessageBox.Show(row.Cells["합계"].Value.ToString());
                if (2 == int.Parse(row.Cells["GUBN"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;

                }
            }
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            int cxtl = dataGridView1.CurrentCell.RowIndex;
            if(cxtl<0)
            {
                MessageBox.Show("작업지시를 선택해 주세요");
                return;
            }
            IT_SCODE_str = dataGridView1.Rows[cxtl].Cells["IT_SCODE"].Value.ToString().Trim();
            IT_SNAME_str = dataGridView1.Rows[cxtl].Cells["IT_SNAME"].Value.ToString().Trim();
            IT_MODEL_str = dataGridView1.Rows[cxtl].Cells["IT_MODEL"].Value.ToString().Trim();
            MO_SQUTY_str = dataGridView1.Rows[cxtl].Cells["MO_SQUTY"].Value.ToString().Trim();
            MO_SNUMB_str = dataGridView1.Rows[cxtl].Cells["MO_SNUMB"].Value.ToString().Trim();
            WC_CODE_str = dataGridView1.Rows[cxtl].Cells["WC_SCODE"].Value.ToString().Trim();
            SITE_CODE_str = dataGridView1.Rows[cxtl].Cells["SITE_CODE"].Value.ToString().Trim();
            ALC_CODE_str = dataGridView1.Rows[cxtl].Cells["ALC_CODE"].Value.ToString().Trim();
            ME_SCODE_str = dataGridView1.Rows[cxtl].Cells["ME_SCODE"].Value.ToString().Trim();
            IT_PKQTY_str = dataGridView1.Rows[cxtl].Cells["IT_PKQTY"].Value.ToString().Trim();
            D_WEIGHT_str = dataGridView1.Rows[cxtl].Cells["D_WEIGHT"].Value.ToString().Trim();
            A_WEIGHT_str = dataGridView1.Rows[cxtl].Cells["A_WEIGHT"].Value.ToString().Trim();
            MATCH_GUBN_str = dataGridView1.Rows[cxtl].Cells["MATCH_GUBN"].Value.ToString().Trim();
            CARRIER_YN_str = dataGridView1.Rows[cxtl].Cells["CARRIER_YN"].Value.ToString().Trim();
            MAX_SQTY_str = dataGridView1.Rows[cxtl].Cells["MAX_SQTY"].Value.ToString().Trim();
            DialogResult = DialogResult.OK;
        }
    }
}
