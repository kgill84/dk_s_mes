﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Mix_PP_Card_Input : Form
    {
        public DataTable Dt = new DataTable();
        public Mix_PP_Card_Input()
        {
            InitializeComponent();
        }
        // += new System.EventHandler(this.btn_pp_card_search_Click);
        private void Mix_PP_Card_Input_Load(object sender, EventArgs e)
        {
            dataGridView2.DataSource = Dt;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "SIZE":
                        Sqty_popup Sqty_popup = new Sqty_popup();
                        Sqty_popup.sqty = senderGrid.Rows[e.RowIndex].Cells["SIZE"].Value.ToString();
                        if (Sqty_popup.ShowDialog() == DialogResult.OK)
                        {
                            senderGrid.Rows[e.RowIndex].Cells["SIZE"].Value = Sqty_popup.sqty;
                        }
                    break;
                    case "BTN_DELETE":
                        Dt.Rows[e.RowIndex].Delete();
                    break;
                }
            }
            
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
