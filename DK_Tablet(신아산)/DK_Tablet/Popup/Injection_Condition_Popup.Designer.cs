﻿namespace DK_Tablet.Popup
{
    partial class Injection_Condition_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_h_3_down = new System.Windows.Forms.Button();
            this.btn_h_2_down = new System.Windows.Forms.Button();
            this.btn_h_1_down = new System.Windows.Forms.Button();
            this.btn_s_3_down = new System.Windows.Forms.Button();
            this.btn_s_2_down = new System.Windows.Forms.Button();
            this.btn_s_1_down = new System.Windows.Forms.Button();
            this.btn_w_4_down = new System.Windows.Forms.Button();
            this.btn_w_3_down = new System.Windows.Forms.Button();
            this.btn_w_2_down = new System.Windows.Forms.Button();
            this.btn_w_1_down = new System.Windows.Forms.Button();
            this.btn_p_4_down = new System.Windows.Forms.Button();
            this.btn_p_3_down = new System.Windows.Forms.Button();
            this.btn_p_2_down = new System.Windows.Forms.Button();
            this.btn_t_8_down = new System.Windows.Forms.Button();
            this.btn_t_7_down = new System.Windows.Forms.Button();
            this.btn_t_6_down = new System.Windows.Forms.Button();
            this.btn_t_5_down = new System.Windows.Forms.Button();
            this.btn_t_4_down = new System.Windows.Forms.Button();
            this.btn_t_3_down = new System.Windows.Forms.Button();
            this.btn_t_2_down = new System.Windows.Forms.Button();
            this.btn_t_1_down = new System.Windows.Forms.Button();
            this.btn_p_1_down = new System.Windows.Forms.Button();
            this.btn_h_3_up = new System.Windows.Forms.Button();
            this.btn_h_2_up = new System.Windows.Forms.Button();
            this.btn_h_1_up = new System.Windows.Forms.Button();
            this.btn_s_3_up = new System.Windows.Forms.Button();
            this.btn_s_2_up = new System.Windows.Forms.Button();
            this.btn_s_1_up = new System.Windows.Forms.Button();
            this.btn_w_4_up = new System.Windows.Forms.Button();
            this.btn_w_3_up = new System.Windows.Forms.Button();
            this.btn_w_2_up = new System.Windows.Forms.Button();
            this.btn_w_1_up = new System.Windows.Forms.Button();
            this.btn_p_4_up = new System.Windows.Forms.Button();
            this.btn_p_3_up = new System.Windows.Forms.Button();
            this.btn_p_2_up = new System.Windows.Forms.Button();
            this.btn_t_8_up = new System.Windows.Forms.Button();
            this.btn_t_7_up = new System.Windows.Forms.Button();
            this.btn_t_6_up = new System.Windows.Forms.Button();
            this.btn_t_5_up = new System.Windows.Forms.Button();
            this.btn_t_4_up = new System.Windows.Forms.Button();
            this.btn_t_3_up = new System.Windows.Forms.Button();
            this.btn_t_2_up = new System.Windows.Forms.Button();
            this.btn_t_1_up = new System.Windows.Forms.Button();
            this.btn_p_1_up = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl47 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl48 = new DevExpress.XtraEditors.LabelControl();
            this.txtVVGATE_DELAY2 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY3 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY4 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY5 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY6 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY7 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY8 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY9 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY10 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN1 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN2 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN3 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN4 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN5 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN6 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN7 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN8 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN9 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN10 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.txtVVGATE_DELAY11 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY12 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY13 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY14 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_DELAY15 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN11 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN12 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN13 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN14 = new DevExpress.XtraEditors.TextEdit();
            this.txtVVGATE_OPEN15 = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.txtCHARGE_PRE = new DevExpress.XtraEditors.TextEdit();
            this.txtCHARGE_SPEED = new DevExpress.XtraEditors.TextEdit();
            this.txtCHARGE_DISTANCE = new DevExpress.XtraEditors.TextEdit();
            this.txtCHARGE_PREVENT = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtPRESSURE_INJ1 = new DevExpress.XtraEditors.TextEdit();
            this.txtPRESSURE_INJ2 = new DevExpress.XtraEditors.TextEdit();
            this.txtPRE_MOLD_SPEED = new DevExpress.XtraEditors.TextEdit();
            this.txtMOLD_PRESSURE = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtTIME_INJ = new DevExpress.XtraEditors.TextEdit();
            this.txtTIME_COOLING = new DevExpress.XtraEditors.TextEdit();
            this.txtTIME_TOTAL = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtSPEED_INJ1 = new DevExpress.XtraEditors.TextEdit();
            this.txtSPEED_INJ2 = new DevExpress.XtraEditors.TextEdit();
            this.txtSPEED_INJ3 = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.txtTEMP_CYLINDER3 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEMP_CYLINDER4 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEMP_CYLINDER5 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEMP_CYLINDER6 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl45 = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtTEMP_NOZZLE1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtTEMP_CYLINDER2 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEMP_CYLINDER1 = new DevExpress.XtraEditors.TextEdit();
            this.txtTEMP_NOZZLE2 = new DevExpress.XtraEditors.TextEdit();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN15.Properties)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_PRE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_SPEED.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_DISTANCE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_PREVENT.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPRESSURE_INJ1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPRESSURE_INJ2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPRE_MOLD_SPEED.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMOLD_PRESSURE.Properties)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_INJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_COOLING.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_TOTAL.Properties)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ3.Properties)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER6.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_NOZZLE1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_NOZZLE2.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.labelControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btn_save);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_3_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_2_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_1_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_3_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_2_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_1_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_4_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_3_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_2_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_1_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_4_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_3_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_2_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_8_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_7_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_6_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_5_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_4_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_3_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_2_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_1_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_1_down);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_3_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_2_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_h_1_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_3_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_2_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_s_1_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_4_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_3_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_2_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_w_1_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_4_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_3_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_2_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_8_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_7_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_6_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_5_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_4_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_3_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_2_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_t_1_up);
            this.splitContainer1.Panel2.Controls.Add(this.btn_p_1_up);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel6);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel5);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel4);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel7);
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Size = new System.Drawing.Size(1123, 588);
            this.splitContainer1.SplitterDistance = 58;
            this.splitContainer1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("맑은 고딕", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(1123, 58);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "작  업  조  건";
            // 
            // btn_save
            // 
            this.btn_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_save.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_save.Location = new System.Drawing.Point(947, 460);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(164, 59);
            this.btn_save.TabIndex = 16;
            this.btn_save.Text = "등   록";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_h_3_down
            // 
            this.btn_h_3_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_3_down.Location = new System.Drawing.Point(1082, 236);
            this.btn_h_3_down.Name = "btn_h_3_down";
            this.btn_h_3_down.Size = new System.Drawing.Size(30, 35);
            this.btn_h_3_down.TabIndex = 2;
            this.btn_h_3_down.Text = "▼";
            this.btn_h_3_down.UseVisualStyleBackColor = true;
            this.btn_h_3_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_3_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_h_2_down
            // 
            this.btn_h_2_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_2_down.Location = new System.Drawing.Point(1082, 198);
            this.btn_h_2_down.Name = "btn_h_2_down";
            this.btn_h_2_down.Size = new System.Drawing.Size(30, 35);
            this.btn_h_2_down.TabIndex = 2;
            this.btn_h_2_down.Text = "▼";
            this.btn_h_2_down.UseVisualStyleBackColor = true;
            this.btn_h_2_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_2_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_h_1_down
            // 
            this.btn_h_1_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_1_down.Location = new System.Drawing.Point(1082, 160);
            this.btn_h_1_down.Name = "btn_h_1_down";
            this.btn_h_1_down.Size = new System.Drawing.Size(30, 35);
            this.btn_h_1_down.TabIndex = 2;
            this.btn_h_1_down.Text = "▼";
            this.btn_h_1_down.UseVisualStyleBackColor = true;
            this.btn_h_1_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_1_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_3_down
            // 
            this.btn_s_3_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_3_down.Location = new System.Drawing.Point(699, 235);
            this.btn_s_3_down.Name = "btn_s_3_down";
            this.btn_s_3_down.Size = new System.Drawing.Size(30, 35);
            this.btn_s_3_down.TabIndex = 2;
            this.btn_s_3_down.Text = "▼";
            this.btn_s_3_down.UseVisualStyleBackColor = true;
            this.btn_s_3_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_3_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_2_down
            // 
            this.btn_s_2_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_2_down.Location = new System.Drawing.Point(699, 198);
            this.btn_s_2_down.Name = "btn_s_2_down";
            this.btn_s_2_down.Size = new System.Drawing.Size(30, 35);
            this.btn_s_2_down.TabIndex = 2;
            this.btn_s_2_down.Text = "▼";
            this.btn_s_2_down.UseVisualStyleBackColor = true;
            this.btn_s_2_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_2_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_1_down
            // 
            this.btn_s_1_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_1_down.Location = new System.Drawing.Point(699, 160);
            this.btn_s_1_down.Name = "btn_s_1_down";
            this.btn_s_1_down.Size = new System.Drawing.Size(30, 35);
            this.btn_s_1_down.TabIndex = 2;
            this.btn_s_1_down.Text = "▼";
            this.btn_s_1_down.UseVisualStyleBackColor = true;
            this.btn_s_1_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_1_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_4_down
            // 
            this.btn_w_4_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_4_down.Location = new System.Drawing.Point(1082, 116);
            this.btn_w_4_down.Name = "btn_w_4_down";
            this.btn_w_4_down.Size = new System.Drawing.Size(30, 35);
            this.btn_w_4_down.TabIndex = 2;
            this.btn_w_4_down.Text = "▼";
            this.btn_w_4_down.UseVisualStyleBackColor = true;
            this.btn_w_4_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_4_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_3_down
            // 
            this.btn_w_3_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_3_down.Location = new System.Drawing.Point(1082, 80);
            this.btn_w_3_down.Name = "btn_w_3_down";
            this.btn_w_3_down.Size = new System.Drawing.Size(30, 35);
            this.btn_w_3_down.TabIndex = 2;
            this.btn_w_3_down.Text = "▼";
            this.btn_w_3_down.UseVisualStyleBackColor = true;
            this.btn_w_3_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_3_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_2_down
            // 
            this.btn_w_2_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_2_down.Location = new System.Drawing.Point(1082, 43);
            this.btn_w_2_down.Name = "btn_w_2_down";
            this.btn_w_2_down.Size = new System.Drawing.Size(30, 35);
            this.btn_w_2_down.TabIndex = 2;
            this.btn_w_2_down.Text = "▼";
            this.btn_w_2_down.UseVisualStyleBackColor = true;
            this.btn_w_2_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_2_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_1_down
            // 
            this.btn_w_1_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_1_down.Location = new System.Drawing.Point(1082, 6);
            this.btn_w_1_down.Name = "btn_w_1_down";
            this.btn_w_1_down.Size = new System.Drawing.Size(30, 35);
            this.btn_w_1_down.TabIndex = 2;
            this.btn_w_1_down.Text = "▼";
            this.btn_w_1_down.UseVisualStyleBackColor = true;
            this.btn_w_1_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_1_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_4_down
            // 
            this.btn_p_4_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_4_down.Location = new System.Drawing.Point(320, 271);
            this.btn_p_4_down.Name = "btn_p_4_down";
            this.btn_p_4_down.Size = new System.Drawing.Size(30, 35);
            this.btn_p_4_down.TabIndex = 2;
            this.btn_p_4_down.Text = "▼";
            this.btn_p_4_down.UseVisualStyleBackColor = true;
            this.btn_p_4_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_4_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_3_down
            // 
            this.btn_p_3_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_3_down.Location = new System.Drawing.Point(320, 234);
            this.btn_p_3_down.Name = "btn_p_3_down";
            this.btn_p_3_down.Size = new System.Drawing.Size(30, 35);
            this.btn_p_3_down.TabIndex = 2;
            this.btn_p_3_down.Text = "▼";
            this.btn_p_3_down.UseVisualStyleBackColor = true;
            this.btn_p_3_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_3_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_2_down
            // 
            this.btn_p_2_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_2_down.Location = new System.Drawing.Point(320, 198);
            this.btn_p_2_down.Name = "btn_p_2_down";
            this.btn_p_2_down.Size = new System.Drawing.Size(30, 35);
            this.btn_p_2_down.TabIndex = 2;
            this.btn_p_2_down.Text = "▼";
            this.btn_p_2_down.UseVisualStyleBackColor = true;
            this.btn_p_2_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_2_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_8_down
            // 
            this.btn_t_8_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_8_down.Location = new System.Drawing.Point(320, 43);
            this.btn_t_8_down.Name = "btn_t_8_down";
            this.btn_t_8_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_8_down.TabIndex = 2;
            this.btn_t_8_down.Text = "▼";
            this.btn_t_8_down.UseVisualStyleBackColor = true;
            this.btn_t_8_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_8_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_7_down
            // 
            this.btn_t_7_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_7_down.Location = new System.Drawing.Point(699, 118);
            this.btn_t_7_down.Name = "btn_t_7_down";
            this.btn_t_7_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_7_down.TabIndex = 2;
            this.btn_t_7_down.Text = "▼";
            this.btn_t_7_down.UseVisualStyleBackColor = true;
            this.btn_t_7_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_7_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_6_down
            // 
            this.btn_t_6_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_6_down.Location = new System.Drawing.Point(699, 80);
            this.btn_t_6_down.Name = "btn_t_6_down";
            this.btn_t_6_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_6_down.TabIndex = 2;
            this.btn_t_6_down.Text = "▼";
            this.btn_t_6_down.UseVisualStyleBackColor = true;
            this.btn_t_6_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_6_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_5_down
            // 
            this.btn_t_5_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_5_down.Location = new System.Drawing.Point(699, 43);
            this.btn_t_5_down.Name = "btn_t_5_down";
            this.btn_t_5_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_5_down.TabIndex = 2;
            this.btn_t_5_down.Text = "▼";
            this.btn_t_5_down.UseVisualStyleBackColor = true;
            this.btn_t_5_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_5_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_4_down
            // 
            this.btn_t_4_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_4_down.Location = new System.Drawing.Point(699, 6);
            this.btn_t_4_down.Name = "btn_t_4_down";
            this.btn_t_4_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_4_down.TabIndex = 2;
            this.btn_t_4_down.Text = "▼";
            this.btn_t_4_down.UseVisualStyleBackColor = true;
            this.btn_t_4_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_4_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_3_down
            // 
            this.btn_t_3_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_3_down.Location = new System.Drawing.Point(320, 117);
            this.btn_t_3_down.Name = "btn_t_3_down";
            this.btn_t_3_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_3_down.TabIndex = 2;
            this.btn_t_3_down.Text = "▼";
            this.btn_t_3_down.UseVisualStyleBackColor = true;
            this.btn_t_3_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_3_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_2_down
            // 
            this.btn_t_2_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_2_down.Location = new System.Drawing.Point(320, 79);
            this.btn_t_2_down.Name = "btn_t_2_down";
            this.btn_t_2_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_2_down.TabIndex = 2;
            this.btn_t_2_down.Text = "▼";
            this.btn_t_2_down.UseVisualStyleBackColor = true;
            this.btn_t_2_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_2_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_1_down
            // 
            this.btn_t_1_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_1_down.Location = new System.Drawing.Point(320, 6);
            this.btn_t_1_down.Name = "btn_t_1_down";
            this.btn_t_1_down.Size = new System.Drawing.Size(30, 35);
            this.btn_t_1_down.TabIndex = 2;
            this.btn_t_1_down.Text = "▼";
            this.btn_t_1_down.UseVisualStyleBackColor = true;
            this.btn_t_1_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_1_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_1_down
            // 
            this.btn_p_1_down.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_1_down.Location = new System.Drawing.Point(320, 160);
            this.btn_p_1_down.Name = "btn_p_1_down";
            this.btn_p_1_down.Size = new System.Drawing.Size(30, 35);
            this.btn_p_1_down.TabIndex = 2;
            this.btn_p_1_down.Text = "▼";
            this.btn_p_1_down.UseVisualStyleBackColor = true;
            this.btn_p_1_down.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_1_down.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_h_3_up
            // 
            this.btn_h_3_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_3_up.Location = new System.Drawing.Point(1050, 236);
            this.btn_h_3_up.Name = "btn_h_3_up";
            this.btn_h_3_up.Size = new System.Drawing.Size(30, 35);
            this.btn_h_3_up.TabIndex = 2;
            this.btn_h_3_up.Text = "▲";
            this.btn_h_3_up.UseVisualStyleBackColor = true;
            this.btn_h_3_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_3_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_h_2_up
            // 
            this.btn_h_2_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_2_up.Location = new System.Drawing.Point(1050, 198);
            this.btn_h_2_up.Name = "btn_h_2_up";
            this.btn_h_2_up.Size = new System.Drawing.Size(30, 35);
            this.btn_h_2_up.TabIndex = 2;
            this.btn_h_2_up.Text = "▲";
            this.btn_h_2_up.UseVisualStyleBackColor = true;
            this.btn_h_2_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_2_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_h_1_up
            // 
            this.btn_h_1_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_h_1_up.Location = new System.Drawing.Point(1050, 160);
            this.btn_h_1_up.Name = "btn_h_1_up";
            this.btn_h_1_up.Size = new System.Drawing.Size(30, 35);
            this.btn_h_1_up.TabIndex = 2;
            this.btn_h_1_up.Text = "▲";
            this.btn_h_1_up.UseVisualStyleBackColor = true;
            this.btn_h_1_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_h_1_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_3_up
            // 
            this.btn_s_3_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_3_up.Location = new System.Drawing.Point(667, 235);
            this.btn_s_3_up.Name = "btn_s_3_up";
            this.btn_s_3_up.Size = new System.Drawing.Size(30, 35);
            this.btn_s_3_up.TabIndex = 2;
            this.btn_s_3_up.Text = "▲";
            this.btn_s_3_up.UseVisualStyleBackColor = true;
            this.btn_s_3_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_3_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_2_up
            // 
            this.btn_s_2_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_2_up.Location = new System.Drawing.Point(667, 198);
            this.btn_s_2_up.Name = "btn_s_2_up";
            this.btn_s_2_up.Size = new System.Drawing.Size(30, 35);
            this.btn_s_2_up.TabIndex = 2;
            this.btn_s_2_up.Text = "▲";
            this.btn_s_2_up.UseVisualStyleBackColor = true;
            this.btn_s_2_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_2_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_s_1_up
            // 
            this.btn_s_1_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_s_1_up.Location = new System.Drawing.Point(667, 160);
            this.btn_s_1_up.Name = "btn_s_1_up";
            this.btn_s_1_up.Size = new System.Drawing.Size(30, 35);
            this.btn_s_1_up.TabIndex = 2;
            this.btn_s_1_up.Text = "▲";
            this.btn_s_1_up.UseVisualStyleBackColor = true;
            this.btn_s_1_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_s_1_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_4_up
            // 
            this.btn_w_4_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_4_up.Location = new System.Drawing.Point(1050, 116);
            this.btn_w_4_up.Name = "btn_w_4_up";
            this.btn_w_4_up.Size = new System.Drawing.Size(30, 35);
            this.btn_w_4_up.TabIndex = 2;
            this.btn_w_4_up.Text = "▲";
            this.btn_w_4_up.UseVisualStyleBackColor = true;
            this.btn_w_4_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_4_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_3_up
            // 
            this.btn_w_3_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_3_up.Location = new System.Drawing.Point(1050, 80);
            this.btn_w_3_up.Name = "btn_w_3_up";
            this.btn_w_3_up.Size = new System.Drawing.Size(30, 35);
            this.btn_w_3_up.TabIndex = 2;
            this.btn_w_3_up.Text = "▲";
            this.btn_w_3_up.UseVisualStyleBackColor = true;
            this.btn_w_3_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_3_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_2_up
            // 
            this.btn_w_2_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_2_up.Location = new System.Drawing.Point(1050, 43);
            this.btn_w_2_up.Name = "btn_w_2_up";
            this.btn_w_2_up.Size = new System.Drawing.Size(30, 35);
            this.btn_w_2_up.TabIndex = 2;
            this.btn_w_2_up.Text = "▲";
            this.btn_w_2_up.UseVisualStyleBackColor = true;
            this.btn_w_2_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_2_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_w_1_up
            // 
            this.btn_w_1_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_w_1_up.Location = new System.Drawing.Point(1050, 6);
            this.btn_w_1_up.Name = "btn_w_1_up";
            this.btn_w_1_up.Size = new System.Drawing.Size(30, 35);
            this.btn_w_1_up.TabIndex = 2;
            this.btn_w_1_up.Text = "▲";
            this.btn_w_1_up.UseVisualStyleBackColor = true;
            this.btn_w_1_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_w_1_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_4_up
            // 
            this.btn_p_4_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_4_up.Location = new System.Drawing.Point(288, 271);
            this.btn_p_4_up.Name = "btn_p_4_up";
            this.btn_p_4_up.Size = new System.Drawing.Size(30, 35);
            this.btn_p_4_up.TabIndex = 2;
            this.btn_p_4_up.Text = "▲";
            this.btn_p_4_up.UseVisualStyleBackColor = true;
            this.btn_p_4_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_4_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_3_up
            // 
            this.btn_p_3_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_3_up.Location = new System.Drawing.Point(288, 234);
            this.btn_p_3_up.Name = "btn_p_3_up";
            this.btn_p_3_up.Size = new System.Drawing.Size(30, 35);
            this.btn_p_3_up.TabIndex = 2;
            this.btn_p_3_up.Text = "▲";
            this.btn_p_3_up.UseVisualStyleBackColor = true;
            this.btn_p_3_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_3_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_2_up
            // 
            this.btn_p_2_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_2_up.Location = new System.Drawing.Point(288, 198);
            this.btn_p_2_up.Name = "btn_p_2_up";
            this.btn_p_2_up.Size = new System.Drawing.Size(30, 35);
            this.btn_p_2_up.TabIndex = 2;
            this.btn_p_2_up.Text = "▲";
            this.btn_p_2_up.UseVisualStyleBackColor = true;
            this.btn_p_2_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_2_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_8_up
            // 
            this.btn_t_8_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_8_up.Location = new System.Drawing.Point(288, 43);
            this.btn_t_8_up.Name = "btn_t_8_up";
            this.btn_t_8_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_8_up.TabIndex = 2;
            this.btn_t_8_up.Text = "▲";
            this.btn_t_8_up.UseVisualStyleBackColor = true;
            this.btn_t_8_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_8_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_7_up
            // 
            this.btn_t_7_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_7_up.Location = new System.Drawing.Point(667, 118);
            this.btn_t_7_up.Name = "btn_t_7_up";
            this.btn_t_7_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_7_up.TabIndex = 2;
            this.btn_t_7_up.Text = "▲";
            this.btn_t_7_up.UseVisualStyleBackColor = true;
            this.btn_t_7_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_7_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_6_up
            // 
            this.btn_t_6_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_6_up.Location = new System.Drawing.Point(667, 80);
            this.btn_t_6_up.Name = "btn_t_6_up";
            this.btn_t_6_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_6_up.TabIndex = 2;
            this.btn_t_6_up.Text = "▲";
            this.btn_t_6_up.UseVisualStyleBackColor = true;
            this.btn_t_6_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_6_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_5_up
            // 
            this.btn_t_5_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_5_up.Location = new System.Drawing.Point(667, 43);
            this.btn_t_5_up.Name = "btn_t_5_up";
            this.btn_t_5_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_5_up.TabIndex = 2;
            this.btn_t_5_up.Text = "▲";
            this.btn_t_5_up.UseVisualStyleBackColor = true;
            this.btn_t_5_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_5_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_4_up
            // 
            this.btn_t_4_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_4_up.Location = new System.Drawing.Point(667, 6);
            this.btn_t_4_up.Name = "btn_t_4_up";
            this.btn_t_4_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_4_up.TabIndex = 2;
            this.btn_t_4_up.Text = "▲";
            this.btn_t_4_up.UseVisualStyleBackColor = true;
            this.btn_t_4_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_4_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_3_up
            // 
            this.btn_t_3_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_3_up.Location = new System.Drawing.Point(288, 117);
            this.btn_t_3_up.Name = "btn_t_3_up";
            this.btn_t_3_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_3_up.TabIndex = 2;
            this.btn_t_3_up.Text = "▲";
            this.btn_t_3_up.UseVisualStyleBackColor = true;
            this.btn_t_3_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_3_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_2_up
            // 
            this.btn_t_2_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_2_up.Location = new System.Drawing.Point(288, 79);
            this.btn_t_2_up.Name = "btn_t_2_up";
            this.btn_t_2_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_2_up.TabIndex = 2;
            this.btn_t_2_up.Tag = "";
            this.btn_t_2_up.Text = "▲";
            this.btn_t_2_up.UseVisualStyleBackColor = true;
            this.btn_t_2_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_2_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_t_1_up
            // 
            this.btn_t_1_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_t_1_up.Location = new System.Drawing.Point(288, 6);
            this.btn_t_1_up.Name = "btn_t_1_up";
            this.btn_t_1_up.Size = new System.Drawing.Size(30, 35);
            this.btn_t_1_up.TabIndex = 2;
            this.btn_t_1_up.Tag = "";
            this.btn_t_1_up.Text = "▲";
            this.btn_t_1_up.UseVisualStyleBackColor = true;
            this.btn_t_1_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_t_1_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // btn_p_1_up
            // 
            this.btn_p_1_up.Font = new System.Drawing.Font("굴림", 10F);
            this.btn_p_1_up.Location = new System.Drawing.Point(288, 160);
            this.btn_p_1_up.Name = "btn_p_1_up";
            this.btn_p_1_up.Size = new System.Drawing.Size(30, 35);
            this.btn_p_1_up.TabIndex = 2;
            this.btn_p_1_up.Text = "▲";
            this.btn_p_1_up.UseVisualStyleBackColor = true;
            this.btn_p_1_up.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseDown);
            this.btn_p_1_up.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_t_1_up_MouseUp);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 16;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.250196F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.24957F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.24957F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.24957F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.24957F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.24957F));
            this.tableLayoutPanel6.Controls.Add(this.labelControl25, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl26, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl27, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl28, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl29, 4, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl30, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl31, 6, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl32, 7, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl33, 8, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl34, 9, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl35, 10, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl47, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.labelControl48, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY2, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY3, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY4, 4, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY5, 5, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY6, 6, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY7, 7, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY8, 8, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY9, 9, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY10, 10, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN1, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN2, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN3, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN4, 4, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN5, 5, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN6, 6, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN7, 7, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN8, 8, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN9, 9, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN10, 10, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY1, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.labelControl36, 11, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl37, 12, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl38, 13, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl39, 14, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl40, 15, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY11, 11, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY12, 12, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY13, 13, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY14, 14, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_DELAY15, 15, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN11, 11, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN12, 12, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN13, 13, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN14, 14, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtVVGATE_OPEN15, 15, 2);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(15, 314);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1096, 140);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // labelControl25
            // 
            this.labelControl25.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl25.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl25.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl25.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl25.Location = new System.Drawing.Point(1, 1);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(67, 45);
            this.labelControl25.TabIndex = 0;
            this.labelControl25.Text = "V/V\r\nGATE";
            // 
            // labelControl26
            // 
            this.labelControl26.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl26.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl26.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl26.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl26.Location = new System.Drawing.Point(69, 1);
            this.labelControl26.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(67, 45);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "1";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl27.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl27.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl27.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl27.Location = new System.Drawing.Point(137, 1);
            this.labelControl27.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(67, 45);
            this.labelControl27.TabIndex = 0;
            this.labelControl27.Text = "2";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl28.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl28.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl28.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl28.Location = new System.Drawing.Point(205, 1);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(67, 45);
            this.labelControl28.TabIndex = 0;
            this.labelControl28.Text = "3";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl29.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl29.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl29.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl29.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl29.Location = new System.Drawing.Point(273, 1);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(67, 45);
            this.labelControl29.TabIndex = 0;
            this.labelControl29.Text = "4";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl30.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl30.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl30.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl30.Location = new System.Drawing.Point(341, 1);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(67, 45);
            this.labelControl30.TabIndex = 0;
            this.labelControl30.Text = "5";
            // 
            // labelControl31
            // 
            this.labelControl31.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl31.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl31.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl31.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl31.Location = new System.Drawing.Point(409, 1);
            this.labelControl31.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(67, 45);
            this.labelControl31.TabIndex = 0;
            this.labelControl31.Text = "6";
            // 
            // labelControl32
            // 
            this.labelControl32.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl32.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl32.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl32.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl32.Location = new System.Drawing.Point(477, 1);
            this.labelControl32.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(67, 45);
            this.labelControl32.TabIndex = 0;
            this.labelControl32.Text = "7";
            // 
            // labelControl33
            // 
            this.labelControl33.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl33.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl33.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl33.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl33.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl33.Location = new System.Drawing.Point(545, 1);
            this.labelControl33.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(67, 45);
            this.labelControl33.TabIndex = 0;
            this.labelControl33.Text = "8";
            // 
            // labelControl34
            // 
            this.labelControl34.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl34.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl34.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl34.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl34.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl34.Location = new System.Drawing.Point(613, 1);
            this.labelControl34.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(67, 45);
            this.labelControl34.TabIndex = 0;
            this.labelControl34.Text = "9";
            // 
            // labelControl35
            // 
            this.labelControl35.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl35.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl35.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl35.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl35.Location = new System.Drawing.Point(681, 1);
            this.labelControl35.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(67, 45);
            this.labelControl35.TabIndex = 0;
            this.labelControl35.Text = "10";
            // 
            // labelControl47
            // 
            this.labelControl47.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl47.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl47.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl47.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl47.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl47.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl47.Location = new System.Drawing.Point(1, 93);
            this.labelControl47.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl47.Name = "labelControl47";
            this.labelControl47.Size = new System.Drawing.Size(67, 46);
            this.labelControl47.TabIndex = 0;
            this.labelControl47.Text = "OPEN";
            // 
            // labelControl48
            // 
            this.labelControl48.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl48.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl48.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl48.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl48.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl48.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl48.Location = new System.Drawing.Point(1, 47);
            this.labelControl48.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl48.Name = "labelControl48";
            this.labelControl48.Size = new System.Drawing.Size(67, 45);
            this.labelControl48.TabIndex = 0;
            this.labelControl48.Text = "DELAY";
            // 
            // txtVVGATE_DELAY2
            // 
            this.txtVVGATE_DELAY2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY2.Location = new System.Drawing.Point(140, 50);
            this.txtVVGATE_DELAY2.Name = "txtVVGATE_DELAY2";
            this.txtVVGATE_DELAY2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY2.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY2.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY2.TabIndex = 1;
            this.txtVVGATE_DELAY2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY3
            // 
            this.txtVVGATE_DELAY3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY3.Location = new System.Drawing.Point(208, 50);
            this.txtVVGATE_DELAY3.Name = "txtVVGATE_DELAY3";
            this.txtVVGATE_DELAY3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY3.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY3.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY3.TabIndex = 1;
            this.txtVVGATE_DELAY3.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY4
            // 
            this.txtVVGATE_DELAY4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY4.Location = new System.Drawing.Point(276, 50);
            this.txtVVGATE_DELAY4.Name = "txtVVGATE_DELAY4";
            this.txtVVGATE_DELAY4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY4.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY4.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY4.TabIndex = 1;
            this.txtVVGATE_DELAY4.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY5
            // 
            this.txtVVGATE_DELAY5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY5.Location = new System.Drawing.Point(344, 50);
            this.txtVVGATE_DELAY5.Name = "txtVVGATE_DELAY5";
            this.txtVVGATE_DELAY5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY5.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY5.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY5.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY5.TabIndex = 1;
            this.txtVVGATE_DELAY5.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY6
            // 
            this.txtVVGATE_DELAY6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY6.Location = new System.Drawing.Point(412, 50);
            this.txtVVGATE_DELAY6.Name = "txtVVGATE_DELAY6";
            this.txtVVGATE_DELAY6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY6.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY6.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY6.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY6.TabIndex = 1;
            this.txtVVGATE_DELAY6.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY7
            // 
            this.txtVVGATE_DELAY7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY7.Location = new System.Drawing.Point(480, 50);
            this.txtVVGATE_DELAY7.Name = "txtVVGATE_DELAY7";
            this.txtVVGATE_DELAY7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY7.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY7.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY7.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY7.TabIndex = 1;
            this.txtVVGATE_DELAY7.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY8
            // 
            this.txtVVGATE_DELAY8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY8.Location = new System.Drawing.Point(548, 50);
            this.txtVVGATE_DELAY8.Name = "txtVVGATE_DELAY8";
            this.txtVVGATE_DELAY8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY8.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY8.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY8.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY8.TabIndex = 1;
            this.txtVVGATE_DELAY8.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY9
            // 
            this.txtVVGATE_DELAY9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY9.Location = new System.Drawing.Point(616, 50);
            this.txtVVGATE_DELAY9.Name = "txtVVGATE_DELAY9";
            this.txtVVGATE_DELAY9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY9.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY9.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY9.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY9.TabIndex = 1;
            this.txtVVGATE_DELAY9.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY10
            // 
            this.txtVVGATE_DELAY10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY10.Location = new System.Drawing.Point(684, 50);
            this.txtVVGATE_DELAY10.Name = "txtVVGATE_DELAY10";
            this.txtVVGATE_DELAY10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY10.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY10.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY10.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY10.TabIndex = 1;
            this.txtVVGATE_DELAY10.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN1
            // 
            this.txtVVGATE_OPEN1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN1.Location = new System.Drawing.Point(72, 96);
            this.txtVVGATE_OPEN1.Name = "txtVVGATE_OPEN1";
            this.txtVVGATE_OPEN1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN1.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN1.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN1.TabIndex = 1;
            this.txtVVGATE_OPEN1.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN2
            // 
            this.txtVVGATE_OPEN2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN2.Location = new System.Drawing.Point(140, 96);
            this.txtVVGATE_OPEN2.Name = "txtVVGATE_OPEN2";
            this.txtVVGATE_OPEN2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN2.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN2.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN2.TabIndex = 1;
            this.txtVVGATE_OPEN2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN3
            // 
            this.txtVVGATE_OPEN3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN3.Location = new System.Drawing.Point(208, 96);
            this.txtVVGATE_OPEN3.Name = "txtVVGATE_OPEN3";
            this.txtVVGATE_OPEN3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN3.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN3.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN3.TabIndex = 1;
            this.txtVVGATE_OPEN3.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN4
            // 
            this.txtVVGATE_OPEN4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN4.Location = new System.Drawing.Point(276, 96);
            this.txtVVGATE_OPEN4.Name = "txtVVGATE_OPEN4";
            this.txtVVGATE_OPEN4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN4.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN4.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN4.TabIndex = 1;
            this.txtVVGATE_OPEN4.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN5
            // 
            this.txtVVGATE_OPEN5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN5.Location = new System.Drawing.Point(344, 96);
            this.txtVVGATE_OPEN5.Name = "txtVVGATE_OPEN5";
            this.txtVVGATE_OPEN5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN5.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN5.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN5.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN5.TabIndex = 1;
            this.txtVVGATE_OPEN5.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN6
            // 
            this.txtVVGATE_OPEN6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN6.Location = new System.Drawing.Point(412, 96);
            this.txtVVGATE_OPEN6.Name = "txtVVGATE_OPEN6";
            this.txtVVGATE_OPEN6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN6.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN6.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN6.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN6.TabIndex = 1;
            this.txtVVGATE_OPEN6.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN7
            // 
            this.txtVVGATE_OPEN7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN7.Location = new System.Drawing.Point(480, 96);
            this.txtVVGATE_OPEN7.Name = "txtVVGATE_OPEN7";
            this.txtVVGATE_OPEN7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN7.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN7.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN7.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN7.TabIndex = 1;
            this.txtVVGATE_OPEN7.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN8
            // 
            this.txtVVGATE_OPEN8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN8.Location = new System.Drawing.Point(548, 96);
            this.txtVVGATE_OPEN8.Name = "txtVVGATE_OPEN8";
            this.txtVVGATE_OPEN8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN8.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN8.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN8.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN8.TabIndex = 1;
            this.txtVVGATE_OPEN8.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN9
            // 
            this.txtVVGATE_OPEN9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN9.Location = new System.Drawing.Point(616, 96);
            this.txtVVGATE_OPEN9.Name = "txtVVGATE_OPEN9";
            this.txtVVGATE_OPEN9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN9.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN9.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN9.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN9.TabIndex = 1;
            this.txtVVGATE_OPEN9.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN10
            // 
            this.txtVVGATE_OPEN10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN10.Location = new System.Drawing.Point(684, 96);
            this.txtVVGATE_OPEN10.Name = "txtVVGATE_OPEN10";
            this.txtVVGATE_OPEN10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN10.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN10.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN10.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN10.TabIndex = 1;
            this.txtVVGATE_OPEN10.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY1
            // 
            this.txtVVGATE_DELAY1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY1.Location = new System.Drawing.Point(72, 50);
            this.txtVVGATE_DELAY1.Name = "txtVVGATE_DELAY1";
            this.txtVVGATE_DELAY1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY1.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY1.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY1.TabIndex = 1;
            this.txtVVGATE_DELAY1.Click += new System.EventHandler(this.txt_Click);
            // 
            // labelControl36
            // 
            this.labelControl36.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl36.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl36.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl36.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl36.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl36.Location = new System.Drawing.Point(749, 1);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(67, 45);
            this.labelControl36.TabIndex = 0;
            this.labelControl36.Text = "11";
            // 
            // labelControl37
            // 
            this.labelControl37.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl37.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl37.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl37.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl37.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl37.Location = new System.Drawing.Point(817, 1);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(67, 45);
            this.labelControl37.TabIndex = 0;
            this.labelControl37.Text = "12";
            // 
            // labelControl38
            // 
            this.labelControl38.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl38.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl38.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl38.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl38.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl38.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl38.Location = new System.Drawing.Point(885, 1);
            this.labelControl38.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(67, 45);
            this.labelControl38.TabIndex = 0;
            this.labelControl38.Text = "13";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl39.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl39.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl39.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl39.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl39.Location = new System.Drawing.Point(953, 1);
            this.labelControl39.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(67, 45);
            this.labelControl39.TabIndex = 0;
            this.labelControl39.Text = "14";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl40.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl40.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl40.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl40.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl40.Location = new System.Drawing.Point(1021, 1);
            this.labelControl40.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(74, 45);
            this.labelControl40.TabIndex = 0;
            this.labelControl40.Text = "15";
            // 
            // txtVVGATE_DELAY11
            // 
            this.txtVVGATE_DELAY11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY11.Location = new System.Drawing.Point(752, 50);
            this.txtVVGATE_DELAY11.Name = "txtVVGATE_DELAY11";
            this.txtVVGATE_DELAY11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY11.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY11.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY11.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY11.TabIndex = 1;
            this.txtVVGATE_DELAY11.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY12
            // 
            this.txtVVGATE_DELAY12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY12.Location = new System.Drawing.Point(820, 50);
            this.txtVVGATE_DELAY12.Name = "txtVVGATE_DELAY12";
            this.txtVVGATE_DELAY12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY12.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY12.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY12.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY12.TabIndex = 1;
            this.txtVVGATE_DELAY12.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY13
            // 
            this.txtVVGATE_DELAY13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY13.Location = new System.Drawing.Point(888, 50);
            this.txtVVGATE_DELAY13.Name = "txtVVGATE_DELAY13";
            this.txtVVGATE_DELAY13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY13.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY13.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY13.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY13.TabIndex = 1;
            this.txtVVGATE_DELAY13.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY14
            // 
            this.txtVVGATE_DELAY14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY14.Location = new System.Drawing.Point(956, 50);
            this.txtVVGATE_DELAY14.Name = "txtVVGATE_DELAY14";
            this.txtVVGATE_DELAY14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY14.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY14.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY14.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_DELAY14.TabIndex = 1;
            this.txtVVGATE_DELAY14.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_DELAY15
            // 
            this.txtVVGATE_DELAY15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_DELAY15.Location = new System.Drawing.Point(1024, 50);
            this.txtVVGATE_DELAY15.Name = "txtVVGATE_DELAY15";
            this.txtVVGATE_DELAY15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_DELAY15.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_DELAY15.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_DELAY15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_DELAY15.Size = new System.Drawing.Size(68, 36);
            this.txtVVGATE_DELAY15.TabIndex = 1;
            this.txtVVGATE_DELAY15.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN11
            // 
            this.txtVVGATE_OPEN11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN11.Location = new System.Drawing.Point(752, 96);
            this.txtVVGATE_OPEN11.Name = "txtVVGATE_OPEN11";
            this.txtVVGATE_OPEN11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN11.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN11.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN11.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN11.TabIndex = 1;
            this.txtVVGATE_OPEN11.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN12
            // 
            this.txtVVGATE_OPEN12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN12.Location = new System.Drawing.Point(820, 96);
            this.txtVVGATE_OPEN12.Name = "txtVVGATE_OPEN12";
            this.txtVVGATE_OPEN12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN12.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN12.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN12.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN12.TabIndex = 1;
            this.txtVVGATE_OPEN12.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN13
            // 
            this.txtVVGATE_OPEN13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN13.Location = new System.Drawing.Point(888, 96);
            this.txtVVGATE_OPEN13.Name = "txtVVGATE_OPEN13";
            this.txtVVGATE_OPEN13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN13.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN13.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN13.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN13.TabIndex = 1;
            this.txtVVGATE_OPEN13.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN14
            // 
            this.txtVVGATE_OPEN14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN14.Location = new System.Drawing.Point(956, 96);
            this.txtVVGATE_OPEN14.Name = "txtVVGATE_OPEN14";
            this.txtVVGATE_OPEN14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN14.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN14.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN14.Size = new System.Drawing.Size(61, 36);
            this.txtVVGATE_OPEN14.TabIndex = 1;
            this.txtVVGATE_OPEN14.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtVVGATE_OPEN15
            // 
            this.txtVVGATE_OPEN15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVVGATE_OPEN15.Location = new System.Drawing.Point(1024, 96);
            this.txtVVGATE_OPEN15.Name = "txtVVGATE_OPEN15";
            this.txtVVGATE_OPEN15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtVVGATE_OPEN15.Properties.Appearance.Options.UseFont = true;
            this.txtVVGATE_OPEN15.Properties.Appearance.Options.UseTextOptions = true;
            this.txtVVGATE_OPEN15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtVVGATE_OPEN15.Size = new System.Drawing.Size(68, 36);
            this.txtVVGATE_OPEN15.TabIndex = 1;
            this.txtVVGATE_OPEN15.Click += new System.EventHandler(this.txt_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.56522F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.34783F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.08696F));
            this.tableLayoutPanel3.Controls.Add(this.labelControl12, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl13, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl14, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelControl15, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelControl16, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtCHARGE_PRE, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtCHARGE_SPEED, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtCHARGE_DISTANCE, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtCHARGE_PREVENT, 2, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(732, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(316, 149);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl12.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl12.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl12.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl12.Location = new System.Drawing.Point(1, 1);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl12.Name = "labelControl12";
            this.tableLayoutPanel3.SetRowSpan(this.labelControl12, 4);
            this.labelControl12.Size = new System.Drawing.Size(61, 147);
            this.labelControl12.TabIndex = 0;
            this.labelControl12.Text = "계량";
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl13.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl13.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl13.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl13.Location = new System.Drawing.Point(63, 1);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(169, 36);
            this.labelControl13.TabIndex = 0;
            this.labelControl13.Text = "계량압력";
            // 
            // labelControl14
            // 
            this.labelControl14.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl14.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl14.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl14.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl14.Location = new System.Drawing.Point(63, 38);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(169, 36);
            this.labelControl14.TabIndex = 0;
            this.labelControl14.Text = "계량속도";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl15.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl15.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl15.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl15.Location = new System.Drawing.Point(63, 75);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(169, 36);
            this.labelControl15.TabIndex = 0;
            this.labelControl15.Text = "계량거리(mm)";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl16.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl16.Appearance.Font = new System.Drawing.Font("맑은 고딕", 13F);
            this.labelControl16.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl16.Location = new System.Drawing.Point(63, 112);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(169, 36);
            this.labelControl16.TabIndex = 0;
            this.labelControl16.Text = "홀림방지거리(mm)";
            // 
            // txtCHARGE_PRE
            // 
            this.txtCHARGE_PRE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCHARGE_PRE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCHARGE_PRE.Location = new System.Drawing.Point(233, 1);
            this.txtCHARGE_PRE.Margin = new System.Windows.Forms.Padding(0);
            this.txtCHARGE_PRE.Name = "txtCHARGE_PRE";
            this.txtCHARGE_PRE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtCHARGE_PRE.Properties.Appearance.Options.UseFont = true;
            this.txtCHARGE_PRE.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCHARGE_PRE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCHARGE_PRE.Properties.NullText = "0";
            this.txtCHARGE_PRE.Size = new System.Drawing.Size(82, 36);
            this.txtCHARGE_PRE.TabIndex = 1;
            this.txtCHARGE_PRE.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtCHARGE_SPEED
            // 
            this.txtCHARGE_SPEED.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCHARGE_SPEED.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCHARGE_SPEED.Location = new System.Drawing.Point(233, 38);
            this.txtCHARGE_SPEED.Margin = new System.Windows.Forms.Padding(0);
            this.txtCHARGE_SPEED.Name = "txtCHARGE_SPEED";
            this.txtCHARGE_SPEED.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtCHARGE_SPEED.Properties.Appearance.Options.UseFont = true;
            this.txtCHARGE_SPEED.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCHARGE_SPEED.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCHARGE_SPEED.Properties.NullText = "0";
            this.txtCHARGE_SPEED.Size = new System.Drawing.Size(82, 36);
            this.txtCHARGE_SPEED.TabIndex = 1;
            this.txtCHARGE_SPEED.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtCHARGE_DISTANCE
            // 
            this.txtCHARGE_DISTANCE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCHARGE_DISTANCE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCHARGE_DISTANCE.Location = new System.Drawing.Point(233, 75);
            this.txtCHARGE_DISTANCE.Margin = new System.Windows.Forms.Padding(0);
            this.txtCHARGE_DISTANCE.Name = "txtCHARGE_DISTANCE";
            this.txtCHARGE_DISTANCE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtCHARGE_DISTANCE.Properties.Appearance.Options.UseFont = true;
            this.txtCHARGE_DISTANCE.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCHARGE_DISTANCE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCHARGE_DISTANCE.Properties.NullText = "0";
            this.txtCHARGE_DISTANCE.Size = new System.Drawing.Size(82, 36);
            this.txtCHARGE_DISTANCE.TabIndex = 1;
            this.txtCHARGE_DISTANCE.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtCHARGE_PREVENT
            // 
            this.txtCHARGE_PREVENT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCHARGE_PREVENT.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCHARGE_PREVENT.Location = new System.Drawing.Point(233, 112);
            this.txtCHARGE_PREVENT.Margin = new System.Windows.Forms.Padding(0);
            this.txtCHARGE_PREVENT.Name = "txtCHARGE_PREVENT";
            this.txtCHARGE_PREVENT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtCHARGE_PREVENT.Properties.Appearance.Options.UseFont = true;
            this.txtCHARGE_PREVENT.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCHARGE_PREVENT.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtCHARGE_PREVENT.Properties.NullText = "0";
            this.txtCHARGE_PREVENT.Size = new System.Drawing.Size(82, 36);
            this.txtCHARGE_PREVENT.TabIndex = 1;
            this.txtCHARGE_PREVENT.Click += new System.EventHandler(this.txt_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.97674F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.60465F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.88372F));
            this.tableLayoutPanel2.Controls.Add(this.labelControl7, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl8, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl9, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelControl10, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl11, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtPRESSURE_INJ1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtPRESSURE_INJ2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtPRE_MOLD_SPEED, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtMOLD_PRESSURE, 2, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(16, 159);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(269, 149);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl7.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(1, 1);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl7.Name = "labelControl7";
            this.tableLayoutPanel2.SetRowSpan(this.labelControl7, 4);
            this.labelControl7.Size = new System.Drawing.Size(71, 147);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "압력\r\n(㎏/㎠)";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl8.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl8.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(73, 1);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(101, 36);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "사출1차";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl9.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl9.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(73, 38);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(101, 36);
            this.labelControl9.TabIndex = 0;
            this.labelControl9.Text = "사출2차";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl10.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl10.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl10.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl10.Location = new System.Drawing.Point(73, 75);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(101, 36);
            this.labelControl10.TabIndex = 0;
            this.labelControl10.Text = "형체속도";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl11.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl11.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl11.Location = new System.Drawing.Point(73, 112);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(101, 36);
            this.labelControl11.TabIndex = 0;
            this.labelControl11.Text = "형체압력";
            // 
            // txtPRESSURE_INJ1
            // 
            this.txtPRESSURE_INJ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPRESSURE_INJ1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPRESSURE_INJ1.Location = new System.Drawing.Point(175, 1);
            this.txtPRESSURE_INJ1.Margin = new System.Windows.Forms.Padding(0);
            this.txtPRESSURE_INJ1.Name = "txtPRESSURE_INJ1";
            this.txtPRESSURE_INJ1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtPRESSURE_INJ1.Properties.Appearance.Options.UseFont = true;
            this.txtPRESSURE_INJ1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPRESSURE_INJ1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPRESSURE_INJ1.Properties.NullText = "0";
            this.txtPRESSURE_INJ1.Size = new System.Drawing.Size(93, 36);
            this.txtPRESSURE_INJ1.TabIndex = 1;
            this.txtPRESSURE_INJ1.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtPRESSURE_INJ2
            // 
            this.txtPRESSURE_INJ2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPRESSURE_INJ2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPRESSURE_INJ2.Location = new System.Drawing.Point(175, 38);
            this.txtPRESSURE_INJ2.Margin = new System.Windows.Forms.Padding(0);
            this.txtPRESSURE_INJ2.Name = "txtPRESSURE_INJ2";
            this.txtPRESSURE_INJ2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtPRESSURE_INJ2.Properties.Appearance.Options.UseFont = true;
            this.txtPRESSURE_INJ2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPRESSURE_INJ2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPRESSURE_INJ2.Properties.NullText = "0";
            this.txtPRESSURE_INJ2.Size = new System.Drawing.Size(93, 36);
            this.txtPRESSURE_INJ2.TabIndex = 1;
            this.txtPRESSURE_INJ2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtPRE_MOLD_SPEED
            // 
            this.txtPRE_MOLD_SPEED.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPRE_MOLD_SPEED.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtPRE_MOLD_SPEED.Location = new System.Drawing.Point(175, 75);
            this.txtPRE_MOLD_SPEED.Margin = new System.Windows.Forms.Padding(0);
            this.txtPRE_MOLD_SPEED.Name = "txtPRE_MOLD_SPEED";
            this.txtPRE_MOLD_SPEED.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtPRE_MOLD_SPEED.Properties.Appearance.Options.UseFont = true;
            this.txtPRE_MOLD_SPEED.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPRE_MOLD_SPEED.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtPRE_MOLD_SPEED.Properties.NullText = "0";
            this.txtPRE_MOLD_SPEED.Size = new System.Drawing.Size(93, 36);
            this.txtPRE_MOLD_SPEED.TabIndex = 1;
            this.txtPRE_MOLD_SPEED.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtMOLD_PRESSURE
            // 
            this.txtMOLD_PRESSURE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMOLD_PRESSURE.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtMOLD_PRESSURE.Location = new System.Drawing.Point(175, 112);
            this.txtMOLD_PRESSURE.Margin = new System.Windows.Forms.Padding(0);
            this.txtMOLD_PRESSURE.Name = "txtMOLD_PRESSURE";
            this.txtMOLD_PRESSURE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtMOLD_PRESSURE.Properties.Appearance.Options.UseFont = true;
            this.txtMOLD_PRESSURE.Properties.Appearance.Options.UseTextOptions = true;
            this.txtMOLD_PRESSURE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtMOLD_PRESSURE.Properties.NullText = "0";
            this.txtMOLD_PRESSURE.Size = new System.Drawing.Size(93, 36);
            this.txtMOLD_PRESSURE.TabIndex = 1;
            this.txtMOLD_PRESSURE.Click += new System.EventHandler(this.txt_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.80831F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.3131F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.87859F));
            this.tableLayoutPanel5.Controls.Add(this.labelControl21, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelControl22, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelControl23, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.labelControl24, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtTIME_INJ, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtTIME_COOLING, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtTIME_TOTAL, 2, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(733, 159);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(314, 113);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // labelControl21
            // 
            this.labelControl21.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl21.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl21.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl21.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl21.Location = new System.Drawing.Point(1, 1);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl21.Name = "labelControl21";
            this.tableLayoutPanel5.SetRowSpan(this.labelControl21, 3);
            this.labelControl21.Size = new System.Drawing.Size(61, 111);
            this.labelControl21.TabIndex = 0;
            this.labelControl21.Text = "시간\r\n(sec)";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl22.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl22.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl22.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl22.Location = new System.Drawing.Point(63, 1);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(168, 36);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "사출";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl23.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl23.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl23.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl23.Location = new System.Drawing.Point(63, 38);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(168, 36);
            this.labelControl23.TabIndex = 0;
            this.labelControl23.Text = "냉각";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl24.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl24.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl24.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl24.Location = new System.Drawing.Point(63, 75);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(168, 37);
            this.labelControl24.TabIndex = 0;
            this.labelControl24.Text = "종합";
            // 
            // txtTIME_INJ
            // 
            this.txtTIME_INJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTIME_INJ.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTIME_INJ.Location = new System.Drawing.Point(232, 1);
            this.txtTIME_INJ.Margin = new System.Windows.Forms.Padding(0);
            this.txtTIME_INJ.Name = "txtTIME_INJ";
            this.txtTIME_INJ.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTIME_INJ.Properties.Appearance.Options.UseFont = true;
            this.txtTIME_INJ.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTIME_INJ.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTIME_INJ.Properties.NullText = "0";
            this.txtTIME_INJ.Size = new System.Drawing.Size(81, 36);
            this.txtTIME_INJ.TabIndex = 1;
            this.txtTIME_INJ.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTIME_COOLING
            // 
            this.txtTIME_COOLING.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTIME_COOLING.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTIME_COOLING.Location = new System.Drawing.Point(232, 38);
            this.txtTIME_COOLING.Margin = new System.Windows.Forms.Padding(0);
            this.txtTIME_COOLING.Name = "txtTIME_COOLING";
            this.txtTIME_COOLING.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTIME_COOLING.Properties.Appearance.Options.UseFont = true;
            this.txtTIME_COOLING.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTIME_COOLING.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTIME_COOLING.Properties.NullText = "0";
            this.txtTIME_COOLING.Size = new System.Drawing.Size(81, 36);
            this.txtTIME_COOLING.TabIndex = 1;
            this.txtTIME_COOLING.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTIME_TOTAL
            // 
            this.txtTIME_TOTAL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTIME_TOTAL.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTIME_TOTAL.Location = new System.Drawing.Point(232, 75);
            this.txtTIME_TOTAL.Margin = new System.Windows.Forms.Padding(0);
            this.txtTIME_TOTAL.Name = "txtTIME_TOTAL";
            this.txtTIME_TOTAL.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTIME_TOTAL.Properties.Appearance.Options.UseFont = true;
            this.txtTIME_TOTAL.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTIME_TOTAL.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTIME_TOTAL.Properties.NullText = "0";
            this.txtTIME_TOTAL.Size = new System.Drawing.Size(81, 36);
            this.txtTIME_TOTAL.TabIndex = 1;
            this.txtTIME_TOTAL.Click += new System.EventHandler(this.txt_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.85185F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.42593F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.72223F));
            this.tableLayoutPanel4.Controls.Add(this.labelControl17, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelControl18, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelControl19, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.labelControl20, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.txtSPEED_INJ1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtSPEED_INJ2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtSPEED_INJ3, 2, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(354, 159);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(311, 113);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl17.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl17.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl17.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(1, 1);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl17.Name = "labelControl17";
            this.tableLayoutPanel4.SetRowSpan(this.labelControl17, 3);
            this.labelControl17.Size = new System.Drawing.Size(82, 111);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "속도\r\n(mm/s)";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl18.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl18.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl18.Location = new System.Drawing.Point(84, 1);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(117, 36);
            this.labelControl18.TabIndex = 0;
            this.labelControl18.Text = "사출속도1";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl19.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl19.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl19.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl19.Location = new System.Drawing.Point(84, 38);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(117, 36);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "사출속도2";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl20.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl20.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl20.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl20.Location = new System.Drawing.Point(84, 75);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(117, 37);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "사출속도3";
            // 
            // txtSPEED_INJ1
            // 
            this.txtSPEED_INJ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSPEED_INJ1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSPEED_INJ1.Location = new System.Drawing.Point(202, 1);
            this.txtSPEED_INJ1.Margin = new System.Windows.Forms.Padding(0);
            this.txtSPEED_INJ1.Name = "txtSPEED_INJ1";
            this.txtSPEED_INJ1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtSPEED_INJ1.Properties.Appearance.Options.UseFont = true;
            this.txtSPEED_INJ1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSPEED_INJ1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSPEED_INJ1.Properties.NullText = "0";
            this.txtSPEED_INJ1.Size = new System.Drawing.Size(108, 36);
            this.txtSPEED_INJ1.TabIndex = 1;
            this.txtSPEED_INJ1.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtSPEED_INJ2
            // 
            this.txtSPEED_INJ2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSPEED_INJ2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSPEED_INJ2.Location = new System.Drawing.Point(202, 38);
            this.txtSPEED_INJ2.Margin = new System.Windows.Forms.Padding(0);
            this.txtSPEED_INJ2.Name = "txtSPEED_INJ2";
            this.txtSPEED_INJ2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtSPEED_INJ2.Properties.Appearance.Options.UseFont = true;
            this.txtSPEED_INJ2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSPEED_INJ2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSPEED_INJ2.Properties.NullText = "0";
            this.txtSPEED_INJ2.Size = new System.Drawing.Size(108, 36);
            this.txtSPEED_INJ2.TabIndex = 1;
            this.txtSPEED_INJ2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtSPEED_INJ3
            // 
            this.txtSPEED_INJ3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSPEED_INJ3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSPEED_INJ3.Location = new System.Drawing.Point(202, 75);
            this.txtSPEED_INJ3.Margin = new System.Windows.Forms.Padding(0);
            this.txtSPEED_INJ3.Name = "txtSPEED_INJ3";
            this.txtSPEED_INJ3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtSPEED_INJ3.Properties.Appearance.Options.UseFont = true;
            this.txtSPEED_INJ3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSPEED_INJ3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtSPEED_INJ3.Properties.NullText = "0";
            this.txtSPEED_INJ3.Size = new System.Drawing.Size(108, 36);
            this.txtSPEED_INJ3.TabIndex = 1;
            this.txtSPEED_INJ3.Click += new System.EventHandler(this.txt_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.97674F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.60465F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.88372F));
            this.tableLayoutPanel7.Controls.Add(this.labelControl41, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.labelControl42, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtTEMP_CYLINDER3, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtTEMP_CYLINDER4, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtTEMP_CYLINDER5, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.txtTEMP_CYLINDER6, 2, 3);
            this.tableLayoutPanel7.Controls.Add(this.labelControl43, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.labelControl44, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.labelControl45, 1, 3);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(354, 5);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(311, 149);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // labelControl41
            // 
            this.labelControl41.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl41.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl41.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl41.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl41.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl41.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl41.Location = new System.Drawing.Point(1, 1);
            this.labelControl41.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl41.Name = "labelControl41";
            this.tableLayoutPanel7.SetRowSpan(this.labelControl41, 4);
            this.labelControl41.Size = new System.Drawing.Size(82, 147);
            this.labelControl41.TabIndex = 0;
            this.labelControl41.Text = "온도\r\n(℃)";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl42.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl42.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl42.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl42.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl42.Location = new System.Drawing.Point(84, 1);
            this.labelControl42.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(117, 36);
            this.labelControl42.TabIndex = 0;
            this.labelControl42.Text = "실린더3";
            // 
            // txtTEMP_CYLINDER3
            // 
            this.txtTEMP_CYLINDER3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER3.Location = new System.Drawing.Point(202, 1);
            this.txtTEMP_CYLINDER3.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER3.Name = "txtTEMP_CYLINDER3";
            this.txtTEMP_CYLINDER3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER3.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER3.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER3.Properties.NullText = "0";
            this.txtTEMP_CYLINDER3.Size = new System.Drawing.Size(108, 36);
            this.txtTEMP_CYLINDER3.TabIndex = 1;
            this.txtTEMP_CYLINDER3.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTEMP_CYLINDER4
            // 
            this.txtTEMP_CYLINDER4.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER4.Location = new System.Drawing.Point(202, 38);
            this.txtTEMP_CYLINDER4.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER4.Name = "txtTEMP_CYLINDER4";
            this.txtTEMP_CYLINDER4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER4.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER4.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER4.Properties.NullText = "0";
            this.txtTEMP_CYLINDER4.Size = new System.Drawing.Size(108, 36);
            this.txtTEMP_CYLINDER4.TabIndex = 1;
            this.txtTEMP_CYLINDER4.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTEMP_CYLINDER5
            // 
            this.txtTEMP_CYLINDER5.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER5.Location = new System.Drawing.Point(202, 75);
            this.txtTEMP_CYLINDER5.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER5.Name = "txtTEMP_CYLINDER5";
            this.txtTEMP_CYLINDER5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER5.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER5.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER5.Properties.NullText = "0";
            this.txtTEMP_CYLINDER5.Size = new System.Drawing.Size(108, 36);
            this.txtTEMP_CYLINDER5.TabIndex = 1;
            this.txtTEMP_CYLINDER5.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTEMP_CYLINDER6
            // 
            this.txtTEMP_CYLINDER6.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER6.Location = new System.Drawing.Point(202, 112);
            this.txtTEMP_CYLINDER6.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER6.Name = "txtTEMP_CYLINDER6";
            this.txtTEMP_CYLINDER6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER6.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER6.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER6.Properties.NullText = "0";
            this.txtTEMP_CYLINDER6.Size = new System.Drawing.Size(108, 36);
            this.txtTEMP_CYLINDER6.TabIndex = 1;
            this.txtTEMP_CYLINDER6.Click += new System.EventHandler(this.txt_Click);
            // 
            // labelControl43
            // 
            this.labelControl43.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl43.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl43.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl43.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl43.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl43.Location = new System.Drawing.Point(84, 38);
            this.labelControl43.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(117, 36);
            this.labelControl43.TabIndex = 0;
            this.labelControl43.Text = "실린더4";
            // 
            // labelControl44
            // 
            this.labelControl44.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl44.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl44.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl44.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl44.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl44.Location = new System.Drawing.Point(84, 75);
            this.labelControl44.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(117, 36);
            this.labelControl44.TabIndex = 0;
            this.labelControl44.Text = "실린더5";
            // 
            // labelControl45
            // 
            this.labelControl45.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl45.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl45.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl45.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl45.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl45.Location = new System.Drawing.Point(84, 112);
            this.labelControl45.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl45.Name = "labelControl45";
            this.labelControl45.Size = new System.Drawing.Size(117, 36);
            this.labelControl45.TabIndex = 0;
            this.labelControl45.Text = "실린더6";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.97674F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.60465F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.88372F));
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtTEMP_NOZZLE1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl6, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtTEMP_CYLINDER2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtTEMP_CYLINDER1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtTEMP_NOZZLE2, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(268, 149);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(1, 1);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.tableLayoutPanel1.SetRowSpan(this.labelControl2, 4);
            this.labelControl2.Size = new System.Drawing.Size(70, 147);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "온도\r\n(℃)";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl3.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl3.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(72, 1);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(101, 36);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "노즐온도1";
            // 
            // txtTEMP_NOZZLE1
            // 
            this.txtTEMP_NOZZLE1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTEMP_NOZZLE1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_NOZZLE1.Location = new System.Drawing.Point(174, 1);
            this.txtTEMP_NOZZLE1.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_NOZZLE1.Name = "txtTEMP_NOZZLE1";
            this.txtTEMP_NOZZLE1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_NOZZLE1.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_NOZZLE1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_NOZZLE1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_NOZZLE1.Properties.Mask.EditMask = "n1";
            this.txtTEMP_NOZZLE1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtTEMP_NOZZLE1.Properties.NullText = "0";
            this.txtTEMP_NOZZLE1.Size = new System.Drawing.Size(93, 36);
            this.txtTEMP_NOZZLE1.TabIndex = 1;
            this.txtTEMP_NOZZLE1.Click += new System.EventHandler(this.txt_Click);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl5.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl5.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(72, 112);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(101, 36);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "실린더2";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl4.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(72, 75);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(101, 36);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "실린더1";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Lavender;
            this.labelControl6.Appearance.BackColor2 = System.Drawing.Color.LightSkyBlue;
            this.labelControl6.Appearance.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(72, 38);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(101, 36);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "노즐온도2";
            // 
            // txtTEMP_CYLINDER2
            // 
            this.txtTEMP_CYLINDER2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER2.Location = new System.Drawing.Point(174, 112);
            this.txtTEMP_CYLINDER2.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER2.Name = "txtTEMP_CYLINDER2";
            this.txtTEMP_CYLINDER2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER2.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER2.Properties.NullText = "0";
            this.txtTEMP_CYLINDER2.Size = new System.Drawing.Size(93, 36);
            this.txtTEMP_CYLINDER2.TabIndex = 1;
            this.txtTEMP_CYLINDER2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTEMP_CYLINDER1
            // 
            this.txtTEMP_CYLINDER1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_CYLINDER1.Location = new System.Drawing.Point(174, 75);
            this.txtTEMP_CYLINDER1.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_CYLINDER1.Name = "txtTEMP_CYLINDER1";
            this.txtTEMP_CYLINDER1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_CYLINDER1.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_CYLINDER1.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_CYLINDER1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_CYLINDER1.Properties.NullText = "0";
            this.txtTEMP_CYLINDER1.Size = new System.Drawing.Size(93, 36);
            this.txtTEMP_CYLINDER1.TabIndex = 1;
            this.txtTEMP_CYLINDER1.Click += new System.EventHandler(this.txt_Click);
            // 
            // txtTEMP_NOZZLE2
            // 
            this.txtTEMP_NOZZLE2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTEMP_NOZZLE2.Location = new System.Drawing.Point(174, 38);
            this.txtTEMP_NOZZLE2.Margin = new System.Windows.Forms.Padding(0);
            this.txtTEMP_NOZZLE2.Name = "txtTEMP_NOZZLE2";
            this.txtTEMP_NOZZLE2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtTEMP_NOZZLE2.Properties.Appearance.Options.UseFont = true;
            this.txtTEMP_NOZZLE2.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTEMP_NOZZLE2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtTEMP_NOZZLE2.Properties.NullText = "0";
            this.txtTEMP_NOZZLE2.Size = new System.Drawing.Size(93, 36);
            this.txtTEMP_NOZZLE2.TabIndex = 1;
            this.txtTEMP_NOZZLE2.Click += new System.EventHandler(this.txt_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 150;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Injection_Condition_Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1123, 588);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Injection_Condition_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Injection_Condition_Popup";
            this.Load += new System.EventHandler(this.Injection_Condition_Popup_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_DELAY15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVVGATE_OPEN15.Properties)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_PRE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_SPEED.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_DISTANCE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCHARGE_PREVENT.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPRESSURE_INJ1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPRESSURE_INJ2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPRE_MOLD_SPEED.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMOLD_PRESSURE.Properties)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_INJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_COOLING.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTIME_TOTAL.Properties)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSPEED_INJ3.Properties)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER6.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_NOZZLE1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_CYLINDER1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTEMP_NOZZLE2.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl47;
        private DevExpress.XtraEditors.LabelControl labelControl48;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY2;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY3;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY4;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY5;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY6;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY7;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY8;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY9;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY10;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN1;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN2;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN3;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN4;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN5;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN6;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN7;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN8;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN9;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.TextEdit txtCHARGE_PRE;
        private DevExpress.XtraEditors.TextEdit txtCHARGE_SPEED;
        private DevExpress.XtraEditors.TextEdit txtCHARGE_DISTANCE;
        private DevExpress.XtraEditors.TextEdit txtCHARGE_PREVENT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtPRESSURE_INJ1;
        private DevExpress.XtraEditors.TextEdit txtPRESSURE_INJ2;
        private DevExpress.XtraEditors.TextEdit txtPRE_MOLD_SPEED;
        private DevExpress.XtraEditors.TextEdit txtMOLD_PRESSURE;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtTIME_INJ;
        private DevExpress.XtraEditors.TextEdit txtTIME_COOLING;
        private DevExpress.XtraEditors.TextEdit txtTIME_TOTAL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtSPEED_INJ1;
        private DevExpress.XtraEditors.TextEdit txtSPEED_INJ2;
        private DevExpress.XtraEditors.TextEdit txtSPEED_INJ3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER1;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER2;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER3;
        private System.Windows.Forms.Button btn_h_3_down;
        private System.Windows.Forms.Button btn_h_2_down;
        private System.Windows.Forms.Button btn_h_1_down;
        private System.Windows.Forms.Button btn_s_3_down;
        private System.Windows.Forms.Button btn_s_2_down;
        private System.Windows.Forms.Button btn_s_1_down;
        private System.Windows.Forms.Button btn_w_4_down;
        private System.Windows.Forms.Button btn_w_3_down;
        private System.Windows.Forms.Button btn_w_2_down;
        private System.Windows.Forms.Button btn_w_1_down;
        private System.Windows.Forms.Button btn_p_4_down;
        private System.Windows.Forms.Button btn_p_3_down;
        private System.Windows.Forms.Button btn_p_2_down;
        private System.Windows.Forms.Button btn_t_4_down;
        private System.Windows.Forms.Button btn_t_3_down;
        private System.Windows.Forms.Button btn_t_2_down;
        private System.Windows.Forms.Button btn_t_1_down;
        private System.Windows.Forms.Button btn_p_1_down;
        private System.Windows.Forms.Button btn_h_3_up;
        private System.Windows.Forms.Button btn_h_2_up;
        private System.Windows.Forms.Button btn_h_1_up;
        private System.Windows.Forms.Button btn_s_3_up;
        private System.Windows.Forms.Button btn_s_2_up;
        private System.Windows.Forms.Button btn_s_1_up;
        private System.Windows.Forms.Button btn_w_4_up;
        private System.Windows.Forms.Button btn_w_3_up;
        private System.Windows.Forms.Button btn_w_2_up;
        private System.Windows.Forms.Button btn_w_1_up;
        private System.Windows.Forms.Button btn_p_4_up;
        private System.Windows.Forms.Button btn_p_3_up;
        private System.Windows.Forms.Button btn_p_2_up;
        private System.Windows.Forms.Button btn_t_4_up;
        private System.Windows.Forms.Button btn_t_3_up;
        private System.Windows.Forms.Button btn_t_2_up;
        private System.Windows.Forms.Button btn_t_1_up;
        private System.Windows.Forms.Button btn_p_1_up;
        private System.Windows.Forms.Button btn_save;
        private DevExpress.XtraEditors.TextEdit txtTEMP_NOZZLE1;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY1;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY11;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY12;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY13;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY14;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_DELAY15;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN11;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN12;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN13;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN14;
        private DevExpress.XtraEditors.TextEdit txtVVGATE_OPEN15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txtTEMP_NOZZLE2;
        private System.Windows.Forms.Button btn_t_8_down;
        private System.Windows.Forms.Button btn_t_7_down;
        private System.Windows.Forms.Button btn_t_6_down;
        private System.Windows.Forms.Button btn_t_5_down;
        private System.Windows.Forms.Button btn_t_8_up;
        private System.Windows.Forms.Button btn_t_7_up;
        private System.Windows.Forms.Button btn_t_6_up;
        private System.Windows.Forms.Button btn_t_5_up;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER4;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER5;
        private DevExpress.XtraEditors.TextEdit txtTEMP_CYLINDER6;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl labelControl45;
    }
}