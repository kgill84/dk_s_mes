﻿namespace DK_Tablet
{
    partial class PPCard_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.WC_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SITE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SIZE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.CHILD_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MM_LOTNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MM_QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SUNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ME_SNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 60;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WC_CODE,
            this.SITE_CODE,
            this.CARD_NO,
            this.IT_SCODE,
            this.SIZE,
            this.CHECK_YN});
            this.dataGridView1.Location = new System.Drawing.Point(12, 111);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 60;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView1.RowTemplate.Height = 60;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(638, 422);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // WC_CODE
            // 
            this.WC_CODE.DataPropertyName = "WC_CODE";
            this.WC_CODE.HeaderText = "작업장";
            this.WC_CODE.Name = "WC_CODE";
            this.WC_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.DataPropertyName = "SITE_CODE";
            this.SITE_CODE.HeaderText = "공장코드";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SITE_CODE.Visible = false;
            // 
            // CARD_NO
            // 
            this.CARD_NO.DataPropertyName = "CARD_NO";
            this.CARD_NO.HeaderText = "카드번호";
            this.CARD_NO.Name = "CARD_NO";
            this.CARD_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CARD_NO.Width = 105;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.DataPropertyName = "IT_SCODE";
            this.IT_SCODE.HeaderText = "품목코드";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SCODE.Width = 250;
            // 
            // SIZE
            // 
            this.SIZE.DataPropertyName = "SIZE";
            this.SIZE.HeaderText = "수량";
            this.SIZE.Name = "SIZE";
            this.SIZE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SIZE.Width = 69;
            // 
            // CHECK_YN
            // 
            this.CHECK_YN.DataPropertyName = "CHECK_YN";
            this.CHECK_YN.HeaderText = "체크";
            this.CHECK_YN.Name = "CHECK_YN";
            this.CHECK_YN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_done.Location = new System.Drawing.Point(112, 538);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(234, 64);
            this.btn_done.TabIndex = 1;
            this.btn_done.Text = "등     록";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(356, 538);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(234, 64);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫     기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(13, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1270, 5);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(280, 35);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "PPCard 등록";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.ColumnHeadersHeight = 60;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CHILD_ITEM,
            this.IT_SNAME,
            this.MM_LOTNO,
            this.MM_QTY,
            this.IT_SUNIT,
            this.ME_SNAME});
            this.dataGridView2.Location = new System.Drawing.Point(656, 111);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 60;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView2.RowTemplate.Height = 60;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(627, 422);
            this.dataGridView2.TabIndex = 0;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.DataPropertyName = "CHILD_ITEM";
            this.CHILD_ITEM.HeaderText = "하위품목";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CHILD_ITEM.Width = 150;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.DataPropertyName = "IT_SNAME";
            this.IT_SNAME.HeaderText = "품목명";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SNAME.Width = 200;
            // 
            // MM_LOTNO
            // 
            this.MM_LOTNO.DataPropertyName = "MM_LOTNO";
            this.MM_LOTNO.HeaderText = "LOT NO";
            this.MM_LOTNO.Name = "MM_LOTNO";
            this.MM_LOTNO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MM_LOTNO.Width = 200;
            // 
            // MM_QTY
            // 
            this.MM_QTY.DataPropertyName = "MM_QTY";
            this.MM_QTY.HeaderText = "재고수량";
            this.MM_QTY.Name = "MM_QTY";
            this.MM_QTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MM_QTY.Width = 80;
            // 
            // IT_SUNIT
            // 
            this.IT_SUNIT.DataPropertyName = "IT_SUNIT";
            this.IT_SUNIT.HeaderText = "단위";
            this.IT_SUNIT.Name = "IT_SUNIT";
            this.IT_SUNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SUNIT.Width = 60;
            // 
            // ME_SNAME
            // 
            this.ME_SNAME.DataPropertyName = "ME_SNAME";
            this.ME_SNAME.HeaderText = "자재유형";
            this.ME_SNAME.Name = "ME_SNAME";
            this.ME_SNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ME_SNAME.Width = 70;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(259, 44);
            this.label2.TabIndex = 53;
            this.label2.Text = "1. PP카드선택";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(656, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 44);
            this.label1.TabIndex = 53;
            this.label1.Text = "※ 하위품목";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PPCard_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 612);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "PPCard_Search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PPCard 등록";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.DataGridViewTextBoxColumn WC_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SITE_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SIZE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHILD_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn MM_LOTNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MM_QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SUNIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ME_SNAME;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}