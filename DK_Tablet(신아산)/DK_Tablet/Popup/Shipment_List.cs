﻿using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Shipment_List : Form
    {
        public DataTable list_dt { get; set; }
        public Shipment_List()
        {
            InitializeComponent();
        }

        private void Shipment_List_Load(object sender, EventArgs e)
        {
            
            //list_dt.DefaultView.Sort = "CHECK_YN ASC";
            gridControl1.DataSource = list_dt;
            foreach (DataRow dr in list_dt.Rows)
            {

            }
        }

        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (list_dt.Rows.Count == 0)
            {
                MessageBox.Show("데이터가 없습니다.");
                return;
            }
            DataTable dt = new DataTable();
            dt.Columns.Add("IT_SCODE", typeof(string));
            dt.Columns.Add("SQTY", typeof(string));

            foreach (DataRow dr in list_dt.Rows)
            {

                DataRow[] drr = dt.Select("IT_SCODE = '" + dr["IT_SCODE"] + "'");
                if (drr.Length == 0)
                {
                    int sh_sqty = 0;
                    DataRow[] datarow = list_dt.Select("IT_SCODE='" + dr["IT_SCODE"] + "'");
                    for (int i = 0; i < datarow.Length; i++)
                    {
                        sh_sqty = sh_sqty + int.Parse(datarow[i]["SH_SQTY"].ToString());
                    }
                    if (sh_sqty > 0)
                    {
                        DataRow insertRowDET = dt.NewRow();

                        insertRowDET["IT_SCODE"] = dr["IT_SCODE"];
                        insertRowDET["SQTY"] = sh_sqty;
                        dt.Rows.Add(insertRowDET);
                    }
                }

            }
            gridControl3.DataSource = dt;
            excel_export_고객사();
        }

        public void excel_export_고객사()
        {
            string path = @"C:\TIC_DIR";

            //DateTime dt = new DateTime();
            string date = DateTime.Now.ToString("yyyyMMdd");
            string dt_str = DateTime.Now.ToString();
            dt_str = dt_str.Replace(" ", "");
            dt_str = dt_str.Replace(":", "'");
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            DirectoryInfo di = new DirectoryInfo(desktop + @"\출하");
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!di.Exists)
            {
                di.Create();
            }
            DirectoryInfo dir = new DirectoryInfo(desktop + @"\출하\" + date);
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!dir.Exists)
            {
                dir.Create();
            }
            DirectoryInfo dir2 = new DirectoryInfo(desktop + @"\출하\" + date+@"\고객사용\");
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!dir2.Exists)
            {
                dir2.Create();
            }

            string file_path = desktop + @"\출하\" + date + @"\고객사용\";
            file_path += dt_str + ".xls";

            try
            {
                gridControl3.ExportToXls(file_path);
                MessageBox.Show(file_path + " 경로로 저장이 됩니다.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRER : "+ex.Message);
            }

            /*ProcessStartInfo info = new ProcessStartInfo("Excel.exe", file_path);
            try
            {84721C1000
                Process.Start(info);
            }
            catch (Exception ex)
            {
                MessageBox.Show("저장 되었지만 실행 할 수 없습니다.(" + ex.Message + ")");
            }*/
        }
        public void excel_export_업로드용()
        {
            string path = @"C:\TIC_DIR";

            //DateTime dt = new DateTime();
            string date = DateTime.Now.ToString("yyyyMMdd");
            string dt_str = DateTime.Now.ToString();
            dt_str = dt_str.Replace(" ", "");
            dt_str = dt_str.Replace(":", "'");
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            DirectoryInfo di = new DirectoryInfo(desktop + @"\출하");
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!di.Exists)
            {
                di.Create();
            }
            DirectoryInfo dir = new DirectoryInfo(desktop + @"\출하\" + date);
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!dir.Exists)
            {
                dir.Create();
            }
            DirectoryInfo dir2 = new DirectoryInfo(desktop + @"\출하\" + date + @"\실적업로드용\");
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!dir2.Exists)
            {
                dir2.Create();
            }

            string file_path = desktop + @"\출하\" + date + @"\실적업로드용\";
            file_path += dt_str + ".xls";
            //MessageBox.Show(file_path + " 경로로 저장이 됩니다.");

            try
            {
                gridControl2.ExportToXls(file_path);
                MessageBox.Show(file_path + " 경로로 저장이 됩니다.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRER : " + ex.Message);
            }
            /*ProcessStartInfo info = new ProcessStartInfo("Excel.exe", file_path);
            try
            {84721C1000
                Process.Start(info);
            }
            catch (Exception ex)
            {
                MessageBox.Show("저장 되었지만 실행 할 수 없습니다.(" + ex.Message + ")");
            }*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("IT_SCODE",typeof(string));
            dt.Columns.Add("SQTY",typeof(string));
            DataRow[] check_row = list_dt.Select("CHECK_YN ='N'");
            if (check_row.Length == 0)
            {
                MessageBox.Show("미입력된 데이터가 없습니다.");
                return;
            }
            foreach (DataRow dr in list_dt.Rows)
            {
                if(dr["CHECK_YN"].ToString().Equals("N"))
                {
                    DataRow[] drr = dt.Select("IT_SCODE = '" + dr["IT_SCODE"] + "'");
                    if (drr.Length == 0)
                    {
                        int sh_sqty = 0;
                        DataRow[] datarow = list_dt.Select("IT_SCODE='" + dr["IT_SCODE"] + "' AND CHECK_YN ='N'");
                        for (int i = 0; i < datarow.Length; i++)
                        {
                            sh_sqty = sh_sqty + int.Parse(datarow[i]["SH_SQTY"].ToString());
                        }
                        if (sh_sqty > 0)
                        {
                            DataRow insertRowDET = dt.NewRow();

                            insertRowDET["IT_SCODE"] = dr["IT_SCODE"];
                            insertRowDET["SQTY"] = sh_sqty;
                            dt.Rows.Add(insertRowDET);
                        }
                    }
                }
            }
            gridControl2.DataSource = dt;
            excel_export_업로드용();
            
        }
    }
}
