﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Dt_Input_time : Form
    {
        public string dt_name { get; set; }
        public string dt_code { get; set; }
        public string dt_stime { get; set; }
        public string dt_etime { get; set; }
        
        public Dt_Input_time()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            dt_stime = dteDt_stime.DateTime.ToString("yyyy-MM-dd") + " " + te_time_s.Time.ToString("HH:mm");
            dt_etime = dteDt_etime.DateTime.ToString("yyyy-MM-dd") + " " + te_time_e.Time.ToString("HH:mm");

            DialogResult = DialogResult.OK;
            
        }

        private void Dt_Input_time_Load(object sender, EventArgs e)
        {
            lblDt_name.Text = dt_name;
            DateTime dt = DateTime.Now;
            dteDt_stime.EditValue = dt;
            dteDt_etime.EditValue = dt;
            te_time_s.EditValue = dt;
            te_time_e.EditValue = dt;
        }
    }
}
