﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class Po_release_search_paint_dev : DevExpress.XtraEditors.XtraForm
    {
        public string IT_SCODE_str { get; set; }
        public string IT_SNAME_str { get; set; }
        public string IT_MODEL_str = "";
        public string MO_SQUTY_str { get; set; }
        public string MO_SNUMB_str { get; set; }
        public string WC_CODE_str { get; set; }
        public string SITE_CODE_str { get; set; }
        public string ALC_CODE_str { get; set; }
        public string ME_SCODE_str { get; set; }
        public string IT_PKQTY_str { get; set; }
        public string MATCH_GUBN_str { get; set; }
        public string D_WEIGHT_str { get; set; }
        public string A_WEIGHT_str { get; set; }

        public string wc_group { get; set; }
        public string wc_code { get; set; }
        public string MAX_SQTY_str { get; set; }
        public string CARRIER_YN_str { get; set; }
        public string product_gubn { get; set; }

        DataTable po_release_DT = new DataTable();
        DataTable it_model_DT = new DataTable();
        public Po_release_search_paint_dev()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            po_release_DT.Columns.Add("NUM", typeof(string));
            po_release_DT.Columns.Add("MO_SNUMB", typeof(string));
            po_release_DT.Columns.Add("SDATE", typeof(string));
            po_release_DT.Columns.Add("IT_SCODE", typeof(string));
            po_release_DT.Columns.Add("IT_SNAME", typeof(string));
            po_release_DT.Columns.Add("IT_MODEL", typeof(string));
            po_release_DT.Columns.Add("MO_SQUTY", typeof(string));
            po_release_DT.Columns.Add("IT_SAFTY", typeof(string));
            po_release_DT.Columns.Add("ALC_CODE", typeof(string));
            po_release_DT.Columns.Add("ME_SCODE", typeof(string));
            po_release_DT.Columns.Add("IT_PKQTY", typeof(string));
            po_release_DT.Columns.Add("D_WEIGHT", typeof(string));
            po_release_DT.Columns.Add("MATCH_GUBN", typeof(string));
            po_release_DT.Columns.Add("A_WEIGHT", typeof(string));
            po_release_DT.Columns.Add("CARRIER_YN", typeof(string));
            po_release_DT.Columns.Add("MAX_SQTY", typeof(string));
            po_release_DT.Columns.Add("REMAIN_SQUTY", typeof(string));
            po_release_DT.Columns.Add("WC_SCODE", typeof(string));

            it_model_DT.Columns.Add("IT_MODEL", typeof(string));
            DateTime dt = DateTime.Now;
            if(dt.Hour>=0 && dt.Hour<8)
            {
                days = days - 1;
            }
            date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
            lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");

            
            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            worker.RunWorkerAsync();
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;
                it_model_DT = getdata_IT_MODEL();
                po_release_DT = getdata();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = po_release_DT;
                gridControl2.DataSource = it_model_DT;
                if (po_release_DT.Rows.Count > 0)
                {
                    string sdate = gridView1.GetRowCellValue(0, "SDATE").ToString();
                    lbl_sdate.Text = sdate.Substring(0, 4) + "-" + sdate.Substring(4, 2) + "-" + sdate.Substring(6, 2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        private void btn_date_Click(object sender, EventArgs e)
        {
            try
            {
                po_release_DT.Rows.Clear();
                IT_MODEL_str = "";

                if (sender == btn_prev_date)
                {
                    days = days - 1;
                }
                else if (sender == btn_next_date)
                {
                    days = days + 1;
                }
                date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
            
        }
        private DataTable getdata_IT_MODEL()
        {
            DataTable dt = new DataTable();
            //splashScreenManager1.ShowWaitForm();
            string sql = "select DISTINCT C.PROJECT AS IT_MODEL from PO_RELEASE_NEW A "
                        + " LEFT JOIN IT_MASTER B ON A.IT_SCODE=B.IT_SCODE"
                        + " LEFT JOIN CAR_SPEC C ON B.PRJ_CODE = C.CAR_CODE"
                        + " WHERE A.END_CHECK='N' AND A.WC_SCODE='" + wc_code + "' AND SDATE='" + date_reg + "'";
                        //+" ORDER BY SUBSTRING(B.IT_MODEL,1,3)";
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "work_plan_model");

                dt = ds.Tables["work_plan_model"];
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();
                //splashScreenManager1.CloseWaitForm();
            }
            return dt;
        }
        private DataTable getdata()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_PO_RELEASE_NEW_SEARCH_2", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                //da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
                da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
                da.SelectCommand.Parameters.AddWithValue("@IT_MODEL", IT_MODEL_str);
                da.SelectCommand.Parameters.AddWithValue("@SDATE", date_reg);
                da.SelectCommand.Parameters.AddWithValue("DAY_NIGHT", btn_day_night.Text.Trim());
                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "PO_RELEASE_NEW");

                dt = ds.Tables["PO_RELEASE_NEW"];

                //gridControl1.DataSource = dt;
                //gridControl1.Refresh();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /*
        private void ColorChange()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //MessageBox.Show(row.Cells["합계"].Value.ToString());
                if (2 == int.Parse(row.Cells["GUBN"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;

                }
            }
        }
        */
        private void btn_done_Click(object sender, EventArgs e)
        {
            //int cxtl = dataGridView1.CurrentCell.RowIndex;
            if (gridView1.SelectedRowsCount < 0)
            {
                MessageBox.Show("작업지시를 선택해 주세요");
                return;
            }
            try
            {
                
                IT_SCODE_str = gridView1.GetFocusedRowCellValue("IT_SCODE").ToString().Trim();
                IT_SNAME_str = gridView1.GetFocusedRowCellValue("IT_SNAME").ToString().Trim();
                IT_MODEL_str = gridView1.GetFocusedRowCellValue("IT_MODEL").ToString().Trim();
                MO_SQUTY_str = gridView1.GetFocusedRowCellValue("MO_SQUTY").ToString().Trim();
                MO_SNUMB_str = gridView1.GetFocusedRowCellValue("MO_SNUMB").ToString().Trim();
                SITE_CODE_str = "D001";
                ALC_CODE_str = gridView1.GetFocusedRowCellValue("ALC_CODE").ToString().Trim();
                ME_SCODE_str = gridView1.GetFocusedRowCellValue("ME_SCODE").ToString().Trim();
                IT_PKQTY_str = gridView1.GetFocusedRowCellValue("IT_PKQTY").ToString().Trim();
                D_WEIGHT_str = gridView1.GetFocusedRowCellValue("D_WEIGHT").ToString().Trim();
                A_WEIGHT_str = gridView1.GetFocusedRowCellValue("A_WEIGHT").ToString().Trim();
                MATCH_GUBN_str = gridView1.GetFocusedRowCellValue("MATCH_GUBN").ToString().Trim();
                CARRIER_YN_str = gridView1.GetFocusedRowCellValue("CARRIER_YN").ToString().Trim();
                MAX_SQTY_str = gridView1.GetFocusedRowCellValue("MAX_SQTY").ToString().Trim();
                DialogResult = DialogResult.OK;
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR : "+ex.Message);
            }
        }


        private void gridView2_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                IT_MODEL_str = gridView2.GetFocusedRowCellValue("IT_MODEL").ToString().Trim();
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_day_night_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_day_night.Text.Equals("주간"))
                {
                    btn_day_night.Text = "야간";
                }
                else
                {
                    btn_day_night.Text = "주간";
                }
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }

        }

    }
}
