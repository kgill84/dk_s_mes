﻿namespace DK_Tablet.Popup
{
    partial class Scale_type_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_scale_typeA = new System.Windows.Forms.Button();
            this.btn_scale_typeB = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_scale_typeA
            // 
            this.btn_scale_typeA.Font = new System.Drawing.Font("맑은 고딕", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_scale_typeA.Location = new System.Drawing.Point(30, 60);
            this.btn_scale_typeA.Name = "btn_scale_typeA";
            this.btn_scale_typeA.Size = new System.Drawing.Size(206, 126);
            this.btn_scale_typeA.TabIndex = 0;
            this.btn_scale_typeA.Text = "A Type\r\n(1~5호기)";
            this.btn_scale_typeA.UseVisualStyleBackColor = true;
            this.btn_scale_typeA.Click += new System.EventHandler(this.btn_scale_typeA_Click);
            // 
            // btn_scale_typeB
            // 
            this.btn_scale_typeB.Font = new System.Drawing.Font("맑은 고딕", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_scale_typeB.Location = new System.Drawing.Point(242, 60);
            this.btn_scale_typeB.Name = "btn_scale_typeB";
            this.btn_scale_typeB.Size = new System.Drawing.Size(206, 126);
            this.btn_scale_typeB.TabIndex = 0;
            this.btn_scale_typeB.Text = "B Type\r\n(6호기)";
            this.btn_scale_typeB.UseVisualStyleBackColor = true;
            this.btn_scale_typeB.Click += new System.EventHandler(this.btn_scale_typeB_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(93, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(303, 40);
            this.label1.TabIndex = 4;
            this.label1.Text = "저울 Type 선택";
            // 
            // Scale_type_Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(488, 207);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_scale_typeB);
            this.Controls.Add(this.btn_scale_typeA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Scale_type_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Scale_type_Popup";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_scale_typeA;
        private System.Windows.Forms.Button btn_scale_typeB;
        private System.Windows.Forms.Label label1;
    }
}