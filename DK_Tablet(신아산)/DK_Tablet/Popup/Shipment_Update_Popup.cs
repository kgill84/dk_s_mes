﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;

namespace DK_Tablet
{
    public partial class Shipment_Update_Popup : DevExpress.XtraEditors.XtraForm
    {
        
        DataTable Shipment_table = new DataTable();
        GET_DATA GET_DATA = new GET_DATA();
        public Shipment_Update_Popup()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            Shipment_table.Columns.Add("SH_SNUMB", typeof(string));
            Shipment_table.Columns.Add("SH_SERNO", typeof(int));
            Shipment_table.Columns.Add("SH_ODATE", typeof(string));
            Shipment_table.Columns.Add("SH_STIME", typeof(string));
            Shipment_table.Columns.Add("IT_SCODE", typeof(string));
            Shipment_table.Columns.Add("IT_SNAME", typeof(string));
            Shipment_table.Columns.Add("SH_SQTY", typeof(int));
            Shipment_table.Columns.Add("C_SQTY", typeof(int));
            Shipment_table.Columns.Add("LOC_CODE", typeof(string));
            DateTime dt = DateTime.Now;
            if(dt.Hour>=0 && dt.Hour<8)
            {
                days = days - 1;
            }
            date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
            lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");

            lueLoc_code.Properties.DataSource = GET_DATA.OUT_LoccodeSelect_DropDown();
            lueLoc_code.Properties.DisplayMember = "LOC_NAME";
            lueLoc_code.Properties.ValueMember = "LOC_CODE";
            lueLoc_code.ItemIndex = 0;


            

            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;
                Shipment_table = getdata();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = Shipment_table;

                if (Shipment_table.Rows.Count > 0)
                {
                    string sdate = gridView1.GetRowCellValue(0, "SH_ODATE").ToString();
                    lbl_sdate.Text = sdate.Substring(0, 4) + "-" + sdate.Substring(4, 2) + "-" + sdate.Substring(6, 2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        private void btn_date_Click(object sender, EventArgs e)
        {
            try
            {
                Shipment_table.Rows.Clear();
                

                if (sender == btn_prev_date)
                {
                    days = days - 1;
                }
                else if (sender == btn_next_date)
                {
                    days = days + 1;
                }
                date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                lueShipment_time.Properties.DataSource = GET_DATA.shipment_sh_stime_get(lueLoc_code.GetColumnValue("LOC_CODE").ToString(), date_reg);
                lueShipment_time.Properties.DisplayMember = "SH_STIME";
                lueShipment_time.Properties.ValueMember = "SH_STIME";
                lueShipment_time.ItemIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
            
        }
        
        private DataTable getdata()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("IF OBJECT_ID('TEMPDB..#TEMP_HISTORY')IS NOT NULL DROP TABLE #TEMP_HISTORY"
                                                +" SELECT SH_SNUMB,IT_SCODE,SUM(SH_SQTY)SH_SQTY, LOC_CODE"
                                                +" INTO #TEMP_HISTORY"
                                                +" FROM (SELECT SH_SNUMB,IT_SCODE,SH_SQTY = CASE WHEN GUBN='1' THEN SH_SQTY ELSE C_SQTY END, LOC_CODE"
                                                +" FROM SHIPMENT_CANCLE_HISTORY WHERE SH_SNUMB=@SH_SNUMB)A"
                                                +" GROUP BY SH_SNUMB,IT_SCODE,LOC_CODE"
                                                +" SELECT A.SH_SNUMB,B.SH_SERNO,A.SH_ODATE,A.SH_STIME,B.IT_SCODE,C.IT_SNAME,B.SH_SQTY+ISNULL(D.SH_SQTY,0)AS SH_SQTY,CAST('0' AS INT)AS C_SQTY,B.LOC_CODE FROM SHIPMENT_HD A"
                                                +" LEFT JOIN SHIPMENT_DET B ON A.SH_SNUMB = B.SH_SNUMB"
                                                +" LEFT JOIN IT_MASTER C ON B.IT_SCODE = C.IT_SCODE"
                                                +" LEFT JOIN #TEMP_HISTORY D ON B.SH_SNUMB = D.SH_SNUMB AND B.IT_SCODE = D.IT_SCODE AND B.LOC_CODE = D.LOC_CODE"
                                                +" WHERE A.SH_ODATE = @SH_ODATE"
                                                +" AND B.LOC_CODE=@LOC_CODE AND A.SH_STIME=@SH_STIME"
                                                + " AND A.SH_SNUMB=@SH_SNUMB AND B.SH_SQTY+ISNULL(D.SH_SQTY,0)>0", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.Text;


                da.SelectCommand.Parameters.AddWithValue("@SH_ODATE", date_reg);
                da.SelectCommand.Parameters.AddWithValue("@LOC_CODE", lueLoc_code.GetColumnValue("LOC_CODE").ToString());
                da.SelectCommand.Parameters.AddWithValue("@SH_STIME", lueShipment_time.GetColumnValue("SH_STIME").ToString());
                da.SelectCommand.Parameters.AddWithValue("@SH_SNUMB", lueShipment_time.GetColumnValue("SH_SNUMB").ToString());
                
                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "SHIPMENT_DATA");

                dt = ds.Tables["SHIPMENT_DATA"];

                //gridControl1.DataSource = dt;
                //gridControl1.Refresh();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /*
        private void ColorChange()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //MessageBox.Show(row.Cells["합계"].Value.ToString());
                if (2 == int.Parse(row.Cells["GUBN"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;

                }
            }
        }
        */
        private void btn_done_Click(object sender, EventArgs e)
        {            
            try
            {
                if (Shipment_table.Rows.Count <= 0)
                {
                    MessageBox.Show("취소할 수량이 없습니다.");
                    return;
                }
                bool cancle_sqty_check = false;
                //입력된 취소 수량이 있는지 체크
                foreach (DataRow dr in Shipment_table.Rows)
                {
                    if (int.Parse(dr["C_SQTY"].ToString()) > 0)
                    {
                        cancle_sqty_check = true;
                        break;
                    }
                }
                if (cancle_sqty_check)
                {
                    if (DialogResult.OK == MessageBox.Show("부분 취소 하시겠습니까?", "부분 취소 여부", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                    {
                        shipment_cancle_db("2");
                    }
                }
                else
                {
                    MessageBox.Show("취소할 수량을 입력 하지 않았습니다.");
                }
                
                    
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR : "+ex.Message);
            }
        }

        private void lueLoc_code_EditValueChanged(object sender, EventArgs e)
        {
            //Shipment_table.Rows.Clear();
            
            //worker.RunWorkerAsync();
            lueShipment_time.Properties.DataSource = GET_DATA.shipment_sh_stime_get(lueLoc_code.GetColumnValue("LOC_CODE").ToString(),date_reg);
            lueShipment_time.Properties.DisplayMember = "SH_STIME";
            lueShipment_time.Properties.ValueMember = "SH_STIME";
            lueShipment_time.ItemIndex = 0;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.Name.Equals("C_SQTY"))
            {
                KeyPad KeyPad = new KeyPad();
                if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (int.Parse(gridView1.GetFocusedRowCellValue(SH_SQTY).ToString()) < int.Parse(KeyPad.txt_value))
                    {
                        MessageBox.Show("출하수량보다 작거나 같아야 됩니다.");
                        return;
                    }
                    gridView1.SetFocusedRowCellValue(C_SQTY, KeyPad.txt_value);
                }
            }
        }

        private void btn_All_Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Shipment_table.Rows.Count <= 0)
                {
                    MessageBox.Show("취소할 수량이 없습니다.");
                    return;
                }
                if (DialogResult.OK == MessageBox.Show("전체 취소 하시겠습니까?", "전체 취소 여부", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                {
                    shipment_cancle_db("1");
                }
                
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR : " + ex.Message);
            }
        }

        public void shipment_cancle_db(string gubn)
        {
            string conStr;

            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();
            //string mm_snumb = SaveMaxNumber();


            //취소
            SqlCommand mov_cmd = new SqlCommand("USP_SHIPMENT_CANCLE_CS", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;
            mov_cmd.CommandTimeout = 0;


            mov_cmd.Parameters.AddWithValue("@TVP", Shipment_table);//
            mov_cmd.Parameters.AddWithValue("@GUBN", gubn);//

            //mov_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//
            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();

                trans.Commit();
                MessageBox.Show("취소가 완료 되었습니다.");
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void lueShipment_time_EditValueChanged(object sender, EventArgs e)
        {
            worker.RunWorkerAsync();
        }

    }
}
