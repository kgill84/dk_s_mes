﻿namespace DK_Tablet.Popup
{
    partial class Sqty_popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_up = new System.Windows.Forms.Button();
            this.btn_down = new System.Windows.Forms.Button();
            this.btn_done = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_sqty = new DevExpress.XtraEditors.TextEdit();
            this.btn_up_5 = new System.Windows.Forms.Button();
            this.btn_down_5 = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.txt_sqty.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_up
            // 
            this.btn_up.Font = new System.Drawing.Font("굴림", 20F);
            this.btn_up.Location = new System.Drawing.Point(223, 103);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(75, 53);
            this.btn_up.TabIndex = 1;
            this.btn_up.Text = "▲";
            this.btn_up.UseVisualStyleBackColor = true;
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_down
            // 
            this.btn_down.Font = new System.Drawing.Font("굴림", 20F);
            this.btn_down.Location = new System.Drawing.Point(223, 155);
            this.btn_down.Name = "btn_down";
            this.btn_down.Size = new System.Drawing.Size(75, 53);
            this.btn_down.TabIndex = 1;
            this.btn_down.Text = "▼";
            this.btn_down.UseVisualStyleBackColor = true;
            this.btn_down.Click += new System.EventHandler(this.btn_down_Click);
            // 
            // btn_done
            // 
            this.btn_done.Font = new System.Drawing.Font("굴림", 30F);
            this.btn_done.Location = new System.Drawing.Point(12, 228);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(166, 75);
            this.btn_done.TabIndex = 2;
            this.btn_done.Text = "입  력";
            this.btn_done.UseVisualStyleBackColor = true;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(104, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 40);
            this.label1.TabIndex = 3;
            this.label1.Text = "수량입력";
            // 
            // txt_sqty
            // 
            this.txt_sqty.EditValue = "0";
            this.txt_sqty.Location = new System.Drawing.Point(12, 103);
            this.txt_sqty.Name = "txt_sqty";
            this.txt_sqty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 60F);
            this.txt_sqty.Properties.Appearance.Options.UseFont = true;
            this.txt_sqty.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_sqty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_sqty.Size = new System.Drawing.Size(211, 104);
            this.txt_sqty.TabIndex = 4;
            // 
            // btn_up_5
            // 
            this.btn_up_5.Font = new System.Drawing.Font("굴림", 20F);
            this.btn_up_5.Location = new System.Drawing.Point(298, 103);
            this.btn_up_5.Name = "btn_up_5";
            this.btn_up_5.Size = new System.Drawing.Size(75, 53);
            this.btn_up_5.TabIndex = 1;
            this.btn_up_5.Text = "+5";
            this.btn_up_5.UseVisualStyleBackColor = true;
            this.btn_up_5.Click += new System.EventHandler(this.btn_up_5_Click);
            // 
            // btn_down_5
            // 
            this.btn_down_5.Font = new System.Drawing.Font("굴림", 20F);
            this.btn_down_5.Location = new System.Drawing.Point(298, 155);
            this.btn_down_5.Name = "btn_down_5";
            this.btn_down_5.Size = new System.Drawing.Size(75, 53);
            this.btn_down_5.TabIndex = 1;
            this.btn_down_5.Text = "-5";
            this.btn_down_5.UseVisualStyleBackColor = true;
            this.btn_down_5.Click += new System.EventHandler(this.btn_down_5_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Font = new System.Drawing.Font("굴림", 30F);
            this.btn_cancel.Location = new System.Drawing.Point(206, 228);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(166, 75);
            this.btn_cancel.TabIndex = 2;
            this.btn_cancel.Text = "취  소";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Sqty_popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(383, 315);
            this.Controls.Add(this.txt_sqty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.btn_down_5);
            this.Controls.Add(this.btn_down);
            this.Controls.Add(this.btn_up_5);
            this.Controls.Add(this.btn_up);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Sqty_popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sqty_popup";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Sqty_popup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txt_sqty.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_up;
        private System.Windows.Forms.Button btn_down;
        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txt_sqty;
        private System.Windows.Forms.Button btn_up_5;
        private System.Windows.Forms.Button btn_down_5;
        private System.Windows.Forms.Button btn_cancel;

    }
}