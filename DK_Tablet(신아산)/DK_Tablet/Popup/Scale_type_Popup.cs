﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Scale_type_Popup : Form
    {
        public string scale_type { get; set; }
        public Scale_type_Popup()
        {
            InitializeComponent();
        }

        private void btn_scale_typeA_Click(object sender, EventArgs e)
        {
            scale_type = "A";
            DialogResult = DialogResult.OK;
        }

        private void btn_scale_typeB_Click(object sender, EventArgs e)
        {
            scale_type = "B";
            DialogResult = DialogResult.OK;
        }
    }
}
