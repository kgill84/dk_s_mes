﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Injection_Sqty_Popup : Form
    {
        public string sqty { get; set; }
        public string base_sqty { get; set; }
        int max_sqty = 0;
        public Injection_Sqty_Popup()
        {
            InitializeComponent();
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn == btn_up)
            {
                if ((int.Parse(txt_sqty.EditValue.ToString()) + 1) > max_sqty)
                {
                    txt_sqty.EditValue = max_sqty.ToString();
                }
                else
                {
                    txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) + 1).ToString();
                }
            }
            else if (btn == btn_down)
            {
                if ((int.Parse(txt_sqty.EditValue.ToString()) - 1) < 0)
                {
                    txt_sqty.EditValue = "0";
                }
                else
                {
                    txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) - 1).ToString();
                }
            }
            else if (btn == btn_up_5)
            {
                if ((int.Parse(txt_sqty.EditValue.ToString()) + 5) > max_sqty)
                {
                    txt_sqty.EditValue = max_sqty.ToString();
                }
                else 
                {
                    txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) + 5).ToString();
                }
            }
            else if (btn == btn_down_5)
            {
                if ((int.Parse(txt_sqty.EditValue.ToString()) - 5) < 0)
                {
                    txt_sqty.EditValue = "0";
                }
                else
                {
                    txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) - 5).ToString();
                }
            }            
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            if (txt_sqty.EditValue.ToString() == "0")
            {
                MessageBox.Show("실적수량이 0 보다 커야 합니다.");
                return;
            }
            sqty = txt_sqty.EditValue.ToString();
            DialogResult = DialogResult.OK;
        }

        private void Sqty_popup_Load(object sender, EventArgs e)
        {
            txt_sqty.EditValue = 1;
            max_sqty = int.Parse(base_sqty.Trim()) - int.Parse(sqty);
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
