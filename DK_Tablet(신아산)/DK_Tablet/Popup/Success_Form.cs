﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Success_Form : Form
    {
        
        public Success_Form()
        {
            InitializeComponent();
        }


        public void set_text(string carrier_no)
        {
            this.WindowState = FormWindowState.Maximized;
            label1.Text = carrier_no + " 카드 이동완료";
        }
    }
}
