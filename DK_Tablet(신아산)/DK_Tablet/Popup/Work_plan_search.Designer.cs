﻿namespace DK_Tablet
{
    partial class Work_plan_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.SITE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARRIER_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAX_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WP_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_MODEL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WP_SQUTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WC_GROUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WKAREA_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SAFTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ALC_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ME_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_PKQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_WEIGHT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MATCH_GUBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.A_WEIGHT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMAIN_SQUTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RESULT_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WC_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WORK_AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 60;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SITE_CODE,
            this.CARRIER_YN,
            this.MAX_SQTY,
            this.NUM,
            this.WP_SNUMB,
            this.IT_SCODE,
            this.IT_SNAME,
            this.IT_MODEL,
            this.WP_SQUTY,
            this.SDATE,
            this.WC_GROUP,
            this.WKAREA_NAME,
            this.IT_SAFTY,
            this.ALC_CODE,
            this.ME_SCODE,
            this.IT_PKQTY,
            this.D_WEIGHT,
            this.MATCH_GUBN,
            this.A_WEIGHT,
            this.REMAIN_SQUTY,
            this.START_SQTY,
            this.RESULT_SQTY,
            this.WC_SCODE,
            this.WORK_AREA});
            this.dataGridView1.Location = new System.Drawing.Point(12, 65);
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 60;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.dataGridView1.RowTemplate.Height = 60;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1099, 422);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(791, 493);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 60);
            this.btn_done.TabIndex = 1;
            this.btn_done.Text = "선택";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(959, 493);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 60);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(13, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1098, 5);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(251, 35);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "생산계획현황";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.DataPropertyName = "SITE_CODE";
            this.SITE_CODE.HeaderText = "공장코드";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SITE_CODE.Visible = false;
            this.SITE_CODE.Width = 104;
            // 
            // CARRIER_YN
            // 
            this.CARRIER_YN.DataPropertyName = "CARRIER_YN";
            this.CARRIER_YN.HeaderText = "CARRIER_YN";
            this.CARRIER_YN.Name = "CARRIER_YN";
            this.CARRIER_YN.Visible = false;
            // 
            // MAX_SQTY
            // 
            this.MAX_SQTY.DataPropertyName = "MAX_SQTY";
            this.MAX_SQTY.HeaderText = "최대적재수량";
            this.MAX_SQTY.Name = "MAX_SQTY";
            this.MAX_SQTY.Visible = false;
            // 
            // NUM
            // 
            this.NUM.DataPropertyName = "NUM";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.NUM.DefaultCellStyle = dataGridViewCellStyle2;
            this.NUM.HeaderText = "우선순위";
            this.NUM.Name = "NUM";
            this.NUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NUM.Width = 110;
            // 
            // WP_SNUMB
            // 
            this.WP_SNUMB.DataPropertyName = "WP_SNUMB";
            this.WP_SNUMB.HeaderText = "작업지시번호";
            this.WP_SNUMB.Name = "WP_SNUMB";
            this.WP_SNUMB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WP_SNUMB.Visible = false;
            this.WP_SNUMB.Width = 148;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.DataPropertyName = "IT_SCODE";
            this.IT_SCODE.HeaderText = "품번";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SCODE.Width = 300;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.DataPropertyName = "IT_SNAME";
            this.IT_SNAME.HeaderText = "품명";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SNAME.Width = 300;
            // 
            // IT_MODEL
            // 
            this.IT_MODEL.DataPropertyName = "IT_MODEL";
            this.IT_MODEL.HeaderText = "모델";
            this.IT_MODEL.Name = "IT_MODEL";
            this.IT_MODEL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_MODEL.Visible = false;
            this.IT_MODEL.Width = 60;
            // 
            // WP_SQUTY
            // 
            this.WP_SQUTY.DataPropertyName = "WP_SQUTY";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.WP_SQUTY.DefaultCellStyle = dataGridViewCellStyle3;
            this.WP_SQUTY.HeaderText = "주간지시량";
            this.WP_SQUTY.Name = "WP_SQUTY";
            this.WP_SQUTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WP_SQUTY.Width = 126;
            // 
            // SDATE
            // 
            this.SDATE.DataPropertyName = "SDATE";
            this.SDATE.HeaderText = "시작일";
            this.SDATE.Name = "SDATE";
            this.SDATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SDATE.Visible = false;
            this.SDATE.Width = 82;
            // 
            // WC_GROUP
            // 
            this.WC_GROUP.DataPropertyName = "WC_GROUP";
            this.WC_GROUP.HeaderText = "작업그룹";
            this.WC_GROUP.Name = "WC_GROUP";
            this.WC_GROUP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WC_GROUP.Visible = false;
            this.WC_GROUP.Width = 126;
            // 
            // WKAREA_NAME
            // 
            this.WKAREA_NAME.DataPropertyName = "WKAREA_NAME";
            this.WKAREA_NAME.HeaderText = "작업그룹명";
            this.WKAREA_NAME.Name = "WKAREA_NAME";
            this.WKAREA_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WKAREA_NAME.Visible = false;
            this.WKAREA_NAME.Width = 104;
            // 
            // IT_SAFTY
            // 
            this.IT_SAFTY.DataPropertyName = "IT_SAFTY";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.IT_SAFTY.DefaultCellStyle = dataGridViewCellStyle4;
            this.IT_SAFTY.HeaderText = "안전재고";
            this.IT_SAFTY.Name = "IT_SAFTY";
            this.IT_SAFTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SAFTY.Width = 104;
            // 
            // ALC_CODE
            // 
            this.ALC_CODE.DataPropertyName = "ALC_CODE";
            this.ALC_CODE.HeaderText = "ALC_CODE";
            this.ALC_CODE.Name = "ALC_CODE";
            this.ALC_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ALC_CODE.Visible = false;
            this.ALC_CODE.Width = 124;
            // 
            // ME_SCODE
            // 
            this.ME_SCODE.DataPropertyName = "ME_SCODE";
            this.ME_SCODE.HeaderText = "자재유형코드";
            this.ME_SCODE.Name = "ME_SCODE";
            this.ME_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ME_SCODE.Visible = false;
            this.ME_SCODE.Width = 148;
            // 
            // IT_PKQTY
            // 
            this.IT_PKQTY.DataPropertyName = "IT_PKQTY";
            this.IT_PKQTY.HeaderText = "포장수량";
            this.IT_PKQTY.Name = "IT_PKQTY";
            this.IT_PKQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_PKQTY.Visible = false;
            this.IT_PKQTY.Width = 104;
            // 
            // D_WEIGHT
            // 
            this.D_WEIGHT.DataPropertyName = "D_WEIGHT";
            this.D_WEIGHT.HeaderText = "표준편차";
            this.D_WEIGHT.Name = "D_WEIGHT";
            this.D_WEIGHT.Visible = false;
            // 
            // MATCH_GUBN
            // 
            this.MATCH_GUBN.DataPropertyName = "MATCH_GUBN";
            this.MATCH_GUBN.HeaderText = "매칭구분";
            this.MATCH_GUBN.Name = "MATCH_GUBN";
            this.MATCH_GUBN.Visible = false;
            // 
            // A_WEIGHT
            // 
            this.A_WEIGHT.DataPropertyName = "A_WEIGHT";
            this.A_WEIGHT.HeaderText = "정미중량";
            this.A_WEIGHT.Name = "A_WEIGHT";
            this.A_WEIGHT.Visible = false;
            // 
            // REMAIN_SQUTY
            // 
            this.REMAIN_SQUTY.DataPropertyName = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.HeaderText = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.Name = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.Visible = false;
            // 
            // START_SQTY
            // 
            this.START_SQTY.DataPropertyName = "START_SQTY";
            this.START_SQTY.HeaderText = "START_SQTY";
            this.START_SQTY.Name = "START_SQTY";
            this.START_SQTY.Visible = false;
            // 
            // RESULT_SQTY
            // 
            this.RESULT_SQTY.DataPropertyName = "RESULT_SQTY";
            this.RESULT_SQTY.HeaderText = "RESULT_SQTY";
            this.RESULT_SQTY.Name = "RESULT_SQTY";
            this.RESULT_SQTY.Visible = false;
            // 
            // WC_SCODE
            // 
            this.WC_SCODE.DataPropertyName = "WC_SCODE";
            this.WC_SCODE.HeaderText = "WC_SCODE";
            this.WC_SCODE.Name = "WC_SCODE";
            this.WC_SCODE.Visible = false;
            // 
            // WORK_AREA
            // 
            this.WORK_AREA.DataPropertyName = "WORK_AREA";
            this.WORK_AREA.HeaderText = "WORK_AREA";
            this.WORK_AREA.Name = "WORK_AREA";
            this.WORK_AREA.Visible = false;
            // 
            // Work_plan_search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1123, 565);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Work_plan_search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "생산계획현황";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SITE_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARRIER_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAX_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn WP_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_MODEL;
        private System.Windows.Forms.DataGridViewTextBoxColumn WP_SQUTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn SDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn WC_GROUP;
        private System.Windows.Forms.DataGridViewTextBoxColumn WKAREA_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SAFTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ALC_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ME_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_PKQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_WEIGHT;
        private System.Windows.Forms.DataGridViewTextBoxColumn MATCH_GUBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn A_WEIGHT;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMAIN_SQUTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn START_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn RESULT_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn WC_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn WORK_AREA;
    }
}