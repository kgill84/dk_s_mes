﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class FME_KeyPad : Form
    {
        public string txt_value { get; set; }
        public int int_value { get; set; }

        public FME_KeyPad()
        {
            InitializeComponent();
        }

        private void FME_KeyPad_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_value))
                txtDigit.Text = txt_value;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Tag == null) return;

            string pattern = @"^\d+\.(\d+)?$";
            string btn_tag = btn.Tag.ToString();
            string txt = txtDigit.Text;

            switch (btn_tag)
            {
                case ".0":
                    if (Regex.IsMatch(txt, pattern)) return;
                    txtDigit.Text += ".";
                    break;

                case "back":
                    if (txtDigit.Text.Equals("0")) return;
                    txtDigit.Text = (txt.Length <= 1) ? "0" : txt.Substring(0, txt.Length - 1);
                    break;

                case "ca":
                    txtDigit.Text = "0";
                    break;

                default:
                    txtDigit.Text = (txt.Equals("0") ? "" : txt) + btn_tag;
                    break;
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            string pattern = @"^\d+(\.\d+)?$";
            string txt = txtDigit.Text;

            if (!Regex.IsMatch(txt, pattern))
                txt = txt.Substring(0, txt.Length - 1);

            txt_value = txt;
            int_value = int.Parse(Math.Floor(double.Parse(txt)).ToString());

            DialogResult = DialogResult.OK;
        }
    }
}
