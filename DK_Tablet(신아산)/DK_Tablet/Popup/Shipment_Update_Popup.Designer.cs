﻿namespace DK_Tablet
{
    partial class Shipment_Update_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Section_Cancel = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SH_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SERNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_ODATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_STIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.C_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl_sdate = new DevExpress.XtraEditors.LabelControl();
            this.btn_next_date = new DevExpress.XtraEditors.SimpleButton();
            this.btn_prev_date = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.btn_All_Cancel = new System.Windows.Forms.Button();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueShipment_time = new DevExpress.XtraEditors.LookUpEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueShipment_time.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Section_Cancel
            // 
            this.btn_Section_Cancel.BackColor = System.Drawing.Color.MistyRose;
            this.btn_Section_Cancel.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_Section_Cancel.Location = new System.Drawing.Point(851, 1);
            this.btn_Section_Cancel.Name = "btn_Section_Cancel";
            this.btn_Section_Cancel.Size = new System.Drawing.Size(162, 70);
            this.btn_Section_Cancel.TabIndex = 1;
            this.btn_Section_Cancel.Text = "부분취소";
            this.btn_Section_Cancel.UseVisualStyleBackColor = false;
            this.btn_Section_Cancel.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1019, 1);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 70);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(4, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1167, 6);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1174, 433);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SH_SNUMB,
            this.SH_SERNO,
            this.SH_ODATE,
            this.SH_STIME,
            this.IT_SCODE,
            this.IT_SNAME,
            this.SH_SQTY,
            this.C_SQTY,
            this.LOC_CODE});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // SH_SNUMB
            // 
            this.SH_SNUMB.Caption = "등록번호";
            this.SH_SNUMB.FieldName = "SH_SNUMB";
            this.SH_SNUMB.Name = "SH_SNUMB";
            // 
            // SH_SERNO
            // 
            this.SH_SERNO.Caption = "시리얼";
            this.SH_SERNO.FieldName = "SH_SERNO";
            this.SH_SERNO.Name = "SH_SERNO";
            // 
            // SH_ODATE
            // 
            this.SH_ODATE.Caption = "출하일";
            this.SH_ODATE.FieldName = "SH_ODATE";
            this.SH_ODATE.Name = "SH_ODATE";
            this.SH_ODATE.Visible = true;
            this.SH_ODATE.VisibleIndex = 0;
            this.SH_ODATE.Width = 185;
            // 
            // SH_STIME
            // 
            this.SH_STIME.Caption = "출하시간";
            this.SH_STIME.FieldName = "SH_STIME";
            this.SH_STIME.Name = "SH_STIME";
            this.SH_STIME.Width = 193;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품번";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 1;
            this.IT_SCODE.Width = 244;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 2;
            this.IT_SNAME.Width = 366;
            // 
            // SH_SQTY
            // 
            this.SH_SQTY.AppearanceCell.BackColor = System.Drawing.Color.Salmon;
            this.SH_SQTY.AppearanceCell.Options.UseBackColor = true;
            this.SH_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.SH_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SH_SQTY.Caption = "출하수량";
            this.SH_SQTY.DisplayFormat.FormatString = "n0";
            this.SH_SQTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SH_SQTY.FieldName = "SH_SQTY";
            this.SH_SQTY.Name = "SH_SQTY";
            this.SH_SQTY.Visible = true;
            this.SH_SQTY.VisibleIndex = 3;
            this.SH_SQTY.Width = 136;
            // 
            // C_SQTY
            // 
            this.C_SQTY.AppearanceCell.BackColor = System.Drawing.Color.MistyRose;
            this.C_SQTY.AppearanceCell.Options.UseBackColor = true;
            this.C_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.C_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.C_SQTY.Caption = "취소수량";
            this.C_SQTY.DisplayFormat.FormatString = "n0";
            this.C_SQTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.C_SQTY.FieldName = "C_SQTY";
            this.C_SQTY.Name = "C_SQTY";
            this.C_SQTY.Visible = true;
            this.C_SQTY.VisibleIndex = 4;
            this.C_SQTY.Width = 147;
            // 
            // LOC_CODE
            // 
            this.LOC_CODE.Caption = "로케이션 코드";
            this.LOC_CODE.FieldName = "LOC_CODE";
            this.LOC_CODE.Name = "LOC_CODE";
            // 
            // lbl_sdate
            // 
            this.lbl_sdate.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lbl_sdate.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbl_sdate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_sdate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_sdate.Location = new System.Drawing.Point(374, 4);
            this.lbl_sdate.Name = "lbl_sdate";
            this.lbl_sdate.Size = new System.Drawing.Size(310, 55);
            this.lbl_sdate.TabIndex = 54;
            this.lbl_sdate.Text = "2016-30-30";
            // 
            // btn_next_date
            // 
            this.btn_next_date.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_next_date.Appearance.Options.UseFont = true;
            this.btn_next_date.Location = new System.Drawing.Point(690, 4);
            this.btn_next_date.Name = "btn_next_date";
            this.btn_next_date.Size = new System.Drawing.Size(78, 57);
            this.btn_next_date.TabIndex = 74;
            this.btn_next_date.Text = "▶";
            this.btn_next_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // btn_prev_date
            // 
            this.btn_prev_date.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_prev_date.Appearance.Options.UseFont = true;
            this.btn_prev_date.Location = new System.Drawing.Point(290, 4);
            this.btn_prev_date.Name = "btn_prev_date";
            this.btn_prev_date.Size = new System.Drawing.Size(78, 57);
            this.btn_prev_date.TabIndex = 75;
            this.btn_prev_date.Text = "◀";
            this.btn_prev_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(876, 11);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(277, 44);
            this.progressBarControl1.TabIndex = 76;
            // 
            // btn_All_Cancel
            // 
            this.btn_All_Cancel.BackColor = System.Drawing.Color.Salmon;
            this.btn_All_Cancel.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_All_Cancel.Location = new System.Drawing.Point(3, 1);
            this.btn_All_Cancel.Name = "btn_All_Cancel";
            this.btn_All_Cancel.Size = new System.Drawing.Size(162, 70);
            this.btn_All_Cancel.TabIndex = 1;
            this.btn_All_Cancel.Text = "전체취소";
            this.btn_All_Cancel.UseVisualStyleBackColor = false;
            this.btn_All_Cancel.Click += new System.EventHandler(this.btn_All_Cancel_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl1.Location = new System.Drawing.Point(3, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(162, 57);
            this.labelControl1.TabIndex = 54;
            this.labelControl1.Text = "사외로케이션\r\n창고";
            // 
            // lueLoc_code
            // 
            this.lueLoc_code.Location = new System.Drawing.Point(171, 4);
            this.lueLoc_code.Name = "lueLoc_code";
            this.lueLoc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lueLoc_code.Properties.Appearance.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueLoc_code.Properties.AutoHeight = false;
            this.lueLoc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_CODE", "로케이션코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_NAME", "로케이션명")});
            this.lueLoc_code.Properties.DropDownRows = 5;
            this.lueLoc_code.Properties.NullText = "";
            this.lueLoc_code.Size = new System.Drawing.Size(449, 57);
            this.lueLoc_code.TabIndex = 77;
            this.lueLoc_code.EditValueChanged += new System.EventHandler(this.lueLoc_code_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl2.Location = new System.Drawing.Point(731, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(162, 57);
            this.labelControl2.TabIndex = 54;
            this.labelControl2.Text = "출하시간";
            // 
            // lueShipment_time
            // 
            this.lueShipment_time.Location = new System.Drawing.Point(899, 4);
            this.lueShipment_time.Name = "lueShipment_time";
            this.lueShipment_time.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lueShipment_time.Properties.Appearance.Options.UseFont = true;
            this.lueShipment_time.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueShipment_time.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueShipment_time.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueShipment_time.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueShipment_time.Properties.AutoHeight = false;
            this.lueShipment_time.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueShipment_time.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueShipment_time.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SH_STIME", "출하시간"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SH_SNUMB", "출하등록번호", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lueShipment_time.Properties.DropDownRows = 5;
            this.lueShipment_time.Properties.NullText = "";
            this.lueShipment_time.Size = new System.Drawing.Size(254, 57);
            this.lueShipment_time.TabIndex = 77;
            this.lueShipment_time.EditValueChanged += new System.EventHandler(this.lueShipment_time_EditValueChanged);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Appearance.BackColor = System.Drawing.Color.LightCyan;
            this.splitContainerControl1.Panel1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.splitContainerControl1.Panel1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.splitContainerControl1.Panel1.Appearance.Options.UseBackColor = true;
            this.splitContainerControl1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainerControl1.Panel1.Controls.Add(this.progressBarControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.lbl_sdate);
            this.splitContainerControl1.Panel1.Controls.Add(this.btn_next_date);
            this.splitContainerControl1.Panel1.Controls.Add(this.btn_prev_date);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1174, 659);
            this.splitContainerControl1.SplitterPosition = 73;
            this.splitContainerControl1.TabIndex = 78;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.IsSplitterFixed = true;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl2.Panel1.Controls.Add(this.lueShipment_time);
            this.splitContainerControl2.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl2.Panel1.Controls.Add(this.lueLoc_code);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1174, 581);
            this.splitContainerControl2.SplitterPosition = 64;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.IsSplitterFixed = true;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.btn_All_Cancel);
            this.splitContainerControl3.Panel2.Controls.Add(this.btn_Section_Cancel);
            this.splitContainerControl3.Panel2.Controls.Add(this.btn_close);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1174, 512);
            this.splitContainerControl3.SplitterPosition = 74;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // Shipment_Update_Popup
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 659);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "Shipment_Update_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "출하변경";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueShipment_time.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Section_Cancel;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl lbl_sdate;
        private DevExpress.XtraEditors.SimpleButton btn_next_date;
        private DevExpress.XtraEditors.SimpleButton btn_prev_date;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraGrid.Columns.GridColumn SH_ODATE;
        private DevExpress.XtraGrid.Columns.GridColumn SH_STIME;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn C_SQTY;
        private System.Windows.Forms.Button btn_All_Cancel;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SNUMB;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit lueLoc_code;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SERNO;
        private DevExpress.XtraGrid.Columns.GridColumn LOC_CODE;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueShipment_time;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
    }
}