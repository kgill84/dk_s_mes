﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class FME_KeyPad2 : Form
    {
        public string txt_value { get; set; }

        public FME_KeyPad2()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            txt_value = (sender as DevExpress.XtraEditors.SimpleButton).Text;
            DialogResult = DialogResult.OK;
        }
    }
}
