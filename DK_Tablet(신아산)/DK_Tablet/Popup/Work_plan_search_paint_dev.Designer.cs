﻿namespace DK_Tablet
{
    partial class Work_plan_search_paint_dev
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.IT_CAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SITE_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CARRIER_YN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAX_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WP_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_MODEL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WP_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WC_GROUP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WKAREA_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SAFTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ALC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ME_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_PKQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D_WEIGHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MATCH_GUBN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.A_WEIGHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.REMAIN_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.START_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RESULT_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WC_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WORK_AREA = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(842, 493);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 60);
            this.btn_done.TabIndex = 1;
            this.btn_done.Text = "선택";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1010, 493);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 60);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(13, 54);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1149, 5);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(251, 35);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "생산계획현황";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.ColumnHeadersHeight = 60;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IT_CAR});
            this.dataGridView2.Location = new System.Drawing.Point(13, 65);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 60;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.dataGridView2.RowTemplate.Height = 60;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(221, 422);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // IT_CAR
            // 
            this.IT_CAR.DataPropertyName = "IT_MODEL";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.IT_CAR.DefaultCellStyle = dataGridViewCellStyle6;
            this.IT_CAR.HeaderText = "차종";
            this.IT_CAR.Name = "IT_CAR";
            this.IT_CAR.ReadOnly = true;
            this.IT_CAR.Width = 180;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(240, 65);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(922, 422);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SITE_CODE,
            this.CARRIER_YN,
            this.MAX_SQTY,
            this.NUM,
            this.WP_SNUMB,
            this.IT_SCODE,
            this.IT_SNAME,
            this.IT_MODEL,
            this.WP_SQUTY,
            this.SDATE,
            this.WC_GROUP,
            this.WKAREA_NAME,
            this.IT_SAFTY,
            this.ALC_CODE,
            this.ME_SCODE,
            this.IT_PKQTY,
            this.D_WEIGHT,
            this.MATCH_GUBN,
            this.A_WEIGHT,
            this.REMAIN_SQUTY,
            this.START_SQTY,
            this.RESULT_SQTY,
            this.WC_SCODE,
            this.WORK_AREA});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.Caption = "공장코드";
            this.SITE_CODE.FieldName = "SITE_CODE";
            this.SITE_CODE.Name = "SITE_CODE";
            // 
            // CARRIER_YN
            // 
            this.CARRIER_YN.Caption = "CARRIER_YN";
            this.CARRIER_YN.FieldName = "CARRIER_YN";
            this.CARRIER_YN.Name = "CARRIER_YN";
            // 
            // MAX_SQTY
            // 
            this.MAX_SQTY.Caption = "최대적재수량";
            this.MAX_SQTY.FieldName = "MAX_SQTY";
            this.MAX_SQTY.Name = "MAX_SQTY";
            // 
            // NUM
            // 
            this.NUM.AppearanceCell.Options.UseTextOptions = true;
            this.NUM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NUM.Caption = "우선순위";
            this.NUM.FieldName = "NUM";
            this.NUM.Name = "NUM";
            this.NUM.Visible = true;
            this.NUM.VisibleIndex = 0;
            this.NUM.Width = 106;
            // 
            // WP_SNUMB
            // 
            this.WP_SNUMB.Caption = "작업지시번호";
            this.WP_SNUMB.FieldName = "WP_SNUMB";
            this.WP_SNUMB.Name = "WP_SNUMB";
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품번";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 2;
            this.IT_SCODE.Width = 268;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 3;
            this.IT_SNAME.Width = 109;
            // 
            // IT_MODEL
            // 
            this.IT_MODEL.Caption = "모델";
            this.IT_MODEL.FieldName = "IT_MODEL";
            this.IT_MODEL.Name = "IT_MODEL";
            this.IT_MODEL.Visible = true;
            this.IT_MODEL.VisibleIndex = 4;
            this.IT_MODEL.Width = 121;
            // 
            // WP_SQUTY
            // 
            this.WP_SQUTY.AppearanceCell.Options.UseTextOptions = true;
            this.WP_SQUTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WP_SQUTY.Caption = "주간지시량";
            this.WP_SQUTY.FieldName = "WP_SQUTY";
            this.WP_SQUTY.Name = "WP_SQUTY";
            this.WP_SQUTY.Visible = true;
            this.WP_SQUTY.VisibleIndex = 5;
            this.WP_SQUTY.Width = 160;
            // 
            // SDATE
            // 
            this.SDATE.Caption = "시작일";
            this.SDATE.FieldName = "SDATE";
            this.SDATE.Name = "SDATE";
            // 
            // WC_GROUP
            // 
            this.WC_GROUP.Caption = "작업그룹";
            this.WC_GROUP.FieldName = "WC_GROUP";
            this.WC_GROUP.Name = "WC_GROUP";
            // 
            // WKAREA_NAME
            // 
            this.WKAREA_NAME.Caption = "작업그룹명";
            this.WKAREA_NAME.FieldName = "WKAREA_NAME";
            this.WKAREA_NAME.Name = "WKAREA_NAME";
            // 
            // IT_SAFTY
            // 
            this.IT_SAFTY.Caption = "안전재고";
            this.IT_SAFTY.FieldName = "IT_SAFTY";
            this.IT_SAFTY.Name = "IT_SAFTY";
            // 
            // ALC_CODE
            // 
            this.ALC_CODE.Caption = "ALC_CODE";
            this.ALC_CODE.FieldName = "ALC_CODE";
            this.ALC_CODE.Name = "ALC_CODE";
            this.ALC_CODE.Visible = true;
            this.ALC_CODE.VisibleIndex = 1;
            this.ALC_CODE.Width = 140;
            // 
            // ME_SCODE
            // 
            this.ME_SCODE.Caption = "자재유형코드";
            this.ME_SCODE.FieldName = "ME_SCODE";
            this.ME_SCODE.Name = "ME_SCODE";
            // 
            // IT_PKQTY
            // 
            this.IT_PKQTY.Caption = "포장수량";
            this.IT_PKQTY.FieldName = "IT_PKQTY";
            this.IT_PKQTY.Name = "IT_PKQTY";
            // 
            // D_WEIGHT
            // 
            this.D_WEIGHT.Caption = "표준편차";
            this.D_WEIGHT.FieldName = "D_WEIGHT";
            this.D_WEIGHT.Name = "D_WEIGHT";
            // 
            // MATCH_GUBN
            // 
            this.MATCH_GUBN.Caption = "매칭구분";
            this.MATCH_GUBN.FieldName = "MATCH_GUBN";
            this.MATCH_GUBN.Name = "MATCH_GUBN";
            // 
            // A_WEIGHT
            // 
            this.A_WEIGHT.Caption = "정미중량";
            this.A_WEIGHT.FieldName = "A_WEIGHT";
            this.A_WEIGHT.Name = "A_WEIGHT";
            // 
            // REMAIN_SQUTY
            // 
            this.REMAIN_SQUTY.Caption = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.FieldName = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.Name = "REMAIN_SQUTY";
            // 
            // START_SQTY
            // 
            this.START_SQTY.Caption = "START_SQTY";
            this.START_SQTY.FieldName = "START_SQTY";
            this.START_SQTY.Name = "START_SQTY";
            // 
            // RESULT_SQTY
            // 
            this.RESULT_SQTY.Caption = "RESULT_SQTY";
            this.RESULT_SQTY.FieldName = "RESULT_SQTY";
            this.RESULT_SQTY.Name = "RESULT_SQTY";
            // 
            // WC_SCODE
            // 
            this.WC_SCODE.Caption = "WC_SCODE";
            this.WC_SCODE.FieldName = "WC_SCODE";
            this.WC_SCODE.Name = "WC_SCODE";
            // 
            // WORK_AREA
            // 
            this.WORK_AREA.Caption = "WORK_AREA";
            this.WORK_AREA.FieldName = "WORK_AREA";
            this.WORK_AREA.Name = "WORK_AREA";
            // 
            // Work_plan_search_paint_dev
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1174, 565);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.dataGridView2);
            this.Name = "Work_plan_search_paint_dev";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "생산계획현황";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_CAR;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SITE_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn CARRIER_YN;
        private DevExpress.XtraGrid.Columns.GridColumn MAX_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn NUM;
        private DevExpress.XtraGrid.Columns.GridColumn WP_SNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn IT_MODEL;
        private DevExpress.XtraGrid.Columns.GridColumn WP_SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn SDATE;
        private DevExpress.XtraGrid.Columns.GridColumn WC_GROUP;
        private DevExpress.XtraGrid.Columns.GridColumn WKAREA_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SAFTY;
        private DevExpress.XtraGrid.Columns.GridColumn ALC_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn ME_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_PKQTY;
        private DevExpress.XtraGrid.Columns.GridColumn D_WEIGHT;
        private DevExpress.XtraGrid.Columns.GridColumn MATCH_GUBN;
        private DevExpress.XtraGrid.Columns.GridColumn A_WEIGHT;
        private DevExpress.XtraGrid.Columns.GridColumn REMAIN_SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn START_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn RESULT_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn WC_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn WORK_AREA;
    }
}