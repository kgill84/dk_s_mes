﻿namespace DK_Tablet.Popup
{
    partial class Process_mov_search_popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ROWNUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VW_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARRIER_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PM_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRDT_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HARFTYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GOOD_QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.PM_SNUMB_C = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 50;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROWNUM,
            this.VW_SNUMB,
            this.CARRIER_DATE,
            this.PM_SNUMB,
            this.HH,
            this.PRDT_ITEM,
            this.LOT_NO,
            this.CHECK_YN,
            this.LOT_DATE,
            this.HARFTYPE,
            this.GOOD_QTY});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Location = new System.Drawing.Point(67, 72);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 50;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 13F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowTemplate.Height = 50;
            this.dataGridView1.Size = new System.Drawing.Size(566, 487);
            this.dataGridView1.TabIndex = 2;
            // 
            // ROWNUM
            // 
            this.ROWNUM.DataPropertyName = "ROWNUM";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ROWNUM.DefaultCellStyle = dataGridViewCellStyle2;
            this.ROWNUM.FillWeight = 50.27934F;
            this.ROWNUM.HeaderText = "NO";
            this.ROWNUM.Name = "ROWNUM";
            this.ROWNUM.ReadOnly = true;
            this.ROWNUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // VW_SNUMB
            // 
            this.VW_SNUMB.DataPropertyName = "VW_SNUMB";
            this.VW_SNUMB.HeaderText = "시리얼";
            this.VW_SNUMB.Name = "VW_SNUMB";
            this.VW_SNUMB.ReadOnly = true;
            this.VW_SNUMB.Visible = false;
            this.VW_SNUMB.Width = 70;
            // 
            // CARRIER_DATE
            // 
            this.CARRIER_DATE.DataPropertyName = "CARRIER_DATE";
            this.CARRIER_DATE.HeaderText = "날짜";
            this.CARRIER_DATE.Name = "CARRIER_DATE";
            this.CARRIER_DATE.ReadOnly = true;
            this.CARRIER_DATE.Visible = false;
            // 
            // PM_SNUMB
            // 
            this.PM_SNUMB.DataPropertyName = "PM_SNUMB";
            this.PM_SNUMB.FillWeight = 73.4309F;
            this.PM_SNUMB.HeaderText = "이동표번호";
            this.PM_SNUMB.Name = "PM_SNUMB";
            this.PM_SNUMB.ReadOnly = true;
            this.PM_SNUMB.Width = 200;
            // 
            // HH
            // 
            this.HH.DataPropertyName = "HH";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HH.DefaultCellStyle = dataGridViewCellStyle3;
            this.HH.FillWeight = 71.10838F;
            this.HH.HeaderText = "경과시간";
            this.HH.Name = "HH";
            this.HH.ReadOnly = true;
            this.HH.Width = 160;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.DataPropertyName = "PRDT_ITEM";
            this.PRDT_ITEM.HeaderText = "품목코드";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.ReadOnly = true;
            this.PRDT_ITEM.Visible = false;
            // 
            // LOT_NO
            // 
            this.LOT_NO.DataPropertyName = "LOT_NO";
            this.LOT_NO.HeaderText = "LOT NO";
            this.LOT_NO.Name = "LOT_NO";
            this.LOT_NO.ReadOnly = true;
            this.LOT_NO.Visible = false;
            // 
            // CHECK_YN
            // 
            this.CHECK_YN.DataPropertyName = "CHECK_YN";
            this.CHECK_YN.HeaderText = "완료구분";
            this.CHECK_YN.Name = "CHECK_YN";
            this.CHECK_YN.ReadOnly = true;
            this.CHECK_YN.Visible = false;
            // 
            // LOT_DATE
            // 
            this.LOT_DATE.DataPropertyName = "LOT_DATE";
            this.LOT_DATE.HeaderText = "LOT 생성날짜";
            this.LOT_DATE.Name = "LOT_DATE";
            this.LOT_DATE.ReadOnly = true;
            this.LOT_DATE.Visible = false;
            // 
            // HARFTYPE
            // 
            this.HARFTYPE.DataPropertyName = "HARFTYPE";
            this.HARFTYPE.HeaderText = "주/야간";
            this.HARFTYPE.Name = "HARFTYPE";
            this.HARFTYPE.ReadOnly = true;
            this.HARFTYPE.Visible = false;
            // 
            // GOOD_QTY
            // 
            this.GOOD_QTY.DataPropertyName = "GOOD_QTY";
            this.GOOD_QTY.HeaderText = "GOOD_QTY";
            this.GOOD_QTY.Name = "GOOD_QTY";
            this.GOOD_QTY.ReadOnly = true;
            this.GOOD_QTY.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(12, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1246, 6);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(324, 41);
            this.button3.TabIndex = 52;
            this.button3.TabStop = false;
            this.button3.Text = "이동표 리스트 조회";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(12, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(49, 487);
            this.labelControl2.TabIndex = 54;
            this.labelControl2.Text = "입\r\n\r\n고\r\n\r\n순\r\n\r\n서";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(639, 72);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(49, 487);
            this.labelControl1.TabIndex = 55;
            this.labelControl1.Text = "입\r\n\r\n고\r\n\r\n이\r\n\r\n동\r\n\r\n표";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.ColumnHeadersHeight = 50;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PM_SNUMB_C});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.Location = new System.Drawing.Point(694, 72);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 50;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView2.RowTemplate.Height = 50;
            this.dataGridView2.Size = new System.Drawing.Size(567, 487);
            this.dataGridView2.TabIndex = 56;
            // 
            // PM_SNUMB_C
            // 
            this.PM_SNUMB_C.DataPropertyName = "PM_SNUMB_C";
            this.PM_SNUMB_C.FillWeight = 73.4309F;
            this.PM_SNUMB_C.HeaderText = "이동표번호";
            this.PM_SNUMB_C.Name = "PM_SNUMB_C";
            this.PM_SNUMB_C.ReadOnly = true;
            this.PM_SNUMB_C.Width = 250;
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1109, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 51);
            this.btn_close.TabIndex = 57;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // Process_mov_search_popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1270, 571);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Process_mov_search_popup";
            this.Text = "이동표 리스트 조회";
            this.Load += new System.EventHandler(this.Process_mov_search_popup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROWNUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn VW_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARRIER_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn PM_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn HH;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRDT_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HARFTYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn GOOD_QTY;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PM_SNUMB_C;
        private System.Windows.Forms.Button btn_close;
    }
}