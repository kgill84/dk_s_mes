﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Fail_Input_Qr_PopUp : Form
    {
        int idx;
        DataTable dt;
        public DataTable rdt { get; set; }
        public int totalQty { get; set; }
        public int total { get; set; }


        public Fail_Input_Qr_PopUp(int idx, DataTable dt)
        {
            InitializeComponent();
            this.idx = idx;
            this.dt = dt;
        }

        private void Fail_Input_Qr_PopUp_Load(object sender, EventArgs e)
        {
            DataTable dt = this.dt.Copy();
            dt.DefaultView.Sort = "QTY DESC";
            gridControl1.DataSource = dt;
           
        }

        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            DataTable dt = this.dt.Copy();
            dt.Rows.Clear();

            

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                DataRow dr = dt.NewRow();
                dr["NUMB"] = gridView1.GetRowCellDisplayText(i, NUMB);
                dr["FT_NAME"] = gridView1.GetRowCellDisplayText(i, FT_NAME);
                dr["QTY"] = int.Parse(gridView1.GetRowCellValue(i, QTY).ToString());
                dt.Rows.Add(dr);
                totalQty += int.Parse(gridView1.GetRowCellValue(i, QTY).ToString());
            }

            if (totalQty > total)
            {
                MessageBox.Show("검사품목의 발주수량보다 불량수량이 많습니다.");
                return;
            }

            this.rdt = dt;

            

            DialogResult = DialogResult.OK;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column == QTY)
            {
                KeyPad KeyPad = new KeyPad();
                KeyPad.txtDigit.Text = "0";
                if (KeyPad.ShowDialog() == DialogResult.OK)
                {
                    string qty = string.IsNullOrWhiteSpace(KeyPad.txt_value) ? "0" : ((int)Double.Parse(KeyPad.txt_value)).ToString();
                    gridView1.SetRowCellValue(e.RowHandle, QTY, qty);
                  
                }
                totalQty = 0;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
