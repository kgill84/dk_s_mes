﻿namespace DK_Tablet.Popup
{
    partial class Mix_PP_Card_Input
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.WC_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SITE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SIZE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_Save = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView2.ColumnHeadersHeight = 60;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.WC_CODE,
            this.SITE_CODE,
            this.CARD_NO,
            this.IT_SCODE,
            this.SIZE,
            this.CHECK_YN,
            this.BTN_DELETE});
            this.dataGridView2.Location = new System.Drawing.Point(12, 89);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 60;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView2.RowTemplate.Height = 60;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(683, 400);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // WC_CODE
            // 
            this.WC_CODE.DataPropertyName = "WC_CODE";
            this.WC_CODE.HeaderText = "작업장";
            this.WC_CODE.Name = "WC_CODE";
            this.WC_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WC_CODE.Visible = false;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.DataPropertyName = "SITE_CODE";
            this.SITE_CODE.HeaderText = "공장코드";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SITE_CODE.Visible = false;
            // 
            // CARD_NO
            // 
            this.CARD_NO.DataPropertyName = "CARD_NO";
            this.CARD_NO.HeaderText = "카드번호";
            this.CARD_NO.Name = "CARD_NO";
            this.CARD_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CARD_NO.Width = 105;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.DataPropertyName = "IT_SCODE";
            this.IT_SCODE.HeaderText = "품목코드";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SCODE.Width = 300;
            // 
            // SIZE
            // 
            this.SIZE.DataPropertyName = "SIZE";
            this.SIZE.HeaderText = "수량";
            this.SIZE.Name = "SIZE";
            this.SIZE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SIZE.Width = 150;
            // 
            // CHECK_YN
            // 
            this.CHECK_YN.DataPropertyName = "CHECK_YN";
            this.CHECK_YN.HeaderText = "체크";
            this.CHECK_YN.Name = "CHECK_YN";
            this.CHECK_YN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CHECK_YN.Visible = false;
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.DataPropertyName = "BTN_DELETE";
            this.BTN_DELETE.HeaderText = "삭제";
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.Text = "삭제";
            this.BTN_DELETE.UseColumnTextForButtonValue = true;
            // 
            // btn_Save
            // 
            this.btn_Save.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_Save.Appearance.Options.UseFont = true;
            this.btn_Save.Location = new System.Drawing.Point(295, 495);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(197, 66);
            this.btn_Save.TabIndex = 2;
            this.btn_Save.Text = "등록";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_Cancel.Appearance.Options.UseFont = true;
            this.btn_Cancel.Location = new System.Drawing.Point(498, 495);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(197, 66);
            this.btn_Cancel.TabIndex = 2;
            this.btn_Cancel.Text = "취소";
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // Mix_PP_Card_Input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(715, 574);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.dataGridView2);
            this.Name = "Mix_PP_Card_Input";
            this.Text = "Mix_PP_Card_Input";
            this.Load += new System.EventHandler(this.Mix_PP_Card_Input_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn WC_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SITE_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SCODE;
        private System.Windows.Forms.DataGridViewButtonColumn SIZE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_YN;
        private System.Windows.Forms.DataGridViewButtonColumn BTN_DELETE;
        private DevExpress.XtraEditors.SimpleButton btn_Save;
        private DevExpress.XtraEditors.SimpleButton btn_Cancel;
    }
}