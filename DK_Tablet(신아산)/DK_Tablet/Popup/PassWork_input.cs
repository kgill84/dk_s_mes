﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class PassWork_input : Form
    {
        public  PassWork_input()
        {
            InitializeComponent();
        }

        private  void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            
            if (sender == btnBack)
            {
                if (txtPassWord.EditValue.ToString().Length < 1)
                    return;
                txtPassWord.EditValue = txtPassWord.EditValue.ToString().Substring(0, txtPassWord.EditValue.ToString().Length - 1);
                
            }
            else
            {
                txtPassWord.EditValue = txtPassWord.EditValue.ToString() + btn.Tag.ToString();
            }

        }

        private  void btnDone_Click(object sender, EventArgs e)
        {
            if (txtPassWord.EditValue.ToString().Equals("111333"))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("비밀번호 오류 입니다.");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
