﻿using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Material_Fail_Popup : Form
    {
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        public string mo_snumb { get; set; }
        public string wc_code { get; set; }

        public Material_Fail_Popup()
        {
            InitializeComponent();
        }

        private void txtIn_put_Enter(object sender, EventArgs e)
        {
            txtIn_put.BackColor = Color.SkyBlue;
        }

        private void txtIn_put_Leave(object sender, EventArgs e)
        {
            txtIn_put.BackColor = Color.DarkRed;
        }

        private void Material_Fail_Popup_Load(object sender, EventArgs e)
        {
            txtIn_put.Focus();
        }

        private void txtIn_put_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                string recieve_str = txtIn_put.Text.Trim();
                //로뜨 연결 삭제 and 자재 출고
                if (recieve_str.Split('*').Length.Equals(2))//ACS 사출  QR코드 읽었을때
                {
                    string[] it_scode_lot = recieve_str.Split('*');

                    SUB_SAVE.material_fail_QR_reading(it_scode_lot[1], it_scode_lot[0], wc_code, mo_snumb);
                }
                

                DialogResult = DialogResult.OK;

            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
