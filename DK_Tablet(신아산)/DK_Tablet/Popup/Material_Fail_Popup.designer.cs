﻿namespace DK_Tablet.Popup
{
    partial class Material_Fail_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIn_put = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightSalmon;
            this.label1.Font = new System.Drawing.Font("굴림", 25F);
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(550, 62);
            this.label1.TabIndex = 0;
            this.label1.Text = "QR 코드를 스캔해 주세요.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIn_put
            // 
            this.txtIn_put.Font = new System.Drawing.Font("굴림", 45F);
            this.txtIn_put.Location = new System.Drawing.Point(13, 155);
            this.txtIn_put.Name = "txtIn_put";
            this.txtIn_put.Size = new System.Drawing.Size(549, 76);
            this.txtIn_put.TabIndex = 1;
            this.txtIn_put.Enter += new System.EventHandler(this.txtIn_put_Enter);
            this.txtIn_put.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIn_put_KeyPress);
            this.txtIn_put.Leave += new System.EventHandler(this.txtIn_put_Leave);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(355, 260);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(207, 70);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Material_Fail_Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(574, 356);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtIn_put);
            this.Controls.Add(this.label1);
            this.Name = "Material_Fail_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "불량스캔";
            this.Load += new System.EventHandler(this.Material_Fail_Popup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIn_put;
        private System.Windows.Forms.Button btnClose;
    }
}