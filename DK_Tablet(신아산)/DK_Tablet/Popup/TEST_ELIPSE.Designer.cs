﻿namespace DK_Tablet.Popup
{
    partial class TEST_ELIPSE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.roundButton1 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton2 = new DK_Tablet.UC_Control.RoundButton();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(94, 174);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // roundButton1
            // 
            this.roundButton1.cr = System.Drawing.Color.Empty;
            this.roundButton1.ForeColor = System.Drawing.Color.Black;
            this.roundButton1.Location = new System.Drawing.Point(114, 85);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton1.Size = new System.Drawing.Size(25, 25);
            this.roundButton1.TabIndex = 1;
            // 
            // roundButton2
            // 
            this.roundButton2.cr = System.Drawing.Color.Empty;
            this.roundButton2.Location = new System.Drawing.Point(161, 91);
            this.roundButton2.Name = "roundButton2";
            this.roundButton2.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton2.Size = new System.Drawing.Size(25, 25);
            this.roundButton2.TabIndex = 2;
            // 
            // TEST_ELIPSE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.roundButton2);
            this.Controls.Add(this.roundButton1);
            this.Controls.Add(this.simpleButton1);
            this.Name = "TEST_ELIPSE";
            this.Text = "TEST_ELIPSE";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private UC_Control.RoundButton roundButton1;
        private UC_Control.RoundButton roundButton2;
    }
}