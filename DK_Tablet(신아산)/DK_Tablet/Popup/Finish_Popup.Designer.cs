﻿namespace DK_Tablet.Popup
{
    partial class Finish_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Finish_Popup));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PC_FINISH = new DevExpress.XtraEditors.SimpleButton();
            this.PROGRAM_FINISH = new DevExpress.XtraEditors.SimpleButton();
            this.CANCLE = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.PC_FINISH, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.PROGRAM_FINISH, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.CANCLE, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(917, 337);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // PC_FINISH
            // 
            this.PC_FINISH.Appearance.BackColor = System.Drawing.Color.Red;
            this.PC_FINISH.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.PC_FINISH.Appearance.Font = new System.Drawing.Font("Tahoma", 54.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PC_FINISH.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.PC_FINISH.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.PC_FINISH.Appearance.Options.UseBackColor = true;
            this.PC_FINISH.Appearance.Options.UseFont = true;
            this.PC_FINISH.Appearance.Options.UseForeColor = true;
            this.PC_FINISH.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.PC_FINISH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PC_FINISH.Image = ((System.Drawing.Image)(resources.GetObject("PC_FINISH.Image")));
            this.PC_FINISH.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.PC_FINISH.Location = new System.Drawing.Point(3, 3);
            this.PC_FINISH.Name = "PC_FINISH";
            this.PC_FINISH.Size = new System.Drawing.Size(299, 331);
            this.PC_FINISH.TabIndex = 0;
            this.PC_FINISH.Text = "PC\r\n종료";
            this.PC_FINISH.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // PROGRAM_FINISH
            // 
            this.PROGRAM_FINISH.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            this.PROGRAM_FINISH.Appearance.BackColor2 = System.Drawing.Color.LightCyan;
            this.PROGRAM_FINISH.Appearance.Font = new System.Drawing.Font("Tahoma", 54.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PROGRAM_FINISH.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.PROGRAM_FINISH.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.PROGRAM_FINISH.Appearance.Options.UseBackColor = true;
            this.PROGRAM_FINISH.Appearance.Options.UseFont = true;
            this.PROGRAM_FINISH.Appearance.Options.UseForeColor = true;
            this.PROGRAM_FINISH.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.PROGRAM_FINISH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PROGRAM_FINISH.Image = ((System.Drawing.Image)(resources.GetObject("PROGRAM_FINISH.Image")));
            this.PROGRAM_FINISH.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.PROGRAM_FINISH.Location = new System.Drawing.Point(308, 3);
            this.PROGRAM_FINISH.Name = "PROGRAM_FINISH";
            this.PROGRAM_FINISH.Size = new System.Drawing.Size(299, 331);
            this.PROGRAM_FINISH.TabIndex = 0;
            this.PROGRAM_FINISH.Text = "프로그램\r\n종료";
            this.PROGRAM_FINISH.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // CANCLE
            // 
            this.CANCLE.Appearance.BackColor = System.Drawing.Color.Silver;
            this.CANCLE.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.CANCLE.Appearance.Font = new System.Drawing.Font("Tahoma", 54.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CANCLE.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.CANCLE.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.CANCLE.Appearance.Options.UseBackColor = true;
            this.CANCLE.Appearance.Options.UseFont = true;
            this.CANCLE.Appearance.Options.UseForeColor = true;
            this.CANCLE.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.CANCLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CANCLE.Location = new System.Drawing.Point(613, 3);
            this.CANCLE.Name = "CANCLE";
            this.CANCLE.Size = new System.Drawing.Size(301, 331);
            this.CANCLE.TabIndex = 0;
            this.CANCLE.Text = "취소";
            this.CANCLE.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Finish_Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 337);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Finish_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Finish_Popup";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton PC_FINISH;
        private DevExpress.XtraEditors.SimpleButton PROGRAM_FINISH;
        private DevExpress.XtraEditors.SimpleButton CANCLE;
    }
}