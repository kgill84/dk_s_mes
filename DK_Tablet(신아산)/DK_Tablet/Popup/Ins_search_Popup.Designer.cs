﻿namespace DK_Tablet
{
    partial class Ins_search_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IN_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl_sdate = new DevExpress.XtraEditors.LabelControl();
            this.btn_next_date = new DevExpress.XtraEditors.SimpleButton();
            this.btn_prev_date = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.IT_SUNIT = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1010, 575);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 70);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(13, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1149, 6);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(251, 41);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "입고 현황";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(13, 76);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1149, 492);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IT_SCODE,
            this.IT_SNAME,
            this.IT_SUNIT,
            this.IN_SQTY,
            this.SQUTY});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품목코드";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 0;
            this.IT_SCODE.Width = 267;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 1;
            this.IT_SNAME.Width = 352;
            // 
            // IN_SQTY
            // 
            this.IN_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.IN_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.IN_SQTY.Caption = "입고량";
            this.IN_SQTY.DisplayFormat.FormatString = "n0";
            this.IN_SQTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.IN_SQTY.FieldName = "IN_SQTY";
            this.IN_SQTY.Name = "IN_SQTY";
            this.IN_SQTY.Visible = true;
            this.IN_SQTY.VisibleIndex = 3;
            this.IN_SQTY.Width = 216;
            // 
            // SQUTY
            // 
            this.SQUTY.AppearanceCell.Options.UseTextOptions = true;
            this.SQUTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SQUTY.Caption = "재고량";
            this.SQUTY.DisplayFormat.FormatString = "n0";
            this.SQUTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SQUTY.FieldName = "SQUTY";
            this.SQUTY.Name = "SQUTY";
            this.SQUTY.Visible = true;
            this.SQUTY.VisibleIndex = 4;
            this.SQUTY.Width = 223;
            // 
            // lbl_sdate
            // 
            this.lbl_sdate.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.lbl_sdate.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbl_sdate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_sdate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_sdate.Location = new System.Drawing.Point(354, 14);
            this.lbl_sdate.Name = "lbl_sdate";
            this.lbl_sdate.Size = new System.Drawing.Size(193, 42);
            this.lbl_sdate.TabIndex = 54;
            // 
            // btn_next_date
            // 
            this.btn_next_date.Location = new System.Drawing.Point(553, 3);
            this.btn_next_date.Name = "btn_next_date";
            this.btn_next_date.Size = new System.Drawing.Size(78, 57);
            this.btn_next_date.TabIndex = 74;
            this.btn_next_date.Text = ">>";
            this.btn_next_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // btn_prev_date
            // 
            this.btn_prev_date.Location = new System.Drawing.Point(270, 3);
            this.btn_prev_date.Name = "btn_prev_date";
            this.btn_prev_date.Size = new System.Drawing.Size(78, 57);
            this.btn_prev_date.TabIndex = 75;
            this.btn_prev_date.Text = "<<";
            this.btn_prev_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(885, 12);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(277, 44);
            this.progressBarControl1.TabIndex = 76;
            // 
            // IT_SUNIT
            // 
            this.IT_SUNIT.AppearanceCell.Options.UseTextOptions = true;
            this.IT_SUNIT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IT_SUNIT.Caption = "단위";
            this.IT_SUNIT.FieldName = "IT_SUNIT";
            this.IT_SUNIT.Name = "IT_SUNIT";
            this.IT_SUNIT.Visible = true;
            this.IT_SUNIT.VisibleIndex = 2;
            this.IT_SUNIT.Width = 73;
            // 
            // Ins_search_Popup
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 659);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.btn_next_date);
            this.Controls.Add(this.btn_prev_date);
            this.Controls.Add(this.lbl_sdate);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Name = "Ins_search_Popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "작업지시현황";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl lbl_sdate;
        private DevExpress.XtraEditors.SimpleButton btn_next_date;
        private DevExpress.XtraEditors.SimpleButton btn_prev_date;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn IN_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SUNIT;
    }
}