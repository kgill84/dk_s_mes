﻿using DevExpress.Sparkline;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;
using DK_Tablet.FUNCTION;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class fail_search_popup : Form
    {
        GET_DATA GET_DATA = new GET_DATA();
        public string wc_group { get; set; }
        public string wc_code { get; set; }
        public string wc_name { get; set; }
        public fail_search_popup()
        {
            InitializeComponent();
        }

        private DataTable CreateChartData(int rowCount)
        {
            // Create an empty table.
            DataTable table = new DataTable("Table1");

            // Add two columns to the table.
            table.Columns.Add("Argument", typeof(Int32));
            table.Columns.Add("Value", typeof(Int32));

            // Add data rows to the table.
            Random rnd = new Random();
            DataRow row = null;
            for (int i = 0; i < rowCount; i++)
            {
                row = table.NewRow();
                row["Argument"] = i;
                row["Value"] = rnd.Next(100);
                table.Rows.Add(row);
            }

            return table;
        }
        XYDiagram XYDiagram = new XYDiagram();
        DateTime now = DateTime.Now;
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();
        PointSeriesLabel pointSeriesLabel1 = new PointSeriesLabel();

        private void fail_search_popup_Load(object sender, EventArgs e)
        {

            txt_title.Text = now.Year + "년 " + now.Month + "월" + wc_name.Trim() + " 불량현황";
            txt_title.Font = CHECK_FUNC.AutoFontSize(txt_title, txt_title.Text);
            try
            {
                ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
                chart.Series.Clear();
                splashScreenManager1.ShowWaitForm();
                // Create an empty Bar series and add it to the chart.
                Series series = new Series("Series1", ViewType.Line);
                chart.Series.Add(series);

                // Generate a data table and bind the series to it.
                //series.DataSource = CreateChartData();
                series.DataSource = GET_DATA.GetFail_Dt(wc_code, now.Month, now.Year);
                // Specify data members to bind the series.
                series.ArgumentScaleType = ScaleType.Numerical;
                series.ArgumentDataMember = "Argument";
                series.ValueScaleType = ScaleType.Numerical;
                series.ValueDataMembers.AddRange(new string[] { "Value" });
                
                //pointSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 12F);
                series.Label.Font = new System.Drawing.Font("Tahoma", 12F);
                series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
                series.Label.Border.Thickness = 3;
                series.Label.Border.Visibility = DevExpress.Utils.DefaultBoolean.True;
                //series.Label.Font = new System.Drawing.Font("Tahoma", 12F);
                series.Label.LineLength = 20;
                series.Label.LineStyle.Thickness = 3;
                series.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
                series.Label.MaxWidth = 100;
                series.Label.Shadow.Size = 8;
                series.Label.Shadow.Visible = true;
                series.View.Color = System.Drawing.Color.LightCoral;

                // Set some properties to get a nice-looking chart.
                //((SideBySideBarSeriesView)series.View).ColorEach = true;

                //XYDiagram.AxisY.Visibility = DevExpress.Utils.DefaultBoolean.False;

                //chart.Diagram = XYDiagram;
                //chart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

                // Dock the chart into its parent and add it to the current form.
                //chart.Dock = DockStyle.Fill;
                //this.Controls.Add(chart);

                gridControl1.DataSource = GET_DATA.GetFail_Dt_DETAIL(wc_code, now.Month, wc_group, now.Year);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                splashScreenManager1.CloseWaitForm();
            }
            
        }



        bool btn_push = false;
        private void btn_fail_detail_Click(object sender, EventArgs e)
        {
            if (btn_push)
            {
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Both;
                btn_push = false;                
                btn_fail_detail.Text = "펼치기";
            }
            else
            {
                splitContainerControl1.PanelVisibility = SplitPanelVisibility.Panel1;
                btn_push = true;
                btn_fail_detail.Text = "접기";
            }
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_prev_Click(object sender, EventArgs e)
        {
            next_perv_btn_click(-1);
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            next_perv_btn_click(1);
        }
        public void next_perv_btn_click(int i)
        {
            now = now.AddMonths(i);
            txt_title.Text = now.Year + "년 " + now.Month + "월" + wc_name.Trim() + " 불량현황";
            
            try
            {
                splashScreenManager1.ShowWaitForm();
                chart.Series.Clear();
                // Create an empty Bar series and add it to the chart.
                Series series = new Series("Series1", ViewType.Line);
                chart.Series.Add(series);
                
                // Generate a data table and bind the series to it.
                //series.DataSource = CreateChartData();
                series.DataSource = GET_DATA.GetFail_Dt(wc_code, now.Month, now.Year);
                // Specify data members to bind the series.
                series.ArgumentScaleType = ScaleType.Numerical;
                series.ArgumentDataMember = "Argument";
                series.ValueScaleType = ScaleType.Numerical;
                series.ValueDataMembers.AddRange(new string[] { "Value" });
                series.Label.Font = new System.Drawing.Font("Tahoma", 12F);
                series.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
                series.Label.Border.Thickness = 3;
                series.Label.Border.Visibility = DevExpress.Utils.DefaultBoolean.True;
                //series.Label.Font = new System.Drawing.Font("Tahoma", 12F);
                series.Label.LineLength = 20;
                series.Label.LineStyle.Thickness = 3;
                series.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
                series.Label.MaxWidth = 100;
                series.Label.Shadow.Size = 8;
                series.Label.Shadow.Visible = true;
                series.View.Color = System.Drawing.Color.LightCoral;

                gridControl1.DataSource = GET_DATA.GetFail_Dt_DETAIL(wc_code, now.Month, wc_group, now.Year);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                splashScreenManager1.CloseWaitForm();
            }
        }



    }
}
