﻿namespace DK_Tablet.Popup
{
    partial class KeyPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDigit7 = new System.Windows.Forms.Button();
            this.btnDigit8 = new System.Windows.Forms.Button();
            this.btnDigit9 = new System.Windows.Forms.Button();
            this.btnDigit4 = new System.Windows.Forms.Button();
            this.btnDigit5 = new System.Windows.Forms.Button();
            this.btnDigit6 = new System.Windows.Forms.Button();
            this.btnDigit3 = new System.Windows.Forms.Button();
            this.btnDigit1 = new System.Windows.Forms.Button();
            this.btnDigit2 = new System.Windows.Forms.Button();
            this.btnDigit0 = new System.Windows.Forms.Button();
            this.btnDigitDot = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.txtDigit = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDigit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel1.Controls.Add(this.btnDigit7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit8, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit9, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit0, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnDigitDot, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnDone, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDigit, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 395);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnDigit7
            // 
            this.btnDigit7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit7.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit7.Location = new System.Drawing.Point(4, 82);
            this.btnDigit7.Name = "btnDigit7";
            this.btnDigit7.Size = new System.Drawing.Size(99, 71);
            this.btnDigit7.TabIndex = 1;
            this.btnDigit7.Tag = "7";
            this.btnDigit7.Text = "7";
            this.btnDigit7.UseVisualStyleBackColor = true;
            this.btnDigit7.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit8
            // 
            this.btnDigit8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit8.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit8.Location = new System.Drawing.Point(110, 82);
            this.btnDigit8.Name = "btnDigit8";
            this.btnDigit8.Size = new System.Drawing.Size(99, 71);
            this.btnDigit8.TabIndex = 1;
            this.btnDigit8.Tag = "8";
            this.btnDigit8.Text = "8";
            this.btnDigit8.UseVisualStyleBackColor = true;
            this.btnDigit8.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit9
            // 
            this.btnDigit9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit9.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit9.Location = new System.Drawing.Point(216, 82);
            this.btnDigit9.Name = "btnDigit9";
            this.btnDigit9.Size = new System.Drawing.Size(99, 71);
            this.btnDigit9.TabIndex = 1;
            this.btnDigit9.Tag = "9";
            this.btnDigit9.Text = "9";
            this.btnDigit9.UseVisualStyleBackColor = true;
            this.btnDigit9.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit4
            // 
            this.btnDigit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit4.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit4.Location = new System.Drawing.Point(4, 160);
            this.btnDigit4.Name = "btnDigit4";
            this.btnDigit4.Size = new System.Drawing.Size(99, 71);
            this.btnDigit4.TabIndex = 1;
            this.btnDigit4.Tag = "4";
            this.btnDigit4.Text = "4";
            this.btnDigit4.UseVisualStyleBackColor = true;
            this.btnDigit4.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit5
            // 
            this.btnDigit5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit5.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit5.Location = new System.Drawing.Point(110, 160);
            this.btnDigit5.Name = "btnDigit5";
            this.btnDigit5.Size = new System.Drawing.Size(99, 71);
            this.btnDigit5.TabIndex = 1;
            this.btnDigit5.Tag = "5";
            this.btnDigit5.Text = "5";
            this.btnDigit5.UseVisualStyleBackColor = true;
            this.btnDigit5.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit6
            // 
            this.btnDigit6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit6.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit6.Location = new System.Drawing.Point(216, 160);
            this.btnDigit6.Name = "btnDigit6";
            this.btnDigit6.Size = new System.Drawing.Size(99, 71);
            this.btnDigit6.TabIndex = 1;
            this.btnDigit6.Tag = "6";
            this.btnDigit6.Text = "6";
            this.btnDigit6.UseVisualStyleBackColor = true;
            this.btnDigit6.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit3
            // 
            this.btnDigit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit3.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit3.Location = new System.Drawing.Point(216, 238);
            this.btnDigit3.Name = "btnDigit3";
            this.btnDigit3.Size = new System.Drawing.Size(99, 71);
            this.btnDigit3.TabIndex = 1;
            this.btnDigit3.Tag = "3";
            this.btnDigit3.Text = "3";
            this.btnDigit3.UseVisualStyleBackColor = true;
            this.btnDigit3.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit1
            // 
            this.btnDigit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit1.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit1.Location = new System.Drawing.Point(4, 238);
            this.btnDigit1.Name = "btnDigit1";
            this.btnDigit1.Size = new System.Drawing.Size(99, 71);
            this.btnDigit1.TabIndex = 1;
            this.btnDigit1.Tag = "1";
            this.btnDigit1.Text = "1";
            this.btnDigit1.UseVisualStyleBackColor = true;
            this.btnDigit1.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit2
            // 
            this.btnDigit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit2.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit2.Location = new System.Drawing.Point(110, 238);
            this.btnDigit2.Name = "btnDigit2";
            this.btnDigit2.Size = new System.Drawing.Size(99, 71);
            this.btnDigit2.TabIndex = 1;
            this.btnDigit2.Tag = "2";
            this.btnDigit2.Text = "2";
            this.btnDigit2.UseVisualStyleBackColor = true;
            this.btnDigit2.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit0
            // 
            this.btnDigit0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit0.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit0.Location = new System.Drawing.Point(110, 316);
            this.btnDigit0.Name = "btnDigit0";
            this.btnDigit0.Size = new System.Drawing.Size(99, 75);
            this.btnDigit0.TabIndex = 1;
            this.btnDigit0.Tag = "0";
            this.btnDigit0.Text = "0";
            this.btnDigit0.UseVisualStyleBackColor = true;
            this.btnDigit0.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigitDot
            // 
            this.btnDigitDot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigitDot.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigitDot.Location = new System.Drawing.Point(4, 316);
            this.btnDigitDot.Name = "btnDigitDot";
            this.btnDigitDot.Size = new System.Drawing.Size(99, 75);
            this.btnDigitDot.TabIndex = 1;
            this.btnDigitDot.Tag = ".0";
            this.btnDigitDot.Text = ".";
            this.btnDigitDot.UseVisualStyleBackColor = true;
            this.btnDigitDot.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("굴림", 24F);
            this.btnBack.Location = new System.Drawing.Point(216, 316);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(99, 75);
            this.btnBack.TabIndex = 1;
            this.btnBack.Tag = "back";
            this.btnBack.Text = "←";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDone
            // 
            this.btnDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDone.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDone.Location = new System.Drawing.Point(322, 82);
            this.btnDone.Name = "btnDone";
            this.tableLayoutPanel1.SetRowSpan(this.btnDone, 4);
            this.btnDone.Size = new System.Drawing.Size(99, 309);
            this.btnDone.TabIndex = 1;
            this.btnDone.Tag = "done";
            this.btnDone.Text = "입력";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // txtDigit
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtDigit, 4);
            this.txtDigit.EditValue = "0";
            this.txtDigit.Location = new System.Drawing.Point(1, 1);
            this.txtDigit.Margin = new System.Windows.Forms.Padding(0);
            this.txtDigit.Name = "txtDigit";
            this.txtDigit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.txtDigit.Properties.Appearance.Options.UseFont = true;
            this.txtDigit.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDigit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDigit.Size = new System.Drawing.Size(423, 78);
            this.txtDigit.TabIndex = 0;
            // 
            // KeyPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 395);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "KeyPad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyPad";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDigit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnDigit7;
        private System.Windows.Forms.Button btnDigit8;
        private System.Windows.Forms.Button btnDigit9;
        private System.Windows.Forms.Button btnDigit4;
        private System.Windows.Forms.Button btnDigit5;
        private System.Windows.Forms.Button btnDigit6;
        private System.Windows.Forms.Button btnDigit3;
        private System.Windows.Forms.Button btnDigit1;
        private System.Windows.Forms.Button btnDigit2;
        private System.Windows.Forms.Button btnDigit0;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDigitDot;
        private System.Windows.Forms.Button btnDone;
        public DevExpress.XtraEditors.TextEdit txtDigit;
    }
}