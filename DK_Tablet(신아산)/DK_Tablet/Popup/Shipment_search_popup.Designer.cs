﻿namespace DK_Tablet.Popup
{
    partial class Shipment_search_popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_done = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SH_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_ODATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MN_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_DELIV = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DELIVERY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LOC_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PC_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PC_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_ORDNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_VNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SHIPPERS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_STIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_ETIME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(12, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1007, 5);
            this.pictureBox1.TabIndex = 53;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(12, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(251, 35);
            this.button3.TabIndex = 52;
            this.button3.TabStop = false;
            this.button3.Text = "출하지시선택";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(867, 522);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 60);
            this.btn_close.TabIndex = 54;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(699, 522);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 60);
            this.btn_done.TabIndex = 55;
            this.btn_done.Text = "선택";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Font = new System.Drawing.Font("굴림", 9F);
            this.gridControl1.Location = new System.Drawing.Point(12, 64);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1007, 452);
            this.gridControl1.TabIndex = 58;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.Lime;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 60;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SH_SNUMB,
            this.SH_SDATE,
            this.SH_ODATE,
            this.MN_SCODE,
            this.SH_DELIV,
            this.DELIVERY,
            this.LOC_CODE,
            this.LOC_NAME,
            this.PC_SCODE,
            this.PC_SNAME,
            this.SH_ORDNO,
            this.SH_VNUMB,
            this.SH_SHIPPERS,
            this.SH_STIME,
            this.SH_ETIME});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.IndicatorWidth = 40;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            // 
            // SH_SNUMB
            // 
            this.SH_SNUMB.Caption = "등록번호";
            this.SH_SNUMB.FieldName = "SH_SNUMB";
            this.SH_SNUMB.Name = "SH_SNUMB";
            // 
            // SH_SDATE
            // 
            this.SH_SDATE.Caption = "등록일";
            this.SH_SDATE.FieldName = "SH_SDATE";
            this.SH_SDATE.Name = "SH_SDATE";
            // 
            // SH_ODATE
            // 
            this.SH_ODATE.Caption = "출하일";
            this.SH_ODATE.FieldName = "SH_ODATE";
            this.SH_ODATE.Name = "SH_ODATE";
            this.SH_ODATE.Visible = true;
            this.SH_ODATE.VisibleIndex = 0;
            // 
            // MN_SCODE
            // 
            this.MN_SCODE.Caption = "담당자";
            this.MN_SCODE.FieldName = "MN_SCODE";
            this.MN_SCODE.Name = "MN_SCODE";
            // 
            // SH_DELIV
            // 
            this.SH_DELIV.Caption = "운송업체";
            this.SH_DELIV.FieldName = "SH_DELIV";
            this.SH_DELIV.Name = "SH_DELIV";
            this.SH_DELIV.Visible = true;
            this.SH_DELIV.VisibleIndex = 1;
            // 
            // DELIVERY
            // 
            this.DELIVERY.Caption = "납품장소";
            this.DELIVERY.FieldName = "DELIVERY";
            this.DELIVERY.Name = "DELIVERY";
            // 
            // LOC_CODE
            // 
            this.LOC_CODE.Caption = "고객사창고";
            this.LOC_CODE.FieldName = "LOC_CODE";
            this.LOC_CODE.Name = "LOC_CODE";
            // 
            // LOC_NAME
            // 
            this.LOC_NAME.Caption = "창고명";
            this.LOC_NAME.FieldName = "LOC_NAME";
            this.LOC_NAME.Name = "LOC_NAME";
            this.LOC_NAME.Visible = true;
            this.LOC_NAME.VisibleIndex = 3;
            // 
            // PC_SCODE
            // 
            this.PC_SCODE.Caption = "거래처코드";
            this.PC_SCODE.FieldName = "PC_SCODE";
            this.PC_SCODE.Name = "PC_SCODE";
            // 
            // PC_SNAME
            // 
            this.PC_SNAME.Caption = "거래처명";
            this.PC_SNAME.FieldName = "PC_SNAME";
            this.PC_SNAME.Name = "PC_SNAME";
            this.PC_SNAME.Visible = true;
            this.PC_SNAME.VisibleIndex = 2;
            // 
            // SH_ORDNO
            // 
            this.SH_ORDNO.Caption = "차수";
            this.SH_ORDNO.FieldName = "SH_ORDNO";
            this.SH_ORDNO.Name = "SH_ORDNO";
            this.SH_ORDNO.Visible = true;
            this.SH_ORDNO.VisibleIndex = 4;
            // 
            // SH_VNUMB
            // 
            this.SH_VNUMB.Caption = "차량번호";
            this.SH_VNUMB.FieldName = "SH_VNUMB";
            this.SH_VNUMB.Name = "SH_VNUMB";
            this.SH_VNUMB.Visible = true;
            this.SH_VNUMB.VisibleIndex = 5;
            // 
            // SH_SHIPPERS
            // 
            this.SH_SHIPPERS.Caption = "차량기사";
            this.SH_SHIPPERS.FieldName = "SH_SHIPPERS";
            this.SH_SHIPPERS.Name = "SH_SHIPPERS";
            this.SH_SHIPPERS.Visible = true;
            this.SH_SHIPPERS.VisibleIndex = 6;
            // 
            // SH_STIME
            // 
            this.SH_STIME.Caption = "출발시간";
            this.SH_STIME.FieldName = "SH_STIME";
            this.SH_STIME.Name = "SH_STIME";
            this.SH_STIME.Visible = true;
            this.SH_STIME.VisibleIndex = 7;
            // 
            // SH_ETIME
            // 
            this.SH_ETIME.Caption = "도착시간";
            this.SH_ETIME.FieldName = "SH_ETIME";
            this.SH_ETIME.Name = "SH_ETIME";
            this.SH_ETIME.Visible = true;
            this.SH_ETIME.VisibleIndex = 8;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // Shipment_search_popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1031, 594);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_done);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Name = "Shipment_search_popup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "출하지시선택";
            this.Load += new System.EventHandler(this.Shipment_search_popup_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_done;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SDATE;
        private DevExpress.XtraGrid.Columns.GridColumn SH_ODATE;
        private DevExpress.XtraGrid.Columns.GridColumn MN_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn SH_DELIV;
        private DevExpress.XtraGrid.Columns.GridColumn DELIVERY;
        private DevExpress.XtraGrid.Columns.GridColumn PC_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn PC_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn SH_ORDNO;
        private DevExpress.XtraGrid.Columns.GridColumn SH_VNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SHIPPERS;
        private DevExpress.XtraGrid.Columns.GridColumn SH_STIME;
        private DevExpress.XtraGrid.Columns.GridColumn SH_ETIME;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.Columns.GridColumn LOC_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn LOC_NAME;
    }
}