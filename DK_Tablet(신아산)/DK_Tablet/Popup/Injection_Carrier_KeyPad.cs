﻿using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Injection_Carrier_KeyPad : Form
    {
        string strCon;
        //string datetime = "";
        public string wc_group { get; set; }
        public string transfer = "NO";
        GET_DATA GET_DATA = new GET_DATA();
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();


        public string txt_value { get; set; }
        public string it_scode { get; set; }
        public Injection_Carrier_KeyPad()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string btn_tag = btn.Tag.ToString();
            if (btn_tag == "0" || btn_tag == "1" || btn_tag == "2" || btn_tag == "3" || btn_tag == "4" || btn_tag == "5" || btn_tag == "6" || btn_tag == "7" || btn_tag == "8" || btn_tag == "9")
            {
                
                if (txtDigit.Text.Length == 3)
                {
                    return;
                }
                txtDigit.EditValue = txtDigit.EditValue + btn_tag;
            }
            
            if (btn_tag == "back")
            {
                if (txtDigit.Text.Length > 0)
                {
                    txtDigit.EditValue = txtDigit.EditValue.ToString().Substring(0, txtDigit.EditValue.ToString().Length - 1);
                }
               
            }

        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_dc.Text.Trim()))
            {
                MessageBox.Show("차종을 선택해 주세요");
                return;
            }
            if (txtDigit.Text.Length != 3)
            {
                MessageBox.Show("3자리를 입력해 주세요..");
                return;
            }
            if (wc_group == "PIM01" || wc_group == "PLR")
            {
                if (txt_dc.Text.Trim().Equals("P"))
                {
                    txt_value = txt_dc.Text.Trim() + txtDigit.EditValue.ToString();                    
                }
                else
                {
                    txt_value = txt_dc.Text.Trim() + "-" + txtDigit.EditValue.ToString();
                }

            }
            else
            {
                if (txt_dc.Text.Trim().Equals("P"))
                {
                    txt_value = txt_dc.Text.Trim() + txtDigit.EditValue.ToString();
                    transfer = "YES";
                }
                else
                {
                    txt_value = txt_dc.Text.Trim() + "-" + txtDigit.EditValue.ToString();
                }
                
            }
            if (CHECK_FUNC.carrier_check(txt_value))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("공대차가 아닙니다.\n 등록 할 수 없는 대차 입니다.");
                return;
            }
        }

        private void Injection_Carrier_KeyPad_Load(object sender, EventArgs e)
        {
            /*if (wc_group == "PIM01" || wc_group == "PLR")
            {
                txt_dc.Text = "P";
                dataGridView4.Enabled = false;
            }
            else
            {*/
                // 모델명 가져오기
                string it_model = "";
            //차종 가져오기
                string prj_code = "";
                it_model = GET_DATA.getIt_model(it_scode);
                prj_code = GET_DATA.getPrj_code(it_scode);
                strCon = Properties.Settings.Default.SQL_DKQT;
                this.Cursor = Cursors.WaitCursor;
                SqlConnection conn = new SqlConnection(strCon);

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT DC_GUBN,CAR_NAME,DC_SPEC,DC_SQTY,DC_DESCR FROM CARRIER_MASTER WHERE PRJ_CODE='" + prj_code + "'", conn);
                da.SelectCommand.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, "CARRIER_MASTER");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("오류 : " + ex.Message);
                }
                this.Cursor = Cursors.Default;
                dataGridView4.DataSource = ds.Tables["CARRIER_MASTER"];

                conn.Close();
                dataGridView4.CurrentCell = null;
            //}
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            txt_dc.Text = dataGridView4.Rows[dataGridView4.CurrentCell.RowIndex].Cells["DC_GUBN"].Value.ToString();            
        }
    }
}
