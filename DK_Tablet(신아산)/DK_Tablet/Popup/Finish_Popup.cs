﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Finish_Popup : Form
    {
        public string btn_state = "";
        public Finish_Popup()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (sender == CANCLE)
            {
                btn_state = "0";
            }
            else if (sender == PROGRAM_FINISH)
            {
                btn_state = "1";
            }
            else if (sender == PC_FINISH)
            {
                btn_state = "2";
            }
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

    }
}
