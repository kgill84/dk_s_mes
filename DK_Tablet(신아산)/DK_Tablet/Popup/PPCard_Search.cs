﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class PPCard_Search : Form
    {
        public DataTable dt { get; set; }
        public string WC_CODE_str {get;set;}
        public string CARD_NO_str {get;set;}
        public string IT_SCODE_str {get;set;}
        public string SIZE_str { get; set; }
        public string SITE_CODE_str { get; set; }


        public string wc_code { get; set; }
        public PPCard_Search()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void po_realese_search_Load(object sender, EventArgs e)
        {
            //getdata();
            dataGridView1.DataSource = dt;
            
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                
                if(dataGridView1.Rows[i].Cells["CHECK_YN"].Value.ToString()=="생산가능")
                {
                    
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.SandyBrown;
                }
            }
            dataGridView1.CurrentCell = null;
        }
        

        private void btn_done_Click(object sender, EventArgs e)
        {
            int cxtl = dataGridView1.CurrentCell.RowIndex;
            if(cxtl<0)
            {
                MessageBox.Show("PP Card를 선택해주세요");
                return;
            }
            if (dataGridView1.Rows[cxtl].Cells["CHECK_YN"].Value.ToString() == "생산가능") { 
                WC_CODE_str  = dataGridView1.Rows[cxtl].Cells["WC_CODE"].Value.ToString();
                CARD_NO_str = dataGridView1.Rows[cxtl].Cells["CARD_NO"].Value.ToString();
                IT_SCODE_str = dataGridView1.Rows[cxtl].Cells["IT_SCODE"].Value.ToString();
                SIZE_str = dataGridView1.Rows[cxtl].Cells["SIZE"].Value.ToString();
                SITE_CODE_str = dataGridView1.Rows[cxtl].Cells["SITE_CODE"].Value.ToString();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("생산 할 수 없는 PP 카드 입니다.");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int cxtl = dataGridView1.CurrentCell.RowIndex;
            splashScreenManager1.ShowWaitForm();
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_ITEM_ROW_JAGO", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", dataGridView1.Rows[cxtl].Cells["IT_SCODE"].Value.ToString());
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "ITEM_ROW_JAGO");

                DataTable dt = ds.Tables["ITEM_ROW_JAGO"];

                dataGridView2.DataSource = dt;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {                
                splashScreenManager1.CloseWaitForm();
            }

            
        }
    }
}
