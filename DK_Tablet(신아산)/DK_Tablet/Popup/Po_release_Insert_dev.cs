﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DK_Tablet.Popup;

namespace DK_Tablet
{
    public partial class Po_release_Insert_dev : DevExpress.XtraEditors.XtraForm
    {
        public string IT_SCODE_str { get; set; }
        public string IT_SNAME_str { get; set; }
        public string IT_MODEL_str = "";
        public string MO_SQUTY_str { get; set; }
        public string MO_SNUMB_str { get; set; }
        public string WC_CODE_str { get; set; }
        public string SITE_CODE_str { get; set; }
        public string ALC_CODE_str { get; set; }
        public string ME_SCODE_str { get; set; }
        public string IT_PKQTY_str { get; set; }
        public string MATCH_GUBN_str { get; set; }
        public string D_WEIGHT_str { get; set; }
        public string A_WEIGHT_str { get; set; }

        public string wc_group { get; set; }
        public string wc_code { get; set; }
        public string MAX_SQTY_str { get; set; }
        public string CARRIER_YN_str { get; set; }
        DataTable routing_DT = new DataTable();
        
        public Po_release_Insert_dev()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            routing_DT.Columns.Add("IT_SCODE", typeof(string));
            routing_DT.Columns.Add("IT_SNAME", typeof(string));
            routing_DT.Columns.Add("IT_MODEL", typeof(string));
            routing_DT.Columns.Add("ALC_CODE", typeof(string));
            routing_DT.Columns.Add("MO_SQUTY", typeof(int));

            DateTime dt = DateTime.Now;
            if(dt.Hour>=0 && dt.Hour<8)
            {
                days = days - 1;
            }
            date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
            lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");

            
            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            worker.RunWorkerAsync();
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;

                routing_DT = getInsert();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = routing_DT;

                /*if (routing_DT.Rows.Count > 0)
                {
                    string sdate = gridView1.GetRowCellValue(0, "SDATE").ToString();
                    lbl_sdate.Text = sdate.Substring(0, 4) + "-" + sdate.Substring(4, 2) + "-" + sdate.Substring(6, 2);
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        private void btn_date_Click(object sender, EventArgs e)
        {
            try
            {
                routing_DT.Rows.Clear();
                IT_MODEL_str = "";

                if (sender == btn_prev_date)
                {
                    days = days - 1;
                }
                else if (sender == btn_next_date)
                {
                    days = days + 1;
                }
                date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
            
        }
        
        private DataTable getInsert()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SELECT A.PRDT_ITEM AS IT_SCODE,B.IT_SNAME,B.IT_MODEL,C.ALC_CODE, 0 AS MO_SQUTY FROM ROUTING_BASIC_V3 A "
                                                    +" INNER JOIN IT_MASTER B ON A.PRDT_ITEM = B.IT_SCODE AND B.SITE_CODE='D001'"
                                                    +" LEFT JOIN ALC_IT_MASTER C ON A.PRDT_ITEM=C.IT_SCODE"
                                                    + " WHERE A.WC_SCODE=@WC_CODE AND A.WO_POINT='Y' ORDER BY A.PRDT_ITEM ", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.Text;

                
                da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
                
                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "ROUTING_NEW");

                dt = ds.Tables["ROUTING_NEW"];

                //gridControl1.DataSource = dt;
                //gridControl1.Refresh();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void btn_done_Click(object sender, EventArgs e)
        {
            //int cxtl = dataGridView1.CurrentCell.RowIndex;
            if (gridView1.SelectedRowsCount < 0)
            {
                MessageBox.Show("품목을 선택해주세요");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("작업 지시를 생성 하시겠습니까?"
                                    , "알림", MessageBoxButtons.OKCancel);
                if (dr == DialogResult.OK)
                {
                    

                    string strCon;
                    string[] str = new string[2];
                    strCon = Properties.Settings.Default.SQL_DKQT;
                    string sql = "";

                    sql = "SP_TABLET_INSERT_PO_RELEASE";

                    SqlConnection conn = new SqlConnection(strCon);
                    SqlCommand cmd =
                            new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("IT_SCODE", gridView1.GetFocusedRowCellValue("IT_SCODE").ToString().Trim());
                    cmd.Parameters.AddWithValue("WC_CODE", wc_code);
                    cmd.Parameters.AddWithValue("MO_SQUTY", int.Parse(gridView1.GetFocusedRowCellValue("MO_SQUTY").ToString()));
                    conn.Open();

                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[0].ToString().Trim() != "")
                            {
                                IT_SCODE_str = reader["IT_SCODE"].ToString();
                                IT_SNAME_str = reader["IT_SNAME"].ToString().Trim();
                                IT_MODEL_str = reader["IT_MODEL"].ToString().Trim();
                                MO_SQUTY_str = reader["MO_SQUTY"].ToString().Trim();
                                MO_SNUMB_str = reader["MO_SNUMB"].ToString().Trim();
                                SITE_CODE_str = "D001";
                                ALC_CODE_str = reader["ALC_CODE"].ToString().Trim();
                                ME_SCODE_str = reader["ME_SCODE"].ToString().Trim();
                                IT_PKQTY_str = reader["IT_PKQTY"].ToString().Trim();
                                D_WEIGHT_str = reader["D_WEIGHT"].ToString().Trim();
                                A_WEIGHT_str = reader["A_WEIGHT"].ToString().Trim();
                                MATCH_GUBN_str = reader["MATCH_GUBN"].ToString().Trim();
                                CARRIER_YN_str = reader["CARRIER_YN"].ToString().Trim();
                                MAX_SQTY_str = reader["MAX_SQTY"].ToString().Trim();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }

                    DialogResult = DialogResult.OK;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR : "+ex.Message);
            }
        }



        private void btn_day_night_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_day_night.Text.Equals("주간"))
                {
                    btn_day_night.Text = "야간";
                }
                else
                {
                    btn_day_night.Text = "주간";
                }
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }

        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Column.Name.Equals("MO_SQUTY"))
            {
                KeyPad KeyPad = new KeyPad();
                if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    gridView1.SetFocusedRowCellValue(MO_SQUTY, KeyPad.txt_value);
                }
            }
        }

    }
}
