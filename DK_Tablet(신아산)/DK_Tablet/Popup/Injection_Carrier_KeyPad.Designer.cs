﻿namespace DK_Tablet.Popup
{
    partial class Injection_Carrier_KeyPad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDigit7 = new System.Windows.Forms.Button();
            this.btnDigit8 = new System.Windows.Forms.Button();
            this.btnDigit9 = new System.Windows.Forms.Button();
            this.btnDigit4 = new System.Windows.Forms.Button();
            this.btnDigit5 = new System.Windows.Forms.Button();
            this.btnDigit6 = new System.Windows.Forms.Button();
            this.btnDigit3 = new System.Windows.Forms.Button();
            this.btnDigit1 = new System.Windows.Forms.Button();
            this.btnDigit2 = new System.Windows.Forms.Button();
            this.btnDigit0 = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDone = new System.Windows.Forms.Button();
            this.txtDigit = new DevExpress.XtraEditors.TextEdit();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.DC_GUBN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAR_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DC_SPEC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DC_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DC_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_dc = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDigit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_dc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel1.Controls.Add(this.btnDigit7, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit8, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit9, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnDigit0, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnDone, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDigit, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(564, 104);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(425, 395);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnDigit7
            // 
            this.btnDigit7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit7.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit7.Location = new System.Drawing.Point(4, 82);
            this.btnDigit7.Name = "btnDigit7";
            this.btnDigit7.Size = new System.Drawing.Size(99, 71);
            this.btnDigit7.TabIndex = 1;
            this.btnDigit7.Tag = "7";
            this.btnDigit7.Text = "7";
            this.btnDigit7.UseVisualStyleBackColor = true;
            this.btnDigit7.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit8
            // 
            this.btnDigit8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit8.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit8.Location = new System.Drawing.Point(110, 82);
            this.btnDigit8.Name = "btnDigit8";
            this.btnDigit8.Size = new System.Drawing.Size(99, 71);
            this.btnDigit8.TabIndex = 1;
            this.btnDigit8.Tag = "8";
            this.btnDigit8.Text = "8";
            this.btnDigit8.UseVisualStyleBackColor = true;
            this.btnDigit8.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit9
            // 
            this.btnDigit9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit9.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit9.Location = new System.Drawing.Point(216, 82);
            this.btnDigit9.Name = "btnDigit9";
            this.btnDigit9.Size = new System.Drawing.Size(99, 71);
            this.btnDigit9.TabIndex = 1;
            this.btnDigit9.Tag = "9";
            this.btnDigit9.Text = "9";
            this.btnDigit9.UseVisualStyleBackColor = true;
            this.btnDigit9.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit4
            // 
            this.btnDigit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit4.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit4.Location = new System.Drawing.Point(4, 160);
            this.btnDigit4.Name = "btnDigit4";
            this.btnDigit4.Size = new System.Drawing.Size(99, 71);
            this.btnDigit4.TabIndex = 1;
            this.btnDigit4.Tag = "4";
            this.btnDigit4.Text = "4";
            this.btnDigit4.UseVisualStyleBackColor = true;
            this.btnDigit4.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit5
            // 
            this.btnDigit5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit5.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit5.Location = new System.Drawing.Point(110, 160);
            this.btnDigit5.Name = "btnDigit5";
            this.btnDigit5.Size = new System.Drawing.Size(99, 71);
            this.btnDigit5.TabIndex = 1;
            this.btnDigit5.Tag = "5";
            this.btnDigit5.Text = "5";
            this.btnDigit5.UseVisualStyleBackColor = true;
            this.btnDigit5.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit6
            // 
            this.btnDigit6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit6.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit6.Location = new System.Drawing.Point(216, 160);
            this.btnDigit6.Name = "btnDigit6";
            this.btnDigit6.Size = new System.Drawing.Size(99, 71);
            this.btnDigit6.TabIndex = 1;
            this.btnDigit6.Tag = "6";
            this.btnDigit6.Text = "6";
            this.btnDigit6.UseVisualStyleBackColor = true;
            this.btnDigit6.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit3
            // 
            this.btnDigit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit3.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit3.Location = new System.Drawing.Point(216, 238);
            this.btnDigit3.Name = "btnDigit3";
            this.btnDigit3.Size = new System.Drawing.Size(99, 71);
            this.btnDigit3.TabIndex = 1;
            this.btnDigit3.Tag = "3";
            this.btnDigit3.Text = "3";
            this.btnDigit3.UseVisualStyleBackColor = true;
            this.btnDigit3.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit1
            // 
            this.btnDigit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit1.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit1.Location = new System.Drawing.Point(4, 238);
            this.btnDigit1.Name = "btnDigit1";
            this.btnDigit1.Size = new System.Drawing.Size(99, 71);
            this.btnDigit1.TabIndex = 1;
            this.btnDigit1.Tag = "1";
            this.btnDigit1.Text = "1";
            this.btnDigit1.UseVisualStyleBackColor = true;
            this.btnDigit1.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit2
            // 
            this.btnDigit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit2.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit2.Location = new System.Drawing.Point(110, 238);
            this.btnDigit2.Name = "btnDigit2";
            this.btnDigit2.Size = new System.Drawing.Size(99, 71);
            this.btnDigit2.TabIndex = 1;
            this.btnDigit2.Tag = "2";
            this.btnDigit2.Text = "2";
            this.btnDigit2.UseVisualStyleBackColor = true;
            this.btnDigit2.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDigit0
            // 
            this.btnDigit0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDigit0.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDigit0.Location = new System.Drawing.Point(110, 316);
            this.btnDigit0.Name = "btnDigit0";
            this.btnDigit0.Size = new System.Drawing.Size(99, 75);
            this.btnDigit0.TabIndex = 1;
            this.btnDigit0.Tag = "0";
            this.btnDigit0.Text = "0";
            this.btnDigit0.UseVisualStyleBackColor = true;
            this.btnDigit0.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("굴림", 24F);
            this.btnBack.Location = new System.Drawing.Point(216, 316);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(99, 75);
            this.btnBack.TabIndex = 1;
            this.btnBack.Tag = "back";
            this.btnBack.Text = "←";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.Button_Click);
            // 
            // btnDone
            // 
            this.btnDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDone.Font = new System.Drawing.Font("굴림", 24F);
            this.btnDone.Location = new System.Drawing.Point(322, 82);
            this.btnDone.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.btnDone.Name = "btnDone";
            this.tableLayoutPanel1.SetRowSpan(this.btnDone, 4);
            this.btnDone.Size = new System.Drawing.Size(102, 309);
            this.btnDone.TabIndex = 1;
            this.btnDone.Tag = "done";
            this.btnDone.Text = "입력";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // txtDigit
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtDigit, 4);
            this.txtDigit.EditValue = "";
            this.txtDigit.Location = new System.Drawing.Point(1, 1);
            this.txtDigit.Margin = new System.Windows.Forms.Padding(0);
            this.txtDigit.Name = "txtDigit";
            this.txtDigit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.txtDigit.Properties.Appearance.Options.UseFont = true;
            this.txtDigit.Properties.Appearance.Options.UseTextOptions = true;
            this.txtDigit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txtDigit.Properties.MaxLength = 3;
            this.txtDigit.Size = new System.Drawing.Size(423, 78);
            this.txtDigit.TabIndex = 0;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AllowUserToAddRows = false;
            this.dataGridView4.AllowUserToDeleteRows = false;
            this.dataGridView4.AllowUserToResizeRows = false;
            this.dataGridView4.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 20F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView4.ColumnHeadersHeight = 60;
            this.dataGridView4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DC_GUBN,
            this.CAR_NAME,
            this.DC_SPEC,
            this.DC_SQTY,
            this.DC_DESCR});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 20F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView4.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView4.Location = new System.Drawing.Point(12, 104);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.ReadOnly = true;
            this.dataGridView4.RowHeadersVisible = false;
            this.dataGridView4.RowTemplate.Height = 60;
            this.dataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView4.Size = new System.Drawing.Size(546, 395);
            this.dataGridView4.TabIndex = 3;
            this.dataGridView4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellClick);
            // 
            // DC_GUBN
            // 
            this.DC_GUBN.DataPropertyName = "DC_GUBN";
            this.DC_GUBN.HeaderText = "대차구분";
            this.DC_GUBN.Name = "DC_GUBN";
            this.DC_GUBN.ReadOnly = true;
            this.DC_GUBN.Width = 150;
            // 
            // CAR_NAME
            // 
            this.CAR_NAME.DataPropertyName = "CAR_NAME";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CAR_NAME.DefaultCellStyle = dataGridViewCellStyle2;
            this.CAR_NAME.HeaderText = "차종";
            this.CAR_NAME.Name = "CAR_NAME";
            this.CAR_NAME.ReadOnly = true;
            this.CAR_NAME.Width = 200;
            // 
            // DC_SPEC
            // 
            this.DC_SPEC.DataPropertyName = "DC_SPEC";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.DC_SPEC.DefaultCellStyle = dataGridViewCellStyle3;
            this.DC_SPEC.HeaderText = "사양";
            this.DC_SPEC.Name = "DC_SPEC";
            this.DC_SPEC.ReadOnly = true;
            this.DC_SPEC.Width = 200;
            // 
            // DC_SQTY
            // 
            this.DC_SQTY.DataPropertyName = "DC_SQTY";
            this.DC_SQTY.HeaderText = "수용수";
            this.DC_SQTY.Name = "DC_SQTY";
            this.DC_SQTY.ReadOnly = true;
            this.DC_SQTY.Visible = false;
            this.DC_SQTY.Width = 80;
            // 
            // DC_DESCR
            // 
            this.DC_DESCR.DataPropertyName = "DC_DESCR";
            this.DC_DESCR.HeaderText = "비고";
            this.DC_DESCR.Name = "DC_DESCR";
            this.DC_DESCR.ReadOnly = true;
            this.DC_DESCR.Visible = false;
            // 
            // txt_dc
            // 
            this.txt_dc.EditValue = "";
            this.txt_dc.Location = new System.Drawing.Point(269, 12);
            this.txt_dc.Name = "txt_dc";
            this.txt_dc.Properties.Appearance.Font = new System.Drawing.Font("맑은 고딕", 40F);
            this.txt_dc.Properties.Appearance.Options.UseFont = true;
            this.txt_dc.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_dc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_dc.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt_dc.Properties.AutoHeight = false;
            this.txt_dc.Size = new System.Drawing.Size(473, 84);
            this.txt_dc.TabIndex = 4;
            // 
            // Injection_Carrier_KeyPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1001, 503);
            this.Controls.Add(this.txt_dc);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Injection_Carrier_KeyPad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Injection_Carrier_KeyPad";
            this.Load += new System.EventHandler(this.Injection_Carrier_KeyPad_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDigit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_dc.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnDigit7;
        private System.Windows.Forms.Button btnDigit8;
        private System.Windows.Forms.Button btnDigit9;
        private System.Windows.Forms.Button btnDigit4;
        private System.Windows.Forms.Button btnDigit5;
        private System.Windows.Forms.Button btnDigit6;
        private System.Windows.Forms.Button btnDigit3;
        private System.Windows.Forms.Button btnDigit1;
        private System.Windows.Forms.Button btnDigit2;
        private System.Windows.Forms.Button btnDigit0;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnDone;
        private DevExpress.XtraEditors.TextEdit txtDigit;
        public System.Windows.Forms.DataGridView dataGridView4;
        private DevExpress.XtraEditors.TextEdit txt_dc;
        private System.Windows.Forms.DataGridViewTextBoxColumn DC_GUBN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAR_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn DC_SPEC;
        private System.Windows.Forms.DataGridViewTextBoxColumn DC_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn DC_DESCR;
    }
}