﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class mold_search : Form
    {
        public mold_search()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void mold_search_Load(object sender, EventArgs e)
        {
            //getdata1();
            //getdata2();
        }
        private void getdata1()
        {
            this.Cursor = Cursors.WaitCursor;
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            //da.SelectCommand.Parameters.AddWithValue("@SDATE", sdate);

            DataSet ds = new DataSet();
            da.Fill(ds, "mold_search1");

            DataTable dt = ds.Tables["mold_search1"];

            dataGridView1.DataSource = dt;
            //totalCnt.Text = dataGridView1.RowCount.ToString("#,#");
            this.Cursor = Cursors.Default;
        }
       
    }
}
