﻿using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Worker_Popup : Form
    {
        //public string str_wc_code { get; set; }
        public string str_wc_group { get; set; }
        public string sw_code { get; set; }
        public DataTable select_worker_dt { get; set; }
        GET_DATA GET_DATA = new GET_DATA();
        public Worker_Popup()
        {
            InitializeComponent();
        }
        
        private void Worker_Popup_Load(object sender, EventArgs e)
        {
            //선택된 작업자 

            dataGridView1.DataSource = GET_DATA.Worker_info_set(str_wc_group);
            
            if (select_worker_dt.Rows.Count > 0)
            {
                lbl_Cnt.Text = select_worker_dt.Rows.Count.ToString();
                foreach (DataRow dr in select_worker_dt.Rows)
                {
                    foreach (DataGridViewRow dgvr in dataGridView1.Rows)
                    {                        
                        if (dgvr.Cells["SERNO"].Value.ToString().Trim().Equals(dr[0].ToString().Trim()))
                        {
                            dgvr.DefaultCellStyle.BackColor = Color.Cyan;
                            dgvr.Cells["CHK"].Value = "선택";
                        }                      
                    }                    
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex.ToString() == "-1")
                return;

            if (dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor == Color.Cyan)
            {
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.White;
                dataGridView1.Rows[e.RowIndex].Cells["CHK"].Value = "";
            }
            else
            {
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Cyan;
                dataGridView1.Rows[e.RowIndex].Cells["CHK"].Value = "선택";
            }
            lbl_Cnt.Text = "0";
            foreach (DataGridViewRow dgvr in dataGridView1.Rows)
            {
                
                if (dgvr.DefaultCellStyle.BackColor == Color.Cyan)
                {
                    lbl_Cnt.Text = (int.Parse(lbl_Cnt.Text) + 1).ToString();
                }
            }
            
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            try
            {
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                foreach (DataGridViewRow dgvr in dataGridView1.Rows)
                {
                    if (!string.IsNullOrWhiteSpace(dgvr.Cells["CHK"].Value.ToString().Trim()))
                    {
                        if (dgvr.Cells["CHK"].Value.ToString().Trim().Equals("선택"))
                        {
                            DataRow dr = select_worker_dt.NewRow();
                            dr["SERNO"] = dgvr.Cells["SERNO"].Value.ToString();
                            dr["WCR_CODE"] = dgvr.Cells["WCR_CODE"].Value.ToString();
                            dr["WCR_NAME"] = dgvr.Cells["WCR_NAME"].Value.ToString();
                            dr["CHK"] = dgvr.Cells["CHK"].Value.ToString();
                            select_worker_dt.Rows.Add(dr);
                        }
                    }
                }
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            
            
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
