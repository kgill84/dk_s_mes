﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class Inspection_Stock_Item_Popup : DevExpress.XtraEditors.XtraForm
    {
        public string IT_SCODE_str { get; set; }
        DataTable Inspection_Stock_Item_DT = new DataTable();
        public Inspection_Stock_Item_Popup()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            try { 
            Inspection_Stock_Item_DT.Columns.Add("IT_SCODE", typeof(string));

            
            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            worker.RunWorkerAsync();
            }
            catch
            {
                MessageBox.Show("ERROR : 다시 시도해주세요");
            }
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;
                
                Inspection_Stock_Item_DT = getdata();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = Inspection_Stock_Item_DT;

            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        
        private DataTable getdata()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SELECT IT_SCODE FROM INSPECTION_STOCK_ITEM WHERE INS_DATE='"+DateTime.Now.ToString("yyyyMMdd")+"'", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.Text;

                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "INSPECTION_STOCK_ITEM");

                dt = ds.Tables["INSPECTION_STOCK_ITEM"];

                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /*
        private void ColorChange()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //MessageBox.Show(row.Cells["합계"].Value.ToString());
                if (2 == int.Parse(row.Cells["GUBN"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;

                }
            }
        }
        */
        private void btn_done_Click(object sender, EventArgs e)
        {
            //int cxtl = dataGridView1.CurrentCell.RowIndex;
            if (gridView1.FocusedRowHandle < 0)
            {
                MessageBox.Show("품목을 선택해 주세요");
                return;
            }
            try
            {
                
                IT_SCODE_str = gridView1.GetFocusedRowCellValue("IT_SCODE").ToString().Trim();
               
                DialogResult = DialogResult.OK;
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR : "+ex.Message);
            }
        }

    }
}
