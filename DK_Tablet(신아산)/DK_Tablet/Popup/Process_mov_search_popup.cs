﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Process_mov_search_popup : Form
    {
        public DataTable dt1 = new DataTable();
        public DataTable dt2 = new DataTable();
        public Process_mov_search_popup()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Process_mov_search_popup_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = dt1;
            dataGridView2.DataSource = dt2;
        }
    }
}
