﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DK_Tablet.Popup;
using DK_Tablet.FUNCTION;

namespace DK_Tablet
{
    public partial class metarial_search : Form
    {
        public string wc_code { get; set; }
        public string str_worker { get; set; }
        GET_DATA GET_DATA = new GET_DATA();
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        DataTable look_dt = new DataTable();
        public metarial_search()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void metarial_search_Load(object sender, EventArgs e)
        {   
            getdata();
            //repositoryItemLookUpEdit1.DataSource = 
            LookUpEditGet();
            if (wc_code == null)
            {
                MessageBox.Show("작업장을 선택해주세요.");
                this.Dispose();
            }

        }
        public void LookUpEditGet()
        {
                
            //totalCnt.Text = dataGridView1.RowCount.ToString("#,#");
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SELECT ''AS CODE, ''AS NAME UNION ALL SELECT CA_SCODE AS CODE,RTRIM(CA_CNTNT) AS NAME FROM  CA_MASTER", conn);
            da.SelectCommand.CommandType = CommandType.Text;
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "metarial_search");

                DataTable dt = ds.Tables["metarial_search"];

                repositoryItemLookUpEdit1.DataSource = dt;
                repositoryItemLookUpEdit1.DisplayMember = "NAME";
                repositoryItemLookUpEdit1.ValueMember = "CODE";                
            }
            catch
            {

            }
            finally
            {
                
            }
        }
        private void getdata()
        {
            
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_METARIAL_JAGO_SELECT_CS", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "metarial_search");

                DataTable dt = ds.Tables["metarial_search"];

                gridControl1.DataSource = dt;
                //totalCnt.Text = dataGridView1.RowCount.ToString("#,#");

                
            }
            catch
            {

            }
            finally
            {
                
            }
        }

        private void btn_jago_out_Click(object sender, EventArgs e)
        {
            PassWork_input PassWork_input = new PassWork_input();
            if (PassWork_input.ShowDialog() == DialogResult.OK)
            {

                bool check = false;
                string strCon;
                strCon = Properties.Settings.Default.SQL_DKQT;

                SqlConnection conn = new SqlConnection(strCon);


                SqlCommand cmd =
                        new SqlCommand("USP_WC_SCODE_METARIAL_OUT_CS", conn);

                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.AddWithValue("@TVP", gridControl1.DataSource);
                cmd.Parameters.AddWithValue("@WC_SCODE", wc_code);


                //커넥션오픈 실행
                conn.Open();

                try
                {
                    cmd.ExecuteNonQuery();
                    check = true;
                    MessageBox.Show("등록 완료");
                    getdata();

                }

                catch (Exception ex)
                {
                    check = false;
                    MessageBox.Show("등록 실패 : " + ex.Message);

                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            
            if (e.Column.Name.Equals("INS_SQTY"))
            {
                KeyPad KeyPad = new KeyPad();
                if (KeyPad.ShowDialog() == DialogResult.OK)
                {
                    gridView1.SetFocusedRowCellValue("INS_SQTY", KeyPad.int_value);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String it_scode = (gridView1.GetRowCellValue(gridView1.FocusedRowHandle, IT_SCODE)).ToString();
            if (string.IsNullOrWhiteSpace(it_scode))
            {
                MessageBox.Show("품목을 선택해주세요");
            }
            this.Cursor = Cursors.WaitCursor;
            Fail_Input_Commit Fail_Input = new Fail_Input_Commit();

            if (string.IsNullOrWhiteSpace(str_worker))
            {
                str_worker = "";
            }

            if (Fail_Input.ShowDialog() == DialogResult.OK)
            {

                //불량 등록
                if (SUB_SAVE.fail_input_data_commit(Properties.Settings.Default.SITE_CODE, it_scode, Fail_Input.lost_code, wc_code, Fail_Input.sqty, str_worker))
                {
                    // fail_qty = (float.Parse(fail_qty) + (float.Parse(Fail_Input.sqty))).ToString();
                }
            }
            this.Cursor = Cursors.WaitCursor;
            getdata();
            this.Cursor = Cursors.Default;
        }

    }
}
