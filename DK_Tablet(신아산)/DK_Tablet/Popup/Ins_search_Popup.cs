﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class Ins_search_Popup : DevExpress.XtraEditors.XtraForm
    {
        public string wc_code = "";
        public DataTable ins_DT = new DataTable();
        public Ins_search_Popup()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            ins_DT.Columns.Add("IT_SCODE",typeof(string));
            ins_DT.Columns.Add("IT_SNAME", typeof(string));
            ins_DT.Columns.Add("IN_SQUTY", typeof(string));
            ins_DT.Columns.Add("TOTAL_SQUTY", typeof(string));

        
            DateTime dt = DateTime.Now;
            if(dt.Hour>=0 && dt.Hour<8)
            {
                days = days - 1;
            }
            date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
            lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");

            
            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            worker.RunWorkerAsync();
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;

                ins_DT = getdata();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = ins_DT;

            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        private void btn_date_Click(object sender, EventArgs e)
        {
            try
            {
                ins_DT.Rows.Clear();

                if (sender == btn_prev_date)
                {
                    days = days - 1;
                }
                else if (sender == btn_next_date)
                {
                    days = days + 1;
                }
                date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
            
        }
        private DataTable getdata()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("USP_INS_QTY_TOTAL", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                //da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
                da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);                
                da.SelectCommand.Parameters.AddWithValue("@SDATE", date_reg);
                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "INS_DATATABLE");

                dt = ds.Tables["INS_DATATABLE"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       


    }
}
