﻿namespace DK_Tablet.Popup
{
    partial class fail_search_popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.RegressionLine regressionLine1 = new DevExpress.XtraCharts.RegressionLine();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            this.chart = new DevExpress.XtraCharts.ChartControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btn_exit = new DevExpress.XtraEditors.SimpleButton();
            this.btn_fail_detail = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FT_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btn_next = new DevExpress.XtraEditors.SimpleButton();
            this.txt_title = new DevExpress.XtraEditors.LabelControl();
            this.btn_prev = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(regressionLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.Cursor = System.Windows.Forms.Cursors.Default;
            xyDiagram1.AxisX.GridLines.Visible = true;
            xyDiagram1.AxisX.Label.Angle = 2;
            xyDiagram1.AxisX.Label.Font = new System.Drawing.Font("Tahoma", 15F);
            xyDiagram1.AxisX.Label.ResolveOverlappingOptions.MinIndent = 3;
            xyDiagram1.AxisX.MinorCount = 1;
            xyDiagram1.AxisX.NumericScaleOptions.AutoGrid = false;
            xyDiagram1.AxisX.NumericScaleOptions.GridOffset = 1D;
            xyDiagram1.AxisX.Tickmarks.Length = 1;
            xyDiagram1.AxisX.Tickmarks.MinorLength = 1;
            xyDiagram1.AxisX.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Label.Font = new System.Drawing.Font("Tahoma", 15F);
            xyDiagram1.AxisY.NumericScaleOptions.AutoGrid = false;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chart.Diagram = xyDiagram1;
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.LeftOutside;
            this.chart.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center;
            this.chart.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chart.Location = new System.Drawing.Point(0, 0);
            this.chart.Name = "chart";
            pointSeriesLabel1.Border.Thickness = 3;
            pointSeriesLabel1.Border.Visibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel1.Font = new System.Drawing.Font("Tahoma", 12F);
            pointSeriesLabel1.LineLength = 20;
            pointSeriesLabel1.LineStyle.Thickness = 3;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pointSeriesLabel1.MaxWidth = 100;
            pointSeriesLabel1.Shadow.Size = 8;
            pointSeriesLabel1.Shadow.Visible = true;
            series1.Label = pointSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.Name = "Series1";
            lineSeriesView1.Color = System.Drawing.Color.LightCoral;
            regressionLine1.Name = "Regression Line 1";
            lineSeriesView1.Indicators.AddRange(new DevExpress.XtraCharts.Indicator[] {
            regressionLine1});
            lineSeriesView1.LineMarkerOptions.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            lineSeriesView1.LineMarkerOptions.Size = 8;
            lineSeriesView1.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.View = lineSeriesView1;
            this.chart.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.chart.SeriesTemplate.View = lineSeriesView2;
            this.chart.Size = new System.Drawing.Size(1280, 290);
            this.chart.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.btn_exit);
            this.splitContainerControl1.Panel1.Controls.Add(this.btn_fail_detail);
            this.splitContainerControl1.Panel1.Controls.Add(this.chart);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1280, 639);
            this.splitContainerControl1.SplitterPosition = 344;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.btn_exit.Appearance.Options.UseFont = true;
            this.btn_exit.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_exit.Location = new System.Drawing.Point(1006, 12);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(128, 52);
            this.btn_exit.TabIndex = 0;
            this.btn_exit.Text = "닫기";
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // btn_fail_detail
            // 
            this.btn_fail_detail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_fail_detail.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.btn_fail_detail.Appearance.Options.UseFont = true;
            this.btn_fail_detail.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_fail_detail.Location = new System.Drawing.Point(1140, 12);
            this.btn_fail_detail.Name = "btn_fail_detail";
            this.btn_fail_detail.Size = new System.Drawing.Size(128, 52);
            this.btn_fail_detail.TabIndex = 0;
            this.btn_fail_detail.Text = "펼치기";
            this.btn_fail_detail.Click += new System.EventHandler(this.btn_fail_detail_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1280, 344);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 12F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NUMB,
            this.FT_NAME,
            this.D1,
            this.D2,
            this.D3,
            this.D4,
            this.D5,
            this.D6,
            this.D7,
            this.D8,
            this.D9,
            this.D10,
            this.D11,
            this.D12,
            this.D13,
            this.D14,
            this.D15,
            this.D16,
            this.D17,
            this.D18,
            this.D19,
            this.D20,
            this.D21,
            this.D22,
            this.D23,
            this.D24,
            this.D25,
            this.D26,
            this.D27,
            this.D28,
            this.D29,
            this.D30,
            this.D31});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // NUMB
            // 
            this.NUMB.Caption = "NUMB";
            this.NUMB.FieldName = "NUMB";
            this.NUMB.Name = "NUMB";
            // 
            // FT_NAME
            // 
            this.FT_NAME.AppearanceCell.Options.UseTextOptions = true;
            this.FT_NAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.FT_NAME.Caption = "불량명";
            this.FT_NAME.FieldName = "FT_NAME";
            this.FT_NAME.Name = "FT_NAME";
            this.FT_NAME.Visible = true;
            this.FT_NAME.VisibleIndex = 0;
            this.FT_NAME.Width = 64;
            // 
            // D1
            // 
            this.D1.Caption = "1";
            this.D1.FieldName = "D1";
            this.D1.Name = "D1";
            this.D1.Visible = true;
            this.D1.VisibleIndex = 1;
            this.D1.Width = 25;
            // 
            // D2
            // 
            this.D2.Caption = "2";
            this.D2.FieldName = "D2";
            this.D2.Name = "D2";
            this.D2.Visible = true;
            this.D2.VisibleIndex = 2;
            this.D2.Width = 25;
            // 
            // D3
            // 
            this.D3.Caption = "3";
            this.D3.FieldName = "D3";
            this.D3.Name = "D3";
            this.D3.Visible = true;
            this.D3.VisibleIndex = 3;
            this.D3.Width = 25;
            // 
            // D4
            // 
            this.D4.Caption = "4";
            this.D4.FieldName = "D4";
            this.D4.Name = "D4";
            this.D4.Visible = true;
            this.D4.VisibleIndex = 4;
            this.D4.Width = 25;
            // 
            // D5
            // 
            this.D5.Caption = "5";
            this.D5.FieldName = "D5";
            this.D5.Name = "D5";
            this.D5.Visible = true;
            this.D5.VisibleIndex = 5;
            this.D5.Width = 25;
            // 
            // D6
            // 
            this.D6.Caption = "6";
            this.D6.FieldName = "D6";
            this.D6.Name = "D6";
            this.D6.Visible = true;
            this.D6.VisibleIndex = 6;
            this.D6.Width = 25;
            // 
            // D7
            // 
            this.D7.Caption = "7";
            this.D7.FieldName = "D7";
            this.D7.Name = "D7";
            this.D7.Visible = true;
            this.D7.VisibleIndex = 7;
            this.D7.Width = 25;
            // 
            // D8
            // 
            this.D8.Caption = "8";
            this.D8.FieldName = "D8";
            this.D8.Name = "D8";
            this.D8.Visible = true;
            this.D8.VisibleIndex = 8;
            this.D8.Width = 25;
            // 
            // D9
            // 
            this.D9.Caption = "9";
            this.D9.FieldName = "D9";
            this.D9.Name = "D9";
            this.D9.Visible = true;
            this.D9.VisibleIndex = 9;
            this.D9.Width = 25;
            // 
            // D10
            // 
            this.D10.Caption = "10";
            this.D10.FieldName = "D10";
            this.D10.Name = "D10";
            this.D10.Visible = true;
            this.D10.VisibleIndex = 10;
            this.D10.Width = 25;
            // 
            // D11
            // 
            this.D11.Caption = "11";
            this.D11.FieldName = "D11";
            this.D11.Name = "D11";
            this.D11.Visible = true;
            this.D11.VisibleIndex = 11;
            this.D11.Width = 25;
            // 
            // D12
            // 
            this.D12.Caption = "12";
            this.D12.FieldName = "D12";
            this.D12.Name = "D12";
            this.D12.Visible = true;
            this.D12.VisibleIndex = 12;
            this.D12.Width = 25;
            // 
            // D13
            // 
            this.D13.Caption = "13";
            this.D13.FieldName = "D13";
            this.D13.Name = "D13";
            this.D13.Visible = true;
            this.D13.VisibleIndex = 13;
            this.D13.Width = 25;
            // 
            // D14
            // 
            this.D14.Caption = "14";
            this.D14.FieldName = "D14";
            this.D14.Name = "D14";
            this.D14.Visible = true;
            this.D14.VisibleIndex = 14;
            this.D14.Width = 25;
            // 
            // D15
            // 
            this.D15.Caption = "15";
            this.D15.FieldName = "D15";
            this.D15.Name = "D15";
            this.D15.Visible = true;
            this.D15.VisibleIndex = 15;
            this.D15.Width = 25;
            // 
            // D16
            // 
            this.D16.Caption = "16";
            this.D16.FieldName = "D16";
            this.D16.Name = "D16";
            this.D16.Visible = true;
            this.D16.VisibleIndex = 16;
            this.D16.Width = 25;
            // 
            // D17
            // 
            this.D17.Caption = "17";
            this.D17.FieldName = "D17";
            this.D17.Name = "D17";
            this.D17.Visible = true;
            this.D17.VisibleIndex = 17;
            this.D17.Width = 25;
            // 
            // D18
            // 
            this.D18.Caption = "18";
            this.D18.FieldName = "D18";
            this.D18.Name = "D18";
            this.D18.Visible = true;
            this.D18.VisibleIndex = 18;
            this.D18.Width = 25;
            // 
            // D19
            // 
            this.D19.Caption = "19";
            this.D19.FieldName = "D19";
            this.D19.Name = "D19";
            this.D19.Visible = true;
            this.D19.VisibleIndex = 19;
            this.D19.Width = 25;
            // 
            // D20
            // 
            this.D20.Caption = "20";
            this.D20.FieldName = "D20";
            this.D20.Name = "D20";
            this.D20.Visible = true;
            this.D20.VisibleIndex = 20;
            this.D20.Width = 25;
            // 
            // D21
            // 
            this.D21.Caption = "21";
            this.D21.FieldName = "D21";
            this.D21.Name = "D21";
            this.D21.Visible = true;
            this.D21.VisibleIndex = 21;
            this.D21.Width = 25;
            // 
            // D22
            // 
            this.D22.Caption = "22";
            this.D22.FieldName = "D22";
            this.D22.Name = "D22";
            this.D22.Visible = true;
            this.D22.VisibleIndex = 22;
            this.D22.Width = 25;
            // 
            // D23
            // 
            this.D23.Caption = "23";
            this.D23.FieldName = "D23";
            this.D23.Name = "D23";
            this.D23.Visible = true;
            this.D23.VisibleIndex = 23;
            this.D23.Width = 25;
            // 
            // D24
            // 
            this.D24.Caption = "24";
            this.D24.FieldName = "D24";
            this.D24.Name = "D24";
            this.D24.Visible = true;
            this.D24.VisibleIndex = 24;
            this.D24.Width = 25;
            // 
            // D25
            // 
            this.D25.Caption = "25";
            this.D25.FieldName = "D25";
            this.D25.Name = "D25";
            this.D25.Visible = true;
            this.D25.VisibleIndex = 25;
            this.D25.Width = 25;
            // 
            // D26
            // 
            this.D26.Caption = "26";
            this.D26.FieldName = "D26";
            this.D26.Name = "D26";
            this.D26.Visible = true;
            this.D26.VisibleIndex = 26;
            this.D26.Width = 25;
            // 
            // D27
            // 
            this.D27.Caption = "27";
            this.D27.FieldName = "D27";
            this.D27.Name = "D27";
            this.D27.Visible = true;
            this.D27.VisibleIndex = 27;
            this.D27.Width = 25;
            // 
            // D28
            // 
            this.D28.Caption = "28";
            this.D28.FieldName = "D28";
            this.D28.Name = "D28";
            this.D28.Visible = true;
            this.D28.VisibleIndex = 28;
            this.D28.Width = 25;
            // 
            // D29
            // 
            this.D29.Caption = "29";
            this.D29.FieldName = "D29";
            this.D29.Name = "D29";
            this.D29.Visible = true;
            this.D29.VisibleIndex = 29;
            this.D29.Width = 25;
            // 
            // D30
            // 
            this.D30.Caption = "30";
            this.D30.FieldName = "D30";
            this.D30.Name = "D30";
            this.D30.Visible = true;
            this.D30.VisibleIndex = 30;
            this.D30.Width = 25;
            // 
            // D31
            // 
            this.D31.Caption = "31";
            this.D31.FieldName = "D31";
            this.D31.Name = "D31";
            this.D31.Visible = true;
            this.D31.VisibleIndex = 31;
            this.D31.Width = 67;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.layoutControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1280, 722);
            this.splitContainerControl2.SplitterPosition = 78;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btn_next);
            this.layoutControl1.Controls.Add(this.txt_title);
            this.layoutControl1.Controls.Add(this.btn_prev);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1280, 78);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btn_next
            // 
            this.btn_next.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.btn_next.Appearance.Options.UseFont = true;
            this.btn_next.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_next.Location = new System.Drawing.Point(1141, 2);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(137, 74);
            this.btn_next.StyleController = this.layoutControl1;
            this.btn_next.TabIndex = 0;
            this.btn_next.Text = "▶";
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // txt_title
            // 
            this.txt_title.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.txt_title.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_title.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt_title.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.txt_title.Location = new System.Drawing.Point(151, 2);
            this.txt_title.Name = "txt_title";
            this.txt_title.Size = new System.Drawing.Size(986, 72);
            this.txt_title.StyleController = this.layoutControl1;
            this.txt_title.TabIndex = 0;
            this.txt_title.Text = "labelControl1";
            // 
            // btn_prev
            // 
            this.btn_prev.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.btn_prev.Appearance.Options.UseFont = true;
            this.btn_prev.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn_prev.Location = new System.Drawing.Point(2, 2);
            this.btn_prev.Name = "btn_prev";
            this.btn_prev.Size = new System.Drawing.Size(145, 74);
            this.btn_prev.StyleController = this.layoutControl1;
            this.btn_prev.TabIndex = 0;
            this.btn_prev.Text = "◀";
            this.btn_prev.Click += new System.EventHandler(this.btn_prev_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1280, 78);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btn_prev;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(48, 51);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(149, 78);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt_title;
            this.layoutControlItem2.Location = new System.Drawing.Point(149, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(990, 78);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btn_next;
            this.layoutControlItem3.Location = new System.Drawing.Point(1139, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(48, 51);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(141, 78);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // fail_search_popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1280, 722);
            this.Controls.Add(this.splitContainerControl2);
            this.Name = "fail_search_popup";
            this.Text = "fail_search_popup";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fail_search_popup_Load);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(regressionLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl chart;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btn_fail_detail;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn NUMB;
        private DevExpress.XtraGrid.Columns.GridColumn FT_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn D1;
        private DevExpress.XtraGrid.Columns.GridColumn D2;
        private DevExpress.XtraGrid.Columns.GridColumn D3;
        private DevExpress.XtraGrid.Columns.GridColumn D4;
        private DevExpress.XtraGrid.Columns.GridColumn D5;
        private DevExpress.XtraGrid.Columns.GridColumn D6;
        private DevExpress.XtraGrid.Columns.GridColumn D7;
        private DevExpress.XtraGrid.Columns.GridColumn D8;
        private DevExpress.XtraGrid.Columns.GridColumn D9;
        private DevExpress.XtraGrid.Columns.GridColumn D10;
        private DevExpress.XtraGrid.Columns.GridColumn D11;
        private DevExpress.XtraGrid.Columns.GridColumn D12;
        private DevExpress.XtraGrid.Columns.GridColumn D13;
        private DevExpress.XtraGrid.Columns.GridColumn D14;
        private DevExpress.XtraGrid.Columns.GridColumn D15;
        private DevExpress.XtraGrid.Columns.GridColumn D16;
        private DevExpress.XtraGrid.Columns.GridColumn D17;
        private DevExpress.XtraGrid.Columns.GridColumn D18;
        private DevExpress.XtraGrid.Columns.GridColumn D19;
        private DevExpress.XtraGrid.Columns.GridColumn D20;
        private DevExpress.XtraGrid.Columns.GridColumn D21;
        private DevExpress.XtraGrid.Columns.GridColumn D22;
        private DevExpress.XtraGrid.Columns.GridColumn D23;
        private DevExpress.XtraGrid.Columns.GridColumn D24;
        private DevExpress.XtraGrid.Columns.GridColumn D25;
        private DevExpress.XtraGrid.Columns.GridColumn D26;
        private DevExpress.XtraGrid.Columns.GridColumn D27;
        private DevExpress.XtraGrid.Columns.GridColumn D28;
        private DevExpress.XtraGrid.Columns.GridColumn D29;
        private DevExpress.XtraGrid.Columns.GridColumn D30;
        private DevExpress.XtraGrid.Columns.GridColumn D31;
        private DevExpress.XtraEditors.SimpleButton btn_exit;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.LabelControl txt_title;
        private DevExpress.XtraEditors.SimpleButton btn_next;
        private DevExpress.XtraEditors.SimpleButton btn_prev;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;





    }
}