﻿using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Work_input_Popup : Form
    {
        public DataTable po_DT = new DataTable();
        public int count = 0;
        public int max_count = 0;
        public string wc_group = "";
        public string str_good_sqty = "0";
        public DataTable Fail_dt = new DataTable();
        public Work_input_Popup()
        {
            InitializeComponent();
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            var senderGrid = (GridView)sender;
            if (e.Column.ToString().Equals("양품"))
            {
                
                Sqty_popup Sqty_popup = new Sqty_popup();
                Sqty_popup.sqty = e.CellValue.ToString();

                if (Sqty_popup.ShowDialog() == DialogResult.OK)
                {
                    if ((int.Parse(Sqty_popup.sqty.ToString()) + int.Parse(gridView1.GetFocusedRowCellValue("FAIL_SQTY").ToString())) > count)
                    {
                        MessageBox.Show("양품과 불량을 합한 수량이 카운트 수량 보다 클 수 없습니다.");
                        return;
                    }
                    senderGrid.SetFocusedRowCellValue(e.Column,Sqty_popup.sqty);
                }
            }
            
        }

        private void Work_input_Popup_Load(object sender, EventArgs e)
        {
            Fail_dt.Columns.Add("FAIL_TYPE", typeof(string));
            Fail_dt.Columns.Add("FAIL_NAME", typeof(string));
            Fail_dt.Columns.Add("FAIL_QTY", typeof(int));

            txt_cnt.Text = count.ToString();
            gridControl1.DataSource = po_DT;
            Fail_dt = Fail_getdata();
            gridControl2.DataSource = Fail_dt;
            //블량 기준정보 가져오기
        }

        private void gridView2_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            var senderGrid = (GridView)sender;
            var base_sqty = e.CellValue;

            if (e.Column.ToString().Equals("불량수량"))
            {
                Sqty_popup Sqty_popup = new Sqty_popup();
                Sqty_popup.sqty = e.CellValue.ToString();
                if (Sqty_popup.ShowDialog() == DialogResult.OK)
                {
                    senderGrid.SetFocusedRowCellValue(e.Column, Sqty_popup.sqty);                    
                }
                
            }
            int sum = 0;
            for (int i=0;i<gridView2.RowCount;i++ )
            {
                sum = sum + int.Parse(gridView2.GetRowCellValue(i, "FAIL_QTY").ToString());
            }
            if ((int.Parse(gridView1.GetFocusedRowCellValue("GOOD_SQTY").ToString()) + sum) > int.Parse(txt_cnt.Text.Trim()))
            {
                MessageBox.Show("양품과 불량을 합한 수량이 카운트 수량보다 많을 수 없습니다.");
                
                senderGrid.SetFocusedRowCellValue(e.Column, base_sqty);     
                return;
            }
            gridView1.SetFocusedRowCellValue("FAIL_SQTY",sum);
        }
        private DataTable Fail_getdata()
        {

            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            string sql = "";
            sql = "SELECT DISTINCT B.NUMB AS FAIL_TYPE,B.FT_NAME AS FAIL_NAME ,CAST('0' AS INT) AS FAIL_QTY FROM FAIL_TYPE_ROUTING A";
            sql = sql + " LEFT JOIN FAIL_TYPE B ON A.NUMB = B.NUMB";
            sql = sql + " WHERE WC_GROUP='" + wc_group + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            try
            {
                
                da.SelectCommand.CommandType = CommandType.Text;

                
                DataSet ds = new DataSet();
                da.Fill(ds, "FAIL_TYPE");

                dt = ds.Tables["FAIL_TYPE"];

                //gridControl1.DataSource = dt;
                //gridControl1.Refresh();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            str_good_sqty = gridView1.GetFocusedRowCellValue("GOOD_SQTY").ToString();
            DialogResult = DialogResult.OK;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

    }
}
