﻿using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class Dt_Input : Form
    {
        public string dt_stime { get; set; }
        public string dt_etime { get; set; }
        public string dt_code { get; set; }
        public string wc_group { get; set; }
        public string PP_SITE_CODE_str{get;set;}
        public string mo_snumb { get; set; }
        public string str_wc_code{get;set;}
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        GET_DATA GET_DATA = new GET_DATA();
        public Dt_Input()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void Dt_Input_Load(object sender, EventArgs e)
        {
            set_btn();
        }
        public void set_btn()
        {
            splashScreenManager1.ShowWaitForm();
            string strCon;
            //string datetime = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT DISTINCT A.DT_CODE,B.DT_NAME FROM DT_MASTER_ROUTING A";
            sql = sql + " LEFT JOIN DT_MASTER B ON A.DT_CODE = B.DT_CODE";
            sql = sql + " WHERE WC_GROUP='" + wc_group + "' ";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();
            int cnt = 1;
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Button btn = new Button();
                    btn.Tag = reader["DT_CODE"].ToString();
                    btn.Name = btn + reader["DT_CODE"].ToString();
                    string name = reader["DT_NAME"].ToString().Trim();
                    if (name.Length > 3)
                    {
                        btn.Text = name.Substring(0, 2) + "\r\n" + name.Substring(2, name.Length-2);
                    }
                    else 
                    { 
                        btn.Text = reader["DT_NAME"].ToString().Trim();
                    }
                    btn.Dock = DockStyle.Fill;
                    btn.TextAlign = ContentAlignment.MiddleCenter;
                    btn.Margin = new System.Windows.Forms.Padding(2);
                    //btn.UseVisualStyleBackColor = true;
                    btn.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
                    btn.Click += new System.EventHandler(this.btn_dt_input);
                    int set_rowcount = 0;
                    if (cnt == 7 || cnt == 13 || cnt == 19 || cnt == 25)
                    {
                        if (cnt % 6 != 0)
                        {
                            set_rowcount = (cnt / 6) + 1;
                            tableLayoutPanel2.RowCount = set_rowcount;
                            this.tableLayoutPanel2.Size = new System.Drawing.Size(679, 84 * set_rowcount);
                            for (int i = 0; i < set_rowcount; i++)
                            {
                                tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
                                this.ClientSize = new System.Drawing.Size(700, 77 + (82 * set_rowcount));
                            }
                        }

                    }
                    tableLayoutPanel2.Controls.Add(btn);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                splashScreenManager1.CloseWaitForm();
            }
        }
        private void btn_dt_input(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            /*
            Dt_Input_select_display Dt_Input_select_display = new Popup.Dt_Input_select_display();
            if (Dt_Input_select_display.ShowDialog() == DialogResult.OK)
            {
                if (Dt_Input_select_display.press_btn)
                {
            */
                    string date_now = GET_DATA.get_time_now();


                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str, mo_snumb, btn.Tag.ToString(), date_now, "", str_wc_code);
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, "", btn.Tag.ToString(), "2");
                    Dt_Input_Start Dt_Input_Start = new Dt_Input_Start();
                    Dt_Input_Start.sdate = date_now;
                    Dt_Input_Start.dt_name = btn.Text.Replace("\n","").Replace("\r","");
                    if (Dt_Input_Start.ShowDialog() == DialogResult.OK)
                    {
                        
                        SUB_SAVE.dt_input_data_update(PP_SITE_CODE_str, mo_snumb, btn.Tag.ToString(), date_now, str_wc_code);
                        SUB_SAVE.작업시작시간_업데이트(str_wc_code, "","", "3");
                        DialogResult = DialogResult.Cancel;
                    }
            /*
                }
                else
                {
                    Dt_Input_time Dt_Input_time = new Dt_Input_time();
                    Dt_Input_time.dt_name = btn.Text.Replace("\n", "").Replace("\r", "");
                    if (Dt_Input_time.ShowDialog() == DialogResult.OK)
                    {
                        dt_stime = Dt_Input_time.dt_stime;
                        dt_etime = Dt_Input_time.dt_etime;
                        dt_code = btn.Tag.ToString();
                        DialogResult = DialogResult.OK;
                    }
                }
            }
            */
 
            

        }
    }
}
