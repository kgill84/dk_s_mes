﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Dt_Input_Start : Form
    {
        public string sdate { get; set; }
        public string dt_name { get; set; }
        public Dt_Input_Start()
        {
            InitializeComponent();
        }

        private void Dt_Input_Start_Load(object sender, EventArgs e)
        {
            txt_stime.Text = sdate;
            txtDt_name.Text = dt_name;
            labelControl2.Focus();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
