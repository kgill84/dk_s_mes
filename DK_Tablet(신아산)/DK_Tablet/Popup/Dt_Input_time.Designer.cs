﻿namespace DK_Tablet.Popup
{
    partial class Dt_Input_time
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.te_time_s = new DevExpress.XtraEditors.TimeEdit();
            this.dteDt_stime = new DevExpress.XtraEditors.DateEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.te_time_e = new DevExpress.XtraEditors.TimeEdit();
            this.dteDt_etime = new DevExpress.XtraEditors.DateEdit();
            this.lblDt_name = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.te_time_s.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_stime.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_stime.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.te_time_e.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_etime.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_etime.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Cornsilk;
            this.panel1.Controls.Add(this.te_time_s);
            this.panel1.Controls.Add(this.dteDt_stime);
            this.panel1.Location = new System.Drawing.Point(5, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 112);
            this.panel1.TabIndex = 2;
            // 
            // te_time_s
            // 
            this.te_time_s.EditValue = new System.DateTime(2015, 1, 12, 0, 0, 0, 0);
            this.te_time_s.Location = new System.Drawing.Point(3, 63);
            this.te_time_s.Name = "te_time_s";
            this.te_time_s.Properties.Appearance.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.te_time_s.Properties.Appearance.Options.UseFont = true;
            this.te_time_s.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.te_time_s.Properties.Mask.EditMask = "t";
            this.te_time_s.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.te_time_s.Size = new System.Drawing.Size(231, 38);
            this.te_time_s.TabIndex = 7;
            // 
            // dteDt_stime
            // 
            this.dteDt_stime.EditValue = new System.DateTime(2015, 1, 12, 15, 16, 19, 372);
            this.dteDt_stime.Location = new System.Drawing.Point(2, 8);
            this.dteDt_stime.Name = "dteDt_stime";
            this.dteDt_stime.Properties.Appearance.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dteDt_stime.Properties.Appearance.Options.UseFont = true;
            this.dteDt_stime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteDt_stime.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteDt_stime.Properties.CalendarTimeProperties.EditFormat.FormatString = "yyyyMMdd";
            this.dteDt_stime.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dteDt_stime.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dteDt_stime.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteDt_stime.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dteDt_stime.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteDt_stime.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dteDt_stime.Size = new System.Drawing.Size(232, 38);
            this.dteDt_stime.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Cornsilk;
            this.panel2.Controls.Add(this.te_time_e);
            this.panel2.Controls.Add(this.dteDt_etime);
            this.panel2.Location = new System.Drawing.Point(284, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(245, 112);
            this.panel2.TabIndex = 2;
            // 
            // te_time_e
            // 
            this.te_time_e.EditValue = new System.DateTime(2015, 1, 12, 0, 0, 0, 0);
            this.te_time_e.Location = new System.Drawing.Point(3, 63);
            this.te_time_e.Name = "te_time_e";
            this.te_time_e.Properties.Appearance.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.te_time_e.Properties.Appearance.Options.UseFont = true;
            this.te_time_e.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.te_time_e.Properties.Mask.EditMask = "t";
            this.te_time_e.Properties.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.TouchUI;
            this.te_time_e.Size = new System.Drawing.Size(237, 38);
            this.te_time_e.TabIndex = 7;
            // 
            // dteDt_etime
            // 
            this.dteDt_etime.EditValue = new System.DateTime(2015, 1, 12, 15, 16, 19, 372);
            this.dteDt_etime.Location = new System.Drawing.Point(3, 8);
            this.dteDt_etime.Name = "dteDt_etime";
            this.dteDt_etime.Properties.Appearance.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dteDt_etime.Properties.Appearance.Options.UseFont = true;
            this.dteDt_etime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteDt_etime.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dteDt_etime.Properties.CalendarTimeProperties.EditFormat.FormatString = "yyyyMMdd";
            this.dteDt_etime.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.dteDt_etime.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dteDt_etime.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteDt_etime.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dteDt_etime.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dteDt_etime.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.dteDt_etime.Size = new System.Drawing.Size(237, 38);
            this.dteDt_etime.TabIndex = 6;
            // 
            // lblDt_name
            // 
            this.lblDt_name.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Bold);
            this.lblDt_name.Location = new System.Drawing.Point(15, 9);
            this.lblDt_name.Name = "lblDt_name";
            this.lblDt_name.Size = new System.Drawing.Size(514, 55);
            this.lblDt_name.TabIndex = 3;
            this.lblDt_name.Text = "label3";
            this.lblDt_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.Font = new System.Drawing.Font("굴림", 22F);
            this.btnSave.Location = new System.Drawing.Point(168, 135);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(185, 70);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "등록";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("굴림", 20F);
            this.label3.Location = new System.Drawing.Point(249, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 27);
            this.label3.TabIndex = 2;
            this.label3.Text = "~";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SeaShell;
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Location = new System.Drawing.Point(5, 67);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(532, 208);
            this.panel3.TabIndex = 5;
            // 
            // Dt_Input_time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(549, 287);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblDt_name);
            this.Name = "Dt_Input_time";
            this.Text = "시간 입력";
            this.Load += new System.EventHandler(this.Dt_Input_time_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.te_time_s.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_stime.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_stime.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.te_time_e.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_etime.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dteDt_etime.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblDt_name;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.DateEdit dteDt_stime;
        private DevExpress.XtraEditors.DateEdit dteDt_etime;
        private DevExpress.XtraEditors.TimeEdit te_time_s;
        private DevExpress.XtraEditors.TimeEdit te_time_e;
    }
}