﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Fail_Sqty_popup : Form
    {
        public string sqty { get; set; }
        public string fail_name { get; set; }
        public Fail_Sqty_popup()
        {
            InitializeComponent();
        }

        private void btn_up_Click(object sender, EventArgs e)
        {            
            txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString())+ 1).ToString();
        }

        private void btn_down_Click(object sender, EventArgs e)
        {
            if ((int.Parse(txt_sqty.EditValue.ToString()) - 1) < 0)
            {
                txt_sqty.EditValue = "0";
            }
            else
            {
                txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) - 1).ToString();
            }

        }

        private void btn_up_5_Click(object sender, EventArgs e)
        {
            txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) + 5).ToString();
        }

        private void btn_down_5_Click(object sender, EventArgs e)
        {
            if ((int.Parse(txt_sqty.EditValue.ToString()) - 5) < 0)
            {
                txt_sqty.EditValue = "0";
            }
            else
            {
                txt_sqty.EditValue = (int.Parse(txt_sqty.EditValue.ToString()) - 5).ToString();
            }
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            if (txt_sqty.EditValue.ToString() == "0")
            {
                MessageBox.Show("불량수량이 0 보다 커야 합니다.");
                return;
            }
            sqty = txt_sqty.EditValue.ToString();
            DialogResult = DialogResult.OK;
        }

        private void Sqty_popup_Load(object sender, EventArgs e)
        {
            txt_sqty.EditValue = sqty;
            txtFail_name.Text = fail_name;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}
