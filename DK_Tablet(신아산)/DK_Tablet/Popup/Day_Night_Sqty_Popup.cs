﻿using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Day_Night_Sqty_Popup : Form
    {
        GET_DATA GET_DATA = new GET_DATA();
        public string wc_code { get; set; }
        DateTime dt;
        public Day_Night_Sqty_Popup()
        {
            InitializeComponent();
        }

        private void btn_prev_Click(object sender, EventArgs e)
        {

            dt = dt.AddDays(-1);
            date_p.DateTime = dt;
            getData();
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            dt = dt.AddDays(+1);
            date_p.DateTime = dt;
            getData();
        }

        private void Day_Night_Sqty_Popup_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            dt = DateTime.Now;
            date_p.DateTime = dt;
            getData();
        }
        public void getData()
        {
            string gubn="";
            if ((bool)ts.EditValue)
            {
                gubn = "2";
            }
            else
            {
                gubn = "1";
            }

            gridControl1.DataSource = GET_DATA.GetDataDay_Sqty(date_p.DateTime.ToString("yyyyMMdd"), wc_code, gubn);
        }

        private void ts_Toggled(object sender, EventArgs e)
        {
            //MessageBox.Show(ts.EditValue.ToString());
            getData();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
