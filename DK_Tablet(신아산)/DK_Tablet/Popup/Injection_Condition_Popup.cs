﻿using DevExpress.XtraEditors;
using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class Injection_Condition_Popup : Form
    {
        public string it_scode { get; set; }
        public string it_sname { get; set; }

        public string wc_code { get; set; }
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();
        public Injection_Condition_Popup()
        {
            InitializeComponent();
        }
        decimal TEMP_NOZZLE1 = 0, TEMP_NOZZLE2 = 0, TEMP_CYLINDER1 = 0, TEMP_CYLINDER2 = 0, TEMP_CYLINDER3 = 0, TEMP_CYLINDER4 = 0, TEMP_CYLINDER5 = 0, TEMP_CYLINDER6 = 0;
        decimal PRESSURE_INJ1 = 0, PRESSURE_INJ2 = 0, PRE_MOLD_SPEED = 0, MOLD_PRESSURE = 0;
        decimal CHARGE_PRE = 0, CHARGE_SPEED = 0, CHARGE_DISTANCE = 0, CHARGE_PREVENT = 0;
        decimal SPEED_INJ1 = 0, SPEED_INJ2 = 0, SPEED_INJ3 = 0;
        decimal TIME_INJ = 0, TIME_COOLING = 0, TIME_TOTAL = 0;
        decimal VVGATE_DELAY1 = 0, VVGATE_DELAY2 = 0, VVGATE_DELAY3 = 0, VVGATE_DELAY4 = 0, VVGATE_DELAY5 = 0;
        decimal VVGATE_DELAY6 = 0, VVGATE_DELAY7 = 0, VVGATE_DELAY8 = 0, VVGATE_DELAY9 = 0, VVGATE_DELAY10 = 0;
        decimal VVGATE_DELAY11 = 0, VVGATE_DELAY12 = 0, VVGATE_DELAY13 = 0, VVGATE_DELAY14 = 0, VVGATE_DELAY15 = 0;
        decimal VVGATE_OPEN1 = 0, VVGATE_OPEN2 = 0, VVGATE_OPEN3 = 0, VVGATE_OPEN4 = 0, VVGATE_OPEN5 = 0;
        decimal VVGATE_OPEN6 = 0, VVGATE_OPEN7 = 0, VVGATE_OPEN8 = 0, VVGATE_OPEN9 = 0, VVGATE_OPEN10 = 0;
        decimal VVGATE_OPEN11 = 0, VVGATE_OPEN12 = 0, VVGATE_OPEN13 = 0, VVGATE_OPEN14 = 0, VVGATE_OPEN15 = 0;
        Button btn;
        

        private void btn_save_Click(object sender, EventArgs e)
        {
            // 온도
            TEMP_NOZZLE1 = txtTEMP_NOZZLE1.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_NOZZLE1.Text);
            TEMP_NOZZLE2 = txtTEMP_NOZZLE2.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_NOZZLE2.Text);
            TEMP_CYLINDER1 = txtTEMP_CYLINDER1.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER1.Text);
            TEMP_CYLINDER2 = txtTEMP_CYLINDER2.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER2.Text);
            TEMP_CYLINDER3 = txtTEMP_CYLINDER3.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER3.Text);
            TEMP_CYLINDER4 = txtTEMP_CYLINDER4.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER4.Text);
            TEMP_CYLINDER5 = txtTEMP_CYLINDER5.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER5.Text);
            TEMP_CYLINDER6 = txtTEMP_CYLINDER6.Text.Trim() == "" ? 0 : decimal.Parse(txtTEMP_CYLINDER6.Text);

            // 속도
            SPEED_INJ1 = txtSPEED_INJ1.Text.Trim() == "" ? 0 : decimal.Parse(txtSPEED_INJ1.Text);
            SPEED_INJ2 = txtSPEED_INJ2.Text.Trim() == "" ? 0 : decimal.Parse(txtSPEED_INJ2.Text);
            SPEED_INJ3 = txtSPEED_INJ3.Text.Trim() == "" ? 0 : decimal.Parse(txtSPEED_INJ3.Text);

            // 압력
            PRESSURE_INJ1 = txtPRESSURE_INJ1.Text.Trim() == "" ? 0 : decimal.Parse(txtPRESSURE_INJ1.Text);
            PRESSURE_INJ2 = txtPRESSURE_INJ2.Text.Trim() == "" ? 0 : decimal.Parse(txtPRESSURE_INJ2.Text);
            PRE_MOLD_SPEED = txtPRE_MOLD_SPEED.Text.Trim() == "" ? 0 : decimal.Parse(txtPRE_MOLD_SPEED.Text);
            MOLD_PRESSURE = txtMOLD_PRESSURE.Text.Trim() == "" ? 0 : decimal.Parse(txtMOLD_PRESSURE.Text);
            
            // 시간
            TIME_INJ = txtTIME_INJ.Text.Trim() == "" ? 0 : decimal.Parse(txtTIME_INJ.Text);
            TIME_COOLING = txtTIME_COOLING.Text.Trim() == "" ? 0 : decimal.Parse(txtTIME_COOLING.Text);
            TIME_TOTAL = txtTIME_TOTAL.Text.Trim() == "" ? 0 : decimal.Parse(txtTIME_TOTAL.Text);

            // 계량
            CHARGE_PRE = txtCHARGE_PRE.Text.Trim() == "" ? 0 : decimal.Parse(txtCHARGE_PRE.Text);
            CHARGE_SPEED = txtCHARGE_SPEED.Text.Trim() == "" ? 0 : decimal.Parse(txtCHARGE_SPEED.Text);
            CHARGE_DISTANCE = txtCHARGE_DISTANCE.Text.Trim() == "" ? 0 : decimal.Parse(txtCHARGE_DISTANCE.Text);
            CHARGE_PREVENT = txtCHARGE_PREVENT.Text.Trim() == "" ? 0 : decimal.Parse(txtCHARGE_PREVENT.Text);

            // V/V GATE
            VVGATE_DELAY1 = txtVVGATE_DELAY1.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY1.Text);
            VVGATE_DELAY2 = txtVVGATE_DELAY2.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY2.Text);
            VVGATE_DELAY3 = txtVVGATE_DELAY3.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY3.Text);
            VVGATE_DELAY4 = txtVVGATE_DELAY4.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY4.Text);
            VVGATE_DELAY5 = txtVVGATE_DELAY5.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY5.Text);
            VVGATE_DELAY6 = txtVVGATE_DELAY6.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY6.Text);
            VVGATE_DELAY7 = txtVVGATE_DELAY7.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY7.Text);
            VVGATE_DELAY8 = txtVVGATE_DELAY8.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY8.Text);
            VVGATE_DELAY9 = txtVVGATE_DELAY9.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY9.Text);
            VVGATE_DELAY10 = txtVVGATE_DELAY10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY10.Text);
            VVGATE_DELAY11 = txtVVGATE_DELAY11.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY11.Text);
            VVGATE_DELAY12 = txtVVGATE_DELAY12.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY12.Text);
            VVGATE_DELAY13 = txtVVGATE_DELAY13.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY13.Text);
            VVGATE_DELAY14 = txtVVGATE_DELAY14.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY14.Text);
            VVGATE_DELAY15 = txtVVGATE_DELAY15.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_DELAY15.Text);
            VVGATE_OPEN1 = txtVVGATE_OPEN1.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN1.Text);
            VVGATE_OPEN2 = txtVVGATE_OPEN2.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN2.Text);
            VVGATE_OPEN3 = txtVVGATE_OPEN3.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN3.Text);
            VVGATE_OPEN4 = txtVVGATE_OPEN4.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN4.Text);
            VVGATE_OPEN5 = txtVVGATE_OPEN5.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN5.Text);
            VVGATE_OPEN6 = txtVVGATE_OPEN6.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN6.Text);
            VVGATE_OPEN7 = txtVVGATE_OPEN7.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN7.Text);
            VVGATE_OPEN8 = txtVVGATE_OPEN8.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN8.Text);
            VVGATE_OPEN9 = txtVVGATE_OPEN9.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN9.Text);
            VVGATE_OPEN10 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN10.Text);
            VVGATE_OPEN11 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN11.Text);
            VVGATE_OPEN12 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN12.Text);
            VVGATE_OPEN13 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN13.Text);
            VVGATE_OPEN14 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN14.Text);
            VVGATE_OPEN15 = txtVVGATE_OPEN10.Text.Trim() == "" ? 0 : decimal.Parse(txtVVGATE_OPEN15.Text);
            dbSave();
            DialogResult = DialogResult.OK;
        }
        private void dbSave()
        {

            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlTransaction trans;

            conn.Open();
            trans = conn.BeginTransaction();
            string insert_sql;//품목정보 저장 쿼리
            //기준정보 ===========================================================================================================
            insert_sql = "SP_INJ_CONDI_SAVE_CS";
            //품목정보 커맨드
            SqlCommand cmd = new SqlCommand(insert_sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Transaction = trans;

            //파라미터 셋팅
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);//
            
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//
            cmd.Parameters.AddWithValue("@IT_SNAME", it_sname);//
            cmd.Parameters.AddWithValue("@INJ_CONDITION", "");//
            cmd.Parameters.AddWithValue("@DIVISION", "");//
            
            // 온도
            cmd.Parameters.AddWithValue("@TEMP_NOZZLE1", TEMP_NOZZLE1);//
            cmd.Parameters.AddWithValue("@TEMP_NOZZLE2", TEMP_NOZZLE2);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER1", TEMP_CYLINDER1);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER2", TEMP_CYLINDER2);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER3", TEMP_CYLINDER3);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER4", TEMP_CYLINDER4);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER5", TEMP_CYLINDER5);//
            cmd.Parameters.AddWithValue("@TEMP_CYLINDER6", TEMP_CYLINDER6);//
             // 속도
            cmd.Parameters.AddWithValue("@SPEED_INJ1", SPEED_INJ1);//
            cmd.Parameters.AddWithValue("@SPEED_INJ2", SPEED_INJ2);//
            cmd.Parameters.AddWithValue("@SPEED_INJ3", SPEED_INJ3);//

            // 압력
            cmd.Parameters.AddWithValue("@PRESSURE_INJ1", PRESSURE_INJ1);//
            cmd.Parameters.AddWithValue("@PRESSURE_INJ2", PRESSURE_INJ2);//
            cmd.Parameters.AddWithValue("@PRE_MOLD_SPEED", PRE_MOLD_SPEED);//
            cmd.Parameters.AddWithValue("@MOLD_PRESSURE", MOLD_PRESSURE);//
            // 시간
            cmd.Parameters.AddWithValue("@TIME_INJ", TIME_INJ);//
            cmd.Parameters.AddWithValue("@TIME_COOLING", TIME_COOLING);//
            cmd.Parameters.AddWithValue("@TIME_TOTAL", TIME_TOTAL);//

            // 계량
            cmd.Parameters.AddWithValue("@CHARGE_PRE", CHARGE_PRE);//
            cmd.Parameters.AddWithValue("@CHARGE_SPEED", CHARGE_SPEED);//
            cmd.Parameters.AddWithValue("@CHARGE_DISTANCE", CHARGE_DISTANCE);//
            cmd.Parameters.AddWithValue("@CHARGE_PREVENT", CHARGE_PREVENT);//
            
            // V/V GATE
            cmd.Parameters.AddWithValue("@VVGATE_DELAY1", VVGATE_DELAY1);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY2", VVGATE_DELAY2);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY3", VVGATE_DELAY3);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY4", VVGATE_DELAY4);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY5", VVGATE_DELAY5);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY6", VVGATE_DELAY6);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY7", VVGATE_DELAY7);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY8", VVGATE_DELAY8);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY9", VVGATE_DELAY9);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY10", VVGATE_DELAY10);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY11", VVGATE_DELAY11);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY12", VVGATE_DELAY12);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY13", VVGATE_DELAY13);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY14", VVGATE_DELAY14);//
            cmd.Parameters.AddWithValue("@VVGATE_DELAY15", VVGATE_DELAY15);//

            cmd.Parameters.AddWithValue("@VVGATE_OPEN1", VVGATE_OPEN1);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN2", VVGATE_OPEN2);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN3", VVGATE_OPEN3);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN4", VVGATE_OPEN4);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN5", VVGATE_OPEN5);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN6", VVGATE_OPEN6);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN7", VVGATE_OPEN7);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN8", VVGATE_OPEN8);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN9", VVGATE_OPEN9);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN10", VVGATE_OPEN10);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN11", VVGATE_OPEN11);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN12", VVGATE_OPEN12);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN13", VVGATE_OPEN13);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN14", VVGATE_OPEN14);//
            cmd.Parameters.AddWithValue("@VVGATE_OPEN15", VVGATE_OPEN15);//
            cmd.Parameters.AddWithValue("@LOT_NO", "");//
            cmd.Parameters.AddWithValue("@IN_QTY", "");//

            try
            {
                //품목정보 저장
                cmd.ExecuteNonQuery();
                //생산정보 저장
                
                trans.Commit();
                
                //저장 품목 검색

            }
            catch (SqlException ex)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패" + ex.Message);
            }
            finally
            {

                conn.Close();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string[] str = btn.Name.Split('_');
            if (str[1].Equals("t"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_NOZZLE1.Text = (decimal.Parse(txtTEMP_NOZZLE1.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_NOZZLE1.Text = (decimal.Parse(txtTEMP_NOZZLE1.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER1.Text = (decimal.Parse(txtTEMP_CYLINDER1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER1.Text = (decimal.Parse(txtTEMP_CYLINDER1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER2.Text = (decimal.Parse(txtTEMP_CYLINDER2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER2.Text = (decimal.Parse(txtTEMP_CYLINDER2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER3.Text = (decimal.Parse(txtTEMP_CYLINDER3.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER3.Text = (decimal.Parse(txtTEMP_CYLINDER3.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("5"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER4.Text = (decimal.Parse(txtTEMP_CYLINDER4.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER4.Text = (decimal.Parse(txtTEMP_CYLINDER4.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("6"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER5.Text = (decimal.Parse(txtTEMP_CYLINDER5.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER5.Text = (decimal.Parse(txtTEMP_CYLINDER5.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("7"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER6.Text = (decimal.Parse(txtTEMP_CYLINDER6.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER6.Text = (decimal.Parse(txtTEMP_CYLINDER6.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("8"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_NOZZLE2.Text = (decimal.Parse(txtTEMP_NOZZLE2.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_NOZZLE2.Text = (decimal.Parse(txtTEMP_NOZZLE2.Text.Trim()) - NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("p"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRESSURE_INJ1.Text = (decimal.Parse(txtPRESSURE_INJ1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRESSURE_INJ1.Text = (decimal.Parse(txtPRESSURE_INJ1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRESSURE_INJ2.Text = (decimal.Parse(txtPRESSURE_INJ2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRESSURE_INJ2.Text = (decimal.Parse(txtPRESSURE_INJ2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRE_MOLD_SPEED.Text = (decimal.Parse(txtPRE_MOLD_SPEED.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRE_MOLD_SPEED.Text = (decimal.Parse(txtPRE_MOLD_SPEED.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtMOLD_PRESSURE.Text = (decimal.Parse(txtMOLD_PRESSURE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtMOLD_PRESSURE.Text = (decimal.Parse(txtMOLD_PRESSURE.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("w"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_PRE.Text = (decimal.Parse(txtCHARGE_PRE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_PRE.Text = (decimal.Parse(txtCHARGE_PRE.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_SPEED.Text = (decimal.Parse(txtCHARGE_SPEED.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_SPEED.Text = (decimal.Parse(txtCHARGE_SPEED.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_DISTANCE.Text = (decimal.Parse(txtCHARGE_DISTANCE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_DISTANCE.Text = (decimal.Parse(txtCHARGE_DISTANCE.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_PREVENT.Text = (decimal.Parse(txtCHARGE_PREVENT.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_PREVENT.Text = (decimal.Parse(txtCHARGE_PREVENT.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("s"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ1.Text = (decimal.Parse(txtSPEED_INJ1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ1.Text = (decimal.Parse(txtSPEED_INJ1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ2.Text = (decimal.Parse(txtSPEED_INJ2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ2.Text = (decimal.Parse(txtSPEED_INJ2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ3.Text = (decimal.Parse(txtSPEED_INJ3.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ3.Text = (decimal.Parse(txtSPEED_INJ3.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("h"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_INJ.Text = (decimal.Parse(txtTIME_INJ.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_INJ.Text = (decimal.Parse(txtTIME_INJ.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_COOLING.Text = (decimal.Parse(txtTIME_COOLING.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_COOLING.Text = (decimal.Parse(txtTIME_COOLING.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_TOTAL.Text = (decimal.Parse(txtTIME_TOTAL.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_TOTAL.Text = (decimal.Parse(txtTIME_TOTAL.Text.Trim()) -NUM).ToString();
                    }
                }
            }
        }
        const decimal NUM = 0.1m;
        

        private void btn_t_1_up_MouseDown(object sender, MouseEventArgs e)
        {

            btn = (Button)sender;
            timer1.Start();
            string[] str = btn.Name.Split('_');
            
            if (str[1].Equals("t"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_NOZZLE1.Text =(decimal.Parse(txtTEMP_NOZZLE1.Text.Trim()) + NUM).ToString();
                        
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_NOZZLE1.Text = (decimal.Parse(txtTEMP_NOZZLE1.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER1.Text = (decimal.Parse(txtTEMP_CYLINDER1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER1.Text = (decimal.Parse(txtTEMP_CYLINDER1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER2.Text = (decimal.Parse(txtTEMP_CYLINDER2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER2.Text = (decimal.Parse(txtTEMP_CYLINDER2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER3.Text = (decimal.Parse(txtTEMP_CYLINDER3.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER3.Text = (decimal.Parse(txtTEMP_CYLINDER3.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("5"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER4.Text = (decimal.Parse(txtTEMP_CYLINDER4.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER4.Text = (decimal.Parse(txtTEMP_CYLINDER4.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("6"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER5.Text = (decimal.Parse(txtTEMP_CYLINDER5.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER5.Text = (decimal.Parse(txtTEMP_CYLINDER5.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("7"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_CYLINDER6.Text = (decimal.Parse(txtTEMP_CYLINDER6.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_CYLINDER6.Text = (decimal.Parse(txtTEMP_CYLINDER6.Text.Trim()) - NUM).ToString();
                    }
                }
                else if (str[2].Equals("8"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTEMP_NOZZLE2.Text = (decimal.Parse(txtTEMP_NOZZLE2.Text.Trim()) + NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTEMP_NOZZLE2.Text = (decimal.Parse(txtTEMP_NOZZLE2.Text.Trim()) - NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("p"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRESSURE_INJ1.Text = (decimal.Parse(txtPRESSURE_INJ1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRESSURE_INJ1.Text = (decimal.Parse(txtPRESSURE_INJ1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRESSURE_INJ2.Text = (decimal.Parse(txtPRESSURE_INJ2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRESSURE_INJ2.Text = (decimal.Parse(txtPRESSURE_INJ2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtPRE_MOLD_SPEED.Text = (decimal.Parse(txtPRE_MOLD_SPEED.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtPRE_MOLD_SPEED.Text = (decimal.Parse(txtPRE_MOLD_SPEED.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtMOLD_PRESSURE.Text = (decimal.Parse(txtMOLD_PRESSURE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtMOLD_PRESSURE.Text = (decimal.Parse(txtMOLD_PRESSURE.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("w"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_PRE.Text = (decimal.Parse(txtCHARGE_PRE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_PRE.Text = (decimal.Parse(txtCHARGE_PRE.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_SPEED.Text = (decimal.Parse(txtCHARGE_SPEED.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_SPEED.Text = (decimal.Parse(txtCHARGE_SPEED.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_DISTANCE.Text = (decimal.Parse(txtCHARGE_DISTANCE.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_DISTANCE.Text = (decimal.Parse(txtCHARGE_DISTANCE.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("4"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtCHARGE_PREVENT.Text = (decimal.Parse(txtCHARGE_PREVENT.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtCHARGE_PREVENT.Text = (decimal.Parse(txtCHARGE_PREVENT.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("s"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ1.Text = (decimal.Parse(txtSPEED_INJ1.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ1.Text = (decimal.Parse(txtSPEED_INJ1.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ2.Text = (decimal.Parse(txtSPEED_INJ2.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ2.Text = (decimal.Parse(txtSPEED_INJ2.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtSPEED_INJ3.Text = (decimal.Parse(txtSPEED_INJ3.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtSPEED_INJ3.Text = (decimal.Parse(txtSPEED_INJ3.Text.Trim()) -NUM).ToString();
                    }
                }
            }
            else if (str[1].Equals("h"))
            {
                if (str[2].Equals("1"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_INJ.Text = (decimal.Parse(txtTIME_INJ.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_INJ.Text = (decimal.Parse(txtTIME_INJ.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("2"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_COOLING.Text = (decimal.Parse(txtTIME_COOLING.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_COOLING.Text = (decimal.Parse(txtTIME_COOLING.Text.Trim()) -NUM).ToString();
                    }
                }
                else if (str[2].Equals("3"))
                {
                    if (str[3].Equals("up"))
                    {
                        txtTIME_TOTAL.Text = (decimal.Parse(txtTIME_TOTAL.Text.Trim()) +NUM).ToString();
                    }
                    else if (str[3].Equals("down"))
                    {
                        txtTIME_TOTAL.Text = (decimal.Parse(txtTIME_TOTAL.Text.Trim()) -NUM).ToString();
                    }
                }
            }
        }

        private void btn_t_1_up_MouseUp(object sender, MouseEventArgs e)
        {
            timer1.Stop();
        }

        private void Injection_Condition_Popup_Load(object sender, EventArgs e)
        {
            
                get_inj_condi(it_scode);
            
        }
        public void get_inj_condi(string it_scode)
        {
            string strCon;
            string[] str = new string[2];
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SP_INJ_CONDI_GET_DATA";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        txtTEMP_NOZZLE1.EditValue= reader["TEMP_NOZZLE1"].ToString();
                        txtTEMP_NOZZLE2.EditValue = reader["TEMP_NOZZLE2"].ToString();
                        txtTEMP_CYLINDER1.EditValue = reader["TEMP_CYLINDER1"].ToString();
                        txtTEMP_CYLINDER2.EditValue = reader["TEMP_CYLINDER2"].ToString();
                        txtTEMP_CYLINDER3.EditValue = reader["TEMP_CYLINDER3"].ToString();
                        txtTEMP_CYLINDER4.EditValue = reader["TEMP_CYLINDER4"].ToString();
                        txtTEMP_CYLINDER5.EditValue = reader["TEMP_CYLINDER5"].ToString();
                        txtTEMP_CYLINDER6.EditValue = reader["TEMP_CYLINDER6"].ToString();
                        txtSPEED_INJ1.EditValue = reader["SPEED_INJ1"].ToString();
                        txtSPEED_INJ2.EditValue = reader["SPEED_INJ2"].ToString();
                        txtSPEED_INJ3.EditValue = reader["SPEED_INJ3"].ToString();
                        txtPRESSURE_INJ1.EditValue = reader["PRESSURE_INJ1"].ToString();
                        txtPRESSURE_INJ2.EditValue = reader["PRESSURE_INJ2"].ToString();
                        txtPRE_MOLD_SPEED.EditValue = reader["PRE_MOLD_SPEED"].ToString();
                        txtMOLD_PRESSURE.EditValue = reader["MOLD_PRESSURE"].ToString();
                        txtTIME_INJ.EditValue = reader["TIME_INJ"].ToString();
                        txtTIME_COOLING.EditValue = reader["TIME_COOLING"].ToString();
                        txtTIME_TOTAL.EditValue = reader["TIME_TOTAL"].ToString();
                        txtCHARGE_PRE.EditValue = reader["CHARGE_PRE"].ToString();
                        txtCHARGE_SPEED.EditValue = reader["CHARGE_SPEED"].ToString();
                        txtCHARGE_DISTANCE.EditValue = reader["CHARGE_DISTANCE"].ToString();
                        txtCHARGE_PREVENT.EditValue = reader["CHARGE_PREVENT"].ToString();
		
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            
        }

        private void txt_Click(object sender, EventArgs e)
        {
            TextEdit txt = (TextEdit)sender;
            KeyPad KeyPad = new KeyPad();
            if (KeyPad.ShowDialog() == DialogResult.OK)
            {
                txt.EditValue = KeyPad.txt_value;
            }
        }
    }
}
