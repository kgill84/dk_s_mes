﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DK_Tablet
{
    public partial class PPCard_Search_New : Form
    {
        public DataTable PP_dt { get; set; }
        public DataTable PO_dt { get; set; }
        public string WC_CODE_str {get;set;}
        public string CARD_NO_str {get;set;}
        public string IT_SCODE_str {get;set;}
        public string SIZE_str { get; set; }
        public string SITE_CODE_str { get; set; }
        public string MO_SNUMB_str { get; set; }
        public string ME_SCODE_str { get;set; }

        public string wc_code { get; set; }
        public PPCard_Search_New()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void po_realese_search_Load(object sender, EventArgs e)
        {
            //dataGridView1.DataSource = PO_dt;
            dataGridView2.DataSource = PP_dt;      
        }
        

        private void btn_done_Click(object sender, EventArgs e)
        {
            //int cxtl1 = dataGridView1.CurrentCell.RowIndex;
            int cxtl2 = dataGridView2.CurrentCell.RowIndex;
            if(cxtl2<0)
            {
                MessageBox.Show("PP Card를 선택해주세요");
                return;
            }
            if (dataGridView2.Rows[cxtl2].Cells["CHECK_YN"].Value.ToString() == "생산가능") { 
                WC_CODE_str  = dataGridView2.Rows[cxtl2].Cells["WC_CODE"].Value.ToString();
                CARD_NO_str = dataGridView2.Rows[cxtl2].Cells["CARD_NO"].Value.ToString();
                IT_SCODE_str = dataGridView2.Rows[cxtl2].Cells["IT_SCODE"].Value.ToString();
                SIZE_str = dataGridView2.Rows[cxtl2].Cells["SIZE"].Value.ToString();
                SITE_CODE_str = dataGridView2.Rows[cxtl2].Cells["SITE_CODE"].Value.ToString();
                //MO_SNUMB_str = dataGridView1.Rows[cxtl1].Cells["MO_SNUMB"].Value.ToString();
                //ME_SCODE_str = dataGridView1.Rows[cxtl1].Cells["PO_ME_SCODE"].Value.ToString();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("생산 할 수 없는 PP 카드 입니다.");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
            DataRow[] dr = PP_dt.Select("IT_SCODE='" + dataGridView1.Rows[e.RowIndex].Cells["PO_IT_SCODE"].Value.ToString().Trim() + "'");
            //PP_dt.Clear();
            DataTable Tag_DT_PP = new DataTable();
            Tag_DT_PP.Columns.Add("SITE_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("WC_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("CARD_NO", typeof(string));
            Tag_DT_PP.Columns.Add("IT_SCODE", typeof(string));
            Tag_DT_PP.Columns.Add("SIZE", typeof(string));
            Tag_DT_PP.Columns.Add("CHECK_YN", typeof(string));

            foreach (DataRow row in dr)
            {
                DataRow dd = Tag_DT_PP.NewRow();
                dd["SITE_CODE"] = row[0];
                dd["WC_CODE"] = row[1];
                dd["CARD_NO"] = row[2];
                dd["IT_SCODE"] = row[3];
                dd["SIZE"] = row[4];
                dd["CHECK_YN"] = row[5];
                Tag_DT_PP.Rows.Add(dd);    
                //MessageBox.Show(row[0].ToString());
                //Tag_DT_PP.Rows.Add(row);
            }
            Tag_DT_PP.DefaultView.Sort = "CARD_NO ASC";
            dataGridView2.DataSource = Tag_DT_PP;
            /*
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {

                if (dataGridView2.Rows[i].Cells["CHECK_YN"].Value.ToString() == "생산가능")
                {

                    dataGridView2.Rows[i].DefaultCellStyle.BackColor = Color.SandyBrown;
                }
            }*/
            dataGridView2.CurrentCell = null;
        }

        
    }
}
