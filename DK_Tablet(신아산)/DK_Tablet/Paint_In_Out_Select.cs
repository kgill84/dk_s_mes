﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class Paint_In_Out_Select : Form
    {
        public string select_in_out { get; set; }
        public string btn_name { get; set; }
        public Paint_In_Out_Select()
        {
            InitializeComponent();
        }

        private void btn_In_Click(object sender, EventArgs e)
        {
            select_in_out = "IN";
            SimpleButton btn = (SimpleButton)sender;
            btn_name = btn.Name;
            DialogResult = DialogResult.OK;
        }

        private void btn_Out_Click(object sender, EventArgs e)
        {
            select_in_out = "OUT";
            SimpleButton btn = (SimpleButton)sender;
            btn_name = btn.Name;
            DialogResult = DialogResult.OK;
        }
    }
}
