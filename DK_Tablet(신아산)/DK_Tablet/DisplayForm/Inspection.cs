﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;


namespace DK_Tablet
{
    public partial class Inspection : Form
    {
        [DllImport("user32.dll")]        
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
        private SwingLibrary.SwingAPI Swing = null;        
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();
        GET_DATA GET_DATA = new GET_DATA();
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        public bool messageCheck = false;
        public string wc_group { get; set; }
        public string str_wc_code { get; set; }
        public string mo_snumb = "", r_start = "";
        public string alc_code = "";
        DataTable Tag_DT_PP = new DataTable();
        DataTable Tag_DT_DC = new DataTable();
        DataTable Tag_DT_QR = new DataTable();
        DataTable Tag_DT_QR_BOOK = new DataTable();
        //string PP_WC_CODE_str="";
        //string PP_IT_SCODE_str="";
        //string CARD_NO_str="";
        //string PP_SIZE_str = "";
        //string PP_SITE_CODE_str = "";
        //string carrier_no = "";
        //string me_scode = "";
        //string it_pkqty ="";
        public int po_start_time { get;set;}
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";
        Dictionary<string, string> combPortDic = new Dictionary<string, string>();
        MAIN parentForm;
        public Inspection(MAIN form)
        {
            this.parentForm = form;
            
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            combPortDic = Utils.GetComList();
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*
            
            
            
            Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
             
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            GET_DATA.get_work_time_master();
            night_time_start = GET_DATA.night_time_start;
            day_time_start = GET_DATA.day_time_start;

            
            lueLoc_code.Properties.DataSource = GET_DATA.LoccodeSelect_DropDown_inspection();
            lueLoc_code.Properties.DisplayMember = "LOC_NAME";
            lueLoc_code.Properties.ValueMember = "LOC_CODE";
            lueLoc_code.ItemIndex = 0;
            //SWING-U
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

            listView_target_list.ContextMenuStrip = remove_menu;
            Tag_DT_PP.Columns.Add("SITE_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("WC_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("CARD_NO", typeof(string));
            Tag_DT_PP.Columns.Add("IT_SCODE", typeof(string));
            Tag_DT_PP.Columns.Add("SIZE", typeof(string));
            Tag_DT_PP.Columns.Add("LOC_CODE", typeof(string));

            Tag_DT_QR.Columns.Add("QR_SDATE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_IT_SCODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SQTY", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SERNO", typeof(string));
            Tag_DT_QR.Columns.Add("QR_CODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOC_CODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_MM_RDATE", typeof(string));

            Tag_DT_QR_BOOK.Columns.Add("QRB_IT_SCODE", typeof(string));
            Tag_DT_QR_BOOK.Columns.Add("QRB_SQTY", typeof(string));
            Tag_DT_QR_BOOK.Columns.Add("QRB_SUNIT", typeof(string));
            Tag_DT_QR_BOOK.Columns.Add("QRB_ME_SCODE", typeof(string));
            Tag_DT_QR_BOOK.Columns.Add("QRB_LOC_CODE", typeof(string));
            

            gridControl1.DataSource = Tag_DT_PP;
            dataGridView1.DataSource = Tag_DT_QR_BOOK;
            //주야간 시작 시간 가져오기

            
            
        }

        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        
                        Tag_DT_PP.Rows.Clear();
                        Tag_DT_QR_BOOK.Rows.Clear();
                        Tag_DT_QR.Rows.Clear();
                        Tag_DT_DC.Rows.Clear();
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        void CloseThreadFunction(object normal)
        {
            bool normal_off = (bool)normal;

            Thread.Sleep(1000);
            this.Invoke(new EventHandler(delegate
            {
                button_com_close_Click(null, null);
            }));

            if (normal_off)
                MessageBox.Show("Swing-U is turned off by user", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Swing-U is turned down abnormaly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                
                Tag_DT_PP.Rows.Clear();
                Tag_DT_DC.Rows.Clear();
            }));

            Swing.ReportTagList();
        }

        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //PP카드 row add
                    //리딩
                    
                    
                    
                    
                    DataRow dr = null;
                    string[] strTags = itemString[3].Split('*');
                    if (strTags.Length == 2)
                    {
                        if (strTags[0].Equals("PP"))
                        {
                            string[] strTag = strTags[1].Split('/');

                            if (strTag.Length == 5)
                            {
                                //if (str_wc_code.Trim() == strTag[1].Trim() && txt_IT_SCODE.Text.Trim() == strTag[3].Trim())
                                //{
                                
                                dr = Tag_DT_PP.NewRow();
                                dr["SITE_CODE"] = strTag[0];
                                dr["WC_CODE"] = strTag[1];
                                dr["CARD_NO"] = strTag[2];
                                dr["IT_SCODE"] = strTag[3];
                                dr["SIZE"] = strTag[4];
                                if(string.IsNullOrWhiteSpace(txtIt_scode.Text.Trim()))
                                {
                                    Tag_DT_PP.Rows.Add(dr);
                                    lbl_cnt.Text = (int.Parse(lbl_cnt.Text.Trim()) + 1).ToString();
                                }
                                else
                                {
                                    if (txtIt_scode.Text.Trim().Equals(strTag[3].Trim()))
                                    {
                                        Tag_DT_PP.Rows.Add(dr);
                                        lbl_cnt.Text = (int.Parse(lbl_cnt.Text.Trim()) + 1).ToString();
                                    }
                                }
                                
                                //}
                            }
                        }
                    }
                    
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }

                /*if (dsmListView_ivt.Items.Count == 2)
                {
                    Swing.InventoryStop();
                }*/
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //리딩
                    MessageBox.Show(itemString[3]);
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
            
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";

                    Tag_DT_PP.Rows.Clear();
                    Tag_DT_DC.Rows.Clear();
                    Swing.SetRFPower(25);
                    Swing.ReportAllInformation();

                    
                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            formClear();
        }
        #endregion
        ContextMenuStrip remove_menu;
        
        


        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();
            Swing.ConnectionClose();
            parentForm.Visible = true;
        }
        

        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }

        //PP카드 dataRow 저장
        public DataRow saveDataRow_PP(string str)
        {
            DataRow dr=null;
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {
                        //if (str_wc_code.Trim() == strTag[1].Trim() && txt_IT_SCODE.Text.Trim() == strTag[3].Trim())
                        //{
                            dr = Tag_DT_PP.NewRow();
                            dr["SITE_CODE"] = strTag[0];
                            dr["WC_CODE"] = strTag[1];
                            dr["CARD_NO"] = strTag[2];
                            dr["IT_SCODE"] = strTag[3];
                            dr["SIZE"] = strTag[4];
                            
                        //}
                    }
                }
            }
            return dr;
        }
        public void saveDataRow_QR(string str)
        {
            DataRow dr2 = null;
            string[] strTags2 = str.Split(';');
            if (strTags2.Length == 4)
            {

                dr2 = Tag_DT_QR_BOOK.NewRow();

                dr2["QRB_IT_SCODE"] = strTags2[0];
                dr2["QRB_SQTY"] = strTags2[1];
                dr2["QRB_SUNIT"] = strTags2[2];
                dr2["QRB_ME_SCODE"] = strTags2[3];
                if (dr2 != null)
                {
                    /*for (int i = 0; i < advBandedGridView1.RowCount; i++)
                    {
                        if (Convert.ToString(advBandedGridView1.GetRowCellValue(i, "CHILD_ITEM")).Trim().Equals(dr["QRB_IT_SCODE"].ToString().Trim()))
                        {*/
                    DataRow[] drr = Tag_DT_QR_BOOK.Select("QRB_IT_SCODE='" + strTags2[0] + "'");
                    if (drr.Length > 0)
                    {
                        foreach (DataRow dgvr in Tag_DT_QR_BOOK.Rows)
                        {
                            if (dgvr["QRB_IT_SCODE"].ToString().Equals(strTags2[0]))
                            {
                                dgvr["QRB_SQTY"] = (int.Parse(dgvr["QRB_SQTY"].ToString()) + int.Parse(strTags2[1].ToString())).ToString();
                            }
                        }
                    }
                    else
                    {
                        Tag_DT_QR_BOOK.Rows.Add(dr2);
                        lbl_cnt.Text = (int.Parse(lbl_cnt.Text) + 1).ToString();
                    }
                    //lbl_Cnt.Text = (int.Parse(lbl_Cnt.Text) + 1).ToString();
                    //      break;
                    //}
                    //}
                    dataGridView1.DataSource = Tag_DT_QR_BOOK;
                }
            }
        }
        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check(string prdt_item, string card_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        
        //폼 clear
        public void formClear()
        {
            

        }
        
        public void work_input_save_virtual_IM(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string pp_serno, string carrier_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_IM", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            SqlParameter paramSITE_CODE =
                    new SqlParameter("@SITE_CODE", SqlDbType.VarChar, 4);
            paramSITE_CODE.Value = site_code;
            cmd.Parameters.Add(paramSITE_CODE);
            //작업지시번호
            SqlParameter paramRSRV_NO =
                    new SqlParameter("@RSRV_NO", SqlDbType.VarChar, 8);
            paramRSRV_NO.Value = rsrv_no;
            cmd.Parameters.Add(paramRSRV_NO);

            //품목코드
            SqlParameter paramPRDT_ITEM =
                    new SqlParameter("@PRDT_ITEM", SqlDbType.VarChar, 15);
            paramPRDT_ITEM.Value = prdt_item;
            cmd.Parameters.Add(paramPRDT_ITEM);

            //작업장코드
            SqlParameter paramWC_CODE =
                    new SqlParameter("@WC_CODE", SqlDbType.VarChar, 8);
            paramWC_CODE.Value = wc_code;
            cmd.Parameters.Add(paramWC_CODE);

            //양품
            SqlParameter paramGOOD_QTY =
                    new SqlParameter("@GOOD_QTY", SqlDbType.Float, 8);
            paramGOOD_QTY.Value = float.Parse(good_qty);
            cmd.Parameters.Add(paramGOOD_QTY);

            //불량
            SqlParameter paramFAIL_QTY =
                    new SqlParameter("@FAIL_QTY", SqlDbType.Float, 8);
            paramFAIL_QTY.Value = 0;
            cmd.Parameters.Add(paramFAIL_QTY);

            //시작시간
            SqlParameter paramR_START =
                    new SqlParameter("@R_START", SqlDbType.VarChar, 20);
            paramR_START.Value = r_start;
            cmd.Parameters.Add(paramR_START);

            //pp 시리얼번호
            SqlParameter paramCARD_NO =
                    new SqlParameter("@CARD_NO", SqlDbType.VarChar, 3);
            paramCARD_NO.Value = pp_serno;
            cmd.Parameters.Add(paramCARD_NO);

            //대차 번호
            SqlParameter paramCARRIER_NO =
                    new SqlParameter("@CARRIER_NO", SqlDbType.VarChar, 10);
            paramCARRIER_NO.Value = carrier_no;
            cmd.Parameters.Add(paramCARRIER_NO);

            SqlParameter paramMM_RDATE =
                    new SqlParameter("@MM_RDATE", SqlDbType.VarChar, 30);
            paramMM_RDATE.Value = DateTime.Now.ToString();
            cmd.Parameters.Add(paramMM_RDATE);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                //양품수량 증가

                //txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(PP_SIZE_str));


            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
        }
        
        

        


        //입고표 등록번호 가지고 오기
        private string getNewNumberToSave()
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            string query = "INSERT INTO LOC_DOCUMENT_GETNUM DEFAULT VALUES";
            string result = "";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = null;
            try
            {

                cmd.ExecuteNonQuery();
                cmd.CommandText = "SELECT MAX(IDNUM) FROM LOC_DOCUMENT_GETNUM";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = reader[0].ToString().Trim();
                }
                reader.Close();
                result = fixNo(result);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        private string fixNo(string slSnumb)
        {
            string result = "00000000";
            result = result.Substring(slSnumb.Length) + slSnumb;
            return result;
        }

        

        
        private void Injection_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            insert();
        }
        private void insert()
        {
            foreach (DataRow dr in Tag_DT_PP.Rows)
            {
                dr["LOC_CODE"] = lueLoc_code.GetColumnValue("LOC_CODE").ToString();                
            }
            foreach (DataRow dr in Tag_DT_QR_BOOK.Rows)
            {
                dr["QRB_LOC_CODE"] = lueLoc_code.GetColumnValue("LOC_CODE").ToString();
                
            }
            //string strQury;
            //bool HeaderCheck;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlTransaction trans;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            trans = conn.BeginTransaction();//트랜잭션 시작
            string insertStr = "";
            /*
            da.InsertCommand = new SqlCommand(insertStr, conn);
            da.InsertCommand.CommandType = CommandType.StoredProcedure;

            da.InsertCommand.Transaction = trans;
            da.InsertCommand.Parameters.Add("@QR_SDATE", SqlDbType.VarChar, 8, "QR_SDATE");
            da.InsertCommand.Parameters.Add("@QR_IT_SCODE", SqlDbType.VarChar, 15, "QR_IT_SCODE");
            da.InsertCommand.Parameters.Add("@QR_SQTY", SqlDbType.Float, 4, "QR_SQTY");
            da.InsertCommand.Parameters.Add("@QR_SERNO", SqlDbType.VarChar, 5, "QR_SERNO");
            da.InsertCommand.Parameters.Add("@QR_CODE", SqlDbType.VarChar, 50, "QR_CODE");
            da.InsertCommand.Parameters.Add("@QR_LOC_CODE", SqlDbType.VarChar, 10, "QR_LOC_CODE");
            da.InsertCommand.Parameters.Add("@QR_MM_RDATE", SqlDbType.VarChar, 30, "QR_MM_RDATE");

            try
            {
                da.Update(qr_dt);
            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("이동 실패 : " + ex.Message);
            }*/
            insertStr = "SP_PP_INSPECTION";

            //da.InsertCommand.Parameters.Clear();
            SqlCommand cmd = new SqlCommand(insertStr, conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Transaction = trans;
            cmd.Parameters.AddWithValue("@TVP", Tag_DT_PP);

            string insertStr2 = "";
            insertStr2 = "SP_QR_INSPECTION";
            SqlCommand cmd2 = new SqlCommand(insertStr2, conn);

            cmd2.CommandType = CommandType.StoredProcedure;

            cmd2.Transaction = trans;
            cmd2.Parameters.AddWithValue("@TVP", Tag_DT_QR_BOOK);


            try
            {
                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();
                trans.Commit();
                lbl_cnt.Text = "0";
                Tag_DT_PP.Rows.Clear();
                Tag_DT_QR_BOOK.Rows.Clear();
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                Swing.InventoryStop();
                Swing.TagListClear();
                MessageBox.Show("전송 완료");

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("전송 실패 : " + ex.Message);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            saveDataRow_QR("84715A7000WK;510;EA;60");
            //84715A7000WK;510;EA;60
        }

        private void btn_item_select_Click(object sender, EventArgs e)
        {
            Inspection_Stock_Item_Popup Inspection_Stock_Item_Popup = new Inspection_Stock_Item_Popup();
            if (Inspection_Stock_Item_Popup.ShowDialog() == DialogResult.OK)
            {
                txtIt_scode.Text = Inspection_Stock_Item_Popup.IT_SCODE_str;
                Swing.InventoryStop();
                Swing.TagListClear();
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                Tag_DT_PP.Rows.Clear();
                Tag_DT_DC.Rows.Clear();
            }
        }

        private void btn_item_delete_Click(object sender, EventArgs e)
        {
            txtIt_scode.Text = "";
        }


    }
}
