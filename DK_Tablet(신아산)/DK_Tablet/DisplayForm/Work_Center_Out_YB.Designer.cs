﻿namespace DK_Tablet
{
    partial class Work_Center_Out_YB
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Work_Center_Out_YB));
            this.txtIt_scode = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_weight_cnt = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.TEST_검사_out = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.TEST_초음파2_out = new System.Windows.Forms.Button();
            this.TEST_검사_in = new System.Windows.Forms.Button();
            this.TEST_진동2_out = new System.Windows.Forms.Button();
            this.TEST_LASER_in = new System.Windows.Forms.Button();
            this.TEXT_진동1_out = new System.Windows.Forms.Button();
            this.TEST_CLIP_in = new System.Windows.Forms.Button();
            this.TEXT_초음파1_out = new System.Windows.Forms.Button();
            this.TEST_초음파2_in = new System.Windows.Forms.Button();
            this.TEST_CLIP_out = new System.Windows.Forms.Button();
            this.TEXT_초음파1_in = new System.Windows.Forms.Button();
            this.TEST_LASER_out = new System.Windows.Forms.Button();
            this.TEXT_진동1_in = new System.Windows.Forms.Button();
            this.TEST_진동2_in = new System.Windows.Forms.Button();
            this.roundButton1 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton2 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton3 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton4 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton5 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton6 = new DK_Tablet.UC_Control.RoundButton();
            this.roundButton7 = new DK_Tablet.UC_Control.RoundButton();
            this.btn_inspection = new DevExpress.XtraEditors.SimpleButton();
            this.txt_MO_SQUTY = new System.Windows.Forms.Label();
            this.btn_re_print = new System.Windows.Forms.Button();
            this.btn_po_release_Insert = new System.Windows.Forms.Button();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.btn_day_work = new System.Windows.Forms.Button();
            this.btn_worker_info = new System.Windows.Forms.Button();
            this.txt_fail_qty = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txt_IT_MODEL = new System.Windows.Forms.Label();
            this.txt_datetime = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_night_sqty = new System.Windows.Forms.Label();
            this.txt_day_sqty = new System.Windows.Forms.Label();
            this.txt_good_qty = new System.Windows.Forms.Label();
            this.txt_IT_SCODE = new System.Windows.Forms.Label();
            this.btn_pp_card_search = new System.Windows.Forms.Button();
            this.btn_dt_input = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.btn_fail_input = new System.Windows.Forms.Button();
            this.btn_meterial_search = new System.Windows.Forms.Button();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.lbl_now_sqty = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDayAndNight = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_IT_SNAME = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.btn_carrier_sqty = new DevExpress.XtraEditors.SimpleButton();
            this.lab_carrier_sqty = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_on_off = new DevExpress.XtraEditors.SimpleButton();
            this.button2 = new System.Windows.Forms.Button();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_fail_search = new DevExpress.XtraEditors.SimpleButton();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.btn_mix_toggle = new DevExpress.XtraEditors.SimpleButton();
            this.label3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.luePrint_type = new DevExpress.XtraEditors.LookUpEdit();
            this.btn_program_finish = new DevExpress.XtraEditors.SimpleButton();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.Btn_Auto_Connection = new System.Windows.Forms.Button();
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePrint_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtIt_scode
            // 
            this.txtIt_scode.Location = new System.Drawing.Point(421, 94);
            this.txtIt_scode.Name = "txtIt_scode";
            this.txtIt_scode.Size = new System.Drawing.Size(94, 21);
            this.txtIt_scode.TabIndex = 2;
            this.txtIt_scode.Visible = false;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(425, 39);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 31);
            this.label27.TabIndex = 11;
            this.label27.Text = "출구";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(12, 2);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(318, 70);
            this.lueWc_code.TabIndex = 15;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(261, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 42);
            this.button1.TabIndex = 16;
            this.button1.Text = "프린트TEST(모비스)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Yellow;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txt_weight_cnt);
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Controls.Add(this.btn_inspection);
            this.panel2.Controls.Add(this.txt_MO_SQUTY);
            this.panel2.Controls.Add(this.btn_re_print);
            this.panel2.Controls.Add(this.btn_po_release_Insert);
            this.panel2.Controls.Add(this.imageSlider1);
            this.panel2.Controls.Add(this.btn_day_work);
            this.panel2.Controls.Add(this.btn_worker_info);
            this.panel2.Controls.Add(this.txt_fail_qty);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Controls.Add(this.txt_IT_MODEL);
            this.panel2.Controls.Add(this.txt_datetime);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txt_night_sqty);
            this.panel2.Controls.Add(this.txt_day_sqty);
            this.panel2.Controls.Add(this.txt_good_qty);
            this.panel2.Controls.Add(this.txt_IT_SCODE);
            this.panel2.Controls.Add(this.btn_pp_card_search);
            this.panel2.Controls.Add(this.btn_dt_input);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.btn_fail_input);
            this.panel2.Controls.Add(this.btn_meterial_search);
            this.panel2.Controls.Add(this.btn_po_release);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label55);
            this.panel2.Location = new System.Drawing.Point(12, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1271, 687);
            this.panel2.TabIndex = 18;
            // 
            // txt_weight_cnt
            // 
            this.txt_weight_cnt.BackColor = System.Drawing.Color.LightCyan;
            this.txt_weight_cnt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_weight_cnt.Font = new System.Drawing.Font("굴림", 35F, System.Drawing.FontStyle.Bold);
            this.txt_weight_cnt.Location = new System.Drawing.Point(1115, 270);
            this.txt_weight_cnt.Name = "txt_weight_cnt";
            this.txt_weight_cnt.Size = new System.Drawing.Size(153, 64);
            this.txt_weight_cnt.TabIndex = 42;
            this.txt_weight_cnt.Text = "0";
            this.txt_weight_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_out, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_초음파2_out, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.roundButton7, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton6, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton5, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton4, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton3, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.roundButton1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_in, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_진동2_out, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_LASER_in, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_진동1_out, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_CLIP_in, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_초음파1_out, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_초음파2_in, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_CLIP_out, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_초음파1_in, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_LASER_out, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_진동1_in, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_진동2_in, 5, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 77);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(873, 100);
            this.tableLayoutPanel1.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("굴림", 25F);
            this.label6.Location = new System.Drawing.Point(1, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 32);
            this.label6.TabIndex = 40;
            this.label6.Text = "in";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEST_검사_out
            // 
            this.TEST_검사_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_검사_out.Location = new System.Drawing.Point(767, 37);
            this.TEST_검사_out.Name = "TEST_검사_out";
            this.TEST_검사_out.Size = new System.Drawing.Size(102, 26);
            this.TEST_검사_out.TabIndex = 39;
            this.TEST_검사_out.Text = "검사";
            this.TEST_검사_out.UseVisualStyleBackColor = true;
            this.TEST_검사_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("굴림", 25F);
            this.label9.Location = new System.Drawing.Point(1, 34);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 32);
            this.label9.TabIndex = 40;
            this.label9.Text = "out";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEST_초음파2_out
            // 
            this.TEST_초음파2_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_초음파2_out.Location = new System.Drawing.Point(658, 37);
            this.TEST_초음파2_out.Name = "TEST_초음파2_out";
            this.TEST_초음파2_out.Size = new System.Drawing.Size(102, 26);
            this.TEST_초음파2_out.TabIndex = 39;
            this.TEST_초음파2_out.Text = "초음파융착2";
            this.TEST_초음파2_out.UseVisualStyleBackColor = true;
            this.TEST_초음파2_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEST_검사_in
            // 
            this.TEST_검사_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_검사_in.Location = new System.Drawing.Point(767, 4);
            this.TEST_검사_in.Name = "TEST_검사_in";
            this.TEST_검사_in.Size = new System.Drawing.Size(102, 26);
            this.TEST_검사_in.TabIndex = 39;
            this.TEST_검사_in.Text = "검사";
            this.TEST_검사_in.UseVisualStyleBackColor = true;
            this.TEST_검사_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEST_진동2_out
            // 
            this.TEST_진동2_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_진동2_out.Location = new System.Drawing.Point(549, 37);
            this.TEST_진동2_out.Name = "TEST_진동2_out";
            this.TEST_진동2_out.Size = new System.Drawing.Size(102, 26);
            this.TEST_진동2_out.TabIndex = 39;
            this.TEST_진동2_out.Text = "진동융착2";
            this.TEST_진동2_out.UseVisualStyleBackColor = true;
            this.TEST_진동2_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEST_LASER_in
            // 
            this.TEST_LASER_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_LASER_in.Location = new System.Drawing.Point(113, 4);
            this.TEST_LASER_in.Name = "TEST_LASER_in";
            this.TEST_LASER_in.Size = new System.Drawing.Size(102, 26);
            this.TEST_LASER_in.TabIndex = 39;
            this.TEST_LASER_in.Text = "레이저컷팅";
            this.TEST_LASER_in.UseVisualStyleBackColor = true;
            this.TEST_LASER_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEXT_진동1_out
            // 
            this.TEXT_진동1_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_진동1_out.Location = new System.Drawing.Point(440, 37);
            this.TEXT_진동1_out.Name = "TEXT_진동1_out";
            this.TEXT_진동1_out.Size = new System.Drawing.Size(102, 26);
            this.TEXT_진동1_out.TabIndex = 39;
            this.TEXT_진동1_out.Text = "진동융착1";
            this.TEXT_진동1_out.UseVisualStyleBackColor = true;
            this.TEXT_진동1_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEST_CLIP_in
            // 
            this.TEST_CLIP_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_CLIP_in.Location = new System.Drawing.Point(222, 4);
            this.TEST_CLIP_in.Name = "TEST_CLIP_in";
            this.TEST_CLIP_in.Size = new System.Drawing.Size(102, 26);
            this.TEST_CLIP_in.TabIndex = 39;
            this.TEST_CLIP_in.Text = "클립조립";
            this.TEST_CLIP_in.UseVisualStyleBackColor = true;
            this.TEST_CLIP_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEXT_초음파1_out
            // 
            this.TEXT_초음파1_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_초음파1_out.Location = new System.Drawing.Point(331, 37);
            this.TEXT_초음파1_out.Name = "TEXT_초음파1_out";
            this.TEXT_초음파1_out.Size = new System.Drawing.Size(102, 26);
            this.TEXT_초음파1_out.TabIndex = 39;
            this.TEXT_초음파1_out.Text = "초음파1";
            this.TEXT_초음파1_out.UseVisualStyleBackColor = true;
            this.TEXT_초음파1_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEST_초음파2_in
            // 
            this.TEST_초음파2_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_초음파2_in.Location = new System.Drawing.Point(658, 4);
            this.TEST_초음파2_in.Name = "TEST_초음파2_in";
            this.TEST_초음파2_in.Size = new System.Drawing.Size(102, 26);
            this.TEST_초음파2_in.TabIndex = 39;
            this.TEST_초음파2_in.Text = "초음파융착2";
            this.TEST_초음파2_in.UseVisualStyleBackColor = true;
            this.TEST_초음파2_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEST_CLIP_out
            // 
            this.TEST_CLIP_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_CLIP_out.Location = new System.Drawing.Point(222, 37);
            this.TEST_CLIP_out.Name = "TEST_CLIP_out";
            this.TEST_CLIP_out.Size = new System.Drawing.Size(102, 26);
            this.TEST_CLIP_out.TabIndex = 39;
            this.TEST_CLIP_out.Text = "클립조립";
            this.TEST_CLIP_out.UseVisualStyleBackColor = true;
            this.TEST_CLIP_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEXT_초음파1_in
            // 
            this.TEXT_초음파1_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_초음파1_in.Location = new System.Drawing.Point(331, 4);
            this.TEXT_초음파1_in.Name = "TEXT_초음파1_in";
            this.TEXT_초음파1_in.Size = new System.Drawing.Size(102, 26);
            this.TEXT_초음파1_in.TabIndex = 39;
            this.TEXT_초음파1_in.Text = "초음파1";
            this.TEXT_초음파1_in.UseVisualStyleBackColor = true;
            this.TEXT_초음파1_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEST_LASER_out
            // 
            this.TEST_LASER_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_LASER_out.Location = new System.Drawing.Point(113, 37);
            this.TEST_LASER_out.Name = "TEST_LASER_out";
            this.TEST_LASER_out.Size = new System.Drawing.Size(102, 26);
            this.TEST_LASER_out.TabIndex = 39;
            this.TEST_LASER_out.Text = "레이저컷팅";
            this.TEST_LASER_out.UseVisualStyleBackColor = true;
            this.TEST_LASER_out.Click += new System.EventHandler(this.TEST_OUT_Click);
            // 
            // TEXT_진동1_in
            // 
            this.TEXT_진동1_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_진동1_in.Location = new System.Drawing.Point(440, 4);
            this.TEXT_진동1_in.Name = "TEXT_진동1_in";
            this.TEXT_진동1_in.Size = new System.Drawing.Size(102, 26);
            this.TEXT_진동1_in.TabIndex = 39;
            this.TEXT_진동1_in.Text = "진동융착1";
            this.TEXT_진동1_in.UseVisualStyleBackColor = true;
            this.TEXT_진동1_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // TEST_진동2_in
            // 
            this.TEST_진동2_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_진동2_in.Location = new System.Drawing.Point(549, 4);
            this.TEST_진동2_in.Name = "TEST_진동2_in";
            this.TEST_진동2_in.Size = new System.Drawing.Size(102, 26);
            this.TEST_진동2_in.TabIndex = 39;
            this.TEST_진동2_in.Text = "진동융착2";
            this.TEST_진동2_in.UseVisualStyleBackColor = true;
            this.TEST_진동2_in.Click += new System.EventHandler(this.TEST_Click);
            // 
            // roundButton1
            // 
            this.roundButton1.cr = System.Drawing.Color.Empty;
            this.roundButton1.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton1.Location = new System.Drawing.Point(113, 70);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton1.Size = new System.Drawing.Size(100, 26);
            this.roundButton1.TabIndex = 41;
            // 
            // roundButton2
            // 
            this.roundButton2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton2.cr = System.Drawing.Color.Empty;
            this.roundButton2.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton2.Location = new System.Drawing.Point(222, 70);
            this.roundButton2.Name = "roundButton2";
            this.roundButton2.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton2.Size = new System.Drawing.Size(102, 26);
            this.roundButton2.TabIndex = 41;
            // 
            // roundButton3
            // 
            this.roundButton3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton3.cr = System.Drawing.Color.Empty;
            this.roundButton3.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton3.Location = new System.Drawing.Point(331, 70);
            this.roundButton3.Name = "roundButton3";
            this.roundButton3.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton3.Size = new System.Drawing.Size(102, 26);
            this.roundButton3.TabIndex = 41;
            // 
            // roundButton4
            // 
            this.roundButton4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton4.cr = System.Drawing.Color.Empty;
            this.roundButton4.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton4.Location = new System.Drawing.Point(440, 70);
            this.roundButton4.Name = "roundButton4";
            this.roundButton4.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton4.Size = new System.Drawing.Size(102, 26);
            this.roundButton4.TabIndex = 41;
            // 
            // roundButton5
            // 
            this.roundButton5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton5.cr = System.Drawing.Color.Empty;
            this.roundButton5.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton5.Location = new System.Drawing.Point(549, 70);
            this.roundButton5.Name = "roundButton5";
            this.roundButton5.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton5.Size = new System.Drawing.Size(102, 26);
            this.roundButton5.TabIndex = 41;
            // 
            // roundButton6
            // 
            this.roundButton6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton6.cr = System.Drawing.Color.Empty;
            this.roundButton6.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton6.Location = new System.Drawing.Point(658, 70);
            this.roundButton6.Name = "roundButton6";
            this.roundButton6.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton6.Size = new System.Drawing.Size(102, 26);
            this.roundButton6.TabIndex = 41;
            // 
            // roundButton7
            // 
            this.roundButton7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.roundButton7.cr = System.Drawing.Color.Empty;
            this.roundButton7.ForeColor = System.Drawing.Color.DimGray;
            this.roundButton7.Location = new System.Drawing.Point(767, 70);
            this.roundButton7.Name = "roundButton7";
            this.roundButton7.Padding = new System.Windows.Forms.Padding(1);
            this.roundButton7.Size = new System.Drawing.Size(102, 26);
            this.roundButton7.TabIndex = 41;
            // 
            // btn_inspection
            // 
            this.btn_inspection.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_inspection.Appearance.Options.UseFont = true;
            this.btn_inspection.Location = new System.Drawing.Point(907, -1);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(128, 72);
            this.btn_inspection.TabIndex = 38;
            this.btn_inspection.Text = "초중종품\r\n등록";
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // txt_MO_SQUTY
            // 
            this.txt_MO_SQUTY.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_MO_SQUTY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_MO_SQUTY.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_MO_SQUTY.Location = new System.Drawing.Point(1036, 123);
            this.txt_MO_SQUTY.Name = "txt_MO_SQUTY";
            this.txt_MO_SQUTY.Size = new System.Drawing.Size(116, 43);
            this.txt_MO_SQUTY.TabIndex = 11;
            this.txt_MO_SQUTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_re_print
            // 
            this.btn_re_print.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_re_print.BackgroundImage")));
            this.btn_re_print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_re_print.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_re_print.Location = new System.Drawing.Point(1034, 432);
            this.btn_re_print.Margin = new System.Windows.Forms.Padding(1);
            this.btn_re_print.Name = "btn_re_print";
            this.btn_re_print.Size = new System.Drawing.Size(118, 62);
            this.btn_re_print.TabIndex = 32;
            this.btn_re_print.Text = "식별표\r\n재출력";
            this.btn_re_print.UseVisualStyleBackColor = false;
            this.btn_re_print.Click += new System.EventHandler(this.btn_re_print_Click);
            // 
            // btn_po_release_Insert
            // 
            this.btn_po_release_Insert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release_Insert.BackgroundImage")));
            this.btn_po_release_Insert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release_Insert.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_po_release_Insert.Location = new System.Drawing.Point(1034, 623);
            this.btn_po_release_Insert.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release_Insert.Name = "btn_po_release_Insert";
            this.btn_po_release_Insert.Size = new System.Drawing.Size(117, 63);
            this.btn_po_release_Insert.TabIndex = 31;
            this.btn_po_release_Insert.Text = "긴급작지\r\n생성";
            this.btn_po_release_Insert.UseVisualStyleBackColor = false;
            this.btn_po_release_Insert.Click += new System.EventHandler(this.btn_po_release_Insert_Click);
            // 
            // imageSlider1
            // 
            this.imageSlider1.AllowLooping = true;
            this.imageSlider1.CurrentImageIndex = -1;
            this.imageSlider1.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.Stretch;
            this.imageSlider1.Location = new System.Drawing.Point(0, 71);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(1035, 615);
            this.imageSlider1.TabIndex = 30;
            this.imageSlider1.Text = "imageSlider1";
            this.imageSlider1.UseDisabledStatePainter = true;
            // 
            // btn_day_work
            // 
            this.btn_day_work.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_day_work.BackgroundImage")));
            this.btn_day_work.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_day_work.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_day_work.Location = new System.Drawing.Point(1151, 559);
            this.btn_day_work.Margin = new System.Windows.Forms.Padding(1);
            this.btn_day_work.Name = "btn_day_work";
            this.btn_day_work.Size = new System.Drawing.Size(117, 63);
            this.btn_day_work.TabIndex = 29;
            this.btn_day_work.Text = "일별\r\n생산현황";
            this.btn_day_work.UseVisualStyleBackColor = false;
            this.btn_day_work.Click += new System.EventHandler(this.btn_day_work_Click);
            // 
            // btn_worker_info
            // 
            this.btn_worker_info.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_worker_info.Location = new System.Drawing.Point(1133, -1);
            this.btn_worker_info.Name = "btn_worker_info";
            this.btn_worker_info.Size = new System.Drawing.Size(135, 71);
            this.btn_worker_info.TabIndex = 28;
            this.btn_worker_info.UseVisualStyleBackColor = true;
            this.btn_worker_info.Click += new System.EventHandler(this.btn_worker_info_Click);
            // 
            // txt_fail_qty
            // 
            this.txt_fail_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_fail_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_fail_qty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_fail_qty.Location = new System.Drawing.Point(1036, 383);
            this.txt_fail_qty.Name = "txt_fail_qty";
            this.txt_fail_qty.Size = new System.Drawing.Size(232, 48);
            this.txt_fail_qty.TabIndex = 11;
            this.txt_fail_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label56.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label56.Location = new System.Drawing.Point(1036, 334);
            this.label56.Margin = new System.Windows.Forms.Padding(0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(232, 50);
            this.label56.TabIndex = 0;
            this.label56.Text = "불량수량";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_MODEL
            // 
            this.txt_IT_MODEL.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_MODEL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_MODEL.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_MODEL.Location = new System.Drawing.Point(147, 36);
            this.txt_IT_MODEL.Name = "txt_IT_MODEL";
            this.txt_IT_MODEL.Size = new System.Drawing.Size(284, 37);
            this.txt_IT_MODEL.TabIndex = 11;
            this.txt_IT_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_datetime
            // 
            this.txt_datetime.BackColor = System.Drawing.SystemColors.Info;
            this.txt_datetime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_datetime.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_datetime.Location = new System.Drawing.Point(147, 0);
            this.txt_datetime.Name = "txt_datetime";
            this.txt_datetime.Size = new System.Drawing.Size(284, 37);
            this.txt_datetime.TabIndex = 11;
            this.txt_datetime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.LightCyan;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(1139, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 54);
            this.label5.TabIndex = 11;
            this.label5.Text = "/";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_night_sqty
            // 
            this.txt_night_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_night_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_night_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_night_sqty.Location = new System.Drawing.Point(1161, 216);
            this.txt_night_sqty.Name = "txt_night_sqty";
            this.txt_night_sqty.Size = new System.Drawing.Size(109, 54);
            this.txt_night_sqty.TabIndex = 11;
            this.txt_night_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_day_sqty
            // 
            this.txt_day_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_day_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_day_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_day_sqty.Location = new System.Drawing.Point(1036, 216);
            this.txt_day_sqty.Name = "txt_day_sqty";
            this.txt_day_sqty.Size = new System.Drawing.Size(103, 54);
            this.txt_day_sqty.TabIndex = 11;
            this.txt_day_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_good_qty
            // 
            this.txt_good_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_good_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_good_qty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_good_qty.Location = new System.Drawing.Point(1152, 123);
            this.txt_good_qty.Name = "txt_good_qty";
            this.txt_good_qty.Size = new System.Drawing.Size(116, 43);
            this.txt_good_qty.TabIndex = 11;
            this.txt_good_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_SCODE
            // 
            this.txt_IT_SCODE.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SCODE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SCODE.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SCODE.Location = new System.Drawing.Point(555, 0);
            this.txt_IT_SCODE.Name = "txt_IT_SCODE";
            this.txt_IT_SCODE.Size = new System.Drawing.Size(348, 71);
            this.txt_IT_SCODE.TabIndex = 11;
            this.txt_IT_SCODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_pp_card_search
            // 
            this.btn_pp_card_search.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image2;
            this.btn_pp_card_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_pp_card_search.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_pp_card_search.Location = new System.Drawing.Point(1152, 432);
            this.btn_pp_card_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_pp_card_search.Name = "btn_pp_card_search";
            this.btn_pp_card_search.Size = new System.Drawing.Size(118, 62);
            this.btn_pp_card_search.TabIndex = 0;
            this.btn_pp_card_search.Text = "실적등록";
            this.btn_pp_card_search.UseVisualStyleBackColor = false;
            this.btn_pp_card_search.Visible = false;
            this.btn_pp_card_search.Click += new System.EventHandler(this.btn_pp_card_search_Click);
            // 
            // btn_dt_input
            // 
            this.btn_dt_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_dt_input.BackgroundImage")));
            this.btn_dt_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_dt_input.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_dt_input.Location = new System.Drawing.Point(1034, 495);
            this.btn_dt_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_dt_input.Name = "btn_dt_input";
            this.btn_dt_input.Size = new System.Drawing.Size(117, 63);
            this.btn_dt_input.TabIndex = 0;
            this.btn_dt_input.Text = "비가동\r\n등록";
            this.btn_dt_input.UseVisualStyleBackColor = false;
            this.btn_dt_input.Click += new System.EventHandler(this.btn_dt_input_Click);
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label44.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(434, 0);
            this.label44.Margin = new System.Windows.Forms.Padding(0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(122, 71);
            this.label44.TabIndex = 0;
            this.label44.Text = "품    번";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_fail_input
            // 
            this.btn_fail_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_fail_input.BackgroundImage")));
            this.btn_fail_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_fail_input.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_fail_input.Location = new System.Drawing.Point(1151, 495);
            this.btn_fail_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_fail_input.Name = "btn_fail_input";
            this.btn_fail_input.Size = new System.Drawing.Size(117, 63);
            this.btn_fail_input.TabIndex = 0;
            this.btn_fail_input.Text = "불량등록";
            this.btn_fail_input.UseVisualStyleBackColor = false;
            this.btn_fail_input.Visible = false;
            this.btn_fail_input.Click += new System.EventHandler(this.btn_fail_input_Click);
            // 
            // btn_meterial_search
            // 
            this.btn_meterial_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_meterial_search.BackgroundImage")));
            this.btn_meterial_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_meterial_search.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_meterial_search.Location = new System.Drawing.Point(1034, 559);
            this.btn_meterial_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_meterial_search.Name = "btn_meterial_search";
            this.btn_meterial_search.Size = new System.Drawing.Size(117, 63);
            this.btn_meterial_search.TabIndex = 0;
            this.btn_meterial_search.Text = "재공자재\r\n재고";
            this.btn_meterial_search.UseVisualStyleBackColor = false;
            this.btn_meterial_search.Click += new System.EventHandler(this.btn_meterial_search_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release.BackgroundImage")));
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(1151, 623);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(117, 63);
            this.btn_po_release.TabIndex = 0;
            this.btn_po_release.Text = "작업지시\r\n선택";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(146, 37);
            this.label47.TabIndex = 0;
            this.label47.Text = "생산일자";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(1036, 0);
            this.label48.Margin = new System.Windows.Forms.Padding(0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 71);
            this.label48.TabIndex = 0;
            this.label48.Text = "작업자";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(1036, 270);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 64);
            this.label10.TabIndex = 0;
            this.label10.Text = "카운트";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(1036, 166);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "주/야";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(1036, 73);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 50);
            this.label4.TabIndex = 0;
            this.label4.Text = "계획수량";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label51.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label51.Location = new System.Drawing.Point(1152, 73);
            this.label51.Margin = new System.Windows.Forms.Padding(0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(116, 50);
            this.label51.TabIndex = 0;
            this.label51.Text = "생산수량";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label55.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label55.Location = new System.Drawing.Point(0, 37);
            this.label55.Margin = new System.Windows.Forms.Padding(0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(146, 37);
            this.label55.TabIndex = 0;
            this.label55.Text = "차종";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_now_sqty
            // 
            this.lbl_now_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.lbl_now_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_now_sqty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.lbl_now_sqty.Location = new System.Drawing.Point(1402, 234);
            this.lbl_now_sqty.Name = "lbl_now_sqty";
            this.lbl_now_sqty.Size = new System.Drawing.Size(192, 96);
            this.lbl_now_sqty.TabIndex = 11;
            this.lbl_now_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_now_sqty.Visible = false;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label50.Location = new System.Drawing.Point(1206, 234);
            this.label50.Margin = new System.Windows.Forms.Padding(0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(193, 96);
            this.label50.TabIndex = 0;
            this.label50.Text = "현재고";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label50.Visible = false;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.LightCyan;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(1328, 483);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(240, 44);
            this.label23.TabIndex = 25;
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.LightCyan;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(1328, 328);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(240, 103);
            this.label24.TabIndex = 26;
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Visible = false;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.LightCyan;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(1328, 432);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(240, 49);
            this.label22.TabIndex = 27;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Visible = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(1155, 483);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 45);
            this.label7.TabIndex = 21;
            this.label7.Text = "생산시작재고";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(1115, 328);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 201);
            this.label25.TabIndex = 22;
            this.label25.Text = "다\r\n음\r\n생\r\n산";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.Visible = false;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(1155, 329);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(170, 102);
            this.label21.TabIndex = 23;
            this.label21.Text = "품목";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.Visible = false;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(1155, 433);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(170, 48);
            this.label8.TabIndex = 24;
            this.label8.Text = "현재고";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Visible = false;
            // 
            // txtDayAndNight
            // 
            this.txtDayAndNight.BackColor = System.Drawing.SystemColors.Info;
            this.txtDayAndNight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtDayAndNight.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txtDayAndNight.Location = new System.Drawing.Point(1401, 210);
            this.txtDayAndNight.Name = "txtDayAndNight";
            this.txtDayAndNight.Size = new System.Drawing.Size(194, 82);
            this.txtDayAndNight.TabIndex = 11;
            this.txtDayAndNight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtDayAndNight.Visible = false;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.LightCyan;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(1367, 528);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(193, 96);
            this.label37.TabIndex = 11;
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label37.Visible = false;
            // 
            // txt_IT_SNAME
            // 
            this.txt_IT_SNAME.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SNAME.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SNAME.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SNAME.Location = new System.Drawing.Point(1436, 55);
            this.txt_IT_SNAME.Name = "txt_IT_SNAME";
            this.txt_IT_SNAME.Size = new System.Drawing.Size(296, 87);
            this.txt_IT_SNAME.TabIndex = 11;
            this.txt_IT_SNAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txt_IT_SNAME.Visible = false;
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label43.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(1172, 528);
            this.label43.Margin = new System.Windows.Forms.Padding(0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(193, 97);
            this.label43.TabIndex = 0;
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label43.Visible = false;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(1206, 127);
            this.label45.Margin = new System.Windows.Forms.Padding(0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(193, 105);
            this.label45.TabIndex = 0;
            this.label45.Text = "최대보관수량";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label45.Visible = false;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label53.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(1224, 54);
            this.label53.Margin = new System.Windows.Forms.Padding(0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(210, 87);
            this.label53.TabIndex = 0;
            this.label53.Text = "품    명";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label53.Visible = false;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label54.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(1189, 210);
            this.label54.Margin = new System.Windows.Forms.Padding(0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(210, 83);
            this.label54.TabIndex = 0;
            this.label54.Text = "근무유형";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label54.Visible = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(758, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 66);
            this.label2.TabIndex = 11;
            this.label2.Text = "출구대차";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(450, 9);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 20;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // btn_carrier_sqty
            // 
            this.btn_carrier_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_carrier_sqty.Appearance.Options.UseFont = true;
            this.btn_carrier_sqty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_carrier_sqty.Location = new System.Drawing.Point(976, 1);
            this.btn_carrier_sqty.Name = "btn_carrier_sqty";
            this.btn_carrier_sqty.Size = new System.Drawing.Size(72, 70);
            this.btn_carrier_sqty.TabIndex = 26;
            this.btn_carrier_sqty.Text = "32";
            this.btn_carrier_sqty.Click += new System.EventHandler(this.btn_carrier_sqty_Click);
            // 
            // lab_carrier_sqty
            // 
            this.lab_carrier_sqty.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.lab_carrier_sqty.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.lab_carrier_sqty.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.lab_carrier_sqty.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.lab_carrier_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lab_carrier_sqty.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lab_carrier_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab_carrier_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lab_carrier_sqty.Location = new System.Drawing.Point(904, 1);
            this.lab_carrier_sqty.Name = "lab_carrier_sqty";
            this.lab_carrier_sqty.Size = new System.Drawing.Size(72, 70);
            this.lab_carrier_sqty.TabIndex = 27;
            this.lab_carrier_sqty.Text = "대 차\r\n수 량";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl1.Location = new System.Drawing.Point(904, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 70);
            this.labelControl1.TabIndex = 29;
            this.labelControl1.Text = "자동";
            this.labelControl1.Visible = false;
            // 
            // btn_on_off
            // 
            this.btn_on_off.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_on_off.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_on_off.Appearance.Options.UseBackColor = true;
            this.btn_on_off.Appearance.Options.UseFont = true;
            this.btn_on_off.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_on_off.Location = new System.Drawing.Point(976, 0);
            this.btn_on_off.Name = "btn_on_off";
            this.btn_on_off.Size = new System.Drawing.Size(72, 70);
            this.btn_on_off.TabIndex = 28;
            this.btn_on_off.Text = "On";
            this.btn_on_off.Visible = false;
            this.btn_on_off.Click += new System.EventHandler(this.btn_on_off_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(90, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 42);
            this.button2.TabIndex = 16;
            this.button2.Text = "프린트TEST(KIA)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton1.Location = new System.Drawing.Point(1172, 99);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(111, 66);
            this.simpleButton1.TabIndex = 30;
            this.simpleButton1.Text = "수동\r\n입력";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn_fail_search
            // 
            this.btn_fail_search.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_fail_search.Appearance.Options.UseFont = true;
            this.btn_fail_search.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_fail_search.Location = new System.Drawing.Point(940, 0);
            this.btn_fail_search.Name = "btn_fail_search";
            this.btn_fail_search.Size = new System.Drawing.Size(108, 70);
            this.btn_fail_search.TabIndex = 26;
            this.btn_fail_search.Text = "월별\r\n불량현황";
            this.btn_fail_search.Visible = false;
            this.btn_fail_search.Click += new System.EventHandler(this.btn_fail_search_Click);
            // 
            // btn_mix_toggle
            // 
            this.btn_mix_toggle.Appearance.BackColor = System.Drawing.Color.Pink;
            this.btn_mix_toggle.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.btn_mix_toggle.Appearance.Options.UseBackColor = true;
            this.btn_mix_toggle.Appearance.Options.UseFont = true;
            this.btn_mix_toggle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_mix_toggle.Location = new System.Drawing.Point(598, 2);
            this.btn_mix_toggle.Name = "btn_mix_toggle";
            this.btn_mix_toggle.Size = new System.Drawing.Size(91, 66);
            this.btn_mix_toggle.TabIndex = 31;
            this.btn_mix_toggle.Text = "Off";
            this.btn_mix_toggle.Visible = false;
            this.btn_mix_toggle.Click += new System.EventHandler(this.btn_mix_toggle_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(504, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 66);
            this.label3.TabIndex = 11;
            this.label3.Text = "혼적";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(527, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 32;
            this.button4.Text = "KIA";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(331, 1);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Size = new System.Drawing.Size(339, 35);
            this.textEdit1.TabIndex = 33;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            this.textEdit1.Leave += new System.EventHandler(this.textEdit1_Leave);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl2.Location = new System.Drawing.Point(669, 1);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(85, 69);
            this.labelControl2.TabIndex = 38;
            this.labelControl2.Text = "식별표\r\n타입";
            // 
            // luePrint_type
            // 
            this.luePrint_type.Location = new System.Drawing.Point(755, 2);
            this.luePrint_type.Name = "luePrint_type";
            this.luePrint_type.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 22F);
            this.luePrint_type.Properties.Appearance.Options.UseFont = true;
            this.luePrint_type.Properties.Appearance.Options.UseTextOptions = true;
            this.luePrint_type.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.luePrint_type.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 22F);
            this.luePrint_type.Properties.AppearanceDropDown.Options.UseFont = true;
            this.luePrint_type.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 22F);
            this.luePrint_type.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.luePrint_type.Properties.AutoHeight = false;
            this.luePrint_type.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.luePrint_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.luePrint_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TYPE_CODE", "타입코드", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TYPE_NAME", "타입명")});
            this.luePrint_type.Properties.DropDownItemHeight = 100;
            this.luePrint_type.Properties.DropDownRows = 3;
            this.luePrint_type.Properties.NullText = "";
            this.luePrint_type.Size = new System.Drawing.Size(149, 68);
            this.luePrint_type.TabIndex = 37;
            this.luePrint_type.EditValueChanged += new System.EventHandler(this.luePrint_type_EditValueChanged);
            // 
            // btn_program_finish
            // 
            this.btn_program_finish.Appearance.BackColor = System.Drawing.Color.Crimson;
            this.btn_program_finish.Appearance.BackColor2 = System.Drawing.Color.PaleVioletRed;
            this.btn_program_finish.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_program_finish.Appearance.Options.UseBackColor = true;
            this.btn_program_finish.Appearance.Options.UseFont = true;
            this.btn_program_finish.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_program_finish.Location = new System.Drawing.Point(1167, 1);
            this.btn_program_finish.Name = "btn_program_finish";
            this.btn_program_finish.Size = new System.Drawing.Size(115, 70);
            this.btn_program_finish.TabIndex = 102;
            this.btn_program_finish.Text = "작업종료";
            this.btn_program_finish.Click += new System.EventHandler(this.btn_program_finish_Click);
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_close.Appearance.BackColor2 = System.Drawing.Color.YellowGreen;
            this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Location = new System.Drawing.Point(1049, 1);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(117, 70);
            this.btn_close.TabIndex = 103;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Btn_Auto_Connection
            // 
            this.Btn_Auto_Connection.Font = new System.Drawing.Font("굴림", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Btn_Auto_Connection.Location = new System.Drawing.Point(331, 37);
            this.Btn_Auto_Connection.Name = "Btn_Auto_Connection";
            this.Btn_Auto_Connection.Size = new System.Drawing.Size(93, 34);
            this.Btn_Auto_Connection.TabIndex = 104;
            this.Btn_Auto_Connection.Text = "설정";
            this.Btn_Auto_Connection.UseVisualStyleBackColor = true;
            this.Btn_Auto_Connection.Click += new System.EventHandler(this.Btn_Auto_Connection_Click);
            // 
            // timer_now
            // 
            this.timer_now.Enabled = true;
            this.timer_now.Interval = 10000;
            this.timer_now.Tick += new System.EventHandler(this.time_now_Tick);
            // 
            // Work_Center_Out_YB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1309, 762);
            this.Controls.Add(this.Btn_Auto_Connection);
            this.Controls.Add(this.btn_program_finish);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.luePrint_type);
            this.Controls.Add(this.lab_carrier_sqty);
            this.Controls.Add(this.btn_carrier_sqty);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btn_on_off);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lueWc_code);
            this.Controls.Add(this.txtIt_scode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDayAndNight);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.lbl_now_sqty);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.txt_IT_SNAME);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btn_mix_toggle);
            this.Controls.Add(this.btn_fail_search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Work_Center_Out_YB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paint_Out";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.SubMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.luePrint_type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtIt_scode;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txt_IT_MODEL;
        private System.Windows.Forms.Label txtDayAndNight;
        private System.Windows.Forms.Label txt_datetime;
        private System.Windows.Forms.Label lbl_now_sqty;
        private System.Windows.Forms.Label txt_fail_qty;
        private System.Windows.Forms.Label txt_good_qty;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label txt_MO_SQUTY;
        private System.Windows.Forms.Label txt_IT_SNAME;
        private System.Windows.Forms.Label txt_IT_SCODE;
        private System.Windows.Forms.Button btn_pp_card_search;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btn_dt_input;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btn_fail_input;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btn_meterial_search;
        private System.Windows.Forms.Button btn_po_release;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private DevExpress.XtraEditors.SimpleButton btn_carrier_sqty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LabelControl lab_carrier_sqty;
        private System.Windows.Forms.Button btn_worker_info;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_on_off;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn_fail_search;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label txt_night_sqty;
        private System.Windows.Forms.Label txt_day_sqty;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_day_work;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.SimpleButton btn_mix_toggle;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private System.Windows.Forms.Button btn_po_release_Insert;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btn_re_print;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit luePrint_type;
        private DevExpress.XtraEditors.SimpleButton btn_inspection;
        private DevExpress.XtraEditors.SimpleButton btn_program_finish;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private System.Windows.Forms.Button Btn_Auto_Connection;
        private System.Windows.Forms.Timer timer_now;
        private System.Windows.Forms.Button TEST_검사_in;
        private System.Windows.Forms.Button TEST_초음파2_in;
        private System.Windows.Forms.Button TEST_진동2_in;
        private System.Windows.Forms.Button TEXT_진동1_in;
        private System.Windows.Forms.Button TEXT_초음파1_in;
        private System.Windows.Forms.Button TEST_CLIP_in;
        private System.Windows.Forms.Button TEST_LASER_in;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button TEST_검사_out;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button TEST_초음파2_out;
        private System.Windows.Forms.Button TEST_진동2_out;
        private System.Windows.Forms.Button TEXT_진동1_out;
        private System.Windows.Forms.Button TEXT_초음파1_out;
        private System.Windows.Forms.Button TEST_CLIP_out;
        private System.Windows.Forms.Button TEST_LASER_out;
        private System.Windows.Forms.Label txt_weight_cnt;
        private System.Windows.Forms.Label label10;
        private UC_Control.RoundButton roundButton1;
        private UC_Control.RoundButton roundButton2;
        private UC_Control.RoundButton roundButton3;
        private UC_Control.RoundButton roundButton4;
        private UC_Control.RoundButton roundButton5;
        private UC_Control.RoundButton roundButton6;
        private UC_Control.RoundButton roundButton7;
    }
}

