﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;


namespace DK_Tablet
{
    public partial class Transfer : Form
    {
        [DllImport("user32.dll")]
        
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);//리더기와 pc간 통신하기 위한 함수
        
        private SwingLibrary.SwingAPI Swing = null;//스윙-u 사용하기 위한 API 객체 생성
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        TRANSFER_FUNC TRANSFER_FUNC = new TRANSFER_FUNC();
        
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        //private SwingLibrary.SwingAPI Swing2 = null;//스윙-u 사용하기 위한 API 객체 생성
        
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "";// r_start = "";//작업계획번호와 작업시작시간
        //입구 리더기
        string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //string rec_lot_no = "";
        string rec_card_no = "";//다음 pp 카드
        string rec_it_scode = "";//다음 품목코드
        string sel_carrier_no = "";//다음대차번호
        //string sel_lot_no = "";
        string sel_card_no = "";//들어온 카드 번호
        string sel_it_scode = "";//들어온 품목코드
        //string carrier_yn = "";//공대차인지 아닌지 구분자
        float f_mm_sqty = 0;//대차에 적재 되어있는 제품 수량
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        string sw_code = "";//근무형태 코드
        //출구 리더기
        PpCard_reading PpCard_reading = new PpCard_reading();
        //string out_carrier_no = "";// 출구로 나가는 대차 번호
        //DataTable Tag_DT_PP = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관
        //DataTable Tag_DT_DC = new DataTable();//출구에서 리더기로 읽어 들인 대차 카드 보관
        //DataTable po_DT = new DataTable();//생산계획을 여러개 선택 할 수 있도록 선택한 생산계획 정보 보관

        //string PP_WC_CODE_str = "";//작업장
        //string PP_IT_SCODE_str = "";//품목코드
        //string CARD_NO_str = "";//카드번호
        //string PP_SIZE_str = "";//수용수
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";

        DataTable TransferDT = new DataTable();
        
        /*
         버튼 클릭 태그
         */
        private string BTN_TAG = "NONE";
        public Transfer(MAIN form)
        {
            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
            
        }
        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else { 
                            //radioButton_ac_multi.Checked = true;
                        }
                            break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no="";
                        rec_it_scode = "";
                        rec_card_no = "";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
            }));

            Swing.ReportTagList();
        }
        
        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }
        
        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //리딩
                    if (BTN_TAG.Equals("NONE"))
                    {
                        Swing.InventoryStop();
                        Swing.TagListClear();
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                    }
                    else
                    {
                        saveDataRow_PP(itemString[3]);
                        saveDataRow_DC_S(itemString[3]);
                    }
                    
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        
        

        public void saveDataRow_PP(string str)
        {            
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {
                        if (txt_pp_card.Text.Trim() == "")
                        {
                            if (BTN_TAG.Equals("AFTER"))
                            {
                                if (pp_card_check(strTag[3], strTag[2]))
                                {
                                    TransferDT.Rows.Clear();
                                    DataTable dt = TRANSFER_FUNC.get_Transfer_it_scode(strTag[3]);
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        DataRow[] Dr = TransferDT.Select("CARRIER_NO='" + dr["CARRIER_NO"].ToString() + "'");

                                        if (Dr.Length == 0)
                                        {
                                            if (dataGridView1.Rows.Count > 0)
                                            {
                                                if (!dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim().Equals(dr["PRDT_ITEM"].ToString().Trim()))
                                                {
                                                    MessageBox.Show("다른 품목이 적재 되어있는 대차는 \n추가 할 수 없습니다.");
                                                    return;
                                                }
                                            }
                                            DataRow drr = TransferDT.NewRow();
                                            drr["SITE_CODE"] = dr["SITE_CODE"].ToString();
                                            drr["RSRV_NO"] = dr["RSRV_NO"].ToString();
                                            drr["PRDT_ITEM"] = dr["PRDT_ITEM"].ToString();
                                            drr["VW_SNUMB"] = dr["VW_SNUMB"].ToString();
                                            drr["WC_CODE"] = dr["WC_CODE"].ToString();
                                            drr["SYSIN_DATE"] = dr["SYSIN_DATE"].ToString();
                                            drr["MANIN_DATE"] = dr["MANIN_DATE"].ToString();
                                            drr["MM_RDATE"] = dr["MM_RDATE"].ToString();
                                            drr["R_START"] = dr["R_START"].ToString();
                                            drr["R_END"] = dr["R_END"].ToString();
                                            drr["GOOD_SQTY"] = dr["GOOD_SQTY"].ToString();
                                            drr["FAIL_SQTY"] = dr["FAIL_SQTY"].ToString();
                                            drr["REMAIN_SQTY"] = dr["REMAIN_SQTY"].ToString();
                                            drr["LOT_NO"] = dr["LOT_NO"].ToString();
                                            drr["CARRIER_NO"] = dr["CARRIER_NO"].ToString();
                                            drr["CARRIER_YN"] = dr["CARRIER_YN"].ToString();
                                            drr["CARD_NO"] = dr["CARD_NO"].ToString();
                                            drr["END_CHECK"] = dr["END_CHECK"].ToString();
                                            drr["MOVE_SQTY"] = 0;
                                            TransferDT.Rows.Add(drr);
                                        }
                                    }
                                    txt_pp_card.Text = strTag[2].Trim();
                                    rec_it_scode = strTag[3].Trim();
                                    it_pkqty = int.Parse(TRANSFER_FUNC.get_it_pkqty(rec_it_scode));    
                                    
                                    if (!string.IsNullOrWhiteSpace(txt_pp_card.Text) && !string.IsNullOrWhiteSpace(txtAfter_carrier_no.Text))
                                    {
                                        Swing.InventoryStop();
                                        Swing.TagListClear();
                                        dsmListView_ivt.Items.Clear();
                                        ddc_ivt.DigitText = "00000";
                                        BTN_TAG = "NONE";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("등록 할 수 없는 카드 입니다.");
                                }
                            }
                            else if (BTN_TAG.Equals("BEFORE"))
                            {
                                DataRow[] Dr = TransferDT.Select("CARD_NO='" + strTag[2].Trim() + "' AND PRDT_ITEM='" + strTag[3].Trim() + "' AND MOVE_SQTY=0");

                                if (Dr.Length == 1)
                                {
                                    int total_sqty = 0;
                                    foreach (DataRow dr2 in TransferDT.Rows)
                                    {
                                        total_sqty += int.Parse(dr2["MOVE_SQTY"].ToString());
                                    }
                                    if (total_sqty < it_pkqty)
                                    {
                                        if (int.Parse(Dr[0]["REMAIN_SQTY"].ToString()) < it_pkqty - total_sqty)
                                        {
                                            Dr[0]["MOVE_SQTY"] = Dr[0]["REMAIN_SQTY"].ToString();
                                        }
                                        else
                                        {
                                            Dr[0]["MOVE_SQTY"] = (it_pkqty - total_sqty).ToString();
                                        }
                                        //TransferDT.Rows.Add(drr);
                                        txt_total_transfer_sqty.Text = (int.Parse(txt_total_transfer_sqty.Text) + int.Parse(Dr[0]["MOVE_SQTY"].ToString())).ToString();
                                        Swing.InventoryStop();
                                        Swing.TagListClear();
                                        dsmListView_ivt.Items.Clear();
                                        ddc_ivt.DigitText = "00000";
                                        TransferDT.DefaultView.Sort = "MOVE_SQTY DESC";
                                        //수용수 만큼 수량 찼을때 자동으로 이적실행
                                        if (int.Parse(txt_total_transfer_sqty.Text) == it_pkqty)
                                        {
                                            btn_transfer_commit.PerformClick();
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("대차를 더이상 추가 할 수 없습니다.");
                                    }

                                    BTN_TAG = "NONE";
                                } 
                            }
                        }
                    }
                }
            }            
        }
        int it_pkqty = 0;
        public void saveDataRow_DC_S(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    if (BTN_TAG.Equals("BEFORE"))
                    {
                        DataRow[] Dr = TransferDT.Select("CARRIER_NO='" + strTags[1] + "' AND MOVE_SQTY=0");

                            if (Dr.Length == 1)
                            {                                
                                int total_sqty = 0;
                                foreach (DataRow dr2 in TransferDT.Rows)
                                {
                                    total_sqty += int.Parse(dr2["MOVE_SQTY"].ToString());
                                }
                                if (total_sqty < it_pkqty)
                                {
                                    if (int.Parse(Dr[0]["REMAIN_SQTY"].ToString()) < it_pkqty - total_sqty)
                                    {
                                        Dr[0]["MOVE_SQTY"] = Dr[0]["REMAIN_SQTY"].ToString();
                                    }
                                    else
                                    {
                                        Dr[0]["MOVE_SQTY"] = (it_pkqty - total_sqty).ToString();
                                    }
                                    //TransferDT.Rows.Add(drr);
                                    txt_total_transfer_sqty.Text = (int.Parse(txt_total_transfer_sqty.Text) + int.Parse(Dr[0]["MOVE_SQTY"].ToString())).ToString();
                                    Swing.InventoryStop();
                                    Swing.TagListClear();
                                    dsmListView_ivt.Items.Clear();
                                    ddc_ivt.DigitText = "00000";
                                    TransferDT.DefaultView.Sort = "MOVE_SQTY DESC";
                                    //수용수 만큼 수량 찼을때 자동으로 이적실행
                                    if (int.Parse(txt_total_transfer_sqty.Text) == it_pkqty)
                                    {
                                        btn_transfer_commit.PerformClick();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("대차를 더이상 추가 할 수 없습니다.");
                                }
                                
                                BTN_TAG = "NONE";
                            }
                        
                        
                        
                    }
                    else if (BTN_TAG.Equals("AFTER"))
                    {
                        //공대차 인지 체크
                        if (CHECK_FUNC.carrier_check(strTags[1].Trim()))
                        {
                            if (txtAfter_carrier_no.Text.Trim() == "")
                            {
                                txtAfter_carrier_no.Text = strTags[1].Trim();
                                
                                if (!string.IsNullOrWhiteSpace(txt_pp_card.Text) && !string.IsNullOrWhiteSpace(txtAfter_carrier_no.Text))
                                {
                                    Swing.InventoryStop();
                                    Swing.TagListClear();
                                    dsmListView_ivt.Items.Clear();
                                    ddc_ivt.DigitText = "00000";
                                    BTN_TAG = "NONE";
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("공대차가 아닙니다.");                            
                        }
                    }
                    
                                      
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();
            
            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {   
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";
                    txtIn_Carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";
                    Swing.SetRFPower(25);
                    Swing.ReportAllInformation();

                    
                       // work_plan_popup_paint();
                   
                    
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }
        #endregion

        public void GetTime()
        {
            while (true)
            {
                N_timer.Text = DateTime.Now.ToString("HH시 mm분 ss초");
            }
        }

        ContextMenuStrip remove_menu;
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                PpCard_reading.Show();
                PpCard_reading.Visible = false;
                Success_Form.Show();
                Success_Form.Visible = false;

                

                //SWING-U
                this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
                SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);
               

                remove_menu = new ContextMenuStrip();
                ToolStripMenuItem item = new ToolStripMenuItem("Remove");
                item.Click += new EventHandler(target_remove);
                remove_menu.Items.Add(item);

                remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

                listView_target_list.ContextMenuStrip = remove_menu;
                
                TransferDT.Columns.Add("SITE_CODE", typeof(string));//0
                TransferDT.Columns.Add("RSRV_NO", typeof(string));//1                
                TransferDT.Columns.Add("PRDT_ITEM", typeof(string));//3
                TransferDT.Columns.Add("VW_SNUMB", typeof(int));//3
                TransferDT.Columns.Add("WC_CODE", typeof(string));//4
                TransferDT.Columns.Add("SYSIN_DATE", typeof(string));//5
                TransferDT.Columns.Add("MANIN_DATE", typeof(string));//6
                TransferDT.Columns.Add("MM_RDATE", typeof(string));//6
                TransferDT.Columns.Add("R_START", typeof(string));//7                
                TransferDT.Columns.Add("R_END", typeof(string));//7                
                TransferDT.Columns.Add("GOOD_SQTY", typeof(float));//9
                TransferDT.Columns.Add("FAIL_SQTY", typeof(float));//9
                TransferDT.Columns.Add("REMAIN_SQTY", typeof(float));//10
                TransferDT.Columns.Add("LOT_NO", typeof(string));//11
                TransferDT.Columns.Add("CARRIER_NO", typeof(string));//12
                TransferDT.Columns.Add("CARD_NO", typeof(string));//13
                TransferDT.Columns.Add("END_CHECK", typeof(string));//14
                TransferDT.Columns.Add("CARRIER_YN", typeof(string));//14
                TransferDT.Columns.Add("MOVE_SQTY", typeof(float));//15
                dataGridView1.DataSource = TransferDT;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }

        }
        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();            
            Swing.ConnectionClose();            
            parentForm.Visible = true;
        }
       

        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check(string prdt_item, string pp_serno)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + pp_serno + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIn_Carrier_no.Text = "";
            txtIt_scode.Text = "";
            txtCard_no.Text = "";
            in_carrier_no = "";
            rec_it_scode = "";
            rec_card_no = "";
            sel_carrier_no="";
            sel_it_scode ="";
            sel_card_no = "";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";

            sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
            Swing.InventoryStart(); 
        }


        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void timer_now_Tick_1(object sender, EventArgs e)
        {
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            timer_now.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            PpCard_reading.TopLevel = false;
            PpCard_reading.TopMost = false;
            PpCard_reading.Visible = false;
            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            timer1.Stop();
        }
        private Success_Form Success_Form=new Success_Form();
        
        private void btn_transfer_search_Click(object sender, EventArgs e)
        {

        }

        private void btn_carrier_before_Click(object sender, EventArgs e)
        {
            BTN_TAG = "BEFORE";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStart();
        }

        private void btn_carrier_after_Click(object sender, EventArgs e)
        {
            BTN_TAG = "AFTER";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStart();
        }

        private void dataGridView1_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                txt_total_transfer_sqty.Text = "0";
                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "MOVE_SQTY":
                        Sqty_popup Sqty_popup = new Sqty_popup();
                        Sqty_popup.sqty = senderGrid.Rows[e.RowIndex].Cells["MOVE_SQTY"].Value.ToString();
                        if (Sqty_popup.ShowDialog() == DialogResult.OK)
                        {
                            
                            int remain_sqty = int.Parse(senderGrid.Rows[e.RowIndex].Cells["REMAIN_SQTY"].Value.ToString());
                            if (remain_sqty < int.Parse(Sqty_popup.sqty))
                            {
                                MessageBox.Show("남은수량을 초과 할 수 없습니다.");
                                return;
                            }
                            senderGrid.Rows[e.RowIndex].Cells["MOVE_SQTY"].Value = Sqty_popup.sqty;
                            
                            
                        }
                        
                        break;
                    case "BTN_DELETE":
                        TransferDT.Rows[e.RowIndex].Delete();
                        break;
                }
                foreach (DataGridViewRow dr in senderGrid.Rows)
                {

                    txt_total_transfer_sqty.Text = (int.Parse(txt_total_transfer_sqty.Text) + int.Parse(dr.Cells["MOVE_SQTY"].Value.ToString())).ToString();
                }
            }
        }

        private void txtAfter_carrier_no_Click(object sender, EventArgs e)
        {
            BTN_TAG = "AFTER";
            txtAfter_carrier_no.Text = "";            
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStart();
            
        }

        private void btn_pp_card_Click(object sender, EventArgs e)
        {
            BTN_TAG = "AFTER";
            txt_pp_card.Text = "";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStart();
        }

        private void btn_transfer_commit_Click(object sender, EventArgs e)
        {
            if (TransferDT.Rows.Count == 0)
            {
                MessageBox.Show("이적 할 대차를 읽어주세요");
                return;
            }
            if (txt_total_transfer_sqty.Text.Trim().Equals("0"))
            {
                MessageBox.Show("이적할 수량이 0개 입니다.\n이적 할 수 없습니다.");
                return;
            }
            if (string.IsNullOrWhiteSpace(txtAfter_carrier_no.Text))
            {
                MessageBox.Show("이적대차를 읽지 않았습니다.");
                return;
            }
            if (string.IsNullOrWhiteSpace(txt_pp_card.Text))
            {
                MessageBox.Show("이적할 PP카드를 읽지 않았습니다.");
                return;
            }
            if (it_pkqty > int.Parse(txt_total_transfer_sqty.Text.Trim()))
            {
                MessageBox.Show("적재수량이 부족합니다.\n" + it_pkqty + "개가 적재 되어야 합니다.");
                return;
            }
            else if (it_pkqty < int.Parse(txt_total_transfer_sqty.Text.Trim()))
            {
                MessageBox.Show("적재수량이 기존 수량보다 많습니다.\n" + it_pkqty + "개가 적재 되어야 합니다.");
                return;
            }
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            trans = conn.BeginTransaction();
            //이적 기능 실행
            try
            {
                
                foreach (DataRow DR in TransferDT.Rows)
                {                    
                    string sql = "";
                    sql = "SP_TRANSFER_CARRIER";

                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SITE_CODE", DR["SITE_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@RSRV_NO", DR["RSRV_NO"].ToString());                    
                    cmd.Parameters.AddWithValue("@PRDT_ITEM", DR["PRDT_ITEM"].ToString());
                    cmd.Parameters.AddWithValue("@VW_SNUMB", DR["VW_SNUMB"].ToString());
                    cmd.Parameters.AddWithValue("@WC_CODE", DR["WC_CODE"].ToString());
                    cmd.Parameters.AddWithValue("@SYSIN_DATE", DR["SYSIN_DATE"].ToString());
                    cmd.Parameters.AddWithValue("@MANIN_DATE", DR["MANIN_DATE"].ToString());
                    cmd.Parameters.AddWithValue("@MM_RDATE", DR["MM_RDATE"].ToString());
                    cmd.Parameters.AddWithValue("@R_START", DR["R_START"].ToString());
                    cmd.Parameters.AddWithValue("@R_END", DR["R_END"].ToString());                    
                    cmd.Parameters.AddWithValue("@GOOD_SQTY", DR["GOOD_SQTY"].ToString());
                    cmd.Parameters.AddWithValue("@FAIL_SQTY", DR["FAIL_SQTY"].ToString());
                    cmd.Parameters.AddWithValue("@REMAIN_SQTY", DR["REMAIN_SQTY"].ToString());
                    cmd.Parameters.AddWithValue("@LOT_NO", DR["LOT_NO"].ToString());
                    cmd.Parameters.AddWithValue("@CARRIER_NO", DR["CARRIER_NO"].ToString());
                    cmd.Parameters.AddWithValue("@CARD_NO", DR["CARD_NO"].ToString());
                    cmd.Parameters.AddWithValue("@END_CHECK", DR["END_CHECK"].ToString());
                    cmd.Parameters.AddWithValue("@MOVE_SQTY", DR["MOVE_SQTY"].ToString());
                    cmd.Parameters.AddWithValue("@CARRIER_YN", DR["CARRIER_YN"].ToString());
                    cmd.Parameters.AddWithValue("@AFTER_CARRIER", txtAfter_carrier_no.Text);
                    cmd.Parameters.AddWithValue("@AFTER_CARD", txt_pp_card.Text);
                    cmd.Transaction = trans;
                    
                    cmd.ExecuteNonQuery();
                }
                string sql_1 = "";
                sql_1 = "SP_TRANSFER_CARRIER_INSTATEMENT";
                SqlCommand cmd_instatement = new SqlCommand(sql_1, conn);
                cmd_instatement.CommandType = CommandType.StoredProcedure;
                cmd_instatement.Parameters.AddWithValue("@SITE_CODE", dataGridView1.Rows[0].Cells["SITE_CODE"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@RSRV_NO", dataGridView1.Rows[0].Cells["RSRV_NO"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@PRDT_ITEM", dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@VW_SNUMB", dataGridView1.Rows[0].Cells["VW_SNUMB"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@WC_CODE", dataGridView1.Rows[0].Cells["WC_CODE"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@SYSIN_DATE", dataGridView1.Rows[0].Cells["SYSIN_DATE"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@MANIN_DATE", dataGridView1.Rows[0].Cells["MANIN_DATE"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@MM_RDATE", dataGridView1.Rows[0].Cells["MM_RDATE"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@R_START", dataGridView1.Rows[0].Cells["R_START"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@R_END", dataGridView1.Rows[0].Cells["R_END"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@GOOD_SQTY", txt_total_transfer_sqty.Text);
                cmd_instatement.Parameters.AddWithValue("@FAIL_SQTY", dataGridView1.Rows[0].Cells["FAIL_SQTY"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@REMAIN_SQTY", dataGridView1.Rows[0].Cells["REMAIN_SQTY"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@LOT_NO", dataGridView1.Rows[0].Cells["LOT_NO"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@CARRIER_NO", dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@CARD_NO", dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@END_CHECK", dataGridView1.Rows[0].Cells["END_CHECK"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@MOVE_SQTY", dataGridView1.Rows[0].Cells["MOVE_SQTY"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@CARRIER_YN", dataGridView1.Rows[0].Cells["CARRIER_YN"].Value.ToString());
                cmd_instatement.Parameters.AddWithValue("@AFTER_CARRIER", txtAfter_carrier_no.Text);
                cmd_instatement.Parameters.AddWithValue("@AFTER_CARD", txt_pp_card.Text);
                cmd_instatement.Transaction = trans;

                cmd_instatement.ExecuteNonQuery();

                trans.Commit();
                TransferDT.Rows.Clear();
                txt_total_transfer_sqty.Text = "0";
                txt_pp_card.Text = "";
                txtAfter_carrier_no.Text = "";
                MessageBox.Show("이적이 완료 되었습니다.");
            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }


        }


    }
}
