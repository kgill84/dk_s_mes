﻿namespace DK_Tablet
{
    partial class Inspection
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_item_delete = new DevExpress.XtraEditors.SimpleButton();
            this.btn_item_select = new DevExpress.XtraEditors.SimpleButton();
            this.txtIt_scode = new DevExpress.XtraEditors.TextEdit();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.QRB_IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QRB_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QRB_SUNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QRB_ME_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_cnt = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SITE_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CARD_NO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SIZE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CHILD_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.소요량 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.재고수량 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.부족분 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.WC_SCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.WC_NAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ME_SNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ME_SCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.btn_inspection = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIt_scode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(104, 46);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(257, 40);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(367, 46);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(448, 46);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_item_delete);
            this.panel1.Controls.Add(this.btn_item_select);
            this.panel1.Controls.Add(this.txtIt_scode);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.lbl_cnt);
            this.panel1.Controls.Add(this.gridControl1);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.lueLoc_code);
            this.panel1.Controls.Add(this.btn_inspection);
            this.panel1.Location = new System.Drawing.Point(12, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 664);
            this.panel1.TabIndex = 8;
            // 
            // btn_item_delete
            // 
            this.btn_item_delete.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_item_delete.Appearance.BackColor2 = System.Drawing.Color.HotPink;
            this.btn_item_delete.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_item_delete.Appearance.Options.UseBackColor = true;
            this.btn_item_delete.Appearance.Options.UseFont = true;
            this.btn_item_delete.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_item_delete.Location = new System.Drawing.Point(993, 4);
            this.btn_item_delete.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btn_item_delete.Name = "btn_item_delete";
            this.btn_item_delete.Size = new System.Drawing.Size(98, 76);
            this.btn_item_delete.TabIndex = 67;
            this.btn_item_delete.Text = "품 목\r\n삭 제";
            this.btn_item_delete.Click += new System.EventHandler(this.btn_item_delete_Click);
            // 
            // btn_item_select
            // 
            this.btn_item_select.Appearance.BackColor = System.Drawing.Color.LightBlue;
            this.btn_item_select.Appearance.BackColor2 = System.Drawing.Color.RoyalBlue;
            this.btn_item_select.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_item_select.Appearance.Options.UseBackColor = true;
            this.btn_item_select.Appearance.Options.UseFont = true;
            this.btn_item_select.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_item_select.Location = new System.Drawing.Point(888, 4);
            this.btn_item_select.Name = "btn_item_select";
            this.btn_item_select.Size = new System.Drawing.Size(98, 76);
            this.btn_item_select.TabIndex = 67;
            this.btn_item_select.Text = "품 목\r\n선 택";
            this.btn_item_select.Click += new System.EventHandler(this.btn_item_select_Click);
            // 
            // txtIt_scode
            // 
            this.txtIt_scode.EditValue = "";
            this.txtIt_scode.Location = new System.Drawing.Point(598, 4);
            this.txtIt_scode.Name = "txtIt_scode";
            this.txtIt_scode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 23F);
            this.txtIt_scode.Properties.Appearance.Options.UseFont = true;
            this.txtIt_scode.Properties.Appearance.Options.UseTextOptions = true;
            this.txtIt_scode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtIt_scode.Properties.AutoHeight = false;
            this.txtIt_scode.Size = new System.Drawing.Size(284, 76);
            this.txtIt_scode.TabIndex = 66;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 60;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QRB_IT_SCODE,
            this.QRB_SQTY,
            this.QRB_SUNIT,
            this.QRB_ME_SCODE});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(645, 131);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 60;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView1.RowTemplate.Height = 60;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(619, 524);
            this.dataGridView1.TabIndex = 65;
            // 
            // QRB_IT_SCODE
            // 
            this.QRB_IT_SCODE.DataPropertyName = "QRB_IT_SCODE";
            this.QRB_IT_SCODE.HeaderText = "품목코드";
            this.QRB_IT_SCODE.Name = "QRB_IT_SCODE";
            this.QRB_IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QRB_IT_SCODE.Width = 250;
            // 
            // QRB_SQTY
            // 
            this.QRB_SQTY.DataPropertyName = "QRB_SQTY";
            this.QRB_SQTY.HeaderText = "수량";
            this.QRB_SQTY.Name = "QRB_SQTY";
            this.QRB_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QRB_SUNIT
            // 
            this.QRB_SUNIT.DataPropertyName = "QRB_SUNIT";
            this.QRB_SUNIT.HeaderText = "단위";
            this.QRB_SUNIT.Name = "QRB_SUNIT";
            this.QRB_SUNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QRB_ME_SCODE
            // 
            this.QRB_ME_SCODE.DataPropertyName = "QRB_ME_SCODE";
            this.QRB_ME_SCODE.HeaderText = "자재유형코드";
            this.QRB_ME_SCODE.Name = "QRB_ME_SCODE";
            this.QRB_ME_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QRB_ME_SCODE.Width = 250;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(642, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 43);
            this.label2.TabIndex = 64;
            this.label2.Text = "QR코드";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(3, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 43);
            this.label5.TabIndex = 64;
            this.label5.Text = "PP카드";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_cnt
            // 
            this.lbl_cnt.Appearance.BackColor = System.Drawing.Color.Black;
            this.lbl_cnt.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.lbl_cnt.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_cnt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_cnt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cnt.Location = new System.Drawing.Point(464, 3);
            this.lbl_cnt.Name = "lbl_cnt";
            this.lbl_cnt.Size = new System.Drawing.Size(127, 78);
            this.lbl_cnt.TabIndex = 63;
            this.lbl_cnt.Text = "0";
            // 
            // gridControl1
            // 
            this.gridControl1.Font = new System.Drawing.Font("굴림", 9F);
            this.gridControl1.Location = new System.Drawing.Point(3, 131);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(637, 524);
            this.gridControl1.TabIndex = 62;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.advBandedGridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.LightBlue;
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.MintCream;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.Lime;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SITE_CODE,
            this.WC_CODE,
            this.CARD_NO,
            this.IT_SCODE,
            this.SIZE});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.IndicatorWidth = 40;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 45;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.SITE_CODE.AppearanceCell.Options.UseFont = true;
            this.SITE_CODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.SITE_CODE.AppearanceHeader.Options.UseFont = true;
            this.SITE_CODE.Caption = "공장";
            this.SITE_CODE.FieldName = "SITE_CODE";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.Width = 220;
            // 
            // WC_CODE
            // 
            this.WC_CODE.Caption = "작업장코드";
            this.WC_CODE.FieldName = "WC_CODE";
            this.WC_CODE.Name = "WC_CODE";
            // 
            // CARD_NO
            // 
            this.CARD_NO.Caption = "카드번호";
            this.CARD_NO.FieldName = "CARD_NO";
            this.CARD_NO.Name = "CARD_NO";
            this.CARD_NO.Visible = true;
            this.CARD_NO.VisibleIndex = 0;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품목코드";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 1;
            // 
            // SIZE
            // 
            this.SIZE.Caption = "수량";
            this.SIZE.FieldName = "SIZE";
            this.SIZE.Name = "SIZE";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.PaleGreen;
            this.advBandedGridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.MintCream;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.advBandedGridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.Lime;
            this.advBandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.advBandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand11,
            this.gridBand9,
            this.gridBand1,
            this.gridBand5,
            this.gridBand13,
            this.gridBand3,
            this.gridBand7,
            this.gridBand8});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.CHILD_ITEM,
            this.IT_SNAME,
            this.소요량,
            this.재고수량,
            this.부족분,
            this.WC_SCODE,
            this.WC_NAME,
            this.ME_SCODE,
            this.ME_SNAME});
            this.advBandedGridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1150, 444, 216, 216);
            this.advBandedGridView1.DefaultRelationIndex = 1;
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.IndicatorWidth = 40;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsCustomization.AllowFilter = false;
            this.advBandedGridView1.OptionsPrint.ExpandAllGroups = false;
            this.advBandedGridView1.OptionsPrint.PrintBandHeader = false;
            this.advBandedGridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.ShowBands = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Columns.Add(this.CHILD_ITEM);
            this.gridBand2.Columns.Add(this.IT_SNAME);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 220;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.CHILD_ITEM.AppearanceCell.Options.UseFont = true;
            this.CHILD_ITEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.CHILD_ITEM.AppearanceHeader.Options.UseFont = true;
            this.CHILD_ITEM.Caption = "품목코드";
            this.CHILD_ITEM.FieldName = "CHILD_ITEM";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.RowCount = 2;
            this.CHILD_ITEM.Visible = true;
            this.CHILD_ITEM.Width = 220;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.IT_SNAME.AppearanceCell.Options.UseFont = true;
            this.IT_SNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.IT_SNAME.AppearanceHeader.Options.UseFont = true;
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.RowCount = 2;
            this.IT_SNAME.RowIndex = 1;
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.Width = 220;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "gridBand11";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Visible = false;
            this.gridBand11.VisibleIndex = -1;
            this.gridBand11.Width = 130;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "gridBand12";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 0;
            this.gridBand12.Width = 130;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand10});
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Visible = false;
            this.gridBand9.VisibleIndex = -1;
            this.gridBand9.Width = 131;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "gridBand10";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 0;
            this.gridBand10.Width = 131;
            // 
            // gridBand1
            // 
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Visible = false;
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 146;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 146;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "gridBand5";
            this.gridBand5.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6});
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Visible = false;
            this.gridBand5.VisibleIndex = -1;
            this.gridBand5.Width = 168;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 0;
            this.gridBand6.Width = 168;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Columns.Add(this.소요량);
            this.gridBand13.Columns.Add(this.재고수량);
            this.gridBand13.Columns.Add(this.부족분);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 1;
            this.gridBand13.Width = 107;
            // 
            // 소요량
            // 
            this.소요량.Caption = "소요량";
            this.소요량.FieldName = "소요량";
            this.소요량.Name = "소요량";
            // 
            // 재고수량
            // 
            this.재고수량.Caption = "재고수량";
            this.재고수량.FieldName = "재고수량";
            this.재고수량.Name = "재고수량";
            // 
            // 부족분
            // 
            this.부족분.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.부족분.AppearanceCell.Options.UseFont = true;
            this.부족분.AppearanceCell.Options.UseTextOptions = true;
            this.부족분.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.부족분.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.부족분.AppearanceHeader.Options.UseFont = true;
            this.부족분.Caption = "부족수량";
            this.부족분.FieldName = "부족분";
            this.부족분.Name = "부족분";
            this.부족분.RowCount = 4;
            this.부족분.Visible = true;
            this.부족분.Width = 107;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Visible = false;
            this.gridBand3.VisibleIndex = -1;
            this.gridBand3.Width = 77;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Columns.Add(this.WC_SCODE);
            this.gridBand7.Columns.Add(this.WC_NAME);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 2;
            this.gridBand7.Width = 138;
            // 
            // WC_SCODE
            // 
            this.WC_SCODE.Caption = "작업장코드";
            this.WC_SCODE.FieldName = "WC_CODE";
            this.WC_SCODE.Name = "WC_SCODE";
            // 
            // WC_NAME
            // 
            this.WC_NAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.WC_NAME.AppearanceCell.Options.UseFont = true;
            this.WC_NAME.AppearanceCell.Options.UseTextOptions = true;
            this.WC_NAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WC_NAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.WC_NAME.AppearanceHeader.Options.UseFont = true;
            this.WC_NAME.Caption = "작업장명";
            this.WC_NAME.FieldName = "WC_NAME";
            this.WC_NAME.Name = "WC_NAME";
            this.WC_NAME.RowCount = 4;
            this.WC_NAME.Visible = true;
            this.WC_NAME.Width = 138;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "gridBand8";
            this.gridBand8.Columns.Add(this.ME_SNAME);
            this.gridBand8.Columns.Add(this.ME_SCODE);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 3;
            this.gridBand8.Width = 99;
            // 
            // ME_SNAME
            // 
            this.ME_SNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SNAME.AppearanceCell.Options.UseFont = true;
            this.ME_SNAME.AppearanceCell.Options.UseTextOptions = true;
            this.ME_SNAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ME_SNAME.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ME_SNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SNAME.AppearanceHeader.Options.UseFont = true;
            this.ME_SNAME.Caption = "자재유형";
            this.ME_SNAME.FieldName = "ME_SNAME";
            this.ME_SNAME.Name = "ME_SNAME";
            this.ME_SNAME.RowCount = 4;
            this.ME_SNAME.Visible = true;
            this.ME_SNAME.Width = 99;
            // 
            // ME_SCODE
            // 
            this.ME_SCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SCODE.AppearanceCell.Options.UseFont = true;
            this.ME_SCODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SCODE.AppearanceHeader.Options.UseFont = true;
            this.ME_SCODE.Caption = "자재유형코드";
            this.ME_SCODE.FieldName = "ME_SCODE";
            this.ME_SCODE.Name = "ME_SCODE";
            this.ME_SCODE.RowCount = 4;
            this.ME_SCODE.Width = 101;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(382, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(78, 79);
            this.labelControl1.TabIndex = 61;
            this.labelControl1.Text = "COUNT";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl2.Location = new System.Drawing.Point(3, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 79);
            this.labelControl2.TabIndex = 61;
            this.labelControl2.Text = "실사\r\n위치";
            // 
            // lueLoc_code
            // 
            this.lueLoc_code.Location = new System.Drawing.Point(73, 3);
            this.lueLoc_code.Name = "lueLoc_code";
            this.lueLoc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_code.Properties.Appearance.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueLoc_code.Properties.AutoHeight = false;
            this.lueLoc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_CODE", "위치코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_NAME", "위치명")});
            this.lueLoc_code.Properties.NullText = "";
            this.lueLoc_code.Size = new System.Drawing.Size(303, 79);
            this.lueLoc_code.TabIndex = 60;
            // 
            // btn_inspection
            // 
            this.btn_inspection.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btn_inspection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_inspection.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_inspection.Location = new System.Drawing.Point(1095, 2);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(172, 79);
            this.btn_inspection.TabIndex = 15;
            this.btn_inspection.Text = "실사전송";
            this.btn_inspection.UseVisualStyleBackColor = true;
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1108, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(172, 79);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(625, 43);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(529, 46);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 11;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "리더기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(1159, 46);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(1108, 43);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.dsmListView_ivt.Location = new System.Drawing.Point(901, 28);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(410, 175);
            this.dsmListView_ivt.TabIndex = 12;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(461, 10);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 21;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(780, 42);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 22;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Inspection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dsmListView_ivt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Inspection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Injection";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Injection_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtIt_scode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Button button_inventory_start;
        private System.Windows.Forms.Label label1;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueLoc_code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SITE_CODE;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn IT_SNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 소요량;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 재고수량;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 부족분;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn WC_SCODE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn WC_NAME;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ME_SNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ME_SCODE;
        private DevExpress.XtraEditors.LabelControl lbl_cnt;
        private DevExpress.XtraGrid.Columns.GridColumn WC_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn CARD_NO;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn SIZE;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_inspection;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn QRB_IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QRB_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QRB_SUNIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn QRB_ME_SCODE;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn_item_delete;
        private DevExpress.XtraEditors.SimpleButton btn_item_select;
        private DevExpress.XtraEditors.TextEdit txtIt_scode;
    }
}

