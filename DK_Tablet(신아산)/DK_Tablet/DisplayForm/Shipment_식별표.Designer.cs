﻿namespace DK_Tablet
{
    partial class Shipment_식별표
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.QR_IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SEQ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_LOT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INPUT_SQTY = new System.Windows.Forms.DataGridViewButtonColumn();
            this.QR_CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.button3 = new System.Windows.Forms.Button();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lueLoc_Car_code = new DevExpress.XtraEditors.LookUpEdit();
            this.lueLoc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.PP_PRDT_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RSRV_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PP_VW_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PP_CARRIER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PP_CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MM_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SQTY = new System.Windows.Forms.DataGridViewButtonColumn();
            this.PP_CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PP_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btn_shipment_search = new System.Windows.Forms.Button();
            this.btn_shipment = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SH_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SERNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_PKQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CARRIER_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SUM_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SH_DESCR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.lbl_Cnt3 = new System.Windows.Forms.Label();
            this.lbl_Cnt2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_Car_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(104, 46);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(257, 40);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(367, 46);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(448, 46);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.lueLoc_Car_code);
            this.panel1.Controls.Add(this.lueLoc_code);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dataGridView3);
            this.panel1.Controls.Add(this.btn_shipment_search);
            this.panel1.Controls.Add(this.btn_shipment);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.gridControl1);
            this.panel1.Controls.Add(this.lbl_Cnt3);
            this.panel1.Controls.Add(this.lbl_Cnt2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.gridControl2);
            this.panel1.Location = new System.Drawing.Point(12, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 664);
            this.panel1.TabIndex = 8;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 50;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QR_IT_SCODE,
            this.QR_SEQ,
            this.QR_LOT_NO,
            this.QR_SQTY,
            this.INPUT_SQTY,
            this.QR_CHECK_YN,
            this.QR_SDATE,
            this.QR_BTN_DELETE,
            this.CHECK_YN});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Location = new System.Drawing.Point(3, 66);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 10F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 24;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 20F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.dataGridView1.RowTemplate.Height = 50;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1251, 506);
            this.dataGridView1.TabIndex = 63;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // QR_IT_SCODE
            // 
            this.QR_IT_SCODE.DataPropertyName = "QR_IT_SCODE";
            this.QR_IT_SCODE.HeaderText = "품목";
            this.QR_IT_SCODE.Name = "QR_IT_SCODE";
            this.QR_IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_IT_SCODE.Width = 320;
            // 
            // QR_SEQ
            // 
            this.QR_SEQ.DataPropertyName = "QR_SEQ";
            this.QR_SEQ.HeaderText = "SEQ";
            this.QR_SEQ.Name = "QR_SEQ";
            this.QR_SEQ.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_SEQ.Width = 200;
            // 
            // QR_LOT_NO
            // 
            this.QR_LOT_NO.DataPropertyName = "QR_LOT_NO";
            this.QR_LOT_NO.HeaderText = "LOT";
            this.QR_LOT_NO.Name = "QR_LOT_NO";
            // 
            // QR_SQTY
            // 
            this.QR_SQTY.DataPropertyName = "QR_SQTY";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.QR_SQTY.DefaultCellStyle = dataGridViewCellStyle2;
            this.QR_SQTY.HeaderText = "기존수량";
            this.QR_SQTY.Name = "QR_SQTY";
            this.QR_SQTY.Width = 180;
            // 
            // INPUT_SQTY
            // 
            this.INPUT_SQTY.DataPropertyName = "INPUT_SQTY";
            this.INPUT_SQTY.HeaderText = "납품수량";
            this.INPUT_SQTY.Name = "INPUT_SQTY";
            this.INPUT_SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.INPUT_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.INPUT_SQTY.Width = 180;
            // 
            // QR_CHECK_YN
            // 
            this.QR_CHECK_YN.DataPropertyName = "QR_CHECK_YN";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.QR_CHECK_YN.DefaultCellStyle = dataGridViewCellStyle3;
            this.QR_CHECK_YN.HeaderText = "체크";
            this.QR_CHECK_YN.Name = "QR_CHECK_YN";
            // 
            // QR_SDATE
            // 
            this.QR_SDATE.DataPropertyName = "QR_SDATE";
            this.QR_SDATE.HeaderText = "등록일";
            this.QR_SDATE.Name = "QR_SDATE";
            this.QR_SDATE.Visible = false;
            // 
            // QR_BTN_DELETE
            // 
            this.QR_BTN_DELETE.DataPropertyName = "QR_BTN_DELETE";
            this.QR_BTN_DELETE.HeaderText = "삭제";
            this.QR_BTN_DELETE.Name = "QR_BTN_DELETE";
            this.QR_BTN_DELETE.Text = "삭제";
            this.QR_BTN_DELETE.UseColumnTextForButtonValue = true;
            this.QR_BTN_DELETE.Width = 150;
            // 
            // CHECK_YN
            // 
            this.CHECK_YN.DataPropertyName = "CHECK_YN";
            this.CHECK_YN.HeaderText = "출하체크";
            this.CHECK_YN.Name = "CHECK_YN";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(565, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 59);
            this.labelControl1.TabIndex = 61;
            this.labelControl1.Text = "기사\r\n선택";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(913, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 59);
            this.button3.TabIndex = 22;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl2.Location = new System.Drawing.Point(214, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 59);
            this.labelControl2.TabIndex = 61;
            this.labelControl2.Text = "이동\r\n위치";
            // 
            // lueLoc_Car_code
            // 
            this.lueLoc_Car_code.Location = new System.Drawing.Point(637, 4);
            this.lueLoc_Car_code.Name = "lueLoc_Car_code";
            this.lueLoc_Car_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_Car_code.Properties.Appearance.Options.UseFont = true;
            this.lueLoc_Car_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_Car_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueLoc_Car_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_Car_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueLoc_Car_code.Properties.AutoHeight = false;
            this.lueLoc_Car_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoc_Car_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoc_Car_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DV_CODE", "기사코드", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DV_NAME", "기사이름"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LC_CODE", "물류회사코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LC_NAME", "물류회사이름")});
            this.lueLoc_Car_code.Properties.NullText = "";
            this.lueLoc_Car_code.Size = new System.Drawing.Size(270, 59);
            this.lueLoc_Car_code.TabIndex = 60;
            this.lueLoc_Car_code.EditValueChanged += new System.EventHandler(this.lueLoc_Car_code_EditValueChanged);
            // 
            // lueLoc_code
            // 
            this.lueLoc_code.Location = new System.Drawing.Point(286, 4);
            this.lueLoc_code.Name = "lueLoc_code";
            this.lueLoc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_code.Properties.Appearance.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 30F);
            this.lueLoc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueLoc_code.Properties.AutoHeight = false;
            this.lueLoc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_CODE", "위치코드", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_NAME", "위치명")});
            this.lueLoc_code.Properties.NullText = "";
            this.lueLoc_code.Size = new System.Drawing.Size(270, 59);
            this.lueLoc_code.TabIndex = 60;
            this.lueLoc_code.EditValueChanged += new System.EventHandler(this.lueLoc_code_EditValueChanged);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(4, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 59);
            this.label5.TabIndex = 55;
            this.label5.Text = "PP카드";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView3.ColumnHeadersHeight = 50;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PP_PRDT_ITEM,
            this.RSRV_NO,
            this.PP_VW_SNUMB,
            this.PP_CARRIER_NO,
            this.PP_CARD_NO,
            this.MM_SQTY,
            this.SQTY,
            this.PP_CHECK_YN,
            this.PP_DATE,
            this.BTN_DELETE});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView3.Location = new System.Drawing.Point(3, 66);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("굴림", 10F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowHeadersWidth = 24;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("굴림", 20F);
            this.dataGridView3.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView3.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.dataGridView3.RowTemplate.Height = 50;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(1251, 503);
            this.dataGridView3.TabIndex = 54;
            this.dataGridView3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellClick);
            // 
            // PP_PRDT_ITEM
            // 
            this.PP_PRDT_ITEM.DataPropertyName = "PRDT_ITEM";
            this.PP_PRDT_ITEM.HeaderText = "품목";
            this.PP_PRDT_ITEM.Name = "PP_PRDT_ITEM";
            this.PP_PRDT_ITEM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PP_PRDT_ITEM.Width = 320;
            // 
            // RSRV_NO
            // 
            this.RSRV_NO.DataPropertyName = "RSRV_NO";
            this.RSRV_NO.HeaderText = "생산계획번호";
            this.RSRV_NO.Name = "RSRV_NO";
            this.RSRV_NO.Visible = false;
            // 
            // PP_VW_SNUMB
            // 
            this.PP_VW_SNUMB.DataPropertyName = "VW_SNUMB";
            this.PP_VW_SNUMB.HeaderText = "가실적번호";
            this.PP_VW_SNUMB.Name = "PP_VW_SNUMB";
            this.PP_VW_SNUMB.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PP_VW_SNUMB.Visible = false;
            // 
            // PP_CARRIER_NO
            // 
            this.PP_CARRIER_NO.DataPropertyName = "CARRIER_NO";
            this.PP_CARRIER_NO.HeaderText = "대차";
            this.PP_CARRIER_NO.Name = "PP_CARRIER_NO";
            this.PP_CARRIER_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PP_CARRIER_NO.Visible = false;
            this.PP_CARRIER_NO.Width = 110;
            // 
            // PP_CARD_NO
            // 
            this.PP_CARD_NO.DataPropertyName = "CARD_NO";
            this.PP_CARD_NO.HeaderText = "카드";
            this.PP_CARD_NO.Name = "PP_CARD_NO";
            this.PP_CARD_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PP_CARD_NO.Width = 200;
            // 
            // MM_SQTY
            // 
            this.MM_SQTY.DataPropertyName = "MM_SQTY";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.MM_SQTY.DefaultCellStyle = dataGridViewCellStyle8;
            this.MM_SQTY.HeaderText = "기존수량";
            this.MM_SQTY.Name = "MM_SQTY";
            this.MM_SQTY.Width = 180;
            // 
            // SQTY
            // 
            this.SQTY.DataPropertyName = "SQTY";
            this.SQTY.HeaderText = "납품수량";
            this.SQTY.Name = "SQTY";
            this.SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.SQTY.Width = 180;
            // 
            // PP_CHECK_YN
            // 
            this.PP_CHECK_YN.DataPropertyName = "CHECK_YN";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.PP_CHECK_YN.DefaultCellStyle = dataGridViewCellStyle9;
            this.PP_CHECK_YN.HeaderText = "체크";
            this.PP_CHECK_YN.Name = "PP_CHECK_YN";
            // 
            // PP_DATE
            // 
            this.PP_DATE.DataPropertyName = "PP_DATE";
            this.PP_DATE.HeaderText = "등록일";
            this.PP_DATE.Name = "PP_DATE";
            this.PP_DATE.Visible = false;
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.DataPropertyName = "BTN_DELETE";
            this.BTN_DELETE.HeaderText = "삭제";
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.Text = "삭제";
            this.BTN_DELETE.UseColumnTextForButtonValue = true;
            this.BTN_DELETE.Width = 150;
            // 
            // btn_shipment_search
            // 
            this.btn_shipment_search.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btn_shipment_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_shipment_search.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_shipment_search.Location = new System.Drawing.Point(3, 574);
            this.btn_shipment_search.Name = "btn_shipment_search";
            this.btn_shipment_search.Size = new System.Drawing.Size(211, 85);
            this.btn_shipment_search.TabIndex = 15;
            this.btn_shipment_search.Text = "출하지시선택";
            this.btn_shipment_search.UseVisualStyleBackColor = true;
            this.btn_shipment_search.Visible = false;
            this.btn_shipment_search.Click += new System.EventHandler(this.btn_shipment_search_Click);
            // 
            // btn_shipment
            // 
            this.btn_shipment.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btn_shipment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_shipment.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_shipment.Location = new System.Drawing.Point(857, 574);
            this.btn_shipment.Name = "btn_shipment";
            this.btn_shipment.Size = new System.Drawing.Size(198, 85);
            this.btn_shipment.TabIndex = 15;
            this.btn_shipment.Text = "출하";
            this.btn_shipment.UseVisualStyleBackColor = true;
            this.btn_shipment.Click += new System.EventHandler(this.btn_shipment_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1058, 574);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 85);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridControl1.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.gridControl1.Font = new System.Drawing.Font("굴림", 15F);
            this.gridControl1.Location = new System.Drawing.Point(3, 47);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gridControl1.Size = new System.Drawing.Size(628, 522);
            this.gridControl1.TabIndex = 57;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Visible = false;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.Lime;
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SH_SNUMB,
            this.SH_SERNO,
            this.IT_SCODE,
            this.IT_SNAME,
            this.SH_PKQTY,
            this.CARRIER_SQTY,
            this.SH_SQTY,
            this.SUM_SQTY,
            this.SH_DESCR});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsPrint.ExpandAllGroups = false;
            this.gridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 40;
            // 
            // SH_SNUMB
            // 
            this.SH_SNUMB.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_SNUMB.AppearanceCell.Options.UseFont = true;
            this.SH_SNUMB.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_SNUMB.AppearanceHeader.Options.UseFont = true;
            this.SH_SNUMB.Caption = "등록번호";
            this.SH_SNUMB.FieldName = "SH_SNUMB";
            this.SH_SNUMB.Name = "SH_SNUMB";
            // 
            // SH_SERNO
            // 
            this.SH_SERNO.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_SERNO.AppearanceHeader.Options.UseFont = true;
            this.SH_SERNO.Caption = "시리얼";
            this.SH_SERNO.FieldName = "SH_SERNO";
            this.SH_SERNO.Name = "SH_SERNO";
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.IT_SCODE.AppearanceCell.Options.UseFont = true;
            this.IT_SCODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.IT_SCODE.AppearanceHeader.Options.UseFont = true;
            this.IT_SCODE.Caption = "품목코드";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 0;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.IT_SNAME.AppearanceCell.Options.UseFont = true;
            this.IT_SNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.IT_SNAME.AppearanceHeader.Options.UseFont = true;
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 1;
            // 
            // SH_PKQTY
            // 
            this.SH_PKQTY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_PKQTY.AppearanceCell.Options.UseFont = true;
            this.SH_PKQTY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_PKQTY.AppearanceHeader.Options.UseFont = true;
            this.SH_PKQTY.Caption = "포장수량";
            this.SH_PKQTY.FieldName = "SH_PKQTY";
            this.SH_PKQTY.Name = "SH_PKQTY";
            this.SH_PKQTY.Visible = true;
            this.SH_PKQTY.VisibleIndex = 2;
            // 
            // CARRIER_SQTY
            // 
            this.CARRIER_SQTY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.CARRIER_SQTY.AppearanceCell.Options.UseFont = true;
            this.CARRIER_SQTY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.CARRIER_SQTY.AppearanceHeader.Options.UseFont = true;
            this.CARRIER_SQTY.Caption = "PLT(대차)";
            this.CARRIER_SQTY.FieldName = "CARRIER_SQTY";
            this.CARRIER_SQTY.Name = "CARRIER_SQTY";
            this.CARRIER_SQTY.Visible = true;
            this.CARRIER_SQTY.VisibleIndex = 3;
            // 
            // SH_SQTY
            // 
            this.SH_SQTY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_SQTY.AppearanceCell.Options.UseFont = true;
            this.SH_SQTY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_SQTY.AppearanceHeader.Options.UseFont = true;
            this.SH_SQTY.Caption = "출하수량";
            this.SH_SQTY.FieldName = "SH_SQTY";
            this.SH_SQTY.Name = "SH_SQTY";
            this.SH_SQTY.Visible = true;
            this.SH_SQTY.VisibleIndex = 4;
            // 
            // SUM_SQTY
            // 
            this.SUM_SQTY.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SUM_SQTY.AppearanceCell.Options.UseFont = true;
            this.SUM_SQTY.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SUM_SQTY.AppearanceHeader.Options.UseFont = true;
            this.SUM_SQTY.Caption = "합계수량";
            this.SUM_SQTY.FieldName = "SUM_SQTY";
            this.SUM_SQTY.Name = "SUM_SQTY";
            this.SUM_SQTY.Visible = true;
            this.SUM_SQTY.VisibleIndex = 5;
            // 
            // SH_DESCR
            // 
            this.SH_DESCR.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_DESCR.AppearanceCell.Options.UseFont = true;
            this.SH_DESCR.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SH_DESCR.AppearanceHeader.Options.UseFont = true;
            this.SH_DESCR.Caption = "비고";
            this.SH_DESCR.FieldName = "SH_DESCR";
            this.SH_DESCR.Name = "SH_DESCR";
            this.SH_DESCR.Visible = true;
            this.SH_DESCR.VisibleIndex = 6;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // lbl_Cnt3
            // 
            this.lbl_Cnt3.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_Cnt3.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.lbl_Cnt3.ForeColor = System.Drawing.Color.Red;
            this.lbl_Cnt3.Location = new System.Drawing.Point(585, 590);
            this.lbl_Cnt3.Name = "lbl_Cnt3";
            this.lbl_Cnt3.Size = new System.Drawing.Size(110, 59);
            this.lbl_Cnt3.TabIndex = 55;
            this.lbl_Cnt3.Text = "0";
            this.lbl_Cnt3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Cnt3.Visible = false;
            // 
            // lbl_Cnt2
            // 
            this.lbl_Cnt2.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_Cnt2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.lbl_Cnt2.ForeColor = System.Drawing.Color.Red;
            this.lbl_Cnt2.Location = new System.Drawing.Point(1148, 4);
            this.lbl_Cnt2.Name = "lbl_Cnt2";
            this.lbl_Cnt2.Size = new System.Drawing.Size(106, 59);
            this.lbl_Cnt2.TabIndex = 55;
            this.lbl_Cnt2.Text = "0";
            this.lbl_Cnt2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Cnt2.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(1078, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 59);
            this.label6.TabIndex = 55;
            this.label6.Text = "행수 : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 44);
            this.label3.TabIndex = 56;
            this.label3.Text = "1. 출하지시";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Gainsboro;
            this.label2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(561, 590);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 59);
            this.label2.TabIndex = 55;
            this.label2.Text = "/";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // gridControl2
            // 
            gridLevelNode1.RelationName = "Level1";
            this.gridControl2.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl2.Location = new System.Drawing.Point(761, 9);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(430, 523);
            this.gridControl2.TabIndex = 62;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            this.gridControl2.Visible = false;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView2.OptionsPrint.PrintFilterInfo = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn1.Caption = "자재번호";
            this.gridColumn1.FieldName = "IT_SCODE";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "수량";
            this.gridColumn2.FieldName = "SH_SQTY";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "초도품";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.dsmListView_ivt.Location = new System.Drawing.Point(855, 10);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(809, 268);
            this.dsmListView_ivt.TabIndex = 12;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(625, 43);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(529, 46);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 11;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "리더기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(1159, 46);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(1108, 43);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(461, 10);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 21;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(774, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(53, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(348, 21);
            this.textBox1.TabIndex = 23;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(408, 15);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Shipment_식별표
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dsmListView_ivt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Shipment_식별표";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shipment";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Injection_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_Car_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Button button_inventory_start;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label label1;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Timer timer_now;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_shipment;
        private System.Windows.Forms.Label lbl_Cnt2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SERNO;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn SH_PKQTY;
        private DevExpress.XtraGrid.Columns.GridColumn CARRIER_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn SH_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn SH_DESCR;
        private System.Windows.Forms.Button btn_shipment_search;
        private DevExpress.XtraGrid.Columns.GridColumn SUM_SQTY;
        private System.Windows.Forms.Label lbl_Cnt3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit lueLoc_code;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit lueLoc_Car_code;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_PRDT_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn RSRV_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_VW_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_CARRIER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MM_SQTY;
        private System.Windows.Forms.DataGridViewButtonColumn SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_CHECK_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn PP_DATE;
        private System.Windows.Forms.DataGridViewButtonColumn BTN_DELETE;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SEQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_LOT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SQTY;
        private System.Windows.Forms.DataGridViewButtonColumn INPUT_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_CHECK_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SDATE;
        private System.Windows.Forms.DataGridViewButtonColumn QR_BTN_DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_YN;
        private System.Windows.Forms.Button button3;
    }
}

