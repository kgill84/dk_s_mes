﻿namespace DK_Tablet
{
    partial class Metarial_Move_2
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lueLoc_Code = new DevExpress.XtraEditors.LookUpEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_where = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CHILD_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.WH_REMAIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.공정재고 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.소요량 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.부족분 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.불출수량 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ME_SNAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ME_SCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.btn_clear = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.LOC_CODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btn_move = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lueMe_scode = new DevExpress.XtraEditors.LookUpEdit();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMe_scode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(104, 46);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(257, 40);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(367, 46);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(448, 46);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lueLoc_Code);
            this.panel1.Controls.Add(this.simpleButton1);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.btn_where);
            this.panel1.Controls.Add(this.gridControl1);
            this.panel1.Controls.Add(this.btn_move);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.lueMe_scode);
            this.panel1.Location = new System.Drawing.Point(12, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 664);
            this.panel1.TabIndex = 8;
            // 
            // lueLoc_Code
            // 
            this.lueLoc_Code.Location = new System.Drawing.Point(210, 3);
            this.lueLoc_Code.Name = "lueLoc_Code";
            this.lueLoc_Code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_Code.Properties.Appearance.Options.UseFont = true;
            this.lueLoc_Code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_Code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueLoc_Code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueLoc_Code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueLoc_Code.Properties.AutoHeight = false;
            this.lueLoc_Code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueLoc_Code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueLoc_Code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_CODE", "코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("LOC_NAME", "명")});
            this.lueLoc_Code.Properties.NullText = "";
            this.lueLoc_Code.Size = new System.Drawing.Size(349, 80);
            this.lueLoc_Code.TabIndex = 14;
            this.lueLoc_Code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton1.Location = new System.Drawing.Point(565, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(124, 80);
            this.simpleButton1.TabIndex = 60;
            this.simpleButton1.Text = "전체";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton2.Location = new System.Drawing.Point(695, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(124, 80);
            this.simpleButton2.TabIndex = 60;
            this.simpleButton2.Text = "불출\r\n현황";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(8, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 79);
            this.labelControl1.TabIndex = 59;
            this.labelControl1.Text = "검색\r\n조건";
            // 
            // btn_where
            // 
            this.btn_where.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_where.Appearance.Options.UseFont = true;
            this.btn_where.Location = new System.Drawing.Point(80, 2);
            this.btn_where.Name = "btn_where";
            this.btn_where.Size = new System.Drawing.Size(124, 80);
            this.btn_where.TabIndex = 58;
            this.btn_where.Tag = "0";
            this.btn_where.Text = "작업장";
            this.btn_where.Click += new System.EventHandler(this.btn_where_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Font = new System.Drawing.Font("굴림", 9F);
            this.gridControl1.Location = new System.Drawing.Point(3, 88);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1261, 571);
            this.gridControl1.TabIndex = 57;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.PaleGreen;
            this.advBandedGridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.MintCream;
            this.advBandedGridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.advBandedGridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.Lime;
            this.advBandedGridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.advBandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand11,
            this.gridBand9,
            this.gridBand1,
            this.gridBand5,
            this.gridBand13,
            this.gridBand3,
            this.gridBand14,
            this.gridBand15,
            this.gridBand16,
            this.gridBand7,
            this.gridBand8,
            this.gridBand17});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.CHILD_ITEM,
            this.IT_SNAME,
            this.소요량,
            this.공정재고,
            this.부족분,
            this.ME_SCODE,
            this.ME_SNAME,
            this.LOC_CODE,
            this.WH_REMAIN,
            this.불출수량,
            this.btn_clear});
            this.advBandedGridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1150, 444, 216, 216);
            this.advBandedGridView1.DefaultRelationIndex = 1;
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.IndicatorWidth = 40;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsBehavior.Editable = false;
            this.advBandedGridView1.OptionsCustomization.AllowFilter = false;
            this.advBandedGridView1.OptionsPrint.ExpandAllGroups = false;
            this.advBandedGridView1.OptionsPrint.PrintBandHeader = false;
            this.advBandedGridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.ShowBands = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.advBandedGridView1_RowCellClick);
            this.advBandedGridView1.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.advBandedGridView1_CustomDrawRowIndicator);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Columns.Add(this.CHILD_ITEM);
            this.gridBand2.Columns.Add(this.IT_SNAME);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 308;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.CHILD_ITEM.AppearanceCell.Options.UseFont = true;
            this.CHILD_ITEM.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.CHILD_ITEM.AppearanceHeader.Options.UseFont = true;
            this.CHILD_ITEM.Caption = "품목코드";
            this.CHILD_ITEM.FieldName = "CHILD_ITEM";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.RowCount = 2;
            this.CHILD_ITEM.Visible = true;
            this.CHILD_ITEM.Width = 308;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.IT_SNAME.AppearanceCell.Options.UseFont = true;
            this.IT_SNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.IT_SNAME.AppearanceHeader.Options.UseFont = true;
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.RowCount = 2;
            this.IT_SNAME.RowIndex = 1;
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.Width = 308;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "gridBand11";
            this.gridBand11.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand12});
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.Visible = false;
            this.gridBand11.VisibleIndex = -1;
            this.gridBand11.Width = 130;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "gridBand12";
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 0;
            this.gridBand12.Width = 130;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand10});
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.Visible = false;
            this.gridBand9.VisibleIndex = -1;
            this.gridBand9.Width = 131;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "gridBand10";
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 0;
            this.gridBand10.Width = 131;
            // 
            // gridBand1
            // 
            this.gridBand1.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand4});
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.Visible = false;
            this.gridBand1.VisibleIndex = -1;
            this.gridBand1.Width = 146;
            // 
            // gridBand4
            // 
            this.gridBand4.Caption = "gridBand4";
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 0;
            this.gridBand4.Width = 146;
            // 
            // gridBand5
            // 
            this.gridBand5.Caption = "gridBand5";
            this.gridBand5.Children.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand6});
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.Visible = false;
            this.gridBand5.VisibleIndex = -1;
            this.gridBand5.Width = 168;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 0;
            this.gridBand6.Width = 168;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Columns.Add(this.WH_REMAIN);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 1;
            this.gridBand13.Width = 120;
            // 
            // WH_REMAIN
            // 
            this.WH_REMAIN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.WH_REMAIN.AppearanceCell.Options.UseFont = true;
            this.WH_REMAIN.AppearanceCell.Options.UseTextOptions = true;
            this.WH_REMAIN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WH_REMAIN.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.WH_REMAIN.AppearanceHeader.Options.UseFont = true;
            this.WH_REMAIN.AppearanceHeader.Options.UseTextOptions = true;
            this.WH_REMAIN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WH_REMAIN.Caption = "창고재고";
            this.WH_REMAIN.DisplayFormat.FormatString = "n0";
            this.WH_REMAIN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.WH_REMAIN.FieldName = "WH_REMAIN";
            this.WH_REMAIN.Name = "WH_REMAIN";
            this.WH_REMAIN.RowCount = 4;
            this.WH_REMAIN.Visible = true;
            this.WH_REMAIN.Width = 120;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.Visible = false;
            this.gridBand3.VisibleIndex = -1;
            this.gridBand3.Width = 77;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "gridBand14";
            this.gridBand14.Columns.Add(this.공정재고);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 2;
            this.gridBand14.Width = 116;
            // 
            // 공정재고
            // 
            this.공정재고.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.공정재고.AppearanceCell.Options.UseFont = true;
            this.공정재고.AppearanceCell.Options.UseTextOptions = true;
            this.공정재고.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.공정재고.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.공정재고.AppearanceHeader.Options.UseFont = true;
            this.공정재고.AppearanceHeader.Options.UseTextOptions = true;
            this.공정재고.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.공정재고.Caption = "공정재고";
            this.공정재고.DisplayFormat.FormatString = "n0";
            this.공정재고.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.공정재고.FieldName = "공정재고";
            this.공정재고.Name = "공정재고";
            this.공정재고.RowCount = 4;
            this.공정재고.Visible = true;
            this.공정재고.Width = 116;
            // 
            // gridBand15
            // 
            this.gridBand15.Caption = "gridBand15";
            this.gridBand15.Columns.Add(this.소요량);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 3;
            this.gridBand15.Width = 118;
            // 
            // 소요량
            // 
            this.소요량.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.소요량.AppearanceCell.Options.UseFont = true;
            this.소요량.AppearanceCell.Options.UseTextOptions = true;
            this.소요량.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.소요량.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.소요량.AppearanceHeader.Options.UseFont = true;
            this.소요량.AppearanceHeader.Options.UseTextOptions = true;
            this.소요량.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.소요량.Caption = "소요량";
            this.소요량.DisplayFormat.FormatString = "n0";
            this.소요량.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.소요량.FieldName = "소요량";
            this.소요량.Name = "소요량";
            this.소요량.RowCount = 4;
            this.소요량.Visible = true;
            this.소요량.Width = 118;
            // 
            // gridBand16
            // 
            this.gridBand16.Caption = "gridBand16";
            this.gridBand16.Columns.Add(this.부족분);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 4;
            this.gridBand16.Width = 125;
            // 
            // 부족분
            // 
            this.부족분.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.부족분.AppearanceCell.Options.UseFont = true;
            this.부족분.AppearanceCell.Options.UseTextOptions = true;
            this.부족분.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.부족분.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.부족분.AppearanceHeader.Options.UseFont = true;
            this.부족분.AppearanceHeader.Options.UseTextOptions = true;
            this.부족분.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.부족분.Caption = "부족수량";
            this.부족분.DisplayFormat.FormatString = "n0";
            this.부족분.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.부족분.FieldName = "부족분";
            this.부족분.Name = "부족분";
            this.부족분.RowCount = 4;
            this.부족분.Visible = true;
            this.부족분.Width = 125;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Columns.Add(this.불출수량);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 5;
            this.gridBand7.Width = 127;
            // 
            // 불출수량
            // 
            this.불출수량.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.불출수량.AppearanceCell.Options.UseFont = true;
            this.불출수량.AppearanceCell.Options.UseTextOptions = true;
            this.불출수량.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.불출수량.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.불출수량.AppearanceHeader.Options.UseFont = true;
            this.불출수량.Caption = "불출수량";
            this.불출수량.DisplayFormat.FormatString = "n0";
            this.불출수량.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.불출수량.FieldName = "불출수량";
            this.불출수량.Name = "불출수량";
            this.불출수량.RowCount = 4;
            this.불출수량.Visible = true;
            this.불출수량.Width = 127;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "gridBand8";
            this.gridBand8.Columns.Add(this.ME_SNAME);
            this.gridBand8.Columns.Add(this.ME_SCODE);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 6;
            this.gridBand8.Width = 135;
            // 
            // ME_SNAME
            // 
            this.ME_SNAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SNAME.AppearanceCell.Options.UseFont = true;
            this.ME_SNAME.AppearanceCell.Options.UseTextOptions = true;
            this.ME_SNAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ME_SNAME.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ME_SNAME.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SNAME.AppearanceHeader.Options.UseFont = true;
            this.ME_SNAME.Caption = "자재유형";
            this.ME_SNAME.FieldName = "ME_SNAME";
            this.ME_SNAME.Name = "ME_SNAME";
            this.ME_SNAME.RowCount = 4;
            this.ME_SNAME.Visible = true;
            this.ME_SNAME.Width = 135;
            // 
            // ME_SCODE
            // 
            this.ME_SCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SCODE.AppearanceCell.Options.UseFont = true;
            this.ME_SCODE.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.ME_SCODE.AppearanceHeader.Options.UseFont = true;
            this.ME_SCODE.Caption = "자재유형코드";
            this.ME_SCODE.FieldName = "ME_SCODE";
            this.ME_SCODE.Name = "ME_SCODE";
            this.ME_SCODE.RowCount = 4;
            this.ME_SCODE.Width = 101;
            // 
            // gridBand17
            // 
            this.gridBand17.Caption = "gridBand17";
            this.gridBand17.Columns.Add(this.btn_clear);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 7;
            this.gridBand17.Width = 133;
            // 
            // btn_clear
            // 
            this.btn_clear.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 22F);
            this.btn_clear.AppearanceCell.Options.UseFont = true;
            this.btn_clear.AppearanceCell.Options.UseTextOptions = true;
            this.btn_clear.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btn_clear.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 15F);
            this.btn_clear.AppearanceHeader.Options.UseFont = true;
            this.btn_clear.Caption = "정정";
            this.btn_clear.ColumnEdit = this.repositoryItemButtonEdit1;
            this.btn_clear.FieldName = "btn_clear";
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.OptionsColumn.AllowMove = false;
            this.btn_clear.OptionsColumn.AllowSize = false;
            this.btn_clear.OptionsEditForm.RowSpan = 4;
            this.btn_clear.OptionsFilter.AllowAutoFilter = false;
            this.btn_clear.OptionsFilter.AllowFilter = false;
            this.btn_clear.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.OptionsFilter.ImmediateUpdateAutoFilter = false;
            this.btn_clear.OptionsFilter.ImmediateUpdatePopupDateFilterOnCheck = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.OptionsFilter.ImmediateUpdatePopupDateFilterOnDateChange = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.OptionsFilter.ShowBlanksFilterItems = DevExpress.Utils.DefaultBoolean.False;
            this.btn_clear.RowCount = 4;
            this.btn_clear.Visible = true;
            this.btn_clear.Width = 133;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 15F);
            serializableAppearanceObject1.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", 50, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.NullText = "tete";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // LOC_CODE
            // 
            this.LOC_CODE.Caption = "로케이션 코드";
            this.LOC_CODE.FieldName = "LOC_CODE";
            this.LOC_CODE.Name = "LOC_CODE";
            this.LOC_CODE.Visible = true;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // btn_move
            // 
            this.btn_move.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btn_move.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_move.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_move.Location = new System.Drawing.Point(916, 3);
            this.btn_move.Name = "btn_move";
            this.btn_move.Size = new System.Drawing.Size(171, 85);
            this.btn_move.TabIndex = 15;
            this.btn_move.Text = "이동";
            this.btn_move.UseVisualStyleBackColor = true;
            this.btn_move.Click += new System.EventHandler(this.btn_move_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1093, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(171, 85);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lueMe_scode
            // 
            this.lueMe_scode.Location = new System.Drawing.Point(210, 3);
            this.lueMe_scode.Name = "lueMe_scode";
            this.lueMe_scode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueMe_scode.Properties.Appearance.Options.UseFont = true;
            this.lueMe_scode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueMe_scode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueMe_scode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueMe_scode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueMe_scode.Properties.AutoHeight = false;
            this.lueMe_scode.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueMe_scode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueMe_scode.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ME_SCODE", "자재유형코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ME_SNAME", "자재유형명")});
            this.lueMe_scode.Properties.NullText = "";
            this.lueMe_scode.Size = new System.Drawing.Size(349, 80);
            this.lueMe_scode.TabIndex = 14;
            this.lueMe_scode.Visible = false;
            this.lueMe_scode.EditValueChanged += new System.EventHandler(this.lueMe_scode_EditValueChanged);
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(625, 43);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(529, 46);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 11;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "리더기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(1159, 46);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(1108, 43);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26});
            this.dsmListView_ivt.Location = new System.Drawing.Point(856, 12);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(39, 74);
            this.dsmListView_ivt.TabIndex = 12;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // timer_now
            // 
            this.timer_now.Tick += new System.EventHandler(this.timer_now_Tick);
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(461, 10);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 21;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(787, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Metarial_Move_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dsmListView_ivt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Metarial_Move_2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Metarial_Move";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Injection_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueLoc_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueMe_scode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Button button_inventory_start;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit lueLoc_Code;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Timer timer_now;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private DevExpress.XtraEditors.LookUpEdit lueMe_scode;
        private System.Windows.Forms.Button btn_move;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn IT_SNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 소요량;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 공정재고;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 부족분;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ME_SNAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ME_SCODE;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_where;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LOC_CODE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn WH_REMAIN;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn 불출수량;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn btn_clear;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}

