﻿namespace DK_Tablet
{
    partial class Laser_JA_YB
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Laser_JA_YB));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ROWNUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VW_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARRIER_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRDT_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HARFTYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GOOD_QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtIt_scode = new System.Windows.Forms.TextBox();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.comboBox_ports2 = new System.Windows.Forms.ComboBox();
            this.button_com_open2 = new System.Windows.Forms.Button();
            this.button_com_close2 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.timer_po_start = new System.Windows.Forms.Timer(this.components);
            this.timer_pp_start = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_dt_input = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.progress_display = new System.Windows.Forms.Panel();
            this.txt_laser_state = new DevExpress.XtraEditors.TextEdit();
            this.txt_injection_state = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_po_release_Insert = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_worker_info = new System.Windows.Forms.Button();
            this.txt_night_sqty = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.sqty_cnt = new System.Windows.Forms.Label();
            this.txt_day_sqty = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_fail_qty = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txt_IT_MODEL = new System.Windows.Forms.Label();
            this.txtDayAndNight = new System.Windows.Forms.Label();
            this.txt_datetime = new System.Windows.Forms.Label();
            this.lbl_now_sqty = new System.Windows.Forms.Label();
            this.txt_good_qty = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txt_MO_SQUTY = new System.Windows.Forms.Label();
            this.txt_IT_SNAME = new System.Windows.Forms.Label();
            this.txt_IT_SCODE = new System.Windows.Forms.Label();
            this.btn_pp_card_search = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.btn_fail_input = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.btn_day_work = new System.Windows.Forms.Button();
            this.btn_meterial_search = new System.Windows.Forms.Button();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtNext_Carrier_no = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCard_no = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIn_Carrier_no = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Btn_Auto_Connection = new System.Windows.Forms.Button();
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.timer_re_carrier = new System.Windows.Forms.Timer(this.components);
            this.txtOut_carrier_no = new System.Windows.Forms.Button();
            this.btn_carrier_sqty = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_on_off = new DevExpress.XtraEditors.SimpleButton();
            this.button2 = new System.Windows.Forms.Button();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_fail_search = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView_target_list2 = new DK_Tablet.dsmListView(this.components);
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt2 = new Owf.Controls.DigitalDisplayControl();
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.dsmListView_ivt2 = new DK_Tablet.dsmListView(this.components);
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.button3 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btn_mix_toggle = new DevExpress.XtraEditors.SimpleButton();
            this.lbl혼적 = new DevExpress.XtraEditors.LabelControl();
            this.lab_carrier_sqty = new DevExpress.XtraEditors.LabelControl();
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader62 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader63 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader64 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader65 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader66 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader67 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader68 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader55 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader56 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader57 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader58 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader59 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader60 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader61 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader76 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader77 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader78 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader79 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader80 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader81 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader82 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader69 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader70 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader71 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader72 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader73 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader74 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader75 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label11 = new System.Windows.Forms.Label();
            this.txtBarcode = new DevExpress.XtraEditors.TextEdit();
            this.columnHeader90 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader91 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader92 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader93 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader94 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader95 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader96 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader83 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader84 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader85 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader86 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader87 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader88 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader89 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader104 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader105 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader106 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader107 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader108 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader109 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader110 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader97 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader98 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader99 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader100 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader101 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader102 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader103 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader118 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader119 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader120 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader121 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader122 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader123 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader124 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader111 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader112 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader113 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader114 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader115 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader116 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader117 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader132 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader133 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader134 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader135 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader136 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader137 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader138 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader125 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader126 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader127 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader128 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader129 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader130 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader131 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_inspection = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader146 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader147 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader148 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader149 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader150 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader151 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader152 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader139 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader140 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader141 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader142 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader143 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader144 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader145 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btn_program_finish = new DevExpress.XtraEditors.SimpleButton();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader160 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader161 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader162 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader163 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader164 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader165 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader166 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader153 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader154 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader155 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader156 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader157 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader158 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader159 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader174 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader175 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader176 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader177 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader178 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader179 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader180 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader167 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader168 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader169 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader170 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader171 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader172 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader173 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader181 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader182 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader183 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader184 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader185 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader186 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader187 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader188 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader189 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader190 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader191 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader192 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader193 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader194 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.progress_display.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_laser_state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_injection_state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(89, 1);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(64, 40);
            this.comboBox_ports.TabIndex = 1;
            this.comboBox_ports.Visible = false;
            // 
            // button_com_open
            // 
            this.button_com_open.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_open.Location = new System.Drawing.Point(159, 1);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Visible = false;
            // 
            // button_com_close
            // 
            this.button_com_close.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_close.Location = new System.Drawing.Point(240, 1);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 13F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ROWNUM,
            this.VW_SNUMB,
            this.CARRIER_DATE,
            this.CARD_NO,
            this.HH,
            this.PRDT_ITEM,
            this.LOT_NO,
            this.CHECK_YN,
            this.LOT_DATE,
            this.HARFTYPE,
            this.GOOD_QTY});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Location = new System.Drawing.Point(870, 88);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 35;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 13F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.RowTemplate.Height = 35;
            this.dataGridView1.Size = new System.Drawing.Size(396, 288);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // ROWNUM
            // 
            this.ROWNUM.DataPropertyName = "ROWNUM";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ROWNUM.DefaultCellStyle = dataGridViewCellStyle2;
            this.ROWNUM.FillWeight = 50.27934F;
            this.ROWNUM.HeaderText = "NO";
            this.ROWNUM.Name = "ROWNUM";
            this.ROWNUM.ReadOnly = true;
            this.ROWNUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ROWNUM.Width = 60;
            // 
            // VW_SNUMB
            // 
            this.VW_SNUMB.DataPropertyName = "VW_SNUMB";
            this.VW_SNUMB.HeaderText = "시리얼";
            this.VW_SNUMB.Name = "VW_SNUMB";
            this.VW_SNUMB.ReadOnly = true;
            this.VW_SNUMB.Visible = false;
            this.VW_SNUMB.Width = 70;
            // 
            // CARRIER_DATE
            // 
            this.CARRIER_DATE.DataPropertyName = "CARRIER_DATE";
            this.CARRIER_DATE.HeaderText = "날짜";
            this.CARRIER_DATE.Name = "CARRIER_DATE";
            this.CARRIER_DATE.ReadOnly = true;
            this.CARRIER_DATE.Visible = false;
            // 
            // CARD_NO
            // 
            this.CARD_NO.DataPropertyName = "CARD_NO";
            this.CARD_NO.FillWeight = 73.4309F;
            this.CARD_NO.HeaderText = "카드N0";
            this.CARD_NO.Name = "CARD_NO";
            this.CARD_NO.ReadOnly = true;
            this.CARD_NO.Width = 150;
            // 
            // HH
            // 
            this.HH.DataPropertyName = "HH";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HH.DefaultCellStyle = dataGridViewCellStyle3;
            this.HH.FillWeight = 71.10838F;
            this.HH.HeaderText = "경과시간";
            this.HH.Name = "HH";
            this.HH.ReadOnly = true;
            this.HH.Width = 160;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.DataPropertyName = "PRDT_ITEM";
            this.PRDT_ITEM.HeaderText = "품목코드";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.ReadOnly = true;
            this.PRDT_ITEM.Visible = false;
            // 
            // LOT_NO
            // 
            this.LOT_NO.DataPropertyName = "LOT_NO";
            this.LOT_NO.HeaderText = "LOT NO";
            this.LOT_NO.Name = "LOT_NO";
            this.LOT_NO.ReadOnly = true;
            this.LOT_NO.Visible = false;
            // 
            // CHECK_YN
            // 
            this.CHECK_YN.DataPropertyName = "CHECK_YN";
            this.CHECK_YN.HeaderText = "완료구분";
            this.CHECK_YN.Name = "CHECK_YN";
            this.CHECK_YN.ReadOnly = true;
            this.CHECK_YN.Visible = false;
            // 
            // LOT_DATE
            // 
            this.LOT_DATE.DataPropertyName = "LOT_DATE";
            this.LOT_DATE.HeaderText = "LOT 생성날짜";
            this.LOT_DATE.Name = "LOT_DATE";
            this.LOT_DATE.ReadOnly = true;
            this.LOT_DATE.Visible = false;
            // 
            // HARFTYPE
            // 
            this.HARFTYPE.DataPropertyName = "HARFTYPE";
            this.HARFTYPE.HeaderText = "주/야간";
            this.HARFTYPE.Name = "HARFTYPE";
            this.HARFTYPE.ReadOnly = true;
            this.HARFTYPE.Visible = false;
            // 
            // GOOD_QTY
            // 
            this.GOOD_QTY.DataPropertyName = "GOOD_QTY";
            this.GOOD_QTY.HeaderText = "GOOD_QTY";
            this.GOOD_QTY.Name = "GOOD_QTY";
            this.GOOD_QTY.ReadOnly = true;
            this.GOOD_QTY.Visible = false;
            // 
            // txtIt_scode
            // 
            this.txtIt_scode.Location = new System.Drawing.Point(421, 94);
            this.txtIt_scode.Name = "txtIt_scode";
            this.txtIt_scode.Size = new System.Drawing.Size(94, 21);
            this.txtIt_scode.TabIndex = 2;
            this.txtIt_scode.Visible = false;
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(302, 32);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // comboBox_ports2
            // 
            this.comboBox_ports2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports2.DropDownWidth = 500;
            this.comboBox_ports2.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports2.FormattingEnabled = true;
            this.comboBox_ports2.Location = new System.Drawing.Point(90, 48);
            this.comboBox_ports2.Name = "comboBox_ports2";
            this.comboBox_ports2.Size = new System.Drawing.Size(324, 40);
            this.comboBox_ports2.TabIndex = 1;
            // 
            // button_com_open2
            // 
            this.button_com_open2.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_open2.Location = new System.Drawing.Point(421, 48);
            this.button_com_open2.Name = "button_com_open2";
            this.button_com_open2.Size = new System.Drawing.Size(75, 40);
            this.button_com_open2.TabIndex = 2;
            this.button_com_open2.Text = "연결";
            this.button_com_open2.UseVisualStyleBackColor = true;
            this.button_com_open2.Click += new System.EventHandler(this.button_com_open_Click2);
            // 
            // button_com_close2
            // 
            this.button_com_close2.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_close2.Location = new System.Drawing.Point(502, 48);
            this.button_com_close2.Name = "button_com_close2";
            this.button_com_close2.Size = new System.Drawing.Size(75, 40);
            this.button_com_close2.TabIndex = 2;
            this.button_com_close2.Text = "해제";
            this.button_com_close2.UseVisualStyleBackColor = true;
            this.button_com_close2.Click += new System.EventHandler(this.button_com_close_Click2);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(12, 1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 39);
            this.label26.TabIndex = 11;
            this.label26.Text = "입구";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label26.Visible = false;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(12, 47);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 39);
            this.label27.TabIndex = 11;
            this.label27.Text = "리더기";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(12, 89);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(403, 77);
            this.lueWc_code.TabIndex = 15;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(261, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 42);
            this.button1.TabIndex = 16;
            this.button1.Text = "프린트TEST(모비스)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer_po_start
            // 
            this.timer_po_start.Interval = 1000;
            this.timer_po_start.Tick += new System.EventHandler(this.timer_po_start_Tick);
            // 
            // timer_pp_start
            // 
            this.timer_pp_start.Tick += new System.EventHandler(this.timer_pp_start_Tick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Yellow;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.btn_dt_input);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.progress_display);
            this.panel2.Controls.Add(this.btn_po_release_Insert);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btn_worker_info);
            this.panel2.Controls.Add(this.txt_night_sqty);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.sqty_cnt);
            this.panel2.Controls.Add(this.txt_day_sqty);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txt_fail_qty);
            this.panel2.Controls.Add(this.label56);
            this.panel2.Controls.Add(this.txt_IT_MODEL);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.txtDayAndNight);
            this.panel2.Controls.Add(this.txt_datetime);
            this.panel2.Controls.Add(this.lbl_now_sqty);
            this.panel2.Controls.Add(this.txt_good_qty);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.txt_MO_SQUTY);
            this.panel2.Controls.Add(this.txt_IT_SNAME);
            this.panel2.Controls.Add(this.txt_IT_SCODE);
            this.panel2.Controls.Add(this.btn_pp_card_search);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.label44);
            this.panel2.Controls.Add(this.btn_fail_input);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.btn_day_work);
            this.panel2.Controls.Add(this.btn_meterial_search);
            this.panel2.Controls.Add(this.btn_po_release);
            this.panel2.Controls.Add(this.label47);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.label50);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.label54);
            this.panel2.Controls.Add(this.label55);
            this.panel2.Location = new System.Drawing.Point(12, 170);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1271, 580);
            this.panel2.TabIndex = 18;
            // 
            // btn_dt_input
            // 
            this.btn_dt_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_dt_input.BackgroundImage")));
            this.btn_dt_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_dt_input.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_dt_input.Location = new System.Drawing.Point(14, 417);
            this.btn_dt_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_dt_input.Name = "btn_dt_input";
            this.btn_dt_input.Size = new System.Drawing.Size(209, 156);
            this.btn_dt_input.TabIndex = 0;
            this.btn_dt_input.Text = "비가동\r\n등록";
            this.btn_dt_input.UseVisualStyleBackColor = false;
            this.btn_dt_input.Click += new System.EventHandler(this.btn_dt_input_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.IndianRed;
            this.button11.Enabled = false;
            this.button11.Location = new System.Drawing.Point(14, 382);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(1252, 31);
            this.button11.TabIndex = 4;
            this.button11.UseVisualStyleBackColor = false;
            // 
            // progress_display
            // 
            this.progress_display.BackColor = System.Drawing.Color.White;
            this.progress_display.Controls.Add(this.txt_laser_state);
            this.progress_display.Controls.Add(this.txt_injection_state);
            this.progress_display.Controls.Add(this.label13);
            this.progress_display.Controls.Add(this.label10);
            this.progress_display.Location = new System.Drawing.Point(406, 5);
            this.progress_display.Name = "progress_display";
            this.progress_display.Size = new System.Drawing.Size(405, 170);
            this.progress_display.TabIndex = 36;
            // 
            // txt_laser_state
            // 
            this.txt_laser_state.Enabled = false;
            this.txt_laser_state.Location = new System.Drawing.Point(212, 83);
            this.txt_laser_state.Name = "txt_laser_state";
            this.txt_laser_state.Properties.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.txt_laser_state.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.txt_laser_state.Properties.Appearance.Options.UseBackColor = true;
            this.txt_laser_state.Properties.Appearance.Options.UseFont = true;
            this.txt_laser_state.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_laser_state.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_laser_state.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt_laser_state.Properties.AutoHeight = false;
            this.txt_laser_state.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txt_laser_state.Properties.NullText = "-";
            this.txt_laser_state.Size = new System.Drawing.Size(193, 86);
            this.txt_laser_state.TabIndex = 1;
            // 
            // txt_injection_state
            // 
            this.txt_injection_state.Enabled = false;
            this.txt_injection_state.Location = new System.Drawing.Point(212, 1);
            this.txt_injection_state.Name = "txt_injection_state";
            this.txt_injection_state.Properties.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.txt_injection_state.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.txt_injection_state.Properties.Appearance.Options.UseBackColor = true;
            this.txt_injection_state.Properties.Appearance.Options.UseFont = true;
            this.txt_injection_state.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_injection_state.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_injection_state.Properties.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.txt_injection_state.Properties.AutoHeight = false;
            this.txt_injection_state.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.txt_injection_state.Properties.NullText = "-";
            this.txt_injection_state.Size = new System.Drawing.Size(193, 81);
            this.txt_injection_state.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.MistyRose;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(0, 83);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(209, 86);
            this.label13.TabIndex = 0;
            this.label13.Text = "레이져리딩";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.MistyRose;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(0, 1);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(209, 81);
            this.label10.TabIndex = 0;
            this.label10.Text = "사출리딩";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_po_release_Insert
            // 
            this.btn_po_release_Insert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release_Insert.BackgroundImage")));
            this.btn_po_release_Insert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release_Insert.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_po_release_Insert.Location = new System.Drawing.Point(850, 417);
            this.btn_po_release_Insert.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release_Insert.Name = "btn_po_release_Insert";
            this.btn_po_release_Insert.Size = new System.Drawing.Size(209, 156);
            this.btn_po_release_Insert.TabIndex = 35;
            this.btn_po_release_Insert.Text = "긴급작지\r\n생성";
            this.btn_po_release_Insert.UseVisualStyleBackColor = false;
            this.btn_po_release_Insert.Click += new System.EventHandler(this.btn_po_release_Insert_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightCyan;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(295, 329);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 47);
            this.label1.TabIndex = 32;
            this.label1.Text = "/";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_worker_info
            // 
            this.btn_worker_info.Font = new System.Drawing.Font("굴림", 25F);
            this.btn_worker_info.Location = new System.Drawing.Point(870, 3);
            this.btn_worker_info.Name = "btn_worker_info";
            this.btn_worker_info.Size = new System.Drawing.Size(196, 84);
            this.btn_worker_info.TabIndex = 28;
            this.btn_worker_info.UseVisualStyleBackColor = true;
            this.btn_worker_info.Click += new System.EventHandler(this.btn_worker_info_Click);
            // 
            // txt_night_sqty
            // 
            this.txt_night_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_night_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_night_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_night_sqty.Location = new System.Drawing.Point(317, 329);
            this.txt_night_sqty.Name = "txt_night_sqty";
            this.txt_night_sqty.Size = new System.Drawing.Size(86, 47);
            this.txt_night_sqty.TabIndex = 33;
            this.txt_night_sqty.Text = "0";
            this.txt_night_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.LightCyan;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(618, 352);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(192, 27);
            this.label23.TabIndex = 25;
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sqty_cnt
            // 
            this.sqty_cnt.BackColor = System.Drawing.Color.LightCyan;
            this.sqty_cnt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.sqty_cnt.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.sqty_cnt.Location = new System.Drawing.Point(209, 376);
            this.sqty_cnt.Name = "sqty_cnt";
            this.sqty_cnt.Size = new System.Drawing.Size(192, 37);
            this.sqty_cnt.TabIndex = 34;
            this.sqty_cnt.Text = "0";
            this.sqty_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sqty_cnt.Visible = false;
            // 
            // txt_day_sqty
            // 
            this.txt_day_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_day_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_day_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_day_sqty.Location = new System.Drawing.Point(209, 329);
            this.txt_day_sqty.Name = "txt_day_sqty";
            this.txt_day_sqty.Size = new System.Drawing.Size(86, 47);
            this.txt_day_sqty.TabIndex = 34;
            this.txt_day_sqty.Text = "0";
            this.txt_day_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.LightCyan;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(618, 229);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(192, 89);
            this.label24.TabIndex = 26;
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(14, 376);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(193, 37);
            this.label9.TabIndex = 31;
            this.label9.Text = "카운트";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.Visible = false;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(14, 329);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(193, 47);
            this.label6.TabIndex = 31;
            this.label6.Text = "주/야";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.LightCyan;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(618, 319);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(192, 32);
            this.label22.TabIndex = 27;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(445, 352);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 28);
            this.label7.TabIndex = 21;
            this.label7.Text = "생산시작재고";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(405, 229);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 150);
            this.label25.TabIndex = 22;
            this.label25.Text = "다\r\n음\r\n생\r\n산";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(445, 230);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(170, 88);
            this.label21.TabIndex = 23;
            this.label21.Text = "품목";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(445, 320);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(170, 31);
            this.label8.TabIndex = 24;
            this.label8.Text = "현재고";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_fail_qty
            // 
            this.txt_fail_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_fail_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_fail_qty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_fail_qty.Location = new System.Drawing.Point(209, 282);
            this.txt_fail_qty.Name = "txt_fail_qty";
            this.txt_fail_qty.Size = new System.Drawing.Size(192, 47);
            this.txt_fail_qty.TabIndex = 11;
            this.txt_fail_qty.Text = "0";
            this.txt_fail_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label56.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(14, 282);
            this.label56.Margin = new System.Windows.Forms.Padding(0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(193, 47);
            this.label56.TabIndex = 0;
            this.label56.Text = "불량수량";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_MODEL
            // 
            this.txt_IT_MODEL.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_MODEL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_MODEL.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_MODEL.Location = new System.Drawing.Point(1159, 3);
            this.txt_IT_MODEL.Name = "txt_IT_MODEL";
            this.txt_IT_MODEL.Size = new System.Drawing.Size(107, 84);
            this.txt_IT_MODEL.TabIndex = 11;
            this.txt_IT_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayAndNight
            // 
            this.txtDayAndNight.BackColor = System.Drawing.SystemColors.Info;
            this.txtDayAndNight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtDayAndNight.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txtDayAndNight.Location = new System.Drawing.Point(617, 5);
            this.txtDayAndNight.Name = "txtDayAndNight";
            this.txtDayAndNight.Size = new System.Drawing.Size(194, 82);
            this.txtDayAndNight.TabIndex = 11;
            this.txtDayAndNight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_datetime
            // 
            this.txt_datetime.BackColor = System.Drawing.SystemColors.Info;
            this.txt_datetime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_datetime.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_datetime.Location = new System.Drawing.Point(209, 4);
            this.txt_datetime.Name = "txt_datetime";
            this.txt_datetime.Size = new System.Drawing.Size(193, 83);
            this.txt_datetime.TabIndex = 11;
            this.txt_datetime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_now_sqty
            // 
            this.lbl_now_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.lbl_now_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_now_sqty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.lbl_now_sqty.Location = new System.Drawing.Point(210, 229);
            this.lbl_now_sqty.Name = "lbl_now_sqty";
            this.lbl_now_sqty.Size = new System.Drawing.Size(192, 52);
            this.lbl_now_sqty.TabIndex = 11;
            this.lbl_now_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_good_qty
            // 
            this.txt_good_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_good_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_good_qty.Font = new System.Drawing.Font("굴림", 40F, System.Drawing.FontStyle.Bold);
            this.txt_good_qty.Location = new System.Drawing.Point(617, 176);
            this.txt_good_qty.Name = "txt_good_qty";
            this.txt_good_qty.Size = new System.Drawing.Size(194, 52);
            this.txt_good_qty.TabIndex = 11;
            this.txt_good_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.LightCyan;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(209, 282);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(193, 96);
            this.label37.TabIndex = 11;
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label37.Visible = false;
            // 
            // txt_MO_SQUTY
            // 
            this.txt_MO_SQUTY.BackColor = System.Drawing.Color.LightCyan;
            this.txt_MO_SQUTY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_MO_SQUTY.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_MO_SQUTY.Location = new System.Drawing.Point(209, 177);
            this.txt_MO_SQUTY.Name = "txt_MO_SQUTY";
            this.txt_MO_SQUTY.Size = new System.Drawing.Size(193, 51);
            this.txt_MO_SQUTY.TabIndex = 11;
            this.txt_MO_SQUTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_SNAME
            // 
            this.txt_IT_SNAME.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SNAME.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SNAME.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SNAME.Location = new System.Drawing.Point(617, 88);
            this.txt_IT_SNAME.Name = "txt_IT_SNAME";
            this.txt_IT_SNAME.Size = new System.Drawing.Size(194, 87);
            this.txt_IT_SNAME.TabIndex = 11;
            this.txt_IT_SNAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_SCODE
            // 
            this.txt_IT_SCODE.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SCODE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SCODE.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SCODE.Location = new System.Drawing.Point(209, 88);
            this.txt_IT_SCODE.Name = "txt_IT_SCODE";
            this.txt_IT_SCODE.Size = new System.Drawing.Size(193, 87);
            this.txt_IT_SCODE.TabIndex = 11;
            this.txt_IT_SCODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_pp_card_search
            // 
            this.btn_pp_card_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_pp_card_search.BackgroundImage")));
            this.btn_pp_card_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_pp_card_search.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_pp_card_search.Location = new System.Drawing.Point(13, 417);
            this.btn_pp_card_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_pp_card_search.Name = "btn_pp_card_search";
            this.btn_pp_card_search.Size = new System.Drawing.Size(179, 156);
            this.btn_pp_card_search.TabIndex = 0;
            this.btn_pp_card_search.Text = "PP Card\r\n등록";
            this.btn_pp_card_search.UseVisualStyleBackColor = false;
            this.btn_pp_card_search.Visible = false;
            this.btn_pp_card_search.Click += new System.EventHandler(this.btn_pp_card_search_Click);
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label43.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(14, 282);
            this.label43.Margin = new System.Windows.Forms.Padding(0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(193, 97);
            this.label43.TabIndex = 0;
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label43.Visible = false;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label44.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(14, 87);
            this.label44.Margin = new System.Windows.Forms.Padding(0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(193, 87);
            this.label44.TabIndex = 0;
            this.label44.Text = "품    번";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_fail_input
            // 
            this.btn_fail_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_fail_input.BackgroundImage")));
            this.btn_fail_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_fail_input.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_fail_input.Location = new System.Drawing.Point(223, 417);
            this.btn_fail_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_fail_input.Name = "btn_fail_input";
            this.btn_fail_input.Size = new System.Drawing.Size(209, 156);
            this.btn_fail_input.TabIndex = 0;
            this.btn_fail_input.Text = "불량등록";
            this.btn_fail_input.UseVisualStyleBackColor = false;
            this.btn_fail_input.Click += new System.EventHandler(this.btn_fail_input_Click);
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(14, 175);
            this.label45.Margin = new System.Windows.Forms.Padding(0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(193, 53);
            this.label45.TabIndex = 0;
            this.label45.Text = "계획수량";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_day_work
            // 
            this.btn_day_work.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_day_work.BackgroundImage")));
            this.btn_day_work.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_day_work.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_day_work.Location = new System.Drawing.Point(641, 417);
            this.btn_day_work.Margin = new System.Windows.Forms.Padding(1);
            this.btn_day_work.Name = "btn_day_work";
            this.btn_day_work.Size = new System.Drawing.Size(209, 156);
            this.btn_day_work.TabIndex = 0;
            this.btn_day_work.Text = "일별\r\n생산현황";
            this.btn_day_work.UseVisualStyleBackColor = false;
            this.btn_day_work.Click += new System.EventHandler(this.btn_day_work_Click);
            // 
            // btn_meterial_search
            // 
            this.btn_meterial_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_meterial_search.BackgroundImage")));
            this.btn_meterial_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_meterial_search.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_meterial_search.Location = new System.Drawing.Point(432, 417);
            this.btn_meterial_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_meterial_search.Name = "btn_meterial_search";
            this.btn_meterial_search.Size = new System.Drawing.Size(209, 156);
            this.btn_meterial_search.TabIndex = 0;
            this.btn_meterial_search.Text = "재공자재\r\n재고";
            this.btn_meterial_search.UseVisualStyleBackColor = false;
            this.btn_meterial_search.Click += new System.EventHandler(this.btn_meterial_search_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release.BackgroundImage")));
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(1059, 417);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(209, 156);
            this.btn_po_release.TabIndex = 0;
            this.btn_po_release.Text = "작업지시\r\n선택";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(14, 4);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(193, 83);
            this.label47.TabIndex = 0;
            this.label47.Text = "생산일자";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(813, 4);
            this.label48.Margin = new System.Windows.Forms.Padding(0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(56, 83);
            this.label48.TabIndex = 0;
            this.label48.Text = "작\r\n업\r\n자";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label50.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label50.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label50.Location = new System.Drawing.Point(14, 229);
            this.label50.Margin = new System.Windows.Forms.Padding(0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(193, 51);
            this.label50.TabIndex = 0;
            this.label50.Text = "현재고";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label51.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label51.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label51.Location = new System.Drawing.Point(405, 176);
            this.label51.Margin = new System.Windows.Forms.Padding(0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(210, 52);
            this.label51.TabIndex = 0;
            this.label51.Text = "생산수량";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label52.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label52.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label52.Location = new System.Drawing.Point(813, 88);
            this.label52.Margin = new System.Windows.Forms.Padding(0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(56, 290);
            this.label52.TabIndex = 0;
            this.label52.Text = "카드\r\n\r\n투입\r\n\r\n순서";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label53.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label53.Location = new System.Drawing.Point(405, 87);
            this.label53.Margin = new System.Windows.Forms.Padding(0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(210, 87);
            this.label53.TabIndex = 0;
            this.label53.Text = "품    명";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label54
            // 
            this.label54.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label54.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label54.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(405, 5);
            this.label54.Margin = new System.Windows.Forms.Padding(0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(210, 83);
            this.label54.TabIndex = 0;
            this.label54.Text = "근무유형";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label55.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label55.Location = new System.Drawing.Point(1069, 4);
            this.label55.Margin = new System.Windows.Forms.Padding(0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(87, 83);
            this.label55.TabIndex = 0;
            this.label55.Text = "차종";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(667, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 38;
            this.button4.Text = "in_reading";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(388, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(277, 21);
            this.textBox1.TabIndex = 37;
            this.textBox1.Text = "PP*D001/IM02/008/84711A7000WK/32";
            this.textBox1.Visible = false;
            // 
            // txtNext_Carrier_no
            // 
            this.txtNext_Carrier_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtNext_Carrier_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtNext_Carrier_no.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.txtNext_Carrier_no.Location = new System.Drawing.Point(840, 99);
            this.txtNext_Carrier_no.Name = "txtNext_Carrier_no";
            this.txtNext_Carrier_no.Size = new System.Drawing.Size(111, 67);
            this.txtNext_Carrier_no.TabIndex = 11;
            this.txtNext_Carrier_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtNext_Carrier_no.Visible = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(954, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 66);
            this.label2.TabIndex = 11;
            this.label2.Text = "출 구\r\n대 차";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(566, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 71);
            this.label3.TabIndex = 11;
            this.label3.Text = "입구카드";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCard_no
            // 
            this.txtCard_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtCard_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtCard_no.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtCard_no.Location = new System.Drawing.Point(665, 95);
            this.txtCard_no.Name = "txtCard_no";
            this.txtCard_no.Size = new System.Drawing.Size(101, 71);
            this.txtCard_no.TabIndex = 11;
            this.txtCard_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(566, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 32);
            this.label4.TabIndex = 11;
            this.label4.Text = "입구대차";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // txtIn_Carrier_no
            // 
            this.txtIn_Carrier_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtIn_Carrier_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtIn_Carrier_no.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtIn_Carrier_no.Location = new System.Drawing.Point(665, 134);
            this.txtIn_Carrier_no.Name = "txtIn_Carrier_no";
            this.txtIn_Carrier_no.Size = new System.Drawing.Size(101, 32);
            this.txtIn_Carrier_no.TabIndex = 11;
            this.txtIn_Carrier_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtIn_Carrier_no.Visible = false;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(772, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 66);
            this.label5.TabIndex = 11;
            this.label5.Text = "다 음\r\n대 차";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Visible = false;
            // 
            // Btn_Auto_Connection
            // 
            this.Btn_Auto_Connection.Font = new System.Drawing.Font("굴림", 22F);
            this.Btn_Auto_Connection.Location = new System.Drawing.Point(421, 94);
            this.Btn_Auto_Connection.Name = "Btn_Auto_Connection";
            this.Btn_Auto_Connection.Size = new System.Drawing.Size(141, 72);
            this.Btn_Auto_Connection.TabIndex = 19;
            this.Btn_Auto_Connection.Text = "자동연결";
            this.Btn_Auto_Connection.UseVisualStyleBackColor = true;
            this.Btn_Auto_Connection.Click += new System.EventHandler(this.Btn_Auto_Connection_Click);
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(310, 12);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 20;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // timer_re_carrier
            // 
            this.timer_re_carrier.Interval = 30000;
            this.timer_re_carrier.Tick += new System.EventHandler(this.timer_re_carrier_Tick);
            // 
            // txtOut_carrier_no
            // 
            this.txtOut_carrier_no.Font = new System.Drawing.Font("굴림", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtOut_carrier_no.Location = new System.Drawing.Point(1021, 99);
            this.txtOut_carrier_no.Name = "txtOut_carrier_no";
            this.txtOut_carrier_no.Size = new System.Drawing.Size(177, 66);
            this.txtOut_carrier_no.TabIndex = 21;
            this.txtOut_carrier_no.UseVisualStyleBackColor = true;
            this.txtOut_carrier_no.Visible = false;
            this.txtOut_carrier_no.Click += new System.EventHandler(this.txtOut_carrier_no_Click);
            // 
            // btn_carrier_sqty
            // 
            this.btn_carrier_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_carrier_sqty.Appearance.Options.UseFont = true;
            this.btn_carrier_sqty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_carrier_sqty.Location = new System.Drawing.Point(865, 95);
            this.btn_carrier_sqty.Name = "btn_carrier_sqty";
            this.btn_carrier_sqty.Size = new System.Drawing.Size(91, 70);
            this.btn_carrier_sqty.TabIndex = 26;
            this.btn_carrier_sqty.Text = "32";
            this.btn_carrier_sqty.Click += new System.EventHandler(this.btn_carrier_sqty_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl1.Location = new System.Drawing.Point(962, 95);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(91, 70);
            this.labelControl1.TabIndex = 29;
            this.labelControl1.Text = "자동";
            // 
            // btn_on_off
            // 
            this.btn_on_off.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_on_off.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_on_off.Appearance.Options.UseBackColor = true;
            this.btn_on_off.Appearance.Options.UseFont = true;
            this.btn_on_off.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_on_off.Location = new System.Drawing.Point(1055, 95);
            this.btn_on_off.Name = "btn_on_off";
            this.btn_on_off.Size = new System.Drawing.Size(91, 70);
            this.btn_on_off.TabIndex = 28;
            this.btn_on_off.Text = "On";
            this.btn_on_off.Click += new System.EventHandler(this.btn_on_off_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(90, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 42);
            this.button2.TabIndex = 16;
            this.button2.Text = "프린트TEST(KIA)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.simpleButton1.Location = new System.Drawing.Point(1199, 99);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(80, 66);
            this.simpleButton1.TabIndex = 30;
            this.simpleButton1.Text = "수동\r\n입력";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn_fail_search
            // 
            this.btn_fail_search.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_fail_search.Appearance.Options.UseFont = true;
            this.btn_fail_search.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_fail_search.Location = new System.Drawing.Point(1152, 95);
            this.btn_fail_search.Name = "btn_fail_search";
            this.btn_fail_search.Size = new System.Drawing.Size(128, 70);
            this.btn_fail_search.TabIndex = 26;
            this.btn_fail_search.Text = "월별\r\n불량현황";
            this.btn_fail_search.Click += new System.EventHandler(this.btn_fail_search_Click);
            // 
            // listView_target_list2
            // 
            this.listView_target_list2.Location = new System.Drawing.Point(672, 87);
            this.listView_target_list2.Name = "listView_target_list2";
            this.listView_target_list2.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list2.TabIndex = 0;
            this.listView_target_list2.UseCompatibleStateImageBehavior = false;
            this.listView_target_list2.Visible = false;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(193, 90);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt2
            // 
            this.ddc_ivt2.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt2.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt2.DigitText = "00000";
            this.ddc_ivt2.Location = new System.Drawing.Point(490, 87);
            this.ddc_ivt2.Name = "ddc_ivt2";
            this.ddc_ivt2.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt2.TabIndex = 9;
            this.ddc_ivt2.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(12, 87);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt2
            // 
            this.dsmListView_ivt2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader188,
            this.columnHeader189,
            this.columnHeader190,
            this.columnHeader191,
            this.columnHeader192,
            this.columnHeader193,
            this.columnHeader194});
            this.dsmListView_ivt2.Location = new System.Drawing.Point(453, 99);
            this.dsmListView_ivt2.Name = "dsmListView_ivt2";
            this.dsmListView_ivt2.Size = new System.Drawing.Size(28, 47);
            this.dsmListView_ivt2.TabIndex = 0;
            this.dsmListView_ivt2.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt2.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt2.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader181,
            this.columnHeader182,
            this.columnHeader183,
            this.columnHeader184,
            this.columnHeader185,
            this.columnHeader186,
            this.columnHeader187});
            this.dsmListView_ivt.Location = new System.Drawing.Point(418, 99);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(29, 47);
            this.dsmListView_ivt.TabIndex = 0;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(379, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 31;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_mix_toggle
            // 
            this.btn_mix_toggle.Appearance.BackColor = System.Drawing.Color.Pink;
            this.btn_mix_toggle.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.btn_mix_toggle.Appearance.Options.UseBackColor = true;
            this.btn_mix_toggle.Appearance.Options.UseFont = true;
            this.btn_mix_toggle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_mix_toggle.Location = new System.Drawing.Point(584, 42);
            this.btn_mix_toggle.Name = "btn_mix_toggle";
            this.btn_mix_toggle.Size = new System.Drawing.Size(94, 51);
            this.btn_mix_toggle.TabIndex = 33;
            this.btn_mix_toggle.Text = "Off";
            this.btn_mix_toggle.Visible = false;
            this.btn_mix_toggle.Click += new System.EventHandler(this.btn_mix_toggle_Click);
            // 
            // lbl혼적
            // 
            this.lbl혼적.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.lbl혼적.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.lbl혼적.Appearance.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl혼적.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.lbl혼적.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl혼적.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl혼적.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl혼적.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lbl혼적.LineVisible = true;
            this.lbl혼적.Location = new System.Drawing.Point(584, 12);
            this.lbl혼적.Name = "lbl혼적";
            this.lbl혼적.Size = new System.Drawing.Size(94, 30);
            this.lbl혼적.TabIndex = 27;
            this.lbl혼적.Text = "혼적";
            this.lbl혼적.Visible = false;
            // 
            // lab_carrier_sqty
            // 
            this.lab_carrier_sqty.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.lab_carrier_sqty.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.lab_carrier_sqty.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.lab_carrier_sqty.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.lab_carrier_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lab_carrier_sqty.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lab_carrier_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab_carrier_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lab_carrier_sqty.Location = new System.Drawing.Point(772, 95);
            this.lab_carrier_sqty.Name = "lab_carrier_sqty";
            this.lab_carrier_sqty.Size = new System.Drawing.Size(91, 70);
            this.lab_carrier_sqty.TabIndex = 34;
            this.lab_carrier_sqty.Text = "대 차\r\n수 량";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("굴림", 14F);
            this.label11.Location = new System.Drawing.Point(580, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 45);
            this.label11.TabIndex = 36;
            this.label11.Text = "리딩";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBarcode
            // 
            this.txtBarcode.EditValue = "";
            this.txtBarcode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtBarcode.Location = new System.Drawing.Point(665, 49);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.txtBarcode.Properties.Appearance.Options.UseFont = true;
            this.txtBarcode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBarcode.Size = new System.Drawing.Size(264, 36);
            this.txtBarcode.TabIndex = 35;
            this.txtBarcode.Enter += new System.EventHandler(this.txtBarcode_Enter);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            this.txtBarcode.Leave += new System.EventHandler(this.txtBarcode_Leave_1);
            // 
            // btn_inspection
            // 
            this.btn_inspection.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.btn_inspection.Appearance.Options.UseFont = true;
            this.btn_inspection.Location = new System.Drawing.Point(932, 12);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(102, 77);
            this.btn_inspection.TabIndex = 45;
            this.btn_inspection.Text = "초중종품\r\n등록";
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.labelControl2.Appearance.BorderColor = System.Drawing.Color.DimGray;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AppearanceDisabled.BackColor = System.Drawing.Color.Silver;
            this.labelControl2.AppearanceDisabled.ForeColor = System.Drawing.Color.White;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(12, 1);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(213, 43);
            this.labelControl2.TabIndex = 46;
            this.labelControl2.Text = "JA/YB";
            this.labelControl2.Click += new System.EventHandler(this.labelControl2_Click);
            // 
            // btn_program_finish
            // 
            this.btn_program_finish.Appearance.BackColor = System.Drawing.Color.Crimson;
            this.btn_program_finish.Appearance.BackColor2 = System.Drawing.Color.PaleVioletRed;
            this.btn_program_finish.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_program_finish.Appearance.Options.UseBackColor = true;
            this.btn_program_finish.Appearance.Options.UseFont = true;
            this.btn_program_finish.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_program_finish.Location = new System.Drawing.Point(1163, 15);
            this.btn_program_finish.Name = "btn_program_finish";
            this.btn_program_finish.Size = new System.Drawing.Size(116, 70);
            this.btn_program_finish.TabIndex = 98;
            this.btn_program_finish.Text = "작업종료";
            this.btn_program_finish.Click += new System.EventHandler(this.btn_program_finish_Click);
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_close.Appearance.BackColor2 = System.Drawing.Color.YellowGreen;
            this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Location = new System.Drawing.Point(1045, 15);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(118, 70);
            this.btn_close.TabIndex = 99;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Laser_JA_YB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1309, 758);
            this.Controls.Add(this.btn_program_finish);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_inspection);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtBarcode);
            this.Controls.Add(this.btn_fail_search);
            this.Controls.Add(this.lab_carrier_sqty);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.btn_on_off);
            this.Controls.Add(this.btn_carrier_sqty);
            this.Controls.Add(this.txtCard_no);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dsmListView_ivt2);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.btn_mix_toggle);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.lbl혼적);
            this.Controls.Add(this.txtOut_carrier_no);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Btn_Auto_Connection);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIn_Carrier_no);
            this.Controls.Add(this.txtNext_Carrier_no);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lueWc_code);
            this.Controls.Add(this.txtIt_scode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.button_com_close2);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open2);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports2);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list2);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt2);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Laser_JA_YB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SubMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.SubMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.progress_display.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_laser_state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_injection_state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarcode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private DK_Tablet.dsmListView dsmListView_ivt;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.ComboBox comboBox_ports2;
        private System.Windows.Forms.Button button_com_open2;
        private System.Windows.Forms.Button button_com_close2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private Owf.Controls.DigitalDisplayControl ddc_ivt2;
        private dsmListView listView_target_list2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtIt_scode;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private dsmListView dsmListView_ivt2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer_po_start;
        private System.Windows.Forms.Timer timer_pp_start;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label txt_IT_MODEL;
        private System.Windows.Forms.Label txtDayAndNight;
        private System.Windows.Forms.Label txt_datetime;
        private System.Windows.Forms.Label lbl_now_sqty;
        private System.Windows.Forms.Label txt_fail_qty;
        private System.Windows.Forms.Label txt_good_qty;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label txt_MO_SQUTY;
        private System.Windows.Forms.Label txt_IT_SNAME;
        private System.Windows.Forms.Label txt_IT_SCODE;
        private System.Windows.Forms.Button btn_pp_card_search;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btn_dt_input;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btn_fail_input;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btn_meterial_search;
        private System.Windows.Forms.Button btn_po_release;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label txtNext_Carrier_no;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtCard_no;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtIn_Carrier_no;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Btn_Auto_Connection;
        private System.Windows.Forms.Timer timer_now;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private System.Windows.Forms.Timer timer_re_carrier;
        private System.Windows.Forms.Button txtOut_carrier_no;
        private DevExpress.XtraEditors.SimpleButton btn_carrier_sqty;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_worker_info;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_on_off;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btn_fail_search;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txt_night_sqty;
        private System.Windows.Forms.Label txt_day_sqty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_day_work;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton btn_mix_toggle;
        private DevExpress.XtraEditors.LabelControl lbl혼적;
        private System.Windows.Forms.DataGridViewTextBoxColumn ROWNUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn VW_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARRIER_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn HH;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRDT_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHECK_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn HARFTYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn GOOD_QTY;
        private DevExpress.XtraEditors.LabelControl lab_carrier_sqty;
        private System.Windows.Forms.ColumnHeader columnHeader62;
        private System.Windows.Forms.ColumnHeader columnHeader63;
        private System.Windows.Forms.ColumnHeader columnHeader64;
        private System.Windows.Forms.ColumnHeader columnHeader65;
        private System.Windows.Forms.ColumnHeader columnHeader66;
        private System.Windows.Forms.ColumnHeader columnHeader67;
        private System.Windows.Forms.ColumnHeader columnHeader68;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.ColumnHeader columnHeader61;
        private System.Windows.Forms.ColumnHeader columnHeader76;
        private System.Windows.Forms.ColumnHeader columnHeader77;
        private System.Windows.Forms.ColumnHeader columnHeader78;
        private System.Windows.Forms.ColumnHeader columnHeader79;
        private System.Windows.Forms.ColumnHeader columnHeader80;
        private System.Windows.Forms.ColumnHeader columnHeader81;
        private System.Windows.Forms.ColumnHeader columnHeader82;
        private System.Windows.Forms.ColumnHeader columnHeader69;
        private System.Windows.Forms.ColumnHeader columnHeader70;
        private System.Windows.Forms.ColumnHeader columnHeader71;
        private System.Windows.Forms.ColumnHeader columnHeader72;
        private System.Windows.Forms.ColumnHeader columnHeader73;
        private System.Windows.Forms.ColumnHeader columnHeader74;
        private System.Windows.Forms.ColumnHeader columnHeader75;
        private System.Windows.Forms.ColumnHeader columnHeader90;
        private System.Windows.Forms.ColumnHeader columnHeader91;
        private System.Windows.Forms.ColumnHeader columnHeader92;
        private System.Windows.Forms.ColumnHeader columnHeader93;
        private System.Windows.Forms.ColumnHeader columnHeader94;
        private System.Windows.Forms.ColumnHeader columnHeader95;
        private System.Windows.Forms.ColumnHeader columnHeader96;
        private System.Windows.Forms.ColumnHeader columnHeader83;
        private System.Windows.Forms.ColumnHeader columnHeader84;
        private System.Windows.Forms.ColumnHeader columnHeader85;
        private System.Windows.Forms.ColumnHeader columnHeader86;
        private System.Windows.Forms.ColumnHeader columnHeader87;
        private System.Windows.Forms.ColumnHeader columnHeader88;
        private System.Windows.Forms.ColumnHeader columnHeader89;
        private System.Windows.Forms.Button btn_po_release_Insert;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit txtBarcode;
        private System.Windows.Forms.ColumnHeader columnHeader97;
        private System.Windows.Forms.ColumnHeader columnHeader98;
        private System.Windows.Forms.ColumnHeader columnHeader99;
        private System.Windows.Forms.ColumnHeader columnHeader100;
        private System.Windows.Forms.ColumnHeader columnHeader101;
        private System.Windows.Forms.ColumnHeader columnHeader102;
        private System.Windows.Forms.ColumnHeader columnHeader103;
        private System.Windows.Forms.Label sqty_cnt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ColumnHeader columnHeader104;
        private System.Windows.Forms.ColumnHeader columnHeader105;
        private System.Windows.Forms.ColumnHeader columnHeader106;
        private System.Windows.Forms.ColumnHeader columnHeader107;
        private System.Windows.Forms.ColumnHeader columnHeader108;
        private System.Windows.Forms.ColumnHeader columnHeader109;
        private System.Windows.Forms.ColumnHeader columnHeader110;
        private System.Windows.Forms.ColumnHeader columnHeader118;
        private System.Windows.Forms.ColumnHeader columnHeader119;
        private System.Windows.Forms.ColumnHeader columnHeader120;
        private System.Windows.Forms.ColumnHeader columnHeader121;
        private System.Windows.Forms.ColumnHeader columnHeader122;
        private System.Windows.Forms.ColumnHeader columnHeader123;
        private System.Windows.Forms.ColumnHeader columnHeader124;
        private System.Windows.Forms.ColumnHeader columnHeader111;
        private System.Windows.Forms.ColumnHeader columnHeader112;
        private System.Windows.Forms.ColumnHeader columnHeader113;
        private System.Windows.Forms.ColumnHeader columnHeader114;
        private System.Windows.Forms.ColumnHeader columnHeader115;
        private System.Windows.Forms.ColumnHeader columnHeader116;
        private System.Windows.Forms.ColumnHeader columnHeader117;
        private System.Windows.Forms.ColumnHeader columnHeader132;
        private System.Windows.Forms.ColumnHeader columnHeader133;
        private System.Windows.Forms.ColumnHeader columnHeader134;
        private System.Windows.Forms.ColumnHeader columnHeader135;
        private System.Windows.Forms.ColumnHeader columnHeader136;
        private System.Windows.Forms.ColumnHeader columnHeader137;
        private System.Windows.Forms.ColumnHeader columnHeader138;
        private System.Windows.Forms.ColumnHeader columnHeader125;
        private System.Windows.Forms.ColumnHeader columnHeader126;
        private System.Windows.Forms.ColumnHeader columnHeader127;
        private System.Windows.Forms.ColumnHeader columnHeader128;
        private System.Windows.Forms.ColumnHeader columnHeader129;
        private System.Windows.Forms.ColumnHeader columnHeader130;
        private System.Windows.Forms.ColumnHeader columnHeader131;
        private System.Windows.Forms.ColumnHeader columnHeader146;
        private System.Windows.Forms.ColumnHeader columnHeader147;
        private System.Windows.Forms.ColumnHeader columnHeader148;
        private System.Windows.Forms.ColumnHeader columnHeader149;
        private System.Windows.Forms.ColumnHeader columnHeader150;
        private System.Windows.Forms.ColumnHeader columnHeader151;
        private System.Windows.Forms.ColumnHeader columnHeader152;
        private System.Windows.Forms.ColumnHeader columnHeader139;
        private System.Windows.Forms.ColumnHeader columnHeader140;
        private System.Windows.Forms.ColumnHeader columnHeader141;
        private System.Windows.Forms.ColumnHeader columnHeader142;
        private System.Windows.Forms.ColumnHeader columnHeader143;
        private System.Windows.Forms.ColumnHeader columnHeader144;
        private System.Windows.Forms.ColumnHeader columnHeader145;
        private DevExpress.XtraEditors.SimpleButton btn_inspection;
        private System.Windows.Forms.ColumnHeader columnHeader160;
        private System.Windows.Forms.ColumnHeader columnHeader161;
        private System.Windows.Forms.ColumnHeader columnHeader162;
        private System.Windows.Forms.ColumnHeader columnHeader163;
        private System.Windows.Forms.ColumnHeader columnHeader164;
        private System.Windows.Forms.ColumnHeader columnHeader165;
        private System.Windows.Forms.ColumnHeader columnHeader166;
        private System.Windows.Forms.ColumnHeader columnHeader153;
        private System.Windows.Forms.ColumnHeader columnHeader154;
        private System.Windows.Forms.ColumnHeader columnHeader155;
        private System.Windows.Forms.ColumnHeader columnHeader156;
        private System.Windows.Forms.ColumnHeader columnHeader157;
        private System.Windows.Forms.ColumnHeader columnHeader158;
        private System.Windows.Forms.ColumnHeader columnHeader159;
        private System.Windows.Forms.Panel progress_display;
        private DevExpress.XtraEditors.TextEdit txt_laser_state;
        private DevExpress.XtraEditors.TextEdit txt_injection_state;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btn_program_finish;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private System.Windows.Forms.ColumnHeader columnHeader174;
        private System.Windows.Forms.ColumnHeader columnHeader175;
        private System.Windows.Forms.ColumnHeader columnHeader176;
        private System.Windows.Forms.ColumnHeader columnHeader177;
        private System.Windows.Forms.ColumnHeader columnHeader178;
        private System.Windows.Forms.ColumnHeader columnHeader179;
        private System.Windows.Forms.ColumnHeader columnHeader180;
        private System.Windows.Forms.ColumnHeader columnHeader167;
        private System.Windows.Forms.ColumnHeader columnHeader168;
        private System.Windows.Forms.ColumnHeader columnHeader169;
        private System.Windows.Forms.ColumnHeader columnHeader170;
        private System.Windows.Forms.ColumnHeader columnHeader171;
        private System.Windows.Forms.ColumnHeader columnHeader172;
        private System.Windows.Forms.ColumnHeader columnHeader173;
        private System.Windows.Forms.ColumnHeader columnHeader188;
        private System.Windows.Forms.ColumnHeader columnHeader189;
        private System.Windows.Forms.ColumnHeader columnHeader190;
        private System.Windows.Forms.ColumnHeader columnHeader191;
        private System.Windows.Forms.ColumnHeader columnHeader192;
        private System.Windows.Forms.ColumnHeader columnHeader193;
        private System.Windows.Forms.ColumnHeader columnHeader194;
        private System.Windows.Forms.ColumnHeader columnHeader181;
        private System.Windows.Forms.ColumnHeader columnHeader182;
        private System.Windows.Forms.ColumnHeader columnHeader183;
        private System.Windows.Forms.ColumnHeader columnHeader184;
        private System.Windows.Forms.ColumnHeader columnHeader185;
        private System.Windows.Forms.ColumnHeader columnHeader186;
        private System.Windows.Forms.ColumnHeader columnHeader187;
    }
}

