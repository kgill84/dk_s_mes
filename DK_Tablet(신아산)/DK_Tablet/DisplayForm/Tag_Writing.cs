﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;



namespace DK_Tablet
{
    public partial class Tag_Writing : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

        private SwingLibrary.SwingAPI Swing = null;
        SqlConnection conn=null;
        string strConn = "";
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };
        //bool usbCheck = false;
        
        //float tempRange = 0;
        //string dataValue = "";
        public bool messageCheck = false;
        public Tag_Writing()
        {
            strConn = Properties.Settings.Default.SQL_DKQT;
            conn = new SqlConnection(strConn);
            
            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }


            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            comboBox_bank.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.MemoryBank));
            comboBox_bank.SelectedIndex = 1;
            textBox_BlockOffset.Text = "2";
            textBox_BlockCount.Text = "18";
            /*
            groupBox_bt.Enabled = checkBox_dongle.Checked;

            label_battery_volt.Text = "Volts: 0.000 [V]";

            checkBox_rawdata.Checked = Properties.Settings.Default.ConsoleEnable;
            Swing.LogWrite = Properties.Settings.Default.ConsoleEnable;
            WinConsole.Visible = checkBox_rawdata.Checked;

            for (int i = 30; i > 2; i--) comboBox_rfpwr.Items.Add(i);
            comboBox_rfpwr.SelectedIndex = 0;

            for (int j = 0; j < 6; j++) comboBox_channel.Items.Add(j);
            comboBox_channel.SelectedIndex = 0;
            comboBox_channel.Enabled = checkBox_fixed_channel.Checked;

            for (int i = 0; i < 16; i++) comboBox_q.Items.Add(i);
            comboBox_q.SelectedIndex = 4;

            comboBox_algorithm.Items.Add("FIXEDQ");
            comboBox_algorithm.Items.Add("DYNAMICQ");
            comboBox_algorithm.SelectedIndex = 1;

            checkBox_toggle_target.Checked = true;

            comboBox_bank.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.MemoryBank));
            comboBox_bank.SelectedIndex = 1;
            textBox_BlockOffset.Text = "2";
            textBox_BlockCount.Text = "6";
            //textBox_accesspwd.Text = "00000000";

            comboBox_mem_killpwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_accesspwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_epc.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_tid.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_user.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });

            comboBox_mem_killpwd.SelectedIndex = 4;
            comboBox_mem_accesspwd.SelectedIndex = 4;
            comboBox_mem_epc.SelectedIndex = 4;
            comboBox_mem_tid.SelectedIndex = 4;
            comboBox_mem_user.SelectedIndex = 4;

            radioButton_vol_max.Checked = true;
            radioButton_bz_trigger.Checked = true;
            */
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
        }

        ContextMenuStrip remove_menu;
        private void Form1_Load(object sender, EventArgs e)
        {
            
            //SWING-U
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

            listView_target_list.ContextMenuStrip = remove_menu;
            getData();
            set_rTB_write_set();
        }
        
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();

                    Swing.ReportAllInformation();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            
        }

        #region swing-u 함수(Swing_Notify)
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion

        #region swing-u 기본함수
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }

        private object locker = new object();
        private bool key = false;
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
            }));

            Swing.ReportTagList();
        }
        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                rTB_read.Clear();
                Utils.AddText(rTB_read, Color.Gray, "UID: ");
                Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    counts = int.Parse(textBox_BlockCount.Text.Trim());

                    Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }
        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }
        #endregion

        

        private void btn_po_realese_search_Click(object sender, EventArgs e)
        {
            /*
            po_realese_search po_realese_search = new po_realese_search();
            if (po_realese_search.ShowDialog() == DialogResult.OK)
            {
            }*/
        }

        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            Dt_Input Dt_Input = new Dt_Input();
            if (Dt_Input.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void btn_fail_input_Click(object sender, EventArgs e)
        {
            Fail_Input Fail_Input = new Fail_Input();
            if (Fail_Input.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            metarial_search metarial_search = new metarial_search();
            if (metarial_search.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void btn_mold_search_Click(object sender, EventArgs e)
        {
            mold_search mold_search = new mold_search();
            if (mold_search.ShowDialog() == DialogResult.OK)
            {
            }
        }

        private void btn_call_Click(object sender, EventArgs e)
        {
            call_admin call_admin = new call_admin();
            if (call_admin.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.ConnectionClose();
        }

        private void button_memClear_Click(object sender, EventArgs e)
        {
            rTB_error.Clear();
            rTB_read.Clear();
            rTB_write.Clear();
            txt_size.Clear();
            txt_pp_serno.Clear();
            txt_it_scode.Clear();
            txt_dc.Clear();
            txt_size.Clear();
        }

        private void button_memRead_Click(object sender, EventArgs e)
        {
            if (!sWing_is_open())
            {
                MessageBox.Show("리더기를 연결해 주세요");
                return;
            }
            SwingLibrary.SwingAPI.MemoryBank bank =
                (SwingLibrary.SwingAPI.MemoryBank)Enum.Parse(typeof(SwingLibrary.SwingAPI.MemoryBank),
                comboBox_bank.SelectedItem.ToString());

            int offset = int.Parse(textBox_BlockOffset.Text.Trim());
            int count = int.Parse(textBox_BlockCount.Text.Trim());

            rTB_error.Clear();
            rTB_read.Clear();
            Swing.MemoryRead(bank, offset, count);
            
            
        }

        private void button_memWrite_Click(object sender, EventArgs e)
        {
            if (!sWing_is_open())
            {
                MessageBox.Show("리더기를 연결해 주세요");
                return;
            }
            string temp = rTB_write.Text;
            Regex regex = new Regex(@"[ㄱ-ㅣ가-힣]");

            if (regex.IsMatch(temp))
            {
                MessageBox.Show("한글은 입력 할 수 없습니다.");
                return;
            }
            
            SwingLibrary.SwingAPI.MemoryBank bank =
                (SwingLibrary.SwingAPI.MemoryBank)Enum.Parse(typeof(SwingLibrary.SwingAPI.MemoryBank),
                comboBox_bank.SelectedItem.ToString());

            int offset, count = 0;

            rTB_error.Clear();
            rTB_read.Clear();
            string space = "                                    ";
            string write_str = "";
            if (rTB_write.Text.Length <= 36)
            {
                int write_length = rTB_write.Text.Length;
                write_str = rTB_write.Text + space.Substring(write_length, 36 - write_length);
            }
            else
            {
                MessageBox.Show("자리수를 초과 하였습니다.");
                return;
            }
            try
            {
                offset = int.Parse(textBox_BlockOffset.Text.Trim());
                count = int.Parse(textBox_BlockCount.Text.Trim());
            }
            catch
            {
                Utils.AddText(rTB_error, Color.Red, "Error: offset or length is not valid");
                return;
            }

            if (count == 0)
            {
                Utils.AddText(rTB_error, Color.Red, "Error: word length is 0");
                return;
            }

            string dataString = Utils.StringSubGap(str2hex(write_str.ToUpper()));
            int dataLength = count * 4;

            if (dataString.Length != dataLength)
            {
                Utils.AddText(rTB_error, Color.Red, "Error: Data length is not equal to Block count");
                return;
            }
            else
            {
                byte[] hexdata = Utils.StringToByteArray(dataString);
                if (hexdata == null)
                {
                    Utils.AddText(rTB_error, Color.Red, "Error: Input datas need Hex-code checking.");
                    return;
                }

                rTB_write.Clear();
                string origindata = Utils.ByteToString(hexdata).Trim().Replace(" ", "");
                Utils.AddText(rTB_write, Color.Blue, Utils.HexToASCII2(origindata).Trim());

                Swing.MemoryWrite(bank, offset, count, dataString);
            }
            
        }
        static public string str2hex(string strData)
        {
            string resultHex = "";
            byte[] arr_byteStr = str2bytes(strData);

            foreach (byte byteStr in arr_byteStr)
            {
                resultHex += string.Format("{0:x2}", byteStr);
            }
            return resultHex;
        }
        static public byte[] str2bytes(string byteData)
        {
            System.Text.ASCIIEncoding asencoding = new System.Text.ASCIIEncoding();
            return Encoding.Default.GetBytes(byteData);
        }


        private void button5_Click(object sender, EventArgs e)
        {
            if (!sWing_is_open())
            {
                MessageBox.Show("리더기를 연결해 주세요");
                return;
            }
            
            SwingLibrary.SwingAPI.MemoryBank bank =
                (SwingLibrary.SwingAPI.MemoryBank)Enum.Parse(typeof(SwingLibrary.SwingAPI.MemoryBank),
                comboBox_bank.SelectedItem.ToString());

            int offset, count = 0;

            rTB_error.Clear();
            rTB_read.Clear();

            try
            {
                offset = 1;
                count = 19;
            }
            catch
            {
                Utils.AddText(rTB_error, Color.Red, "Error: offset or length is not valid");
                return;
            }

            if (count == 0)
            {
                Utils.AddText(rTB_error, Color.Red, "Error: word length is 0");
                return;
            }

            string dataString = Utils.StringSubGap("9000202020202020202020202020202020202020202020202020202020202020202020202020");
            int dataLength = count * 4;

            if (dataString.Length != dataLength)
            {
                Utils.AddText(rTB_error, Color.Red, "Error: Data length is not equal to Block count");
                return;
            }
            else
            {
                byte[] hexdata = Utils.StringToByteArray(dataString);
                if (hexdata == null)
                {
                    Utils.AddText(rTB_error, Color.Red, "Error: Input datas need Hex-code checking.");
                    return;
                }

                rTB_write.Clear();
                string origindata = Utils.ByteToString(hexdata).Trim().Replace(" ", "");
                Utils.AddText(rTB_write, Color.Blue, Utils.HexToASCII2(origindata));

                Swing.MemoryWrite(bank, offset, count, dataString);
            }
            
        }

        private void rTB_write_KeyUp(object sender, KeyEventArgs e)
        {
            write_str_cnt.Text = rTB_write.Text.Length.ToString();
        }

        private void btn_clear_cnt_Click(object sender, EventArgs e)
        {
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            //btnExport.Enabled = false;
        }

        private bool sWing_is_open()
        {
            return Swing.IsOpen;                
        }

        private void getData()
        {
            this.Cursor = Cursors.WaitCursor;
            SqlConnection conn = new SqlConnection(strConn);
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter("SP_WORK_CENTER_SEARCH", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataSet ds = new DataSet();
            da.Fill(ds, "WC_SEARCH");
            
            this.Cursor = Cursors.Default;
            dataGridView1.DataSource = ds.Tables["WC_SEARCH"];
            conn.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row == -1)
                return;
            txt_wc_code.Text = dataGridView1.Rows[row].Cells["WC_CODE"].Value.ToString();
            set_rTB_write_set();
            getData_it_scode(txt_wc_code.Text.Trim());

        }

        private void dsmListView_ivt_MouseClick(object sender, MouseEventArgs e)
        {
            
            string[] arrTagText;
            arrTagText = dsmListView_ivt.FocusedItem.SubItems[3].Text.Split('*');
            if (arrTagText.Length == 2)
            {
                if (arrTagText[0]=="PP")
                {
                    string[] pp_str = arrTagText[1].Split('/');
                    if (pp_str.Length == 4) {
                        rd_set1.Checked = true;
                        txt_wc_code.Text = pp_str[0];
                        txt_pp_serno.Text = pp_str[1];
                        txt_it_scode.Text = pp_str[2];
                        txt_size.Text = pp_str[3];
                    }

                }
                else if (arrTagText[0] == "DC")
                {
                    rd_set1.Checked = false;
                    rd_set2.Checked = true;
                    txt_dc.Text=arrTagText[1];
                    

                }
                set_rTB_write_set();
                
            }
        }

        private void rd_set1_CheckedChanged(object sender, EventArgs e)
        {
            if (rd_set1.Checked)
            {
                panel1.Visible = true;
                panel2.Visible = false;
                
            }
            else
            {
                panel1.Visible = false;
                panel2.Visible = true;
            }
            set_rTB_write_set();
        }
        public void set_rTB_write_set(){
            if (rd_set1.Checked)
            {
                panel1.Visible = true;
                panel2.Visible = false;
                rTB_write.Text = "PP*" + txt_wc_code.Text.Trim() + "/" + txt_pp_serno.Text.Trim() + "/" + txt_it_scode.Text.Trim() + "/" + txt_size.Text.Trim();
            }
            else
            {
                panel1.Visible = false;
                panel2.Visible = true;
                rTB_write.Text = "DC*" + txt_dc.Text.Trim();
            }
            write_str_cnt.Text = rTB_write.Text.Length.ToString();
        }

        private void txt_wc_code_KeyUp(object sender, KeyEventArgs e)
        {
            set_rTB_write_set();
        }

        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }

        private void button_inventory_stop_Click(object sender, EventArgs e)
        {
            Swing.InventoryStop();
        }
        private void getData_it_scode(string wc_code)
        {
            this.Cursor = Cursors.WaitCursor;
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_ROUTING_IT_SCODE", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);

            DataSet ds = new DataSet();
            da.Fill(ds, "routing_it_scode_search");

            DataTable dt = ds.Tables["routing_it_scode_search"];

            dataGridView2.DataSource = dt;

            this.Cursor = Cursors.Default;
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row == -1)
                return;
            txt_it_scode.Text = dataGridView2.Rows[row].Cells["PRDT_ITEM"].Value.ToString();
            set_rTB_write_set();
            
        }
    }
}
