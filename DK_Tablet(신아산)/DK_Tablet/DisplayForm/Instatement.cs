﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DK_Tablet.Popup;


namespace DK_Tablet
{
    public partial class Instatement : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
        private SwingLibrary.SwingAPI Swing = null;
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();
        GET_DATA GET_DATA = new GET_DATA();
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        public bool messageCheck = false;
        public string wc_group { get; set; }
        public string str_wc_code { get; set; }

        public string mo_snumb = "", r_start = "";
        DataTable Tag_DT_QR = new DataTable();
        DataTable Tag_DT_OP = new DataTable();
        DataTable Tag_DT_TR = new DataTable();

        //string PP_WC_CODE_str="";
        //string PP_IT_SCODE_str="";
        //string CARD_NO_str="";
        //string PP_SIZE_str = "";
        //string PP_SITE_CODE_str = "";
        //string carrier_no = "";
        //string me_scode = "";
        //string it_pkqty ="";
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";
        Dictionary<string, string> combPortDic = new Dictionary<string, string>();
        MAIN parentForm;
        public Instatement(MAIN form)
        {
            this.parentForm = form;

            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;


                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            combPortDic = Utils.GetComList();
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*
            
            
            
            Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            /*
            groupBox_bt.Enabled = checkBox_dongle.Checked;

            label_battery_volt.Text = "Volts: 0.000 [V]";

            checkBox_rawdata.Checked = Properties.Settings.Default.ConsoleEnable;
            Swing.LogWrite = Properties.Settings.Default.ConsoleEnable;
            WinConsole.Visible = checkBox_rawdata.Checked;

            for (int i = 30; i > 2; i--) comboBox_rfpwr.Items.Add(i);
            comboBox_rfpwr.SelectedIndex = 0;

            for (int j = 0; j < 6; j++) comboBox_channel.Items.Add(j);
            comboBox_channel.SelectedIndex = 0;
            comboBox_channel.Enabled = checkBox_fixed_channel.Checked;

            for (int i = 0; i < 16; i++) comboBox_q.Items.Add(i);
            comboBox_q.SelectedIndex = 4;

            comboBox_algorithm.Items.Add("FIXEDQ");
            comboBox_algorithm.Items.Add("DYNAMICQ");
            comboBox_algorithm.SelectedIndex = 1;

            checkBox_toggle_target.Checked = true;

            comboBox_bank.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.MemoryBank));
            comboBox_bank.SelectedIndex = 1;
            textBox_BlockOffset.Text = "2";
            textBox_BlockCount.Text = "6";
            //textBox_accesspwd.Text = "00000000";

            comboBox_mem_killpwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_accesspwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_epc.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_tid.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_user.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });

            comboBox_mem_killpwd.SelectedIndex = 4;
            comboBox_mem_accesspwd.SelectedIndex = 4;
            comboBox_mem_epc.SelectedIndex = 4;
            comboBox_mem_tid.SelectedIndex = 4;
            comboBox_mem_user.SelectedIndex = 4;

            radioButton_vol_max.Checked = true;
            radioButton_bz_trigger.Checked = true;
            */
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            GET_DATA.get_work_time_master();
            night_time_start = GET_DATA.night_time_start;
            day_time_start = GET_DATA.day_time_start;


            GET_DATA.get_timer_master(wc_group);


            //SWING-U
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

            listView_target_list.ContextMenuStrip = remove_menu;
            Tag_DT_QR.Columns.Add("QR_SDATE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_IT_SCODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SQTY", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SERNO", typeof(string));
            Tag_DT_QR.Columns.Add("QR_CODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOC_CODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_MM_RDATE", typeof(string));


            Tag_DT_OP.Columns.Add("OP_SITE_CODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_PC_SCODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_CARD_NO", typeof(string));
            Tag_DT_OP.Columns.Add("OP_IT_SCODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_SIZE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_LOC_CODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_MM_RDATE", typeof(string));

            //gridControl1.DataSource = GET_DATA.get_po_cnfm("20150103001");
            //.TR_SNUMB,A.TR_SERNO,A.SO_SNUMB,A.SO_SERNO,A.IT_SCODE,A.SO_DUEDT,A.LOT_NO,A.SO_SQUTY, B.IT_SNAME 

            //Tag_DT_TR.Columns.Add("TR_SNUMB", typeof(string));
            //Tag_DT_TR.Columns.Add("TR_SERNO", typeof(string));
            //Tag_DT_TR.Columns.Add("SO_SNUMB", typeof(string));
            //Tag_DT_TR.Columns.Add("SO_SERNO", typeof(string));
            //Tag_DT_TR.Columns.Add("IT_SCODE", typeof(string));
            //Tag_DT_TR.Columns.Add("SO_DUEDT", typeof(string));
            //Tag_DT_TR.Columns.Add("LOT_NO", typeof(string));
            //Tag_DT_TR.Columns.Add("SO_SQUTY", typeof(string));
            //Tag_DT_TR.Columns.Add("IT_SNAME", typeof(string));
            //Tag_DT_TR.Columns.Add("INSP_OK_QTY", typeof(string));
            //Tag_DT_TR.Columns.Add("FILLED_QTY", typeof(string));
        }

        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        Tag_DT_OP.Rows.Clear();
                        Tag_DT_QR.Rows.Clear();
                        Tag_DT_TR.Rows.Clear();
                        lbl_Cnt.Text = "0";
                        //lbl_Cnt2.Text = "0";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        void CloseThreadFunction(object normal)
        {
            bool normal_off = (bool)normal;

            Thread.Sleep(1000);
            this.Invoke(new EventHandler(delegate
            {
                button_com_close_Click(null, null);
            }));

            if (normal_off)
                MessageBox.Show("Swing-U is turned off by user", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Swing-U is turned down abnormaly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";

                Tag_DT_OP.Rows.Clear();
                Tag_DT_QR.Rows.Clear();
            }));

            Swing.ReportTagList();
        }

        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }

        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //OP카드 row add
                    /*
                    DataRow dr = saveDataRow_OP(itemString[3]);

                    if (dr != null)
                    {
                        for (int i = 0; i < advBandedGridView1.RowCount; i++)
                        {
                            if (Convert.ToString(advBandedGridView1.GetRowCellValue(i, "IT_SCODE")).Trim().Equals(dr["OP_IT_SCODE"].ToString().Trim()))
                            {
                                Tag_DT_OP.Rows.Add(dr);
                                lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text) + 1).ToString();
                            }
                            //dataInCell = Convert.ToString(gv.GetRowCellValue(i, "IT_SCODE"));
                        }
                        dataGridView3.DataSource = Tag_DT_OP;
                        
                    }   */
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }

                /*if (dsmListView_ivt.Items.Count == 2)
                {
                    Swing.InventoryStop();
                }*/
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    //리딩
                    str_tr_snumb = "";
                    str_so_duedt = "";
                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    Tag_DT_QR.Rows.Clear();
                    saveDataRow_QR(itemString[3]);

                    //gridControl1.DataSource = GET_DATA.get_po_cnfm(itemString[3]);
                    if (str_tr_snumb.Length == 11)
                    {
                        
                        Tag_DT_TR = GET_DATA.get_po_cnfm(str_tr_snumb);
                        dataGridView1.DataSource = Tag_DT_TR;
                        fillZeroLoss();
                        if (Tag_DT_TR.Rows.Count > 0)
                        {
                            if (btn_Auto_instatement.Text.Trim().Equals("On"))
                            {
                                btn_move.PerformClick();
                            }
                        }
                        else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                        {
                            MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                        }
                        else
                        {
                            MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
                    }
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {

                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";

                    Tag_DT_OP.Rows.Clear();
                    Tag_DT_QR.Rows.Clear();
                    Swing.SetRFPower(25);
                    Swing.ReportAllInformation();


                    lbl_Cnt.Text = "0";

                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;

        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            formClear();

        }
        #endregion
        ContextMenuStrip remove_menu;








        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();
            Swing.ConnectionClose();
            parentForm.Visible = true;
        }


        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }
        //OP카드 dataRow 저장
        public DataRow saveDataRow_OP(string str)
        {
            DataRow dr = null;
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("OP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {

                        dr = Tag_DT_OP.NewRow();
                        dr["OP_SITE_CODE"] = strTag[0];
                        dr["OP_PC_SCODE"] = strTag[1];
                        dr["OP_CARD_NO"] = strTag[2];
                        dr["OP_IT_SCODE"] = strTag[3];
                        dr["OP_SIZE"] = strTag[4];

                    }
                }
            }
            return dr;
        }
        //QR dataRow 저장
        string str_tr_snumb = "";
        string str_so_duedt = "";
        public void saveDataRow_QR(string str)
        {
            //DataRow dr=null;
            string[] strTags = str.Split('/');
            if (strTags.Length == 2)
            {

                str_tr_snumb = strTags[0];
                str_so_duedt = strTags[1];

            }
            else
            {
                str_tr_snumb = "";
                str_so_duedt = "";
            }
        }
        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check(string prdt_item, string card_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }


        private void Injection_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Swing.InventoryStop();
            this.Close();
        }

        private void timer_now_Tick(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            N_timer.Text = now.ToString();
        }

        public void formClear()
        {
            lbl_Cnt.Text = "0";
            Tag_DT_OP.Rows.Clear();
            Tag_DT_QR.Rows.Clear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStop();
            Swing.TagListClear();
            Swing.InventoryStart();
        }

        private void btn_move_Click(object sender, EventArgs e)
        {
            bool check_loc_code = true;
            foreach (DataRow dr in Tag_DT_TR.Rows)
            {
                if (dr["LOC_CHECK"].Equals("N"))
                {
                    check_loc_code = false;
                    break;
                }
            }
            /*
            if (dataGridView2.Rows.Count == 0 && dataGridView3.Rows.Count == 0)
            {
                MessageBox.Show("입고할 자재QR 코드 또는 OP 카드를 읽지 않았습니다.");
            }
            else
            {
              */
            if (check_loc_code)
            {
                insert(str_tr_snumb, str_so_duedt);
            }
            else
            {
                MessageBox.Show("기준정보에 입고 로케이션 설정이 되지않아서\n입고처리를 할 수 없습니다.");
            }
            /*
            Tag_DT_QR.Columns.Add("QR_SDATE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_IT_SCODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SQTY", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOT_SERNO", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOT_NO", typeof(string));

            Tag_DT_OP.Columns.Add("OP_SITE_CODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_PC_SCODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_CARD_NO", typeof(string));
            Tag_DT_OP.Columns.Add("OP_IT_SCODE", typeof(string));
            Tag_DT_OP.Columns.Add("OP_SIZE", typeof(string));
            */



            //}
        }

        private void insert(string tr_snumb, string so_duedt)
        {
            string strCon = Properties.Settings.Default.SQL_DKQT;
            try
            {
                using (SqlConnection conn = new SqlConnection(strCon))
                {
                    conn.Open();
                    using (SqlTransaction trans = conn.BeginTransaction())
                    {
                        string query_update = "UPDATE PO_CNFM SET SO_SQUTY=@SO_SQUTY WHERE TR_SNUMB=@TR_SNUMB AND TR_SERNO=@TR_SERNO";
                        using (SqlCommand cmd_first = new SqlCommand(query_update, conn, trans))
                        {
                            for (int idx = 0; idx < dataGridView1.Rows.Count; idx++)
                            {
                                cmd_first.Parameters.Clear();
                                cmd_first.Parameters.AddWithValue("@TR_SNUMB", dataGridView1.Rows[idx].Cells["TR_SNUMB"].Value.ToString());
                                cmd_first.Parameters.AddWithValue("@SO_SQUTY", dataGridView1.Rows[idx].Cells["REAL_QTY"].Value.ToString());
                                cmd_first.Parameters.AddWithValue("@TR_SERNO", dataGridView1.Rows[idx].Cells["TR_SERNO"].Value.ToString());

                                cmd_first.ExecuteNonQuery();
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand("SP_ALL_AUTO_SCM_NEW", conn, trans))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@LOGIN_ID", SqlDbType.VarChar, 20).Value = "";
                            cmd.Parameters.Add("@TR_SNUMB", SqlDbType.VarChar, 11).Value = tr_snumb;
                            cmd.Parameters.Add("@SDATE", SqlDbType.VarChar, 8).Value = so_duedt;
                            cmd.Parameters.Add("@EDATE", SqlDbType.VarChar, 8).Value = so_duedt;
                            cmd.Parameters.Add("@SITE_CODE", SqlDbType.VarChar, 10).Value = Properties.Settings.Default.SITE_CODE;

                            cmd.ExecuteNonQuery();
                        }

                        string query_swap = "UPDATE PO_CNFM SET SO_SQUTY=@SO_SQUTY, INSP_OK_QTY=@INSP_OK_QTY "
                                            + " WHERE TR_SNUMB=@TR_SNUMB AND TR_SERNO=@TR_SERNO AND IT_SCODE=@IT_SCODE";
                        using (SqlCommand cmd_swap = new SqlCommand(query_swap, conn, trans))
                        {
                            for (int idx = 0; idx < dataGridView1.Rows.Count; idx++)
                            {
                                cmd_swap.Parameters.Clear();
                                cmd_swap.Parameters.AddWithValue("@SO_SQUTY", dataGridView1.Rows[idx].Cells["SO_SQUTY"].Value.ToString());
                                cmd_swap.Parameters.AddWithValue("@INSP_OK_QTY", DBNull.Value);
                                cmd_swap.Parameters.AddWithValue("@TR_SNUMB", dataGridView1.Rows[idx].Cells["TR_SNUMB"].Value.ToString());
                                cmd_swap.Parameters.AddWithValue("@TR_SERNO", dataGridView1.Rows[idx].Cells["TR_SERNO"].Value.ToString());
                                cmd_swap.Parameters.AddWithValue("@IT_SCODE", dataGridView1.Rows[idx].Cells["IT_SCODE"].Value.ToString());

                                cmd_swap.ExecuteNonQuery();
                            }
                        }
                        
                        string query_slio_yn = "USP_TABLET_INSTATEMENT_SLIO_CHECK";
                        using (SqlCommand cmd_check_slio_yn = new SqlCommand(query_slio_yn, conn, trans))
                        {
                            cmd_check_slio_yn.CommandType = CommandType.StoredProcedure;

                            cmd_check_slio_yn.Parameters.AddWithValue("@TR_SNUMB", tr_snumb);

                            cmd_check_slio_yn.ExecuteNonQuery();
                        }

                        trans.Commit();

                        lbl_Cnt.Text = "0";
                        //lbl_Cnt2.Text = "0";
                        Tag_DT_OP.Rows.Clear();
                        Tag_DT_QR.Rows.Clear();
                        Tag_DT_TR.Rows.Clear();
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        Swing.InventoryStop();
                        Swing.TagListClear();
                        MyMessageBox_2.ShowBox("입고가 완료 되었습니다.", "입고완료", "거래명세서 번호 : " + str_tr_snumb, 3);
                        str_tr_snumb = "";
                        str_so_duedt = "";
                    } // trans
                } // conn
            } // try
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void advBandedGridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void advBandedGridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {

                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    /*
                    // 검수차이
                    case "LOSS":
                        KeyPad KeyPad = new KeyPad();
                        // KeyPad.txtDigit.Text = senderGrid.Rows[e.RowIndex].Cells["SO_SQUTY"].Value.ToString();
                        KeyPad.txtDigit.Text = "0";
                        if (KeyPad.ShowDialog() == DialogResult.OK)
                        {

                            //int remain_sqty = int.Parse(senderGrid.Rows[e.RowIndex].Cells["SO_SQUTY"].Value.ToString());
                            //if (remain_sqty < int.Parse(KeyPad.txt_value))
                            //{
                            //    MessageBox.Show("기존수량을 초과 할 수 없습니다.");
                            //    return;
                            //}
                            var dt = senderGrid.DataSource as DataTable;
                            dt.Rows[e.RowIndex]["LOSS"] = KeyPad.txt_value;
                            dt.Rows[e.RowIndex]["REAL_QTY"] = int.Parse(dt.Rows[e.RowIndex]["QTY"].ToString()) - int.Parse(KeyPad.txt_value);
                        }

                        break;
                    */
                    case "REAL_QTY":
                        KeyPad KeyPad = new KeyPad();
                        KeyPad.txtDigit.Text = "0";
                        if (KeyPad.ShowDialog() == DialogResult.OK)
                        {
                            var dt = senderGrid.DataSource as DataTable;
                            dt.Rows[e.RowIndex]["REAL_QTY"] = ((int)Convert.ToDouble(KeyPad.txt_value)).ToString();
                            dt.Rows[e.RowIndex]["LOSS"] = int.Parse(dt.Rows[e.RowIndex]["QTY"].ToString()) - int.Parse(dt.Rows[e.RowIndex]["REAL_QTY"].ToString());
                        }
                        break;
                    case "BTN_DELETE":
                        Tag_DT_TR.Rows[e.RowIndex].Delete();
                        break;
                }

            }
        }

        private void btn_Auto_instatement_Click(object sender, EventArgs e)
        {
            if (btn_Auto_instatement.Text.Trim().Equals("On"))
            {
                btn_Auto_instatement.Text = "Off";
            }
            else
            {
                btn_Auto_instatement.Text = "On";
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            lbl_Cnt.Text = "0";
            //lbl_Cnt2.Text = "0";
            Tag_DT_OP.Rows.Clear();
            Tag_DT_QR.Rows.Clear();
            Tag_DT_TR.Rows.Clear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStop();
            Swing.TagListClear();

            str_tr_snumb = "";
            str_so_duedt = "";
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            str_tr_snumb = "";
            str_so_duedt = "";

            Tag_DT_QR.Rows.Clear();
            saveDataRow_QR(textEdit1.Text);

            if (str_tr_snumb.Length == 11)
            {
                
                Tag_DT_TR = GET_DATA.get_po_cnfm(str_tr_snumb);
                dataGridView1.DataSource = Tag_DT_TR;
                fillZeroLoss();
                if (Tag_DT_TR.Rows.Count > 0)
                {
                    if (btn_Auto_instatement.Text.Trim().Equals("On"))
                    {
                        btn_move.PerformClick();
                    }
                }
                else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                {
                    MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                }
                else
                {
                    MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                }
            }
            else
            {
                MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            str_tr_snumb = textEdit1.Text;
            str_so_duedt = DateTime.Now.ToString("yyyyMMdd"); // 테스트용 임시 오늘 날짜

            Tag_DT_QR.Rows.Clear();

            if (str_tr_snumb.Length == 11)
            {
                
                Tag_DT_TR = GET_DATA.get_po_cnfm(str_tr_snumb);
                dataGridView1.DataSource = Tag_DT_TR;
                fillZeroLoss();
                if (Tag_DT_TR.Rows.Count > 0)
                {
                    if (btn_Auto_instatement.Text.Trim().Equals("On"))
                    {
                        btn_move.PerformClick();
                    }
                }
                else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                {
                    MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                }
                else
                {
                    MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                }
            }
            else
            {
                MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
            }

        }

        private void fillZeroLoss()
        {
            var dt = dataGridView1.DataSource as DataTable;
            dt.Columns.Add("LOSS", typeof(string));
            dt.Columns.Add("QTY", typeof(string));
            dt.Columns.Add("REAL_QTY", typeof(string));
            for (int idx = 0; idx < dataGridView1.RowCount; idx++)
            {
                dt.Rows[idx]["LOSS"] = 0;
                dt.Rows[idx]["QTY"] = Convert.ToInt32(dt.Rows[idx]["INSP_OK_QTY"]) - Convert.ToInt32(dt.Rows[idx]["SL_SQUTY"]);
                dt.Rows[idx]["REAL_QTY"] = dt.Rows[idx]["QTY"];
            }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                simpleButton1.PerformClick();
            }
        }


    }
}
