﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;
using DK_Tablet.PRINT;
using DevExpress.XtraReports.UI;


namespace DK_Tablet
{
    public partial class Paint_Out : Form
    {
        [DllImport("user32.dll")]        
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);//리더기와 pc간 통신하기 위한 함수

            
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        Func_Mobis_Print Func_Mobis_Print = new Func_Mobis_Print();
        private SwingLibrary.SwingAPI Swing2 = null;//스윙-u 사용하기 위한 API 객체 생성
        private string[] str_bank2 = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        //입구 리더기
        //string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //string rec_lot_no = "";
        //string rec_card_no = "";//다음 pp 카드
        //string rec_it_scode = "";//다음 품목코드
        //string sel_carrier_no = "";//다음대차번호
        //string sel_lot_no = "";
        //string sel_card_no = "";//들어온 카드 번호
        //string sel_it_scode = "";//들어온 품목코드
        string carrier_yn = "";//공대차인지 아닌지 구분자
        //float f_mm_sqty = 0;//대차에 적재 되어있는 제품 수량
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        string sw_code = "";//근무형태 코드
        string str_worker = "";//작업자
        //출구 리더기
        PpCard_reading PpCard_reading;
        string out_carrier_no = "";// 출구로 나가는 대차 번호
        DataTable Tag_DT_PP = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관
        DataTable Tag_DT_DC = new DataTable();//출구에서 리더기로 읽어 들인 대차 카드 보관
        DataTable po_DT = new DataTable();//생산계획을 여러개 선택 할 수 있도록 선택한 생산계획 정보 보관
        
        PpCard_Success10 PpCard_Success10 = new PpCard_Success10();
        string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //혼적 datatable
        DataTable Tag_DT_PP_MIX = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관

        string PP_WC_CODE_str = "";//작업장
        string PP_IT_SCODE_str = "";//품목코드
        string CARD_NO_str = "";//카드번호
        string PP_SIZE_str = "";//수용수
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";


        private BackgroundWorker worker = new BackgroundWorker();
        FtpUtil ftpUtil;
        string path = "ftp://203.251.168.131:4104";
        string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        Image[] work_image = new Image[10];


        DataTable DataGridViewDT = new DataTable();
        public Paint_Out(MAIN form)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            

            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
            Utils.GetComList(comboBox_ports2);
            if (comboBox_ports2.Items.Count > 0)
            {
                button_com_open2.Enabled = true;
                button_com_close2.Enabled = false;

                for (int i = 0; i < comboBox_ports2.Items.Count; i++)
                {
                    if (comboBox_ports2.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName2))
                    {
                        comboBox_ports2.SelectedIndex = i;
                        break;
                    }
                }
            }

            Swing2 = new SwingLibrary.SwingAPI();
            Swing2.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus2);
            Swing2.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory2);
            Swing2.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent2);
            Swing2.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged2);
            /*Swing2.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing2.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound2);
            Swing2.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD2);
            Swing2.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent2);
            PpCard_reading = new PpCard_reading(this);
        }
        
        

        #region Swing-u 함수2(Notify)
        void Swing_NotifyParameterChanged2(SwingLibrary.SwingParameter parameterType)
        {
            
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing2.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing2.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing2.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing2.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing2.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing2.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing2.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing2.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing2.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing2.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing2.GetVersionHW();
                        //label_version_fw.Text = Swing2.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing2.GetTagCount();
                        int ui_count = dsmListView_ivt2.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList2).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing2.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing2.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing2.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing2.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound2(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID2(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID2(found_tag_uid, found_tag_index);
                }

                if (Swing2.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing2.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing2.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD2(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport2(data.Trim('\0'));
            else
                Swing_ParseTagReport2(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent2(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt2.Items.Clear();
                        ddc_ivt2.DigitText = "00000";
                        //txtOut_carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        //out_carrier_no = "";
                        //Tag_DT_DC.Clear();
                        Tag_DT_PP.Clear();
                        //po_DT.Clear();
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus2(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory2(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport2(data.Trim('\0'));
            else
                Swing_ParseTagReport2(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent2(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수2
        private object locker2 = new object();
        private bool key2 = false;
        private void SyncTagList2()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt2.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt2.Items[i].SubItems[2].Text; }));

                lock (locker2) key2 = false;
                Swing2.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker2) if (key2) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
            }));

            Swing2.ReportTagList();
        }

        private void Swing_ParseTagReport2(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing2.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing2.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID2(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID2(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport2(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess2(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport2(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess2(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess2(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();

                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);

                    if (dsmListView_ivt2.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport2(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing2.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing2.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID2(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID2(data, 0);
            }
        }
        private void UpdateUID2(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID2(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID2(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {

                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                    //리딩
                    //PP카드 row add
                    
                    DataRow dr = saveDataRow_PP2(itemString[3]);

                    if (dr != null)
                    {
                        if (btn_mix_toggle.Text.Trim() == "Off")
                        {
                            Tag_DT_PP.Rows.Add(dr);
                            Tag_DT_PP.DefaultView.Sort = "CARD_NO ASC";
                        }
                        else if (btn_mix_toggle.Text.Trim() == "On")
                        {
                            DataRow[] drr = Tag_DT_PP_MIX.Select("IT_SCODE='" + dr["IT_SCODE"].ToString() + "'");

                            if (drr.Length == 0)
                            {
                                //if (작업장에서 생산계획이 있는품목인지 체크)
                                if (CHECK_FUNC.Mix_work_plan_check_item(dr["IT_SCODE"].ToString()))
                                {
                                    Tag_DT_PP_MIX.Rows.Add(dr);
                                }

                            }
                        }

                    }
                    /*
                    if (txtOut_carrier_no.Enabled)
                    {
                        saveDataRow_DC_S2(itemString[3]);
                    }*/
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        public void saveDataRow_DC_S2(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    if (CHECK_FUNC.carrier_check(strTags[1].Trim()))
                    {
                        if (txtOut_carrier_no.Text.Trim() == "")
                        {
                            if (CHECK_FUNC.Match_carrier_no(txt_IT_SCODE.Text.Trim(), strTags[1].Trim()))//대차 번호 가 맞는지 체크 
                            {
                                out_carrier_no = strTags[1].Trim();
                                txtOut_carrier_no.Text = out_carrier_no;
                                //MessageBox.Show(out_carrier_no);
                                if (btn_on_off.Text.Trim().Equals("On"))
                                {

                                    Swing2.InventoryStop();
                                    Swing2.TagListClear();
                                    dsmListView_ivt2.Items.Clear();
                                    ddc_ivt2.DigitText = "00000";
                                }
                            }
                            else
                            {
                                MessageBox.Show("품목에 해당하는 대차가 아닙니다.");
                            }
                        }
                    }
                    else
                    {
                        //saveDataRow_DC_S(str);

                        if (!string.IsNullOrWhiteSpace(in_carrier_no))
                        {
                            //if (sel_carrier_no == in_carrier_no && sel_it_scode == rec_it_scode && sel_card_no == rec_card_no)
                            //{
                            //대차가 작업장에 있는지 체크 (품목,PP시리얼,대차코드)
                            ////
                            DataRow[] Dr = DataGridViewDT.Select("CARRIER_NO='" + in_carrier_no + "'");

                            if (Dr.Length > 0)
                            {
                                Swing2.InventoryStop();
                                //작업장으로 이동

                                MOVE_FUNC.move_insert_NEW(Dr[0][5].ToString(), Dr[0][4].ToString(), Dr[0][3].ToString(), float.Parse(Dr[0][10].ToString())
                                    , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());

                                //Success_Form Success_Form = new Success_Form();
                                //Success_Form.carrier_no = Dr[0][3].ToString();
                                //Success_Form.Show();
                                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                                /*dataGridView1.DataSource = DataGridViewDT;
                                dataGridView1.CurrentCell = null;
                                */

                                //txtIn_Carrier_no.Text = "";
                                txtIt_scode.Text = "";
                                //txtCard_no.Text = "";
                                in_carrier_no = "";
                                //rec_it_scode = "";
                                //rec_card_no = "";
                                //sel_carrier_no = "";
                                //sel_it_scode = "";
                                //sel_card_no = "";
                                
                                Swing2.TagListClear();
                                dsmListView_ivt2.Items.Clear();
                                ddc_ivt2.DigitText = "00000";

                                Swing2.InventoryStart();
                                
                            }
                            else
                            {
                                //txtIn_Carrier_no.Text = "";
                                txtIt_scode.Text = "";
                                //txtCard_no.Text = "";
                                in_carrier_no = "";
                                //rec_it_scode = "";
                                //rec_card_no = "";

                                //Swing.TagListClear();
                                //dsmListView_ivt.Items.Clear();
                                //ddc_ivt.DigitText = "00000";
                            }

                        }   
                    }

                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        /// 
        /*
        public void saveDataRow_DC_S(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    in_carrier_no = strTags[1].Trim();
                }
            }

        }*/
        private void UpdateUID2(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening2(object sender, CancelEventArgs e)
        {
            if (listView_target_list2.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove2(object sender, EventArgs e)
        {
            if (listView_target_list2.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list2.SelectedIndices[0];
            ListViewItem item = listView_target_list2.Items[idx];

            listView_target_list2.BeginUpdate();

            listView_target_list2.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list2.Items.Count; i++)
            {
                listView_target_list2.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list2.EndUpdate();
        }

        private void button_com_open_Click2(object sender, EventArgs e)
        {
            try
            {
                //Swing2.ConnectionOpen(comboBox_ports2.SelectedItem.ToString());
                Swing2.ConnectionOpen(comboBox_ports2.SelectedValue.ToString(), 5);

                if (Swing2.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing2.PortName);
                    Properties.Settings.Default.ComPortName = Swing2.PortName;
                    Properties.Settings.Default.Save();
                    Swing2.InventoryStop();
                    Swing2.TagListClear();
                    dsmListView_ivt2.Items.Clear();
                    ddc_ivt2.DigitText = "00000";
                    txtOut_carrier_no.Text = "";
                    txtIt_scode.Text = "";

                    Swing2.SetRFPower(27);
                    Swing2.ReportAllInformation();
                   
                        work_plan_popup_paint();
                   
                    //po_release_popup();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing2.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing2.PortName);
                WinConsole.WriteLine(ex.Message);
            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }

            comboBox_ports2.Enabled = !Swing2.IsOpen;
            button_com_open2.Enabled = !Swing2.IsOpen;
            button_com_close2.Enabled = Swing2.IsOpen;
            //checkBox_dongle.Enabled = !Swing2.IsOpen;
        }

        private void button_com_close_Click2(object sender, EventArgs e)
        {
            try
            {
                if (Swing2.ConnectionClose())
                {
                    WinConsole.WriteLine("{0} is closed successfully", Swing2.PortName);
                }
                else
                {
                    MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }

            comboBox_ports2.Enabled = !Swing2.IsOpen;
            button_com_open2.Enabled = !Swing2.IsOpen;
            button_com_close2.Enabled = Swing2.IsOpen;
            //checkBox_dongle.Enabled = !Swing2.IsOpen;
        }
        #endregion

        ContextMenuStrip remove_menu2;
        
        private void Form1_Load(object sender, EventArgs e)
        {
            

            try
            {
                sqty_arry[0] = "0";
                sqty_arry[1] = "0";
                
                PpCard_reading.Show();
                PpCard_reading.Visible = false;
                PpCard_Success10.Show();
                PpCard_Success10.Visible = false;
                
                    lab_carrier_sqty.Visible = true;
                    btn_carrier_sqty.Visible = true;
                
                GET_DATA.get_work_time_master();
                night_time_start = GET_DATA.night_time_start;
                day_time_start = GET_DATA.day_time_start;
                
                //timer_now.Start();
                //GET_DATA.SYSTEMTIME stime = new GET_DATA.SYSTEMTIME();
                //stime = GET_DATA.GetTime();
                //N_timer.Text = stime.wYear.ToString() + "-" + stime.wMonth + "-" + stime.wDay + " " + stime.wHour + ":" + stime.wMinute + ":" + stime.wSecond;
                
                GET_DATA.get_timer_master(wc_group);
                timer_po_start.Interval = int.Parse(GET_DATA.po_timer) * 1000;
                timer_pp_start.Interval = int.Parse(GET_DATA.pp_timer) * 1000;

                lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";


                luePrint_type.Properties.DataSource = GET_DATA.Print_type_DropDown();
                luePrint_type.Properties.DisplayMember = "TYPE_NAME";
                luePrint_type.Properties.ValueMember = "TYPE_CODE";
                luePrint_type.EditValue = "A";
                //lookUpEdit1.SelectionStart = 0;
                //lueWc_code.ItemIndex = 0;
                try
                {
                    if (regKey.GetValue("WC_CODE") == null)
                    {
                        regKey.SetValue("WC_CODE", "");
                    }
                    else
                    {
                        if (regKey.GetValue("WC_CODE").ToString() != "")
                        {

                            lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR : "+ex.Message);
                }

                //SWING-U
                this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
                SendMessage((int)dsmListView_ivt2.Handle, 0x1000 + 54, 0x00010000, 0x00010000);
               
                remove_menu2 = new ContextMenuStrip();
                ToolStripMenuItem item2 = new ToolStripMenuItem("Remove");
                item2.Click += new EventHandler(target_remove2);
                remove_menu2.Items.Add(item2);

                remove_menu2.Opening += new CancelEventHandler(remove_menu_Opening2);

                listView_target_list2.ContextMenuStrip = remove_menu2;
                Tag_DT_PP.Columns.Add("SITE_CODE", typeof(string));
                Tag_DT_PP.Columns.Add("WC_CODE", typeof(string));
                Tag_DT_PP.Columns.Add("CARD_NO", typeof(string));
                Tag_DT_PP.Columns.Add("IT_SCODE", typeof(string));
                Tag_DT_PP.Columns.Add("SIZE", typeof(string));
                Tag_DT_PP.Columns.Add("CHECK_YN", typeof(string));

                Tag_DT_PP_MIX.Columns.Add("SITE_CODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("WC_CODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("CARD_NO", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("IT_SCODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("SIZE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("CHECK_YN", typeof(string));

                po_DT.Columns.Add("MO_SNUMB", typeof(string));
                po_DT.Columns.Add("PO_IT_SCODE", typeof(string));
                po_DT.Columns.Add("PO_IT_SNAME", typeof(string));
                po_DT.Columns.Add("PO_ME_SCODE", typeof(string));
                
                DataGridViewDT.Columns.Add("ROWNUM", typeof(string));//0
                DataGridViewDT.Columns.Add("VW_SNUMB", typeof(string));//1
                DataGridViewDT.Columns.Add("CARRIER_DATE", typeof(string));//2
                //DataGridViewDT.Columns.Add("CARRIER_NO", typeof(string));//3
                DataGridViewDT.Columns.Add("CARD_NO", typeof(string));//4
                DataGridViewDT.Columns.Add("PRDT_ITEM", typeof(string));//5
                DataGridViewDT.Columns.Add("LOT_DATE", typeof(string));//6
                DataGridViewDT.Columns.Add("HARFTYPE", typeof(string));//7
                DataGridViewDT.Columns.Add("LOT_NO", typeof(string));//8
                DataGridViewDT.Columns.Add("CHECK_YN", typeof(string));//9
                DataGridViewDT.Columns.Add("GOOD_QTY", typeof(string));//10
                DataGridViewDT.Columns.Add("HH", typeof(string));//11

                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                //작업표준서 가져오기
                timer_now.Stop();
                imageSlider1.Images.Clear();
                string ftp_str = AdjustDir(txt_IT_SCODE.Text.Trim());

                ftpUtil.FTPDirectioryCheck(ftp_str,"OUT");

                work_image = ftpUtil.get_file_list(ftp_str,"OUT");

                for (int i = 0; i < work_image.Length; i++)
                {
                    if (work_image[i] != null)
                        imageSlider1.Images.Add(work_image[i]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                timer_now.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing2.InventoryStop();
            Swing2.ConnectionClose();
            parentForm.Visible = true;
        }

        bool check_pp_message = false;
        //PP카드 dataRow 저장
        public DataRow saveDataRow_PP2(string str)
        {
            DataRow dr = null;
            try
            {
                check_pp_message = false;
                string[] strTags = str.Split('*');
                if (strTags.Length == 2)
                {
                    if (strTags[0].Equals("PP"))
                    {
                        string[] strTag = strTags[1].Split('/');

                        if (strTag.Length == 5)
                        {
                            //if (str_wc_code.Trim() == strTag[1].Trim())
                            //{
                            if (btn_mix_toggle.Text.Trim() == "Off")
                            {
                                dr = Tag_DT_PP.NewRow();
                                dr["SITE_CODE"] = strTag[0];
                                dr["WC_CODE"] = strTag[1];
                                dr["CARD_NO"] = strTag[2];
                                dr["IT_SCODE"] = strTag[3];
                                if (btn_carrier_sqty.Visible)
                                {
                                    dr["SIZE"] = btn_carrier_sqty.Text.Trim();
                                }
                                else
                                {
                                    dr["SIZE"] = strTag[4];
                                }

                                if (dr["SIZE"].ToString().Trim().Equals("0"))
                                {
                                    Sqty_popup Sqty_popup = new Sqty_popup();
                                    Sqty_popup.sqty = "0";
                                    if (Sqty_popup.ShowDialog() == DialogResult.OK)
                                    {
                                        dr["SIZE"] = Sqty_popup.sqty;
                                    }

                                }
                                if (PpCard_reading_chk)
                                {
                                    if (pp_card_check2(strTag[3], strTag[2], strTag[1]))
                                    {
                                        if (btn_on_off.Text.Trim().Equals("On"))//자동일때 실적 바로 등록
                                        {
                                            Swing2.InventoryStop();
                                            //foreach (DataRow Po_dr in po_DT.Rows)
                                            //{
                                            if (txt_IT_SCODE.Text.Trim().Equals(strTag[3]))
                                            {
                                                bool check = false;

                                                check = work_input_save_virtual(strTag[0].ToString(), mo_snumb, strTag[3].ToString(), str_wc_code.Trim()
                                                    , dr["SIZE"].ToString(), txt_datetime.Text, strTag[2].ToString(), out_carrier_no, "Y");

                                                if (check)
                                                {
                                                    
                                                    dr["CHECK_YN"] = "생산완료";
                                                    PpCard_reading.TopLevel = false;
                                                    PpCard_reading.TopMost = false;
                                                    PpCard_reading.Visible = false;
                                                    PpCard_reading_chk = false;
                                                    Swing2.TagListClear();
                                                    dsmListView_ivt2.Items.Clear();
                                                    ddc_ivt2.DigitText = "00000";
                                                    Tag_DT_PP.Rows.Clear();
                                                    Tag_DT_DC.Rows.Clear();
                                                    out_carrier_no = "";
                                                    txtOut_carrier_no.Text = "";
                                                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                                    txt_datetime.Text = r_start;
                                                    set_현재고_최대생산가능수량();
                                                    if (me_scode.Trim().Equals("40"))
                                                    {
                                                        // 식별표 출력
                                                        //it_chart_print(strTag[3].ToString(), int.Parse(dr["SIZE"].ToString()));

                                                        PpCard_Success10.TopLevel = true;
                                                        PpCard_Success10.TopMost = true;
                                                        PpCard_Success10.Visible = true;
                                                        PpCard_Success10.set_text(strTag[2].ToString(), 11);
                                                    }
                                                    else
                                                    {
                                                        PpCard_Success10.TopLevel = true;
                                                        PpCard_Success10.TopMost = true;
                                                        PpCard_Success10.Visible = true;
                                                        PpCard_Success10.set_text(strTag[2].ToString(), 11);
                                                    }
                                                }
                                                else
                                                {
                                                    PpCard_reading.TopLevel = false;
                                                    PpCard_reading.TopMost = false;
                                                    PpCard_reading.Visible = false;
                                                    PpCard_reading_chk = false;
                                                    Swing2.TagListClear();
                                                    dsmListView_ivt2.Items.Clear();
                                                    ddc_ivt2.DigitText = "00000";
                                                    Tag_DT_PP.Rows.Clear();
                                                    Tag_DT_DC.Rows.Clear();
                                                    out_carrier_no = "";
                                                    txtOut_carrier_no.Text = "";
                                                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                                    txt_datetime.Text = r_start;
                                                    set_현재고_최대생산가능수량();
                                                    MessageBox.Show("실적 등록 처리중 오류가 발생 하였습니다.\n다시 한번 등록 해주세요.....");
                                                }
                                            }
                                            else
                                            {

                                                Swing2.TagListClear();
                                                dsmListView_ivt2.Items.Clear();
                                                ddc_ivt2.DigitText = "00000";
                                                Tag_DT_PP.Rows.Clear();
                                                PpCard_reading.TopLevel = false;
                                                PpCard_reading.TopMost = false;
                                                PpCard_reading.Visible = false;
                                                PpCard_reading_chk = false;
                                                MessageBox.Show("PP카드의 품목과 \n선택된 생산계획의 품목이 틀립니다.");
                                            }
                                            //}

                                        }
                                        else
                                        {
                                            dr["CHECK_YN"] = "생산가능";
                                        }
                                    }
                                    else
                                    {
                                        if (btn_on_off.Text.Trim().Equals("On"))
                                        {
                                            PpCard_reading.TopLevel = false;
                                            PpCard_reading.TopMost = false;
                                            PpCard_reading.Visible = false;
                                            PpCard_reading_chk = false;
                                            MessageBox.Show("생산완료된 카드 입니다.");
                                        }
                                        dr["CHECK_YN"] = "생산완료";
                                    }
                                }
                                else
                                {
                                    Swing2.InventoryStop();
                                    Swing2.TagListClear();
                                    dsmListView_ivt2.Items.Clear();
                                    ddc_ivt2.DigitText = "00000";
                                }
                            }
                            else if (btn_mix_toggle.Text.Trim() == "On")
                            {
                                dr = Tag_DT_PP_MIX.NewRow();
                                dr["SITE_CODE"] = strTag[0];
                                dr["WC_CODE"] = strTag[1];
                                dr["CARD_NO"] = strTag[2];
                                dr["IT_SCODE"] = strTag[3];

                                dr["SIZE"] = 0;

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("error : " + ex.Message);
            }
            finally
            {

            }
            return dr;

        }
        /*
         * 
         */
        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check2(string prdt_item, string pp_serno,string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + pp_serno + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        
        private void btn_po_release_Click(object sender, EventArgs e)
        {
            try
            {
                work_plan_popup_paint();
                //po_release_popup();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        string[] sqty_arry = new string[2];
        
        public void work_plan_popup_paint()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                //if (Swing2.IsOpen)
                if (true)
                {

                    Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                    Work_plan_search.wc_group = wc_group;
                    Work_plan_search.wc_code = str_wc_code;
                    if (Work_plan_search.ShowDialog() == DialogResult.OK)
                    {
                        formClear2();
                        txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;                        
                        //작업표준서
                        worker.RunWorkerAsync();
                        txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                        txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                        txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                        me_scode = Work_plan_search.ME_SCODE_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                        me_scode = Work_plan_search.ME_SCODE_str;
                        //max_sqty = Work_plan_search.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        carrier_yn = "Y";
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        btn_carrier_sqty.Text = Work_plan_search.IT_PKQTY_str;
                        /*
                        DataRow[] drr = po_DT.Select("PO_IT_SCODE='" + txt_IT_SCODE.Text.Trim() + "'");
                        for (int i = 0; i < drr.Length; i++)
                            po_DT.Rows.Remove(drr[i]);
                        po_DT.AcceptChanges();

                        DataRow[] po_row = po_DT.Select("MO_SNUMB='" + mo_snumb + "'");

                        if (po_row.Length == 0)
                        {
                            DataRow dr;
                            dr = po_DT.NewRow();
                            dr["MO_SNUMB"] = mo_snumb;
                            dr["PO_IT_SCODE"] = txt_IT_SCODE.Text;
                            dr["PO_IT_SNAME"] = txt_IT_SNAME.Text;
                            dr["PO_ME_SCODE"] = me_scode;

                            po_DT.Rows.Add(dr);
                        }*/
                        //양품수량 불량수량 들고오기
                        GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        txt_good_qty.Text = GET_DATA.good_qty;
                        txt_fail_qty.Text = GET_DATA.fail_qty;
                        Swing2.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                        //Swing2.InventoryStart();
                        DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                        txtIt_scode.Text = "";
                        set_현재고_최대생산가능수량();
                        shiftwork();//작업유형 가져오기
                        set_worker_info();//작업자 팝업
                        sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                        txt_day_sqty.Text = sqty_arry[0];
                        txt_night_sqty.Text = sqty_arry[1];
                        txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                        timer_po_start.Start();
                        lueWc_code.Enabled = false;

                        //작업 시작 시간 업데이트 
                        SUB_SAVE.작업시작시간_업데이트(str_wc_code,txt_IT_SCODE.Text.Trim(),"","1");

                        string print_type = Settings_Xml.read(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                        if (string.IsNullOrWhiteSpace(print_type))
                        {
                            Settings_Xml.reWrite(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                            print_type = luePrint_type.EditValue.ToString().Trim();
                        }

                        luePrint_type.EditValue = print_type;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }/*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }

        //클리어
        public void formClear()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_fail_qty.Text = null;            
            mo_snumb = "";            
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

        }
        public void formClear2()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_fail_qty.Text = null;
            txt_day_sqty.Text = null;
            txt_night_sqty.Text = null;
            txtOut_carrier_no.Text = null;
            out_carrier_no = "";
            mo_snumb = "";
            Swing2.InventoryStop();
            Swing2.TagListClear();
            dsmListView_ivt2.Items.Clear();
            ddc_ivt2.DigitText = "00000";
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

        }
        //비가동 등록
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            try
            {
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                //PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code
                Dt_Input.PP_SITE_CODE_str = PP_SITE_CODE_str;
                Dt_Input.mo_snumb = mo_snumb;
                Dt_Input.str_wc_code = str_wc_code;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str, mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime, str_wc_code);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        


        private void btn_fail_input_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("생산계획을 선택해주세요");
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    Fail_Input Fail_Input = new Fail_Input();
                    Fail_Input.wc_group = wc_group;
                    if (Fail_Input.ShowDialog() == DialogResult.OK)
                    {

                        //불량 등록
                        int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                        if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb, str_wc_code))
                        {
                            txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                        }
                    }
                    this.Cursor = Cursors.Default;
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }


        private void btn_pp_card_search_Click(object sender, EventArgs e)
        {
            try
            {
                if (!me_scode.Equals("40"))
                {


                    Swing2.TagListClear();
                    dsmListView_ivt2.Items.Clear();
                    ddc_ivt2.DigitText = "00000";
                    Tag_DT_PP.Rows.Clear();
                    Tag_DT_DC.Rows.Clear();
                    Swing2.InventoryStart();
                    //timer_pp_start.Start();
                    if (string.IsNullOrWhiteSpace(mo_snumb))
                    {
                        MessageBox.Show("작업지시를 선택해주세요");
                        return;
                    }
                    if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
                    {
                        MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                        return;
                    }
                    /*
                    if (txtOut_carrier_no.Enabled)
                    {
                        if (string.IsNullOrWhiteSpace(out_carrier_no))
                        {
                            MessageBox.Show("대차 정보를 읽지 못했습니다.");
                            return;
                        }
                    }*/
                    if (btn_on_off.Text.Trim().Equals("Off"))
                    {
                        PPCard_Search_New PPCard_Search_New = new PPCard_Search_New();
                        PPCard_Search_New.PP_dt = Tag_DT_PP;
                        PPCard_Search_New.PO_dt = po_DT;
                        PPCard_Search_New.wc_code = str_wc_code;
                        if (PPCard_Search_New.ShowDialog() == DialogResult.OK)
                        {

                            if (PPCard_Search_New.SIZE_str.Trim() == "0")
                            {
                                Sqty_popup Sqty_popup = new Sqty_popup();
                                Sqty_popup.sqty = "0";
                                if (Sqty_popup.ShowDialog() == DialogResult.OK)
                                {
                                    PP_SIZE_str = Sqty_popup.sqty;
                                }
                            }
                            else
                            {
                                PP_SIZE_str = PPCard_Search_New.SIZE_str.Trim();
                            }
                            PP_WC_CODE_str = PPCard_Search_New.WC_CODE_str;
                            PP_IT_SCODE_str = PPCard_Search_New.IT_SCODE_str;
                            CARD_NO_str = PPCard_Search_New.CARD_NO_str;

                            PP_SITE_CODE_str = PPCard_Search_New.SITE_CODE_str;

                            //가실적 테이블 저장
                            bool check = work_input_save_virtual(PP_SITE_CODE_str, mo_snumb, PP_IT_SCODE_str, PP_WC_CODE_str, PP_SIZE_str, txt_datetime.Text, CARD_NO_str, out_carrier_no, carrier_yn);
                            Swing2.TagListClear();
                            dsmListView_ivt2.Items.Clear();
                            ddc_ivt2.DigitText = "00000";
                            Tag_DT_PP.Rows.Clear();
                            Tag_DT_DC.Rows.Clear();
                            out_carrier_no = "";
                            txtOut_carrier_no.Text = "";
                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            txt_datetime.Text = r_start;
                            set_현재고_최대생산가능수량();
                            if (check)
                            {
                                if (me_scode == "40")
                                {
                                    // 식별표 출력
                                    //it_chart_print(PP_IT_SCODE_str, int.Parse(PP_SIZE_str));
                                    PpCard_Success10.TopLevel = true;
                                    PpCard_Success10.TopMost = true;
                                    PpCard_Success10.Visible = true;
                                    PpCard_Success10.set_text(CARD_NO_str.Trim(), 11);
                                }
                            }
                            else
                            {
                                PpCard_reading.TopLevel = false;
                                PpCard_reading.TopMost = false;
                                PpCard_reading.Visible = false;
                                PpCard_reading_chk = false;
                                Swing2.TagListClear();
                                dsmListView_ivt2.Items.Clear();
                                ddc_ivt2.DigitText = "00000";
                                Tag_DT_PP.Rows.Clear();
                                Tag_DT_DC.Rows.Clear();
                                out_carrier_no = "";
                                txtOut_carrier_no.Text = "";
                                r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                txt_datetime.Text = r_start;
                                set_현재고_최대생산가능수량();
                                MessageBox.Show("실적 등록 처리중 오류가 발생 하였습니다.\n다시 한번 등록 해주세요.....");
                            }
                        }
                    }
                    else if (btn_on_off.Text.Trim().Equals("On"))
                    {
                        /*if (txtOut_carrier_no.Text.Trim() == "")
                        {
                            MessageBox.Show("대차를 읽어주세요");
                            return;
                        }*/
                        if (btn_mix_toggle.Text.Trim() == "On")
                        {
                            //Mix_PP_Card_Input Mix_PP_Card_Input = new Mix_PP_Card_Input();
                            //Mix_PP_Card_Input.Dt = Tag_DT_PP_MIX;
                            //if (Mix_PP_Card_Input.ShowDialog() == DialogResult.OK)
                            //{
                            //    string mo_snumb_h = "";
                            //    foreach (DataRow drr in Mix_PP_Card_Input.Dt.Rows)
                            //    {
                            //        //생산계획번호 가져오기 drr["IT_SCODE"].ToString();
                            //        mo_snumb_h = CHECK_FUNC.Mix_get_work_plan_wp_snumb(drr["IT_SCODE"].ToString());
                            //        string pp_me_scode = CHECK_FUNC.Mix_get_work_plan_me_scode(drr["IT_SCODE"].ToString());
                            //        work_input_save_virtual(drr["SITE_CODE"].ToString(), mo_snumb_h, drr["IT_SCODE"].ToString(), str_wc_code.Trim()
                            //                                , drr["SIZE"].ToString(), txt_datetime.Text, drr["CARD_NO"].ToString(), out_carrier_no, "Y");
                            //        if (pp_me_scode == "40")
                            //        {
                            //            // 식별표 출력
                            //            //it_chart_print(PP_IT_SCODE_str, int.Parse(PP_SIZE_str));
                            //            PpCard_Success10.TopLevel = true;
                            //            PpCard_Success10.TopMost = true;
                            //            PpCard_Success10.Visible = true;
                            //            PpCard_Success10.set_text(CARD_NO_str.Trim(), 11);
                            //        }
                            //    }
                            //    Swing2.TagListClear();
                            //    dsmListView_ivt2.Items.Clear();
                            //    ddc_ivt2.DigitText = "00000";
                            //    Tag_DT_PP.Rows.Clear();
                            //    Tag_DT_DC.Rows.Clear();
                            //    out_carrier_no = "";
                            //    txtOut_carrier_no.Text = "";
                            //    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            //    txt_datetime.Text = r_start;
                            //    set_현재고_최대생산가능수량();


                            //}
                        }
                        else
                        {
                            Tag_DT_PP.Rows.Clear();
                            PpCard_reading.TopLevel = true;
                            PpCard_reading.TopMost = true;
                            PpCard_reading.Visible = true;
                            PpCard_reading_chk = true;
                        }

                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(mo_snumb))
                    {
                        if (btn_carrier_sqty.Text.Trim() != "0")
                        {
                            string pd_lot_no = work_input_save_virtual_NEW("D001", mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code.Trim()
                                                                , btn_carrier_sqty.Text.Trim(), txt_datetime.Text, "", out_carrier_no, "Y");
                            if (me_scode.Trim().Equals("40"))
                            {
                                // 식별표 출력
                                //it_chart_print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no);
                                if (luePrint_type.EditValue.ToString().Equals("A"))
                                {
                                    //모비스 식별표 출력
                                    Func_Mobis_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                                }
                                else if (luePrint_type.EditValue.ToString().Equals("B"))
                                {
                                    //기아 식별표 출력
                                    Func_Kia_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                                }
                                
                                PpCard_Success10.TopLevel = true;
                                PpCard_Success10.TopMost = true;
                                PpCard_Success10.Visible = true;
                                PpCard_Success10.set_text2(11);
                            }
                        }
                        else
                        {
                            MessageBox.Show("용기 수량이 0 입니다. 등록 할 수 없습니다.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("작업지시를 선택해주세요");
                    }
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        //완제품 실적 등록시 
        public string work_input_save_virtual_NEW(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string card_no, string carrier_no, string carrier_yn)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string lot_no = "";
            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            SqlDataReader reader = null;

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_ASSY_NEW", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            
            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY", float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY", 0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);

            //pp 시리얼번호
            cmd.Parameters.AddWithValue("@CARD_NO", card_no);

            //대차 번호
            cmd.Parameters.AddWithValue("@CARRIER_NO", "");

            cmd.Parameters.AddWithValue("@MM_RDATE",  DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@CARRIER_YN", carrier_yn);

            cmd.Parameters.AddWithValue("@WORKER", str_worker);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                reader = cmd.ExecuteReader();

                //양품수량 증가
                while (reader.Read())
                {
                    lot_no = reader["PD_LOT_NO"].ToString();
                }
                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                if (DateTime.Now.Hour > 18 || DateTime.Now.Hour < 8)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }
                if (reader!=null)
                    reader.Close(); 
                
                trans.Commit();
            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return lot_no;
        }

        bool PpCard_reading_chk = false;
        //식별표 출력
        

        //가실적 등록 
        public bool work_input_save_virtual(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string card_no, string carrier_no,string carrier_yn)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            
            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY", float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY", 0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);

            //pp 시리얼번호
            cmd.Parameters.AddWithValue("@CARD_NO", card_no);

            //대차 번호
            cmd.Parameters.AddWithValue("@CARRIER_NO", "");

            cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@CARRIER_YN", carrier_yn);

            cmd.Parameters.AddWithValue("@WORKER", str_worker);


            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                //양품수량 증가

                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));


                string hh = DateTime.Now.Hour.ToString();
                string mm = DateTime.Now.Minute.ToString();
                if (hh.Length == 1)
                {
                    hh = "0" + hh;
                }
                if (mm.Length == 1)
                {
                    mm = "0" + mm;
                }
                string time = hh + mm;


                if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }
                check = true;

            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public void work_input_save_virtual_transfer(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string card_no, string carrier_no, string carrier_yn, string transfer)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_NEW", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            SqlParameter paramSITE_CODE =
                    new SqlParameter("@SITE_CODE", SqlDbType.VarChar, 4);
            paramSITE_CODE.Value = site_code;
            cmd.Parameters.Add(paramSITE_CODE);
            //작업지시번호
            SqlParameter paramRSRV_NO =
                    new SqlParameter("@RSRV_NO", SqlDbType.VarChar, 8);
            paramRSRV_NO.Value = rsrv_no;
            cmd.Parameters.Add(paramRSRV_NO);

            //품목코드
            SqlParameter paramPRDT_ITEM =
                    new SqlParameter("@PRDT_ITEM", SqlDbType.VarChar, 15);
            paramPRDT_ITEM.Value = prdt_item;
            cmd.Parameters.Add(paramPRDT_ITEM);

            //작업장코드
            SqlParameter paramWC_CODE =
                    new SqlParameter("@WC_CODE", SqlDbType.VarChar, 8);
            paramWC_CODE.Value = wc_code;
            cmd.Parameters.Add(paramWC_CODE);

            //양품
            SqlParameter paramGOOD_QTY =
                    new SqlParameter("@GOOD_QTY", SqlDbType.Float, 8);
            paramGOOD_QTY.Value = float.Parse(good_qty);
            cmd.Parameters.Add(paramGOOD_QTY);

            //불량
            SqlParameter paramFAIL_QTY =
                    new SqlParameter("@FAIL_QTY", SqlDbType.Float, 8);
            paramFAIL_QTY.Value = 0;
            cmd.Parameters.Add(paramFAIL_QTY);

            //시작시간
            SqlParameter paramR_START =
                    new SqlParameter("@R_START", SqlDbType.VarChar, 20);
            paramR_START.Value = r_start;
            cmd.Parameters.Add(paramR_START);

            //pp 시리얼번호
            SqlParameter paramCARD_NO =
                    new SqlParameter("@CARD_NO", SqlDbType.VarChar, 3);
            paramCARD_NO.Value = card_no;
            cmd.Parameters.Add(paramCARD_NO);

            //대차 번호
            SqlParameter paramCARRIER_NO =
                    new SqlParameter("@CARRIER_NO", SqlDbType.VarChar, 10);
            paramCARRIER_NO.Value = "";
            cmd.Parameters.Add(paramCARRIER_NO);

            SqlParameter paramMM_RDATE =
                    new SqlParameter("@MM_RDATE", SqlDbType.VarChar, 30);
            paramMM_RDATE.Value = DateTime.Now.ToString();
            cmd.Parameters.Add(paramMM_RDATE);

            SqlParameter paramCARRIER_YN =
                    new SqlParameter("@CARRIER_YN", SqlDbType.Char, 1);
            paramCARRIER_YN.Value = carrier_yn;
            cmd.Parameters.Add(paramCARRIER_YN);

            SqlParameter paramTRANSFER =
                    new SqlParameter("@TRANSFER", SqlDbType.VarChar, 5);
            paramTRANSFER.Value = transfer;
            cmd.Parameters.Add(paramTRANSFER);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                //양품수량 증가

                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                string hh = DateTime.Now.Hour.ToString();
                string mm = DateTime.Now.Minute.ToString();
                if (hh.Length == 1)
                {
                    hh = "0" + hh;
                }
                if (mm.Length == 1)
                {
                    mm = "0" + mm;
                }
                string time = hh + mm;


                if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }

            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
        }

        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                regKey.SetValue("WC_CODE", str_wc_code);
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            
        }

        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_po_start_Tick(object sender, EventArgs e)
        {
            Swing2.InventoryStop();
            timer_po_start.Stop();
        }

        private void timer_pp_start_Tick(object sender, EventArgs e)
        {
            Swing2.InventoryStop();
            timer_pp_start.Stop();
        }

        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {
            try
            {
                string[] arryPort_power_out = null;
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.Trim()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }

                try
                {
                    arryPort_power_out = GET_DATA.get_connection_port(lueWc_code.GetColumnValue("WC_CODE").ToString(), "OUT");
                    //Swing2.ConnectionOpen(comboBox_ports2.SelectedItem.ToString());
                    Swing2.ConnectionOpen(arryPort_power_out[0], 25);

                    if (Swing2.IsOpen)
                    {

                    }
                    else
                    {
                        WinConsole.WriteLine("Failed to open {0}", Swing2.PortName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    WinConsole.WriteLine("Failed to open {0}", Swing2.PortName);
                    WinConsole.WriteLine(ex.Message);
                }

                comboBox_ports2.Enabled = !Swing2.IsOpen;
                button_com_open2.Enabled = !Swing2.IsOpen;
                button_com_close2.Enabled = Swing2.IsOpen;
                if (Swing2.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing2.PortName);
                    Properties.Settings.Default.ComPortName = Swing2.PortName;
                    Properties.Settings.Default.Save();
                    Swing2.InventoryStop();
                    Swing2.TagListClear();
                    dsmListView_ivt2.Items.Clear();
                    ddc_ivt2.DigitText = "00000";
                    txtOut_carrier_no.Text = "";
                    txtIt_scode.Text = "";

                    Swing2.SetRFPower(30 - int.Parse(arryPort_power_out[1]));
                    Swing2.ReportAllInformation();

                    work_plan_popup_paint();

                    //po_release_popup();
                }
                else
                {

                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            try
            {
                metarial_search metarial_search = new metarial_search();
                metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                if (metarial_search.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }

        private void txtOut_carrier_no_Click(object sender, EventArgs e)
        {
            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
            dsmListView_ivt2.Items.Clear();
            ddc_ivt2.DigitText = "00000";            
            txtOut_carrier_no.Text = "";
            out_carrier_no = "";
            txtIt_scode.Text = "";
            Tag_DT_DC.Clear();
            Tag_DT_PP.Clear();
            
                Swing2.InventoryStop();
                Swing2.TagListClear();
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
                Swing2.InventoryStart();
                timer_pp_start.Start();
            
        }

        private void btn_carrier_sqty_Click(object sender, EventArgs e)
        {
            try
            {
                KeyPad KeyPad = new KeyPad();
                if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    btn_carrier_sqty.Text = KeyPad.txt_value;
                }/*
            if (btn_carrier_sqty.Text.Trim().Equals("32"))
            {
                btn_carrier_sqty.Text = "40";
            }
            else
            {
                btn_carrier_sqty.Text = "32";
            }*/
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }


        //작업유형 가져오기
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code = shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            //Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_group = wc_group;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;

                //작업 화면에 표시 해주기 위한 변수
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }

                //DB에 저장 하기 위한 변수
                str_worker = "";
                DataRow[] drr = select_worker_dt.Select();
                for (int i = 0; i < drr.Length; i++)
                {
                    if (i == drr.Length - 1)
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString();
                    }
                    else
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString() + ",";
                    }
                }

            }
        }

        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            try
            {
                set_worker_info();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_on_off_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_on_off.Text.Trim().Equals("On"))
                {
                    btn_on_off.Text = "Off";
                }
                else
                {
                    btn_on_off.Text = "On";
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
        public string transfer = "NO";
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                MessageBox.Show("생산 계획을 선택해 주세요");
                return;
            }
            Injection_Carrier_KeyPad Injection_Carrier_KeyPad = new Injection_Carrier_KeyPad();
            Injection_Carrier_KeyPad.it_scode = txt_IT_SCODE.Text.Trim();
            Injection_Carrier_KeyPad.wc_group = wc_group;
            if (Injection_Carrier_KeyPad.ShowDialog() == DialogResult.OK)
            {
                transfer = Injection_Carrier_KeyPad.transfer;
                out_carrier_no = Injection_Carrier_KeyPad.txt_value;
                txtOut_carrier_no.Text = out_carrier_no;
                Swing2.InventoryStop();
                Swing2.TagListClear();
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
            }
        }

        private void btn_fail_search_Click(object sender, EventArgs e)
        {
            try
            {
                fail_search_popup fail_search_popup = new fail_search_popup();
                fail_search_popup.wc_group = wc_group;
                fail_search_popup.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
                fail_search_popup.wc_name = lueWc_code.GetColumnValue("WC_NAME").ToString();
                fail_search_popup.Show();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_day_work_Click(object sender, EventArgs e)
        {
            try
            {
                Day_Night_Sqty_Popup Day_Night_Sqty_Popup = new Day_Night_Sqty_Popup();
                Day_Night_Sqty_Popup.wc_code = str_wc_code;
                Day_Night_Sqty_Popup.Show();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_mix_toggle_Click(object sender, EventArgs e)
        {
            if(btn_mix_toggle.Text.Trim()=="Off")
            {
                btn_mix_toggle.Text = "On";
            }
            else
            {
                btn_mix_toggle.Text = "Off";
            }
        }

        private void time_now_Tick(object sender, EventArgs e)
        {
            imageSlider1.SlideNext();
        }

        private void btn_po_release_Insert_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                try
                {
                    if (true)
                    {

                        Po_release_Insert_dev Po_release_Insert_dev = new Po_release_Insert_dev();
                        Po_release_Insert_dev.wc_group = wc_group;
                        Po_release_Insert_dev.wc_code = str_wc_code;
                        if (Po_release_Insert_dev.ShowDialog() == DialogResult.OK)
                        {
                            formClear2();
                            txt_IT_SCODE.Text = Po_release_Insert_dev.IT_SCODE_str;
                            //작업표준서
                            worker.RunWorkerAsync();
                            txt_IT_SNAME.Text = Po_release_Insert_dev.IT_SNAME_str;
                            txt_IT_MODEL.Text = Po_release_Insert_dev.IT_MODEL_str;
                            txt_MO_SQUTY.Text = Po_release_Insert_dev.MO_SQUTY_str;
                            me_scode = Po_release_Insert_dev.ME_SCODE_str;
                            //str_wc_code = Work_plan_search.WC_CODE_str;
                            mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                            PP_SITE_CODE_str = Po_release_Insert_dev.SITE_CODE_str;
                            me_scode = Po_release_Insert_dev.ME_SCODE_str;
                            //max_sqty = Po_release_Insert_dev.MAX_SQTY_str;
                            //txt_MO_SQUTY.Text = max_sqty;
                            //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            txt_datetime.Text = r_start;
                            carrier_yn = "Y";
                            mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                            btn_carrier_sqty.Text = Po_release_Insert_dev.IT_PKQTY_str;
                            /*
                            DataRow[] drr = po_DT.Select("PO_IT_SCODE='" + txt_IT_SCODE.Text.Trim() + "'");
                            for (int i = 0; i < drr.Length; i++)
                                po_DT.Rows.Remove(drr[i]);
                            po_DT.AcceptChanges();

                            DataRow[] po_row = po_DT.Select("MO_SNUMB='" + mo_snumb + "'");

                            if (po_row.Length == 0)
                            {
                                DataRow dr;
                                dr = po_DT.NewRow();
                                dr["MO_SNUMB"] = mo_snumb;
                                dr["PO_IT_SCODE"] = txt_IT_SCODE.Text;
                                dr["PO_IT_SNAME"] = txt_IT_SNAME.Text;
                                dr["PO_ME_SCODE"] = me_scode;

                                po_DT.Rows.Add(dr);
                            }*/
                            //양품수량 불량수량 들고오기
                            GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                            txt_good_qty.Text = GET_DATA.good_qty;
                            txt_fail_qty.Text = GET_DATA.fail_qty;
                            Swing2.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                            //Swing2.InventoryStart();
                            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            txtIt_scode.Text = "";
                            set_현재고_최대생산가능수량();
                            shiftwork();//작업유형 가져오기
                            set_worker_info();//작업자 팝업
                            sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                            txt_day_sqty.Text = sqty_arry[0];
                            txt_night_sqty.Text = sqty_arry[1];
                            txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                            timer_po_start.Start();
                            lueWc_code.Enabled = false;
                            SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");

                            string print_type = Settings_Xml.read(Po_release_Insert_dev.IT_SCODE_str.Trim(), luePrint_type.EditValue.ToString().Trim());
                            if (string.IsNullOrWhiteSpace(print_type))
                            {
                                Settings_Xml.reWrite(Po_release_Insert_dev.IT_SCODE_str.Trim(), luePrint_type.EditValue.ToString().Trim());
                                print_type = luePrint_type.EditValue.ToString().Trim();
                            }

                            luePrint_type.EditValue = print_type;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("다시 시도해주세요 : " + ex.Message);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
           
            SqlCommand cmd =
                    new SqlCommand("SP_HD_IT_CHART_INSERT_TABLET", conn);
        
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", txt_IT_SCODE.Text.Trim());

            cmd.Parameters.AddWithValue("@SQUTY", 5);


            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_HD_QR_PRINT_NEW", conn);

                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SNUMB");
                    DT.Columns.Add("IT_SCODE");
                    DT.Columns.Add("IT_SNAME");
                    DT.Columns.Add("KO_IT_SNAME");
                    DT.Columns.Add("IC_SQTY");
                    DT.Columns.Add("IC_PDATE");
                    DT.Columns.Add("CAR_CODE");
                    DT.Columns.Add("AREA");
                    DT.Columns.Add("SITE_CODE");
                    DT.Columns.Add("SHIPP_CODE");
                    DT.Columns.Add("PKQTY");
                    DT.Columns.Add("PROCESS_NO");
                    DT.Columns.Add("HPC");
                    DT.Columns.Add("LH_RH");
                    DT.Columns.Add("QR_CODE");
                    DT.Columns.Add("IC_SEQNO");
                    DT.Columns.Add("PDATE_CODE");
                    DT.Columns.Add("SPEC");
                    
                    
                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();
                        dr["IC_SNUMB"] = reader1["IC_SNUMB"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["KO_IT_SNAME"] = reader1["KO_IT_SNAME"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["IC_PDATE"] = reader1["IC_PDATE"].ToString();
                        dr["CAR_CODE"] = reader1["CAR_CODE"].ToString();
                        dr["AREA"] = reader1["AREA"].ToString();
                        dr["SITE_CODE"] = reader1["SITE_CODE"].ToString();
                        dr["SHIPP_CODE"] = reader1["SHIPP_CODE"].ToString();
                        dr["PKQTY"] = reader1["PKQTY"].ToString();
                        dr["PROCESS_NO"] = reader1["PROCESS_NO"].ToString();
                        dr["HPC"] = reader1["HPC"].ToString();
                        dr["LH_RH"] = reader1["LH_RH"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["PDATE_CODE"] = reader1["PDATE_CODE"].ToString();
                        dr["SPEC"] = reader1["SPEC"].ToString();
                    

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("HD_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();
                    Mobis_Print Mobis_Print = new Mobis_Print();
                    Mobis_Print.DataSource = ds;

                    Mobis_Print.ShowPrintMarginsWarning = false;
                    Mobis_Print.ShowPrintStatusDialog = false;

                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Mobis_Print);
                    ReportPrintTool.Print();
                    ReportPrintTool.Print();

                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
            
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Kia_Print Kia_Print = new Kia_Print();
            //Alc_code_Print.DataSource = ds;
            //Alc_code_Print.PrinterName = "PRINT_A";
            Kia_Print.ShowPrintMarginsWarning = false;
            Kia_Print.ShowPrintStatusDialog = false;

            ReportPrintTool ReportPrintTool = new ReportPrintTool(Kia_Print);
            ReportPrintTool.Print();
            ReportPrintTool.Print();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SP_HD_IT_CHART_INSERT_TABLET", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", txt_IT_SCODE.Text.Trim());

            cmd.Parameters.AddWithValue("@SQUTY", 5);


            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_HD_QR_PRINT_NEW", conn);

                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SNUMB");
                    DT.Columns.Add("IT_SCODE");
                    DT.Columns.Add("IT_SNAME");
                    DT.Columns.Add("KO_IT_SNAME");
                    DT.Columns.Add("IC_SQTY");
                    DT.Columns.Add("IC_PDATE");
                    DT.Columns.Add("CAR_CODE");
                    DT.Columns.Add("AREA");
                    DT.Columns.Add("SITE_CODE");
                    DT.Columns.Add("SHIPP_CODE");
                    DT.Columns.Add("PKQTY");
                    DT.Columns.Add("PROCESS_NO");
                    DT.Columns.Add("HPC");
                    DT.Columns.Add("LH_RH");
                    DT.Columns.Add("QR_CODE");
                    DT.Columns.Add("IC_SEQNO");
                    DT.Columns.Add("PDATE_CODE");
                    DT.Columns.Add("SPEC");


                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();
                        dr["IC_SNUMB"] = reader1["IC_SNUMB"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["KO_IT_SNAME"] = reader1["KO_IT_SNAME"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["IC_PDATE"] = reader1["IC_PDATE"].ToString();
                        dr["CAR_CODE"] = reader1["CAR_CODE"].ToString();
                        dr["AREA"] = reader1["AREA"].ToString();
                        dr["SITE_CODE"] = reader1["SITE_CODE"].ToString();
                        dr["SHIPP_CODE"] = reader1["SHIPP_CODE"].ToString();
                        dr["PKQTY"] = reader1["PKQTY"].ToString();
                        dr["PROCESS_NO"] = reader1["PROCESS_NO"].ToString();
                        dr["HPC"] = reader1["HPC"].ToString();
                        dr["LH_RH"] = reader1["LH_RH"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["PDATE_CODE"] = reader1["PDATE_CODE"].ToString();
                        dr["SPEC"] = reader1["SPEC"].ToString();


                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("HD_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();
                    
                    Hyundae_Print Hyundae_Print = new Hyundae_Print();
                    Hyundae_Print.DataSource = ds;
                    //Alc_code_Print.PrinterName = "PRINT_A";
                    Hyundae_Print.ShowPrintMarginsWarning = false;
                    Hyundae_Print.ShowPrintStatusDialog = false;

                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Hyundae_Print);
                    ReportPrintTool.Print();
                    ReportPrintTool.Print();
                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
            
        }

        private void btn_re_print_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    MessageBox.Show("작업지시를 선택해 주세요");
                    return;
                }
                if (luePrint_type.EditValue.ToString().Equals("A"))
                {
                    Func_Mobis_Print.re_Print(txt_IT_SCODE.Text.Trim(), 2);
                }
                else if (luePrint_type.EditValue.ToString().Equals("B"))
                {
                    Func_Kia_Print.re_Print(txt_IT_SCODE.Text.Trim(), 2);
                }
                
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {

                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("작업지시를 선택해 주세요");
                    return;
                }
                PpCard_reading_chk = true;
                try
                {
                    DataRow dr = saveDataRow_PP2(Utils.HexToASCII(textEdit1.Text.Trim()));
                    //DataRow dr = saveDataRow_PP2(textEdit1.Text.Trim());
                    
                    if (dr != null)
                    {
                       
                            Tag_DT_PP.Rows.Add(dr);
                            Tag_DT_PP.DefaultView.Sort = "CARD_NO ASC";
                       
                    }
                }
                catch
                {

                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
            }

        }
        public bool pp_card_reading(string text)
        {

            DataRow dr = saveDataRow_PP2(text);

            if (dr != null)
            {
                if (btn_mix_toggle.Text.Trim() == "Off")
                {
                    Tag_DT_PP.Rows.Add(dr);
                    Tag_DT_PP.DefaultView.Sort = "CARD_NO ASC";
                }
                else if (btn_mix_toggle.Text.Trim() == "On")
                {
                    DataRow[] drr = Tag_DT_PP_MIX.Select("IT_SCODE='" + dr["IT_SCODE"].ToString() + "'");

                    if (drr.Length == 0)
                    {
                        //if (작업장에서 생산계획이 있는품목인지 체크)
                        if (CHECK_FUNC.Mix_work_plan_check_item(dr["IT_SCODE"].ToString()))
                        {
                            Tag_DT_PP_MIX.Rows.Add(dr);
                        }

                    }
                }

            }
            return check_pp_message;

        }

        private void luePrint_type_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    return;
                }
                else
                {
                    Settings_Xml.reWrite(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(str_wc_code.ToString().Trim()))
            {
                MessageBox.Show("작업장을 선택 해 주새요");
                return;
            }
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(parentForm);
            FME_InsResultReg.wc_code = str_wc_code.ToString().Trim();
            FME_InsResultReg.Show();
        }

        private void btn_program_finish_Click(object sender, EventArgs e)
        {
            Finish_Popup Finish_Popup = new Finish_Popup();
            if (Finish_Popup.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Finish_Popup.btn_state.Equals("0"))
                {

                }
                else if (Finish_Popup.btn_state.Equals("1"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    Application.Exit();
                }
                else if (Finish_Popup.btn_state.Equals("2"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");

                }
            }
        }
    }
}
