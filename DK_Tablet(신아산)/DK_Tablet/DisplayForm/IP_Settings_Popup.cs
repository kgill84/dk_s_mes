using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;

namespace DK_Tablet
{
    public partial class IP_Settings_Popup : Form
    {
        public IP_Settings_Popup()
        {
            InitializeComponent();

           
            txtip_1.Text = IP_Settings.ip.ip1;
            txtip_2.Text = IP_Settings.ip.ip2;
            txtip_3.Text = IP_Settings.ip.ip3;
            txtip_4.Text = IP_Settings.ip.ip4;
            txtip_5.Text = IP_Settings.ip.ip5;
            txtip_6.Text = IP_Settings.ip.ip6;
            txtip_7.Text = IP_Settings.ip.ip7;
		}

		// OK
		private void button1_Click(object sender, EventArgs e)
		{
            IP_Settings.ip.ip1 = txtip_1.Text.Trim();
            IP_Settings.ip.ip2 = txtip_2.Text.Trim();
            IP_Settings.ip.ip3 = txtip_3.Text.Trim();
            IP_Settings.ip.ip4 = txtip_4.Text.Trim();
            IP_Settings.ip.ip5 = txtip_5.Text.Trim();
            IP_Settings.ip.ip6 = txtip_6.Text.Trim();
            IP_Settings.ip.ip7 = txtip_7.Text.Trim();

            IP_Settings.ip.port1 = txtport_1.Text.Trim();
            IP_Settings.ip.port2 = txtport_2.Text.Trim();
            IP_Settings.ip.port3 = txtport_3.Text.Trim();
            IP_Settings.ip.port4 = txtport_4.Text.Trim();
            IP_Settings.ip.port5 = txtport_5.Text.Trim();
            IP_Settings.ip.port6 = txtport_6.Text.Trim();
            IP_Settings.ip.port7 = txtport_7.Text.Trim();


            IP_Settings.Write();

			Close();
		}

		// Cancel
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}