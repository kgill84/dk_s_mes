﻿namespace DK_Tablet
{
    partial class Fail_input_qr
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Fail_input_qr));
            this.panel1 = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SO_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FAIL_QTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TextEdit_FAIL_QTY = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.FAIL_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INSP_OK_QTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FAIL_DESCR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TextEdit_FAIL_DESCR = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.TR_SERNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INSPECTION_YN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit_FAIL_TYPE = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.btn_move = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.button_com_open = new System.Windows.Forms.Button();
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.입고검수 = new System.Windows.Forms.Button();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnRowAdd = new System.Windows.Forms.Button();
            this.btnRowDel = new System.Windows.Forms.Button();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.lbl_PC_SCODE = new System.Windows.Forms.Label();
            this.btn_worker_info = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_FAIL_QTY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_FAIL_DESCR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_FAIL_TYPE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_worker_info.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.gridControl1);
            this.panel1.Location = new System.Drawing.Point(12, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 545);
            this.panel1.TabIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.TextEdit_FAIL_QTY,
            this.TextEdit_FAIL_DESCR,
            this.LookUpEdit_FAIL_TYPE});
            this.gridControl1.Size = new System.Drawing.Size(1261, 537);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IT_SCODE,
            this.IT_SNAME,
            this.SO_SQUTY,
            this.FAIL_QTY,
            this.FAIL_TYPE,
            this.INSP_OK_QTY,
            this.FAIL_DESCR,
            this.TR_SERNO,
            this.INSPECTION_YN,
            this.gridColumn1});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.AllowCellMerge = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 40;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            this.gridView1.CellMerge += new DevExpress.XtraGrid.Views.Grid.CellMergeEventHandler(this.gridView1_CellMerge);
            this.gridView1.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridView1_RowCellStyle);
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품번";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.IT_SCODE.OptionsColumn.ReadOnly = true;
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 0;
            this.IT_SCODE.Width = 180;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.OptionsColumn.AllowEdit = false;
            this.IT_SNAME.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.IT_SNAME.OptionsColumn.ReadOnly = true;
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 1;
            this.IT_SNAME.Width = 280;
            // 
            // SO_SQUTY
            // 
            this.SO_SQUTY.AppearanceCell.Options.UseTextOptions = true;
            this.SO_SQUTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SO_SQUTY.Caption = "발주수량";
            this.SO_SQUTY.DisplayFormat.FormatString = "{0:#,##0}";
            this.SO_SQUTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SO_SQUTY.FieldName = "SO_SQUTY";
            this.SO_SQUTY.Name = "SO_SQUTY";
            this.SO_SQUTY.OptionsColumn.AllowEdit = false;
            this.SO_SQUTY.OptionsColumn.ReadOnly = true;
            this.SO_SQUTY.Visible = true;
            this.SO_SQUTY.VisibleIndex = 2;
            this.SO_SQUTY.Width = 120;
            // 
            // FAIL_QTY
            // 
            this.FAIL_QTY.AppearanceCell.Options.UseTextOptions = true;
            this.FAIL_QTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FAIL_QTY.Caption = "불량수량";
            this.FAIL_QTY.ColumnEdit = this.TextEdit_FAIL_QTY;
            this.FAIL_QTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FAIL_QTY.FieldName = "FAIL_QTY";
            this.FAIL_QTY.Name = "FAIL_QTY";
            this.FAIL_QTY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.FAIL_QTY.OptionsColumn.ReadOnly = true;
            this.FAIL_QTY.Visible = true;
            this.FAIL_QTY.VisibleIndex = 3;
            this.FAIL_QTY.Width = 120;
            // 
            // TextEdit_FAIL_QTY
            // 
            this.TextEdit_FAIL_QTY.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_FAIL_QTY.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.TextEdit_FAIL_QTY.Appearance.Options.UseFont = true;
            this.TextEdit_FAIL_QTY.AutoHeight = false;
            this.TextEdit_FAIL_QTY.Name = "TextEdit_FAIL_QTY";
            this.TextEdit_FAIL_QTY.NullText = "0";
            this.TextEdit_FAIL_QTY.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextEdit_FAIL_QTY_MouseDown);
            // 
            // FAIL_TYPE
            // 
            this.FAIL_TYPE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.FAIL_TYPE.AppearanceCell.Options.UseFont = true;
            this.FAIL_TYPE.Caption = "불량유형";
            this.FAIL_TYPE.FieldName = "FAIL_TYPE";
            this.FAIL_TYPE.Name = "FAIL_TYPE";
            this.FAIL_TYPE.OptionsColumn.AllowEdit = false;
            this.FAIL_TYPE.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.FAIL_TYPE.OptionsColumn.ReadOnly = true;
            this.FAIL_TYPE.Visible = true;
            this.FAIL_TYPE.VisibleIndex = 4;
            this.FAIL_TYPE.Width = 160;
            // 
            // INSP_OK_QTY
            // 
            this.INSP_OK_QTY.AppearanceCell.Options.UseTextOptions = true;
            this.INSP_OK_QTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.INSP_OK_QTY.Caption = "입고수량";
            this.INSP_OK_QTY.DisplayFormat.FormatString = "{0:#,##0}";
            this.INSP_OK_QTY.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.INSP_OK_QTY.FieldName = "INSP_OK_QTY";
            this.INSP_OK_QTY.Name = "INSP_OK_QTY";
            this.INSP_OK_QTY.OptionsColumn.AllowEdit = false;
            this.INSP_OK_QTY.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.INSP_OK_QTY.OptionsColumn.ReadOnly = true;
            this.INSP_OK_QTY.Visible = true;
            this.INSP_OK_QTY.VisibleIndex = 5;
            this.INSP_OK_QTY.Width = 120;
            // 
            // FAIL_DESCR
            // 
            this.FAIL_DESCR.Caption = "비고";
            this.FAIL_DESCR.ColumnEdit = this.TextEdit_FAIL_DESCR;
            this.FAIL_DESCR.FieldName = "FAIL_DESCR";
            this.FAIL_DESCR.Name = "FAIL_DESCR";
            this.FAIL_DESCR.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.FAIL_DESCR.Visible = true;
            this.FAIL_DESCR.VisibleIndex = 6;
            this.FAIL_DESCR.Width = 200;
            // 
            // TextEdit_FAIL_DESCR
            // 
            this.TextEdit_FAIL_DESCR.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.TextEdit_FAIL_DESCR.Appearance.Options.UseFont = true;
            this.TextEdit_FAIL_DESCR.AutoHeight = false;
            this.TextEdit_FAIL_DESCR.Name = "TextEdit_FAIL_DESCR";
            // 
            // TR_SERNO
            // 
            this.TR_SERNO.Caption = "TR_SERNO";
            this.TR_SERNO.FieldName = "TR_SERNO";
            this.TR_SERNO.Name = "TR_SERNO";
            this.TR_SERNO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            // 
            // INSPECTION_YN
            // 
            this.INSPECTION_YN.Caption = "INSPECTION_YN";
            this.INSPECTION_YN.FieldName = "INSPECTION_YN";
            this.INSPECTION_YN.Name = "INSPECTION_YN";
            this.INSPECTION_YN.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 7;
            // 
            // LookUpEdit_FAIL_TYPE
            // 
            this.LookUpEdit_FAIL_TYPE.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.LookUpEdit_FAIL_TYPE.Appearance.Options.UseFont = true;
            this.LookUpEdit_FAIL_TYPE.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.LookUpEdit_FAIL_TYPE.AppearanceDropDown.Options.UseFont = true;
            this.LookUpEdit_FAIL_TYPE.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.LookUpEdit_FAIL_TYPE.AppearanceDropDownHeader.Options.UseFont = true;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 15F);
            serializableAppearanceObject1.Options.UseFont = true;
            this.LookUpEdit_FAIL_TYPE.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.LookUpEdit_FAIL_TYPE.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("NUMB", "번호", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FT_NAME", 80, "불량유형")});
            this.LookUpEdit_FAIL_TYPE.Name = "LookUpEdit_FAIL_TYPE";
            this.LookUpEdit_FAIL_TYPE.NullText = "";
            // 
            // btn_move
            // 
            this.btn_move.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_move.BackgroundImage")));
            this.btn_move.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_move.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_move.Location = new System.Drawing.Point(879, 663);
            this.btn_move.Name = "btn_move";
            this.btn_move.Size = new System.Drawing.Size(198, 85);
            this.btn_move.TabIndex = 15;
            this.btn_move.Text = "완료";
            this.btn_move.UseVisualStyleBackColor = true;
            this.btn_move.Click += new System.EventHandler(this.btn_move_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1085, 663);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 85);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(174, 24);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(201, 20);
            this.textEdit1.TabIndex = 37;
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(381, 23);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 38;
            this.simpleButton1.Text = "qr";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(529, 61);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 43;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(448, 61);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 41;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(367, 61);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 42;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(104, 61);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(257, 40);
            this.comboBox_ports.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 40);
            this.label2.TabIndex = 39;
            this.label2.Text = "리더기";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(607, 61);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 45);
            this.label3.TabIndex = 44;
            this.label3.Text = "검사자";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(932, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 45);
            this.label1.TabIndex = 46;
            this.label1.Text = "거래처";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1280, 5);
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            // 
            // 입고검수
            // 
            this.입고검수.FlatAppearance.BorderSize = 0;
            this.입고검수.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.입고검수.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.입고검수.Image = ((System.Drawing.Image)(resources.GetObject("입고검수.Image")));
            this.입고검수.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.입고검수.Location = new System.Drawing.Point(8, 12);
            this.입고검수.Name = "입고검수";
            this.입고검수.Size = new System.Drawing.Size(289, 35);
            this.입고검수.TabIndex = 54;
            this.입고검수.TabStop = false;
            this.입고검수.Text = "수입검사";
            this.입고검수.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.입고검수.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.입고검수.UseVisualStyleBackColor = true;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(462, 23);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 38;
            this.simpleButton2.Text = "버튼";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnRowAdd
            // 
            this.btnRowAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRowAdd.BackgroundImage")));
            this.btnRowAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRowAdd.Font = new System.Drawing.Font("굴림", 22F);
            this.btnRowAdd.Location = new System.Drawing.Point(12, 663);
            this.btnRowAdd.Name = "btnRowAdd";
            this.btnRowAdd.Size = new System.Drawing.Size(211, 85);
            this.btnRowAdd.TabIndex = 15;
            this.btnRowAdd.Text = "불량유형 추가";
            this.btnRowAdd.UseVisualStyleBackColor = true;
            this.btnRowAdd.Visible = false;
            this.btnRowAdd.Click += new System.EventHandler(this.btnRowAdd_Click);
            // 
            // btnRowDel
            // 
            this.btnRowDel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRowDel.BackgroundImage")));
            this.btnRowDel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRowDel.Font = new System.Drawing.Font("굴림", 22F);
            this.btnRowDel.Location = new System.Drawing.Point(229, 663);
            this.btnRowDel.Name = "btnRowDel";
            this.btnRowDel.Size = new System.Drawing.Size(198, 85);
            this.btnRowDel.TabIndex = 15;
            this.btnRowDel.Text = "행삭제";
            this.btnRowDel.UseVisualStyleBackColor = true;
            this.btnRowDel.Visible = false;
            this.btnRowDel.Click += new System.EventHandler(this.btnRowDel_Click);
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(699, 12);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 56;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader48,
            this.columnHeader49,
            this.columnHeader50,
            this.columnHeader51,
            this.columnHeader52,
            this.columnHeader53,
            this.columnHeader54});
            this.dsmListView_ivt.Location = new System.Drawing.Point(940, 1);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(42, 60);
            this.dsmListView_ivt.TabIndex = 57;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(543, 12);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 58;
            this.ddc_ivt.Visible = false;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(826, 12);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 59;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // lbl_PC_SCODE
            // 
            this.lbl_PC_SCODE.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbl_PC_SCODE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_PC_SCODE.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_PC_SCODE.Location = new System.Drawing.Point(1026, 60);
            this.lbl_PC_SCODE.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_PC_SCODE.Name = "lbl_PC_SCODE";
            this.lbl_PC_SCODE.Size = new System.Drawing.Size(255, 45);
            this.lbl_PC_SCODE.TabIndex = 46;
            this.lbl_PC_SCODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_worker_info
            // 
            this.btn_worker_info.EditValue = "";
            this.btn_worker_info.Location = new System.Drawing.Point(699, 62);
            this.btn_worker_info.Name = "btn_worker_info";
            this.btn_worker_info.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.Appearance.Options.UseFont = true;
            this.btn_worker_info.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.AppearanceDisabled.Options.UseFont = true;
            this.btn_worker_info.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.AppearanceDropDown.Options.UseFont = true;
            this.btn_worker_info.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.btn_worker_info.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.AppearanceFocused.Options.UseFont = true;
            this.btn_worker_info.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_worker_info.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.btn_worker_info.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.btn_worker_info.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("S01", "Name5", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("S02", "검사자")});
            this.btn_worker_info.Size = new System.Drawing.Size(230, 42);
            this.btn_worker_info.TabIndex = 60;
            // 
            // Fail_input_qr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.btn_worker_info);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.입고검수);
            this.Controls.Add(this.lbl_PC_SCODE);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnRowDel);
            this.Controls.Add(this.btnRowAdd);
            this.Controls.Add(this.btn_move);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Fail_input_qr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "수입검사";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_FAIL_QTY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_FAIL_DESCR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_FAIL_TYPE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_worker_info.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Button btn_move;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Button button_inventory_start;
        private System.Windows.Forms.Button button_com_close;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button 입고검수;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn SO_SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn FAIL_QTY;
        private DevExpress.XtraGrid.Columns.GridColumn FAIL_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn INSP_OK_QTY;
        private DevExpress.XtraGrid.Columns.GridColumn FAIL_DESCR;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit_FAIL_QTY;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit TextEdit_FAIL_DESCR;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookUpEdit_FAIL_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn TR_SERNO;
        private System.Windows.Forms.Button btnRowAdd;
        private System.Windows.Forms.Button btnRowDel;
        private DevExpress.XtraGrid.Columns.GridColumn INSPECTION_YN;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private dsmListView listView_target_list;
        private System.Windows.Forms.Label lbl_PC_SCODE;
        private DevExpress.XtraEditors.LookUpEdit btn_worker_info;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}

