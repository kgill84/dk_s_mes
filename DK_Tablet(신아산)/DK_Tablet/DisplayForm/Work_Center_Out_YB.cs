﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;
using DK_Tablet.PRINT;
using DevExpress.XtraReports.UI;


namespace DK_Tablet
{
    public partial class Work_Center_Out_YB : Form
    {
        
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        Func_Mobis_Print Func_Mobis_Print = new Func_Mobis_Print();
        
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        
        string carrier_yn = "";//공대차인지 아닌지 구분자
        
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        string sw_code = "";//근무형태 코드
        string str_worker = "";//작업자
        //출구 리더기
        
        string out_carrier_no = "";// 출구로 나가는 대차 번호
        
        DataTable Tag_DT_DC = new DataTable();//출구에서 리더기로 읽어 들인 대차 카드 보관
        
        
        PpCard_Success10 PpCard_Success10 = new PpCard_Success10();
        string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //혼적 datatable
        

        string PP_WC_CODE_str = "";//작업장
        string PP_IT_SCODE_str = "";//품목코드
        string CARD_NO_str = "";//카드번호
        string PP_SIZE_str = "";//수용수
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";

        //작업표준서 
        private BackgroundWorker worker = new BackgroundWorker();
        FtpUtil ftpUtil;
        string path = "ftp://203.251.168.131:4104";
        string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        Image[] work_image = new Image[10];

        //각각 설비 ready,
        
        
        public Work_Center_Out_YB(MAIN form)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();

            
            
        }
        
        Dictionary<string, string> EQUIP = new Dictionary<string,string>();
        
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                EQUIP.Add("A1", "OFF");
                EQUIP.Add("B1", "OFF");
                EQUIP.Add("C1", "OFF");
                EQUIP.Add("E1", "OFF");
                EQUIP.Add("E2", "OFF");
                EQUIP.Add("C2", "OFF");
                EQUIP.Add("F1", "OFF");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            



            try
            {
                sqty_arry[0] = "0";
                sqty_arry[1] = "0";
                
                
                PpCard_Success10.Show();
                PpCard_Success10.Visible = false;
                
                    lab_carrier_sqty.Visible = true;
                    btn_carrier_sqty.Visible = true;
                
                GET_DATA.get_work_time_master();
                night_time_start = GET_DATA.night_time_start;
                day_time_start = GET_DATA.day_time_start;
                
               
                
                GET_DATA.get_timer_master(wc_group);
                

                lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";


                luePrint_type.Properties.DataSource = GET_DATA.Print_type_DropDown();
                luePrint_type.Properties.DisplayMember = "TYPE_NAME";
                luePrint_type.Properties.ValueMember = "TYPE_CODE";
                luePrint_type.EditValue = "A";
                //lookUpEdit1.SelectionStart = 0;
                //lueWc_code.ItemIndex = 0;
                try
                {
                    if (regKey.GetValue("WC_CODE") == null)
                    {
                        regKey.SetValue("WC_CODE", "");
                    }
                    else
                    {
                        if (regKey.GetValue("WC_CODE").ToString() != "")
                        {

                            lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR : "+ex.Message);
                }

                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                //작업표준서 가져오기
                timer_now.Stop();
                imageSlider1.Images.Clear();
                string ftp_str = AdjustDir(txt_IT_SCODE.Text.Trim());

                ftpUtil.FTPDirectioryCheck(ftp_str,"OUT");

                work_image = ftpUtil.get_file_list(ftp_str,"OUT");

                for (int i = 0; i < work_image.Length; i++)
                {
                    if (work_image[i] != null)
                        imageSlider1.Images.Add(work_image[i]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                timer_now.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            parentForm.Visible = true;
        }

        bool check_pp_message = false;
        
        
        private void btn_po_release_Click(object sender, EventArgs e)
        {
            try
            {
                work_plan_popup_paint();
                //po_release_popup();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        string[] sqty_arry = new string[2];

        public void work_plan_popup_paint()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {

                Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                Work_plan_search.wc_group = wc_group;
                Work_plan_search.wc_code = str_wc_code;
                if (Work_plan_search.ShowDialog() == DialogResult.OK)
                {
                    formClear2();
                    txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                    //작업표준서
                    worker.RunWorkerAsync();
                    txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                    txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                    txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                    me_scode = Work_plan_search.ME_SCODE_str;
                    //str_wc_code = Work_plan_search.WC_CODE_str;
                    mo_snumb = Work_plan_search.MO_SNUMB_str;
                    PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                    me_scode = Work_plan_search.ME_SCODE_str;
                    //max_sqty = Work_plan_search.MAX_SQTY_str;
                    //txt_MO_SQUTY.Text = max_sqty;
                    //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                    txt_datetime.Text = r_start;
                    carrier_yn = "Y";
                    mo_snumb = Work_plan_search.MO_SNUMB_str;
                    btn_carrier_sqty.Text = Work_plan_search.IT_PKQTY_str;

                    //양품수량 불량수량 들고오기
                    GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                    txt_good_qty.Text = GET_DATA.good_qty;
                    txt_fail_qty.Text = GET_DATA.fail_qty;

                    //Swing2.InventoryStart();
                    
                    txtIt_scode.Text = "";
                    set_현재고_최대생산가능수량();
                    shiftwork();//작업유형 가져오기
                    set_worker_info();//작업자 팝업
                    sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                    txt_day_sqty.Text = sqty_arry[0];
                    txt_night_sqty.Text = sqty_arry[1];
                    txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                    txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
                    lueWc_code.Enabled = false;

                    //작업 시작 시간 업데이트 
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");

                    string print_type = Settings_Xml.read(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                    if (string.IsNullOrWhiteSpace(print_type))
                    {
                        Settings_Xml.reWrite(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                        print_type = luePrint_type.EditValue.ToString().Trim();
                    }

                    luePrint_type.EditValue = print_type;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }/*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }

        //클리어
        
        public void formClear2()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_fail_qty.Text = null;
            txt_day_sqty.Text = null;
            txt_night_sqty.Text = null;
            
            out_carrier_no = "";
            mo_snumb = "";
            
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_DC.Rows.Clear();

        }
        //비가동 등록
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            try
            {
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                //PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code
                Dt_Input.PP_SITE_CODE_str = PP_SITE_CODE_str;
                Dt_Input.mo_snumb = mo_snumb;
                Dt_Input.str_wc_code = str_wc_code;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str, mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime, str_wc_code);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        


        private void btn_fail_input_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("생산계획을 선택해주세요");
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    Fail_Input Fail_Input = new Fail_Input();
                    Fail_Input.wc_group = wc_group;
                    if (Fail_Input.ShowDialog() == DialogResult.OK)
                    {

                        //불량 등록
                        int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                        if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb, str_wc_code))
                        {
                            txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                        }
                    }
                    this.Cursor = Cursors.Default;
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }


        private void btn_pp_card_search_Click(object sender, EventArgs e)
        {
            try
            {

                if (!string.IsNullOrWhiteSpace(mo_snumb))
                {
                    if (btn_carrier_sqty.Text.Trim() != "0")
                    {
                        string pd_lot_no = work_input_save_virtual_NEW("D001", mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code.Trim()
                                                            , btn_carrier_sqty.Text.Trim(), txt_datetime.Text, "", out_carrier_no, "Y");
                        //if (me_scode.Trim().Equals("40"))
                        //{
                            // 식별표 출력
                            //it_chart_print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no);
                            if (luePrint_type.EditValue.ToString().Equals("A"))
                            {
                                //모비스 식별표 출력
                                Func_Mobis_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                            }
                            else if (luePrint_type.EditValue.ToString().Equals("B"))
                            {
                                //기아 식별표 출력
                                Func_Kia_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                            }

                            PpCard_Success10.TopLevel = true;
                            PpCard_Success10.TopMost = true;
                            PpCard_Success10.Visible = true;
                            PpCard_Success10.set_text2(11);
                        //}
                    }
                    else
                    {
                        MessageBox.Show("용기 수량이 0 입니다. 등록 할 수 없습니다.");
                    }
                }
                else
                {
                    MessageBox.Show("작업지시를 선택해주세요");
                }

            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        //완제품 실적 등록시 
        public string work_input_save_virtual_NEW(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string card_no, string carrier_no, string carrier_yn)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string lot_no = "";
            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            SqlDataReader reader = null;

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_ASSY_NEW", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            
            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY", float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY", 0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);

            //pp 시리얼번호
            cmd.Parameters.AddWithValue("@CARD_NO", card_no);

            //대차 번호
            cmd.Parameters.AddWithValue("@CARRIER_NO", "");

            cmd.Parameters.AddWithValue("@MM_RDATE",  DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@CARRIER_YN", carrier_yn);

            cmd.Parameters.AddWithValue("@WORKER", str_worker);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                reader = cmd.ExecuteReader();

                //양품수량 증가
                while (reader.Read())
                {
                    lot_no = reader["PD_LOT_NO"].ToString();
                }
                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                if (DateTime.Now.Hour > 18 || DateTime.Now.Hour < 8)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }
                if (reader!=null)
                    reader.Close(); 
                
                trans.Commit();
            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return lot_no;
        }

        //식별표 출력
        


        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                regKey.SetValue("WC_CODE", str_wc_code);
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            
        }

        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {
            //IP 컨넥션 환경설정 셋팅 

        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            try
            {
                metarial_search metarial_search = new metarial_search();
                metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                if (metarial_search.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }

        private void txtOut_carrier_no_Click(object sender, EventArgs e)
        {          
            
            out_carrier_no = "";
            txtIt_scode.Text = "";
            Tag_DT_DC.Clear();
            
            
        }

        private void btn_carrier_sqty_Click(object sender, EventArgs e)
        {
            try
            {
                KeyPad KeyPad = new KeyPad();
                if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    btn_carrier_sqty.Text = KeyPad.txt_value;
                }/*
            if (btn_carrier_sqty.Text.Trim().Equals("32"))
            {
                btn_carrier_sqty.Text = "40";
            }
            else
            {
                btn_carrier_sqty.Text = "32";
            }*/
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }


        //작업유형 가져오기
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code = shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            //Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_group = wc_group;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;

                //작업 화면에 표시 해주기 위한 변수
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }

                //DB에 저장 하기 위한 변수
                str_worker = "";
                DataRow[] drr = select_worker_dt.Select();
                for (int i = 0; i < drr.Length; i++)
                {
                    if (i == drr.Length - 1)
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString();
                    }
                    else
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString() + ",";
                    }
                }

            }
        }

        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            try
            {
                set_worker_info();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_on_off_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_on_off.Text.Trim().Equals("On"))
                {
                    btn_on_off.Text = "Off";
                }
                else
                {
                    btn_on_off.Text = "On";
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
        public string transfer = "NO";
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                MessageBox.Show("생산 계획을 선택해 주세요");
                return;
            }
            Injection_Carrier_KeyPad Injection_Carrier_KeyPad = new Injection_Carrier_KeyPad();
            Injection_Carrier_KeyPad.it_scode = txt_IT_SCODE.Text.Trim();
            Injection_Carrier_KeyPad.wc_group = wc_group;
            if (Injection_Carrier_KeyPad.ShowDialog() == DialogResult.OK)
            {
                transfer = Injection_Carrier_KeyPad.transfer;
                out_carrier_no = Injection_Carrier_KeyPad.txt_value;
                
                
                
            }
        }

        private void btn_fail_search_Click(object sender, EventArgs e)
        {
            try
            {
                fail_search_popup fail_search_popup = new fail_search_popup();
                fail_search_popup.wc_group = wc_group;
                fail_search_popup.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
                fail_search_popup.wc_name = lueWc_code.GetColumnValue("WC_NAME").ToString();
                fail_search_popup.Show();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_day_work_Click(object sender, EventArgs e)
        {
            try
            {
                Day_Night_Sqty_Popup Day_Night_Sqty_Popup = new Day_Night_Sqty_Popup();
                Day_Night_Sqty_Popup.wc_code = str_wc_code;
                Day_Night_Sqty_Popup.Show();
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_mix_toggle_Click(object sender, EventArgs e)
        {
            if(btn_mix_toggle.Text.Trim()=="Off")
            {
                btn_mix_toggle.Text = "On";
            }
            else
            {
                btn_mix_toggle.Text = "Off";
            }
        }

        private void time_now_Tick(object sender, EventArgs e)
        {
            imageSlider1.SlideNext();
        }

        private void btn_po_release_Insert_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                try
                {
                    if (true)
                    {

                        Po_release_Insert_dev Po_release_Insert_dev = new Po_release_Insert_dev();
                        Po_release_Insert_dev.wc_group = wc_group;
                        Po_release_Insert_dev.wc_code = str_wc_code;
                        if (Po_release_Insert_dev.ShowDialog() == DialogResult.OK)
                        {
                            formClear2();
                            txt_IT_SCODE.Text = Po_release_Insert_dev.IT_SCODE_str;
                            //작업표준서
                            worker.RunWorkerAsync();
                            txt_IT_SNAME.Text = Po_release_Insert_dev.IT_SNAME_str;
                            txt_IT_MODEL.Text = Po_release_Insert_dev.IT_MODEL_str;
                            txt_MO_SQUTY.Text = Po_release_Insert_dev.MO_SQUTY_str;
                            me_scode = Po_release_Insert_dev.ME_SCODE_str;
                            //str_wc_code = Work_plan_search.WC_CODE_str;
                            mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                            PP_SITE_CODE_str = Po_release_Insert_dev.SITE_CODE_str;
                            me_scode = Po_release_Insert_dev.ME_SCODE_str;
                            //max_sqty = Po_release_Insert_dev.MAX_SQTY_str;
                            //txt_MO_SQUTY.Text = max_sqty;
                            //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            txt_datetime.Text = r_start;
                            carrier_yn = "Y";
                            mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                            btn_carrier_sqty.Text = Po_release_Insert_dev.IT_PKQTY_str;
                            /*
                            DataRow[] drr = po_DT.Select("PO_IT_SCODE='" + txt_IT_SCODE.Text.Trim() + "'");
                            for (int i = 0; i < drr.Length; i++)
                                po_DT.Rows.Remove(drr[i]);
                            po_DT.AcceptChanges();

                            DataRow[] po_row = po_DT.Select("MO_SNUMB='" + mo_snumb + "'");

                            if (po_row.Length == 0)
                            {
                                DataRow dr;
                                dr = po_DT.NewRow();
                                dr["MO_SNUMB"] = mo_snumb;
                                dr["PO_IT_SCODE"] = txt_IT_SCODE.Text;
                                dr["PO_IT_SNAME"] = txt_IT_SNAME.Text;
                                dr["PO_ME_SCODE"] = me_scode;

                                po_DT.Rows.Add(dr);
                            }*/
                            //양품수량 불량수량 들고오기
                            GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                            txt_good_qty.Text = GET_DATA.good_qty;
                            txt_fail_qty.Text = GET_DATA.fail_qty;
                            
                            //Swing2.InventoryStart();
                            
                            txtIt_scode.Text = "";
                            set_현재고_최대생산가능수량();
                            shiftwork();//작업유형 가져오기
                            set_worker_info();//작업자 팝업
                            sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                            txt_day_sqty.Text = sqty_arry[0];
                            txt_night_sqty.Text = sqty_arry[1];
                            txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                            
                            lueWc_code.Enabled = false;
                            SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");

                            string print_type = Settings_Xml.read(Po_release_Insert_dev.IT_SCODE_str.Trim(), luePrint_type.EditValue.ToString().Trim());
                            if (string.IsNullOrWhiteSpace(print_type))
                            {
                                Settings_Xml.reWrite(Po_release_Insert_dev.IT_SCODE_str.Trim(), luePrint_type.EditValue.ToString().Trim());
                                print_type = luePrint_type.EditValue.ToString().Trim();
                            }

                            luePrint_type.EditValue = print_type;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("다시 시도해주세요 : " + ex.Message);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_re_print_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    MessageBox.Show("작업지시를 선택해 주세요");
                    return;
                }
                if (luePrint_type.EditValue.ToString().Equals("A"))
                {
                    Func_Mobis_Print.re_Print(txt_IT_SCODE.Text.Trim(), 2);
                }
                else if (luePrint_type.EditValue.ToString().Equals("B"))
                {
                    Func_Kia_Print.re_Print(txt_IT_SCODE.Text.Trim(), 2);
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }
        bool all_check = false;
        string child_it_scode = "";
        string child_lot_no = "";
        string read_assy_barcode = "";
        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("작업지시를 선택해주세요");
                    textEdit1.Text = "";
                    return;
                }
                try
                {
                    //BOM 체크(이종체크)
                    string barcode = textEdit1.Text.Trim();
                    string[] arrBarcode = barcode.Split('*');
                    if (arrBarcode.Length.Equals(2))//사출 QR코드 리딩 
                    {
                        string read_it_scode = arrBarcode[0].ToString();
                        string read_lot = arrBarcode[1].ToString();
                        string[] arr_lot = read_lot.Split('/');
                        if (arr_lot.Length.Equals(4))
                        {

                            DataTable dt = GET_DATA.bom_chk(txt_IT_SCODE.Text,"N");//완성품품목으로 bom 정전개
                            DataRow[] dr = dt.Select("IT_SCODE='" + read_it_scode + "'");//리딩된 사출품번이 bom에 있는지 체크
                            if (dr.Length > 0)
                            {
                                //검사기데이터 까지 완료 되었는지 체크 
                                if (check_func_child(read_it_scode, read_lot))
                                {
                                    child_it_scode = read_it_scode;
                                    child_lot_no = read_lot;
                                }
                                else
                                {
                                    child_it_scode = "";
                                    child_lot_no = "";
                                    MessageBox.Show("검사하지 않는 데이터 입니다.");
                                }
                            }
                            else
                            {
                                MessageBox.Show("잘못된 제품 입니다.");
                            }
                        }
                        
                    }
                    else//검사기에서 출력되는 바코드 리딩
                    {
                        //체크 기능 필요 체크 테이블 

                        read_assy_barcode = barcode; 
                    }
                    if (!string.IsNullOrWhiteSpace(read_assy_barcode) && !string.IsNullOrWhiteSpace(child_lot_no))
                    {
                        //카운트 증가 및 READING_DATA UPDATE
                        MessageBox.Show("등록시작");
                        개별등록시작();
                        read_assy_barcode = "";
                        child_lot_no = "";
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
            }

        }
        //개별 등록 시작
        public void 개별등록시작()
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("작업지시를 선택해주세요");
                return;
            }
            if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
            {
                MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                return;
            }
            if (txt_weight_cnt.Text.Trim() == btn_carrier_sqty.Text.Trim())
            {
                MessageBox.Show("대차에 수량이 가득 찼습니다. \n 수동으로 수량을 입력 할 수 없습니다.");
                return;
            }

            else
            {


                string gubn = "Y";

                //가실적 번호 가져오기
                int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);


                if (gubn.Equals("Y"))
                {
                    //saveWeight(PP_SITE_CODE_str, mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code, vw_snumb, lblWeight.Text, gubn);
                    //txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();
                    SqlTransaction tran = null;
                    string strCon;
                    strCon = Properties.Settings.Default.SQL_DKQT;

                    SqlConnection conn = new SqlConnection(strCon);
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("SP_TABLET_SAVE_ASSY_LOT", conn);
                    cmd.Transaction = tran;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SITE_CODE", PP_SITE_CODE_str);
                    cmd.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
                    cmd.Parameters.AddWithValue("@PRDT_ITEM", txt_IT_SCODE.Text.Trim());
                    cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);
                    cmd.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);
                    cmd.Parameters.AddWithValue("@IN_WEIGHT", 0);
                    cmd.Parameters.AddWithValue("@CHECK_YN", gubn);
                    cmd.Parameters.AddWithValue("@BARCODE_READ", read_assy_barcode);
                    cmd.Parameters.AddWithValue("@CHILD_ITEM", child_it_scode);
                    cmd.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);

                    DataSet ds = new DataSet();
                    try
                    {
                        string product_lotno = "";
                        string sdate = "";
                        string stime = "";
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            product_lotno = reader["PRODUCT_LOTNO"].ToString();
                            sdate = reader["SDATE"].ToString();
                            stime = reader["STIME"].ToString();

                        }
                        reader.Close();
                        reader.Dispose();
                        txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        MessageBox.Show(ex.Message + " : (0)");
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                if (int.Parse(txt_weight_cnt.Text.Trim()) >= int.Parse(btn_carrier_sqty.Text.Trim()))
                {
                    string pd_lot_no = "";
                    pd_lot_no = work_input_save_virtual_Assy(Properties.Settings.Default.SITE_CODE.ToString()
                        , mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code.Trim(), btn_carrier_sqty.Text.Trim(), txt_datetime.Text, CARD_NO_str);
                    if (luePrint_type.EditValue.ToString().Equals("A"))
                    {
                        //모비스 식별표 출력
                        Func_Mobis_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                    }
                    else if (luePrint_type.EditValue.ToString().Equals("B"))
                    {
                        //기아 식별표 출력
                        Func_Kia_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                    }

                    PpCard_Success10.TopLevel = true;
                    PpCard_Success10.TopMost = true;
                    PpCard_Success10.Visible = true;
                    PpCard_Success10.set_text2(11);
                }
            }
        }

        public string work_input_save_virtual_Assy(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string pp_serno)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            string lot_no = "";
            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            SqlDataReader reader = null;
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_ASSY_LOT", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY", float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY", 0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);
            
            cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@WORKER", str_worker);



            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                reader = cmd.ExecuteReader();

                //양품수량 증가
                while (reader.Read())
                {
                    lot_no = reader["PD_LOT_NO"].ToString();
                }
                reader.Close();
                reader.Dispose();
                trans.Commit();
                //양품수량 증가
                

                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                string hh = DateTime.Now.Hour.ToString();
                string mm = DateTime.Now.Minute.ToString();
                if (hh.Length == 1)
                {
                    hh = "0" + hh;
                }
                if (mm.Length == 1)
                {
                    mm = "0" + mm;
                }
                string time = hh + mm;


                if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }
                txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
                
            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
            return lot_no;
        }
        public bool check_func_child(string child_item, string child_lot)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "DECLARE @V_TAB TABLE(NUMB INT,OP_CODE VARCHAR(10))"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(1,'A1')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(2,'B1')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(3,'C1')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(4,'E1')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(5,'E2')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(6,'C2')"
                        +" INSERT INTO @V_TAB(NUMB,OP_CODE)VALUES(7,'F1')"
                        +" IF EXISTS(SELECT * FROM READING_DATA WHERE CHILD_LOT_NO = '"+child_lot+"' AND CHILD_IT_SCODE='"+child_item+"' "
                        +" AND OP_CODE_ING = (SELECT OP_CODE FROM @V_TAB WHERE NUMB = (SELECT MAX(NUMB) FROM @V_TAB)))"
                        +" SELECT 'OK' AS RESULT"
                        +" ELSE SELECT 'NG' AS RESULT";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["RESULT"].ToString().Trim() == "OK")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        private void luePrint_type_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    return;
                }
                else
                {
                    Settings_Xml.reWrite(txt_IT_SCODE.Text.Trim(), luePrint_type.EditValue.ToString().Trim());
                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Text = "";
                textEdit1.Focus();
            }
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(str_wc_code.ToString().Trim()))
            {
                MessageBox.Show("작업장을 선택 해 주새요");
                return;
            }
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(parentForm);
            FME_InsResultReg.wc_code = str_wc_code.ToString().Trim();
            FME_InsResultReg.Show();
        }

        private void btn_program_finish_Click(object sender, EventArgs e)
        {
            Finish_Popup Finish_Popup = new Finish_Popup();
            if (Finish_Popup.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Finish_Popup.btn_state.Equals("0"))
                {

                }
                else if (Finish_Popup.btn_state.Equals("1"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    Application.Exit();
                }
                else if (Finish_Popup.btn_state.Equals("2"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");

                }
            }
        }

        private void TEST_Click(object sender, EventArgs e)
        {
            
            string equip_laser_str_in = "A10S";
            string equip_clip_str_in = "B10S";
            string equip_초음파1_str_in = "C10S";
            string equip_초음파2_str_in = "C20S";
            string equip_진동1_str_in = "E10S";
            string equip_진동2_str_in = "E20S";
            string equip_검사_str_in = "F10S";            

            if (sender == TEST_LASER_in)
            {
                //insert into 
                Equip_in_out_insert(equip_laser_str_in, "");
                
            }
            else if (sender == TEST_CLIP_in)
            {
                Equip_in_out_insert(equip_clip_str_in, "");
            }
            else if (sender == TEXT_초음파1_in)
            {
                Equip_in_out_insert(equip_초음파1_str_in, "");
            }
            else if (sender == TEXT_진동1_in)
            {
                Equip_in_out_insert(equip_진동1_str_in, "");
            }
            else if (sender == TEST_진동2_in)
            {
                Equip_in_out_insert(equip_진동2_str_in, "");
            }
            else if (sender == TEST_초음파2_in)
            {
                Equip_in_out_insert(equip_초음파2_str_in, "");
            }
            else if (sender == TEST_검사_in)
            {
                Equip_in_out_insert(equip_검사_str_in, "");
            }


        }
        private void TEST_OUT_Click(object sender, EventArgs e)
        {

            string equip_laser_str_out = "A11S00";
            equip_laser_str_out = "A11S010305#01OK05#02OK07#0310.1";
            string equip_clip_str_out = "B11S00";
            string equip_초음파1_str_out = "C11S00";
            string equip_초음파2_str_out = "C21S00";
            string equip_진동1_str_out = "E11S00";
            string equip_진동2_str_out = "E21S00";
            string equip_검사_str_out = "F11S00";

            if (sender == TEST_LASER_out)
            {
                //insert into 
                Equip_in_out_insert(equip_laser_str_out, "");
            }
            else if (sender == TEST_CLIP_out)
            {
                Equip_in_out_insert(equip_clip_str_out, "");
            }
            else if (sender == TEXT_초음파1_out)
            {
                Equip_in_out_insert(equip_초음파1_str_out, "");

            }
            else if (sender == TEXT_진동1_out)
            {
                Equip_in_out_insert(equip_진동1_str_out, "");
            }
            else if (sender == TEST_진동2_out)
            {
                Equip_in_out_insert(equip_진동2_str_out, "");
            }
            else if (sender == TEST_초음파2_out)
            {
                Equip_in_out_insert(equip_초음파2_str_out, "");
            }
            else if (sender == TEST_검사_out)
            {
                Equip_in_out_insert(equip_검사_str_out, "");
            }
        }
        public void Equip_in_out_insert(string data, string child_lot_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            string 장비번호 = data.Substring(0,2);
            string 상태값 = data.Substring(2,1); // 0:in , 1:out
            string 전송구분 = data.Substring(3,1);// S:전송 , R:재전송
            string OK_NG = "X"; // 0:OK , 1:NG
            string 검사유무 = "X"; // 0:무 , 1:유
            int 블럭수 = 0;            
            string ins_data = "";    
            
            if (상태값.Equals("1"))//out 일때
            {
                OK_NG = data.Substring(4, 1);
                검사유무 = data.Substring(5, 1);
                if (검사유무.Equals("1"))
                {
                    블럭수 = int.Parse(data.Substring(6,2));

                    ins_data     = data.Substring(8, data.Length - 8);
                

                }
                
            }
            //IN 일때
            if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                return;
            }
            else if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                EQUIP[장비번호] = "RUN";
            }

            //OUT 일때
            if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                return;
            }
            else if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                EQUIP[장비번호] = "OFF";
            }
            
            DataTable ins_dt = new DataTable();
            ins_dt.Columns.Add("INS_DATA",typeof(string));
            for (int i = 0; i < 블럭수; i++)
            {
                int ins_data_length = int.Parse(ins_data.Substring(0, 2));
                DataRow dr = ins_dt.NewRow();
                dr["INS_DATA"] = ins_data.Substring(2, ins_data_length);
                ins_dt.Rows.Add(dr);
                ins_data = ins_data.Substring(2 + ins_data_length, ins_data.Length- (2 + ins_data_length));
            }
            //색 변경해 주기
            roundButton_color_change(장비번호, 상태값, OK_NG);
            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@IT_SCODE", txt_IT_SCODE.Text.Trim());            
            cmd.Parameters.AddWithValue("@OP_CODE", 장비번호);
            cmd.Parameters.AddWithValue("@TOTAL_INS", OK_NG);
            cmd.Parameters.AddWithValue("@IN_OUT_FLAG", 상태값);
            cmd.Parameters.AddWithValue("@TVP", ins_dt);
            cmd.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);


            try
            {

                cmd.ExecuteNonQuery();

                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        public void roundButton_color_change(string 장비값, string 상태값, string ok_ng)
        {

            //MessageBox.Show(상태값);
            //MessageBox.Show(ok_ng);
            if (상태값.Equals("0"))//in
            {
                switch (장비값)
                {
                    case "A1": roundButton1.ForeColor = Color.Yellow;                        
                        break;
                    case "B1": roundButton2.ForeColor = Color.Yellow;
                        break;
                    case "C1": roundButton3.ForeColor = Color.Yellow;
                        break;
                    case "E1": roundButton4.ForeColor = Color.Yellow;
                        break;
                    case "E2": roundButton5.ForeColor = Color.Yellow;
                        break;
                    case "C2": roundButton6.ForeColor = Color.Yellow;
                        break;
                    case "F1": roundButton7.ForeColor = Color.Yellow;
                        break;
                }
            }
            else if (상태값.Equals("1"))//out
            {
                if (ok_ng.Equals("0"))//ok
                {
                    switch (장비값)
                    {
                        case "A1": roundButton1.ForeColor = Color.Blue;
                            Thread t1 = new Thread(()=>Reflesh_color(장비값));
                            t1.Start();
                            break;
                        case "B1": roundButton2.ForeColor = Color.Blue;
                            Thread t2 = new Thread(()=>Reflesh_color(장비값));
                            t2.Start();
                            break;
                        case "C1": roundButton3.ForeColor = Color.Blue;
                            Thread t3 = new Thread(()=>Reflesh_color(장비값));
                            t3.Start();
                            break;
                        case "E1": roundButton4.ForeColor = Color.Blue;
                            Thread t4 = new Thread(()=>Reflesh_color(장비값));
                            t4.Start();
                            break;
                        case "E2": roundButton5.ForeColor = Color.Blue;
                            Thread t5 = new Thread(()=>Reflesh_color(장비값));
                            t5.Start();
                            break;
                        case "C2": roundButton6.ForeColor = Color.Blue;
                            Thread t6 = new Thread(()=>Reflesh_color(장비값));
                            t6.Start();
                            break;
                        case "F1": roundButton7.ForeColor = Color.Blue;
                            Thread t7 = new Thread(()=>Reflesh_color(장비값));
                            t7.Start();
                            break;
                    }
                }
                else if (ok_ng.Equals("1"))//ng
                {
                    switch (장비값)
                    {
                        case "A1": roundButton1.ForeColor = Color.Red;
                            Thread t1 = new Thread(()=>Reflesh_color(장비값));
                            t1.Start();
                            break;
                        case "B1": roundButton2.ForeColor = Color.Red;
                            Thread t2 = new Thread(()=>Reflesh_color(장비값));
                            t2.Start();
                            break;
                        case "C1": roundButton3.ForeColor = Color.Red;
                            Thread t3 = new Thread(()=>Reflesh_color(장비값));
                            t3.Start();
                            break;
                        case "E1": roundButton4.ForeColor = Color.Red;
                            Thread t4 = new Thread(()=>Reflesh_color(장비값));
                            t4.Start();
                            break;
                        case "E2": roundButton5.ForeColor = Color.Red;
                            Thread t5 = new Thread(()=>Reflesh_color(장비값));
                            t5.Start();
                            break;
                        case "C2": roundButton6.ForeColor = Color.Red;
                            Thread t6 = new Thread(()=>Reflesh_color(장비값));
                            t6.Start();
                            break;
                        case "F1": roundButton7.ForeColor = Color.Red;
                            Thread t7 = new Thread(()=>Reflesh_color(장비값));
                            t7.Start();
                            break;
                    }
                }
            }

        }
        void Reflesh_color(string 장비값)
        {   
            Thread.Sleep(5000);
            switch (장비값)
            {
                case "A1": roundButton1.ForeColor = Color.DimGray;
                    break;
                case "B1": roundButton2.ForeColor = Color.DimGray;
                    break;
                case "C1": roundButton3.ForeColor = Color.DimGray;
                    break;
                case "E1": roundButton4.ForeColor = Color.DimGray;
                    break;
                case "E2": roundButton5.ForeColor = Color.DimGray;
                    break;
                case "C2": roundButton6.ForeColor = Color.DimGray;
                    break;
                case "F1": roundButton7.ForeColor = Color.DimGray;
                    break;
            }
        }
        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            MessageBox.Show(e.Message);
        }
        
    }
}
