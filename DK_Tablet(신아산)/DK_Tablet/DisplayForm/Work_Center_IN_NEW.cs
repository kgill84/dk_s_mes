﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;
using System.Net.Sockets;


namespace DK_Tablet
{
    public partial class Work_Center_IN_NEW : Form
    {
        
        
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        //private SwingLibrary.SwingAPI Swing2 = null;//스윙-u 사용하기 위한 API 객체 생성
        
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        //string carrier_yn = "";//공대차인지 아닌지 구분자
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        //출구 리더기
        PpCard_reading_paint_in PpCard_reading;
        
        PpCard_Success PpCard_Success = new PpCard_Success();
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";

        bool 레이져컷팅_toggle = false;
        bool 클립조립_toggle = false;
        bool 초음파1_toggle = false;
        bool 진동융착_toggle = false;
        bool 초음파2_toggle = false;
        bool 검사기_toggle = false;
        string ip_레이져컷팅 = "172.168.100.10";
        string ip_클립조립 = "172.168.100.20";
        string ip_초음파융착1 = "172.168.100.30";
        string ip_진동융착 = "172.168.100.40";
        string ip_초음파융착2 = "172.168.100.31";
        string ip_검사기 = "172.168.100.50";
        string ip_Op_Pannel = "172.168.100.60";
        Socket clientsock_초음파1; //LS 초음파1
        Socket clientsock_초음파2; //LS 초음파2
        // A buffer of transmitted
        byte[] MsgSendBuff_초음파1 = new byte[39];//LS PLC 보낼 데이터 변수
        byte[] MsgSendBuff_초음파2 = new byte[39];//LS PLC 보낼 데이터 변수
        // A buffer of received  
        byte[] MsgRecvBuff_초음파1 = new byte[512];//LS PLC 받는 데이터 변수
        byte[] MsgRecvBuff_초음파2 = new byte[512];//LS PLC 받는 데이터 변수


        public Work_Center_IN_NEW(MAIN form)
        {
         PpCard_reading = new PpCard_reading_paint_in(this);

            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            
        }
      
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {/*
                Thread TimeThread = new Thread(GetTime);
                TimeThread.IsBackground = true;
                TimeThread.Start();*/
                PpCard_reading.Show();
                PpCard_reading.Visible = false;
                Success_Form.Show();
                Success_Form.Visible = false;

                GET_DATA.get_work_time_master();
                night_time_start = GET_DATA.night_time_start;
                day_time_start = GET_DATA.day_time_start;
                
                //timer_now.Start();
                //GET_DATA.SYSTEMTIME stime = new GET_DATA.SYSTEMTIME();
                //stime = GET_DATA.GetTime();
                //N_timer.Text = stime.wYear.ToString() + "-" + stime.wMonth + "-" + stime.wDay + " " + stime.wHour + ":" + stime.wMinute + ":" + stime.wSecond;
                
                GET_DATA.get_timer_master(wc_group);
                
                

                lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";
                //lookUpEdit1.SelectionStart = 0;
                //lueWc_code.ItemIndex = 0;
                if (regKey.GetValue("WC_CODE") == null)
                {
                    regKey.SetValue("WC_CODE", "");
                }
                else
                {
                    if (regKey.GetValue("WC_CODE").ToString() != "")
                    {

                        lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                    }
                }

                

                dateEdit1.DateTime = DateTime.Now;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }
            finally
            {
                textEdit1.Text = ""; textEdit1.Focus();
            }

        }
        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {    
            parentForm.Visible = true;
        }
       

       

        private void btn_po_release_Click(object sender, EventArgs e)
        {
            try
            {
                work_plan_popup_paint();
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
            //po_release_popup();
            
        }


        public void work_plan_popup_paint()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                //if (Swing2.IsOpen)
                if (true)
                {

                    Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                    Work_plan_search.wc_group = wc_group;
                    Work_plan_search.wc_code = str_wc_code;
                    if (Work_plan_search.ShowDialog() == DialogResult.OK)
                    {
                        formClear2();
                      
                        //txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                        me_scode = Work_plan_search.ME_SCODE_str;
                        max_sqty = Work_plan_search.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        
                        //carrier_yn = "Y";

                        //양품수량 불량수량 들고오기
                        //GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        //txt_good_qty.Text = GET_DATA.good_qty;
                        //txt_fail_qty.Text = GET_DATA.fail_qty;

                       
                        
                        //set_현재고_최대생산가능수량();
                        //shiftwork();//작업유형 가져오기
                        //set_worker_info();//작업자 팝업
                       
                        timer_re_carrier.Stop();
                        timer_re_carrier.Start();


                        lueWc_code.Enabled = false;

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }    /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }

        //클리어
        public void formClear()
        {
            mo_snumb = "";
            
            /*PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";*/
            PP_SITE_CODE_str = "";
            

        }
        public void formClear2()
        {
            mo_snumb = null;
            
            
            //txt_MO_SQUTY.Text = null;
            //txt_good_qty.Text = null;
            //txt_fail_qty.Text = null;
            mo_snumb = "";
            /*PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";*/
            PP_SITE_CODE_str = "";       
        }
        //비가동 등록
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {
                    
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code);
                }
                this.Cursor = Cursors.Default;
            }
        }

        


        private void btn_fail_input_Click(object sender, EventArgs e)
        {/*
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                Fail_Input Fail_Input = new Fail_Input();
                Fail_Input.wc_group = wc_group;
                if (Fail_Input.ShowDialog() == DialogResult.OK)
                {

                    //불량 등록
                    int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb,str_wc_code);
                    if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb,str_wc_code))
                    {
                        txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                    }
                }
                this.Cursor = Cursors.Default;
            }
          */
        }


        //bool PpCard_reading_chk = false;
        
        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            
            regKey.SetValue("WC_CODE", str_wc_code);
        }


        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {
            try
            {
                string[] arryPort_power_in = null;
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.Trim()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                try
                {
                    arryPort_power_in = GET_DATA.get_connection_port(lueWc_code.GetColumnValue("WC_CODE").ToString(), "IN");
                    //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                    
                }
                catch (Exception ex)
                {
                    
                }

                

                
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            try
            {
                metarial_search metarial_search = new metarial_search();
                metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                if (metarial_search.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
            
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }


        private void timer_re_carrier_Tick(object sender, EventArgs e)
        {
            
        }
        /*
        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }
        */


        //작업유형 가져오기
        /*
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code = shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }*/

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        /*
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_code = str_wc_code;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0][1].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0][1].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }
            }
        }
        */


        private void timer1_Tick(object sender, EventArgs e)
        {
            PpCard_reading.TopLevel = false;
            PpCard_reading.TopMost = false;
            PpCard_reading.Visible = false;
            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            timer1.Stop();
        }
        private Success_Form Success_Form=new Success_Form();
        

        
        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("작업지시를 선택해주세요");
                    textEdit1.Text = "";
                    return;
                }
                try
                {
                    
                    string barcode = textEdit1.Text.Trim();
                    //string[] arrBarcode = barcode.Split('*');
                    //if (arrBarcode.Length.Equals(2))
                    //{
                    //    string read_it_scode = arrBarcode[0].ToString();
                    //    string read_lot = arrBarcode[1].ToString();
                    //    string[] arr_lot = read_lot.Split('/');
                    //    if (arr_lot.Length.Equals(4))
                    //    {
                    //        //리딩된 사출 QR 코드로 레이져공정을 거쳐서 왔는지 체크
                    //        //BOM 체크(이종체크)
                    //        DataTable dt = GET_DATA.bom_chk(txt_IT_SCODE.Text,"N");//완성품품목으로 bom 정전개
                    //        DataRow[] dr = dt.Select("IT_SCODE='"+read_it_scode+"'");//리딩된 사출품번이 bom에 있는지 체크
                    //        if (dr.Length>0)
                    //        {
                    //            /*
                    //             * read 사출 qr 코드가 사용 되었는지 체크 NG/진행중/완료 인지 체크 (테이블 : READING_DATA)
                    //             * 
                    //             * 완료되었으면 메세지 띄워주고 return
                    //             * NG : 재투입
                    //             * 진행중이면 return
                    //            */
                    //            check_save_read_data(txt_IT_SCODE.Text.Trim(),read_it_scode,read_lot);

                                
                    //        }
                    //        else
                    //        {
                    //            PpCard_Success.TopLevel = true;
                    //            PpCard_Success.TopMost = true;
                    //            PpCard_Success.Visible = true;
                    //            PpCard_Success.set_text("NG", 5);
                    //            PpCard_Success.BackColor = Color.Red;
                    //        }
                    //    }
                    //    //사용되지 않았으면 체크하는 테이블에 저장 하면서 카운트
                    //}
                    //else 
                        if (barcode.Substring(0, 2).Equals("PM"))
                    {
                        bool check = false;
                        DataTable dt = GET_DATA.bom_chk("","Y");
                        string child_it_scode = GET_DATA.get_pm_child_code(barcode);
                        DataRow[] dr = dt.Select("IT_SCODE='" + child_it_scode + "'");
                        if (dr.Length > 0)
                        {
                            if (GET_DATA.get_pm_CHECK(barcode))
                            {


                                check = MOVE_FUNC.move_insert_NEW_pm(barcode, str_wc_code);
                                if (check)
                                {

                                    Success_Form.TopLevel = true;
                                    Success_Form.TopMost = true;
                                    Success_Form.Visible = true;
                                    Success_Form.set_text(barcode);
                                    timer1.Start();
                                }
                                else
                                {
                                    PpCard_Success.TopLevel = true;
                                    PpCard_Success.TopMost = true;
                                    PpCard_Success.Visible = true;
                                    PpCard_Success.set_text("NG", 5);
                                    PpCard_Success.BackColor = Color.Red;

                                }
                            }
                            else
                            {
                                PpCard_Success.TopLevel = true;
                                PpCard_Success.TopMost = true;
                                PpCard_Success.Visible = true;
                                PpCard_Success.set_text("완료된 이동표 입니다.", 5);
                                PpCard_Success.BackColor = Color.Salmon;
                            }
                        }
                        else
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("해당하는 제품이 아닙니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;

                        }
                        textEdit1.Text = "";
                    }
                        else
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("잘못된 이동표 입니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;
                        }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
            }

        }

        public void check_save_read_data(string it_scode, string child_it_scode, string child_lot_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_IN_READ_DATA_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);
            cmd.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);

            try
            {   
                cmd.ExecuteNonQuery();
                
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                
            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            if (e.Message.Trim().Equals("OK"))
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
            }
            else
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }
        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //bool bFind = false;
            //DriveInfo[] diArray = DriveInfo.GetDrives();
            //foreach (DriveInfo di in diArray)
            //{
            //    MessageBox.Show(di.IsReady.ToString());
            //    MessageBox.Show(di.DriveType.ToString());
            //}
            check_save_read_data("", "", "");
        }

        private void btn_ins_search_Click(object sender, EventArgs e)
        {
            Ins_search_Popup Ins_search_Popup = new Ins_search_Popup();
            Ins_search_Popup.wc_code = str_wc_code;
            Ins_search_Popup.ShowDialog();
        }


    }
}
