﻿namespace DK_Tablet
{
    partial class Tag_Writing
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rTB_write = new System.Windows.Forms.RichTextBox();
            this.rd_set2 = new System.Windows.Forms.RadioButton();
            this.rd_set1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.write_str_cnt = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.button_memWrite = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.rTB_error = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox_bank = new System.Windows.Forms.ComboBox();
            this.textBox_BlockOffset = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox_BlockCount = new System.Windows.Forms.TextBox();
            this.button_memRead = new System.Windows.Forms.Button();
            this.textBox_accesspwd_rw = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.rTB_read = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button_memClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_size = new System.Windows.Forms.TextBox();
            this.txt_pp_serno = new System.Windows.Forms.TextBox();
            this.txt_it_scode = new System.Windows.Forms.TextBox();
            this.txt_wc_code = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_dc = new System.Windows.Forms.TextBox();
            this.btn_clear_cnt = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.button_inventory_stop = new System.Windows.Forms.Button();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.cmbSiteCode = new System.Windows.Forms.ComboBox();
            this.groupBox6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownWidth = 150;
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(12, 12);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(179, 20);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(197, 12);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 23);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(278, 12);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 23);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(359, 12);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 14;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cmbSiteCode);
            this.groupBox6.Controls.Add(this.rTB_write);
            this.groupBox6.Controls.Add(this.rd_set2);
            this.groupBox6.Controls.Add(this.rd_set1);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.write_str_cnt);
            this.groupBox6.Controls.Add(this.button5);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.button_memWrite);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.rTB_error);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.comboBox_bank);
            this.groupBox6.Controls.Add(this.textBox_BlockOffset);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.textBox_BlockCount);
            this.groupBox6.Controls.Add(this.button_memRead);
            this.groupBox6.Controls.Add(this.textBox_accesspwd_rw);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.rTB_read);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.button_memClear);
            this.groupBox6.Controls.Add(this.panel1);
            this.groupBox6.Controls.Add(this.panel2);
            this.groupBox6.Location = new System.Drawing.Point(471, 106);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(430, 307);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Read/Write";
            // 
            // rTB_write
            // 
            this.rTB_write.BackColor = System.Drawing.SystemColors.Control;
            this.rTB_write.Enabled = false;
            this.rTB_write.Location = new System.Drawing.Point(67, 215);
            this.rTB_write.Name = "rTB_write";
            this.rTB_write.Size = new System.Drawing.Size(300, 35);
            this.rTB_write.TabIndex = 23;
            this.rTB_write.Text = "";
            // 
            // rd_set2
            // 
            this.rd_set2.AutoSize = true;
            this.rd_set2.Location = new System.Drawing.Point(160, 138);
            this.rd_set2.Name = "rd_set2";
            this.rd_set2.Size = new System.Drawing.Size(47, 16);
            this.rd_set2.TabIndex = 21;
            this.rd_set2.Text = "대차";
            this.rd_set2.UseVisualStyleBackColor = true;
            // 
            // rd_set1
            // 
            this.rd_set1.AutoSize = true;
            this.rd_set1.Checked = true;
            this.rd_set1.Location = new System.Drawing.Point(67, 138);
            this.rd_set1.Name = "rd_set1";
            this.rd_set1.Size = new System.Drawing.Size(70, 16);
            this.rd_set1.TabIndex = 21;
            this.rd_set1.TabStop = true;
            this.rd_set1.Text = "PP Card";
            this.rd_set1.UseVisualStyleBackColor = true;
            this.rd_set1.CheckedChanged += new System.EventHandler(this.rd_set1_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(400, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 20;
            this.label3.Text = "36";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(389, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "/";
            // 
            // write_str_cnt
            // 
            this.write_str_cnt.AutoSize = true;
            this.write_str_cnt.Location = new System.Drawing.Point(373, 238);
            this.write_str_cnt.Name = "write_str_cnt";
            this.write_str_cnt.Size = new System.Drawing.Size(11, 12);
            this.write_str_cnt.TabIndex = 20;
            this.write_str_cnt.Text = "0";
            this.write_str_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(34, 283);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 23);
            this.button5.TabIndex = 19;
            this.button5.Text = "Tag Reset";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label22.Location = new System.Drawing.Point(290, 30);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 12);
            this.label22.TabIndex = 9;
            this.label22.Text = "AccessPWD";
            // 
            // button_memWrite
            // 
            this.button_memWrite.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_memWrite.Location = new System.Drawing.Point(292, 283);
            this.button_memWrite.Name = "button_memWrite";
            this.button_memWrite.Size = new System.Drawing.Size(75, 25);
            this.button_memWrite.TabIndex = 11;
            this.button_memWrite.Text = "Write";
            this.button_memWrite.UseVisualStyleBackColor = true;
            this.button_memWrite.Click += new System.EventHandler(this.button_memWrite_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(11, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "Write";
            // 
            // rTB_error
            // 
            this.rTB_error.Location = new System.Drawing.Point(67, 256);
            this.rTB_error.Name = "rTB_error";
            this.rTB_error.ReadOnly = true;
            this.rTB_error.Size = new System.Drawing.Size(300, 21);
            this.rTB_error.TabIndex = 18;
            this.rTB_error.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.Location = new System.Drawing.Point(9, 259);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 12);
            this.label18.TabIndex = 15;
            this.label18.Text = "Result";
            // 
            // comboBox_bank
            // 
            this.comboBox_bank.Enabled = false;
            this.comboBox_bank.FormattingEnabled = true;
            this.comboBox_bank.Location = new System.Drawing.Point(67, 48);
            this.comboBox_bank.Name = "comboBox_bank";
            this.comboBox_bank.Size = new System.Drawing.Size(90, 20);
            this.comboBox_bank.TabIndex = 16;
            // 
            // textBox_BlockOffset
            // 
            this.textBox_BlockOffset.Enabled = false;
            this.textBox_BlockOffset.Location = new System.Drawing.Point(167, 47);
            this.textBox_BlockOffset.Name = "textBox_BlockOffset";
            this.textBox_BlockOffset.Size = new System.Drawing.Size(50, 21);
            this.textBox_BlockOffset.TabIndex = 17;
            this.textBox_BlockOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.Location = new System.Drawing.Point(174, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 12);
            this.label17.TabIndex = 10;
            this.label17.Text = "Offset";
            // 
            // textBox_BlockCount
            // 
            this.textBox_BlockCount.Enabled = false;
            this.textBox_BlockCount.Location = new System.Drawing.Point(227, 47);
            this.textBox_BlockCount.Name = "textBox_BlockCount";
            this.textBox_BlockCount.Size = new System.Drawing.Size(50, 21);
            this.textBox_BlockCount.TabIndex = 17;
            this.textBox_BlockCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button_memRead
            // 
            this.button_memRead.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_memRead.Location = new System.Drawing.Point(207, 283);
            this.button_memRead.Name = "button_memRead";
            this.button_memRead.Size = new System.Drawing.Size(75, 25);
            this.button_memRead.TabIndex = 11;
            this.button_memRead.Text = "Read";
            this.button_memRead.UseVisualStyleBackColor = true;
            this.button_memRead.Click += new System.EventHandler(this.button_memRead_Click);
            // 
            // textBox_accesspwd_rw
            // 
            this.textBox_accesspwd_rw.Enabled = false;
            this.textBox_accesspwd_rw.Location = new System.Drawing.Point(286, 47);
            this.textBox_accesspwd_rw.Name = "textBox_accesspwd_rw";
            this.textBox_accesspwd_rw.Size = new System.Drawing.Size(80, 21);
            this.textBox_accesspwd_rw.TabIndex = 17;
            this.textBox_accesspwd_rw.Text = "00000000";
            this.textBox_accesspwd_rw.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(9, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 12);
            this.label19.TabIndex = 15;
            this.label19.Text = "Read";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.Location = new System.Drawing.Point(231, 30);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 12);
            this.label20.TabIndex = 9;
            this.label20.Text = "Length";
            // 
            // rTB_read
            // 
            this.rTB_read.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rTB_read.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rTB_read.Location = new System.Drawing.Point(67, 74);
            this.rTB_read.Name = "rTB_read";
            this.rTB_read.ReadOnly = true;
            this.rTB_read.Size = new System.Drawing.Size(300, 52);
            this.rTB_read.TabIndex = 14;
            this.rTB_read.Text = "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.Location = new System.Drawing.Point(72, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 12);
            this.label21.TabIndex = 10;
            this.label21.Text = "MemoryBank";
            // 
            // button_memClear
            // 
            this.button_memClear.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_memClear.Location = new System.Drawing.Point(122, 283);
            this.button_memClear.Name = "button_memClear";
            this.button_memClear.Size = new System.Drawing.Size(75, 25);
            this.button_memClear.TabIndex = 11;
            this.button_memClear.Text = "Clear";
            this.button_memClear.UseVisualStyleBackColor = true;
            this.button_memClear.Click += new System.EventHandler(this.button_memClear_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txt_size);
            this.panel1.Controls.Add(this.txt_pp_serno);
            this.panel1.Controls.Add(this.txt_it_scode);
            this.panel1.Controls.Add(this.txt_wc_code);
            this.panel1.Location = new System.Drawing.Point(67, 158);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 50);
            this.panel1.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(1, 27);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 21);
            this.label7.TabIndex = 1;
            this.label7.Text = "품목코드";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(212, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "수용수";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(109, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "PP Ser";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "작업장";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_size
            // 
            this.txt_size.Location = new System.Drawing.Point(264, 2);
            this.txt_size.Name = "txt_size";
            this.txt_size.Size = new System.Drawing.Size(33, 21);
            this.txt_size.TabIndex = 0;
            this.txt_size.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_wc_code_KeyUp);
            // 
            // txt_pp_serno
            // 
            this.txt_pp_serno.Location = new System.Drawing.Point(166, 2);
            this.txt_pp_serno.Name = "txt_pp_serno";
            this.txt_pp_serno.Size = new System.Drawing.Size(40, 21);
            this.txt_pp_serno.TabIndex = 0;
            this.txt_pp_serno.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_wc_code_KeyUp);
            // 
            // txt_it_scode
            // 
            this.txt_it_scode.BackColor = System.Drawing.SystemColors.Control;
            this.txt_it_scode.Enabled = false;
            this.txt_it_scode.Location = new System.Drawing.Point(55, 26);
            this.txt_it_scode.Name = "txt_it_scode";
            this.txt_it_scode.Size = new System.Drawing.Size(242, 21);
            this.txt_it_scode.TabIndex = 0;
            this.txt_it_scode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_wc_code_KeyUp);
            // 
            // txt_wc_code
            // 
            this.txt_wc_code.BackColor = System.Drawing.SystemColors.Control;
            this.txt_wc_code.Enabled = false;
            this.txt_wc_code.Location = new System.Drawing.Point(55, 2);
            this.txt_wc_code.Name = "txt_wc_code";
            this.txt_wc_code.Size = new System.Drawing.Size(52, 21);
            this.txt_wc_code.TabIndex = 0;
            this.txt_wc_code.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_wc_code_KeyUp);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txt_dc);
            this.panel2.Location = new System.Drawing.Point(67, 158);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 50);
            this.panel2.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "대차 No";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_dc
            // 
            this.txt_dc.Location = new System.Drawing.Point(68, 3);
            this.txt_dc.Name = "txt_dc";
            this.txt_dc.Size = new System.Drawing.Size(94, 21);
            this.txt_dc.TabIndex = 0;
            this.txt_dc.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_wc_code_KeyUp);
            // 
            // btn_clear_cnt
            // 
            this.btn_clear_cnt.Location = new System.Drawing.Point(220, 77);
            this.btn_clear_cnt.Name = "btn_clear_cnt";
            this.btn_clear_cnt.Size = new System.Drawing.Size(75, 23);
            this.btn_clear_cnt.TabIndex = 21;
            this.btn_clear_cnt.Text = "Clear";
            this.btn_clear_cnt.UseVisualStyleBackColor = true;
            this.btn_clear_cnt.Click += new System.EventHandler(this.btn_clear_cnt_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 30;
            this.dataGridView1.Location = new System.Drawing.Point(471, 419);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(430, 210);
            this.dataGridView1.TabIndex = 22;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19});
            this.dsmListView_ivt.HoverSelection = true;
            this.dsmListView_ivt.Location = new System.Drawing.Point(12, 106);
            this.dsmListView_ivt.MultiSelect = false;
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(453, 523);
            this.dsmListView_ivt.TabIndex = 11;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dsmListView_ivt_MouseClick);
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "NO.";
            this.columnHeader13.Width = 50;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "R/B";
            this.columnHeader14.Width = 38;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "TAG UID";
            this.columnHeader15.Width = 100;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "TAG TEXT";
            this.columnHeader16.Width = 180;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Counts";
            this.columnHeader17.Width = 80;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "RSSI";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Meters";
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(12, 106);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(410, 157);
            this.listView_target_list.TabIndex = 12;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(12, 41);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 65);
            this.ddc_ivt.TabIndex = 13;
            // 
            // button_inventory_stop
            // 
            this.button_inventory_stop.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_stop.Location = new System.Drawing.Point(381, 77);
            this.button_inventory_stop.Name = "button_inventory_stop";
            this.button_inventory_stop.Size = new System.Drawing.Size(75, 23);
            this.button_inventory_stop.TabIndex = 23;
            this.button_inventory_stop.Text = "Stop";
            this.button_inventory_stop.UseVisualStyleBackColor = true;
            this.button_inventory_stop.Click += new System.EventHandler(this.button_inventory_stop_Click);
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(301, 77);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 23);
            this.button_inventory_start.TabIndex = 24;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.ColumnHeadersHeight = 30;
            this.dataGridView2.Location = new System.Drawing.Point(907, 106);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(430, 523);
            this.dataGridView2.TabIndex = 22;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // cmbSiteCode
            // 
            this.cmbSiteCode.FormattingEnabled = true;
            this.cmbSiteCode.Location = new System.Drawing.Point(233, 133);
            this.cmbSiteCode.Name = "cmbSiteCode";
            this.cmbSiteCode.Size = new System.Drawing.Size(121, 20);
            this.cmbSiteCode.TabIndex = 24;
            // 
            // Tag_Writing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1345, 712);
            this.Controls.Add(this.button_inventory_stop);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_clear_cnt);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Name = "Tag_Writing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tag Writing";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private System.IO.Ports.SerialPort serialPort1;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private dsmListView listView_target_list;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button_memWrite;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rTB_error;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox_bank;
        private System.Windows.Forms.TextBox textBox_BlockOffset;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox_BlockCount;
        private System.Windows.Forms.Button button_memRead;
        private System.Windows.Forms.TextBox textBox_accesspwd_rw;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.RichTextBox rTB_read;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button_memClear;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label write_str_cnt;
        private System.Windows.Forms.Button btn_clear_cnt;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RadioButton rd_set2;
        private System.Windows.Forms.RadioButton rd_set1;
        private System.Windows.Forms.RichTextBox rTB_write;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_size;
        private System.Windows.Forms.TextBox txt_pp_serno;
        private System.Windows.Forms.TextBox txt_it_scode;
        private System.Windows.Forms.TextBox txt_wc_code;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_dc;
        private System.Windows.Forms.Button button_inventory_stop;
        private System.Windows.Forms.Button button_inventory_start;
        public System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ComboBox cmbSiteCode;
    }
}

