﻿namespace DK_Tablet
{
    partial class Loading
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Transfer));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.SITE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RSRV_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRDT_ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VW_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WC_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SYSIN_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MANIN_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R_START = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.R_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAIL_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARRIER_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARRIER_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_CHECK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GOOD_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REMAIN_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOVE_SQTY = new System.Windows.Forms.DataGridViewButtonColumn();
            this.MM_RDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtIt_scode = new System.Windows.Forms.TextBox();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txt_pp_card = new System.Windows.Forms.Button();
            this.txtAfter_carrier_no = new System.Windows.Forms.Button();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt_total_transfer_sqty = new DevExpress.XtraEditors.LabelControl();
            this.labelcontrols2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_transfer_commit = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btn_carrier_after = new System.Windows.Forms.Button();
            this.btn_carrier_before = new System.Windows.Forms.Button();
            this.btn_transfer_search = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.txtNext_Carrier_no = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCard_no = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIn_Carrier_no = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.timer_re_carrier = new System.Windows.Forms.Timer(this.components);
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClose = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.button1 = new System.Windows.Forms.Button();
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(91, 45);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(271, 40);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_open.Location = new System.Drawing.Point(369, 45);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.button_com_close.Location = new System.Drawing.Point(450, 45);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeight = 50;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BTN_DELETE,
            this.SITE_CODE,
            this.RSRV_NO,
            this.PRDT_ITEM,
            this.VW_SNUMB,
            this.WC_CODE,
            this.SYSIN_DATE,
            this.MANIN_DATE,
            this.R_START,
            this.R_END,
            this.LOT_NO,
            this.FAIL_SQTY,
            this.CARRIER_YN,
            this.CARRIER_NO,
            this.CARD_NO,
            this.END_CHECK,
            this.GOOD_SQTY,
            this.REMAIN_SQTY,
            this.MOVE_SQTY,
            this.MM_RDATE});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(64, 16);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 50;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 13F);
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowTemplate.Height = 50;
            this.dataGridView1.Size = new System.Drawing.Size(962, 438);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick_1);
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.HeaderText = "삭제";
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.ReadOnly = true;
            this.BTN_DELETE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BTN_DELETE.Text = "삭제";
            this.BTN_DELETE.UseColumnTextForButtonValue = true;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.DataPropertyName = "SITE_CODE";
            this.SITE_CODE.HeaderText = "공장코드";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.ReadOnly = true;
            this.SITE_CODE.Visible = false;
            // 
            // RSRV_NO
            // 
            this.RSRV_NO.DataPropertyName = "RSRV_NO";
            this.RSRV_NO.HeaderText = "계획번호";
            this.RSRV_NO.Name = "RSRV_NO";
            this.RSRV_NO.ReadOnly = true;
            this.RSRV_NO.Visible = false;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.DataPropertyName = "PRDT_ITEM";
            this.PRDT_ITEM.HeaderText = "품목";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.ReadOnly = true;
            this.PRDT_ITEM.Width = 200;
            // 
            // VW_SNUMB
            // 
            this.VW_SNUMB.DataPropertyName = "VW_SNUMB";
            this.VW_SNUMB.HeaderText = "가실적번호";
            this.VW_SNUMB.Name = "VW_SNUMB";
            this.VW_SNUMB.ReadOnly = true;
            this.VW_SNUMB.Visible = false;
            // 
            // WC_CODE
            // 
            this.WC_CODE.DataPropertyName = "WC_CODE";
            this.WC_CODE.HeaderText = "작업장코드";
            this.WC_CODE.Name = "WC_CODE";
            this.WC_CODE.ReadOnly = true;
            this.WC_CODE.Visible = false;
            // 
            // SYSIN_DATE
            // 
            this.SYSIN_DATE.DataPropertyName = "SYSIN_DATE";
            this.SYSIN_DATE.HeaderText = "등록일";
            this.SYSIN_DATE.Name = "SYSIN_DATE";
            this.SYSIN_DATE.ReadOnly = true;
            this.SYSIN_DATE.Visible = false;
            // 
            // MANIN_DATE
            // 
            this.MANIN_DATE.DataPropertyName = "MANIN_DATE";
            this.MANIN_DATE.HeaderText = "실적일";
            this.MANIN_DATE.Name = "MANIN_DATE";
            this.MANIN_DATE.ReadOnly = true;
            this.MANIN_DATE.Visible = false;
            // 
            // R_START
            // 
            this.R_START.DataPropertyName = "R_START";
            this.R_START.HeaderText = "시작시간";
            this.R_START.Name = "R_START";
            this.R_START.ReadOnly = true;
            this.R_START.Visible = false;
            // 
            // R_END
            // 
            this.R_END.DataPropertyName = "R_END";
            this.R_END.HeaderText = "종료시간";
            this.R_END.Name = "R_END";
            this.R_END.ReadOnly = true;
            this.R_END.Visible = false;
            // 
            // LOT_NO
            // 
            this.LOT_NO.DataPropertyName = "LOT_NO";
            this.LOT_NO.HeaderText = "LOT NO";
            this.LOT_NO.Name = "LOT_NO";
            this.LOT_NO.ReadOnly = true;
            this.LOT_NO.Visible = false;
            // 
            // FAIL_SQTY
            // 
            this.FAIL_SQTY.DataPropertyName = "FAIL_SQTY";
            this.FAIL_SQTY.HeaderText = "불량수량";
            this.FAIL_SQTY.Name = "FAIL_SQTY";
            this.FAIL_SQTY.ReadOnly = true;
            this.FAIL_SQTY.Visible = false;
            // 
            // CARRIER_YN
            // 
            this.CARRIER_YN.DataPropertyName = "CARRIER_YN";
            this.CARRIER_YN.HeaderText = "CARRIER_YN";
            this.CARRIER_YN.Name = "CARRIER_YN";
            this.CARRIER_YN.ReadOnly = true;
            this.CARRIER_YN.Visible = false;
            // 
            // CARRIER_NO
            // 
            this.CARRIER_NO.DataPropertyName = "CARRIER_NO";
            this.CARRIER_NO.HeaderText = "대차";
            this.CARRIER_NO.Name = "CARRIER_NO";
            this.CARRIER_NO.ReadOnly = true;
            this.CARRIER_NO.Width = 180;
            // 
            // CARD_NO
            // 
            this.CARD_NO.DataPropertyName = "CARD_NO";
            this.CARD_NO.HeaderText = "카드번호";
            this.CARD_NO.Name = "CARD_NO";
            this.CARD_NO.ReadOnly = true;
            this.CARD_NO.Width = 120;
            // 
            // END_CHECK
            // 
            this.END_CHECK.DataPropertyName = "END_CHECK";
            this.END_CHECK.HeaderText = "체크구분";
            this.END_CHECK.Name = "END_CHECK";
            this.END_CHECK.ReadOnly = true;
            this.END_CHECK.Visible = false;
            // 
            // GOOD_SQTY
            // 
            this.GOOD_SQTY.DataPropertyName = "GOOD_SQTY";
            this.GOOD_SQTY.HeaderText = "기존수량";
            this.GOOD_SQTY.Name = "GOOD_SQTY";
            this.GOOD_SQTY.ReadOnly = true;
            this.GOOD_SQTY.Width = 120;
            // 
            // REMAIN_SQTY
            // 
            this.REMAIN_SQTY.DataPropertyName = "REMAIN_SQTY";
            this.REMAIN_SQTY.HeaderText = "남은수량";
            this.REMAIN_SQTY.Name = "REMAIN_SQTY";
            this.REMAIN_SQTY.ReadOnly = true;
            this.REMAIN_SQTY.Width = 120;
            // 
            // MOVE_SQTY
            // 
            this.MOVE_SQTY.DataPropertyName = "MOVE_SQTY";
            this.MOVE_SQTY.HeaderText = "이적수량";
            this.MOVE_SQTY.Name = "MOVE_SQTY";
            this.MOVE_SQTY.ReadOnly = true;
            this.MOVE_SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.MOVE_SQTY.Width = 120;
            // 
            // MM_RDATE
            // 
            this.MM_RDATE.DataPropertyName = "MM_RDATE";
            this.MM_RDATE.HeaderText = "MM_RDATE";
            this.MM_RDATE.Name = "MM_RDATE";
            this.MM_RDATE.ReadOnly = true;
            this.MM_RDATE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.MM_RDATE.Visible = false;
            // 
            // txtIt_scode
            // 
            this.txtIt_scode.Location = new System.Drawing.Point(421, 94);
            this.txtIt_scode.Name = "txtIt_scode";
            this.txtIt_scode.Size = new System.Drawing.Size(94, 21);
            this.txtIt_scode.TabIndex = 2;
            this.txtIt_scode.Visible = false;
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(302, 32);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(12, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 39);
            this.label26.TabIndex = 11;
            this.label26.Text = "리더기";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Yellow;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txt_pp_card);
            this.panel2.Controls.Add(this.txtAfter_carrier_no);
            this.panel2.Controls.Add(this.labelControl3);
            this.panel2.Controls.Add(this.txt_total_transfer_sqty);
            this.panel2.Controls.Add(this.labelcontrols2);
            this.panel2.Controls.Add(this.labelControl1);
            this.panel2.Controls.Add(this.btn_transfer_commit);
            this.panel2.Controls.Add(this.labelControl2);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.btn_carrier_after);
            this.panel2.Controls.Add(this.btn_carrier_before);
            this.panel2.Controls.Add(this.btn_transfer_search);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Location = new System.Drawing.Point(12, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1271, 651);
            this.panel2.TabIndex = 18;
            // 
            // txt_pp_card
            // 
            this.txt_pp_card.Font = new System.Drawing.Font("굴림", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt_pp_card.Location = new System.Drawing.Point(1027, 284);
            this.txt_pp_card.Name = "txt_pp_card";
            this.txt_pp_card.Size = new System.Drawing.Size(237, 66);
            this.txt_pp_card.TabIndex = 31;
            this.txt_pp_card.UseVisualStyleBackColor = true;
            this.txt_pp_card.Click += new System.EventHandler(this.btn_pp_card_Click);
            // 
            // txtAfter_carrier_no
            // 
            this.txtAfter_carrier_no.Font = new System.Drawing.Font("굴림", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtAfter_carrier_no.Location = new System.Drawing.Point(1027, 147);
            this.txtAfter_carrier_no.Name = "txtAfter_carrier_no";
            this.txtAfter_carrier_no.Size = new System.Drawing.Size(237, 66);
            this.txtAfter_carrier_no.TabIndex = 31;
            this.txtAfter_carrier_no.UseVisualStyleBackColor = true;
            this.txtAfter_carrier_no.Click += new System.EventHandler(this.txtAfter_carrier_no_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelControl3.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl3.Location = new System.Drawing.Point(1027, 219);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(237, 59);
            this.labelControl3.TabIndex = 30;
            this.labelControl3.Text = "이적 카드";
            // 
            // txt_total_transfer_sqty
            // 
            this.txt_total_transfer_sqty.Appearance.BackColor = System.Drawing.Color.White;
            this.txt_total_transfer_sqty.Appearance.BackColor2 = System.Drawing.Color.DarkOrange;
            this.txt_total_transfer_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.txt_total_transfer_sqty.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.txt_total_transfer_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txt_total_transfer_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.txt_total_transfer_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.txt_total_transfer_sqty.Location = new System.Drawing.Point(1176, 17);
            this.txt_total_transfer_sqty.Name = "txt_total_transfer_sqty";
            this.txt_total_transfer_sqty.Size = new System.Drawing.Size(88, 59);
            this.txt_total_transfer_sqty.TabIndex = 30;
            this.txt_total_transfer_sqty.Text = "0";
            // 
            // labelcontrols2
            // 
            this.labelcontrols2.Appearance.BackColor = System.Drawing.Color.White;
            this.labelcontrols2.Appearance.BackColor2 = System.Drawing.Color.Bisque;
            this.labelcontrols2.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.labelcontrols2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelcontrols2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelcontrols2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelcontrols2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelcontrols2.Location = new System.Drawing.Point(1027, 17);
            this.labelcontrols2.Name = "labelcontrols2";
            this.labelcontrols2.Size = new System.Drawing.Size(143, 59);
            this.labelcontrols2.TabIndex = 30;
            this.labelcontrols2.Text = "이적수량";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl1.Location = new System.Drawing.Point(1027, 82);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(237, 59);
            this.labelControl1.TabIndex = 30;
            this.labelControl1.Text = "이적대차";
            // 
            // btn_transfer_commit
            // 
            this.btn_transfer_commit.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_transfer_commit.Appearance.Options.UseFont = true;
            this.btn_transfer_commit.Location = new System.Drawing.Point(1027, 356);
            this.btn_transfer_commit.Name = "btn_transfer_commit";
            this.btn_transfer_commit.Size = new System.Drawing.Size(237, 95);
            this.btn_transfer_commit.TabIndex = 29;
            this.btn_transfer_commit.Text = "이적 실행";
            this.btn_transfer_commit.Click += new System.EventHandler(this.btn_transfer_commit_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.GreenYellow;
            this.labelControl2.Appearance.BackColor2 = System.Drawing.Color.LightSteelBlue;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(14, 16);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(49, 435);
            this.labelControl2.TabIndex = 28;
            this.labelControl2.Text = "대\r\n\r\n상\r\n\r\n대\r\n\r\n차";
            // 
            // btn_carrier_after
            // 
            this.btn_carrier_after.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_carrier_after.BackgroundImage")));
            this.btn_carrier_after.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_carrier_after.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_carrier_after.Location = new System.Drawing.Point(1060, 492);
            this.btn_carrier_after.Margin = new System.Windows.Forms.Padding(1);
            this.btn_carrier_after.Name = "btn_carrier_after";
            this.btn_carrier_after.Size = new System.Drawing.Size(206, 156);
            this.btn_carrier_after.TabIndex = 0;
            this.btn_carrier_after.Text = "이적대차리딩";
            this.btn_carrier_after.UseVisualStyleBackColor = false;
            this.btn_carrier_after.Click += new System.EventHandler(this.btn_carrier_after_Click);
            // 
            // btn_carrier_before
            // 
            this.btn_carrier_before.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_carrier_before.BackgroundImage")));
            this.btn_carrier_before.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_carrier_before.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_carrier_before.Location = new System.Drawing.Point(14, 492);
            this.btn_carrier_before.Margin = new System.Windows.Forms.Padding(1);
            this.btn_carrier_before.Name = "btn_carrier_before";
            this.btn_carrier_before.Size = new System.Drawing.Size(206, 156);
            this.btn_carrier_before.TabIndex = 0;
            this.btn_carrier_before.Text = "대상대차리딩";
            this.btn_carrier_before.UseVisualStyleBackColor = false;
            this.btn_carrier_before.Click += new System.EventHandler(this.btn_carrier_before_Click);
            // 
            // btn_transfer_search
            // 
            this.btn_transfer_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_transfer_search.BackgroundImage")));
            this.btn_transfer_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_transfer_search.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_transfer_search.Location = new System.Drawing.Point(555, 492);
            this.btn_transfer_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_transfer_search.Name = "btn_transfer_search";
            this.btn_transfer_search.Size = new System.Drawing.Size(206, 156);
            this.btn_transfer_search.TabIndex = 0;
            this.btn_transfer_search.Text = "이적 대기현황";
            this.btn_transfer_search.UseVisualStyleBackColor = false;
            this.btn_transfer_search.Click += new System.EventHandler(this.btn_transfer_search_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.IndianRed;
            this.button11.Enabled = false;
            this.button11.Location = new System.Drawing.Point(13, 457);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(1253, 31);
            this.button11.TabIndex = 4;
            this.button11.UseVisualStyleBackColor = false;
            // 
            // txtNext_Carrier_no
            // 
            this.txtNext_Carrier_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtNext_Carrier_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtNext_Carrier_no.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.txtNext_Carrier_no.Location = new System.Drawing.Point(1082, 94);
            this.txtNext_Carrier_no.Name = "txtNext_Carrier_no";
            this.txtNext_Carrier_no.Size = new System.Drawing.Size(200, 70);
            this.txtNext_Carrier_no.TabIndex = 11;
            this.txtNext_Carrier_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtNext_Carrier_no.Visible = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(568, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 70);
            this.label3.TabIndex = 11;
            this.label3.Text = "입구카드";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Visible = false;
            // 
            // txtCard_no
            // 
            this.txtCard_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtCard_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtCard_no.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtCard_no.Location = new System.Drawing.Point(669, 94);
            this.txtCard_no.Name = "txtCard_no";
            this.txtCard_no.Size = new System.Drawing.Size(121, 70);
            this.txtCard_no.TabIndex = 11;
            this.txtCard_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtCard_no.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(796, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 70);
            this.label4.TabIndex = 11;
            this.label4.Text = "입구대차";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // txtIn_Carrier_no
            // 
            this.txtIn_Carrier_no.BackColor = System.Drawing.SystemColors.Info;
            this.txtIn_Carrier_no.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtIn_Carrier_no.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.txtIn_Carrier_no.Location = new System.Drawing.Point(897, 94);
            this.txtIn_Carrier_no.Name = "txtIn_Carrier_no";
            this.txtIn_Carrier_no.Size = new System.Drawing.Size(116, 69);
            this.txtIn_Carrier_no.TabIndex = 11;
            this.txtIn_Carrier_no.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtIn_Carrier_no.Visible = false;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(1014, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 71);
            this.label5.TabIndex = 11;
            this.label5.Text = "다 음\r\n대 차";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Visible = false;
            // 
            // timer_now
            // 
            this.timer_now.Interval = 15000;
            this.timer_now.Tick += new System.EventHandler(this.timer_now_Tick_1);
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(460, 12);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 20;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // timer_re_carrier
            // 
            this.timer_re_carrier.Interval = 30000;
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1085, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 81);
            this.btnClose.TabIndex = 17;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(193, 90);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(12, 87);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40});
            this.dsmListView_ivt.Location = new System.Drawing.Point(828, 45);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(29, 47);
            this.dsmListView_ivt.TabIndex = 0;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(669, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // Transfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1309, 758);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtIt_scode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.txtCard_no);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIn_Carrier_no);
            this.Controls.Add(this.txtNext_Carrier_no);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Transfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SubMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.SubMain_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private DK_Tablet.dsmListView dsmListView_ivt;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtIt_scode;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label txtNext_Carrier_no;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtCard_no;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtIn_Carrier_no;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer_now;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private System.Windows.Forms.Timer timer_re_carrier;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.Button btn_transfer_search;
        private System.Windows.Forms.Button btn_carrier_after;
        private System.Windows.Forms.Button btn_carrier_before;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_transfer_commit;
        private System.Windows.Forms.Button txt_pp_card;
        private System.Windows.Forms.Button txtAfter_carrier_no;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl txt_total_transfer_sqty;
        private DevExpress.XtraEditors.LabelControl labelcontrols2;
        private System.Windows.Forms.DataGridViewButtonColumn BTN_DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SITE_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn RSRV_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRDT_ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn VW_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn WC_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SYSIN_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn MANIN_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn R_START;
        private System.Windows.Forms.DataGridViewTextBoxColumn R_END;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAIL_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARRIER_YN;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARRIER_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn END_CHECK;
        private System.Windows.Forms.DataGridViewTextBoxColumn GOOD_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn REMAIN_SQTY;
        private System.Windows.Forms.DataGridViewButtonColumn MOVE_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn MM_RDATE;
    }
}

