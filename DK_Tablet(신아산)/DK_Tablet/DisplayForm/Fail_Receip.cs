﻿using DevExpress.XtraEditors;
using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using DK_Tablet.PRINT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class Fail_Receip : Form
    {
        GET_DATA GET_DATA = new GET_DATA();
        private SwingLibrary.SwingAPI Swing = null;  //스윙-u 사용하기 위한 API 객체 생성
        MAIN parentForm;
        FtpUtil ftpUtil;
        WebClient wc = new WebClient();
        string strCon = "";
        string plant = "";
        DataTable DT = null;
        public string fail_regno { get; set; }
        public string Pro_Regno { get; set; }
        public string it_scode { get; set; }
        public string lost_scode { get; set; }
        public string wc_code { get; set; }
        private int idx = 0;

        public string site_code = Properties.Settings.Default.SITE_CODE.ToString();

        public Fail_Receip(MAIN form)
        {
            this.parentForm = form;
            InitializeComponent();

         
        }

        private void FME_InsResultReg_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            this.strCon = Properties.Settings.Default.SQL_DKQT;
            plant = GET_DATA.getPlant(Properties.Settings.Default.SITE_CODE.ToString());
            lueWc_code_get();
            GetlueProPerson();
            getData();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void getData() 
        {
            string where = "AND ISNULL(A.PROCESSING_YN,'N') LIKE '%'+'" + radioGroup1.EditValue.ToString().Trim() + "'+'%'"
                        +  "AND A.FAIL_SDATE BETWEEN '" +dtp_Fail_Sdate.Value.ToString("yyyyMMdd  ")+ "' AND '" + dtp_Fail_Edate.Value.ToString("yyyyMMdd")+"' "
                        +  "AND ISNULL(A.WC_CODE,'') LIKE '%'+'" + lueWc_code.EditValue.ToString() + "'+'%'";

            SqlConnection conn = new SqlConnection(strCon);
            try
            {

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("USP_FAIL_RECEIP_PRS_SEARCH", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@WHERE", where);
                da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
                DataSet ds = new DataSet();
                da.Fill(ds, "fail_input_new");

                gridControl_1.DataSource = ds.Tables["fail_input_new"];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
           
        }

        private void radioGroup1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // getData();
        }

      

        private void gridView1_RowClick_1(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            setDetail();
        }
        private void setDetail()
        {
            if (gridView1.RowCount > 0)
            {
                fail_regno = gridView1.GetFocusedRowCellValue("FAIL_REGNO").ToString().Trim();
                it_scode = gridView1.GetFocusedRowCellValue("IT_SCODE").ToString().Trim();
                lost_scode = gridView1.GetFocusedRowCellValue("LOST_SCODE").ToString().Trim();

            }
            string sql = "SELECT DISTINCT A.FAIL_REGNO,A.IT_SCODE,B.IT_SNAME,A.LOST_SCODE,E.FT_NAME AS LOST_SNAME "
                        + "FROM FAIL_INPUT_NEW A "
                        + " LEFT JOIN IT_MASTER B "
                        + " ON A.IT_SCODE = B.IT_SCODE "
                        + " LEFT JOIN FAIL_TYPE E ON A.LOST_SCODE=E.NUMB "
                        + " WHERE ISNULL(FAIL_REGNO,'') != '' AND ISNULL(PROCESSING_YN,'N') = 'N' "
                        + " AND A.FAIL_REGNO = @FAIL_REGNO AND A.SITE_CODE = @SITE_CODE AND A.IT_SCODE = @IT_SCODE AND A.LOST_SCODE = @LOST_SCODE";

            SqlConnection conn = new SqlConnection(strCon);
            try
            {

                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.SelectCommand.CommandType = CommandType.Text;
                da.SelectCommand.Parameters.AddWithValue("@FAIL_REGNO", fail_regno);
                da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", site_code);
                da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", it_scode);
                da.SelectCommand.Parameters.AddWithValue("@LOST_SCODE", lost_scode);
                SqlDataReader reader = da.SelectCommand.ExecuteReader();

                while (reader.Read())
                {
                    getRowEmpty();
                    fpSpread1_Sheet1.SetText(idx, fpSpread1_Sheet1.Columns["FAIL_REGNO1"].Index, reader["FAIL_REGNO"].ToString().Trim());
                    fpSpread1_Sheet1.SetText(idx, fpSpread1_Sheet1.Columns["IT_SCODE"].Index, reader["IT_SCODE"].ToString().Trim());
                    fpSpread1_Sheet1.SetText(idx, fpSpread1_Sheet1.Columns["IT_SNAME"].Index, reader["IT_SNAME"].ToString().Trim());
                    fpSpread1_Sheet1.SetText(idx, fpSpread1_Sheet1.Columns["LOST_SCODE"].Index, reader["LOST_SCODE"].ToString().Trim());
                    fpSpread1_Sheet1.SetText(idx, fpSpread1_Sheet1.Columns["LOST_SNAME"].Index, reader["LOST_SNAME"].ToString().Trim());

                }
                reader.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private void getRowEmpty()
        {

            for (int i = 0; i < fpSpread1_Sheet1.RowCount; i++)
            {
                if (string.IsNullOrWhiteSpace(fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index)))
                {
                    idx = i;
                    break;
                }
            }
        }


        int row = 0;
   

        private void 행추가ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fpSpread1.Sheets[0].AddRows(row, 1);
        }

        private void 행삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int s_row;
            s_row = row;
            //메시지를 안띄우면 선택셀 데이터가 삭제됨
            DialogResult dr = MessageBox.Show((s_row + 1).ToString() + "행을 삭제 하시겠습니까?"
                                    , "알림", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                fpSpread1.Sheets[0].Rows.Remove(s_row, 1);
            }
        }
        private bool sum()
        {
            bool check = false;
            int proqty = 0;
            int failqty = 0;
            string it_scode = "";
            string lost_scode = "";
            string fail_regno = "";



            for (int i = 0; i < fpSpread1_Sheet1.RowCount; i++)
            {
                if (!string.IsNullOrWhiteSpace(fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index)))
                {
                    it_scode = fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index).ToString();
                    lost_scode = fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["LOST_SCODE"].Index).ToString();
                    fail_regno = fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["FAIL_REGNO1"].Index).ToString();

                    
                    proqty = sumProQty(it_scode, lost_scode, fail_regno);
                    failqty = sumfailQty(it_scode, fail_regno, lost_scode);

                    if (proqty - failqty != 0)
                    {
                        return false;
                    }

                }
            }
            return true;
        }
        private int sumfailQty(string it_scode, string fail_regno, string lost_scode)
        {
            string result = "";
            int qty = 0;
            string query = "SELECT SUM(FAIL_QTY) FROM FAIL_INPUT_NEW WHERE IT_SCODE = @IT_SCODE AND FAIL_REGNO = @FAIL_REGNO AND LOST_SCODE = @LOST_SCODE";
            //MessageBox.Show("strCon:"+strCon);
            SqlConnection conn = new SqlConnection(strCon);
            SqlDataReader reader = null;
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@FAIL_REGNO", fail_regno);
            cmd.Parameters.AddWithValue("@LOST_SCODE", lost_scode);
            try
            {

                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = reader[0].ToString().Trim();
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            qty = int.Parse(result);

            return qty;

        }
        private int sumProQty(string it_scode, string lost_scode,string fail_regno)
        {
            int sqty = 0;
            string qty = "";
            for (int i = 0; i < fpSpread1_Sheet1.RowCount; i++)
             {
                if (fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index).Equals(it_scode) && fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["LOST_SCODE"].Index).Equals(lost_scode)
                    && fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["FAIL_REGNO1"].Index).Equals(fail_regno))
                {
                    qty = fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PRO_QTY"].Index);
                 
                    sqty += int.Parse(isNull(fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PRO_QTY"].Index).ToString(), "0"));
                }
            }
            return sqty;
        }
        private string isNull(string str1, string str2)
        {
            string result = str1;
            if (string.IsNullOrWhiteSpace(str1))
            {
                return str2;
            }
            else if (str1.Trim() == "-")//마이너스 입력시 숫자 입력전에 합계를 계산하기 때문에 오류발생 하므로 0으로 처리 2014 10 30 김기윤
            {
                return "0";
            }
            return result;
        }
        private void fpSpread1_CellClick(object sender, FarPoint.Win.Spread.CellClickEventArgs e)
        {

            row = e.Row;
            if (e.Button == MouseButtons.Right)
            {
                contextMenuStrip1.Show(fpSpread1, e.X, e.Y);
            }
            else if (e.Button == MouseButtons.Left)
            {
                if (fpSpread1_Sheet1.ActiveColumnIndex == fpSpread1.Sheets[0].Columns["PRO_QTY"].Index)
                {

                    Sqty_popup Sqty_popup = new Sqty_popup();
                    Sqty_popup.sqty = "0";

                    fpSpread1_Sheet1.SetActiveCell(row, fpSpread1.Sheets[0].Columns["PRO_QTY"].Index);

                    if (Sqty_popup.ShowDialog() == DialogResult.OK)
                    {

                        fpSpread1_Sheet1.Cells[row, fpSpread1.Sheets[0].Columns["PRO_QTY"].Index].Text = Sqty_popup.sqty;
                    }
                }
                
            }
        }
        public void lueWc_code_get()
        {
            string strQury;
            strQury = "SELECT '' AS WC_CODE, '' AS WC_NAME UNION SELECT WC_CODE,WC_NAME FROM WORK_CENTER";

            SqlConnection conn = new SqlConnection(strCon);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_CENTER");
                DataTable dt = ds.Tables["WORK_CENTER"];
                lueWc_code.Properties.DataSource = dt;
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
        }
        public void GetlueProPerson()
        {
            
            string strQury;
            strQury = "SELECT S01,RTRIM(S02) AS 'S02' FROM TOP_COMM.DBO.TOP_USR WHERE S04 LIKE '%품질관리%' AND S00 = @S00";

            SqlConnection conn = new SqlConnection(strCon);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
         
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            da.SelectCommand.Parameters.AddWithValue("@S00", plant);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "TOP_COMM");
                DataTable dt = ds.Tables["TOP_COMM"];

                luePro.Properties.DataSource = dt;
                luePro.Properties.DisplayMember = "S02";
                luePro.Properties.ValueMember = "S01";
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
        }
        private void radioGroup1_MouseClick(object sender, MouseEventArgs e)
        {
            // getData();
        }

   
        private void gridControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            string column = gridView1.FocusedColumn.FieldName;

            if (column.Equals("폐기") || column.Equals("특채") || column.Equals("재작업") || column.Equals("반품"))
            {
                setSqty(column);
            }
            else
            {
                return;
            }

        }

        private void setSqty(string column)
        {
           int row = int.Parse(fpSpread1_Sheet1.ActiveRow.ToString());
          // if (fpSpread1_Sheet1.GetTag(row, fpSpread1_Sheet1.Columns["FAIL_QTY"].Index).ToString().Equals("FAIL_QTY"))
           if (fpSpread1_Sheet1.ActiveColumnIndex == fpSpread1.Sheets[0].Columns["it_sname"].Index)
            {

                Sqty_popup Sqty_popup = new Sqty_popup();
                Sqty_popup.sqty = "0";
                string sqty = "";
                int qty = 0;
                if (Sqty_popup.ShowDialog() == DialogResult.OK)
                {
                 
                    //sqty = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, column).ToString();
                  // qty = int.Parse(sqty) + int.Parse(Sqty_popup.sqty.ToString());
                    //gridView1.SetRowCellValue(gridView1.FocusedRowHandle, column, qty);
                    //gridView1.ShowEditor();
                    fpSpread1_Sheet1.Cells[fpSpread1_Sheet1.ActiveRowIndex, fpSpread1.Sheets[0].Columns["LOST_SNAME"].Index].Text = Sqty_popup.sqty;
                }
            }
        }
        private string getNewNumberToSave()
        {
            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_PRO_REG_NUM", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@PRO_REGNO", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string maxnum = paramMaxNum.Value.ToString();
                return maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }
        }
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(luePro.Text))
            {
                MessageBox.Show("담당자를 입력하세요");
                return;
            }
            if (!sum())
            {
                MessageBox.Show("픔목수량이 맞지 않습니다.");
                return;
            }

                if (MessageBox.Show("저장 하시겠습니까? ", "", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;
                this.Pro_Regno = getNewNumberToSave();    //저장하기 위해 새로운 번호 채번 
                saveData();
                //clearForm();
                getData();
              
          
        }
        private void clearForm()
        {
            Control[] controls = GetAllControls(this);
            foreach (Control control in controls)
            {
                if (control.GetType() == typeof(TextBox))
                    control.Text = "";
                else if (control.GetType() == typeof(CheckBox))
                    ((CheckBox)control).Checked = false;
                else if (control.GetType() == typeof(DevExpress.XtraEditors.DateEdit))
                    ((DevExpress.XtraEditors.DateEdit)control).EditValue = DateTime.Today;
                else if (control.GetType() == typeof(DevExpress.XtraEditors.TextEdit))
                    ((DevExpress.XtraEditors.TextEdit)control).EditValue = "";
                else if (control.GetType() == typeof(DevExpress.XtraEditors.LookUpEdit))
                    ((DevExpress.XtraEditors.LookUpEdit)control).EditValue = "";
                else if (control.GetType() == typeof(DevExpress.XtraEditors.ComboBoxEdit))
                    ((DevExpress.XtraEditors.ComboBoxEdit)control).SelectedIndex = 0;
            }
            //dataGridView1.Rows.Clear();
            //dataGridView1.Rows.Add(100);
            //dtpQmng_Occdate.Value = DateTime.Today;
            //dtpQmng_Regdate.Value = DateTime.Today;
           
            //dtpRdate.Value = DateTime.Now;
            //dtpSdate.Value = DateTime.Now;
        }
        private Control[] GetAllControls(Control containerControl)
        {
            List<Control> allControls = new List<Control>();
            Queue<Control.ControlCollection> queue = new Queue<Control.ControlCollection>();
            queue.Enqueue(containerControl.Controls);
            while (queue.Count > 0)
            {
                Control.ControlCollection controls
                = (Control.ControlCollection)queue.Dequeue();
                if (controls == null || controls.Count == 0)
                    continue;
                foreach (Control control in controls)
                {
                    allControls.Add(control);
                    queue.Enqueue(control.Controls);
                }
            }
            return allControls.ToArray();
        }
        private void saveData()
        {
            string sql = "UPDATE A SET PROCESSING_YN = 'N' FROM FAIL_INPUT_NEW A,FAIL_PROCESSING B "
                       + " WHERE A.FAIL_REGNO = B.FAIL_REGNO AND A.IT_SCODE = B.IT_SCODE AND B.SITE_CODE = @SITE_CODE AND B.PRO_REGNO = @PRO_REGNO";

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction tran = null;
            bool printAct = false;
            try
            {
                conn.Open();
                tran = conn.BeginTransaction();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Transaction = tran;
                cmd.Parameters.AddWithValue("@PRO_REGNO", Pro_Regno);
                cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
                cmd.ExecuteNonQuery();


                sql = "DELETE FROM FAIL_PROCESSING WHERE PRO_REGNO = @PRO_REGNO AND SITE_CODE = @SITE_CODE";
                cmd.CommandText = sql;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@PRO_REGNO", Pro_Regno);
                cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
                cmd.ExecuteNonQuery();

                sql = " INSERT INTO FAIL_PROCESSING(SITE_CODE,PRO_REGNO,FAIL_REGNO,SERNO,PRO_RDATE,PRO_SDATE,PRO_PERSON,IT_SCODE,LOST_SCODE,PROCESSING_TYPE,PRO_QTY) "
                      + " VALUES(@SITE_CODE,@PRO_REGNO,@FAIL_REGNO,@SERNO,@PRO_RDATE,@PRO_SDATE,@PRO_PERSON,@IT_SCODE,@LOST_SCODE,@PROCESSING_TYPE,@PRO_QTY) ";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;

                int serno = 1;

                for (int i = 0; i < fpSpread1_Sheet1.RowCount; i++)
                {
                    if (!string.IsNullOrWhiteSpace(fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index)))
                    {
                        if (string.IsNullOrWhiteSpace(fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PROCESSING_TYPE"].Index)))
                        {
                            MessageBox.Show("처리유형을 입력 해주세요");
                            return;
                        }

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@SITE_CODE", this.site_code);
                        cmd.Parameters.AddWithValue("@PRO_REGNO", Pro_Regno);
                        cmd.Parameters.AddWithValue("@FAIL_REGNO", fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["FAIL_REGNO1"].Index));
                        cmd.Parameters.AddWithValue("@SERNO", serno);
                        cmd.Parameters.AddWithValue("@PRO_RDATE", DateTime.Now.ToString("yyyyMMdd"));
                        cmd.Parameters.AddWithValue("@PRO_SDATE", DateTime.Now.ToString("yyyyMMdd"));
                        cmd.Parameters.AddWithValue("@PRO_PERSON", luePro.EditValue.ToString());
                        cmd.Parameters.AddWithValue("@IT_SCODE", fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["IT_SCODE"].Index));
                        cmd.Parameters.AddWithValue("@LOST_SCODE", fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["LOST_SCODE"].Index));
                        cmd.Parameters.AddWithValue("@PROCESSING_TYPE", fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PROCESSING_TYPE"].Index));
                        cmd.Parameters.AddWithValue("@PRO_QTY", fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PRO_QTY"].Index));
                        cmd.ExecuteNonQuery();
                        serno++;
                        printAct = (fpSpread1_Sheet1.GetText(i, fpSpread1_Sheet1.Columns["PROCESSING_TYPE"].Index) == "반품");
                    }
                }

                sql = "USP_PRO_PROCESSING_REG";
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@PRO_REGNO", Pro_Regno);
                cmd.Parameters.AddWithValue("@SITE_CODE", this.site_code);
                cmd.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("저장완료");
            
                fpSpread1_Sheet1.ClearRange(0, 0, fpSpread1.ActiveSheet.RowCount, fpSpread1.ActiveSheet.ColumnCount, true);
                idx = 0;
            }
            catch (Exception e)
            {
                tran.Rollback();
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
                if (printAct) fail_Print.Print(Pro_Regno, luePro.Text);
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            getData();
        }
       
    }
}

