namespace DK_Tablet
{
    partial class IP_Settings_Popup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtip_1 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_2 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_4 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_5 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_6 = new DevExpress.XtraEditors.TextEdit();
            this.txtip_7 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_1 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_2 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_4 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_5 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_6 = new DevExpress.XtraEditors.TextEdit();
            this.txtport_7 = new DevExpress.XtraEditors.TextEdit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_7.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtip_7);
            this.groupBox1.Controls.Add(this.txtip_6);
            this.groupBox1.Controls.Add(this.txtip_5);
            this.groupBox1.Controls.Add(this.txtip_4);
            this.groupBox1.Controls.Add(this.textEdit4);
            this.groupBox1.Controls.Add(this.txtip_3);
            this.groupBox1.Controls.Add(this.txtip_2);
            this.groupBox1.Controls.Add(this.txtip_1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 182);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 131);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "IP6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "IP4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "IP5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "IP3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "IP2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtport_7);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtport_6);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtport_5);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtport_4);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textEdit12);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txtport_3);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtport_2);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtport_1);
            this.groupBox2.Location = new System.Drawing.Point(207, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 183);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PORT";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(425, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 21);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(425, 57);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 21);
            this.button2.TabIndex = 3;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "IP7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "PORT1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 12);
            this.label9.TabIndex = 2;
            this.label9.Text = "PORT2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 12);
            this.label10.TabIndex = 3;
            this.label10.Text = "PORT3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 12);
            this.label11.TabIndex = 4;
            this.label11.Text = "PORT5";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 12);
            this.label12.TabIndex = 5;
            this.label12.Text = "PORT4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 12);
            this.label13.TabIndex = 6;
            this.label13.Text = "PORT6";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 12);
            this.label14.TabIndex = 6;
            this.label14.Text = "PORT7";
            // 
            // txtip_1
            // 
            this.txtip_1.Location = new System.Drawing.Point(34, 16);
            this.txtip_1.Name = "txtip_1";
            this.txtip_1.Size = new System.Drawing.Size(140, 20);
            this.txtip_1.TabIndex = 7;
            // 
            // txtip_2
            // 
            this.txtip_2.Location = new System.Drawing.Point(34, 38);
            this.txtip_2.Name = "txtip_2";
            this.txtip_2.Size = new System.Drawing.Size(140, 20);
            this.txtip_2.TabIndex = 7;
            // 
            // txtip_3
            // 
            this.txtip_3.Location = new System.Drawing.Point(34, 60);
            this.txtip_3.Name = "txtip_3";
            this.txtip_3.Size = new System.Drawing.Size(140, 20);
            this.txtip_3.TabIndex = 7;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(34, 84);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(140, 20);
            this.textEdit4.TabIndex = 7;
            // 
            // txtip_4
            // 
            this.txtip_4.Location = new System.Drawing.Point(34, 82);
            this.txtip_4.Name = "txtip_4";
            this.txtip_4.Size = new System.Drawing.Size(140, 20);
            this.txtip_4.TabIndex = 7;
            // 
            // txtip_5
            // 
            this.txtip_5.Location = new System.Drawing.Point(34, 104);
            this.txtip_5.Name = "txtip_5";
            this.txtip_5.Size = new System.Drawing.Size(140, 20);
            this.txtip_5.TabIndex = 7;
            // 
            // txtip_6
            // 
            this.txtip_6.Location = new System.Drawing.Point(34, 126);
            this.txtip_6.Name = "txtip_6";
            this.txtip_6.Size = new System.Drawing.Size(140, 20);
            this.txtip_6.TabIndex = 7;
            // 
            // txtip_7
            // 
            this.txtip_7.Location = new System.Drawing.Point(34, 148);
            this.txtip_7.Name = "txtip_7";
            this.txtip_7.Size = new System.Drawing.Size(140, 20);
            this.txtip_7.TabIndex = 7;
            // 
            // txtport_1
            // 
            this.txtport_1.Location = new System.Drawing.Point(56, 17);
            this.txtport_1.Name = "txtport_1";
            this.txtport_1.Size = new System.Drawing.Size(140, 20);
            this.txtport_1.TabIndex = 7;
            // 
            // txtport_2
            // 
            this.txtport_2.Location = new System.Drawing.Point(56, 39);
            this.txtport_2.Name = "txtport_2";
            this.txtport_2.Size = new System.Drawing.Size(140, 20);
            this.txtport_2.TabIndex = 7;
            // 
            // txtport_3
            // 
            this.txtport_3.Location = new System.Drawing.Point(56, 61);
            this.txtport_3.Name = "txtport_3";
            this.txtport_3.Size = new System.Drawing.Size(140, 20);
            this.txtport_3.TabIndex = 7;
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(56, 89);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(140, 20);
            this.textEdit12.TabIndex = 7;
            // 
            // txtport_4
            // 
            this.txtport_4.Location = new System.Drawing.Point(56, 83);
            this.txtport_4.Name = "txtport_4";
            this.txtport_4.Size = new System.Drawing.Size(140, 20);
            this.txtport_4.TabIndex = 7;
            // 
            // txtport_5
            // 
            this.txtport_5.Location = new System.Drawing.Point(56, 105);
            this.txtport_5.Name = "txtport_5";
            this.txtport_5.Size = new System.Drawing.Size(140, 20);
            this.txtport_5.TabIndex = 7;
            // 
            // txtport_6
            // 
            this.txtport_6.Location = new System.Drawing.Point(56, 127);
            this.txtport_6.Name = "txtport_6";
            this.txtport_6.Size = new System.Drawing.Size(140, 20);
            this.txtport_6.TabIndex = 7;
            // 
            // txtport_7
            // 
            this.txtport_7.Location = new System.Drawing.Point(56, 149);
            this.txtport_7.Name = "txtport_7";
            this.txtport_7.Size = new System.Drawing.Size(140, 20);
            this.txtport_7.TabIndex = 7;
            // 
            // IP_Settings_Popup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(525, 207);
            this.ControlBox = false;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "IP_Settings_Popup";
            this.Text = "IP & PORT Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtip_7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtport_7.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtip_7;
        private DevExpress.XtraEditors.TextEdit txtip_6;
        private DevExpress.XtraEditors.TextEdit txtip_5;
        private DevExpress.XtraEditors.TextEdit txtip_4;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit txtip_3;
        private DevExpress.XtraEditors.TextEdit txtip_2;
        private DevExpress.XtraEditors.TextEdit txtip_1;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtport_7;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.TextEdit txtport_6;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.TextEdit txtport_5;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.TextEdit txtport_4;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtport_3;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit txtport_2;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.TextEdit txtport_1;
    }
}