﻿namespace DK_Tablet
{
    partial class FME_InsResultReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FME_InsResultReg));
            this.INS_VALUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueWC_CODE = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.CF_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CF_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INS_CONTENT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.DEFAULT_VALUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VALUE_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DEFAULT_VALUE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DEFAULT_VALUE2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DEFAULT_MIN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DEFAULT_MAX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INS_MANAGER = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INS_METHOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SERNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lueDAY_NIGHT_GUBN = new DevExpress.XtraEditors.LookUpEdit();
            this.txtIT_SNAME = new DevExpress.XtraEditors.TextEdit();
            this.txtIT_SCODE = new DevExpress.XtraEditors.TextEdit();
            this.teINS_RTIME = new DevExpress.XtraEditors.TimeEdit();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lueFME_GUBN = new DevExpress.XtraEditors.LookUpEdit();
            this.deINS_RDATE = new DevExpress.XtraEditors.DateEdit();
            this.btnAttach = new System.Windows.Forms.Button();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            ((System.ComponentModel.ISupportInitialize)(this.lueWC_CODE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDAY_NIGHT_GUBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIT_SNAME.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIT_SCODE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teINS_RTIME.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFME_GUBN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deINS_RDATE.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deINS_RDATE.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // INS_VALUE
            // 
            this.INS_VALUE.AppearanceCell.Options.UseTextOptions = true;
            this.INS_VALUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.INS_VALUE.Caption = "검사값";
            this.INS_VALUE.FieldName = "INS_VALUE";
            this.INS_VALUE.Name = "INS_VALUE";
            this.INS_VALUE.Visible = true;
            this.INS_VALUE.VisibleIndex = 3;
            this.INS_VALUE.Width = 86;
            // 
            // lueWC_CODE
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.lueWC_CODE, 2);
            this.lueWC_CODE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lueWC_CODE.Location = new System.Drawing.Point(3, 3);
            this.lueWC_CODE.Name = "lueWC_CODE";
            this.lueWC_CODE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lueWC_CODE.Properties.Appearance.Options.UseFont = true;
            this.lueWC_CODE.Properties.Appearance.Options.UseTextOptions = true;
            this.lueWC_CODE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lueWC_CODE.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWC_CODE.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWC_CODE.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWC_CODE.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWC_CODE.Properties.AutoHeight = false;
            this.lueWC_CODE.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWC_CODE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWC_CODE.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", 80, "명")});
            this.lueWC_CODE.Properties.DropDownRows = 5;
            this.lueWC_CODE.Properties.NullText = "";
            this.lueWC_CODE.Properties.PopupWidth = 500;
            this.lueWC_CODE.Size = new System.Drawing.Size(366, 59);
            this.lueWC_CODE.TabIndex = 0;
            this.lueWC_CODE.EditValueChanged += new System.EventHandler(this.lueWC_CODE_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(0);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1});
            this.gridControl1.Size = new System.Drawing.Size(466, 555);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("굴림", 12F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("굴림", 13F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.CF_CODE,
            this.CF_NAME,
            this.INS_CONTENT,
            this.DEFAULT_VALUE,
            this.VALUE_TYPE,
            this.DEFAULT_VALUE1,
            this.DEFAULT_VALUE2,
            this.DEFAULT_MIN,
            this.DEFAULT_MAX,
            this.INS_MANAGER,
            this.INS_METHOD,
            this.INS_VALUE,
            this.SERNO});
            gridFormatRule1.Column = this.INS_VALUE;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            formatConditionRuleValue1.Appearance.Options.HighPriority = true;
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Expression;
            formatConditionRuleValue1.Expression = resources.GetString("formatConditionRuleValue1.Expression");
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.GroupFormat = "{1} {2}";
            this.gridView1.GroupRowHeight = 25;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowColumnResizing = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsMenu.EnableColumnMenu = false;
            this.gridView1.OptionsPrint.ExpandAllDetails = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.CF_NAME, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // CF_CODE
            // 
            this.CF_CODE.AppearanceCell.Options.UseTextOptions = true;
            this.CF_CODE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CF_CODE.Caption = "검사구분";
            this.CF_CODE.FieldName = "CF_CODE";
            this.CF_CODE.Name = "CF_CODE";
            // 
            // CF_NAME
            // 
            this.CF_NAME.Caption = "검사구분";
            this.CF_NAME.FieldName = "CF_NAME";
            this.CF_NAME.Name = "CF_NAME";
            this.CF_NAME.Visible = true;
            this.CF_NAME.VisibleIndex = 0;
            this.CF_NAME.Width = 150;
            // 
            // INS_CONTENT
            // 
            this.INS_CONTENT.Caption = "검사기준";
            this.INS_CONTENT.ColumnEdit = this.repositoryItemMemoEdit1;
            this.INS_CONTENT.FieldName = "INS_CONTENT";
            this.INS_CONTENT.Name = "INS_CONTENT";
            this.INS_CONTENT.Visible = true;
            this.INS_CONTENT.VisibleIndex = 0;
            this.INS_CONTENT.Width = 197;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemMemoEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemMemoEdit1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            // 
            // DEFAULT_VALUE
            // 
            this.DEFAULT_VALUE.AppearanceCell.Options.UseTextOptions = true;
            this.DEFAULT_VALUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DEFAULT_VALUE.Caption = "기준값";
            this.DEFAULT_VALUE.FieldName = "DEFAULT_VALUE";
            this.DEFAULT_VALUE.Name = "DEFAULT_VALUE";
            this.DEFAULT_VALUE.Width = 192;
            // 
            // VALUE_TYPE
            // 
            this.VALUE_TYPE.Caption = "타입";
            this.VALUE_TYPE.FieldName = "VALUE_TYPE";
            this.VALUE_TYPE.Name = "VALUE_TYPE";
            // 
            // DEFAULT_VALUE1
            // 
            this.DEFAULT_VALUE1.Caption = "기준값1";
            this.DEFAULT_VALUE1.FieldName = "DEFAULT_VALUE1";
            this.DEFAULT_VALUE1.Name = "DEFAULT_VALUE1";
            // 
            // DEFAULT_VALUE2
            // 
            this.DEFAULT_VALUE2.Caption = "기준값2";
            this.DEFAULT_VALUE2.FieldName = "DEFAULT_VALUE2";
            this.DEFAULT_VALUE2.Name = "DEFAULT_VALUE2";
            // 
            // DEFAULT_MIN
            // 
            this.DEFAULT_MIN.Caption = "하한";
            this.DEFAULT_MIN.FieldName = "DEFAULT_MIN";
            this.DEFAULT_MIN.Name = "DEFAULT_MIN";
            // 
            // DEFAULT_MAX
            // 
            this.DEFAULT_MAX.Caption = "상한";
            this.DEFAULT_MAX.FieldName = "DEFAULT_MAX";
            this.DEFAULT_MAX.Name = "DEFAULT_MAX";
            // 
            // INS_MANAGER
            // 
            this.INS_MANAGER.AppearanceCell.Options.UseTextOptions = true;
            this.INS_MANAGER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.INS_MANAGER.Caption = "검사자";
            this.INS_MANAGER.FieldName = "INS_MANAGER";
            this.INS_MANAGER.Name = "INS_MANAGER";
            this.INS_MANAGER.Visible = true;
            this.INS_MANAGER.VisibleIndex = 1;
            this.INS_MANAGER.Width = 82;
            // 
            // INS_METHOD
            // 
            this.INS_METHOD.AppearanceCell.Options.UseTextOptions = true;
            this.INS_METHOD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.INS_METHOD.Caption = "검사방법";
            this.INS_METHOD.FieldName = "INS_METHOD";
            this.INS_METHOD.Name = "INS_METHOD";
            this.INS_METHOD.Visible = true;
            this.INS_METHOD.VisibleIndex = 2;
            this.INS_METHOD.Width = 73;
            // 
            // SERNO
            // 
            this.SERNO.Caption = "순번";
            this.SERNO.FieldName = "SERNO";
            this.SERNO.Name = "SERNO";
            // 
            // lueDAY_NIGHT_GUBN
            // 
            this.lueDAY_NIGHT_GUBN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lueDAY_NIGHT_GUBN.Location = new System.Drawing.Point(747, 68);
            this.lueDAY_NIGHT_GUBN.Name = "lueDAY_NIGHT_GUBN";
            this.lueDAY_NIGHT_GUBN.Properties.Appearance.Font = new System.Drawing.Font("굴림", 15F);
            this.lueDAY_NIGHT_GUBN.Properties.Appearance.Options.UseFont = true;
            this.lueDAY_NIGHT_GUBN.Properties.Appearance.Options.UseTextOptions = true;
            this.lueDAY_NIGHT_GUBN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lueDAY_NIGHT_GUBN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("굴림", 20F);
            this.lueDAY_NIGHT_GUBN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueDAY_NIGHT_GUBN.Properties.AutoHeight = false;
            this.lueDAY_NIGHT_GUBN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDAY_NIGHT_GUBN.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "주/야"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Key", "Key", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lueDAY_NIGHT_GUBN.Properties.DropDownRows = 3;
            this.lueDAY_NIGHT_GUBN.Properties.NullText = "";
            this.lueDAY_NIGHT_GUBN.Size = new System.Drawing.Size(182, 29);
            this.lueDAY_NIGHT_GUBN.TabIndex = 25;
            this.lueDAY_NIGHT_GUBN.EditValueChanged += new System.EventHandler(this.EditValueChanged);
            // 
            // txtIT_SNAME
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtIT_SNAME, 3);
            this.txtIT_SNAME.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIT_SNAME.Location = new System.Drawing.Point(189, 68);
            this.txtIT_SNAME.Name = "txtIT_SNAME";
            this.txtIT_SNAME.Properties.Appearance.Font = new System.Drawing.Font("굴림", 18F);
            this.txtIT_SNAME.Properties.Appearance.Options.UseFont = true;
            this.txtIT_SNAME.Properties.AutoHeight = false;
            this.txtIT_SNAME.Properties.ReadOnly = true;
            this.txtIT_SNAME.Size = new System.Drawing.Size(552, 29);
            this.txtIT_SNAME.TabIndex = 24;
            this.txtIT_SNAME.Click += new System.EventHandler(this.txtIT_SCODE_Click);
            // 
            // txtIT_SCODE
            // 
            this.txtIT_SCODE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIT_SCODE.Location = new System.Drawing.Point(3, 68);
            this.txtIT_SCODE.Name = "txtIT_SCODE";
            this.txtIT_SCODE.Properties.Appearance.Font = new System.Drawing.Font("굴림", 18F);
            this.txtIT_SCODE.Properties.Appearance.Options.UseFont = true;
            this.txtIT_SCODE.Properties.AutoHeight = false;
            this.txtIT_SCODE.Properties.ReadOnly = true;
            this.txtIT_SCODE.Size = new System.Drawing.Size(180, 29);
            this.txtIT_SCODE.TabIndex = 23;
            this.txtIT_SCODE.Click += new System.EventHandler(this.txtIT_SCODE_Click);
            // 
            // teINS_RTIME
            // 
            this.teINS_RTIME.Dock = System.Windows.Forms.DockStyle.Fill;
            this.teINS_RTIME.EditValue = new System.DateTime(2015, 11, 16, 0, 0, 0, 0);
            this.teINS_RTIME.Location = new System.Drawing.Point(561, 3);
            this.teINS_RTIME.Name = "teINS_RTIME";
            this.teINS_RTIME.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.teINS_RTIME.Properties.Appearance.Options.UseFont = true;
            this.teINS_RTIME.Properties.Appearance.Options.UseTextOptions = true;
            this.teINS_RTIME.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teINS_RTIME.Properties.AutoHeight = false;
            this.teINS_RTIME.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teINS_RTIME.Properties.Mask.EditMask = "HH:mm";
            this.teINS_RTIME.Size = new System.Drawing.Size(180, 59);
            this.teINS_RTIME.TabIndex = 18;
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Font = new System.Drawing.Font("굴림", 22F);
            this.btnSave.Location = new System.Drawing.Point(1, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(172, 100);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(173, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(172, 100);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lueFME_GUBN
            // 
            this.lueFME_GUBN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lueFME_GUBN.Location = new System.Drawing.Point(747, 3);
            this.lueFME_GUBN.Name = "lueFME_GUBN";
            this.lueFME_GUBN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lueFME_GUBN.Properties.Appearance.Options.UseFont = true;
            this.lueFME_GUBN.Properties.Appearance.Options.UseTextOptions = true;
            this.lueFME_GUBN.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lueFME_GUBN.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lueFME_GUBN.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueFME_GUBN.Properties.AutoHeight = false;
            this.lueFME_GUBN.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueFME_GUBN.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "초/중/종"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Key", "Key", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default)});
            this.lueFME_GUBN.Properties.DropDownRows = 3;
            this.lueFME_GUBN.Properties.NullText = "";
            this.lueFME_GUBN.Size = new System.Drawing.Size(182, 59);
            this.lueFME_GUBN.TabIndex = 3;
            this.lueFME_GUBN.EditValueChanged += new System.EventHandler(this.EditValueChanged);
            // 
            // deINS_RDATE
            // 
            this.deINS_RDATE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deINS_RDATE.EditValue = null;
            this.deINS_RDATE.Location = new System.Drawing.Point(375, 3);
            this.deINS_RDATE.Name = "deINS_RDATE";
            this.deINS_RDATE.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 32F);
            this.deINS_RDATE.Properties.Appearance.Options.UseFont = true;
            this.deINS_RDATE.Properties.Appearance.Options.UseTextOptions = true;
            this.deINS_RDATE.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deINS_RDATE.Properties.AutoHeight = false;
            this.deINS_RDATE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deINS_RDATE.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deINS_RDATE.Properties.CalendarTimeProperties.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.deINS_RDATE.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.TouchUI;
            this.deINS_RDATE.Properties.ReadOnly = true;
            this.deINS_RDATE.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.deINS_RDATE.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.deINS_RDATE.Size = new System.Drawing.Size(180, 59);
            this.deINS_RDATE.TabIndex = 0;
            this.deINS_RDATE.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.deINS_RDATE_QueryPopUp);
            // 
            // btnAttach
            // 
            this.btnAttach.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btnAttach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAttach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAttach.Font = new System.Drawing.Font("굴림", 22F);
            this.btnAttach.Location = new System.Drawing.Point(0, 0);
            this.btnAttach.Name = "btnAttach";
            this.btnAttach.Size = new System.Drawing.Size(466, 85);
            this.btnAttach.TabIndex = 17;
            this.btnAttach.Text = "첨 부 파 일";
            this.btnAttach.UseVisualStyleBackColor = true;
            this.btnAttach.Click += new System.EventHandler(this.btnAttach_Click);
            // 
            // imageSlider1
            // 
            this.imageSlider1.AllowLooping = true;
            this.imageSlider1.ContextButtonOptions.AnimationType = DevExpress.Utils.ContextAnimationType.OutAnimation;
            this.imageSlider1.CurrentImageIndex = -1;
            this.imageSlider1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageSlider1.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.Stretch;
            this.imageSlider1.Location = new System.Drawing.Point(0, 0);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(814, 645);
            this.imageSlider1.TabIndex = 2;
            this.imageSlider1.Text = "imageSlider1";
            this.imageSlider1.UseDisabledStatePainter = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(5, 5);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1285, 750);
            this.splitContainerControl1.TabIndex = 26;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.IsSplitterFixed = true;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.btnClose);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnSave);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1285, 100);
            this.splitContainerControl2.SplitterPosition = 348;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.lueWC_CODE, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lueDAY_NIGHT_GUBN, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.deINS_RDATE, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.teINS_RTIME, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtIT_SNAME, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lueFME_GUBN, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtIT_SCODE, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(932, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.IsSplitterFixed = true;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.imageSlider1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1285, 645);
            this.splitContainerControl3.SplitterPosition = 466;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.IsSplitterFixed = true;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.btnAttach);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(466, 645);
            this.splitContainerControl4.SplitterPosition = 85;
            this.splitContainerControl4.TabIndex = 0;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // FME_InsResultReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FME_InsResultReg";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "FME_InsResultReg";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FME_InsResultReg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lueWC_CODE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDAY_NIGHT_GUBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIT_SNAME.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIT_SCODE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teINS_RTIME.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFME_GUBN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deINS_RDATE.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deINS_RDATE.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lueWC_CODE;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.DateEdit deINS_RDATE;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LookUpEdit lueFME_GUBN;
        private DevExpress.XtraGrid.Columns.GridColumn CF_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn CF_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn INS_CONTENT;
        private DevExpress.XtraGrid.Columns.GridColumn DEFAULT_VALUE;
        private DevExpress.XtraGrid.Columns.GridColumn VALUE_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn DEFAULT_VALUE1;
        private DevExpress.XtraGrid.Columns.GridColumn DEFAULT_VALUE2;
        private DevExpress.XtraGrid.Columns.GridColumn DEFAULT_MIN;
        private DevExpress.XtraGrid.Columns.GridColumn DEFAULT_MAX;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraEditors.TimeEdit teINS_RTIME;
        private DevExpress.XtraEditors.TextEdit txtIT_SCODE;
        private DevExpress.XtraEditors.TextEdit txtIT_SNAME;
        private DevExpress.XtraEditors.LookUpEdit lueDAY_NIGHT_GUBN;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private DevExpress.XtraGrid.Columns.GridColumn INS_MANAGER;
        private DevExpress.XtraGrid.Columns.GridColumn INS_METHOD;
        private DevExpress.XtraGrid.Columns.GridColumn INS_VALUE;
        private DevExpress.XtraGrid.Columns.GridColumn SERNO;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private System.Windows.Forms.Button btnAttach;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
    }
}