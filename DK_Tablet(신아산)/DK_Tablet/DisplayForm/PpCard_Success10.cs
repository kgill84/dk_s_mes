﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.DisplayForm
{
    public partial class PpCard_Success10 : Form
    {
        public string pp_card_no { get; set; }
        public PpCard_Success10()
        {
            InitializeComponent();
        }

        private void Injection_Success_Load(object sender, EventArgs e)
        {
            
        }
        int cnt = 11;
        private void timer1_Tick(object sender, EventArgs e)
        {
            cnt = cnt - 1;
            if (cnt == -1)
            {
                timer1.Stop();
                this.Visible = false;
            }
            else
            {
                label2.Text = cnt.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.Visible = false;
        }
        public void set_text(string pp_card_no,int cnt)
        {
            this.cnt = cnt;
            label1.Text = pp_card_no+" 카드가 등록 되었습니다.";
            timer1.Start();
        }
        public void set_text2(int cnt)
        {
            this.cnt = cnt;
            label1.Text = "실적이 등록 되었습니다.";
            timer1.Start();
        }
    }
}
