﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;


namespace DK_Tablet
{
    public partial class Work_Center_In : Form
    {
        [DllImport("user32.dll")]
        
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);//리더기와 pc간 통신하기 위한 함수
        
        public SwingLibrary.SwingAPI Swing = null;//스윙-u 사용하기 위한 API 객체 생성
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        //private SwingLibrary.SwingAPI Swing2 = null;//스윙-u 사용하기 위한 API 객체 생성
        
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        //입구 리더기
        string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //string rec_lot_no = "";
        string rec_card_no = "";//다음 pp 카드
        string rec_it_scode = "";//다음 품목코드
        string sel_carrier_no = "";//다음대차번호
        //string sel_lot_no = "";
        string sel_card_no = "";//들어온 카드 번호
        string sel_it_scode = "";//들어온 품목코드
        //string carrier_yn = "";//공대차인지 아닌지 구분자
        float f_mm_sqty = 0;//대차에 적재 되어있는 제품 수량
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        string sw_code = "";//근무형태 코드
        //출구 리더기
        PpCard_reading_paint_in PpCard_reading;
        //string out_carrier_no = "";// 출구로 나가는 대차 번호
        //DataTable Tag_DT_PP = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관
        //DataTable Tag_DT_DC = new DataTable();//출구에서 리더기로 읽어 들인 대차 카드 보관
        //DataTable po_DT = new DataTable();//생산계획을 여러개 선택 할 수 있도록 선택한 생산계획 정보 보관

        //string PP_WC_CODE_str = "";//작업장
        //string PP_IT_SCODE_str = "";//품목코드
        //string CARD_NO_str = "";//카드번호
        //string PP_SIZE_str = "";//수용수
        PpCard_Success PpCard_Success = new PpCard_Success();
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";

        DataTable DataGridViewDT = new DataTable();
        DataTable DataGridViewDT_c = new DataTable();

        public Work_Center_In(MAIN form)
        {
         PpCard_reading = new PpCard_reading_paint_in(this);

            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
            
        }
        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else { 
                            //radioButton_ac_multi.Checked = true;
                        }
                            break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no="";
                        rec_it_scode = "";
                        rec_card_no = "";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
            }));

            Swing.ReportTagList();
        }
        
        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }
        
        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //리딩
                    saveDataRow_PP(itemString[3]);
                    //saveDataRow_DC_S(itemString[3]);
                    //txtIn_Carrier_no.Text = in_carrier_no;
                    txtIt_scode.Text = rec_it_scode;
                    txtCard_no.Text = rec_card_no;
                    /*
                    if (in_carrier_no == sel_carrier_no) { 
                        txtIn_Carrier_no.Text = in_carrier_no;
                    }
                    if (rec_it_scode == sel_it_scode) { 
                        txtIt_scode.Text = rec_it_scode;
                    }
                    if (rec_card_no == sel_card_no) { 
                        txtCard_no.Text = rec_card_no;
                    }
                    */
                    
                    if (!string.IsNullOrWhiteSpace(txtIn_Carrier_no.Text) || !string.IsNullOrWhiteSpace(txtIt_scode.Text) || !string.IsNullOrWhiteSpace(txtCard_no.Text))
                    {
                        //if (sel_carrier_no == in_carrier_no && sel_it_scode == rec_it_scode && sel_card_no == rec_card_no)
                        //{
                        //대차가 작업장에 있는지 체크 (품목,PP시리얼,대차코드)
                        ////
                        DataRow[] Dr = DataGridViewDT.Select("PRDT_ITEM='" + rec_it_scode + "' AND CARD_NO='" + rec_card_no + "'");

                        if (Dr.Length > 0)
                        {
                            DataRow dr_c = DataGridViewDT_c.NewRow();
                            //dr_c["CARRIER_NO_C"] = Dr[0]["CARRIER_NO"];
                            dr_c["CARD_NO_C"] = Dr[0]["CARD_NO"];
                            DataGridViewDT_c.Rows.InsertAt(dr_c,0);
                            if (DataGridViewDT_c.Rows.Count == 11)
                            {
                                DataGridViewDT_c.Rows.RemoveAt(10);
                            }
                            Swing.InventoryStop();
                            //작업장으로 이동

                            MOVE_FUNC.move_insert_NEW(Dr[0][4].ToString(), Dr[0][3].ToString(), "", float.Parse(Dr[0][9].ToString())
                                , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            PpCard_reading.TopLevel = false;
                            PpCard_reading.TopMost = false;
                            PpCard_reading.Visible = false;
                            Success_Form.TopLevel = true;
                            Success_Form.TopMost = true;
                            Success_Form.Visible = true;
                            
                            Success_Form.set_text(Dr[0][3].ToString().Trim());
                            timer1.Start();
                            /*Success_Form Success_Form = new Success_Form();
                            Success_Form.carrier_no = Dr[0][3].ToString();
                            Success_Form.Show();*/
                            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            dataGridView1.DataSource = DataGridViewDT;
                            dataGridView1.CurrentCell = null;

                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";
                            sel_carrier_no = "";
                            sel_it_scode = "";
                            sel_card_no = "";

                            
                            if (dataGridView1.Rows.Count == 0)
                            {
                                txtNext_Carrier_no.Text = "";
                            }
                            else
                            {
                                //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                                sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                                sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                                //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                            }
                            
                            Swing.TagListClear();
                            dsmListView_ivt.Items.Clear();
                            ddc_ivt.DigitText = "00000";
                        }
                            
                        else
                        {
                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";
                            Swing.InventoryStop();
                            Swing.TagListClear();
                            dsmListView_ivt.Items.Clear();
                            ddc_ivt.DigitText = "00000";
                        }
                        
                    }                    
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        
        

        public void saveDataRow_PP(string str)
        {            
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');
                    
                    if (strTag.Length == 5)
                    {
                        //dataGridView1.Rows[0].Cells["PP_SERNO"].Value.ToString();
                        //dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString();
                        //lot_no = dataGridView1.Rows[0].Cells["LOT_NO"].Value.ToString();
                        
                        //dr["SITE_CODE"] = strTag[0];
                        //dr["WC_CODE"] = strTag[1];
                        rec_card_no = strTag[2].Trim();
                        rec_it_scode = strTag[3].Trim();
                        f_mm_sqty = float.Parse(strTag[4]);
                    }
                    
                }
            }            
        }
        public void saveDataRow_DC_S(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    in_carrier_no = strTags[1].Trim();                    
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();
            
            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {   
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";
                    txtIn_Carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";
                    Swing.SetRFPower(27);
                    Swing.ReportAllInformation();

                    
                        work_plan_popup_paint();
                   
                    
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }
            
            finally { textEdit1.Text = ""; textEdit1.Focus(); }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }
        #endregion

        public void GetTime()
        {
            while (true)
            {
                N_timer.Text = DateTime.Now.ToString("HH시 mm분 ss초");
            }
        }

        ContextMenuStrip remove_menu;
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {/*
                Thread TimeThread = new Thread(GetTime);
                TimeThread.IsBackground = true;
                TimeThread.Start();*/
                PpCard_reading.Show();
                PpCard_reading.Visible = false;
                Success_Form.Show();
                Success_Form.Visible = false;

                GET_DATA.get_work_time_master();
                night_time_start = GET_DATA.night_time_start;
                day_time_start = GET_DATA.day_time_start;
                
                //timer_now.Start();
                //GET_DATA.SYSTEMTIME stime = new GET_DATA.SYSTEMTIME();
                //stime = GET_DATA.GetTime();
                //N_timer.Text = stime.wYear.ToString() + "-" + stime.wMonth + "-" + stime.wDay + " " + stime.wHour + ":" + stime.wMinute + ":" + stime.wSecond;
                
                GET_DATA.get_timer_master(wc_group);
                
                

                lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";
                //lookUpEdit1.SelectionStart = 0;
                //lueWc_code.ItemIndex = 0;
                if (regKey.GetValue("WC_CODE") == null)
                {
                    regKey.SetValue("WC_CODE", "");
                }
                else
                {
                    if (regKey.GetValue("WC_CODE").ToString() != "")
                    {

                        lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                    }
                }

                //SWING-U
                this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
                SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);
               

                remove_menu = new ContextMenuStrip();
                ToolStripMenuItem item = new ToolStripMenuItem("Remove");
                item.Click += new EventHandler(target_remove);
                remove_menu.Items.Add(item);

                remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

                listView_target_list.ContextMenuStrip = remove_menu;
                
                DataGridViewDT.Columns.Add("ROWNUM", typeof(string));//0
                DataGridViewDT.Columns.Add("VW_SNUMB", typeof(string));//1
                DataGridViewDT.Columns.Add("CARRIER_DATE", typeof(string));//2
                //DataGridViewDT.Columns.Add("CARRIER_NO", typeof(string));//3
                DataGridViewDT.Columns.Add("CARD_NO", typeof(string));//4
                DataGridViewDT.Columns.Add("PRDT_ITEM", typeof(string));//5
                DataGridViewDT.Columns.Add("LOT_DATE", typeof(string));//6
                DataGridViewDT.Columns.Add("HARFTYPE", typeof(string));//7
                DataGridViewDT.Columns.Add("LOT_NO", typeof(string));//8
                DataGridViewDT.Columns.Add("CHECK_YN", typeof(string));//9
                DataGridViewDT.Columns.Add("GOOD_QTY", typeof(string));//10
                DataGridViewDT.Columns.Add("HH", typeof(string));//11

                //DataGridViewDT_c.Columns.Add("CARRIER_NO_C", typeof(string));//3
                DataGridViewDT_c.Columns.Add("CARD_NO_C", typeof(string));//4
                dataGridView2.DataSource = DataGridViewDT_c;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }
            finally
            {
                textEdit1.Text = ""; textEdit1.Focus();
            }

        }
        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();            
            Swing.ConnectionClose();            
            parentForm.Visible = true;
        }
       

        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check2(string prdt_item, string pp_serno,string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + pp_serno + "' AND WC_CODE='"+wc_code+"' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIn_Carrier_no.Text = "";
            txtIt_scode.Text = "";
            txtCard_no.Text = "";
            in_carrier_no = "";
            rec_it_scode = "";
            rec_card_no = "";
            sel_carrier_no="";
            sel_it_scode ="";
            sel_card_no = "";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";

           // sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
            Swing.InventoryStart(); 
        }

        private void btn_po_release_Click(object sender, EventArgs e)
        {
            try
            {
                work_plan_popup_paint();
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
            //po_release_popup();
            
        }

        public void work_plan_popup()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            //if (Swing2.IsOpen)
            if(true)
            {

                Work_plan_search Work_plan_search = new Work_plan_search();
                Work_plan_search.wc_group = wc_group;
                Work_plan_search.wc_code = str_wc_code;
                if (Work_plan_search.ShowDialog() == DialogResult.OK)
                {
                    formClear2();
                    txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                    txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                    txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                    //txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                    //str_wc_code = Work_plan_search.WC_CODE_str;
                    mo_snumb = Work_plan_search.MO_SNUMB_str;
                    PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                    me_scode = Work_plan_search.ME_SCODE_str;
                    max_sqty = Work_plan_search.MAX_SQTY_str;
                    //txt_MO_SQUTY.Text = max_sqty;
                    //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                    txt_datetime.Text = r_start;
                    //carrier_yn = "Y";

                    //양품수량 불량수량 들고오기
                    //GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                    //txt_good_qty.Text = GET_DATA.good_qty;
                    //txt_fail_qty.Text = GET_DATA.fail_qty;
                    
                    DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                    dataGridView1.DataSource = DataGridViewDT;
                    dataGridView1.CurrentCell = null;
                    txtIn_Carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";
                    sel_carrier_no = "";
                    sel_it_scode = "";
                    sel_card_no = "";
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";
                    //set_현재고_최대생산가능수량();
                    //shiftwork();//작업유형 가져오기
                    //set_worker_info();//작업자 팝업
                    if (dataGridView1.Rows.Count > 0)
                    {
                        //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                        sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                        sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                        Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                        
                        //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                    }
                    timer_re_carrier.Stop();
                    timer_re_carrier.Start();
                    
                    
                    
                    
                }

            }/*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }

        public void work_plan_popup_paint()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                //if (Swing2.IsOpen)
                if (true)
                {

                    Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                    Work_plan_search.wc_group = wc_group;
                    Work_plan_search.wc_code = str_wc_code;
                    if (Work_plan_search.ShowDialog() == DialogResult.OK)
                    {
                        formClear2();
                        txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                        txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                        txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                        //txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                        me_scode = Work_plan_search.ME_SCODE_str;
                        max_sqty = Work_plan_search.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        //carrier_yn = "Y";

                        //양품수량 불량수량 들고오기
                        //GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        //txt_good_qty.Text = GET_DATA.good_qty;
                        //txt_fail_qty.Text = GET_DATA.fail_qty;

                        DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                        dataGridView1.DataSource = DataGridViewDT;
                        dataGridView1.CurrentCell = null;
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no = "";
                        rec_it_scode = "";
                        rec_card_no = "";
                        sel_carrier_no = "";
                        sel_it_scode = "";
                        sel_card_no = "";
                        Swing.InventoryStop();
                        Swing.TagListClear();
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        //set_현재고_최대생산가능수량();
                        //shiftwork();//작업유형 가져오기
                        //set_worker_info();//작업자 팝업
                        if (dataGridView1.Rows.Count > 0)
                        {
                            //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                            Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);

                            //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                        }
                        timer_re_carrier.Stop();
                        timer_re_carrier.Start();


                        lueWc_code.Enabled = false;

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }    /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }

        //클리어
        public void formClear()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            //txt_MO_SQUTY.Text = null;
            //txt_good_qty.Text = null;
            //txt_fail_qty.Text = null;
            txtIn_Carrier_no.Text = null;
            in_carrier_no = "";
            mo_snumb = "";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            /*PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";*/
            PP_SITE_CODE_str = "";
            

        }
        public void formClear2()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            //txt_MO_SQUTY.Text = null;
            //txt_good_qty.Text = null;
            //txt_fail_qty.Text = null;
            mo_snumb = "";
            /*PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";*/
            PP_SITE_CODE_str = "";       
        }
        //비가동 등록
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {
                    
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code);
                }
                this.Cursor = Cursors.Default;
            }
        }

        


        private void btn_fail_input_Click(object sender, EventArgs e)
        {/*
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                Fail_Input Fail_Input = new Fail_Input();
                Fail_Input.wc_group = wc_group;
                if (Fail_Input.ShowDialog() == DialogResult.OK)
                {

                    //불량 등록
                    int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb,str_wc_code);
                    if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb,str_wc_code))
                    {
                        txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                    }
                }
                this.Cursor = Cursors.Default;
            }
          */
        }


        //bool PpCard_reading_chk = false;
        
        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            
            regKey.SetValue("WC_CODE", str_wc_code);
        }


        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {
            try
            {
                string[] arryPort_power_in = null;
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.Trim()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                try
                {
                    arryPort_power_in = GET_DATA.get_connection_port(lueWc_code.GetColumnValue("WC_CODE").ToString(), "IN");
                    //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                    Swing.ConnectionOpen(arryPort_power_in[0], 25);

                    if (Swing.IsOpen)
                    {



                    }
                    else
                    {
                        WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                    WinConsole.WriteLine(ex.Message);
                }

                comboBox_ports.Enabled = !Swing.IsOpen;
                button_com_open.Enabled = !Swing.IsOpen;
                button_com_close.Enabled = Swing.IsOpen;

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";
                    txtIn_Carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";
                    Swing.SetRFPower(30 - int.Parse(arryPort_power_in[1]));
                    Swing.ReportAllInformation();


                    work_plan_popup_paint();

                    //po_release_popup();
                }
                else
                {

                }
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            try
            {
                metarial_search metarial_search = new metarial_search();
                metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                if (metarial_search.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
            
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                    dataGridView1.DataSource = DataGridViewDT;
                    dataGridView1.CurrentCell = null;
                    if (dataGridView1.Rows.Count == 0)
                    {
                        txtNext_Carrier_no.Text = "";
                    }
                    else
                    {
                        //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                        sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                        sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                        //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();


                    }
                }
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }


        private void timer_re_carrier_Tick(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                dataGridView1.DataSource = DataGridViewDT;
                dataGridView1.CurrentCell = null;
                if (dataGridView1.Rows.Count == 0)
                {
                    txtNext_Carrier_no.Text = "";
                }
                else
                {
                    //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                    sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                    sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                    //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                }
            }
        }
        /*
        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }
        */


        //작업유형 가져오기
        /*
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code = shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }*/

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        /*
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_code = str_wc_code;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0][1].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0][1].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }
            }
        }
        */
        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            //set_worker_info();
        }



        private void btn_fail_search_Click(object sender, EventArgs e)
        {
            fail_search_popup fail_search_popup = new fail_search_popup();
            fail_search_popup.wc_group = wc_group;
            fail_search_popup.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            fail_search_popup.wc_name = lueWc_code.GetColumnValue("WC_NAME").ToString();
            fail_search_popup.Show();
        }

        private void btn_pp_card_search_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("작업지시를 선택해주세요");
                    return;
                }
                if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
                {
                    MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                    return;
                }
                Swing.InventoryStop();
                Swing.TagListClear();
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                dataGridView1.DataSource = DataGridViewDT;
                dataGridView1.CurrentCell = null;

                PpCard_reading.TopLevel = true;
                PpCard_reading.TopMost = true;
                PpCard_reading.Visible = true;

                Swing.InventoryStart();

                //timer1.Start();
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }
        


        private void timer1_Tick(object sender, EventArgs e)
        {
            PpCard_reading.TopLevel = false;
            PpCard_reading.TopMost = false;
            PpCard_reading.Visible = false;
            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            timer1.Stop();
        }
        private Success_Form Success_Form=new Success_Form();
        private void button1_Click(object sender, EventArgs e)
        {
            DataRow dr = DataGridViewDT_c.NewRow();
            dr[0] = "1";
            dr[1] = "2";
            DataGridViewDT_c.Rows.Add(dr);
        }

        private void btn_po_release_Insert_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                try
                {
                    if (true)
                    {

                        Po_release_Insert_dev Po_release_Insert_dev = new Po_release_Insert_dev();
                        Po_release_Insert_dev.wc_group = wc_group;
                        Po_release_Insert_dev.wc_code = str_wc_code;
                        if (Po_release_Insert_dev.ShowDialog() == DialogResult.OK)
                        {
                            formClear2();
                            txt_IT_SCODE.Text = Po_release_Insert_dev.IT_SCODE_str;
                            txt_IT_SNAME.Text = Po_release_Insert_dev.IT_SNAME_str;
                            txt_IT_MODEL.Text = Po_release_Insert_dev.IT_MODEL_str;
                            //txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                            //str_wc_code = Work_plan_search.WC_CODE_str;
                            mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                            PP_SITE_CODE_str = Po_release_Insert_dev.SITE_CODE_str;
                            me_scode = Po_release_Insert_dev.ME_SCODE_str;
                            max_sqty = Po_release_Insert_dev.MAX_SQTY_str;
                            //txt_MO_SQUTY.Text = max_sqty;
                            //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            txt_datetime.Text = r_start;
                            //carrier_yn = "Y";

                            //양품수량 불량수량 들고오기
                            //GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                            //txt_good_qty.Text = GET_DATA.good_qty;
                            //txt_fail_qty.Text = GET_DATA.fail_qty;

                            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            dataGridView1.DataSource = DataGridViewDT;
                            dataGridView1.CurrentCell = null;
                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";
                            sel_carrier_no = "";
                            sel_it_scode = "";
                            sel_card_no = "";
                            Swing.InventoryStop();
                            Swing.TagListClear();
                            dsmListView_ivt.Items.Clear();
                            ddc_ivt.DigitText = "00000";
                            //set_현재고_최대생산가능수량();
                            //shiftwork();//작업유형 가져오기
                            //set_worker_info();//작업자 팝업
                            if (dataGridView1.Rows.Count > 0)
                            {
                                //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                                sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                                sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);

                                //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                            }
                            timer_re_carrier.Stop();
                            timer_re_carrier.Start();
                            lueWc_code.Enabled = false;
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("다시 시도해주세요 : " + ex.Message);
                }
            }
            catch { }
            finally { textEdit1.Text = ""; textEdit1.Focus(); }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("작업지시를 선택해주세요");
                    textEdit1.Text = "";
                    return;
                }
                try
                {
                    
                    string barcode = textEdit1.Text.Trim();
                    //string[] arrBarcode = barcode.Split('*');
                    //if (arrBarcode.Length.Equals(2))
                    //{
                    //    string read_it_scode = arrBarcode[0].ToString();
                    //    string read_lot = arrBarcode[1].ToString();
                    //    string[] arr_lot = read_lot.Split('/');
                    //    if (arr_lot.Length.Equals(4))
                    //    {
                    //        //리딩된 사출 QR 코드로 레이져공정을 거쳐서 왔는지 체크
                    //        //BOM 체크(이종체크)
                    //        DataTable dt = GET_DATA.bom_chk(txt_IT_SCODE.Text,"N");//완성품품목으로 bom 정전개
                    //        DataRow[] dr = dt.Select("IT_SCODE='"+read_it_scode+"'");//리딩된 사출품번이 bom에 있는지 체크
                    //        if (dr.Length>0)
                    //        {
                    //            /*
                    //             * read 사출 qr 코드가 사용 되었는지 체크 NG/진행중/완료 인지 체크 (테이블 : READING_DATA)
                    //             * 
                    //             * 완료되었으면 메세지 띄워주고 return
                    //             * NG : 재투입
                    //             * 진행중이면 return
                    //            */
                    //            check_save_read_data(txt_IT_SCODE.Text.Trim(),read_it_scode,read_lot);

                                
                    //        }
                    //        else
                    //        {
                    //            PpCard_Success.TopLevel = true;
                    //            PpCard_Success.TopMost = true;
                    //            PpCard_Success.Visible = true;
                    //            PpCard_Success.set_text("NG", 5);
                    //            PpCard_Success.BackColor = Color.Red;
                    //        }
                    //    }
                    //    //사용되지 않았으면 체크하는 테이블에 저장 하면서 카운트
                    //}
                    //else 
                        if (barcode.Substring(0, 2).Equals("PM"))
                    {
                        bool check = false;
                        DataTable dt = GET_DATA.bom_chk(txt_IT_SCODE.Text,"Y");
                        string child_it_scode = GET_DATA.get_pm_child_code(barcode);
                        DataRow[] dr = dt.Select("IT_SCODE='" + child_it_scode + "'");
                        if (dr.Length > 0)
                        {
                            if (GET_DATA.get_pm_CHECK(barcode))
                            {


                                check = MOVE_FUNC.move_insert_NEW_pm(barcode, str_wc_code);
                                if (check)
                                {

                                    Success_Form.TopLevel = true;
                                    Success_Form.TopMost = true;
                                    Success_Form.Visible = true;
                                    Success_Form.set_text(barcode);
                                    timer1.Start();
                                }
                                else
                                {
                                    PpCard_Success.TopLevel = true;
                                    PpCard_Success.TopMost = true;
                                    PpCard_Success.Visible = true;
                                    PpCard_Success.set_text("NG", 5);
                                    PpCard_Success.BackColor = Color.Red;

                                }
                            }
                            else
                            {
                                PpCard_Success.TopLevel = true;
                                PpCard_Success.TopMost = true;
                                PpCard_Success.Visible = true;
                                PpCard_Success.set_text("완료된 이동표 입니다.", 5);
                                PpCard_Success.BackColor = Color.Salmon;
                            }
                        }
                        else
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("해당하는 제품이 아닙니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;

                        }
                        textEdit1.Text = "";
                    }
                        else
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("잘못된 이동표 입니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;
                        }
                    ////saveDataRow_PP(Utils.HexToASCII(textEdit1.Text.Trim()));
                    //saveDataRow_PP(textEdit1.Text.Trim());
                    //txtIt_scode.Text = rec_it_scode;
                    //txtCard_no.Text = rec_card_no;
                    
                    //if (!string.IsNullOrWhiteSpace(txtIt_scode.Text) || !string.IsNullOrWhiteSpace(txtCard_no.Text))
                    //{
                    

                    //    DataRow[] Dr = DataGridViewDT.Select("PRDT_ITEM='" + rec_it_scode + "' AND CARD_NO='" + rec_card_no + "'");
                    
                    //    if (Dr.Length > 0)
                    //    {
                            
                    //        DataRow dr_c = DataGridViewDT_c.NewRow();
                    //        //dr_c["CARRIER_NO_C"] = Dr[0]["CARRIER_NO"];
                    //        dr_c["CARD_NO_C"] = Dr[0]["CARD_NO"];
                    //        DataGridViewDT_c.Rows.InsertAt(dr_c, 0);
                    //        if (DataGridViewDT_c.Rows.Count == 11)
                    //        {
                    //            DataGridViewDT_c.Rows.RemoveAt(10);
                    //        }
                    //        Swing.InventoryStop();
                    //        //작업장으로 이동

                    //        MOVE_FUNC.move_insert_NEW(Dr[0][4].ToString(), Dr[0][3].ToString(), "", float.Parse(Dr[0][9].ToString())
                    //            , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                    //        PpCard_reading.TopLevel = false;
                    //        PpCard_reading.TopMost = false;
                    //        PpCard_reading.Visible = false;
                    //        Success_Form.TopLevel = true;
                    //        Success_Form.TopMost = true;
                    //        Success_Form.Visible = true;

                    //        Success_Form.set_text(Dr[0][3].ToString().Trim());
                    //        timer1.Start();

                    //        DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                    //        dataGridView1.DataSource = DataGridViewDT;
                    //        dataGridView1.CurrentCell = null;

                    //        txtIn_Carrier_no.Text = "";
                    //        txtIt_scode.Text = "";
                    //        txtCard_no.Text = "";
                    //        in_carrier_no = "";
                    //        rec_it_scode = "";
                    //        rec_card_no = "";
                    //        sel_carrier_no = "";
                    //        sel_it_scode = "";
                    //        sel_card_no = "";


                    //        if (dataGridView1.Rows.Count == 0)
                    //        {
                    //            txtNext_Carrier_no.Text = "";
                    //        }
                    //        else
                    //        {

                    //            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                    //            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호

                    //        }

                    //        Swing.TagListClear();
                    //        dsmListView_ivt.Items.Clear();
                    //        ddc_ivt.DigitText = "00000";
                    //    }
                    //    else
                    //    {
                    //        txtIn_Carrier_no.Text = "";
                    //        txtIt_scode.Text = "";
                    //        txtCard_no.Text = "";
                    //        in_carrier_no = "";
                    //        rec_it_scode = "";
                    //        rec_card_no = "";
                    //        Swing.InventoryStop();
                    //        Swing.TagListClear();
                    //        dsmListView_ivt.Items.Clear();
                    //        ddc_ivt.DigitText = "00000";
                    //    }

                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }
            }

        }

        public void check_save_read_data(string it_scode, string child_it_scode, string child_lot_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_IN_READ_DATA_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);
            cmd.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);

            try
            {   
                cmd.ExecuteNonQuery();
                
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                
            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            if (e.Message.Trim().Equals("OK"))
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
            }
            else
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }
        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //bool bFind = false;
            //DriveInfo[] diArray = DriveInfo.GetDrives();
            //foreach (DriveInfo di in diArray)
            //{
            //    MessageBox.Show(di.IsReady.ToString());
            //    MessageBox.Show(di.DriveType.ToString());
            //}
            check_save_read_data("", "", "");
        }

        private void btn_ins_search_Click(object sender, EventArgs e)
        {
            Ins_search_Popup Ins_search_Popup = new Ins_search_Popup();
            Ins_search_Popup.wc_code = str_wc_code;
            Ins_search_Popup.ShowDialog();
        }


    }
}
