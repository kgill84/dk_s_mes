﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.DisplayForm
{
    public partial class weight_Fail : Form
    {
        public weight_Fail()
        {
            InitializeComponent();
        }

        private void Injection_Success_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
        int cnt = 4;
        private void timer1_Tick(object sender, EventArgs e)
        {
            cnt = cnt - 1;
            if (cnt == -1)
            {
                timer1.Stop();
                this.Close();
            }
            else
            {
                label2.Text = cnt.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.Close();
        }
    }
}
