﻿using DevExpress.XtraEditors;
using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class FME_InsResultReg : Form
    {
        MAIN parentForm;
        FtpUtil ftpUtil;
        WebClient wc = new WebClient();
        string path = "ftp://203.251.168.131:4104";
        string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        string root = "INSPECTION_STANDARD";
        string strCon = "";
        string plant = "DKQT";

        public string wc_code { get; set; }

        public FME_InsResultReg(MAIN form)
        {
            this.parentForm = form;
            InitializeComponent();

            CheckForIllegalCrossThreadCalls = false;
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            wc.Credentials = GetCredentials();
        }

        private void FME_InsResultReg_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
            this.strCon = Properties.Settings.Default.SQL_DKQT;
            deINS_RDATE.DateTime = DateTime.Now;
            teINS_RTIME.Time = DateTime.Now;

            setWorkCenter();
            setType();
            setDayNight();

            gridControl1.Select();
            if (!string.IsNullOrWhiteSpace(wc_code))
            {
                lueWC_CODE.EditValue = wc_code;
            }
        }

        private void setWorkCenter()
        {
            string sql = "";
            SqlConnection conn = new SqlConnection(this.strCon);

            try
            {
                conn.Open();

                sql = " SELECT '' AS WC_CODE, '' AS WC_NAME UNION"
                    + " SELECT RTRIM(WC_CODE) AS WC_CODE,RTRIM(WC_NAME) AS WC_NAME FROM WORK_CENTER";

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);
                da.SelectCommand.CommandType = CommandType.Text;

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                da.Fill(dt);
                lueWC_CODE.Properties.DataSource = dt;
                lueWC_CODE.Properties.DisplayMember = "WC_NAME";
                lueWC_CODE.Properties.ValueMember = "WC_CODE";
                lueWC_CODE.ItemIndex = 0;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void setType()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("1", "초물");
            list.Add("2", "중물");
            list.Add("3", "종물");

            lueFME_GUBN.Properties.DataSource = list;
            lueFME_GUBN.Properties.DisplayMember = "Value";
            lueFME_GUBN.Properties.ValueMember = "Key";
            lueFME_GUBN.ItemIndex = 0;
        }

        private void setDayNight()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("1", "주간");
            list.Add("2", "야간");

            lueDAY_NIGHT_GUBN.Properties.DataSource = list;
            lueDAY_NIGHT_GUBN.Properties.DisplayMember = "Value";
            lueDAY_NIGHT_GUBN.Properties.ValueMember = "Key";
            lueDAY_NIGHT_GUBN.ItemIndex = 0;
        }

        private void getCheckList()
        {
            string sql = "";
            SqlConnection conn = new SqlConnection(this.strCon);
            DataTable dt;

            try
            {
                conn.Open();

                sql = " SELECT"
                    + " A.INS_CONTENT, A.CF_CODE, C.CF_NAME, A.VALUE_TYPE, A.DEFAULT_VALUE1, A.DEFAULT_VALUE2, A.DEFAULT_MIN, A.DEFAULT_MAX, A.INS_METHOD,"
                    + " ISNULL(D.INS_MANAGER, A.INS_MANAGER) AS INS_MANAGER,"
                    + " CASE WHEN A.VALUE_TYPE = '1' THEN A.DEFAULT_VALUE1"
                    + "     WHEN A.VALUE_TYPE = '2' THEN CAST(A.DEFAULT_VALUE2 AS VARCHAR) END AS DEFAULT_VALUE,"
                    + " CASE WHEN A.VALUE_TYPE = '1' THEN ''"
                    + "     WHEN A.VALUE_TYPE = '2' THEN CAST(A.DEFAULT_MIN AS VARCHAR) + ' ~ ' + CAST(A.DEFAULT_MAX AS VARCHAR) END AS VAL_RANGE,"
                    + " ISNULL(D.INS_VALUE, '') AS INS_VALUE, A.SERNO"
                    + " FROM FME_INSITEM A"
                    + " LEFT JOIN WORK_CENTER B ON A.WC_CODE = B.WC_CODE"
                    + " LEFT JOIN FME_INSCLASSIFY C ON A.CF_CODE = C.CF_CODE"
                    + " LEFT JOIN (SELECT INS_SERNO, INS_VALUE, INS_MANAGER FROM FME_INSRESULT"
                    + "     WHERE WC_CODE = @WC_CODE AND INS_RDATE = @INS_RDATE"
                    + "     AND IT_SCODE = @IT_SCODE AND FME_GUBN = @FME_GUBN AND DAY_NIGHT_GUBN = @DAY_NIGHT_GUBN"
                    + " ) D ON A.SERNO = D.INS_SERNO"
                    + " WHERE A.WC_CODE = @WC_CODE AND A.IT_SCODE = @IT_SCODE";

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);
                da.SelectCommand.CommandType = CommandType.Text;
                da.SelectCommand.Parameters.AddWithValue("@WC_CODE", lueWC_CODE.EditValue.ToString());
                da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", txtIT_SCODE.Text.Trim());
                da.SelectCommand.Parameters.AddWithValue("@INS_RDATE", deINS_RDATE.DateTime.ToString("yyyyMMdd"));
                da.SelectCommand.Parameters.AddWithValue("@FME_GUBN", lueFME_GUBN.EditValue.ToString());
                da.SelectCommand.Parameters.AddWithValue("@DAY_NIGHT_GUBN", lueDAY_NIGHT_GUBN.EditValue.ToString());
                da.Fill(dt = new DataTable());

                gridControl1.DataSource = dt;

                gridControl1.Select();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void setImage()
        {
            imageSlider1.Images.Clear();

            string ftp_str = AdjustDir(lueWC_CODE.EditValue.ToString()) + AdjustDir(txtIT_SCODE.Text.Trim());
            FTPDirectioryCheck(ftp_str);

            foreach (Image item in get_file_list(ftp_str))
            {
                if (item != null) imageSlider1.Images.Add(item);
            }
        }

        public Image[] get_file_list(string currentDirectory)
        {
            string ftpFileFullPath = string.Format("{0}{1}", this.path, AdjustDir(root) + AdjustDir(this.plant) + currentDirectory);
            int idx = 0;

            Image[] arr = new Image[5];
            FtpWebRequest ftpWebRequest = WebRequest.Create(new Uri(ftpFileFullPath)) as FtpWebRequest;
            ftpWebRequest.Credentials = new NetworkCredential(this.ftpID, this.ftpPass);
            ftpWebRequest.UseBinary = true;
            ftpWebRequest.UsePassive = true;
            ftpWebRequest.Timeout = 10000;
            ftpWebRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = null;

            try
            {
                response = ftpWebRequest.GetResponse() as FtpWebResponse;

                if (response != null)
                {
                    try
                    {
                        StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                        string[] data_arr = streamReader.ReadToEnd().Split(new char[] { '\r', '\n' });

                        foreach (string str in data_arr)
                        {
                            if (!string.IsNullOrWhiteSpace(str))
                            {
                                string file_str = ftpFileFullPath + AdjustDir(str);
                                string image_path = Path.GetTempPath() + str;

                                if (!System.IO.File.Exists(image_path))
                                    wc.DownloadFile(file_str, Path.GetTempPath() + str);

                                arr[idx] = Image.FromFile(image_path);
                                idx++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error(WebClient)dddddddd : " + ex.Message);
                    }
                }
            }
            finally
            {
                if (response != null) response.Close();
            }

            return arr;
        }

        public void FTPDirectioryCheck(string directoryPath)
        {
            directoryPath = AdjustDir(root) + AdjustDir(this.plant) + directoryPath;

            string[] directoryPaths = directoryPath.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

            string currentDirectory = string.Empty;
            foreach (string directory in directoryPaths)
            {
                currentDirectory += string.Format("/{0}", directory);
                if (!IsExtistDirectory(currentDirectory))
                {
                    MakeDirectory(currentDirectory);
                }
            }
        }

        private bool MakeDirectory(string dirpath)
        {
            string URI = string.Format("{0}/{1}", this.path, dirpath);
            System.Net.FtpWebRequest ftp = GetRequest(URI);
            ftp.Method = System.Net.WebRequestMethods.Ftp.MakeDirectory;

            try
            {
                string str = GetStringResponse(ftp);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private bool IsExtistDirectory(string currentDirectory)
        {
            string ftpFileFullPath = string.Format("{0}{1}", this.path, GetParentDirectory(currentDirectory));

            FtpWebRequest ftpWebRequest = WebRequest.Create(new Uri(ftpFileFullPath)) as FtpWebRequest;
            ftpWebRequest.Credentials = new NetworkCredential(this.ftpID, this.ftpPass);
            ftpWebRequest.UseBinary = true;
            ftpWebRequest.UsePassive = true;
            ftpWebRequest.Timeout = 10000;
            ftpWebRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            FtpWebResponse response = null;
            string data = string.Empty;

            try
            {
                response = ftpWebRequest.GetResponse() as FtpWebResponse;
                if (response != null)
                {
                    StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.Default);

                    data = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n다시 시도해주세요");
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            string[] directorys = data.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            return (from directory in directorys
                    select directory.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                        into directoryInfos
                        where directoryInfos[0][0] == 'd'
                        select directoryInfos[8]).Any(
                        name => name == (currentDirectory.Split('/')[currentDirectory.Split('/').Length - 1]).ToString());
        }

        private string GetStringResponse(FtpWebRequest ftp)
        {
            string result = "";
            using (FtpWebResponse response = (FtpWebResponse)ftp.GetResponse())
            {
                long size = response.ContentLength;
                using (Stream datastream = response.GetResponseStream())
                {
                    if (datastream != null)
                    {
                        using (StreamReader sr = new StreamReader(datastream))
                        {
                            result = sr.ReadToEnd();
                            sr.Close();
                        }

                        datastream.Close();
                    }
                }
                response.Close();
            }
            return result;
        }

        private ICredentials GetCredentials()
        {
            return new NetworkCredential(this.ftpID, this.ftpPass);
        }

        private FtpWebRequest GetRequest(string URI)
        {
            FtpWebRequest result = (FtpWebRequest)WebRequest.Create(URI);
            result.Credentials = GetCredentials();
            result.KeepAlive = false;
            return result;
        }

        private string GetParentDirectory(string currentDirectory)
        {
            string[] directorys = currentDirectory.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            string parentDirectory = string.Empty;
            for (int i = 0; i < directorys.Length - 1; i++)
            {
                parentDirectory += "/" + directorys[i];
            }

            return parentDirectory;
        }

        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (gridView1.RowCount < 0) return;
            if (e.Column != INS_VALUE) return;

            string type = gridView1.GetRowCellValue(e.RowHandle, VALUE_TYPE).ToString();
            Func<string, string> cutStr = (s) =>
            {
                if (s.Contains("."))
                    if (s.Split('.')[1].Length > 2)
                        s = s.Substring(0, (s.IndexOf(".") + 3));
                return s;
            };

            switch (type)
            {
                case "1" :
                    FME_KeyPad2 pad2 = new FME_KeyPad2();
                    if (pad2.ShowDialog() == DialogResult.OK)
                        gridView1.SetRowCellValue(e.RowHandle, INS_VALUE, cutStr(pad2.txt_value));
                    break;

                case "2":
                    FME_KeyPad pad = new FME_KeyPad();
                    pad.txt_value = gridView1.GetRowCellDisplayText(e.RowHandle, INS_VALUE);
                    if (pad.ShowDialog() == DialogResult.OK)
                        gridView1.SetRowCellValue(e.RowHandle, INS_VALUE, cutStr(pad.txt_value));
                    break;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private DataTable getDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("INS_CONTENT");
            dt.Columns.Add("INS_SERNO");
            dt.Columns.Add("INS_VALUE");
            dt.Columns.Add("INS_MANAGER");

            for (int i = 0; i < gridView1.DataRowCount; i++)
            {
                string value = gridView1.GetRowCellValue(i, INS_VALUE).ToString();

                DataRow dr = dt.NewRow();
                dr["INS_CONTENT"] = gridView1.GetRowCellValue(i, INS_CONTENT).ToString();
                dr["INS_SERNO"] = gridView1.GetRowCellValue(i, SERNO).ToString();
                dr["INS_VALUE"] = string.IsNullOrWhiteSpace(value) ? "" : value;
                dr["INS_MANAGER"] = gridView1.GetRowCellValue(i, INS_MANAGER).ToString();
                dt.Rows.Add(dr);
            }

            return dt;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (lueWC_CODE.EditValue ==  null)
            {
                MessageBox.Show("작업장을 선택하세요.");
                return;
            }

            if (string.IsNullOrWhiteSpace(lueWC_CODE.EditValue.ToString()))
            {
                MessageBox.Show("작업장을 선택하세요.");
                return;
            }

            DataTable dt = getDataTable();

            if (dt.Rows.Count < 1)
            {
                MessageBox.Show("저장할 항목이 없습니다.");
                return;
            }

            string sql = "";
            SqlConnection conn = new SqlConnection(this.strCon);

            try
            {
                conn.Open();

                sql = "USP_TABLET_INS_RESULT_INSERT";

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TVP", dt);
                cmd.Parameters.AddWithValue("@INS_RDATE", deINS_RDATE.DateTime.ToString("yyyyMMdd"));
                cmd.Parameters.AddWithValue("@INS_RTIME", teINS_RTIME.Time.ToString("HHmm"));
                cmd.Parameters.AddWithValue("@FME_GUBN", lueFME_GUBN.EditValue.ToString());
                cmd.Parameters.AddWithValue("@WC_CODE", lueWC_CODE.EditValue.ToString());
                cmd.Parameters.AddWithValue("@IT_SCODE", txtIT_SCODE.Text.Trim());
                cmd.Parameters.AddWithValue("@DAY_NIGHT_GUBN", lueDAY_NIGHT_GUBN.EditValue.ToString());
                cmd.Parameters.AddWithValue("@PC_SCODE", "");
                cmd.ExecuteNonQuery();

                MessageBox.Show("등록완료");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void lueWC_CODE_EditValueChanged(object sender, EventArgs e)
        {
            workPlan();
        }

        private void txtIT_SCODE_Click(object sender, EventArgs e)
        {
            workPlan();
        }

        private void workPlan()
        {
            if (lueWC_CODE.EditValue == null) return;
            if (string.IsNullOrWhiteSpace(lueWC_CODE.EditValue.ToString())) return;

            try
            {
                Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                Work_plan_search.wc_code = lueWC_CODE.EditValue.ToString();

                if (Work_plan_search.ShowDialog() == DialogResult.OK)
                {
                    txtIT_SCODE.Text = Work_plan_search.IT_SCODE_str.Trim();
                    txtIT_SNAME.Text = Work_plan_search.IT_SNAME_str.Trim();

                    getCheckList();
                    setImage();
                }
                else
                {
                    txtIT_SCODE.Text = "";
                    txtIT_SNAME.Text = "";
                    gridControl1.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }
        }

        private void EditValueChanged(object sender, EventArgs e)
        {
            if (lueWC_CODE.EditValue == null) return;
            if (string.IsNullOrWhiteSpace(lueWC_CODE.EditValue.ToString())) return;

            getCheckList();
        }

        private void deINS_RDATE_QueryPopUp(object sender, CancelEventArgs e)
        {
            DateEdit dateEdit = sender as DateEdit;
            e.Cancel = !dateEdit.Enabled || dateEdit.Properties.ReadOnly;
        }

        private void btnAttach_Click(object sender, EventArgs e)
        {
            if (lueWC_CODE.EditValue == null) return;
            if (string.IsNullOrWhiteSpace(lueWC_CODE.EditValue.ToString())) return;
            if (string.IsNullOrWhiteSpace(txtIT_SCODE.Text)) return;

            FME_AttachFileForm form = new FME_AttachFileForm();
            form.plant = this.plant;
            form.sdate = deINS_RDATE.DateTime.ToString("yyyyMMdd");
            form.dayNight = lueDAY_NIGHT_GUBN.EditValue.ToString();
            form.wc_code = lueWC_CODE.EditValue.ToString();
            form.it_scode = txtIT_SCODE.Text.Trim();
            form.ShowDialog();
        }
    }
}

