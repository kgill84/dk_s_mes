﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DK_Tablet.Popup;
using System.Drawing.Printing;
using DK_Tablet.PRINT;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors;


namespace DK_Tablet
{
    public partial class Site_Move : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
        private SwingLibrary.SwingAPI Swing = null;        
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();
        GET_DATA GET_DATA = new GET_DATA();
        SUB_SAVE SUB_SAVE = new SUB_SAVE();
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();
        Func_Mobis_Print Func_Mobis_Print = new Func_Mobis_Print();
        Shipment_List Shipment_List = new Shipment_List();
        DataTable DT_FINAL = new DataTable();
        string str_loc_code = "";
        string str_delivery = "";
        string str_pc_scode = "";
        public bool messageCheck = false;
        public string wc_group { get; set; }
        public string str_wc_code { get; set; }
        
        public string mo_snumb = "", r_start = "";
        DataTable Tag_DT_QR = new DataTable();

        DataTable Reading_Dt = new DataTable();
        
        DataTable DT_it_chart_update = new DataTable();//모비스 식별표 구분자 업데이트
        DataTable KIA_it_chart_update = new DataTable();//기아 식별표 구분자 업데이트
        DataTable HD_it_chart_update = new DataTable();//현대 식별표 구분자 업데이트
        DataTable DT_IT_MASTER = new DataTable();//품목정보 (품목코드, 품명, 용기수량, 자재유형)

        /*
        string PP_WC_CODE_str="";
        string PP_IT_SCODE_str="";
        string CARD_NO_str="";
        string PP_SIZE_str = "";
        string PP_SITE_CODE_str = "";
        string carrier_no = "";
        string me_scode = "";
        string it_pkqty ="";
         */
        public int po_start_time { get;set;}
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";
        Dictionary<string, string> combPortDic = new Dictionary<string, string>();
        MAIN parentForm;
        
        string sh_snumb = "";
        string loc_code = "";

        FtpUtil ftpUtil;
        string path = "ftp://ftp.pantos.com";
        string ftpID = "dkind";
        string ftpPass = "dk2015!@#";
        string main_site_code = Properties.Settings.Default.SITE_CODE.ToString();

        public Site_Move(MAIN form)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.parentForm = form;
            
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;
                

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            combPortDic = Utils.GetComList();
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*
            
            
            
            Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            /*
            groupBox_bt.Enabled = checkBox_dongle.Checked;

            label_battery_volt.Text = "Volts: 0.000 [V]";

            checkBox_rawdata.Checked = Properties.Settings.Default.ConsoleEnable;
            Swing.LogWrite = Properties.Settings.Default.ConsoleEnable;
            WinConsole.Visible = checkBox_rawdata.Checked;

            for (int i = 30; i > 2; i--) comboBox_rfpwr.Items.Add(i);
            comboBox_rfpwr.SelectedIndex = 0;

            for (int j = 0; j < 6; j++) comboBox_channel.Items.Add(j);
            comboBox_channel.SelectedIndex = 0;
            comboBox_channel.Enabled = checkBox_fixed_channel.Checked;

            for (int i = 0; i < 16; i++) comboBox_q.Items.Add(i);
            comboBox_q.SelectedIndex = 4;

            comboBox_algorithm.Items.Add("FIXEDQ");
            comboBox_algorithm.Items.Add("DYNAMICQ");
            comboBox_algorithm.SelectedIndex = 1;

            checkBox_toggle_target.Checked = true;

            comboBox_bank.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.MemoryBank));
            comboBox_bank.SelectedIndex = 1;
            textBox_BlockOffset.Text = "2";
            textBox_BlockCount.Text = "6";
            //textBox_accesspwd.Text = "00000000";

            comboBox_mem_killpwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_accesspwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_epc.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_tid.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_user.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });

            comboBox_mem_killpwd.SelectedIndex = 4;
            comboBox_mem_accesspwd.SelectedIndex = 4;
            comboBox_mem_epc.SelectedIndex = 4;
            comboBox_mem_tid.SelectedIndex = 4;
            comboBox_mem_user.SelectedIndex = 4;

            radioButton_vol_max.Checked = true;
            radioButton_bz_trigger.Checked = true;
            */
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
             
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
            GET_DATA.get_work_time_master();
            night_time_start = GET_DATA.night_time_start;
            day_time_start = GET_DATA.day_time_start;
            DT_IT_MASTER.Columns.Add("IT_SCODE",typeof(string));
            DT_IT_MASTER.Columns.Add("IT_SNAME", typeof(string));
            DT_IT_MASTER.Columns.Add("IT_PKQTY", typeof(string));
            DT_IT_MASTER.Columns.Add("ME_SCODE", typeof(string));
            
            //GET_DATA.get_timer_master(wc_group);

            
            //LUE_LOC_CODE.ItemIndex = 0;
            lueLoc_Car_code.Properties.DataSource = GET_DATA.OUT_Loc_carSelect_DropDown();
            lueLoc_Car_code.Properties.DisplayMember = "DV_NAME";
            lueLoc_Car_code.Properties.ValueMember = "DV_CODE";
            lueLoc_Car_code.ItemIndex = 0;

            DT_IT_MASTER = GET_DATA.SHIP_MOVE_ITEM_MASTER();
            lueIt_Scode.Properties.DataSource = DT_IT_MASTER;
            lueIt_Scode.Properties.DisplayMember = "IT_SCODE";
            lueIt_Scode.Properties.ValueMember = "IT_SCODE";
            //SWING-U
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

            listView_target_list.ContextMenuStrip = remove_menu;

            Tag_DT_QR.Columns.Add("QR_SDATE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_IT_SCODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_SQTY", typeof(int));
            Tag_DT_QR.Columns.Add("INPUT_SQTY", typeof(int));
            Tag_DT_QR.Columns.Add("QR_SEQ", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOT_NO", typeof(string));
            Tag_DT_QR.Columns.Add("QR_CHECK_YN", typeof(string));
            Tag_DT_QR.Columns.Add("QR_LOC_CODE", typeof(string));
            Tag_DT_QR.Columns.Add("QR_MM_RDATE", typeof(string));
            Tag_DT_QR.Columns.Add("ME_SCODE", typeof(string));
            Tag_DT_QR.Columns.Add("CHECK_YN", typeof(string));
            Tag_DT_QR.Columns.Add("GUBN", typeof(string));


            
            Reading_Dt.Columns.Add("IT_SCODE", typeof(string));
            Reading_Dt.Columns.Add("SH_SQTY", typeof(string));            
            Reading_Dt.Columns.Add("CHECK_YN", typeof(string));


            dataGridView1.DataSource = Tag_DT_QR;
            DT_it_chart_update.Columns.Add("IT_SCODE", typeof(string));
            DT_it_chart_update.Columns.Add("SEQ", typeof(string));
            DT_it_chart_update.Columns.Add("LOT_NO", typeof(string));

            KIA_it_chart_update.Columns.Add("IT_SCODE", typeof(string));
            KIA_it_chart_update.Columns.Add("SEQ", typeof(string));
            KIA_it_chart_update.Columns.Add("LOT_NO", typeof(string));

            HD_it_chart_update.Columns.Add("IT_SCODE", typeof(string));
            HD_it_chart_update.Columns.Add("SEQ", typeof(string));
            HD_it_chart_update.Columns.Add("LOT_NO", typeof(string));


            DT_FINAL.Columns.Add("QR_IT_SCODE",typeof(string));            
            DT_FINAL.Columns.Add("QR_SQTY", typeof(int));
            DT_FINAL.Columns.Add("FROM_SITE_CODE", typeof(string));
            DT_FINAL.Columns.Add("FROM_SITE_NAME", typeof(string));
            DT_FINAL.Columns.Add("FROM_LOC_CODE", typeof(string));
            DT_FINAL.Columns.Add("FROM_LOC_NAME", typeof(string));
            DT_FINAL.Columns.Add("TO_SITE_CODE", typeof(string));
            DT_FINAL.Columns.Add("TO_SITE_NAME", typeof(string));
            DT_FINAL.Columns.Add("TO_LOC_CODE", typeof(string));
            DT_FINAL.Columns.Add("TO_LOC_NAME", typeof(string));
            DT_FINAL.Columns.Add("MOVE_COST", typeof(float));
            gridControl3.DataSource = DT_FINAL;
        }
        
        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            //if (data.Contains("M"))
            //    Swing_ParseMemoryReadReport(data.Trim('\0'));
            //else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        
                        
                        Tag_DT_QR.Rows.Clear();
                        
                        
                        lbl_Cnt2.Text = "0";
                        lbl_Cnt3.Text = "0";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            //if (data.Contains("M"))
            //    Swing_ParseMemoryReadReport(data.Trim('\0'));
            //else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        void CloseThreadFunction(object normal)
        {
            bool normal_off = (bool)normal;

            Thread.Sleep(1000);
            this.Invoke(new EventHandler(delegate
            {
                button_com_close_Click(null, null);
            }));

            if (normal_off)
                MessageBox.Show("Swing-U is turned off by user", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Swing-U is turned down abnormaly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                
                Tag_DT_QR.Rows.Clear();
                
                
            }));

            Swing.ReportTagList();
        }

        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    //리딩
                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //PP카드 row add
                    //saveDataRow_PP(itemString[3]);
                    /*DataRow dr = saveDataRow_PP(itemString[3]);

                    if (dr != null)
                    {
                        
                        Tag_DT_PP.Rows.Add(dr);
                        lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text) + 1).ToString();                        
                        dataGridView3.DataSource = Tag_DT_PP;
                    } */             
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }

                /*if (dsmListView_ivt.Items.Count == 2)
                {
                    Swing.InventoryStop();
                }*/
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    saveDataRow_QR_식별표(itemString[3]);
                    //DataRow dr = saveDataRow_QR(itemString[3]);
                    
                    /*
                    if (dr != null)
                    {
                        for (int i = 0; i < advBandedGridView1.RowCount; i++)
                        {
                            if (Convert.ToString(advBandedGridView1.GetRowCellValue(i, "IT_SCODE")).Trim().Equals(dr["QR_IT_SCODE"].ToString().Trim()))
                            {
                                
                                Tag_DT_QR.Rows.Add(dr);
                                lbl_Cnt.Text = (int.Parse(lbl_Cnt.Text) + 1).ToString();
                                break;
                            }
                        }
                        dataGridView2.DataSource = Tag_DT_QR;
                    }
                     */
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }

        public void saveDataRow_QR_식별표(string str)
        {
            try
            {
                //DataRow dr_M = null;
                string[] strTags = str.Split('*');
                if (strTags.Length == 2)
                {
                    if (strTags[0].Equals("QR"))
                    {
                        if (strTags[1].Substring(0, 4).Equals("U678"))//기아 식별표
                        {
                            string[] H_str = Regex.Split(strTags[1], "U678");
                            string it_scode = "",lot_no="",ser_no="",sdate ="";
                            float sqty = 0;
                            sqty = float.Parse(H_str[1].Substring(H_str[1].Length - 5,5));
                            it_scode = H_str[1].Substring(0, H_str[1].Length-5);
                            DataRow[] drArry = DT_IT_MASTER.Select("IT_SCODE='"+it_scode.Trim()+"'");
                            if (drArry.Length == 0)
                            {
                                MessageBox.Show("공장대체품 정보에 등록 되지 않은 품목입니다.\r\n" + it_scode.Trim() + "");
                                return;
                            }
                            lot_no = "U678"+H_str[2];
                            sdate = H_str[2].Substring(0,H_str[2].Length-6);
                            ser_no = H_str[2].Substring(H_str[2].Length - 6,6);
                            DataRow drr = Tag_DT_QR.NewRow();
                            drr["QR_IT_SCODE"] = it_scode;
                            drr["QR_SEQ"] = ser_no;
                            drr["QR_LOT_NO"] = lot_no;
                            drr["QR_SQTY"] = sqty;
                            drr["INPUT_SQTY"] = sqty;
                            drr["QR_SDATE"] = sdate;
                            drr["QR_CHECK_YN"] = "Y";
                            drr["GUBN"] = "KIA";
                            drr["ME_SCODE"] = "";
                            drr["QR_LOC_CODE"] = "";
                            if (GET_DATA.기아식별표완료체크(it_scode, ser_no, lot_no))//체크 기능
                            {
                                
                                drr["CHECK_YN"] = "Y";
                                Tag_DT_QR.Rows.Add(drr);
                            }
                            else
                            {
                                drr["CHECK_YN"] = "N";
                                Tag_DT_QR.Rows.Add(drr);
                            }
                            
                        }
                        else//모비스 식별표
                        {
                            string str_sdate = strTags[1].Substring(0, 8);//날짜
                            string str_it_scode = strTags[1].Substring(26, strTags[1].Length - 26);//품목코드
                            float sqty = float.Parse(strTags[1].Substring(10, 5));//수량
                            string str_seq = strTags[1].Substring(8, 2);//SEQ
                            string str_lot = strTags[1].Substring(15, 7);//lot
                            DataRow[] drArry = DT_IT_MASTER.Select("IT_SCODE='" + str_it_scode.Trim() + "'");
                            if (drArry.Length == 0)
                            {
                                MessageBox.Show("공장대체품 정보에 등록 되지 않은 품목입니다.\r\n" + str_it_scode.Trim() + "");
                                return;
                            }
                            DataRow[] Dr = Tag_DT_QR.Select("QR_IT_SCODE='" + str_it_scode + "' AND QR_LOT_NO='" + str_lot + "' AND QR_SEQ='" + str_seq + "'");

                            if (Dr.Length < 1)
                            {
                                //DataRow ddrr = GET_DATA.Get_Shipment_Select_NEW(str_it_scode, str_lot, Tag_DT_QR);
                                DataRow drr = Tag_DT_QR.NewRow();
                                drr["QR_IT_SCODE"] = str_it_scode;
                                drr["QR_SEQ"] = str_seq;
                                drr["QR_LOT_NO"] = str_lot;
                                drr["QR_SQTY"] = sqty;
                                drr["INPUT_SQTY"] = sqty;
                                drr["QR_SDATE"] = str_sdate;
                                drr["QR_CHECK_YN"] = "Y";
                                drr["GUBN"] = "MOBIS";
                                drr["QR_LOC_CODE"] = "";


                                //외주 완제품
                                if (GET_DATA.외주완제품_체크(str_it_scode))
                                {
                                    if (GET_DATA.외주완제품_식별표완료체크(str_it_scode, str_seq, str_lot))
                                    {
                                        if (drr != null)
                                        {
                                            drr["ME_SCODE"] = "70";
                                            drr["CHECK_YN"] = "Y";
                                            Tag_DT_QR.Rows.Add(drr);
                                            lbl_Cnt2.Text = Tag_DT_QR.Rows.Count.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (drr != null)
                                        {
                                            drr["ME_SCODE"] = "70";
                                            drr["CHECK_YN"] = "N";
                                            Tag_DT_QR.Rows.Add(drr);
                                        }
                                    }
                                }
                                else//완제품
                                {
                                    if (GET_DATA.식별표완료체크(str_it_scode, str_seq, str_lot))
                                    {
                                        if (drr != null)
                                        {
                                            drr["ME_SCODE"] = "40";
                                            drr["CHECK_YN"] = "Y";
                                            Tag_DT_QR.Rows.Add(drr);
                                            lbl_Cnt2.Text = Tag_DT_QR.Rows.Count.ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (drr != null)
                                        {
                                            drr["ME_SCODE"] = "40";
                                            drr["CHECK_YN"] = "N";
                                            Tag_DT_QR.Rows.Add(drr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (str.Substring(0, 4).Equals("U678"))
                {
                    string[] hd_arry = str.Split(' ');
                    string it_scode = hd_arry[0].Substring(4, hd_arry[0].Length - 4);
                    float sqty = float.Parse(hd_arry[1].Substring(0, 5));
                    string date_str = hd_arry[1].Substring(5, 3);
                    string ser_no = hd_arry[1].Substring(8, 4);
                    DataRow[] drArry = DT_IT_MASTER.Select("IT_SCODE='" + it_scode.Trim() + "'");
                    if (drArry.Length == 0)
                    {
                        MessageBox.Show("공장대체품 정보에 등록 되지 않은 품목입니다.\r\n" + it_scode.Trim() + "");
                        return;
                    }
                    DataRow drr = Tag_DT_QR.NewRow();
                    drr["QR_IT_SCODE"] = it_scode;
                    drr["QR_SEQ"] = ser_no;
                    drr["QR_LOT_NO"] = date_str;
                    drr["QR_SQTY"] = sqty;
                    drr["INPUT_SQTY"] = sqty;
                    drr["QR_SDATE"] = "";
                    drr["QR_CHECK_YN"] = "Y";
                    drr["GUBN"] = "HD";
                    drr["ME_SCODE"] = "";
                    if (GET_DATA.현대식별표완료체크(it_scode, ser_no, date_str))//체크 기능
                    {

                        drr["CHECK_YN"] = "Y";
                        Tag_DT_QR.Rows.Add(drr);
                    }
                    else
                    {
                        drr["CHECK_YN"] = "N";
                        Tag_DT_QR.Rows.Add(drr);
                    }

                }
                foreach (DataGridViewRow dgvr in dataGridView1.Rows)    
                {
                    if (dgvr.Cells["QR_CHECK_YN"].Value.Equals("Y"))
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.Cyan;
                        lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text) + 1).ToString();
                    }
                    else
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.White;
                    }
                    if (dgvr.Cells["CHECK_YN"].Value.Equals("N"))
                    {
                        dgvr.DefaultCellStyle.BackColor = Color.Red;
                        
                    }
                    
                }
                GridControl_Parse(Tag_DT_QR);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public void GridControl_Parse(DataTable DT)
        {
            DataTable TEMP_DT = GetGroupedBy(DT, "QR_IT_SCODE,INPUT_SQTY", "QR_IT_SCODE", "SUM");
            //gridControl3.DataSource = null;

            foreach (DataRow dr in TEMP_DT.Rows)
            {
                DataRow[] datarow = DT_FINAL.Select("QR_IT_SCODE='" + dr["QR_IT_SCODE"].ToString() + "'");
                if (datarow.Length > 0)
                {
                    datarow[0]["QR_SQTY"] = dr["INPUT_SQTY"].ToString();
                }
                else
                {
                    DataRow Ndr = DT_FINAL.NewRow();
                    Ndr["QR_IT_SCODE"] = dr["QR_IT_SCODE"];

                    Ndr["QR_SQTY"] = dr["INPUT_SQTY"];
                    
                    DataTable DT_IT = GET_DATA.공장대체품목_LOCATION_정보(dr["QR_IT_SCODE"].ToString());
                    
                    
                    
                        foreach (DataRow drr in DT_IT.Rows)
                        {
                            Ndr["FROM_SITE_CODE"] = main_site_code;
                            Ndr["FROM_SITE_NAME"] = main_site_code == "D001" ? "신아산" : main_site_code == "A001" ? "아산" :main_site_code == "U001" ? "울산" :main_site_code == "K001" ? "경주" :"";
                            Ndr["FROM_LOC_CODE"] = drr["FROM_LOC_CODE"].ToString();
                            Ndr["FROM_LOC_NAME"] = drr["FROM_LOC_NAME"].ToString();

                            Ndr["TO_SITE_CODE"] = drr["TO_SITE_CODE"].ToString();
                            Ndr["TO_SITE_NAME"] = drr["TO_SITE_NAME"].ToString();
                            Ndr["TO_LOC_CODE"] = drr["TO_LOC_CODE"].ToString();
                            Ndr["TO_LOC_NAME"] = drr["TO_LOC_NAME"].ToString();
                            Ndr["MOVE_COST"] = float.Parse(drr["MOVE_COST"].ToString());
                        }

                    DT_FINAL.Rows.Add(Ndr);
                }
            }
            foreach (DataRow drr in DT_FINAL.Rows)
            {
                DataRow[] datarow = TEMP_DT.Select("QR_IT_SCODE='" + drr["QR_IT_SCODE"].ToString() + "'");
                if (datarow.Length < 1)
                {
                    DT_FINAL.Rows.Remove(drr);
                    break;
                }
            }
        }
    
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
            
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    //Swing.InventoryStop();
                    //Swing.TagListClear();

                    //dsmListView_ivt.Items.Clear();
                    //ddc_ivt.DigitText = "00000";

                    //Tag_DT_PP.Rows.Clear();
                    //Tag_DT_QR.Rows.Clear();
                    
                    Swing.SetRFPower(25);
                    Swing.ReportAllInformation();

                    //select_shipment();
                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            formClear();
            
        }
        #endregion
        ContextMenuStrip remove_menu;

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();
            Swing.ConnectionClose();
            parentForm.Visible = true;
        }
        

        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }
        
           
        //QR dataRow 저장
        public DataRow saveDataRow_QR(string str)
        {
            DataRow dr=null;
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("QR"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 4)
                    {
                        dr = Tag_DT_QR.NewRow();
                        dr["QR_SDATE"] = strTag[0];
                        dr["QR_IT_SCODE"] = strTag[1];
                        dr["QR_SQTY"] = strTag[2];
                        dr["QR_SERNO"] = strTag[3];
                        dr["QR_CODE"] = strTags[1];

                    }
                }
            }
            return dr;
        }
        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check(string prdt_item, string card_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
       

        //입고표 등록번호 가지고 오기
        private string getNewNumberToSave()
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            string query = "INSERT INTO LOC_DOCUMENT_GETNUM DEFAULT VALUES";
            string result = "";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = null;
            try
            {

                cmd.ExecuteNonQuery();
                cmd.CommandText = "SELECT MAX(IDNUM) FROM LOC_DOCUMENT_GETNUM";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = reader[0].ToString().Trim();
                }
                reader.Close();
                result = fixNo(result);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        private string fixNo(string slSnumb)
        {
            string result = "00000000";
            result = result.Substring(slSnumb.Length) + slSnumb;
            return result;
        }

        

        public bool carrier_check(string carrier_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from CARRIER_REG WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }


        private void Injection_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Swing.InventoryStop();
            this.Close();
        }

        private void timer_now_Tick(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            N_timer.Text = now.ToString();
        }

        public void formClear()
        {
            //lbl_Cnt.Text = "0";
            sh_snumb = "";
            loc_code = "";
            Tag_DT_QR.Rows.Clear();
            DT_FINAL.Rows.Clear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStop();
            Swing.TagListClear();
            Swing.InventoryStart();
            lbl_Cnt3.Text = "0";
            lbl_Cnt2.Text = "0";
        }
        

        public void shipment_save(DataTable DT_hd, DataTable DT_det)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);

            SqlDataAdapter da = new SqlDataAdapter();
            SqlTransaction trans;
            da.InsertCommand = new SqlCommand("INSERT INTO SHIPMENT_HD(SH_SNUMB,SH_SDATE,SH_ODATE,SH_DELIV,DELIVERY,PC_SCODE,SH_VNUMB,SH_SHIPPERS,CHECK_YN)  "
                    + " VALUES(@SH_SNUMB,@SH_SDATE,@SH_ODATE,@SH_DELIV,@DELIVERY,@PC_SCODE,@SH_VNUMB,@SH_SHIPPERS,@CHECK_YN)", conn);

            da.InsertCommand.Parameters.Add("@SH_SNUMB", SqlDbType.VarChar, 8, "SH_SNUMB");
            da.InsertCommand.Parameters.Add("@SH_SDATE", SqlDbType.VarChar, 8, "SH_SDATE");
            da.InsertCommand.Parameters.Add("@SH_ODATE", SqlDbType.VarChar, 8, "SH_ODATE");
            //da.InsertCommand.Parameters.Add("@MN_SCODE", SqlDbType.VarChar, 10, "MN_SCODE");
            da.InsertCommand.Parameters.Add("@SH_DELIV", SqlDbType.VarChar, 30, "SH_DELIV");
            da.InsertCommand.Parameters.Add("@DELIVERY", SqlDbType.VarChar, 50, "DELIVERY");
            da.InsertCommand.Parameters.Add("@PC_SCODE", SqlDbType.VarChar, 8, "PC_SCODE");
            //da.InsertCommand.Parameters.Add("@SH_ORDNO", SqlDbType.Int, 8, "SH_ORDNO");
            da.InsertCommand.Parameters.Add("@SH_VNUMB", SqlDbType.VarChar, 20, "SH_VNUMB");
            da.InsertCommand.Parameters.Add("@SH_SHIPPERS", SqlDbType.VarChar, 50, "SH_SHIPPERS");
            da.InsertCommand.Parameters.Add("@CHECK_YN", SqlDbType.VarChar, 1, "CHECK_YN");
            //da.InsertCommand.Parameters.Add("@SH_STIME", SqlDbType.VarChar, 10, "SH_STIME");
            //da.InsertCommand.Parameters.Add("@SH_ETIME", SqlDbType.VarChar, 10, "SH_ETIME");
            //da.InsertCommand.Parameters.Add("@LOC_CODE", SqlDbType.VarChar, 10, "LOC_CODE");

            conn.Open();

            trans = conn.BeginTransaction();

            da.InsertCommand.Transaction = trans;
            try
            {
                da.Update(DT_hd);
                SqlDataAdapter da_det = new SqlDataAdapter();

                da_det.InsertCommand = new SqlCommand("INSERT INTO SHIPMENT_DET(SH_SNUMB,SH_SERNO,IT_SCODE,SH_SQTY,LOC_CODE,SH_DESCR)  "
                        + " VALUES(@SH_SNUMB,@SH_SERNO,@IT_SCODE,@SH_SQTY,@LOC_CODE,@SH_DESCR)", conn);

                da_det.InsertCommand.Parameters.Add("@SH_SNUMB", SqlDbType.VarChar, 8, "SH_SNUMB");
                da_det.InsertCommand.Parameters.Add("@SH_SERNO", SqlDbType.Int, 8, "SH_SERNO");
                da_det.InsertCommand.Parameters.Add("@IT_SCODE", SqlDbType.VarChar, 15, "IT_SCODE");
                da_det.InsertCommand.Parameters.Add("@SH_SQTY", SqlDbType.Int, 8, "SH_SQTY");
                da_det.InsertCommand.Parameters.Add("@LOC_CODE", SqlDbType.VarChar, 10, "LOC_CODE");
                da_det.InsertCommand.Parameters.Add("@SH_DESCR", SqlDbType.VarChar, 200, "SH_DESCR");
                da_det.InsertCommand.Transaction = trans;
                da_det.Update(DT_det);
                trans.Commit();
                //gridControl2.DataSource = GetGroupedBy(DT_det, "IT_SCODE,SH_SQTY", "IT_SCODE", "SUM");
                //excel_export();
                

                gridControl2.DataSource = null;
            }
            catch(Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("오류"+ex.Message);
            }
            finally
            {
                conn.Close();

            }
        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        //출하지시 채번
        private string getNewshipmentToSave()
        {
            string strCon = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            string query = "DECLARE @MM_SNUMB VARCHAR(10) INSERT INTO SHIPMENT_GETNUM DEFAULT VALUES"
                + " SET @MM_SNUMB = @@IDENTITY; SELECT @MM_SNUMB AS MM_SNUMB";
            string result = "";
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader reader = null;
            try
            {

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = reader[0].ToString().Trim();
                }
                reader.Close();
                result = fixNo_shipment(result);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
        private string fixNo_shipment(string sh_snumb)
        {
            string result = "000000";
            result = "SH" + result.Substring(sh_snumb.Length) + sh_snumb;
            return result;
        }
        private void advBandedGridView1_CustomDrawRowIndicator(object sender, RowIndicatorCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.Info.IsRowIndicator && e.RowHandle >= 0)
            {
                e.Info.DisplayText = (e.RowHandle+1).ToString();
            }
        }

        private void advBandedGridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            MessageBox.Show(e.CellValue.ToString());
        }

        private void btn_shipment_search_Click(object sender, EventArgs e)
        {
            //select_shipment();
        }
        /*
        private void select_shipment()
        {
            if (Swing.IsOpen)
            {
                Shipment_search_popup Shipment_search_popup = new Shipment_search_popup();
                if (Shipment_search_popup.ShowDialog() == DialogResult.OK)
                {
                    formClear();
                    sh_snumb = Shipment_search_popup.sh_snumb;
                    loc_code = Shipment_search_popup.loc_code;
                    DTshipment_det = GET_DATA.getDataGridView_ship(sh_snumb);
                    gridControl1.DataSource = DTshipment_det;
                    Tag_DT_PP = GET_DATA.Get_Shipment_Select(DTshipment_det);
                    dataGridView3.Columns["PP_ROWNUM"].DisplayIndex = 0;
                    dataGridView3.Columns["PP_PRDT_ITEM"].DisplayIndex = 1;
                    dataGridView3.Columns["PP_CARRIER_NO"].DisplayIndex = 2;
                    dataGridView3.Columns["PP_CARD_NO"].DisplayIndex = 3;
                    dataGridView3.Columns["MM_SQTY"].DisplayIndex = 4;
                    dataGridView3.Columns["SQTY"].DisplayIndex = 5;
                    dataGridView3.Columns["PP_CHECK_YN"].DisplayIndex = 6;
                    dataGridView3.Columns["BTN_DELETE"].DisplayIndex = 7;
                    dataGridView3.Columns["RSRV_NO"].DisplayIndex = 8;
                    dataGridView3.Columns["PP_VW_SNUMB"].DisplayIndex = 9;
                    dataGridView3.Columns["PP_DATE"].DisplayIndex = 10;
                    dataGridView3.DataSource = Tag_DT_PP;
                    
                    
                    lbl_Cnt3.Text = Tag_DT_PP.Rows.Count.ToString();
                }
            }
            else
            {
                MessageBox.Show("리더기를 연결 해주세요.");
            }
        }
        */
        


        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(getNewshipmentToSave());
        }

        private void lueLoc_Car_code_EditValueChanged(object sender, EventArgs e)
        {
            
        }
        //식별표 QR코드 읽은 데이터로 출하
        private void btn_shipment_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataRow drr in DT_FINAL.Rows)
                {
                    if (string.IsNullOrWhiteSpace(drr["FROM_LOC_CODE"].ToString()))
                    {
                        MessageBox.Show("출고로케이션이 누락 되었습니다.\r\n로케이션을 지정해 주세요.");
                        return;
                    }
                    if (string.IsNullOrWhiteSpace(drr["TO_LOC_CODE"].ToString()))
                    {
                        MessageBox.Show("입고로케이션이 누락 되었습니다.\r\n로케이션을 지정해 주세요.");
                        return;
                    }
                    if (int.Parse(drr["QR_SQTY"].ToString()) == 0)
                    {
                        MessageBox.Show("납품 수량이 0 일 수 없습니다.\r\n수량을 확인해 주세요.");
                        return;
                    }
                    if (int.Parse(drr["MOVE_COST"].ToString()) == 0)
                    {
                        MessageBox.Show("사내대체가 금액이 0원 입니다. 출고 할 수 없습니다.");
                        return;
                    }
                }

                if (Tag_DT_QR.Rows.Count < 1)
                {
                    MessageBox.Show("공장대체할 식별표를 읽지 않았습니다.");
                    return;
                }
                if (DialogResult.OK == MessageBox.Show("출고 하시겠습니까?", "공장대체", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                {
                    //출하지시 등록번호 채번
                    string sh_snumb = "";
                    sh_snumb = getNewshipmentToSave();
                    //출하지시 헤더 
                    string SH_DELIV = "";
                    string SH_VNUMB = "";
                    string SH_SHIPPERS = "";
                    if (string.IsNullOrWhiteSpace(lueLoc_Car_code.Text.Trim()))
                    {
                        SH_DELIV = "";
                        SH_SHIPPERS = "";
                    }
                    else
                    {
                        SH_DELIV = lueLoc_Car_code.GetColumnValue("LC_CODE").ToString();
                        SH_SHIPPERS = lueLoc_Car_code.GetColumnValue("DV_CODE").ToString();
                    }
                    //SH_VNUMB = lueLoc_Car_code.GetColumnValue("CAR_NUMB").ToString();

                    //str_loc_code
                    //str_delivery
                    
                    DataTable insertHd = new DataTable();
                    insertHd.Columns.Add("SH_SNUMB", typeof(string));
                    insertHd.Columns.Add("SH_SDATE", typeof(string));
                    insertHd.Columns.Add("SH_ODATE", typeof(string));
                    insertHd.Columns.Add("SH_DELIV", typeof(string));
                    insertHd.Columns.Add("DELIVERY", typeof(string));
                    insertHd.Columns.Add("PC_SCODE", typeof(string));
                    insertHd.Columns.Add("SH_VNUMB", typeof(string));
                    insertHd.Columns.Add("SH_SHIPPERS", typeof(string));
                    insertHd.Columns.Add("CHECK_YN", typeof(string));
                    

                    DataRow insertRowHD = insertHd.NewRow();
                    insertRowHD["SH_SNUMB"] = sh_snumb;
                    insertRowHD["SH_SDATE"] = DateTime.Now.ToString("yyyyMMdd");
                    insertRowHD["SH_ODATE"] = DateTime.Now.ToString("yyyyMMdd");
                    insertRowHD["SH_DELIV"] = SH_DELIV;
                    insertRowHD["DELIVERY"] = str_delivery;
                    insertRowHD["PC_SCODE"] = str_pc_scode;
                    insertRowHD["SH_VNUMB"] = SH_VNUMB;
                    insertRowHD["SH_SHIPPERS"] = SH_SHIPPERS;
                    insertRowHD["CHECK_YN"] = "Y";
                    
                    insertHd.Rows.Add(insertRowHD);
                    
                    //출하지시 디테일 
                    DataTable insertDet = new DataTable();
                    insertDet.Columns.Add("SH_SNUMB", typeof(string));
                    insertDet.Columns.Add("SH_SERNO", typeof(int));
                    insertDet.Columns.Add("IT_SCODE", typeof(string));
                    insertDet.Columns.Add("SH_SQTY", typeof(int));
                    insertDet.Columns.Add("LOC_CODE", typeof(string));
                    insertDet.Columns.Add("SH_DESCR", typeof(string));
                    //품목별로 정렬
                    Tag_DT_QR.DefaultView.Sort = "QR_IT_SCODE ASC";

                    int serno = 1;
                    
                    foreach (DataRow dr in DT_FINAL.Rows)
                    {
                        int sh_sqty = 0;
                        //DataRow[] datarow = Tag_DT_QR.Select("QR_IT_SCODE='" + after_it_scode + "' AND CHECK_YN='Y'");

                        DataRow[] CHECK_DR = insertDet.Select("IT_SCODE = '" + dr["QR_IT_SCODE"].ToString() + "'");
                        if (CHECK_DR.Length == 0)
                        {
                            DataRow insertRowDET = insertDet.NewRow();
                            insertRowDET["SH_SNUMB"] = sh_snumb;
                            insertRowDET["SH_SERNO"] = serno;
                            insertRowDET["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                            insertRowDET["SH_SQTY"] = dr["QR_SQTY"].ToString();
                            insertRowDET["LOC_CODE"] = dr["TO_LOC_CODE"].ToString();
                            insertRowDET["SH_DESCR"] = "공장간대체:" + dr["TO_SITE_NAME"].ToString() + " 창고(" + dr["TO_LOC_CODE"].ToString() + ")";
                            insertDet.Rows.Add(insertRowDET);
                            serno = serno + 1;
                        }
                    }
                    
                    bool move_check = false;
                    //공장대체 전표 발생 (출고, 입고)
                    DT_FINAL.DefaultView.Sort = "TO_SITE_CODE,TO_LOC_CODE ASC";
                    
                    move_check = MOVE_FUNC.move_insert_site_move(sh_snumb, DT_FINAL);
                    
                                        
                    if (move_check)
                    {
                        foreach (DataRow dr in Tag_DT_QR.Rows)
                        {
                            if (dr["GUBN"].ToString().Equals("MOBIS"))
                            {


                                if (int.Parse(dr["INPUT_SQTY"].ToString()) > 0)
                                {
                                    DataRow udr = DT_it_chart_update.NewRow();
                                    udr["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                                    udr["SEQ"] = dr["QR_SEQ"].ToString();
                                    udr["LOT_NO"] = dr["QR_LOT_NO"].ToString();
                                    DT_it_chart_update.Rows.Add(udr);
                                    //식별표 정보 업데이트

                                }
                            }
                            else if (dr["GUBN"].ToString().Equals("KIA"))
                            {
                                if (int.Parse(dr["INPUT_SQTY"].ToString()) > 0)
                                {
                                    DataRow udr = KIA_it_chart_update.NewRow();
                                    udr["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                                    udr["SEQ"] = dr["QR_SEQ"].ToString();
                                    udr["LOT_NO"] = dr["QR_LOT_NO"].ToString();
                                    KIA_it_chart_update.Rows.Add(udr);
                                    //식별표 정보 업데이트

                                }
                            }
                            else if (dr["GUBN"].ToString().Equals("HD"))
                            {
                                if (int.Parse(dr["INPUT_SQTY"].ToString()) > 0)
                                {
                                    DataRow udr = HD_it_chart_update.NewRow();
                                    udr["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                                    udr["SEQ"] = dr["QR_SEQ"].ToString();
                                    udr["LOT_NO"] = dr["QR_LOT_NO"].ToString();
                                    HD_it_chart_update.Rows.Add(udr);
                                    //식별표 정보 업데이트

                                }
                            }
                        }

                        if (DT_it_chart_update.Rows.Count > 0)
                        {
                            GET_DATA.식별표구분자업데이트(DT_it_chart_update);//모비스 식별표 구분자 업데이트
                        }
                        if (KIA_it_chart_update.Rows.Count > 0)
                        {
                            GET_DATA.식별표구분자업데이트_KIA(KIA_it_chart_update);//기아 식별표 구분자 업데이트
                        }
                        if (HD_it_chart_update.Rows.Count > 0)
                        {
                            GET_DATA.식별표구분자업데이트_HD(HD_it_chart_update);//현대 식별표 구분자 업데이트
                        }
                        //출하지시 저장
                        shipment_save(insertHd, insertDet);
                        
                        MessageBox.Show("출하가 완료 되었습니다.");
                        formClear();
                    }
                    else
                    {
                        MessageBox.Show("출하 도중 오류가 발생 하였습니다.");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR(출하) : " + ex.Message);
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {

                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "INPUT_SQTY":
                        Sqty_popup Sqty_popup = new Sqty_popup();
                        Sqty_popup.minus_enabled = true;
                        Sqty_popup.sqty = senderGrid.Rows[e.RowIndex].Cells["QR_SQTY"].Value.ToString();
                        if (Sqty_popup.ShowDialog() == DialogResult.OK)
                        {

                            int remain_sqty = int.Parse(senderGrid.Rows[e.RowIndex].Cells["QR_SQTY"].Value.ToString());
                            //if (remain_sqty < int.Parse(Sqty_popup.sqty))
                            //{
                            //    MessageBox.Show("기존수량을 초과 할 수 없습니다.");
                            //    return;
                            //}
                            senderGrid.Rows[e.RowIndex].Cells["INPUT_SQTY"].Value = Sqty_popup.sqty;
                            //DataRow[] dr = Tag_DT_QR.Select("QR_IT_SCODE='" + senderGrid.Rows[e.RowIndex].Cells["QR_IT_SCODE"].Value.ToString() + "'");
                            //int sum_sqty=0;
                            //for (int i = 0; i < dr.Length; i++)
                            //{
                            //    sum_sqty += int.Parse(dr[i]["INPUT_SQTY"].ToString());
                            //}


                            senderGrid.Rows[e.RowIndex].Cells["QR_CHECK_YN"].Value = "Y";

                            lbl_Cnt2.Text = "0";
                            foreach (DataGridViewRow dgvr in dataGridView1.Rows)
                            {
                                if (dgvr.Cells["QR_CHECK_YN"].Value.Equals("Y"))
                                {
                                    dgvr.DefaultCellStyle.BackColor = Color.Cyan;
                                    lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text) + 1).ToString();
                                }
                                else
                                {
                                    dgvr.DefaultCellStyle.BackColor = Color.White;
                                }
                                if (dgvr.Cells["CHECK_YN"].Value.Equals("N"))
                                {
                                    dgvr.DefaultCellStyle.BackColor = Color.Red;

                                }
                            }
                        }

                        break;
                    case "QR_BTN_DELETE":
                        if (senderGrid.Rows[e.RowIndex].Cells["QR_CHECK_YN"].Value.Equals("Y"))
                            lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text.Trim()) - 1).ToString();


                        senderGrid.Rows.RemoveAt(e.RowIndex);


                        break;
                }
                GridControl_Parse(Tag_DT_QR);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            saveDataRow_QR_식별표(textBox1.Text.Trim());
            //this.gridView3.PostEditor();
            //this.gridView3.SetFocusedRowCellValue("QR_LOC_CODE", null);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Shipment_List.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            getFtp_Upload_File(textEdit1.Text.Trim());
        }
        public bool getFtp_Upload_File(string sh_snumb)
        {
            bool check = false;
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_GET_SHIP_DATA";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("SH_SNUMB", sh_snumb);
            da.SelectCommand.Parameters.AddWithValue("SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            try
            {
                ds = new DataSet();

                da.Fill(ds, "FTP_DATA");
                dt = ds.Tables["FTP_DATA"];


                DateTime Date = DateTime.Now;

                string date = Date.ToString("yyyyMMdd");
                string dt_str = Date.ToString("yyyyMMddHHmmss");
                string file_name = "TEMP_SHIP_" + dt_str + "_" + Properties.Settings.Default.SITE_CODE.ToString();
                string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                DirectoryInfo di = new DirectoryInfo(desktop + @"\출하");
                // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
                if (!di.Exists)
                {
                    di.Create();
                }
                DirectoryInfo dir = new DirectoryInfo(desktop + @"\출하\" + date);
                // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
                if (!dir.Exists)
                {
                    dir.Create();
                }

                string file_path = desktop + @"\출하\" + date + @"\";
                string file_path_csv = file_path + file_name + ".txt";


                //MessageBox.Show(file_path + " 경로로 저장이 됩니다.");
                try
                {
                    using (CsvFileWriter writer = new CsvFileWriter(file_path_csv))
                    {
                        
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            CsvRow row = new CsvRow();
                            DataRow dr = dt.Rows[i];
                            if (i == 0)
                            {
                                for (int j = 0; j < dt.Columns.Count-10; j++)
                                {
                                    row.Add(String.Format("{0}", dr[j]));
                                }
                            }
                            else
                            {
                                for (int j = 0; j < dt.Columns.Count; j++)
                                {
                                    row.Add(String.Format("{0}", dr[j]));
                                }
                            }
                            writer.WriteRow(row);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                string ftpfile_full_path = ftpUtil.getFtpFile_Full_Path();


                //string ftp_str = AdjustDir("SHIP_DATA" + "/" + Date.ToShortDateString());

                //ftpUtil.FTPDirectioryCheck(ftp_str);

                string ext = Path.GetExtension(file_path_csv.Substring(file_path_csv.LastIndexOf("\\") + 1)).ToLower().Replace(".", "");
                string filename = file_path_csv.Substring(file_path_csv.LastIndexOf("\\") + 1).Replace(ext, "").Substring(0, file_path_csv.Substring(file_path_csv.LastIndexOf("\\") + 1).Replace(ext, "").Length - 1);

                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    ftpUtil.Upload_SHIP(file_path_csv, "dev/" + "dkind2pantos/" + filename + "." + ext, filename + "." + ext);
                    //getFileData();
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("완료");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR : " + ex.Message);
                }
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }

        private void btn_detail_change_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Visible)
            {
                dataGridView1.Visible = false;
                btn_detail_change.Text = "상세보기(전환)";
                dataGridView1.SendToBack();
            }
            else
            {
                dataGridView1.Visible = true;
                btn_detail_change.Text = "집계(전환)";
                dataGridView1.BringToFront();
            }
        }

        private void gridView3_ShownEditor(object sender, EventArgs e)
        {
            
            //ColumnView view = (ColumnView)sender;
            //if (view.FocusedColumn.FieldName == "QR_LOC_CODE")
            //{
            //    LookUpEdit editor = (LookUpEdit)view.ActiveEditor;
            //    string it_scode = Convert.ToString(view.GetFocusedRowCellValue("QR_IT_SCODE"));

            //    editor.Properties.DataSource = GET_DATA.OUT_LoccodeSelect_DropDownTOit_scode(it_scode);
            //    editor.Properties.DisplayMember = "LOC_NAME";
            //    editor.Properties.ValueMember = "LOC_CODE";
            //    //if (DT.Rows.Count == 1)
            //    //{
            //    //    editor.ItemIndex = 1;
            //    //}
            //}

        }

        private void btn_Item_Add_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lueIt_Scode.Text.Trim()))
            {
                MessageBox.Show("품목을 선택해 주세요");
                return;
            }
            foreach (DataRow dr in DT_FINAL.Rows)
            {
                if (dr["QR_IT_SCODE"].ToString().Equals(lueIt_Scode.Text))
                {
                    MessageBox.Show("이미 추가가 되어 있는 품목 입니다.");
                    return;
                }
                
            }
            DataRow[] arry_dr = DT_IT_MASTER.Select("IT_SCODE='" + lueIt_Scode.Text.Trim() + "'");
            if (arry_dr.Length < 1)
            {
                MessageBox.Show("품목정보에 없는 품목입니다.\r\n품목을 선택해주세요");
                lueIt_Scode.Text = "";
                return;
            }
            foreach (DataRow dr in GET_DATA.공장대체품목_LOCATION_정보(lueIt_Scode.Text.Trim()).Rows)
            {
                if (string.IsNullOrWhiteSpace(dr["FROM_LOC_CODE"].ToString().Trim()))
                {
                    MessageBox.Show("로케이션이 등록되어 있지 않습니다.\n해당 품목의 로케이션을 등록해주세요.");
                    return;
                }
                string to_site = dr["TO_SITE_CODE"].ToString().Trim();
                if (!string.IsNullOrWhiteSpace(to_site))
                {
                    if (!CHECK_FUNC.chkSiteIt(to_site, lueIt_Scode.Text.Trim()))
                    {
                        string site = to_site == "A001" ? "아산" : to_site == "D001" ? "신아산" : to_site == "K001" ? "경주" : to_site == "U001" ? "울산" : to_site;
                        MessageBox.Show(site + " 공장에 품목이 존재 하지 않습니다.\n품번 : " + lueIt_Scode.Text.Trim());
                        return;
                    }
                }

            }
            
            DataRow drr = Tag_DT_QR.NewRow();
            drr["QR_IT_SCODE"] = lueIt_Scode.GetColumnValue("IT_SCODE").ToString().Trim();
            drr["QR_SEQ"] = "";
            drr["QR_LOT_NO"] = "";
            drr["QR_SQTY"] = (lueIt_Scode.GetColumnValue("IT_PKQTY").ToString() == "") ? 0 : int.Parse(lueIt_Scode.GetColumnValue("IT_PKQTY").ToString());
            drr["INPUT_SQTY"] = (lueIt_Scode.GetColumnValue("IT_PKQTY").ToString() == "") ? 0 : int.Parse(lueIt_Scode.GetColumnValue("IT_PKQTY").ToString());
            drr["QR_SDATE"] = DateTime.Now.ToString("yyyyMMdd");
            drr["QR_CHECK_YN"] = "Y";
            drr["GUBN"] = "MOBIS";
            drr["QR_LOC_CODE"] = "";
            drr["ME_SCODE"] = lueIt_Scode.GetColumnValue("ME_SCODE").ToString();
            drr["CHECK_YN"] = "Y";
            Tag_DT_QR.Rows.Add(drr);
            GridControl_Parse(Tag_DT_QR);
            lueIt_Scode.Text = "";
        }
    }
}
