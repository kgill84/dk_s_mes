﻿using DK_Tablet.FUNCTION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class FME_AttachFileForm : Form
    {
        string path = "ftp://203.251.168.131:4104";
        string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        string root = "INSPECTION_RESULT";
        string ftp_str = "";
        FtpUtil ftpUtil;

        public string plant { get; set; }
        public string sdate { get; set; }
        public string dayNight { get; set; }
        public string wc_code { get; set; }
        public string it_scode { get; set; }

        public FME_AttachFileForm()
        {
            InitializeComponent();
        }

        private void AttachFileForm_Load(object sender, EventArgs e)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.ftp_str = string.Format("{0}{1}{2}{3}{4}", AdjustDir(this.plant), AdjustDir(this.sdate),
                AdjustDir(this.dayNight), AdjustDir(this.wc_code), AdjustDir(this.it_scode));
            getList(ftp_str);
        }

        private void getList(string ftp_str)
        {
            string ftpFileFullPath = string.Format("{0}{1}{2}", this.path, AdjustDir(this.root), ftp_str);

            FTPDirectioryCheck_file(ftp_str);

            FtpWebRequest ftpWebRequest = WebRequest.Create(new Uri(ftpFileFullPath)) as FtpWebRequest;
            ftpWebRequest.Credentials = new NetworkCredential(this.ftpID, this.ftpPass);
            ftpWebRequest.UseBinary = true;
            ftpWebRequest.UsePassive = true;
            ftpWebRequest.Timeout = 10000;
            ftpWebRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = null;

            DataTable dt = new DataTable();
            dt.Columns.Add("FILE_NAME");
            dt.Columns.Add("FILE_DOWN");
            dt.Columns.Add("FILE_DEL");

            try
            {
                response = ftpWebRequest.GetResponse() as FtpWebResponse;

                if (response != null)
                {
                    try
                    {
                        StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                        string[] data_arr = streamReader.ReadToEnd().Split(new char[] { '\r', '\n' });

                        foreach (string str in data_arr)
                        {
                            if (!string.IsNullOrWhiteSpace(str))
                            {
                                DataRow dr = dt.NewRow();
                                dr["FILE_NAME"] = str;
                                dr["FILE_DOWN"] = "다운";
                                dr["FILE_DEL"] = "삭제";
                                dt.Rows.Add(dr);
                            }
                        }

                        gridControl2.DataSource = dt;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error(WebClient)dddddddd : " + ex.Message);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                if (response != null) response.Close();
            }
        }

        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }

        public void FTPDirectioryCheck_file(string directoryPath)
        {
            directoryPath = AdjustDir(root) + directoryPath;

            string[] directoryPaths = directoryPath.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

            string currentDirectory = string.Empty;
            foreach (string directory in directoryPaths)
            {
                currentDirectory += string.Format("/{0}", directory);
                if (!IsExtistDirectory(currentDirectory))
                {
                    MakeDirectory(currentDirectory);
                }
            }
        }

        private bool IsExtistDirectory(string currentDirectory)
        {
            string ftpFileFullPath = string.Format("{0}{1}", this.path, GetParentDirectory(currentDirectory));

            FtpWebRequest ftpWebRequest = WebRequest.Create(new Uri(ftpFileFullPath)) as FtpWebRequest;
            ftpWebRequest.Credentials = new NetworkCredential(this.ftpID, this.ftpPass);
            ftpWebRequest.UseBinary = true;
            ftpWebRequest.UsePassive = true;
            ftpWebRequest.Timeout = 10000;
            ftpWebRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            FtpWebResponse response = null;
            string data = string.Empty;

            try
            {
                response = ftpWebRequest.GetResponse() as FtpWebResponse;
                if (response != null)
                {
                    StreamReader streamReader = new StreamReader(response.GetResponseStream(), Encoding.Default);

                    data = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n다시 시도해주세요");
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            string[] directorys = data.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            return (from directory in directorys
                    select directory.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                        into directoryInfos
                        where directoryInfos[0][0] == 'd'
                        select directoryInfos[8]).Any(
                        name => name == (currentDirectory.Split('/')[currentDirectory.Split('/').Length - 1]).ToString());
        }

        private bool MakeDirectory(string dirpath)
        {
            string URI = string.Format("{0}/{1}", this.path, dirpath);
            System.Net.FtpWebRequest ftp = GetRequest(URI);
            ftp.Method = System.Net.WebRequestMethods.Ftp.MakeDirectory;

            try
            {
                string str = GetStringResponse(ftp);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private string GetParentDirectory(string currentDirectory)
        {
            string[] directorys = currentDirectory.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
            string parentDirectory = string.Empty;
            for (int i = 0; i < directorys.Length - 1; i++)
            {
                parentDirectory += "/" + directorys[i];
            }

            return parentDirectory;
        }

        private FtpWebRequest GetRequest(string URI)
        {
            FtpWebRequest result = (FtpWebRequest)WebRequest.Create(URI);
            result.Credentials = GetCredentials();
            result.KeepAlive = false;
            return result;
        }

        private ICredentials GetCredentials()
        {
            return new NetworkCredential(this.ftpID, this.ftpPass);
        }

        private string GetStringResponse(FtpWebRequest ftp)
        {
            string result = "";
            using (FtpWebResponse response = (FtpWebResponse)ftp.GetResponse())
            {
                long size = response.ContentLength;
                using (Stream datastream = response.GetResponseStream())
                {
                    if (datastream != null)
                    {
                        using (StreamReader sr = new StreamReader(datastream))
                        {
                            result = sr.ReadToEnd();
                            sr.Close();
                        }

                        datastream.Close();
                    }
                }
                response.Close();
            }
            return result;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            uploadFiles(this.sdate, this.dayNight, this.wc_code, this.it_scode);
        }

        private void uploadFiles(string sdate, string dayNight, string wc_code, string it_scode)
        {
            openFileDialog1.Dispose();
            openFileDialog1.FileName = "";
            openFileDialog1.Multiselect = true;

            string ftp_str = this.ftp_str;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager.ShowDefaultWaitForm(this, true, false, "업로드중입니다.");

                    FTPDirectioryCheck_file(ftp_str);

                    foreach (string file in openFileDialog1.FileNames)
                    {
                        string ext = Path.GetExtension(file.Substring(file.LastIndexOf("\\") + 1)).ToLower().Replace(".", "");
                        string filename = file.Substring(file.LastIndexOf("\\") + 1).Replace(ext, "").Substring(0, file.Substring(file.LastIndexOf("\\") + 1).Replace(ext, "").Length - 1);

                        try
                        {
                            ftpUtil.Upload(file, this.root + ftp_str + "/" + filename + "." + ext);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR : " + ex.Message);
                        }
                    }
                }
                catch (Exception openE)
                {
                    MessageBox.Show("파일을 여는데 오류가 발생하였습니다." + openE.Message.ToString());
                }
                finally
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseDefaultWaitForm();
                    getList(ftp_str);
                }
            }
        }

        private void advBandedGridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.RowHandle < -1) return;

            string ftp_str = this.ftp_str;
            string file_name = advBandedGridView1.GetRowCellValue(e.RowHandle, FILE_NAME).ToString();

            if (e.Column == FILE_DOWN)
            {
                downloadFile(ftp_str, file_name);
            }
            else if (e.Column == FILE_DEL)
            {
                string message = "삭제하시겠습니까?\n삭제하시면 파일복구는 불가능합니다.";

                if (DialogResult.OK == MessageBox.Show(message, "삭제여부", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                {
                    try
                    {
                        deleteFile(ftp_str, file_name);
                        MessageBox.Show("삭제가 완료되었습니다.");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        getList(ftp_str);
                    }
                }
            }
        }

        private void downloadFile(string ftp_str, string file_name)
        {
            Uri uri = new Uri(string.Format("{0}{1}{2}{3}", this.path, AdjustDir(this.root), ftp_str, AdjustDir(file_name)));

            try
            {
                using (WebClient wc = new WebClient())
                {
                    saveFileDialog1.FileName = file_name;
                    wc.Credentials = GetCredentials();

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if (saveFileDialog1.FileName != "")
                        {
                            wc.DownloadFile(uri, saveFileDialog1.FileName);
                            ProcessStartInfo startinfo = new ProcessStartInfo();
                            startinfo.FileName = saveFileDialog1.FileName;
                            Process.Start(startinfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteFile(string ftp_str, string file_name)
        {
            FtpWebRequest requestFileDelete = GetRequest(string.Format("{0}{1}{2}{3}", this.path, AdjustDir(this.root), ftp_str, AdjustDir(file_name)));
            requestFileDelete.Credentials = GetCredentials();
            requestFileDelete.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            requestFileDelete.Method = WebRequestMethods.Ftp.DeleteFile;
            FtpWebResponse responseFileDelete = (FtpWebResponse)requestFileDelete.GetResponse();
            responseFileDelete.Close();
        }
    }
}
