﻿namespace DK_Tablet
{
    partial class FME_AttachFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FILE_NAME = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FILE_DOWN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FILE_DEL = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.btn_done = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.advBandedGridView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1128, 533);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.ActiveFilterEnabled = false;
            this.advBandedGridView1.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 30F);
            this.advBandedGridView1.Appearance.BandPanel.Options.UseFont = true;
            this.advBandedGridView1.BandPanelRowHeight = 50;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1});
            this.advBandedGridView1.ColumnPanelRowHeight = 50;
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.FILE_NAME,
            this.FILE_DOWN,
            this.FILE_DEL});
            this.advBandedGridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.advBandedGridView1.GridControl = this.gridControl2;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsCustomization.AllowBandMoving = false;
            this.advBandedGridView1.OptionsCustomization.AllowFilter = false;
            this.advBandedGridView1.OptionsCustomization.AllowSort = false;
            this.advBandedGridView1.OptionsMenu.EnableColumnMenu = false;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.ShowColumnHeaders = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.OptionsView.ShowIndicator = false;
            this.advBandedGridView1.RowHeight = 50;
            this.advBandedGridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.advBandedGridView1_RowCellClick);
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "파일명";
            this.gridBand1.Columns.Add(this.FILE_NAME);
            this.gridBand1.Columns.Add(this.FILE_DOWN);
            this.gridBand1.Columns.Add(this.FILE_DEL);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 1266;
            // 
            // FILE_NAME
            // 
            this.FILE_NAME.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 23F);
            this.FILE_NAME.AppearanceCell.Options.UseFont = true;
            this.FILE_NAME.Caption = "파일명";
            this.FILE_NAME.FieldName = "FILE_NAME";
            this.FILE_NAME.Name = "FILE_NAME";
            this.FILE_NAME.OptionsColumn.ReadOnly = true;
            this.FILE_NAME.Visible = true;
            this.FILE_NAME.Width = 986;
            // 
            // FILE_DOWN
            // 
            this.FILE_DOWN.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FILE_DOWN.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 25F);
            this.FILE_DOWN.AppearanceCell.Options.UseBackColor = true;
            this.FILE_DOWN.AppearanceCell.Options.UseFont = true;
            this.FILE_DOWN.AppearanceCell.Options.UseTextOptions = true;
            this.FILE_DOWN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FILE_DOWN.Caption = "다운";
            this.FILE_DOWN.FieldName = "FILE_DOWN";
            this.FILE_DOWN.Name = "FILE_DOWN";
            this.FILE_DOWN.OptionsColumn.AllowEdit = false;
            this.FILE_DOWN.OptionsColumn.FixedWidth = true;
            this.FILE_DOWN.OptionsColumn.ReadOnly = true;
            this.FILE_DOWN.Visible = true;
            this.FILE_DOWN.Width = 140;
            // 
            // FILE_DEL
            // 
            this.FILE_DEL.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FILE_DEL.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 25F);
            this.FILE_DEL.AppearanceCell.Options.UseBackColor = true;
            this.FILE_DEL.AppearanceCell.Options.UseFont = true;
            this.FILE_DEL.AppearanceCell.Options.UseTextOptions = true;
            this.FILE_DEL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FILE_DEL.Caption = "삭제";
            this.FILE_DEL.FieldName = "FILE_DEL";
            this.FILE_DEL.Name = "FILE_DEL";
            this.FILE_DEL.OptionsColumn.AllowEdit = false;
            this.FILE_DEL.OptionsColumn.FixedWidth = true;
            this.FILE_DEL.OptionsColumn.ReadOnly = true;
            this.FILE_DEL.Visible = true;
            this.FILE_DEL.Width = 140;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridControl2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1134, 619);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_close);
            this.panel2.Controls.Add(this.btn_done);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 542);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1128, 74);
            this.panel2.TabIndex = 7;
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(973, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 70);
            this.btn_close.TabIndex = 2;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(0, 3);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 70);
            this.btn_done.TabIndex = 3;
            this.btn_done.Text = "업로드";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // AttachFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1174, 659);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AttachFileForm";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.Text = "첨부파일";
            this.Load += new System.EventHandler(this.AttachFileForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FILE_NAME;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FILE_DOWN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FILE_DEL;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button btn_done;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}