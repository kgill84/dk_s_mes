﻿namespace DK_Tablet
{
    partial class Instatement
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Instatement));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new DevExpress.XtraEditors.SimpleButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SO_SQUTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SL_SQUTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOSS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REAL_QTY = new System.Windows.Forms.DataGridViewButtonColumn();
            this.TR_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SO_DUEDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TR_SERNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SO_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SO_SERNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOC_CHECK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTN_DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.LOC_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOC_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbl_Cnt2 = new System.Windows.Forms.Label();
            this.lbl_Cnt = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.OP_SITE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_PC_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CARD_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_SIZE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OP_CHECK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.QR_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SDATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SERNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR_CHECK_YN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.lab_1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_Auto_instatement = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClose = new System.Windows.Forms.Button();
            this.btn_move = new System.Windows.Forms.Button();
            this.columnHeader55 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader56 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader57 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader58 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader59 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader60 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader61 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 24F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(104, 46);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(257, 40);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(367, 46);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(75, 40);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(448, 46);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(75, 40);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.lbl_Cnt2);
            this.panel1.Controls.Add(this.lbl_Cnt);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dataGridView3);
            this.panel1.Controls.Add(this.dataGridView2);
            this.panel1.Location = new System.Drawing.Point(12, 92);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 664);
            this.panel1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.button1.Appearance.Options.UseFont = true;
            this.button1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.button1.Location = new System.Drawing.Point(874, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 43);
            this.button1.TabIndex = 60;
            this.button1.Text = "새로고침";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("굴림", 20F);
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView1.ColumnHeadersHeight = 60;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IT_SCODE,
            this.IT_SNAME,
            this.SO_SQUTY,
            this.SL_SQUTY,
            this.QTY,
            this.LOSS,
            this.REAL_QTY,
            this.TR_SNUMB,
            this.SO_DUEDT,
            this.TR_SERNO,
            this.SO_SNUMB,
            this.SO_SERNO,
            this.LOC_CHECK,
            this.LOT_NO,
            this.BTN_DELETE,
            this.LOC_CODE,
            this.LOC_NAME});
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridView1.Location = new System.Drawing.Point(7, 48);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidth = 60;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridView1.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowTemplate.Height = 60;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1249, 607);
            this.dataGridView1.TabIndex = 57;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.DataPropertyName = "IT_SCODE";
            this.IT_SCODE.HeaderText = "품목코드";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.ReadOnly = true;
            this.IT_SCODE.Width = 200;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.DataPropertyName = "IT_SNAME";
            this.IT_SNAME.HeaderText = "품목명";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.ReadOnly = true;
            this.IT_SNAME.Width = 240;
            // 
            // SO_SQUTY
            // 
            this.SO_SQUTY.DataPropertyName = "SO_SQUTY";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.Format = "###0";
            dataGridViewCellStyle19.NullValue = "###0";
            this.SO_SQUTY.DefaultCellStyle = dataGridViewCellStyle19;
            this.SO_SQUTY.HeaderText = "발주량";
            this.SO_SQUTY.Name = "SO_SQUTY";
            this.SO_SQUTY.ReadOnly = true;
            this.SO_SQUTY.Width = 150;
            // 
            // SL_SQUTY
            // 
            this.SL_SQUTY.DataPropertyName = "SL_SQUTY";
            dataGridViewCellStyle20.Format = "###0";
            dataGridViewCellStyle20.NullValue = "###0";
            this.SL_SQUTY.DefaultCellStyle = dataGridViewCellStyle20;
            this.SL_SQUTY.HeaderText = "기입고수량";
            this.SL_SQUTY.Name = "SL_SQUTY";
            this.SL_SQUTY.ReadOnly = true;
            this.SL_SQUTY.Width = 155;
            // 
            // QTY
            // 
            this.QTY.DataPropertyName = "QTY";
            dataGridViewCellStyle21.Format = "###0";
            dataGridViewCellStyle21.NullValue = "###0";
            this.QTY.DefaultCellStyle = dataGridViewCellStyle21;
            this.QTY.HeaderText = "납품수량";
            this.QTY.Name = "QTY";
            this.QTY.ReadOnly = true;
            this.QTY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.QTY.Width = 150;
            // 
            // LOSS
            // 
            this.LOSS.DataPropertyName = "LOSS";
            dataGridViewCellStyle22.Format = "###0";
            dataGridViewCellStyle22.NullValue = "###0";
            this.LOSS.DefaultCellStyle = dataGridViewCellStyle22;
            this.LOSS.HeaderText = "검수차이";
            this.LOSS.Name = "LOSS";
            this.LOSS.ReadOnly = true;
            this.LOSS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LOSS.Width = 150;
            // 
            // REAL_QTY
            // 
            this.REAL_QTY.DataPropertyName = "REAL_QTY";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.Format = "###0";
            dataGridViewCellStyle23.NullValue = "###0";
            this.REAL_QTY.DefaultCellStyle = dataGridViewCellStyle23;
            this.REAL_QTY.HeaderText = "실입고수량";
            this.REAL_QTY.Name = "REAL_QTY";
            this.REAL_QTY.ReadOnly = true;
            this.REAL_QTY.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.REAL_QTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.REAL_QTY.Width = 155;
            // 
            // TR_SNUMB
            // 
            this.TR_SNUMB.DataPropertyName = "TR_SNUMB";
            this.TR_SNUMB.HeaderText = "명세서번호";
            this.TR_SNUMB.Name = "TR_SNUMB";
            this.TR_SNUMB.ReadOnly = true;
            this.TR_SNUMB.Visible = false;
            // 
            // SO_DUEDT
            // 
            this.SO_DUEDT.DataPropertyName = "SO_DUEDT";
            this.SO_DUEDT.HeaderText = "납품일";
            this.SO_DUEDT.Name = "SO_DUEDT";
            this.SO_DUEDT.ReadOnly = true;
            this.SO_DUEDT.Visible = false;
            // 
            // TR_SERNO
            // 
            this.TR_SERNO.DataPropertyName = "TR_SERNO";
            this.TR_SERNO.HeaderText = "시리얼";
            this.TR_SERNO.Name = "TR_SERNO";
            this.TR_SERNO.ReadOnly = true;
            this.TR_SERNO.Visible = false;
            // 
            // SO_SNUMB
            // 
            this.SO_SNUMB.DataPropertyName = "SO_SNUMB";
            this.SO_SNUMB.HeaderText = "발주번호";
            this.SO_SNUMB.Name = "SO_SNUMB";
            this.SO_SNUMB.ReadOnly = true;
            this.SO_SNUMB.Visible = false;
            // 
            // SO_SERNO
            // 
            this.SO_SERNO.DataPropertyName = "SO_SERNO";
            this.SO_SERNO.HeaderText = "발주시리얼";
            this.SO_SERNO.Name = "SO_SERNO";
            this.SO_SERNO.ReadOnly = true;
            this.SO_SERNO.Visible = false;
            // 
            // LOC_CHECK
            // 
            this.LOC_CHECK.DataPropertyName = "LOC_CHECK";
            this.LOC_CHECK.HeaderText = "입고로케이션체크";
            this.LOC_CHECK.Name = "LOC_CHECK";
            this.LOC_CHECK.ReadOnly = true;
            this.LOC_CHECK.Visible = false;
            this.LOC_CHECK.Width = 185;
            // 
            // LOT_NO
            // 
            this.LOT_NO.DataPropertyName = "LOT_NO";
            this.LOT_NO.HeaderText = "LOT NO";
            this.LOT_NO.Name = "LOT_NO";
            this.LOT_NO.ReadOnly = true;
            this.LOT_NO.Visible = false;
            // 
            // BTN_DELETE
            // 
            this.BTN_DELETE.HeaderText = "삭제";
            this.BTN_DELETE.Name = "BTN_DELETE";
            this.BTN_DELETE.ReadOnly = true;
            this.BTN_DELETE.Text = "삭제";
            this.BTN_DELETE.UseColumnTextForButtonValue = true;
            this.BTN_DELETE.Visible = false;
            this.BTN_DELETE.Width = 200;
            // 
            // LOC_CODE
            // 
            this.LOC_CODE.DataPropertyName = "LOC_CODE";
            this.LOC_CODE.HeaderText = "로케이션코드";
            this.LOC_CODE.Name = "LOC_CODE";
            this.LOC_CODE.ReadOnly = true;
            this.LOC_CODE.Visible = false;
            // 
            // LOC_NAME
            // 
            this.LOC_NAME.DataPropertyName = "LOC_NAME";
            this.LOC_NAME.HeaderText = "로케이션명";
            this.LOC_NAME.Name = "LOC_NAME";
            this.LOC_NAME.ReadOnly = true;
            this.LOC_NAME.Visible = false;
            // 
            // lbl_Cnt2
            // 
            this.lbl_Cnt2.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_Cnt2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.lbl_Cnt2.ForeColor = System.Drawing.Color.Red;
            this.lbl_Cnt2.Location = new System.Drawing.Point(1143, 334);
            this.lbl_Cnt2.Name = "lbl_Cnt2";
            this.lbl_Cnt2.Size = new System.Drawing.Size(111, 44);
            this.lbl_Cnt2.TabIndex = 55;
            this.lbl_Cnt2.Text = "0";
            this.lbl_Cnt2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Cnt2.Visible = false;
            // 
            // lbl_Cnt
            // 
            this.lbl_Cnt.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_Cnt.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.lbl_Cnt.ForeColor = System.Drawing.Color.Red;
            this.lbl_Cnt.Location = new System.Drawing.Point(1143, 0);
            this.lbl_Cnt.Name = "lbl_Cnt";
            this.lbl_Cnt.Size = new System.Drawing.Size(111, 44);
            this.lbl_Cnt.TabIndex = 55;
            this.lbl_Cnt.Text = "0";
            this.lbl_Cnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_Cnt.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(1057, 334);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 44);
            this.label6.TabIndex = 55;
            this.label6.Text = "행수 : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Visible = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(1057, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 44);
            this.label4.TabIndex = 55;
            this.label4.Text = "행수 : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(633, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(387, 44);
            this.label5.TabIndex = 55;
            this.label5.Text = "2. 입고 외주반제품";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(633, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(370, 44);
            this.label2.TabIndex = 55;
            this.label2.Text = "2. 입고 자재";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 44);
            this.label3.TabIndex = 56;
            this.label3.Text = "1. 거래명세서 QR 리딩";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToResizeColumns = false;
            this.dataGridView3.AllowUserToResizeRows = false;
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridView3.ColumnHeadersHeight = 60;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OP_SITE_CODE,
            this.OP_PC_SCODE,
            this.OP_CARD_NO,
            this.OP_IT_SCODE,
            this.OP_SIZE,
            this.OP_CHECK});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridView3.Location = new System.Drawing.Point(637, 381);
            this.dataGridView3.MultiSelect = false;
            this.dataGridView3.Name = "dataGridView3";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.RowHeadersWidth = 60;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView3.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridView3.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView3.RowTemplate.Height = 60;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(619, 274);
            this.dataGridView3.TabIndex = 54;
            this.dataGridView3.Visible = false;
            // 
            // OP_SITE_CODE
            // 
            this.OP_SITE_CODE.DataPropertyName = "OP_SITE_CODE";
            this.OP_SITE_CODE.HeaderText = "공장코드";
            this.OP_SITE_CODE.Name = "OP_SITE_CODE";
            this.OP_SITE_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_SITE_CODE.Visible = false;
            this.OP_SITE_CODE.Width = 250;
            // 
            // OP_PC_SCODE
            // 
            this.OP_PC_SCODE.DataPropertyName = "OP_PC_SCODE";
            this.OP_PC_SCODE.HeaderText = "거래처코드";
            this.OP_PC_SCODE.Name = "OP_PC_SCODE";
            this.OP_PC_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_PC_SCODE.Visible = false;
            // 
            // OP_CARD_NO
            // 
            this.OP_CARD_NO.DataPropertyName = "OP_CARD_NO";
            this.OP_CARD_NO.HeaderText = "카드번호";
            this.OP_CARD_NO.Name = "OP_CARD_NO";
            this.OP_CARD_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_CARD_NO.Width = 150;
            // 
            // OP_IT_SCODE
            // 
            this.OP_IT_SCODE.DataPropertyName = "OP_IT_SCODE";
            this.OP_IT_SCODE.HeaderText = "품목코드";
            this.OP_IT_SCODE.Name = "OP_IT_SCODE";
            this.OP_IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_IT_SCODE.Width = 300;
            // 
            // OP_SIZE
            // 
            this.OP_SIZE.DataPropertyName = "OP_SIZE";
            this.OP_SIZE.HeaderText = "수량";
            this.OP_SIZE.Name = "OP_SIZE";
            this.OP_SIZE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_SIZE.Width = 150;
            // 
            // OP_CHECK
            // 
            this.OP_CHECK.DataPropertyName = "OP_CHECK";
            this.OP_CHECK.HeaderText = "체크";
            this.OP_CHECK.Name = "OP_CHECK";
            this.OP_CHECK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.OP_CHECK.Visible = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridView2.ColumnHeadersHeight = 60;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.QR_CODE,
            this.QR_SDATE,
            this.QR_SERNO,
            this.QR_IT_SCODE,
            this.QR_SQTY,
            this.QR_CHECK_YN});
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridView2.Location = new System.Drawing.Point(637, 48);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("굴림", 12F);
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 60;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("굴림", 16F);
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridView2.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.dataGridView2.RowTemplate.Height = 60;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(619, 283);
            this.dataGridView2.TabIndex = 54;
            this.dataGridView2.Visible = false;
            // 
            // QR_CODE
            // 
            this.QR_CODE.DataPropertyName = "QR_CODE";
            this.QR_CODE.HeaderText = "QR_CODE";
            this.QR_CODE.Name = "QR_CODE";
            this.QR_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_CODE.Width = 250;
            // 
            // QR_SDATE
            // 
            this.QR_SDATE.DataPropertyName = "QR_SDATE";
            this.QR_SDATE.HeaderText = "날짜";
            this.QR_SDATE.Name = "QR_SDATE";
            this.QR_SDATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QR_SERNO
            // 
            this.QR_SERNO.DataPropertyName = "QR_SERNO";
            this.QR_SERNO.HeaderText = "SERNO";
            this.QR_SERNO.Name = "QR_SERNO";
            this.QR_SERNO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QR_IT_SCODE
            // 
            this.QR_IT_SCODE.DataPropertyName = "QR_IT_SCODE";
            this.QR_IT_SCODE.HeaderText = "품목코드";
            this.QR_IT_SCODE.Name = "QR_IT_SCODE";
            this.QR_IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_IT_SCODE.Width = 250;
            // 
            // QR_SQTY
            // 
            this.QR_SQTY.DataPropertyName = "QR_SQTY";
            this.QR_SQTY.HeaderText = "수량";
            this.QR_SQTY.Name = "QR_SQTY";
            this.QR_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_SQTY.Width = 69;
            // 
            // QR_CHECK_YN
            // 
            this.QR_CHECK_YN.DataPropertyName = "QR_CHECK_YN";
            this.QR_CHECK_YN.HeaderText = "체크";
            this.QR_CHECK_YN.Name = "QR_CHECK_YN";
            this.QR_CHECK_YN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.QR_CHECK_YN.Visible = false;
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(625, 43);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(529, 46);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 11;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "리더기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(1159, 46);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(1108, 43);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader55,
            this.columnHeader56,
            this.columnHeader57,
            this.columnHeader58,
            this.columnHeader59,
            this.columnHeader60,
            this.columnHeader61});
            this.dsmListView_ivt.Location = new System.Drawing.Point(265, 3);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(42, 60);
            this.dsmListView_ivt.TabIndex = 12;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(461, 10);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 21;
            this.N_timer.Text = "labelControl1";
            this.N_timer.Visible = false;
            // 
            // lab_1
            // 
            this.lab_1.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.lab_1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.lab_1.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.lab_1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.lab_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lab_1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lab_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab_1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lab_1.Location = new System.Drawing.Point(625, 23);
            this.lab_1.Name = "lab_1";
            this.lab_1.Size = new System.Drawing.Size(136, 63);
            this.lab_1.TabIndex = 36;
            this.lab_1.Text = "자동입고";
            // 
            // btn_Auto_instatement
            // 
            this.btn_Auto_instatement.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_Auto_instatement.Appearance.Options.UseFont = true;
            this.btn_Auto_instatement.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Auto_instatement.Location = new System.Drawing.Point(763, 23);
            this.btn_Auto_instatement.Name = "btn_Auto_instatement";
            this.btn_Auto_instatement.Size = new System.Drawing.Size(91, 63);
            this.btn_Auto_instatement.TabIndex = 35;
            this.btn_Auto_instatement.Text = "On";
            this.btn_Auto_instatement.Click += new System.EventHandler(this.btn_Auto_instatement_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(20, 10);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(201, 20);
            this.textEdit1.TabIndex = 37;
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(227, 9);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 38;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(308, 9);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 39;
            this.simpleButton2.Text = "버튼";
            this.simpleButton2.Visible = false;
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1083, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 85);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btn_move
            // 
            this.btn_move.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_move.BackgroundImage")));
            this.btn_move.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_move.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_move.Location = new System.Drawing.Point(877, 3);
            this.btn_move.Name = "btn_move";
            this.btn_move.Size = new System.Drawing.Size(198, 85);
            this.btn_move.TabIndex = 15;
            this.btn_move.Text = "입고";
            this.btn_move.UseVisualStyleBackColor = true;
            this.btn_move.Click += new System.EventHandler(this.btn_move_Click);
            // 
            // Instatement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.btn_Auto_instatement);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_move);
            this.Controls.Add(this.lab_1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Instatement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Metarial_Move";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Injection_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Button button_inventory_start;
        private System.Windows.Forms.Label label1;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label lbl_Cnt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_move;
        private System.Windows.Forms.Label lbl_Cnt2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_SITE_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_PC_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CARD_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_SIZE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OP_CHECK;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SDATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SERNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR_CHECK_YN;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DevExpress.XtraEditors.LabelControl lab_1;
        private DevExpress.XtraEditors.SimpleButton btn_Auto_instatement;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private DevExpress.XtraEditors.SimpleButton button1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn SO_SQUTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn SL_SQUTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOSS;
        private System.Windows.Forms.DataGridViewButtonColumn REAL_QTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn SO_DUEDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn TR_SERNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SO_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn SO_SERNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOC_CHECK;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOT_NO;
        private System.Windows.Forms.DataGridViewButtonColumn BTN_DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOC_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOC_NAME;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.ColumnHeader columnHeader61;
    }
}

