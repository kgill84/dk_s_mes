﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DK_Tablet.Popup;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;


namespace DK_Tablet
{
    public partial class Fail_input_qr : Form
    {
        DataTable select_worker_dt = new DataTable();
        GET_DATA GET_DATA = new GET_DATA();
        public string wc_group { get; set; }
        string str_worker { get; set; }
        string pc_scode { get; set; }
        string str_tr_snumb { get; set; }
        string str_so_duedt { get; set; }
        string site_code { get; set; }
        string plant { get; set; }
        Dictionary<int, DataTable> dic = new Dictionary<int, DataTable>();

        //
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
        private SwingLibrary.SwingAPI Swing = null;
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };
        Dictionary<string, string> combPortDic = new Dictionary<string, string>();
        ContextMenuStrip remove_menu;

        public Fail_input_qr()
        {
            site_code = Properties.Settings.Default.SITE_CODE;
            InitializeComponent();
            SwingSet1();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            fillLookUpEdit_FAIL_TYPE(); // 룩업에디터 불량유형 데이터소스 채
           
            plant = GET_DATA.getPlant(Properties.Settings.Default.SITE_CODE.ToString());
            SwingSet2();
            GetlueProPerson();
            str_worker = btn_worker_info.EditValue.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Swing.InventoryStop();
            this.Close();
        }

        public void formClear()
        {
            str_tr_snumb = "";
            str_so_duedt = "";
            gridControl1.DataSource = new DataTable();
            textEdit1.Text = "";
            lbl_PC_SCODE.Text = "";

            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Swing.InventoryStop();
            Swing.TagListClear();
            Swing.InventoryStart();
        }

        // 룩업에디터 불량유형 데이터소스 받아와서 채움
        private void fillLookUpEdit_FAIL_TYPE()
        {
            string strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "SELECT '' AS NUMB, '' AS FT_NAME UNION "
                       + "SELECT DISTINCT RTRIM(NUMB) AS NUMB, RTRIM(FT_NAME) AS FT_NAME FROM FAIL_TYPE";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);

            try
            {
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                LookUpEdit_FAIL_TYPE.DataSource = dataTable;
                LookUpEdit_FAIL_TYPE.ValueMember = "NUMB"; // 불량코드
                LookUpEdit_FAIL_TYPE.DisplayMember = "FT_NAME"; // 불량유형
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private DataTable get_FAIL_TYPE()
        {
            string strCon = Properties.Settings.Default.SQL_DKQT;
            string sql =  "SELECT DISTINCT RTRIM(NUMB) AS NUMB, RTRIM(FT_NAME) AS FT_NAME, 0 AS QTY FROM FAIL_TYPE";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);

            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            set_worker_info();
        }
        // 작업자 선택 기존코드
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            //Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_group = wc_group;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;

                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }

                str_worker = "";
                DataRow[] drr = select_worker_dt.Select();
                for (int i = 0; i < drr.Length; i++)
                {
                    if (i == drr.Length - 1)
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString();
                    }
                    else
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString() + ",";
                    }
                }

            }
        }

        public void saveDataRow_QR(string str)
        {
            string[] strTags = str.Split('/');
            if (strTags.Length == 2)
            {
                str_tr_snumb = strTags[0];
                str_so_duedt = strTags[1];

            }
            else
            {
                str_tr_snumb = "";
                str_so_duedt = "";
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            str_tr_snumb = "";
            str_so_duedt = "";

            saveDataRow_QR(textEdit1.Text);

            if (str_tr_snumb.Length == 11)
            {
                // 수입검사 완료 체크 true면 검사함.
                fillGridView(str_tr_snumb);

                if (gridView1.RowCount > 0)
                {
                    getPC_SCODE(str_tr_snumb);
                    getInspection_YN(str_tr_snumb);
                }
                else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                {
                    MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                }
                else
                {
                    MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                }
            }
            else
            {
                MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
            }
        }
        // 테스트용으로 만든 검색버튼 (거래명세서 코드를 직접 입력해야함)
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (textEdit1.Text.Length == 11)
            {
                str_tr_snumb = textEdit1.Text;
                str_so_duedt = DateTime.Now.ToString("yyyyMMdd"); // 테스트용 임시 오늘 날짜
                str_worker = "tic"; // 테스트용 임시 작업자

                fillGridView(textEdit1.Text);

                if (gridView1.RowCount > 0)
                {
                    getPC_SCODE(str_tr_snumb);
                    getInspection_YN(str_tr_snumb);
                }
                else if (GET_DATA.명세서_완료_체크(textEdit1.Text) > 0)
                {
                    MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                }
                else
                {
                    MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                }
            }
            else
            {
                MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
            }
        }
        // 입고시검사여부(기준정보) 숨겨진 열에 Parse
        // 검사여부가 'N'일 경우 불량검사를 하지 않음 (불량 수량 입력 불가)
        private void getInspection_YN(String TR_SNUMB)
        {
            string strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand("SELECT B.INSPECTION_YN FROM PO_CNFM A LEFT JOIN IT_MASTER B ON A.IT_SCODE = B.IT_SCODE WHERE TR_SNUMB = @TR_SNUMB", conn);
            cmd.Parameters.AddWithValue("TR_SNUMB", TR_SNUMB);
            try
            {
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    int idx = 0;
                    while (reader.Read())
                    {
                        string ins_YN = reader["INSPECTION_YN"] as string;
                        gridView1.SetRowCellValue(idx, INSPECTION_YN, ins_YN);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        // 데이터소스 채움
        private void fillGridView(String TR_SNUMB)
        {
            dic.Clear();
            DataTable emptyTable = new DataTable();
            emptyTable.Columns.Add(IT_SCODE.FieldName, IT_SCODE.ColumnType);
            emptyTable.Columns.Add(IT_SNAME.FieldName, IT_SNAME.ColumnType);
            emptyTable.Columns.Add(SO_SQUTY.FieldName, SO_SQUTY.ColumnType);
            emptyTable.Columns.Add(FAIL_QTY.FieldName, FAIL_QTY.ColumnType);
            emptyTable.Columns.Add(FAIL_TYPE.FieldName, FAIL_TYPE.ColumnType);
            emptyTable.Columns.Add(INSP_OK_QTY.FieldName, INSP_OK_QTY.ColumnType);
            emptyTable.Columns.Add(FAIL_DESCR.FieldName, FAIL_DESCR.ColumnType);
            emptyTable.Columns.Add(TR_SERNO.FieldName, TR_SERNO.ColumnType);
            emptyTable.Columns.Add(INSPECTION_YN.FieldName, INSPECTION_YN.ColumnType);

            DataTable dataTable = GET_DATA.get_po_cnfm(TR_SNUMB);

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                emptyTable.Rows.Add(emptyTable.NewRow());
            }
            gridControl1.DataSource = emptyTable;

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                gridView1.SetRowCellValue(i, IT_SCODE, dataTable.Rows[i]["IT_SCODE"]);
                gridView1.SetRowCellValue(i, IT_SNAME, dataTable.Rows[i]["IT_SNAME"]);
                gridView1.SetRowCellValue(i, SO_SQUTY, dataTable.Rows[i]["SO_SQUTY"]);
                gridView1.SetRowCellValue(i, FAIL_QTY, 0);
                gridView1.SetRowCellValue(i, INSP_OK_QTY, dataTable.Rows[i]["INSP_OK_QTY"]);
                gridView1.SetRowCellValue(i, TR_SERNO, dataTable.Rows[i]["TR_SERNO"]);
                gridView1.SetRowCellValue(i, INSPECTION_YN, dataTable.Rows[i]["INSPECTION_YN"]);
                dic.Add(int.Parse(dataTable.Rows[i]["TR_SERNO"].ToString()), get_FAIL_TYPE());
            }

        }
        // 거래처
        private void getPC_SCODE(string TR_SNUMB)
        {
            string strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT A.PC_SCODE, RTRIM(B.PC_SNAME) AS PC_SNAME FROM PO_CNFM A LEFT JOIN CS_MASTER B ON A.PC_SCODE = B.PC_SCODE WHERE TR_SNUMB = @TR_SNUMB", conn);
            cmd.Parameters.AddWithValue("TR_SNUMB", TR_SNUMB);
            try
            {
                conn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            pc_scode = reader["PC_SCODE"] as string;
                            lbl_PC_SCODE.Text = reader["PC_SNAME"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
        // 등록
        private void btn_move_Click(object sender, EventArgs e)
        {
            // 불량 수량이 입력되어 있지만 유형이 없으면 경고
            bool beforeSaveCondition = true;
            /*
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (Int16.Parse(gridView1.GetRowCellValue(i, FAIL_QTY).ToString()) > 0
                    && string.IsNullOrWhiteSpace(gridView1.GetRowCellValue(i, FAIL_TYPE).ToString()))
                {
                    beforeSaveCondition = false;
                    break;
                }
                else
                {
                    beforeSaveCondition = true;
                }
            }
            */

            if (string.IsNullOrWhiteSpace(btn_worker_info.Text))
            {
                MessageBox.Show("검사자를 입력하세요");
                return;
            }

            if (beforeSaveCondition)
            {
                insert(str_tr_snumb, str_so_duedt);
            }
            else
            {
                MessageBox.Show("불량유형을 입력하세요");
            }
        }

        private void insert(string tr_snumb, string so_duedt)
        {
            this.Cursor = Cursors.WaitCursor;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            List<String> checkList = new List<String>();
            try
            {
                conn.Open();
                trans = conn.BeginTransaction();

                // 거래명세서 업데이트 (거래 명세서 수량 수정하지 않음, 불량 임시 열에 저장함)
                SqlCommand cmd_first = new SqlCommand(" UPDATE PO_CNFM SET "
                 + " INSP_OK_QTY = @INSP_OK_QTY "
                 + " WHERE TR_SNUMB=@TR_SNUMB AND TR_SERNO=@TR_SERNO ", conn);
                cmd_first.Transaction = trans;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    cmd_first.Parameters.Clear();
                    cmd_first.Parameters.AddWithValue("@TR_SNUMB", str_tr_snumb);
                    cmd_first.Parameters.AddWithValue("@INSP_OK_QTY", gridView1.GetRowCellValue(i, INSP_OK_QTY));
                    cmd_first.Parameters.AddWithValue("@TR_SERNO", gridView1.GetRowCellValue(i, TR_SERNO));
                    cmd_first.ExecuteNonQuery();
                }

                // 채번
                SqlCommand regno_cmd = new SqlCommand("SP_FAIL_REG_NUM", conn);
                regno_cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paramMaxNum = new SqlParameter("@FAIL_REGNO", SqlDbType.Char, 8);
                paramMaxNum.Direction = ParameterDirection.Output;
                regno_cmd.Parameters.Add(paramMaxNum);

                regno_cmd.Transaction = trans;
                regno_cmd.ExecuteNonQuery();
                string fail_regno = paramMaxNum.Value.ToString();

                // 불량 등록
                string fail_input_sql = "INSERT INTO FAIL_INPUT_NEW"
                + "(SITE_CODE, RSRV_NO, WI_SNUMB, IN_SERNO,LOST_SCODE,FAIL_QTY,FAIL_DESCR,IT_SCODE,FAIL_REGNO,FAIL_RDATE,FAIL_SDATE,FAIL_TYPE,FAIL_PERSON,OUT_YN,PROCESSING_YN,PC_SCODE)"
                + " VALUES (@SITE_CODE, '', '', @IN_SERNO,@LOST_SCODE,@FAIL_QTY,@FAIL_DESCR,@IT_SCODE,@FAIL_REGNO,@FAIL_RDATE,@FAIL_SDATE,@FAIL_TYPE,@FAIL_PERSON,@OUT_YN,@PROCESSING_YN,@PC_SCODE)";
                SqlCommand fail_input_cmd = new SqlCommand(fail_input_sql, conn);
                fail_input_cmd.Transaction = trans;
                int in_serno = 1;
                for (int i = 0; i < gridView1.RowCount; i++)
                {
                    if (Int16.Parse(gridView1.GetRowCellValue(i, FAIL_QTY).ToString()) > 0)
                    {
                        DataTable dt = dic[int.Parse(gridView1.GetRowCellValue(i, TR_SERNO).ToString())];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            if (!dt.Rows[j]["QTY"].ToString().Equals("0"))
                            {
                                fail_input_cmd.Parameters.Clear();
                                fail_input_cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);
                                fail_input_cmd.Parameters.AddWithValue("@IN_SERNO", in_serno);
                                fail_input_cmd.Parameters.AddWithValue("@LOST_SCODE", dt.Rows[j]["NUMB"]);
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_QTY", int.Parse(dt.Rows[j]["QTY"].ToString()));
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_DESCR", gridView1.GetRowCellValue(i, FAIL_DESCR));
                                fail_input_cmd.Parameters.AddWithValue("@IT_SCODE", gridView1.GetRowCellValue(i, IT_SCODE));
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_REGNO", fail_regno);
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_RDATE", DateTime.Now.ToString("yyyyMMdd"));
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_SDATE", DateTime.Now.ToString("yyyyMMdd"));
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_TYPE", "수입검사");
                                fail_input_cmd.Parameters.AddWithValue("@FAIL_PERSON", string.IsNullOrWhiteSpace(str_worker) ? "" : str_worker);
                                fail_input_cmd.Parameters.AddWithValue("@OUT_YN", "Y");
                                fail_input_cmd.Parameters.AddWithValue("@PROCESSING_YN", "Y");
                                fail_input_cmd.Parameters.AddWithValue("@PC_SCODE", pc_scode);
                                fail_input_cmd.ExecuteNonQuery();
                                in_serno++;
                            }
                        }
                    }
                }
                /*
                // 불량창고 입고전표 / 창고 출고전표 생성
                string fail_tran_sql = "USP_INSP_MM_TRAN_REG";
                SqlCommand fail_tran_cmd = new SqlCommand(fail_tran_sql, conn);
                fail_tran_cmd.CommandType = CommandType.StoredProcedure;
                fail_tran_cmd.Transaction = trans;
                fail_tran_cmd.Parameters.AddWithValue("@FAIL_REGNO", fail_regno);
                fail_tran_cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);
                fail_tran_cmd.Parameters.AddWithValue("@LOC_SCODE", "");
                fail_tran_cmd.ExecuteNonQuery();

                string pro_processing_query = "USP_TABLET_IMPORT_INSP_PROCESSING";
                SqlCommand pro_processing_cmd = new SqlCommand(pro_processing_query, conn);
                pro_processing_cmd.CommandType = CommandType.StoredProcedure;
                pro_processing_cmd.Transaction = trans;
                pro_processing_cmd.Parameters.AddWithValue("@FAIL_REGNO", fail_regno);
                pro_processing_cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);
                pro_processing_cmd.ExecuteNonQuery();
                */
                trans.Commit();
                formClear();
                MyMessageBox_2.ShowBox("수입검사가 완료 되었습니다.", "수입검사완료", "거래명세서 번호 : " + str_tr_snumb, 2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            finally
            {
                conn.Close();
                this.Cursor = Cursors.Default;
            }
        }
        // 불량 수량 입력
        private void TextEdit_FAIL_QTY_MouseDown(object sender, MouseEventArgs e)
        {
            // 마우스 위치로 현재 행을 얻어 옴
            int rowHandle = -1;
            var HitPoint = gridView1.CalcHitInfo(gridControl1.PointToClient(Control.MousePosition));
            if (HitPoint.InRowCell)
            {
                rowHandle = HitPoint.RowHandle;
            }
            if (rowHandle != -1)
            {
                // 검사를 하지 않는 품목일 시 경고
                string yn = string.IsNullOrWhiteSpace(gridView1.GetRowCellValue(rowHandle, INSPECTION_YN).ToString()) ?
                    "" : gridView1.GetRowCellValue(rowHandle, INSPECTION_YN).ToString();
                if (yn.Equals("N"))
                {
                    MessageBox.Show("검사를 하지 않는 품목입니다.");
                    return;
                }
                TextEdit textEdit = (TextEdit)sender;
                KeyPad KeyPad = new KeyPad();
                KeyPad.txtDigit.Text = textEdit.Text;
                if (KeyPad.ShowDialog() == DialogResult.OK)
                {
                    textEdit.Text = string.IsNullOrWhiteSpace(KeyPad.txt_value) ? "0" : ((int)Double.Parse(KeyPad.txt_value)).ToString();
                    gridView1.SetRowCellValue(rowHandle, FAIL_QTY, textEdit.Text);
                    // 수량 입력시 실입하수량 변경
                    setQTY(rowHandle);
                }
                // 발주수량 초과 입력 시 경고
                if (int.Parse(gridView1.GetRowCellValue(rowHandle, INSP_OK_QTY).ToString()) < 0)
                {
                    MessageBox.Show("기존수량을 초과 할 수 없습니다.");
                    gridView1.SetRowCellValue(rowHandle, FAIL_QTY, 0);
                    setQTY(rowHandle);
                    return;
                }
            }
        }
        // 불량유형 행 추가
        private void btnRowAdd_Click(object sender, EventArgs e)
        {
            if (gridView1.SelectedRowsCount != 1)
            {
                return;
            }
            int idx = gridView1.FocusedRowHandle;
            string yn = string.IsNullOrWhiteSpace(gridView1.GetRowCellValue(idx, INSPECTION_YN).ToString()) ? "" : gridView1.GetRowCellValue(idx, INSPECTION_YN).ToString();
            // 검사를 하지 않는 품목일시 경고
            if (yn.Equals("N"))
            {
                MessageBox.Show("검사를 하지 않는 품목입니다.");
                return;
            }
            if (gridView1.RowCount > 0 && idx >= 0)
            {
                // 상위 행의 품목, 불량수량, 검사여부를 그대로 복사
                DataTable dt = gridControl1.DataSource as DataTable;
                DataRow dr = dt.NewRow();
                dr["IT_SCODE"] = gridView1.GetRowCellValue(idx, IT_SCODE);
                dr["IT_SNAME"] = gridView1.GetRowCellValue(idx, IT_SNAME);
                dr["SO_SQUTY"] = gridView1.GetRowCellValue(idx, SO_SQUTY);
                dr["TR_SERNO"] = gridView1.GetRowCellValue(idx, TR_SERNO);
                dr["INSP_OK_QTY"] = gridView1.GetRowCellValue(idx, INSP_OK_QTY);
                dr["INSPECTION_YN"] = yn;
                // 복제된 행을 데이터소스에 추가
                dt.Rows.InsertAt(dr, idx + 1);
                // 불량수량 0
                gridView1.SetRowCellValue(idx + 1, FAIL_QTY, 0);
                gridView1.RefreshRowCell(idx + 1, FAIL_QTY);
                gridView1.RefreshRowCell(idx + 1, FAIL_TYPE);
            }
        }
        // 셀 머징 (복제된 행만 머징)
        private void gridView1_CellMerge(object sender, CellMergeEventArgs e)
        {
            DataRow row1 = gridView1.GetDataRow(e.RowHandle1); // 비교 상위 행
            DataRow row2 = gridView1.GetDataRow(e.RowHandle2); // 비교 하위 행

            string TR_SERNO1 = row1["TR_SERNO"].ToString();
            string TR_SERNO2 = row2["TR_SERNO"].ToString();

            if (TR_SERNO1 == TR_SERNO2)
            {
                e.Merge = true;
            }
            else
            {
                e.Merge = false;
            }
            e.Handled = true;
        }
        // 입고수량 수정
        private void setQTY(int rowHandle)
        {
            if (rowHandle == -1)
            {
                return;
            }
            // 선택된 품목의 모든 행을 찾음
            int fromIdx = -1;
            int toIdx = -1;
            String tr_serno = gridView1.GetRowCellValue(rowHandle, TR_SERNO).ToString();
            // 발주수량
            int qty = int.Parse(gridView1.GetRowCellValue(rowHandle, SO_SQUTY).ToString());
            // 총불량수량
            int totalFail_Qty = 0;

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (tr_serno == gridView1.GetRowCellValue(i, TR_SERNO).ToString())
                {
                    if (fromIdx == -1)
                    {
                        fromIdx = i;
                        toIdx = i + 1;
                    }
                    else
                    {
                        toIdx = i + 1;
                    }
                }
            }
            // 불량수량의 총합을 구함
            for (int i = fromIdx; i < toIdx; i++)
            {
                totalFail_Qty += int.Parse(gridView1.GetRowCellValue(i, FAIL_QTY).ToString());
            }
            // 입고수량 수정
            while (fromIdx < toIdx)
            {
                gridView1.SetRowCellValue(fromIdx, INSP_OK_QTY, (qty - totalFail_Qty));
                fromIdx++;
            }
        }

        private void setQTY(string tr_serno)
        {
            int rowHandle = -1;
            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (tr_serno == gridView1.GetRowCellValue(i, TR_SERNO).ToString())
                {
                    rowHandle = i;
                    break;
                }
            }
            setQTY(rowHandle);
        }

        // 테스트용 행 삭제 버튼
        private void btnRowDel_Click(object sender, EventArgs e)
        {
            int rowHandle = gridView1.FocusedRowHandle;
            int rowcount = 0;
            String tr_serno = gridView1.GetRowCellValue(rowHandle, TR_SERNO).ToString();

            for (int i = 0; i < gridView1.RowCount; i++)
            {
                if (tr_serno == gridView1.GetRowCellValue(i, TR_SERNO).ToString())
                {
                    rowcount++;
                }
            }
            if (rowcount <= 1)
            {
                MessageBox.Show("이 행은 삭제할 수 없습니다.");
                return;
            }

            DataTable dt = gridControl1.DataSource as DataTable;
            dt.Rows.RemoveAt(rowHandle);
            setQTY(tr_serno);
        }
        // 검사 물품이 아닐 경우 붉은색 배경
        private void gridView1_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle == View.FocusedRowHandle)
            {
                e.Appearance.BackColor = Color.Aquamarine;
            }
            if (e.Column.FieldName == "FAIL_QTY" || e.Column.FieldName == "FAIL_TYPE")
            {
                string yn = string.IsNullOrWhiteSpace(View.GetRowCellValue(e.RowHandle, View.Columns["INSPECTION_YN"]).ToString()) ? "" : View.GetRowCellValue(e.RowHandle, View.Columns["INSPECTION_YN"]).ToString();
                if (yn.Equals("N"))
                {
                    e.Appearance.BackColor = Color.IndianRed;
                }
            }
        }

        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        void CloseThreadFunction(object normal)
        {
            bool normal_off = (bool)normal;

            Thread.Sleep(1000);
            this.Invoke(new EventHandler(delegate
            {
                button_com_close_Click(null, null);
            }));

            if (normal_off)
                MessageBox.Show("Swing-U is turned off by user", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Swing-U is turned down abnormaly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
            }));

            Swing.ReportTagList();
        }

        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }

        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //OP카드 row add
                    /*
                    DataRow dr = saveDataRow_OP(itemString[3]);

                    if (dr != null)
                    {
                        for (int i = 0; i < advBandedGridView1.RowCount; i++)
                        {
                            if (Convert.ToString(advBandedGridView1.GetRowCellValue(i, "IT_SCODE")).Trim().Equals(dr["OP_IT_SCODE"].ToString().Trim()))
                            {
                                Tag_DT_OP.Rows.Add(dr);
                                lbl_Cnt2.Text = (int.Parse(lbl_Cnt2.Text) + 1).ToString();
                            }
                            //dataInCell = Convert.ToString(gv.GetRowCellValue(i, "IT_SCODE"));
                        }
                        dataGridView3.DataSource = Tag_DT_OP;
                        
                    }   */
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }

                /*if (dsmListView_ivt.Items.Count == 2)
                {
                    Swing.InventoryStop();
                }*/
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    //리딩
                    str_tr_snumb = "";
                    str_so_duedt = "";
                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    saveDataRow_QR(itemString[3]);

                    if (str_tr_snumb.Length == 11)
                    {
                        fillGridView(str_tr_snumb);

                        if (gridView1.RowCount > 0)
                        {
                            getPC_SCODE(str_tr_snumb);
                            getInspection_YN(str_tr_snumb);
                        }
                        else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                        {
                            MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                        }
                        else
                        {
                            MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                        }
                    }
                    else
                    {
                        MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
                    }
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {

                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";

                    Swing.SetRFPower(25);
                    Swing.ReportAllInformation();

                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;

        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            formClear();

        }
        #endregion
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();
            Swing.ConnectionClose();
        }

        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }
        private void SwingSet1()
        {
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;


                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            combPortDic = Utils.GetComList();
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));

        }

        private void SwingSet2()
        {
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);
            listView_target_list.ContextMenuStrip = remove_menu;
        }
        public void GetlueProPerson()
        {

            string strQury;
            strQury = "SELECT S01,RTRIM(S02) AS 'S02' FROM TOP_COMM.DBO.TOP_USR WHERE S04 LIKE '%품질관리%' AND S00 = @S00";

            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT.ToString());

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            da.SelectCommand.Parameters.AddWithValue("@S00", plant);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "TOP_COMM");
                DataTable dt = ds.Tables["TOP_COMM"];

                btn_worker_info.Properties.DataSource = dt;
                btn_worker_info.Properties.DisplayMember = "S02";
                btn_worker_info.Properties.ValueMember = "S01";
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Column == FAIL_TYPE)
            {
                if (gridView1.GetRowCellValue(e.RowHandle, INSPECTION_YN).ToString().Equals("N"))
                {
                    MessageBox.Show("검사품목이 아닙니다.");
                    return;
                }
                int tr_snumb = int.Parse(gridView1.GetRowCellValue(e.RowHandle, TR_SERNO).ToString());
                DataTable dt = new DataTable();
                dt = dic[tr_snumb];

                Fail_Input_Qr_PopUp Fail_Input_Qr_PopUp = new Fail_Input_Qr_PopUp(tr_snumb, dt);
                Fail_Input_Qr_PopUp.total = int.Parse(gridView1.GetRowCellValue(e.RowHandle, SO_SQUTY).ToString());
              
                if (Fail_Input_Qr_PopUp.ShowDialog() == DialogResult.OK)
                {
                    dic.Remove(tr_snumb);
                    dic.Add(tr_snumb, Fail_Input_Qr_PopUp.rdt);
                    gridView1.SetFocusedRowCellValue(FAIL_QTY, Fail_Input_Qr_PopUp.totalQty);
                    setQTY(e.RowHandle);
                }

            }
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                str_tr_snumb = "";
                str_so_duedt = "";

                saveDataRow_QR(textEdit1.Text);

                if (str_tr_snumb.Length == 11)
                {
                    // 수입검사 완료 체크 true면 검사함.
                    fillGridView(str_tr_snumb);

                    if (gridView1.RowCount > 0)
                    {
                        getPC_SCODE(str_tr_snumb);
                        getInspection_YN(str_tr_snumb);
                    }
                    else if (GET_DATA.명세서_완료_체크(str_tr_snumb) > 0)
                    {
                        MessageBox.Show("입고가 완료된 거래명세서 입니다. ");
                    }
                    else
                    {
                        MessageBox.Show("SCM에서 거래명세서를 확인해주세요");
                    }
                }
                else
                {
                    MessageBox.Show("거래명세서 QR 코드가 아닙니다.");
                }
            }
        }
    }
}
