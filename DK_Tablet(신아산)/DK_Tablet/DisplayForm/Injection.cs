﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using Microsoft.Win32;
using DK_Tablet.Popup;
using System.Drawing.Printing;



namespace DK_Tablet
{
    public partial class Injection : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);//리더기와 pc간 통신하기 위한 함수
        private SwingLibrary.SwingAPI Swing = null;  //스윙-u 사용하기 위한 API 객체 생성
        private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성        
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성        
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        string str_worker = ""; //작업자
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code { get; set; }//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        public string alc_code = "";//alc코드
        DataTable Tag_DT_PP = new DataTable();//pp카드 리딩시 보관 되는곳
        DataTable Tag_DT_DC = new DataTable();//대차 리딩시 보관 되는곳
        DataTable select_worker_dt = new DataTable();
        string PP_WC_CODE_str = ""; //작업장
        string PP_IT_SCODE_str = "";//품목코드
        string CARD_NO_str = "";//pp카드 번호
        string PP_SIZE_str = "";//pp카드 수용수
        string PP_SITE_CODE_str = "";//pp카드 공장
        string carrier_no = "";//대차번호
        string me_scode = "";//자재유형코드
        string it_pkqty ="";//수용수(정상작업 32개)
        string it_pkqty_temp = "40";//수용수(대차부족으로 인한 작업 40개)
        string max_sqty = "0";//최대보관수량
        string a_weight = "";//정미중량
        string d_weight = "";//편차범위
        string sw_code = "";//근무형태 코드
        PpCard_Success PpCard_Success = new PpCard_Success(); //생산완료시 pp card message
        
        Weight_print Weight_print = new Weight_print();
        public int po_start_time { get; set; }//작업지시 선택시 리딩 시간
        public int pp_start_time { get; set; }//pp카드 팝업시 리딩 시간
        string night_time_start = ""; //야간 시작 시간
        string day_time_start = ""; //주간 시작 시간
        Dictionary<string, string> combPortDic = new Dictionary<string, string>();//저울 연결에 필요한 포트 정보
        
        private BackgroundWorker worker = new BackgroundWorker();
        FtpUtil ftpUtil;
        string path = "ftp://203.251.168.131:4104";
        string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        Image[] work_image = new Image[10];
        MAIN parentForm;
        string[] sqty_arry = new string[2];
        public Injection(MAIN form)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.parentForm = form;
            parentForm.Visible = false;
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            WinConsole.Visible = false;
            WinConsole.Title = "Raw Data Logger";
            WinConsole.Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;
                btn_port_open.Enabled = true;
                btn_port_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            combPortDic = Utils.GetComList();
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            /*
            
            
            
            Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);
            /*
            groupBox_bt.Enabled = checkBox_dongle.Checked;

            label_battery_volt.Text = "Volts: 0.000 [V]";

            checkBox_rawdata.Checked = Properties.Settings.Default.ConsoleEnable;
            Swing.LogWrite = Properties.Settings.Default.ConsoleEnable;
            WinConsole.Visible = checkBox_rawdata.Checked;

            for (int i = 30; i > 2; i--) comboBox_rfpwr.Items.Add(i);
            comboBox_rfpwr.SelectedIndex = 0;

            for (int j = 0; j < 6; j++) comboBox_channel.Items.Add(j);
            comboBox_channel.SelectedIndex = 0;
            comboBox_channel.Enabled = checkBox_fixed_channel.Checked;

            for (int i = 0; i < 16; i++) comboBox_q.Items.Add(i);
            comboBox_q.SelectedIndex = 4;

            comboBox_algorithm.Items.Add("FIXEDQ");
            comboBox_algorithm.Items.Add("DYNAMICQ");
            comboBox_algorithm.SelectedIndex = 1;

            checkBox_toggle_target.Checked = true;

            comboBox_bank.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.MemoryBank));
            comboBox_bank.SelectedIndex = 1;
            textBox_BlockOffset.Text = "2";
            textBox_BlockCount.Text = "6";
            //textBox_accesspwd.Text = "00000000";

            comboBox_mem_killpwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_accesspwd.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_epc.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_tid.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });
            comboBox_mem_user.Items.AddRange(new string[] { "ACCESSIBLE", "ALWAYS_ACCESSIBLE", "SECURED_ACCESSIBLE", "ALWAYS_NOT_ACCESSIBLE", "NO_CHANGE" });

            comboBox_mem_killpwd.SelectedIndex = 4;
            comboBox_mem_accesspwd.SelectedIndex = 4;
            comboBox_mem_epc.SelectedIndex = 4;
            comboBox_mem_tid.SelectedIndex = 4;
            comboBox_mem_user.SelectedIndex = 4;

            radioButton_vol_max.Checked = true;
            radioButton_bz_trigger.Checked = true;
            */
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
             
        }
        weight_Success weight_Success = new weight_Success();
        private void Form1_Load(object sender, EventArgs e)
        {
            
            GET_DATA.get_work_time_master();
            night_time_start = GET_DATA.night_time_start;
            day_time_start = GET_DATA.day_time_start;
            
            weight_Success.Show();
            weight_Success.Visible = false;
            //timer_now.Start();
            GET_DATA.get_timer_master(wc_group);
            //timer_po_start.Interval = int.Parse(GET_DATA.po_timer) * 1000;
            //timer_pp_start.Interval = int.Parse(GET_DATA.pp_timer) * 1000;
            PpCard_Success.Show();
            PpCard_Success.Visible = false;
            //저울
            if (combPort_Auto())
            {            
                combPort.SelectedIndex = 0;
                try
                {                    
                    serialPort1.PortName = combPort.Text;                    
                }
                catch
                {
                    MessageBox.Show("저울 포트오류");
                }
            }

            lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
            lueWc_code.Properties.DisplayMember = "WC_NAME";
            lueWc_code.Properties.ValueMember = "WC_CODE";
            //lueWc_code.ItemIndex = 0;
            
            if (regKey.GetValue("WC_CODE") == null)
            {
                regKey.SetValue("WC_CODE", "");
            }
            else
            {
                if (regKey.GetValue("WC_CODE").ToString() != "")
                {
                    
                    lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                }
            }
            //SWING-U
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);

            remove_menu = new ContextMenuStrip();
            ToolStripMenuItem item = new ToolStripMenuItem("Remove");
            item.Click += new EventHandler(target_remove);
            remove_menu.Items.Add(item);

            remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

            listView_target_list.ContextMenuStrip = remove_menu;
            Tag_DT_PP.Columns.Add("SITE_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("WC_CODE", typeof(string));
            Tag_DT_PP.Columns.Add("CARD_NO", typeof(string));
            Tag_DT_PP.Columns.Add("IT_SCODE", typeof(string));
            Tag_DT_PP.Columns.Add("SIZE", typeof(string));
            Tag_DT_PP.Columns.Add("CHECK_YN", typeof(string));
            
            //선택된 작업자 
            
            
            
            if (regKey.GetValue("COM_PORT") == null)
            {
                regKey.SetValue("COM_PORT", "");
            }
            else
            {
                if (regKey.GetValue("COM_PORT").ToString() != "")
                {                    
                    combPort.SelectedItem = regKey.GetValue("COM_PORT").ToString();
                    //lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                }
            }

            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
        }
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                //작업표준서 가져오기
                timer_now.Stop();
                imageSlider1.Images.Clear();
                string ftp_str = AdjustDir(txt_IT_SCODE.Text.Trim());

                ftpUtil.FTPDirectioryCheck(ftp_str, "OUT");

                work_image = ftpUtil.get_file_list(ftp_str, "OUT");

                for (int i = 0; i < work_image.Length; i++)
                {
                    if (work_image[i] != null)
                        imageSlider1.Images.Add(work_image[i]);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                timer_now.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        public bool combPort_Auto()
        {
            bool check = false;
            combPort.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            

            if (ports.Length == 0)
            {
                check = false;
            }
            else
            {
                foreach (string port in ports)
                {
                    combPort.Items.Add(port);
                }
                check = true;
            }
            return check;
        }
        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        
                        Tag_DT_PP.Rows.Clear();
                        Tag_DT_DC.Rows.Clear();
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        void CloseThreadFunction(object normal)
        {
            bool normal_off = (bool)normal;

            Thread.Sleep(1000);
            this.Invoke(new EventHandler(delegate
            {
                button_com_close_Click(null, null);
            }));

            if (normal_off)
                MessageBox.Show("Swing-U is turned off by user", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Swing-U is turned down abnormaly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                
                Tag_DT_PP.Rows.Clear();
                Tag_DT_DC.Rows.Clear();
            }));

            Swing.ReportTagList();
        }

        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    //리딩
                    //PP카드 row add
                    DataRow dr = saveDataRow_PP(itemString[3]);
                    if (dr != null) { 
                        Tag_DT_PP.Rows.Add(dr);
                        
                    }
                    //대차 row add
                    //DataRow dr2 = saveDataRow_DC(itemString[3]);
                    //saveDataRow_DC_S(itemString[3]);
                    //if (Dc != "")
                    //{
                      //  carrier_no = Dc;
                        /*
                        Tag_DT_DC.Rows.Clear();
                        Tag_DT_DC.Rows.Add(dr2);
                         */
                        
                    //}
                }
            }));

            if (new_item)
            {

                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }

                /*if (dsmListView_ivt.Items.Count == 2)
                {
                    Swing.InventoryStop();
                }*/
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                    
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            
            try
            {
            
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);
                
                

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";

                    Tag_DT_PP.Rows.Clear();
                    Tag_DT_DC.Rows.Clear();
                    Swing.SetRFPower(27);
                    Swing.ReportAllInformation();

                    
                    //po_release_popup();
                    work_plan_popup();
                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
                WinConsole.WriteLine("{0} is closed successfully", Swing.PortName);
                if (btn_port_close.Enabled) { 
                    this.btn_port_close.PerformClick();
                }
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
            formClear();
        }
        #endregion
        ContextMenuStrip remove_menu;
        
        

        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            
                
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                //PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code
                Dt_Input.PP_SITE_CODE_str = PP_SITE_CODE_str;
                Dt_Input.mo_snumb = mo_snumb;
                Dt_Input.str_wc_code = str_wc_code;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {                    
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code);
                }
                
           
        }

        private void btn_fail_input_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                Fail_Input Fail_Input = new Fail_Input();
                Fail_Input.wc_group = wc_group;
                if (Fail_Input.ShowDialog() == DialogResult.OK)
                {

                    //불량 등록
                    int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                    if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb, str_wc_code))
                    { 
                        txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                        if (Fail_Input.lost_code.Trim().Equals("B16")) //중량미달 코드 일단 하드코딩
                        {
                            saveWeight(PP_SITE_CODE_str, mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code, vw_snumb, lblWeight.Text, "N");
                        }
                    }
                }
                this.Cursor = Cursors.Default;
            }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            
            metarial_search metarial_search = new metarial_search();
            metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            
            if (metarial_search.ShowDialog() == DialogResult.OK)
            {
            }
            
        }

        private void btn_mold_search_Click(object sender, EventArgs e)
        {
            mold_search mold_search = new mold_search();
            if (mold_search.ShowDialog() == DialogResult.OK)
            {
            }
        }


        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing.InventoryStop();
            Swing.ConnectionClose();
            parentForm.Visible = true;
        }
        

        private void button_inventory_start_Click(object sender, EventArgs e)
        {
            Swing.InventoryStart();
        }

        //PP카드 dataRow 저장
        public DataRow saveDataRow_PP(string str)
        {
            
            DataRow dr=null;
            string[] strTags = str.Split('*');
            try
            {
                if (strTags.Length == 2)
                {
                    if (strTags[0].Equals("PP"))
                    {
                        string[] strTag = strTags[1].Split('/');

                        if (strTag.Length == 5)
                        {
                            //if (str_wc_code.Trim() == strTag[1].Trim() && txt_IT_SCODE.Text.Trim() == strTag[3].Trim())
                            if (txt_IT_SCODE.Text.Trim() == strTag[3].Trim())
                            {
                                string sqty = "0";
                                dr = Tag_DT_PP.NewRow();
                                dr["SITE_CODE"] = strTag[0];
                                dr["WC_CODE"] = strTag[1];
                                dr["CARD_NO"] = strTag[2];
                                dr["IT_SCODE"] = strTag[3];


                                if (pp_card_check(strTag[3], strTag[2]))
                                {
                                    if (weight_Success.Visible)
                                    {
                                        Swing.InventoryStop();
                                        dr["CHECK_YN"] = "생산가능";
                                        if (btn_on_off.Text.Trim().Equals("On"))
                                        {
                                            dr["SIZE"] = btn_carrier_sqty.Text.Trim();
                                            sqty = btn_carrier_sqty.Text.Trim();

                                            if (txt_IT_SCODE.Text.Trim() == strTag[3].Trim())
                                            {
                                                int weight_cnt = int.Parse(txt_weight_cnt.Text.Trim());
                                                int pp_size = int.Parse(sqty);

                                                if (pp_size <= weight_cnt)
                                                {
                                                    PP_WC_CODE_str = strTag[1].Trim();
                                                    PP_IT_SCODE_str = strTag[3].Trim();
                                                    CARD_NO_str = strTag[2].Trim();
                                                    PP_SIZE_str = sqty;
                                                    PP_SITE_CODE_str = strTag[0].Trim();

                                                    //가실적 테이블 저장
                                                    work_input_save_virtual_IM(PP_SITE_CODE_str, mo_snumb, PP_IT_SCODE_str, str_wc_code.Trim(), PP_SIZE_str, txt_datetime.Text, CARD_NO_str, carrier_no);
                                                    weight_Success.TopLevel = false;
                                                    weight_Success.TopMost = false;
                                                    weight_Success.Visible = false;

                                                    PpCard_Success.TopLevel = true;
                                                    PpCard_Success.TopMost = true;
                                                    PpCard_Success.Visible = true;
                                                    PpCard_Success.set_text(CARD_NO_str, 60);


                                                    
                                                    Swing.TagListClear();
                                                    dsmListView_ivt.Items.Clear();
                                                    ddc_ivt.DigitText = "00000";
                                                    
                                                    Tag_DT_PP.Rows.Clear();
                                                    Tag_DT_DC.Rows.Clear();
                                                    carrier_no = "";
                                                    txtCarrier_no.Text = "";
                                                    txt_weight_cnt.Text = "0";
                                                    set_현재고_최대생산가능수량();
                                                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                                    txt_datetime.Text = r_start;

                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show("생산계획과 PP 카드 품목이 틀립니다.");
                                            }

                                        }
                                        else
                                        {
                                            dsmListView_ivt.Items.Clear();
                                            ddc_ivt.DigitText = "00000";
                                            Tag_DT_PP.Rows.Clear();
                                            Tag_DT_DC.Rows.Clear();
                                        }
                                    }
                                    else
                                    {
                                        dr["SIZE"] = strTag[4];
                                    }
                                }
                                else
                                {
                                    if (btn_on_off.Text.Trim().Equals("On"))
                                    {
                                        MessageBox.Show("이미 등록된 카드 입니다.");
                                    }
                                    dr["CHECK_YN"] = "생산완료";
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR" + ex.Message);
            }
            return dr;
        }
        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check(string prdt_item, string card_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //대차 코드만 dataRow 저장
        /*public DataRow saveDataRow_DC(string str)
        {
            DataRow dr = null;
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    dr = Tag_DT_DC.NewRow();
                    dr["DC"] = strTags[1];
                }
            }
            return dr;
        }*/
        public void saveDataRow_DC_S(string str)
        {
            if (string.IsNullOrWhiteSpace(txtCarrier_no.Text))
            {
                string[] strTags = str.Split('*');
                if (strTags.Length == 2)
                {
                    if (strTags[0].Equals("DC"))
                    {
                        if (carrier_check(strTags[1].Trim()))
                        {
                            if (strTags[1].Substring(0, 1).Equals("P"))//공용대차 일때
                            {

                                carrier_no = strTags[1].Trim();
                                txtCarrier_no.Text = carrier_no;

                            }
                            else
                            {
                                if (CHECK_FUNC.Match_carrier_no(txt_IT_SCODE.Text.Trim(), strTags[1].Trim()))//대차 번호 가 맞는지 체크 
                                {
                                    carrier_no = strTags[1].Trim();
                                    txtCarrier_no.Text = carrier_no;
                                }
                                else
                                {
                                    MessageBox.Show("품목에 해당하는 대차가 아닙니다.");
                                }
                            }
                        }
                    }
                }
            }
            
        }
        //작업지시 팝업 선택시 리더기 읽는 상태로 변경
        private void btn_po_release_Click(object sender, EventArgs e)
        {
            work_plan_popup();
        }
        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }

        public void work_plan_popup()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                if (true)
                {

                    Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                    Work_plan_search.wc_group = wc_group;
                    Work_plan_search.wc_code = str_wc_code;
                    if (Work_plan_search.ShowDialog() == DialogResult.OK)
                    {
                        formClear();
                        txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                        worker.RunWorkerAsync();
                        Injection_Condition_Popup Injection_Condition_Popup = new Injection_Condition_Popup();
                        Injection_Condition_Popup.it_scode = Work_plan_search.IT_SCODE_str;
                        Injection_Condition_Popup.wc_code = str_wc_code;
                        Injection_Condition_Popup.it_sname = Work_plan_search.IT_SNAME_str;
                        if (Injection_Condition_Popup.ShowDialog() == DialogResult.OK)
                        {

                        }
                        
                        txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                        txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                        txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        it_pkqty = Work_plan_search.IT_PKQTY_str;
                        btn_carrier_sqty.Text = it_pkqty;
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        alc_code = Work_plan_search.ALC_CODE_str;
                        max_sqty = Work_plan_search.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        a_weight = Work_plan_search.A_WEIGHT_str;
                        d_weight = Work_plan_search.D_WEIGHT_str;
                        txtD_weight.Text = "범위:" + a_weight + "±" + d_weight;
                        //자재유형 코드 가지고 오기(전역변수)
                        me_scode = Work_plan_search.ME_SCODE_str;
                        PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        //양품수량 불량수량 들고오기
                        GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        
                        //저울 수량 들고오기
                        txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
                        txt_good_qty.Text = GET_DATA.good_qty;
                        txt_fail_qty.Text = GET_DATA.fail_qty;
                        Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                        //Swing.InventoryStart();

                        //timer_po_start.Start();
                        //btn_save_cancle_enable_set();
                        set_현재고_최대생산가능수량();
                        shiftwork();//작업유형 가져오기
                        set_worker_info();//작업자 팝업
                        sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                        txt_day_sqty.Text = sqty_arry[0];
                        txt_night_sqty.Text = sqty_arry[1];
                        txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();

                        SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");

                        if (int.Parse(btn_carrier_sqty.Text.Trim()) <= int.Parse(txt_weight_cnt.Text.Trim()))
                        {

                            if (btn_on_off.Text.Trim().Equals("On"))
                            {
                                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                                Swing.InventoryStart();
                                //timer_pp_start.Start();
                                
                                weight_Success.TopLevel = true;
                                weight_Success.TopMost = true;
                                weight_Success.Visible = true;

                            }
                            else
                            {
                                MessageBox.Show("포장수량을 초과할 수 없습니다. 실적을 등록해 주세요");
                                btn_pp_card_search.PerformClick();
                            }
                        }
                        lueWc_code.Enabled = false;

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }
                /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }
         
        //폼 clear
        public void formClear()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txtDayAndNight.Text = null;
            btn_worker_info.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_day_sqty.Text = null;
            txt_night_sqty.Text = null;
            txt_fail_qty.Text = null;
            lblWeight.Text = null;
            label22.Text = null;
            label23.Text = null;
            txt_weight_cnt.Text = null;
            txtCarrier_no.Text = null;
            carrier_no = "";
            mo_snumb = "";
            Swing.InventoryStop();
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

        }
        //PP 카드 팝업, PP카드 선택시 가실적 등록, 로뜨번호 생성
        private void btn_pp_card_search_Click(object sender, EventArgs e)
        {
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();
            
            Swing.InventoryStart();
            //timer_pp_start.Start();
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("생산계획을 선택해주세요");
            }
            /*else if (string.IsNullOrWhiteSpace(carrier_no))
            {
                MessageBox.Show("대차 정보를 읽지 못했습니다.");
            }*/
            else
            {
                
                PPCard_Search PPCard_Search = new PPCard_Search();
                PPCard_Search.dt = Tag_DT_PP;
                PPCard_Search.wc_code = str_wc_code;
                if (PPCard_Search.ShowDialog() == DialogResult.OK)
                {
                    if (txt_IT_SCODE.Text.Trim() == PPCard_Search.IT_SCODE_str)
                    {
                        int weight_cnt = int.Parse(txt_weight_cnt.Text.Trim());
                        int pp_size = int.Parse(PPCard_Search.SIZE_str);
                        if (pp_size == 0)
                        {
                            Sqty_popup Sqty_popup = new Sqty_popup();
                            Sqty_popup.sqty = txt_weight_cnt.Text.Trim();
                            if (Sqty_popup.ShowDialog() == DialogResult.OK)
                            {
                                pp_size = int.Parse(Sqty_popup.sqty);
                            }
                            else
                            {
                                return;
                            }
                        }
                        if (pp_size==weight_cnt) 
                        { 
                            PP_WC_CODE_str = PPCard_Search.WC_CODE_str;
                            PP_IT_SCODE_str = PPCard_Search.IT_SCODE_str;
                            CARD_NO_str= PPCard_Search.CARD_NO_str;
                            PP_SIZE_str = PPCard_Search.SIZE_str;
                            PP_SITE_CODE_str = PPCard_Search.SITE_CODE_str;

                            //가실적 테이블 저장
                            work_input_save_virtual_IM(PP_SITE_CODE_str, mo_snumb, PP_IT_SCODE_str, PP_WC_CODE_str, PP_SIZE_str, txt_datetime.Text, CARD_NO_str, carrier_no);
                            Swing.TagListClear();
                            dsmListView_ivt.Items.Clear();
                            ddc_ivt.DigitText = "00000";
                            Tag_DT_PP.Rows.Clear();
                            Tag_DT_DC.Rows.Clear();
                            carrier_no = "";
                            txtCarrier_no.Text = "";
                            txt_weight_cnt.Text = "0";
                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                            txt_datetime.Text = r_start;
                            set_현재고_최대생산가능수량();
                            /*
                            if (me_scode == "40")
                            {
                                // 식별표 출력
                                it_chart_print(PP_IT_SCODE_str, int.Parse(PP_SIZE_str));
                            }*/
                        }
                    }
                    else
                    {
                        MessageBox.Show("생산계획과 PP 카드의 품목이 틀립니다.");
                    }
                }
            }
        }

        public void work_input_save_virtual_IM(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string pp_serno, string carrier_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_IM", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY",  float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY",  0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);

            //pp 시리얼번호
            cmd.Parameters.AddWithValue("@CARD_NO",  pp_serno);

            //대차 번호
            cmd.Parameters.AddWithValue("@CARRIER_NO",  "");

            cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@WORKER", str_worker);



            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                //양품수량 증가

                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                string hh = DateTime.Now.Hour.ToString();
                string mm = DateTime.Now.Minute.ToString();
                if (hh.Length == 1)
                {
                    hh = "0" + hh;
                }
                if (mm.Length == 1)
                {
                    mm = "0" + mm;
                }
                string time = hh + mm;


                if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                }
                

            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        
        
        //저울 원본 수신 데이터 저장
        public void save_received(string data)
        {
            //SP_RECEIVED_DATA_CS
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_RECEIVED_DATA_CS", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;
                        
            SqlParameter paramD_TEXT =
                    new SqlParameter("@D_TEXT", SqlDbType.VarChar, 200);
            paramD_TEXT.Value = data;
            cmd.Parameters.Add(paramD_TEXT);
            
            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
            }

            catch (Exception ex)
            {
                trans.Rollback();                
                MessageBox.Show("등록 실패 : " + ex.Message);
                

            }
            finally
            {
                conn.Close();
            }
        }

        delegate void SetTextCallback(string Text);
        #region 저울 값 received
        string dataValue = "";
        
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {            
            this.Invoke(new EventHandler(SerialReceived));             
        }
        private void SerialReceived(object o, EventArgs e)
        {
            try
            {
                Thread.Sleep(200);
                string weight = "";
                string gubn = "";
                dataValue = serialPort1.ReadExisting();
                //원본 데이터 저장
                save_received(dataValue);

                //중량만 가지고 오는 정규식 필요
                if (scale_type == "A")
                {
                    try
                    {
                        Regex rx = new Regex(@"[0-9]+[ ]*g{1}$");
                        Match mc = rx.Match(dataValue.Trim());
                        weight = mc.Value;

                        Regex rxf = new Regex(@"^[0-9]+");
                        //숫자 형식으로 들어왔는지 한번더 체크 
                        if (rxf.IsMatch(weight))
                        {
                            Match mcf = rxf.Match(weight);
                            weight = mcf.Value;
                            Regex rxfinal = new Regex(@"^[0-9]+$");
                            if (rxfinal.IsMatch(weight))
                            {
                                Regex rx_gubn = new Regex(@"[NY]");
                                Match mc_gubn = rx_gubn.Match(dataValue.Trim());
                                gubn = mc_gubn.Value.Trim();
                            }
                            else
                            {
                                weight_Fail weight_Fail = new weight_Fail();
                                weight_Fail.Show();
                                return;
                            }

                        }
                        else
                        {
                            weight_Fail weight_Fail = new weight_Fail();
                            weight_Fail.Show();
                            return;

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + " : (2)");
                        return;
                    }

                }
                else if (scale_type == "B")
                {
                    
                    
                    Regex rx_b = new Regex(@"[Gross]+[ ]+[0-9]+[ ]+g{1}");
                    Match mc_b = rx_b.Match(dataValue.Trim());
                    weight = mc_b.Value;
                    
                    Regex rxf_b = new Regex(@"[0-9]+");
                    Match mcf_b = rxf_b.Match(weight);
                    weight = mcf_b.Value;

                    int min_weight = int.Parse(a_weight) - int.Parse(d_weight);
                    int max_weight = int.Parse(a_weight) + int.Parse(d_weight);
                    if (min_weight <= int.Parse(weight) && int.Parse(weight) <= max_weight)
                    {
                        gubn = "Y";
                    }
                    else
                    {
                        gubn = "N";
                    }

                }
                
                if (int.Parse(weight) > 500)
                {
                    if (string.IsNullOrWhiteSpace(mo_snumb))
                    {
                        MessageBox.Show("작업지시를 선택해주세요");
                        return;
                    }
                    if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
                    {
                        MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                        return;
                    }

                    if (int.Parse(btn_carrier_sqty.Text.Trim()) <= int.Parse(txt_weight_cnt.Text.Trim()))
                    {

                        if (btn_on_off.Text.Trim().Equals("On"))
                        {
                            Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                            Swing.InventoryStart();
                            //timer_pp_start.Start();
                            
                            weight_Success.TopLevel = true;
                            weight_Success.TopMost = true;
                            weight_Success.Visible = true;
                            
                        }
                        else
                        {
                            MessageBox.Show("포장수량을 초과할 수 없습니다. 실적을 등록해 주세요");
                            btn_pp_card_search.PerformClick();
                        }
                    }
                    else
                    {
                        lblWeight.Text = weight.Trim();

                        //가실적 번호 가져오기
                        int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);

                        //중량 테이블 저장

                        if (gubn.Equals("Y"))
                        {
                            //saveWeight(PP_SITE_CODE_str, mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code, vw_snumb, lblWeight.Text, gubn);
                            //txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();
                            SqlTransaction tran = null;
                            string strCon;
                            strCon = Properties.Settings.Default.SQL_DKQT;

                            SqlConnection conn = new SqlConnection(strCon);
                            conn.Open();
                            tran = conn.BeginTransaction();
                            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_SAVE_WEIGHT", conn);
                            da.SelectCommand.Transaction = tran;

                            da.SelectCommand.CommandType = CommandType.StoredProcedure;

                            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", PP_SITE_CODE_str);
                            da.SelectCommand.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
                            da.SelectCommand.Parameters.AddWithValue("@PRDT_ITEM", txt_IT_SCODE.Text.Trim());
                            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", str_wc_code);
                            da.SelectCommand.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);
                            da.SelectCommand.Parameters.AddWithValue("@IN_WEIGHT", float.Parse(lblWeight.Text.Trim()));
                            da.SelectCommand.Parameters.AddWithValue("@CHECK_YN", gubn);


                            DataSet ds = new DataSet();
                            try
                            {
                                da.Fill(ds, "WEIGHT_CHART");
                                if (gubn.Trim().Equals("Y"))
                                {
                                    try
                                    {
                                           
                                        Weight_print.SetDataSource(ds);
                                        
                                        Weight_print.PrintToPrinter(1, false, 1, 1);
                                        
                                        
                                        txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();

                                        //if (!serialPort1.IsOpen)
                                        //{
                                        //    serialPort1.Open();
                                        //}
                                        tran.Commit();

                                    }
                                    catch (Exception ex)
                                    {
                                        tran.Rollback();
                                        conn.Close();

                                        MessageBox.Show(ex.StackTrace + ", " + ex.Message + " : 중량을 다시 측정해 주세요");
                                        return;
                                    }
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message + " : (0)");
                            }
                            finally
                            {
                                conn.Close();
                            }
                            
                        }


                        if (int.Parse(txt_weight_cnt.Text.Trim())>=int.Parse(btn_carrier_sqty.Text.Trim()))
                        {
                            if (btn_on_off.Text.Trim().Equals("On"))
                            {
                                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                                Swing.InventoryStart();
                                //timer_pp_start.Stop();
                                //timer_pp_start.Start();

                                weight_Success.TopLevel = true;
                                weight_Success.TopMost = true;
                                weight_Success.Visible = true;
                                
                            }
                            else
                            {
                                btn_pp_card_search.PerformClick();
                            }
                        }
                    }
                    //btn_save_cancle_enable_set();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " : (1)");
                return;
            }
        }

        
        //저울값 중량 테이블에 저장
        public void saveWeight(string site_code, string rsrv_no, string prdt_item, string wc_code,int in_serno, string weight,string check_yn)
        {
            SqlTransaction tran = null;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            tran = conn.BeginTransaction();
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_SAVE_WEIGHT", conn);
            da.SelectCommand.Transaction = tran;

            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", site_code);
            da.SelectCommand.Parameters.AddWithValue("@RSRV_NO", rsrv_no);
            da.SelectCommand.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@VW_SNUMB", in_serno);
            da.SelectCommand.Parameters.AddWithValue("@IN_WEIGHT", float.Parse(weight));
            da.SelectCommand.Parameters.AddWithValue("@CHECK_YN", check_yn);
            

            DataSet ds = new DataSet();
            try
            {
                da.Fill(ds, "WEIGHT_CHART");
                if (check_yn.Trim().Equals("Y"))
                {
                    try
                    {
                        
                        Weight_print.SetDataSource(ds);
                        Weight_print.PrintToPrinter(1, false, 1, 1);
                        tran.Commit();
                    }
                    catch
                    {
                        
                        Weight_print.SetDataSource(ds);
                        Weight_print.PrintToPrinter(1, false, 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+" : (3)");
            }
            
            
        }
        #endregion

        //저울 포트 open
        //저울 타입
        string scale_type = "";
        private void btn_port_open_Click(object sender, EventArgs e)
        {
            
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("생산계획을 선택해 주세요");
                }
                else
                {
                    try
                    {
                        Scale_type_Popup Scale_type_Popup = new Popup.Scale_type_Popup();
                        if (Scale_type_Popup.ShowDialog() == DialogResult.OK)
                        {
                            serialPort1.PortName = combPort.Text;
                            serialPort1.Open();
                            btn_port_open.Enabled = false;
                            btn_port_close.Enabled = true;                        
                            scale_type = Scale_type_Popup.scale_type;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            
        }

        //저울 포트 close
        private void btn_port_close_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            btn_port_open.Enabled = true;
            btn_port_close.Enabled = false;
        }


        
        

        public bool carrier_check(string carrier_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from CARRIER_REG WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            regKey.SetValue("WC_CODE", str_wc_code);
            worker_Dt.Clear();
            
        }

        private void Injection_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
            timesync(GET_DATA.maxTimeSync());
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Swing.InventoryStop();
            parentForm.Visible = true;
            this.Close();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string datavalue = textBox1.Text.Trim();
            string weight = "";
            string gubn = "";
            Regex rx = new Regex(@"[0-9]+[ ]*g{1}$");
            Match mc = rx.Match(datavalue.Trim());
            weight = mc.Value;

            Regex rxf = new Regex(@"^[0-9]+");
            //숫자 형식으로 들어왔는지 한번더 체크 
            if (rxf.IsMatch(weight))
            {
                Match mcf = rxf.Match(weight);
                weight = mcf.Value;
                Regex rxfinal = new Regex(@"^[0-9]+$");
                if (rxfinal.IsMatch(weight))
                {
                    Regex rx_gubn = new Regex(@"[NY]");
                    Match mc_gubn = rx_gubn.Match(datavalue.Trim());
                    gubn = mc_gubn.Value.Trim();
                }
                else
                {
                    weight_Fail weight_Fail = new weight_Fail();
                    weight_Fail.Show();
                    return;
                }

            }
            else
            {
                weight_Fail weight_Fail = new weight_Fail();
                weight_Fail.Show();
                return;

            }
            MessageBox.Show(weight+"  "+gubn);
        }

        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {

            string[] arryPort_power_out = null;
            try
            {
                if (string.IsNullOrWhiteSpace(lueWc_code.Text.Trim()))
                {
                    MessageBox.Show("작업장을 선택해주세요");
                    return;
                }
                arryPort_power_out = GET_DATA.get_connection_port(lueWc_code.GetColumnValue("WC_CODE").ToString(), "OUT");
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());

                Swing.ConnectionOpen(arryPort_power_out[0], 15);

                if (Swing.IsOpen)
                {
                    WinConsole.WriteLine("{0} is opend successfully", Swing.PortName);
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";

                    Tag_DT_PP.Rows.Clear();
                    Tag_DT_DC.Rows.Clear();
                    Swing.SetRFPower(30-int.Parse(arryPort_power_out[1]));
                    Swing.ReportAllInformation();


                    //po_release_popup();
                    work_plan_popup();
                    
                    this.btn_port_open.PerformClick();
                    
                    //Swing.InventoryStart();
                }
                else
                {
                    WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                WinConsole.WriteLine("Failed to open {0}", Swing.PortName);
                WinConsole.WriteLine(ex.Message);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
            saveWeight(PP_SITE_CODE_str, mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code, vw_snumb, "2048", "Y");
            txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();
            lblWeight.Text = "2048";
            //print_weight(mo_snumb,alc_code);
        }
        public void btn_save_cancle_enable_set()
        {
            if (txt_weight_cnt.Text.Trim() == "0")
            {
                int max_vw_snumb = CHECK_FUNC.max_vw_snumb(mo_snumb, PP_WC_CODE_str);
                if (max_vw_snumb == 0)
                {
                    btn_save_cancel.Enabled = false;
                }
                else
                {
                    if (CHECK_FUNC.cancle_MOVE_check(mo_snumb, max_vw_snumb, PP_WC_CODE_str))
                    {
                        btn_save_cancel.Enabled = true;
                    }
                    else
                    {
                        btn_save_cancel.Enabled = false;
                    }
                }
            }
            else
            {
                btn_save_cancel.Enabled = false;
            }
        }

        private void btn_save_cancel_Click(object sender, EventArgs e)
        {
            int max_vw_snumb = CHECK_FUNC.max_vw_snumb(mo_snumb, PP_WC_CODE_str);
            cancel_work_input(mo_snumb, max_vw_snumb, PP_WC_CODE_str);
            //양품수량 불량수량 들고오기
            GET_DATA.get_good_fail(mo_snumb, str_wc_code);
            //저울 수량 들고오기
            txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
            txt_good_qty.Text = GET_DATA.good_qty;
            txt_fail_qty.Text = GET_DATA.fail_qty;
            Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();
            carrier_no = "";
            txtCarrier_no.Text = "";
        }
        public void cancel_work_input(string rsrv_no,int vw_snumb,string wc_code)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_CANCEL_WORK_INPUT", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            SqlParameter paramRSRV_NO =
                    new SqlParameter("@RSRV_NO", SqlDbType.VarChar, 10);
            paramRSRV_NO.Value = rsrv_no;
            cmd.Parameters.Add(paramRSRV_NO);
            //작업지시번호
            SqlParameter paramVW_SNUMB =
                    new SqlParameter("@VW_SNUMB", SqlDbType.Int, 4);
            paramVW_SNUMB.Value = vw_snumb;
            cmd.Parameters.Add(paramVW_SNUMB);

            //품목코드
            SqlParameter paramWC_CODE =
                    new SqlParameter("@WC_CODE", SqlDbType.VarChar, 10);
            paramWC_CODE.Value = wc_code;
            cmd.Parameters.Add(paramWC_CODE);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
            }

            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();
            }
        }

        private void btn_injection_condition_Click(object sender, EventArgs e)
        {
            Injection_Condition_Popup Injection_Condition_Popup = new Injection_Condition_Popup();
            Injection_Condition_Popup.it_scode = txt_IT_SCODE.Text.Trim();
            Injection_Condition_Popup.wc_code = str_wc_code;
            Injection_Condition_Popup.it_sname = txt_IT_SNAME.Text.Trim();
            if (Injection_Condition_Popup.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            KeyPad KeyPad = new KeyPad();
            KeyPad.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /*
            PpCard_Success.TopLevel = true;
            PpCard_Success.TopMost = true;
            PpCard_Success.Visible = true;
            PpCard_Success.set_text("P001", 11);*/

            /*
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                MessageBox.Show(PrinterSettings.InstalledPrinters[i]);
            }
            TEST_PRINT TEST_PRINT = new TEST_PRINT();
            TEST_PRINT.PrintOptions.PrinterName = "ZDesigner GT800 (ZPL)";
            TEST_PRINT.PrintToPrinter(1, false, 0, 0);
            */
            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                MessageBox.Show(PrinterSettings.InstalledPrinters[i]);
            }
            
        }

        private void button6_Click(object sender, EventArgs e)
        {

            timesync(GET_DATA.maxTimeSync());
        }


        [DllImport("Kernel32.dll")]
        public static extern void SetSystemTime([In] SystemTime st);


        [StructLayout(LayoutKind.Sequential)]
        public class SystemTime
        {
            public ushort year;
            public ushort month;
            public ushort dayofweek;
            public ushort day;
            public ushort hour;
            public ushort minute;
            public ushort second;
            public ushort milliseconds;
        }




        public void timesync(string LSysdate)
        {
            if (!string.IsNullOrWhiteSpace(LSysdate))
            {
                string l_datetime = DateTime.Parse(LSysdate).AddHours(-9).ToString("yyyy-MM-dd HH:mm:ss");

                DateTime sysTime = new DateTime(Convert.ToInt32(l_datetime.Substring(0, 4).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(5, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(8, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(11, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(14, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(17, 2).ToString()));

                SystemTime sTime = new SystemTime();
                sTime.year = Convert.ToUInt16(sysTime.Year);
                sTime.month = Convert.ToUInt16(sysTime.Month);
                sTime.dayofweek = Convert.ToUInt16(sysTime.DayOfWeek);
                sTime.day = Convert.ToUInt16(sysTime.Day);
                sTime.hour = Convert.ToUInt16(sysTime.Hour);
                sTime.minute = Convert.ToUInt16(sysTime.Minute);
                sTime.second = Convert.ToUInt16(sysTime.Second);
                sTime.milliseconds = Convert.ToUInt16(sysTime.Millisecond);

                SetSystemTime(sTime);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(CHECK_FUNC.IsConnectedToInternet().ToString());
        }

        private void combPort_SelectionChangeCommitted(object sender, EventArgs e)
        {
            regKey.SetValue("COM_PORT", combPort.SelectedItem.ToString());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            /*
            if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS)
            {
                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.SINGLE);
            }
            else
            {
                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
            }*/

            Work_plan_search_dev Work_plan_search = new Work_plan_search_dev();
                Work_plan_search.wc_group = wc_group;
                Work_plan_search.wc_code = str_wc_code;
                if (Work_plan_search.ShowDialog() == DialogResult.OK)
                {
                    Injection_Condition_Popup Injection_Condition_Popup = new Injection_Condition_Popup();
                    Injection_Condition_Popup.it_scode = Work_plan_search.IT_SCODE_str;
                    Injection_Condition_Popup.wc_code = str_wc_code;
                    Injection_Condition_Popup.it_sname = Work_plan_search.IT_SNAME_str;
                    if (Injection_Condition_Popup.ShowDialog() == DialogResult.OK)
                    {

                    }
                    formClear();
                    txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                    txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                    txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                    //txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                    //str_wc_code = Work_plan_search.WC_CODE_str;
                    it_pkqty = Work_plan_search.IT_PKQTY_str;
                    btn_carrier_sqty.Text = it_pkqty;
                    mo_snumb = Work_plan_search.MO_SNUMB_str;
                    alc_code = Work_plan_search.ALC_CODE_str;
                    max_sqty = Work_plan_search.MAX_SQTY_str;
                    txt_MO_SQUTY.Text = max_sqty;
                    a_weight=Work_plan_search.A_WEIGHT_str;
                    d_weight = Work_plan_search.D_WEIGHT_str;
                    txtD_weight.Text = "범위:" + a_weight + "±" + d_weight;
                    //자재유형 코드 가지고 오기(전역변수)
                    me_scode = Work_plan_search.ME_SCODE_str;
                    PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                    //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                    txt_datetime.Text = r_start;
                    //양품수량 불량수량 들고오기
                    GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                    //저울 수량 들고오기
                    txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
                    txt_good_qty.Text = GET_DATA.good_qty;
                    txt_fail_qty.Text = GET_DATA.fail_qty;
                    Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                    //Swing.InventoryStart();

                    
                    //btn_save_cancle_enable_set();
                    set_현재고_최대생산가능수량();
                    shiftwork();//작업유형 가져오기
                    set_worker_info();//작업자 팝업
                    sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                    txt_day_sqty.Text = sqty_arry[0];
                    txt_night_sqty.Text = sqty_arry[1];

                }

            
                /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/

            
        }

        private void txtCarrier_no_Click(object sender, EventArgs e)
        {

            txtCarrier_no.Text = "";
            carrier_no = "";

            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";

            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

            
                Swing.TagListClear();
                Swing.InventoryStart();
                
            
        }

        private void btn_on_off_Click(object sender, EventArgs e)
        {
            if (btn_on_off.Text.Trim().Equals("On"))
            {
                btn_on_off.Text = "Off";
            }
            else
            {
                btn_on_off.Text = "On";
            }

        }

        private void btn_carrier_sqty_Click(object sender, EventArgs e)
        {
            KeyPad KeyPad = new KeyPad();
            if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                btn_carrier_sqty.Text = KeyPad.txt_value;
            }
            if (int.Parse(txt_weight_cnt.Text.Trim()) >= int.Parse(btn_carrier_sqty.Text.Trim()))
            {
                if (btn_on_off.Text.Trim().Equals("On"))
                {
                    Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                    Swing.InventoryStart();
                    //timer_pp_start.Stop();
                    //timer_pp_start.Start();

                    weight_Success.TopLevel = true;
                    weight_Success.TopMost = true;
                    weight_Success.Visible = true;

                }
                else
                {
                    btn_pp_card_search.PerformClick();
                }
            }
            /*
            if (btn_carrier_sqty.Text.Trim().Equals(it_pkqty))
            {
                btn_carrier_sqty.Text = it_pkqty_temp;
            }
            else
            {
                btn_carrier_sqty.Text = it_pkqty;
            }*/
        }
        //작업유형 가져오기
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code =shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            //Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_group = wc_group;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;

                //작업 화면에 표시 해주기 위한 변수
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }

                //DB에 저장 하기 위한 변수
                str_worker = "";
                DataRow[] drr = select_worker_dt.Select();
                for (int i = 0; i < drr.Length; i++)
                {
                    if (i == drr.Length - 1)
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString();
                    }
                    else
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString() + ",";
                    }
                }

            }
        }

        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            set_worker_info();
        }
        Random r;
        private void btn_Manually_sqty_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("작업지시를 선택해주세요");
                return;
            }
            if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
            {
                MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                return;
            }
            if (txt_weight_cnt.Text.Trim() == btn_carrier_sqty.Text.Trim())
            {
                MessageBox.Show("대차에 수량이 가득 찼습니다. \n 수동으로 수량을 입력 할 수 없습니다.");
                return;
            }
            
            else
            {
                Injection_Sqty_Popup Injection_Sqty_Popup = new Injection_Sqty_Popup();
                Injection_Sqty_Popup.base_sqty = btn_carrier_sqty.Text.Trim();
                Injection_Sqty_Popup.sqty = txt_weight_cnt.Text.Trim();
                if (Injection_Sqty_Popup.ShowDialog() == DialogResult.OK)
                {
                    string gubn = "Y";
                    int max_num = int.Parse(Injection_Sqty_Popup.sqty.Trim());
                    //가실적 번호 가져오기
                    if ((max_num + int.Parse(txt_weight_cnt.Text.Trim())) > int.Parse(btn_carrier_sqty.Text.Trim()))
                    {
                        MessageBox.Show("수량을 초과 할 수 없습니다.");
                        return;
                    }
                    int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                    for (int i = 0; i < max_num; i++)
                    {
                        //중량 테이블 저장
                        //난수 발생
                        r = new Random();
                        int min_weight = int.Parse(a_weight) - int.Parse(d_weight);
                        int max_weight = int.Parse(a_weight) + int.Parse(d_weight);
                        int winning_number = r.Next(min_weight, max_weight);
                        lblWeight.Text = winning_number.ToString();
                        if (gubn.Equals("Y"))
                        {
                            //saveWeight(PP_SITE_CODE_str, mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code, vw_snumb, lblWeight.Text, gubn);
                            //txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();
                            SqlTransaction tran = null;
                            string strCon;
                            strCon = Properties.Settings.Default.SQL_DKQT;

                            SqlConnection conn = new SqlConnection(strCon);
                            conn.Open();
                            tran = conn.BeginTransaction();
                            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_SAVE_WEIGHT", conn);
                            da.SelectCommand.Transaction = tran;

                            da.SelectCommand.CommandType = CommandType.StoredProcedure;

                            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", PP_SITE_CODE_str);
                            da.SelectCommand.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
                            da.SelectCommand.Parameters.AddWithValue("@PRDT_ITEM", txt_IT_SCODE.Text.Trim());
                            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", str_wc_code);
                            da.SelectCommand.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);
                            da.SelectCommand.Parameters.AddWithValue("@IN_WEIGHT", float.Parse(winning_number.ToString()));
                            da.SelectCommand.Parameters.AddWithValue("@CHECK_YN", gubn);


                            DataSet ds = new DataSet();
                            try
                            {
                                da.Fill(ds, "WEIGHT_CHART");
                                if (gubn.Trim().Equals("Y"))
                                {
                                    try
                                    {
                                        Weight_print.SetDataSource(ds);
                                        Weight_print.PrintToPrinter(1, false, 1, 1);

                                        tran.Commit();
                                        
                                        txt_weight_cnt.Text = (int.Parse(txt_weight_cnt.Text.Trim()) + 1).ToString();

                                        if (!serialPort1.IsOpen)
                                        {
                                            serialPort1.Open();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        tran.Rollback();
                                        conn.Close();

                                        MessageBox.Show(ex.StackTrace + ", " + ex.Message + " : 중량을 다시 측정해 주세요");
                                        return;
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message + " : (0)");
                            }
                            finally
                            {
                                conn.Close();
                            }

                        }
                        if (int.Parse(txt_weight_cnt.Text.Trim()) >=int.Parse(btn_carrier_sqty.Text.Trim()))
                        {
                            if (btn_on_off.Text.Trim().Equals("On"))
                            {
                                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                                Swing.InventoryStart();
                                
                                weight_Success.TopLevel = true;
                                weight_Success.TopMost = true;
                                weight_Success.Visible = true;
                            }
                            else
                            {
                                btn_pp_card_search.PerformClick();
                            }
                        }
                    }
                        
                }
            }
        }

        private void btn_Manually_carrier_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                MessageBox.Show("생산 계획을 선택해 주세요");
                return;
            }
            Injection_Carrier_KeyPad Injection_Carrier_KeyPad = new Injection_Carrier_KeyPad();
            Injection_Carrier_KeyPad.it_scode = txt_IT_SCODE.Text.Trim();
            Injection_Carrier_KeyPad.wc_group = wc_group;
            if (Injection_Carrier_KeyPad.ShowDialog() == DialogResult.OK)
            {
                carrier_no= Injection_Carrier_KeyPad.txt_value;
                txtCarrier_no.Text = carrier_no;
            }
        }

        private void btn_fail_search_Click(object sender, EventArgs e)
        {
            
            fail_search_popup fail_search_popup = new fail_search_popup();
            fail_search_popup.wc_group = wc_group;
            fail_search_popup.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            fail_search_popup.wc_name = lueWc_code.GetColumnValue("WC_NAME").ToString();
            fail_search_popup.Show();
            
        }

        private void btn_po_release_Insert_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                if (true)
                {

                    Po_release_Insert_dev Po_release_Insert_dev = new Po_release_Insert_dev();
                    Po_release_Insert_dev.wc_group = wc_group;
                    Po_release_Insert_dev.wc_code = str_wc_code;
                    if (Po_release_Insert_dev.ShowDialog() == DialogResult.OK)
                    {
                        Injection_Condition_Popup Injection_Condition_Popup = new Injection_Condition_Popup();
                        Injection_Condition_Popup.it_scode = Po_release_Insert_dev.IT_SCODE_str;
                        Injection_Condition_Popup.wc_code = str_wc_code;
                        Injection_Condition_Popup.it_sname = Po_release_Insert_dev.IT_SNAME_str;
                        if (Injection_Condition_Popup.ShowDialog() == DialogResult.OK)
                        {

                        }
                        formClear();
                        txt_IT_SCODE.Text = Po_release_Insert_dev.IT_SCODE_str;
                        txt_IT_SNAME.Text = Po_release_Insert_dev.IT_SNAME_str;
                        txt_IT_MODEL.Text = Po_release_Insert_dev.IT_MODEL_str;
                        txt_MO_SQUTY.Text = Po_release_Insert_dev.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        it_pkqty = Po_release_Insert_dev.IT_PKQTY_str;
                        btn_carrier_sqty.Text = it_pkqty;
                        mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                        alc_code = Po_release_Insert_dev.ALC_CODE_str;
                        max_sqty = Po_release_Insert_dev.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        a_weight = Po_release_Insert_dev.A_WEIGHT_str;
                        d_weight = Po_release_Insert_dev.D_WEIGHT_str;
                        txtD_weight.Text = "범위:" + a_weight + "±" + d_weight;
                        //자재유형 코드 가지고 오기(전역변수)
                        me_scode = Po_release_Insert_dev.ME_SCODE_str;
                        PP_SITE_CODE_str = Po_release_Insert_dev.SITE_CODE_str;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        //양품수량 불량수량 들고오기
                        GET_DATA.get_good_fail(mo_snumb, str_wc_code);

                        //저울 수량 들고오기
                        txt_weight_cnt.Text = GET_DATA.get_weight_cnt(mo_snumb, str_wc_code);
                        txt_good_qty.Text = GET_DATA.good_qty;
                        txt_fail_qty.Text = GET_DATA.fail_qty;
                        Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                        //Swing.InventoryStart();

                        //timer_po_start.Start();
                        //btn_save_cancle_enable_set();
                        set_현재고_최대생산가능수량();
                        shiftwork();//작업유형 가져오기
                        set_worker_info();//작업자 팝업
                        sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text, str_wc_code);
                        txt_day_sqty.Text = sqty_arry[0];
                        txt_night_sqty.Text = sqty_arry[1];
                        txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();

                        SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");

                        if (int.Parse(btn_carrier_sqty.Text.Trim()) <= int.Parse(txt_weight_cnt.Text.Trim()))
                        {

                            if (btn_on_off.Text.Trim().Equals("On"))
                            {
                                Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);
                                Swing.InventoryStart();
                                //timer_pp_start.Start();

                                weight_Success.TopLevel = true;
                                weight_Success.TopMost = true;
                                weight_Success.Visible = true;

                            }
                            else
                            {
                                MessageBox.Show("포장수량을 초과할 수 없습니다. 실적을 등록해 주세요");
                                btn_pp_card_search.PerformClick();
                            }
                        }
                        lueWc_code.Enabled = false;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 : "+ex.Message);
            }
        }

        private void timer_now_Tick_1(object sender, EventArgs e)
        {
            imageSlider1.SlideNext();
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(str_wc_code.ToString().Trim()))
            {
                MessageBox.Show("작업장을 선택 해 주새요");
                return;
            }
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(parentForm);
            FME_InsResultReg.wc_code = str_wc_code.ToString().Trim();
            FME_InsResultReg.Show();
        }

        private void btn_program_finish_Click(object sender, EventArgs e)
        {
            Finish_Popup Finish_Popup = new Finish_Popup();
            if (Finish_Popup.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Finish_Popup.btn_state.Equals("0"))
                {

                }
                else if (Finish_Popup.btn_state.Equals("1"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    Application.Exit();
                }
                else if (Finish_Popup.btn_state.Equals("2"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            saveDataRow_PP(textBox2.Text.Trim());
        }

    }
}
