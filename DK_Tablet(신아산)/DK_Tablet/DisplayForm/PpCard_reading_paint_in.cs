﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.DisplayForm
{
    public partial class PpCard_reading_paint_in : Form
    {
        Paint_In parent;
        Work_Center_In parent_w;
        Work_Center_IN_NEW parent_w_NEW;
        string Display_State;
        public PpCard_reading_paint_in(Paint_In main)
        {
            this.parent = main;
            InitializeComponent();
            Display_State = "1";
        }
        public PpCard_reading_paint_in(Work_Center_In main)
        {
            this.parent_w = main;
            InitializeComponent();
            Display_State = "2";
        }
        public PpCard_reading_paint_in(Work_Center_IN_NEW main)
        {
            this.parent_w_NEW = main;
            InitializeComponent();
            Display_State = "3";
        }

        private void Injection_Success_Load(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (Display_State.Equals("1"))
            {
                this.TopLevel = false;
                this.TopMost = false;
                this.Visible = false;
                parent.Swing.InventoryStop();
            }
            else if (Display_State.Equals("2"))
            {
                this.TopLevel = false;
                this.TopMost = false;
                this.Visible = false;
                parent_w.Swing.InventoryStop();
            }
            else if (Display_State.Equals("3"))
            {
                this.TopLevel = false;
                this.TopMost = false;
                this.Visible = false;
                
            }
        }
        
    }
}
