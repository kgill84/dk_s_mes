﻿namespace DK_Tablet
{
    partial class Injection_carrier
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Injection_carrier));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.button_com_open = new System.Windows.Forms.Button();
            this.button_com_close = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtD_weight = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_inspection = new DevExpress.XtraEditors.SimpleButton();
            this.txt_MO_SQUTY = new System.Windows.Forms.Label();
            this.btn_worker_info = new System.Windows.Forms.Button();
            this.btn_re_print = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_night_sqty = new System.Windows.Forms.Label();
            this.txt_day_sqty = new System.Windows.Forms.Label();
            this.imageSlider1 = new DevExpress.XtraEditors.Controls.ImageSlider();
            this.txt_IT_MODEL = new System.Windows.Forms.Label();
            this.txt_IT_SCODE = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.ts_carrier = new DevExpress.XtraEditors.ToggleSwitch();
            this.btn_po_release_Insert = new System.Windows.Forms.Button();
            this.btn_Manually_sqty = new DevExpress.XtraEditors.SimpleButton();
            this.txtCarrier_no = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tS_auto = new DevExpress.XtraEditors.ToggleSwitch();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_datetime = new System.Windows.Forms.Label();
            this.txt_weight_cnt = new System.Windows.Forms.Label();
            this.txt_fail_qty = new System.Windows.Forms.Label();
            this.txt_good_qty = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.btn_pp_card_search = new System.Windows.Forms.Button();
            this.btn_dt_input = new System.Windows.Forms.Button();
            this.btn_fail_input = new System.Windows.Forms.Button();
            this.btn_meterial_search = new System.Windows.Forms.Button();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbl_now_sqty = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDayAndNight = new System.Windows.Forms.Label();
            this.txt_IT_SNAME = new System.Windows.Forms.Label();
            this.btn_save_cancel = new System.Windows.Forms.Button();
            this.btn_on_off = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lab_carrier_sqty = new DevExpress.XtraEditors.LabelControl();
            this.btn_carrier_sqty = new DevExpress.XtraEditors.SimpleButton();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.Btn_Auto_Connection = new System.Windows.Forms.Button();
            this.comboBox_inventory_mode = new System.Windows.Forms.ComboBox();
            this.button_inventory_start = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.combPort = new System.Windows.Forms.ComboBox();
            this.btn_port_open = new System.Windows.Forms.Button();
            this.btn_port_close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.listView_target_list = new DK_Tablet.dsmListView(this.components);
            this.ddc_ivt = new Owf.Controls.DigitalDisplayControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dsmListView_ivt = new DK_Tablet.dsmListView(this.components);
            this.N_timer = new DevExpress.XtraEditors.LabelControl();
            this.btn_fail_search = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer_now = new System.Windows.Forms.Timer(this.components);
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader55 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader56 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader57 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader58 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader59 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader60 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader61 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader62 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader63 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader64 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader65 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader66 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader67 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader68 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_print_type = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader69 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader70 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader71 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader72 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader73 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader74 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader75 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_program_finish = new DevExpress.XtraEditors.SimpleButton();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.columnHeader76 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader77 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader78 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader79 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader80 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader81 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader82 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader83 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader84 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader85 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader86 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader87 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader88 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader89 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btn_mold_search = new System.Windows.Forms.Button();
            this.columnHeader90 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader91 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader92 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader93 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader94 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader95 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader96 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ts_carrier.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tS_auto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "NO.";
            this.columnHeader1.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "R/B";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 38;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TAG UID ▽";
            this.columnHeader2.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Counts";
            this.columnHeader3.Width = 80;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "RSSI";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Meters";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NO.";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "R/B";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 38;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "TAG UID ▽";
            this.columnHeader9.Width = 240;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Counts";
            this.columnHeader10.Width = 80;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "RSSI";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Meters";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_ports.DropDownWidth = 500;
            this.comboBox_ports.Font = new System.Drawing.Font("굴림", 19F);
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(497, 3);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(71, 33);
            this.comboBox_ports.TabIndex = 1;
            // 
            // button_com_open
            // 
            this.button_com_open.Location = new System.Drawing.Point(570, 3);
            this.button_com_open.Name = "button_com_open";
            this.button_com_open.Size = new System.Drawing.Size(52, 32);
            this.button_com_open.TabIndex = 2;
            this.button_com_open.Text = "연결";
            this.button_com_open.UseVisualStyleBackColor = true;
            this.button_com_open.Click += new System.EventHandler(this.button_com_open_Click);
            // 
            // button_com_close
            // 
            this.button_com_close.Location = new System.Drawing.Point(621, 3);
            this.button_com_close.Name = "button_com_close";
            this.button_com_close.Size = new System.Drawing.Size(52, 32);
            this.button_com_close.TabIndex = 2;
            this.button_com_close.Text = "해제";
            this.button_com_close.UseVisualStyleBackColor = true;
            this.button_com_close.Click += new System.EventHandler(this.button_com_close_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(2, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 35);
            this.label2.TabIndex = 0;
            this.label2.Text = "생산일자";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(477, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 71);
            this.label3.TabIndex = 0;
            this.label3.Text = "품    번";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(13, 256);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 50);
            this.label4.TabIndex = 0;
            this.label4.Text = "최대보관수량";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(1041, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 29);
            this.label5.TabIndex = 0;
            this.label5.Text = "측정중량(g)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(1154, 168);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 33);
            this.label9.TabIndex = 0;
            this.label9.Text = "생산수량";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(419, 168);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(210, 87);
            this.label10.TabIndex = 0;
            this.label10.Text = "품    명";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Visible = false;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(419, 86);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 83);
            this.label11.TabIndex = 0;
            this.label11.Text = "근무유형";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(1041, 1);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 71);
            this.label12.TabIndex = 0;
            this.label12.Text = "작업자";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(2, 36);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 35);
            this.label13.TabIndex = 0;
            this.label13.Text = "차종";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(1041, 388);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(226, 33);
            this.label15.TabIndex = 0;
            this.label15.Text = "불량수량";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(1041, 301);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(226, 33);
            this.label16.TabIndex = 0;
            this.label16.Text = "누적수량";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtD_weight
            // 
            this.txtD_weight.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtD_weight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtD_weight.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.txtD_weight.Location = new System.Drawing.Point(1041, 102);
            this.txtD_weight.Margin = new System.Windows.Forms.Padding(0);
            this.txtD_weight.Name = "txtD_weight";
            this.txtD_weight.Size = new System.Drawing.Size(226, 28);
            this.txtD_weight.TabIndex = 0;
            this.txtD_weight.Text = "범위 : ";
            this.txtD_weight.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Yellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btn_inspection);
            this.panel1.Controls.Add(this.txt_MO_SQUTY);
            this.panel1.Controls.Add(this.btn_worker_info);
            this.panel1.Controls.Add(this.btn_re_print);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.txt_night_sqty);
            this.panel1.Controls.Add(this.txt_day_sqty);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.imageSlider1);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtD_weight);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txt_IT_MODEL);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txt_IT_SCODE);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.ts_carrier);
            this.panel1.Controls.Add(this.btn_po_release_Insert);
            this.panel1.Controls.Add(this.btn_Manually_sqty);
            this.panel1.Controls.Add(this.txtCarrier_no);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.tS_auto);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.txt_datetime);
            this.panel1.Controls.Add(this.txt_weight_cnt);
            this.panel1.Controls.Add(this.txt_fail_qty);
            this.panel1.Controls.Add(this.txt_good_qty);
            this.panel1.Controls.Add(this.lblWeight);
            this.panel1.Controls.Add(this.btn_pp_card_search);
            this.panel1.Controls.Add(this.btn_dt_input);
            this.panel1.Controls.Add(this.btn_fail_input);
            this.panel1.Controls.Add(this.btn_meterial_search);
            this.panel1.Controls.Add(this.btn_po_release);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.lbl_now_sqty);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtDayAndNight);
            this.panel1.Controls.Add(this.txt_IT_SNAME);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.btn_save_cancel);
            this.panel1.Location = new System.Drawing.Point(12, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1269, 683);
            this.panel1.TabIndex = 8;
            // 
            // btn_inspection
            // 
            this.btn_inspection.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_inspection.Appearance.Options.UseFont = true;
            this.btn_inspection.Location = new System.Drawing.Point(913, 0);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(128, 72);
            this.btn_inspection.TabIndex = 44;
            this.btn_inspection.Text = "초중종품\r\n등록";
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // txt_MO_SQUTY
            // 
            this.txt_MO_SQUTY.BackColor = System.Drawing.Color.Gainsboro;
            this.txt_MO_SQUTY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_MO_SQUTY.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_MO_SQUTY.Location = new System.Drawing.Point(1041, 201);
            this.txt_MO_SQUTY.Name = "txt_MO_SQUTY";
            this.txt_MO_SQUTY.Size = new System.Drawing.Size(113, 33);
            this.txt_MO_SQUTY.TabIndex = 11;
            this.txt_MO_SQUTY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_worker_info
            // 
            this.btn_worker_info.Font = new System.Drawing.Font("굴림", 25F);
            this.btn_worker_info.Location = new System.Drawing.Point(1130, 1);
            this.btn_worker_info.Name = "btn_worker_info";
            this.btn_worker_info.Size = new System.Drawing.Size(137, 71);
            this.btn_worker_info.TabIndex = 43;
            this.btn_worker_info.UseVisualStyleBackColor = true;
            this.btn_worker_info.Click += new System.EventHandler(this.btn_worker_info_Click);
            // 
            // btn_re_print
            // 
            this.btn_re_print.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_re_print.BackgroundImage")));
            this.btn_re_print.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_re_print.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_re_print.Location = new System.Drawing.Point(1041, 454);
            this.btn_re_print.Margin = new System.Windows.Forms.Padding(1);
            this.btn_re_print.Name = "btn_re_print";
            this.btn_re_print.Size = new System.Drawing.Size(225, 57);
            this.btn_re_print.TabIndex = 42;
            this.btn_re_print.Text = "식별표 재출력";
            this.btn_re_print.UseVisualStyleBackColor = false;
            this.btn_re_print.Click += new System.EventHandler(this.btn_re_print_Click);
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.LightCyan;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(1144, 267);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(21, 34);
            this.label26.TabIndex = 39;
            this.label26.Text = "/";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_night_sqty
            // 
            this.txt_night_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_night_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_night_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_night_sqty.Location = new System.Drawing.Point(1166, 267);
            this.txt_night_sqty.Name = "txt_night_sqty";
            this.txt_night_sqty.Size = new System.Drawing.Size(102, 34);
            this.txt_night_sqty.TabIndex = 40;
            this.txt_night_sqty.Text = "0";
            this.txt_night_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_day_sqty
            // 
            this.txt_day_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_day_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_day_sqty.Font = new System.Drawing.Font("굴림", 25F, System.Drawing.FontStyle.Bold);
            this.txt_day_sqty.Location = new System.Drawing.Point(1041, 267);
            this.txt_day_sqty.Name = "txt_day_sqty";
            this.txt_day_sqty.Size = new System.Drawing.Size(102, 34);
            this.txt_day_sqty.TabIndex = 41;
            this.txt_day_sqty.Text = "0";
            this.txt_day_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageSlider1
            // 
            this.imageSlider1.AllowLooping = true;
            this.imageSlider1.ContextButtonOptions.AnimationType = DevExpress.Utils.ContextAnimationType.OutAnimation;
            this.imageSlider1.CurrentImageIndex = -1;
            this.imageSlider1.LayoutMode = DevExpress.Utils.Drawing.ImageLayoutMode.Stretch;
            this.imageSlider1.Location = new System.Drawing.Point(2, 72);
            this.imageSlider1.Name = "imageSlider1";
            this.imageSlider1.Size = new System.Drawing.Size(1039, 611);
            this.imageSlider1.TabIndex = 38;
            this.imageSlider1.Text = "imageSlider1";
            this.imageSlider1.UseDisabledStatePainter = true;
            // 
            // txt_IT_MODEL
            // 
            this.txt_IT_MODEL.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_MODEL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_MODEL.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_MODEL.Location = new System.Drawing.Point(123, 36);
            this.txt_IT_MODEL.Name = "txt_IT_MODEL";
            this.txt_IT_MODEL.Size = new System.Drawing.Size(351, 35);
            this.txt_IT_MODEL.TabIndex = 11;
            this.txt_IT_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_IT_SCODE
            // 
            this.txt_IT_SCODE.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SCODE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SCODE.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SCODE.Location = new System.Drawing.Point(596, 1);
            this.txt_IT_SCODE.Name = "txt_IT_SCODE";
            this.txt_IT_SCODE.Size = new System.Drawing.Size(315, 71);
            this.txt_IT_SCODE.TabIndex = 11;
            this.txt_IT_SCODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(711, 2);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 71);
            this.label25.TabIndex = 0;
            this.label25.Text = "대차\r\n유무";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.Visible = false;
            // 
            // ts_carrier
            // 
            this.ts_carrier.Location = new System.Drawing.Point(792, 8);
            this.ts_carrier.Name = "ts_carrier";
            this.ts_carrier.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ts_carrier.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.ts_carrier.Properties.Appearance.Options.UseBackColor = true;
            this.ts_carrier.Properties.Appearance.Options.UseFont = true;
            this.ts_carrier.Properties.AutoWidth = true;
            this.ts_carrier.Properties.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.ts_carrier.Properties.OffText = "Off";
            this.ts_carrier.Properties.OnText = "On";
            this.ts_carrier.Properties.EditValueChanged += new System.EventHandler(this.tS_auto_Properties_EditValueChanged);
            this.ts_carrier.Size = new System.Drawing.Size(246, 59);
            this.ts_carrier.TabIndex = 22;
            this.ts_carrier.Visible = false;
            this.ts_carrier.Toggled += new System.EventHandler(this.ts_carrier_Toggled);
            // 
            // btn_po_release_Insert
            // 
            this.btn_po_release_Insert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release_Insert.BackgroundImage")));
            this.btn_po_release_Insert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release_Insert.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_po_release_Insert.Location = new System.Drawing.Point(1041, 626);
            this.btn_po_release_Insert.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release_Insert.Name = "btn_po_release_Insert";
            this.btn_po_release_Insert.Size = new System.Drawing.Size(112, 56);
            this.btn_po_release_Insert.TabIndex = 37;
            this.btn_po_release_Insert.Text = "긴급작지\r\n생성";
            this.btn_po_release_Insert.UseVisualStyleBackColor = false;
            this.btn_po_release_Insert.Click += new System.EventHandler(this.btn_po_release_Insert_Click);
            // 
            // btn_Manually_sqty
            // 
            this.btn_Manually_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.btn_Manually_sqty.Appearance.Options.UseFont = true;
            this.btn_Manually_sqty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_Manually_sqty.Location = new System.Drawing.Point(1188, 334);
            this.btn_Manually_sqty.Name = "btn_Manually_sqty";
            this.btn_Manually_sqty.Size = new System.Drawing.Size(80, 53);
            this.btn_Manually_sqty.TabIndex = 36;
            this.btn_Manually_sqty.Text = "수동\r\n입력";
            this.btn_Manually_sqty.Click += new System.EventHandler(this.btn_plus_Click);
            // 
            // txtCarrier_no
            // 
            this.txtCarrier_no.Font = new System.Drawing.Font("굴림", 35F);
            this.txtCarrier_no.Location = new System.Drawing.Point(974, 3);
            this.txtCarrier_no.Name = "txtCarrier_no";
            this.txtCarrier_no.Size = new System.Drawing.Size(282, 81);
            this.txtCarrier_no.TabIndex = 24;
            this.txtCarrier_no.UseVisualStyleBackColor = true;
            this.txtCarrier_no.Visible = false;
            this.txtCarrier_no.Click += new System.EventHandler(this.txtCarrier_no_Click);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("굴림", 22F);
            this.label17.Location = new System.Drawing.Point(925, 3);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 82);
            this.label17.TabIndex = 24;
            this.label17.Text = "대\r\n차";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.Visible = false;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("굴림", 16F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(594, 20);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(169, 56);
            this.label6.TabIndex = 0;
            this.label6.Text = "카드 선택 자동";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Visible = false;
            // 
            // tS_auto
            // 
            this.tS_auto.Location = new System.Drawing.Point(635, 17);
            this.tS_auto.Name = "tS_auto";
            this.tS_auto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.tS_auto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.tS_auto.Properties.Appearance.Options.UseBackColor = true;
            this.tS_auto.Properties.Appearance.Options.UseFont = true;
            this.tS_auto.Properties.AutoWidth = true;
            this.tS_auto.Properties.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.tS_auto.Properties.OffText = "Off";
            this.tS_auto.Properties.OnText = "On";
            this.tS_auto.Properties.EditValueChanged += new System.EventHandler(this.tS_auto_Properties_EditValueChanged);
            this.tS_auto.Size = new System.Drawing.Size(246, 59);
            this.tS_auto.TabIndex = 22;
            this.tS_auto.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(591, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 36);
            this.button3.TabIndex = 21;
            this.button3.Text = "저울 프린트TEST";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(592, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txt_datetime
            // 
            this.txt_datetime.BackColor = System.Drawing.SystemColors.Info;
            this.txt_datetime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_datetime.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_datetime.Location = new System.Drawing.Point(123, 1);
            this.txt_datetime.Name = "txt_datetime";
            this.txt_datetime.Size = new System.Drawing.Size(351, 35);
            this.txt_datetime.TabIndex = 11;
            this.txt_datetime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_weight_cnt
            // 
            this.txt_weight_cnt.BackColor = System.Drawing.Color.LightCyan;
            this.txt_weight_cnt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_weight_cnt.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_weight_cnt.Location = new System.Drawing.Point(1041, 334);
            this.txt_weight_cnt.Name = "txt_weight_cnt";
            this.txt_weight_cnt.Size = new System.Drawing.Size(146, 53);
            this.txt_weight_cnt.TabIndex = 11;
            this.txt_weight_cnt.Text = "0";
            this.txt_weight_cnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_fail_qty
            // 
            this.txt_fail_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_fail_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_fail_qty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_fail_qty.Location = new System.Drawing.Point(1041, 421);
            this.txt_fail_qty.Name = "txt_fail_qty";
            this.txt_fail_qty.Size = new System.Drawing.Size(226, 32);
            this.txt_fail_qty.TabIndex = 11;
            this.txt_fail_qty.Text = "0";
            this.txt_fail_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txt_good_qty
            // 
            this.txt_good_qty.BackColor = System.Drawing.Color.LightCyan;
            this.txt_good_qty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_good_qty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_good_qty.Location = new System.Drawing.Point(1154, 201);
            this.txt_good_qty.Name = "txt_good_qty";
            this.txt_good_qty.Size = new System.Drawing.Size(113, 33);
            this.txt_good_qty.TabIndex = 11;
            this.txt_good_qty.Text = "0";
            this.txt_good_qty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWeight
            // 
            this.lblWeight.BackColor = System.Drawing.SystemColors.Info;
            this.lblWeight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblWeight.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.lblWeight.Location = new System.Drawing.Point(1041, 130);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(226, 37);
            this.lblWeight.TabIndex = 11;
            this.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_pp_card_search
            // 
            this.btn_pp_card_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_pp_card_search.BackgroundImage")));
            this.btn_pp_card_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_pp_card_search.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_pp_card_search.Location = new System.Drawing.Point(1154, 512);
            this.btn_pp_card_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_pp_card_search.Name = "btn_pp_card_search";
            this.btn_pp_card_search.Size = new System.Drawing.Size(112, 56);
            this.btn_pp_card_search.TabIndex = 0;
            this.btn_pp_card_search.Text = "PP Card\r\n등록";
            this.btn_pp_card_search.UseVisualStyleBackColor = false;
            this.btn_pp_card_search.Click += new System.EventHandler(this.btn_pp_card_search_Click);
            // 
            // btn_dt_input
            // 
            this.btn_dt_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_dt_input.BackgroundImage")));
            this.btn_dt_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_dt_input.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_dt_input.Location = new System.Drawing.Point(1041, 512);
            this.btn_dt_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_dt_input.Name = "btn_dt_input";
            this.btn_dt_input.Size = new System.Drawing.Size(112, 56);
            this.btn_dt_input.TabIndex = 0;
            this.btn_dt_input.Text = "비가동\r\n등록";
            this.btn_dt_input.UseVisualStyleBackColor = false;
            this.btn_dt_input.Click += new System.EventHandler(this.btn_dt_input_Click);
            // 
            // btn_fail_input
            // 
            this.btn_fail_input.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_fail_input.BackgroundImage")));
            this.btn_fail_input.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_fail_input.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_fail_input.Location = new System.Drawing.Point(1154, 569);
            this.btn_fail_input.Margin = new System.Windows.Forms.Padding(1);
            this.btn_fail_input.Name = "btn_fail_input";
            this.btn_fail_input.Size = new System.Drawing.Size(112, 56);
            this.btn_fail_input.TabIndex = 0;
            this.btn_fail_input.Text = "불량등록";
            this.btn_fail_input.UseVisualStyleBackColor = false;
            this.btn_fail_input.Click += new System.EventHandler(this.btn_fail_input_Click);
            // 
            // btn_meterial_search
            // 
            this.btn_meterial_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_meterial_search.BackgroundImage")));
            this.btn_meterial_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_meterial_search.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_meterial_search.Location = new System.Drawing.Point(1041, 569);
            this.btn_meterial_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_meterial_search.Name = "btn_meterial_search";
            this.btn_meterial_search.Size = new System.Drawing.Size(112, 56);
            this.btn_meterial_search.TabIndex = 0;
            this.btn_meterial_search.Text = "재공자재\r\n재고";
            this.btn_meterial_search.UseVisualStyleBackColor = false;
            this.btn_meterial_search.Click += new System.EventHandler(this.btn_meterial_search_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_po_release.BackgroundImage")));
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(1154, 626);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(112, 56);
            this.btn_po_release.TabIndex = 0;
            this.btn_po_release.Text = "작업지시\r\n선택";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(1041, 234);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(226, 33);
            this.label20.TabIndex = 0;
            this.label20.Text = "주/야";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(1041, 168);
            this.label18.Margin = new System.Windows.Forms.Padding(0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 33);
            this.label18.TabIndex = 0;
            this.label18.Text = "계획수량";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl_now_sqty
            // 
            this.lbl_now_sqty.BackColor = System.Drawing.Color.LightCyan;
            this.lbl_now_sqty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_now_sqty.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.lbl_now_sqty.Location = new System.Drawing.Point(208, 307);
            this.lbl_now_sqty.Name = "lbl_now_sqty";
            this.lbl_now_sqty.Size = new System.Drawing.Size(208, 54);
            this.lbl_now_sqty.TabIndex = 20;
            this.lbl_now_sqty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_now_sqty.Visible = false;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(13, 307);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(192, 54);
            this.label14.TabIndex = 18;
            this.label14.Text = "현재고";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label14.Visible = false;
            // 
            // txtDayAndNight
            // 
            this.txtDayAndNight.BackColor = System.Drawing.SystemColors.Info;
            this.txtDayAndNight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtDayAndNight.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txtDayAndNight.Location = new System.Drawing.Point(631, 86);
            this.txtDayAndNight.Name = "txtDayAndNight";
            this.txtDayAndNight.Size = new System.Drawing.Size(204, 82);
            this.txtDayAndNight.TabIndex = 11;
            this.txtDayAndNight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtDayAndNight.Visible = false;
            // 
            // txt_IT_SNAME
            // 
            this.txt_IT_SNAME.BackColor = System.Drawing.SystemColors.Info;
            this.txt_IT_SNAME.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txt_IT_SNAME.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.txt_IT_SNAME.Location = new System.Drawing.Point(631, 169);
            this.txt_IT_SNAME.Name = "txt_IT_SNAME";
            this.txt_IT_SNAME.Size = new System.Drawing.Size(334, 87);
            this.txt_IT_SNAME.TabIndex = 11;
            this.txt_IT_SNAME.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txt_IT_SNAME.Visible = false;
            // 
            // btn_save_cancel
            // 
            this.btn_save_cancel.BackgroundImage = global::DK_Tablet.Properties.Resources.버튼image;
            this.btn_save_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_save_cancel.Enabled = false;
            this.btn_save_cancel.Font = new System.Drawing.Font("굴림", 22F);
            this.btn_save_cancel.Location = new System.Drawing.Point(1106, 604);
            this.btn_save_cancel.Name = "btn_save_cancel";
            this.btn_save_cancel.Size = new System.Drawing.Size(164, 59);
            this.btn_save_cancel.TabIndex = 15;
            this.btn_save_cancel.Text = "등록취소";
            this.btn_save_cancel.UseVisualStyleBackColor = true;
            this.btn_save_cancel.Visible = false;
            this.btn_save_cancel.Click += new System.EventHandler(this.btn_save_cancel_Click);
            // 
            // btn_on_off
            // 
            this.btn_on_off.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_on_off.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_on_off.Appearance.Options.UseBackColor = true;
            this.btn_on_off.Appearance.Options.UseFont = true;
            this.btn_on_off.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_on_off.Location = new System.Drawing.Point(955, 3);
            this.btn_on_off.Name = "btn_on_off";
            this.btn_on_off.Size = new System.Drawing.Size(78, 68);
            this.btn_on_off.TabIndex = 30;
            this.btn_on_off.Text = "On";
            this.btn_on_off.Click += new System.EventHandler(this.btn_on_off_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("맑은 고딕", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Location = new System.Drawing.Point(881, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(74, 68);
            this.labelControl1.TabIndex = 32;
            this.labelControl1.Text = "자동";
            // 
            // lab_carrier_sqty
            // 
            this.lab_carrier_sqty.Appearance.BackColor = System.Drawing.Color.Cyan;
            this.lab_carrier_sqty.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.lab_carrier_sqty.Appearance.Font = new System.Drawing.Font("맑은 고딕", 17F, System.Drawing.FontStyle.Bold);
            this.lab_carrier_sqty.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.lab_carrier_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lab_carrier_sqty.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lab_carrier_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lab_carrier_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lab_carrier_sqty.Location = new System.Drawing.Point(674, 3);
            this.lab_carrier_sqty.Name = "lab_carrier_sqty";
            this.lab_carrier_sqty.Size = new System.Drawing.Size(74, 68);
            this.lab_carrier_sqty.TabIndex = 35;
            this.lab_carrier_sqty.Text = "대 차\r\n수 량";
            // 
            // btn_carrier_sqty
            // 
            this.btn_carrier_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F);
            this.btn_carrier_sqty.Appearance.Options.UseFont = true;
            this.btn_carrier_sqty.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_carrier_sqty.Location = new System.Drawing.Point(748, 3);
            this.btn_carrier_sqty.Name = "btn_carrier_sqty";
            this.btn_carrier_sqty.Size = new System.Drawing.Size(81, 68);
            this.btn_carrier_sqty.TabIndex = 29;
            this.btn_carrier_sqty.Text = "0";
            this.btn_carrier_sqty.Click += new System.EventHandler(this.btn_carrier_sqty_Click);
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.LightCyan;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(1390, 465);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 27);
            this.label23.TabIndex = 19;
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label23.Visible = false;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.LightCyan;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(1390, 342);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 87);
            this.label24.TabIndex = 20;
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Visible = false;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.LightCyan;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(1390, 432);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 29);
            this.label22.TabIndex = 20;
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Visible = false;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(1260, 464);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 28);
            this.label7.TabIndex = 17;
            this.label7.Text = "생산시작재고";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // label21
            // 
            this.label21.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(1260, 342);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(127, 86);
            this.label21.TabIndex = 18;
            this.label21.Text = "다음생산예상제품";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.Visible = false;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(1260, 432);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 28);
            this.label8.TabIndex = 18;
            this.label8.Text = "현재고";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Visible = false;
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(15, 3);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(322, 70);
            this.lueWc_code.TabIndex = 14;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // Btn_Auto_Connection
            // 
            this.Btn_Auto_Connection.Font = new System.Drawing.Font("굴림", 22F);
            this.Btn_Auto_Connection.Location = new System.Drawing.Point(339, 3);
            this.Btn_Auto_Connection.Name = "Btn_Auto_Connection";
            this.Btn_Auto_Connection.Size = new System.Drawing.Size(81, 70);
            this.Btn_Auto_Connection.TabIndex = 2;
            this.Btn_Auto_Connection.Text = "자동연결";
            this.Btn_Auto_Connection.UseVisualStyleBackColor = true;
            this.Btn_Auto_Connection.Click += new System.EventHandler(this.Btn_Auto_Connection_Click);
            // 
            // comboBox_inventory_mode
            // 
            this.comboBox_inventory_mode.FormattingEnabled = true;
            this.comboBox_inventory_mode.Location = new System.Drawing.Point(625, 43);
            this.comboBox_inventory_mode.Name = "comboBox_inventory_mode";
            this.comboBox_inventory_mode.Size = new System.Drawing.Size(121, 20);
            this.comboBox_inventory_mode.TabIndex = 10;
            this.comboBox_inventory_mode.Visible = false;
            // 
            // button_inventory_start
            // 
            this.button_inventory_start.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_inventory_start.Location = new System.Drawing.Point(343, 23);
            this.button_inventory_start.Name = "button_inventory_start";
            this.button_inventory_start.Size = new System.Drawing.Size(75, 40);
            this.button_inventory_start.TabIndex = 11;
            this.button_inventory_start.Text = "Start";
            this.button_inventory_start.UseVisualStyleBackColor = true;
            this.button_inventory_start.Visible = false;
            this.button_inventory_start.Click += new System.EventHandler(this.button_inventory_start_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // combPort
            // 
            this.combPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combPort.DropDownWidth = 500;
            this.combPort.Font = new System.Drawing.Font("굴림", 19F);
            this.combPort.FormattingEnabled = true;
            this.combPort.Location = new System.Drawing.Point(497, 39);
            this.combPort.Name = "combPort";
            this.combPort.Size = new System.Drawing.Size(71, 33);
            this.combPort.TabIndex = 1;
            // 
            // btn_port_open
            // 
            this.btn_port_open.Location = new System.Drawing.Point(570, 39);
            this.btn_port_open.Name = "btn_port_open";
            this.btn_port_open.Size = new System.Drawing.Size(52, 32);
            this.btn_port_open.TabIndex = 2;
            this.btn_port_open.Text = "연결";
            this.btn_port_open.UseVisualStyleBackColor = true;
            this.btn_port_open.Click += new System.EventHandler(this.btn_port_open_Click);
            // 
            // btn_port_close
            // 
            this.btn_port_close.Location = new System.Drawing.Point(621, 39);
            this.btn_port_close.Name = "btn_port_close";
            this.btn_port_close.Size = new System.Drawing.Size(52, 32);
            this.btn_port_close.TabIndex = 2;
            this.btn_port_close.Text = "해제";
            this.btn_port_close.UseVisualStyleBackColor = true;
            this.btn_port_close.Click += new System.EventHandler(this.btn_port_close_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(421, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "리더기";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(421, 40);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 32);
            this.label19.TabIndex = 0;
            this.label19.Text = "저울";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listView_target_list
            // 
            this.listView_target_list.Location = new System.Drawing.Point(1159, 46);
            this.listView_target_list.Name = "listView_target_list";
            this.listView_target_list.Size = new System.Drawing.Size(108, 20);
            this.listView_target_list.TabIndex = 0;
            this.listView_target_list.UseCompatibleStateImageBehavior = false;
            this.listView_target_list.Visible = false;
            // 
            // ddc_ivt
            // 
            this.ddc_ivt.BackColor = System.Drawing.Color.Transparent;
            this.ddc_ivt.DigitColor = System.Drawing.Color.DarkGreen;
            this.ddc_ivt.DigitText = "00000";
            this.ddc_ivt.Location = new System.Drawing.Point(1108, 43);
            this.ddc_ivt.Name = "ddc_ivt";
            this.ddc_ivt.Size = new System.Drawing.Size(175, 23);
            this.ddc_ivt.TabIndex = 9;
            this.ddc_ivt.Visible = false;
            // 
            // dsmListView_ivt
            // 
            this.dsmListView_ivt.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader90,
            this.columnHeader91,
            this.columnHeader92,
            this.columnHeader93,
            this.columnHeader94,
            this.columnHeader95,
            this.columnHeader96});
            this.dsmListView_ivt.Location = new System.Drawing.Point(86, 3);
            this.dsmListView_ivt.Name = "dsmListView_ivt";
            this.dsmListView_ivt.Size = new System.Drawing.Size(50, 43);
            this.dsmListView_ivt.TabIndex = 12;
            this.dsmListView_ivt.UseCompatibleStateImageBehavior = false;
            this.dsmListView_ivt.View = System.Windows.Forms.View.Details;
            this.dsmListView_ivt.Visible = false;
            // 
            // N_timer
            // 
            this.N_timer.Appearance.BackColor = System.Drawing.Color.AliceBlue;
            this.N_timer.Appearance.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.N_timer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.N_timer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.N_timer.Location = new System.Drawing.Point(461, 10);
            this.N_timer.Name = "N_timer";
            this.N_timer.Size = new System.Drawing.Size(389, 27);
            this.N_timer.TabIndex = 21;
            this.N_timer.Text = "labelControl1";
            // 
            // btn_fail_search
            // 
            this.btn_fail_search.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_fail_search.Appearance.Options.UseFont = true;
            this.btn_fail_search.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btn_fail_search.Location = new System.Drawing.Point(972, 3);
            this.btn_fail_search.Name = "btn_fail_search";
            this.btn_fail_search.Size = new System.Drawing.Size(82, 68);
            this.btn_fail_search.TabIndex = 28;
            this.btn_fail_search.Text = "월별\r\n불량현황";
            this.btn_fail_search.Click += new System.EventHandler(this.btn_fail_search_Click);
            // 
            // timer_now
            // 
            this.timer_now.Enabled = true;
            this.timer_now.Interval = 10000;
            this.timer_now.Tick += new System.EventHandler(this.timer_now_Tick_1);
            // 
            // btn_print_type
            // 
            this.btn_print_type.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_print_type.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.btn_print_type.Appearance.Options.UseBackColor = true;
            this.btn_print_type.Appearance.Options.UseFont = true;
            this.btn_print_type.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_print_type.Location = new System.Drawing.Point(829, 3);
            this.btn_print_type.Name = "btn_print_type";
            this.btn_print_type.Size = new System.Drawing.Size(143, 68);
            this.btn_print_type.TabIndex = 45;
            this.btn_print_type.Tag = "B";
            this.btn_print_type.Text = "식별표";
            this.btn_print_type.Click += new System.EventHandler(this.btn_print_type_Click);
            // 
            // btn_program_finish
            // 
            this.btn_program_finish.Appearance.BackColor = System.Drawing.Color.Crimson;
            this.btn_program_finish.Appearance.BackColor2 = System.Drawing.Color.PaleVioletRed;
            this.btn_program_finish.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_program_finish.Appearance.Options.UseBackColor = true;
            this.btn_program_finish.Appearance.Options.UseFont = true;
            this.btn_program_finish.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_program_finish.Location = new System.Drawing.Point(1168, 3);
            this.btn_program_finish.Name = "btn_program_finish";
            this.btn_program_finish.Size = new System.Drawing.Size(111, 70);
            this.btn_program_finish.TabIndex = 98;
            this.btn_program_finish.Text = "작업종료";
            this.btn_program_finish.Click += new System.EventHandler(this.btn_program_finish_Click);
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_close.Appearance.BackColor2 = System.Drawing.Color.YellowGreen;
            this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Location = new System.Drawing.Point(1054, 3);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(113, 70);
            this.btn_close.TabIndex = 99;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btn_mold_search
            // 
            this.btn_mold_search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_mold_search.BackgroundImage")));
            this.btn_mold_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_mold_search.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold);
            this.btn_mold_search.Location = new System.Drawing.Point(1236, 551);
            this.btn_mold_search.Margin = new System.Windows.Forms.Padding(1);
            this.btn_mold_search.Name = "btn_mold_search";
            this.btn_mold_search.Size = new System.Drawing.Size(106, 102);
            this.btn_mold_search.TabIndex = 0;
            this.btn_mold_search.Text = "금형현황";
            this.btn_mold_search.UseVisualStyleBackColor = false;
            this.btn_mold_search.Visible = false;
            this.btn_mold_search.Click += new System.EventHandler(this.btn_mold_search_Click);
            // 
            // Injection_carrier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1295, 760);
            this.Controls.Add(this.btn_program_finish);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_fail_search);
            this.Controls.Add(this.btn_print_type);
            this.Controls.Add(this.Btn_Auto_Connection);
            this.Controls.Add(this.dsmListView_ivt);
            this.Controls.Add(this.button_com_close);
            this.Controls.Add(this.btn_on_off);
            this.Controls.Add(this.button_com_open);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboBox_ports);
            this.Controls.Add(this.lab_carrier_sqty);
            this.Controls.Add(this.btn_carrier_sqty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_port_close);
            this.Controls.Add(this.btn_port_open);
            this.Controls.Add(this.combPort);
            this.Controls.Add(this.listView_target_list);
            this.Controls.Add(this.ddc_ivt);
            this.Controls.Add(this.comboBox_inventory_mode);
            this.Controls.Add(this.N_timer);
            this.Controls.Add(this.button_inventory_start);
            this.Controls.Add(this.lueWc_code);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.btn_mold_search);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Injection_carrier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Injection";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Injection_Shown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageSlider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ts_carrier.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tS_auto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button button_com_open;
        private System.Windows.Forms.Button button_com_close;
        private DK_Tablet.dsmListView listView_target_list;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btn_pp_card_search;
        private System.Windows.Forms.Button btn_dt_input;
        private System.Windows.Forms.Button btn_fail_input;
        private System.Windows.Forms.Button btn_meterial_search;
        private System.Windows.Forms.Button btn_mold_search;
        private System.Windows.Forms.Button btn_po_release;
        private System.Windows.Forms.Label txtD_weight;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private Owf.Controls.DigitalDisplayControl ddc_ivt;
        private System.Windows.Forms.ComboBox comboBox_inventory_mode;
        private System.Windows.Forms.Label txt_IT_SCODE;
        private System.Windows.Forms.Label txt_datetime;
        private System.Windows.Forms.Label txt_MO_SQUTY;
        private System.Windows.Forms.Label txt_IT_SNAME;
        private System.Windows.Forms.Label txt_IT_MODEL;
        private System.Windows.Forms.Label txtDayAndNight;
        private System.Windows.Forms.Label txt_fail_qty;
        private System.Windows.Forms.Label txt_good_qty;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Button button_inventory_start;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox combPort;
        private System.Windows.Forms.Button btn_port_open;
        private System.Windows.Forms.Button btn_port_close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private System.Windows.Forms.Label txt_weight_cnt;
        private dsmListView dsmListView_ivt;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button Btn_Auto_Connection;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LabelControl N_timer;
        private System.Windows.Forms.Button button3;
        private DevExpress.XtraEditors.ToggleSwitch tS_auto;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_save_cancel;
        private System.Windows.Forms.Label lbl_now_sqty;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private DevExpress.XtraEditors.ToggleSwitch ts_carrier;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button txtCarrier_no;
        private DevExpress.XtraEditors.SimpleButton btn_fail_search;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btn_carrier_sqty;
        private DevExpress.XtraEditors.SimpleButton btn_on_off;
        private DevExpress.XtraEditors.LabelControl lab_carrier_sqty;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private DevExpress.XtraEditors.SimpleButton btn_Manually_sqty;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.Button btn_po_release_Insert;
        private DevExpress.XtraEditors.Controls.ImageSlider imageSlider1;
        private System.Windows.Forms.Timer timer_now;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label txt_night_sqty;
        private System.Windows.Forms.Label txt_day_sqty;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.ColumnHeader columnHeader61;
        private System.Windows.Forms.Button btn_re_print;
        private System.Windows.Forms.Button btn_worker_info;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ColumnHeader columnHeader62;
        private System.Windows.Forms.ColumnHeader columnHeader63;
        private System.Windows.Forms.ColumnHeader columnHeader64;
        private System.Windows.Forms.ColumnHeader columnHeader65;
        private System.Windows.Forms.ColumnHeader columnHeader66;
        private System.Windows.Forms.ColumnHeader columnHeader67;
        private System.Windows.Forms.ColumnHeader columnHeader68;
        private System.Windows.Forms.ColumnHeader columnHeader69;
        private System.Windows.Forms.ColumnHeader columnHeader70;
        private System.Windows.Forms.ColumnHeader columnHeader71;
        private System.Windows.Forms.ColumnHeader columnHeader72;
        private System.Windows.Forms.ColumnHeader columnHeader73;
        private System.Windows.Forms.ColumnHeader columnHeader74;
        private System.Windows.Forms.ColumnHeader columnHeader75;
        private DevExpress.XtraEditors.SimpleButton btn_inspection;
        private DevExpress.XtraEditors.SimpleButton btn_print_type;
        private System.Windows.Forms.ColumnHeader columnHeader76;
        private System.Windows.Forms.ColumnHeader columnHeader77;
        private System.Windows.Forms.ColumnHeader columnHeader78;
        private System.Windows.Forms.ColumnHeader columnHeader79;
        private System.Windows.Forms.ColumnHeader columnHeader80;
        private System.Windows.Forms.ColumnHeader columnHeader81;
        private System.Windows.Forms.ColumnHeader columnHeader82;
        private DevExpress.XtraEditors.SimpleButton btn_program_finish;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private System.Windows.Forms.ColumnHeader columnHeader83;
        private System.Windows.Forms.ColumnHeader columnHeader84;
        private System.Windows.Forms.ColumnHeader columnHeader85;
        private System.Windows.Forms.ColumnHeader columnHeader86;
        private System.Windows.Forms.ColumnHeader columnHeader87;
        private System.Windows.Forms.ColumnHeader columnHeader88;
        private System.Windows.Forms.ColumnHeader columnHeader89;
        private System.Windows.Forms.ColumnHeader columnHeader90;
        private System.Windows.Forms.ColumnHeader columnHeader91;
        private System.Windows.Forms.ColumnHeader columnHeader92;
        private System.Windows.Forms.ColumnHeader columnHeader93;
        private System.Windows.Forms.ColumnHeader columnHeader94;
        private System.Windows.Forms.ColumnHeader columnHeader95;
        private System.Windows.Forms.ColumnHeader columnHeader96;
    }
}

