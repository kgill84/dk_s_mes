﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DK_Tablet;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using DK_Tablet.FUNCTION;
using DK_Tablet.DisplayForm;
using ThoughtWorks.QRCode.Codec;
using DK_Tablet.Popup;
using Microsoft.Win32;
using System.Collections;
using DevExpress.XtraReports.UI;
using DK_Tablet.PRINT;
using DevExpress.XtraEditors;


namespace DK_Tablet
{
    public partial class SubMain_Y : Form
    {
        [DllImport("user32.dll")]
        
        public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);//리더기와 pc간 통신하기 위한 함수
        
        //private SwingLibrary.SwingAPI Swing = null;//스윙-u 사용하기 위한 API 객체 생성
        //private string[] str_bank = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        CHECK_FUNC CHECK_FUNC = new CHECK_FUNC();//데이터 체크 객체 생성
        GET_DATA GET_DATA = new GET_DATA();//데이터 불러오는 객체 생성
        MOVE_FUNC MOVE_FUNC = new MOVE_FUNC();//이동시 저장하는 객체 생성
        SUB_SAVE SUB_SAVE = new SUB_SAVE();//저장 함수 객체 생성
        DataTable select_worker_dt = new DataTable();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        private SwingLibrary.SwingAPI Swing2 = null;//스윙-u 사용하기 위한 API 객체 생성
        Func_Mobis_Print Func_Mobis_Print = new Func_Mobis_Print();
        private string[] str_bank2 = { "RESERVED", "EPC", "TID", "USER" };//리더기 셋팅에 필요한 집합
        public bool messageCheck = false;//메세지 체크 
        public string wc_group { get; set; }//작업그룹
        public string str_wc_code = "";//작업장
        public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        //입구 리더기
        string in_carrier_no = "";//입구로 in 될때 읽힌 대차 번호
        //string rec_lot_no = "";
        string rec_card_no = "";//다음 pp 카드
        string rec_it_scode = "";//다음 품목코드
        string sel_carrier_no = "";//다음대차번호
        //string sel_lot_no = "";
        string sel_card_no = "";//들어온 카드 번호
        string sel_it_scode = "";//들어온 품목코드
        string carrier_yn = "";//공대차인지 아닌지 구분자
        float f_mm_sqty = 0;//대차에 적재 되어있는 제품 수량
        string me_scode = "";//자재 유형
        string max_sqty = "0";//최대보관수량
        string sw_code = "";//근무형태 코드
        string str_worker = "";//작업자
        private Success_Form Success_Form = new Success_Form();
        //출구 리더기
        PpCard_reading PpCard_reading = new PpCard_reading();
        string out_carrier_no = "";// 출구로 나가는 대차 번호
        DataTable Tag_DT_PP = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관
        DataTable Tag_DT_DC = new DataTable();//출구에서 리더기로 읽어 들인 대차 카드 보관
        DataTable po_DT = new DataTable();//생산계획을 여러개 선택 할 수 있도록 선택한 생산계획 정보 보관
        PpCard_Success10 PpCard_Success10 = new PpCard_Success10();
        DataTable Tag_DT_PP_MIX = new DataTable();//출구에서 리더기로 읽어 들인 pp 카드 보관
        PpCard_Success PpCard_Success = new PpCard_Success();
        string PP_WC_CODE_str = "";//작업장
        string PP_IT_SCODE_str = "";//품목코드
        string CARD_NO_str = "";//카드번호
        string PP_SIZE_str = "";//수용수
        string PP_SITE_CODE_str = "";//공장코드
        MAIN parentForm;
        public int po_start_time { get; set; }
        public int pp_start_time { get; set; }
        string night_time_start = "";
        string day_time_start = "";
        FtpUtil ftpUtil;
        string path = "ftp://203.251.168.131:4104";
            string ftpID = "administrator";
        string ftpPass = "dk_scm0595";
        Image[] work_image = new Image[10];
        private BackgroundWorker worker = new BackgroundWorker();

        DataTable DataGridViewDT = new DataTable();

        private class Line
        {
            public string Str;
            public Color ForeColor;

            public Line(string str, Color color)
            {
                Str = str;
                ForeColor = color;
            }
        };
        ContextMenu popUpMenu;
        private void outputList_Initialize()
        {
            // owner draw for listbox so we can add color
            outputList.DrawMode = DrawMode.OwnerDrawFixed;
            outputList.DrawItem += new DrawItemEventHandler(outputList_DrawItem);
            outputList.ClearSelected();

            // build the outputList context menu
            popUpMenu = new ContextMenu();
            popUpMenu.MenuItems.Add("&Copy", new EventHandler(outputList_Copy));
            popUpMenu.MenuItems[0].Visible = true;
            popUpMenu.MenuItems[0].Enabled = false;
            popUpMenu.MenuItems[0].Shortcut = Shortcut.CtrlC;
            popUpMenu.MenuItems[0].ShowShortcut = true;
            popUpMenu.MenuItems.Add("Copy All", new EventHandler(outputList_CopyAll));
            popUpMenu.MenuItems[1].Visible = true;
            popUpMenu.MenuItems.Add("Select &All", new EventHandler(outputList_SelectAll));
            popUpMenu.MenuItems[2].Visible = true;
            popUpMenu.MenuItems[2].Shortcut = Shortcut.CtrlA;
            popUpMenu.MenuItems[2].ShowShortcut = true;
            popUpMenu.MenuItems.Add("Clear Selected", new EventHandler(outputList_ClearSelected));
            popUpMenu.MenuItems[3].Visible = true;
            outputList.ContextMenu = popUpMenu;
        }
        /// <summary>
        /// clear selected in output window
        /// </summary>
        private void outputList_ClearSelected(object sender, EventArgs e)
        {
            outputList.ClearSelected();
            outputList.SelectedItem = -1;
        }
        /// <summary>
        /// select all lines in output window
        /// </summary>
        private void outputList_SelectAll(object sender, EventArgs e)
        {
            outputList.BeginUpdate();
            for (int i = 0; i < outputList.Items.Count; ++i)
            {
                outputList.SetSelected(i, true);
            }
            outputList.EndUpdate();
        }
        /// <summary>
        /// copy all lines in output window
        /// </summary>
        private void outputList_CopyAll(object sender, EventArgs e)
        {
            int iCount = outputList.Items.Count;
            if (iCount > 0)
            {
                String[] source = new String[iCount];
                for (int i = 0; i < iCount; ++i)
                {
                    source[i] = ((Line)outputList.Items[i]).Str;
                }

                String dest = String.Join("\r\n", source);
                Clipboard.SetText(dest);
            }
        }
        /// <summary>
        /// copy selection in output window to clipboard
        /// </summary>
        private void outputList_Copy(object sender, EventArgs e)
        {
            int iCount = outputList.SelectedItems.Count;
            if (iCount > 0)
            {
                String[] source = new String[iCount];
                for (int i = 0; i < iCount; ++i)
                {
                    source[i] = ((Line)outputList.SelectedItems[i]).Str;
                }

                String dest = String.Join("\r\n", source);
                Clipboard.SetText(dest);
            }
        }
        /// <summary>
        /// draw item with color in output window
        /// </summary>
        void outputList_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            if (e.Index >= 0 && e.Index < outputList.Items.Count)
            {
                Line line = (Line)outputList.Items[e.Index];

                // if selected, make the text color readable
                Color color = line.ForeColor;
                if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                {
                    color = Color.Black;	// make it readable
                }

                e.Graphics.DrawString(line.Str, e.Font, new SolidBrush(color),
                    e.Bounds, StringFormat.GenericDefault);
            }
            e.DrawFocusRectangle();
        }
        ArrayList lines = new ArrayList();
        Font origFont;
        public SubMain_Y(MAIN form)
        {
            ftpUtil = new FtpUtil(path, ftpID, ftpPass);
            this.parentForm = form;
            //저울 
            CheckForIllegalCrossThreadCalls = false;

            //SWING-U
            InitializeComponent();
            //융착 포트
            outputList_Initialize();

            Utils.GetComList(comboBox_ports);
            if (comboBox_ports.Items.Count > 0)
            {
                button_com_open.Enabled = true;
                button_com_close.Enabled = false;

                for (int i = 0; i < comboBox_ports.Items.Count; i++)
                {
                    if (comboBox_ports.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName))
                    {
                        comboBox_ports.SelectedIndex = i;
                        break;
                    }
                }
            }
            /*
            Swing = new SwingLibrary.SwingAPI();
            Swing.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus);
            Swing.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory);
            Swing.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent);
            Swing.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged);
            //Swing.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);
            Swing.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound);            
            Swing.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD);
            Swing.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent);*/
            
            comboBox_inventory_mode.DataSource = Enum.GetValues(typeof(SwingLibrary.SwingAPI.InventoryMode));
            Utils.GetComList(comboBox_ports2);
            if (comboBox_ports2.Items.Count > 0)
            {
                button_com_open2.Enabled = true;
                button_com_close2.Enabled = false;

                for (int i = 0; i < comboBox_ports2.Items.Count; i++)
                {
                    if (comboBox_ports2.Items[i].ToString().Equals(Properties.Settings.Default.ComPortName2))
                    {
                        comboBox_ports2.SelectedIndex = i;
                        break;
                    }
                }
            }

            Swing2 = new SwingLibrary.SwingAPI();
            Swing2.NotifyStatusCheck += new SwingLibrary.StatusDelegate(Swing_NotifyStatus2);
            Swing2.NotifyInventory += new SwingLibrary.DataDelegate(Swing_NotifyInventory2);
            Swing2.NotifyButtonEvent += new SwingLibrary.ButtonDelegate(Swing_NotifyButtonEvent2);
            Swing2.NotifyParameterChanged += new SwingLibrary.ParameterDelegate(Swing_NotifyParameterChanged2);
            /*Swing2.NotifyError += new SwingLibrary.DataDelegate(Swing_NotifyError);*/
            Swing2.NotifyTagFound += new SwingLibrary.DataDelegate(Swing_NotifyTagFound2);
            Swing2.NotifyInventoryBCD += new SwingLibrary.DataDelegate(Swing_NotifyInventoryBCD2);
            Swing2.NotifyReadEvent += new SwingLibrary.ReadDelegate(Swing_NotifyReadEvent2);

            Settings_1.Read();
            TopMost = Settings_1.Option.StayOnTop;

            // let form use multiple fonts
            origFont = Font;
            //FontFamily ff = new FontFamily("Courier New");
            //monoFont = new Font(ff, 8, FontStyle.Regular);
            //Font = Settings_1.Option.MonoFont ? monoFont : origFont;

            CommPort com = CommPort.Instance;
            com.StatusChanged += OnStatusChanged;
            com.DataReceived += OnDataReceived;
            com.Open();
        }
        internal delegate void StringDelegate(string data);
        string filterString = "";
        bool scrolling = true;
        Color receivedColor = Color.Green;
        Color sentColor = Color.Blue;
        string STR_DATA = "";
        public void OnStatusChanged(string status)
        {
            //Handle multi-threading
            if (InvokeRequired)
            {
                Invoke(new StringDelegate(OnStatusChanged), new object[] { status });
                return;
            }

            textBox1.Text = status;
        }

        public void logFile_writeLine(string stringOut)
        {
            if (Settings_1.Option.LogFileName != "")
            {
                Stream myStream = File.Open(Settings_1.Option.LogFileName,
                    FileMode.Append, FileAccess.Write, FileShare.Read);
                if (myStream != null)
                {
                    StreamWriter myWriter = new StreamWriter(myStream, Encoding.UTF8);
                    myWriter.WriteLine(stringOut);
                    myWriter.Close();
                }
            }


        }
        /// <summary>
        /// Prepare a string for output by converting non-printable characters.
        /// </summary>
        /// <param name="StringIn">input string to prepare.</param>
        /// <returns>output string.</returns>
        private String PrepareData(String StringIn)
        {
            // The names of the first 32 characters
            string[] charNames = { "NUL", "SOH", "STX", "ETX", "EOT",
				"ENQ", "ACK", "BEL", "BS", "TAB", "LF", "VT", "FF", "CR", "SO", "SI",
				"DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB", "CAN", "EM", "SUB",
				"ESC", "FS", "GS", "RS", "US", "Space"};

            string StringOut = "";

            foreach (char c in StringIn)
            {
                if (Settings_1.Option.HexOutput)
                {
                    StringOut = StringOut + String.Format("{0:X2} ", (int)c);
                }
                else if (c < 32 && c != 9)
                {
                    StringOut = StringOut + "<" + charNames[c] + ">";

                    //Uglier "Termite" style
                    //StringOut = StringOut + String.Format("[{0:X2}]", (int)c);
                }
                else
                {
                    StringOut = StringOut + c;
                }
            }
            return StringOut;
        }
        private Line partialLine = null;

        /// <summary>
        /// Add data to the output.
        /// </summary>
        /// <param name="StringIn"></param>
        /// <returns></returns>

        private Line AddData(String StringIn)
        {

            String StringOut = PrepareData(StringIn);
            // if we have a partial line, add to it.
            if (partialLine != null)
            {
                // tack it on

                partialLine.Str = partialLine.Str + StringOut;
                //data_STR = data_STR + partialLine.Str;

                outputList_Update(partialLine);

                return partialLine;
            }

            return outputList_Add(StringOut, receivedColor);
        }
        /// <summary>
        /// add a new line to output window
        /// </summary>
        Line outputList_Add(string str, Color color)
        {
            Line newLine = new Line(str, color);
            lines.Add(newLine);

            if (outputList_ApplyFilter(newLine.Str))
            {
                outputList.Items.Add(newLine);
                outputList_Scroll();
            }

            return newLine;
        }
        /// <summary>
        /// Scroll to bottom of output window
        /// </summary>
        void outputList_Scroll()
        {
            if (scrolling)
            {
                int itemsPerPage = (int)(outputList.Height / outputList.ItemHeight);
                outputList.TopIndex = outputList.Items.Count - itemsPerPage;
            }
        }
        private void outputList_SelectedIndexChanged(object sender, EventArgs e)
        {
            popUpMenu.MenuItems[0].Enabled = (outputList.SelectedItems.Count > 0);
        }
        /// <summary>
        /// Update a line in the output window.
        /// </summary>
        /// <param name="line">line to update</param>
        void outputList_Update(Line line)
        {
            // should we add to output?
            if (outputList_ApplyFilter(line.Str))
            {
                // is the line already displayed?
                bool found = false;
                for (int i = 0; i < outputList.Items.Count; ++i)
                {
                    int index = (outputList.Items.Count - 1) - i;
                    if (line == outputList.Items[index])
                    {
                        // is item visible?
                        int itemsPerPage = (int)(outputList.Height / outputList.ItemHeight);
                        if (index >= outputList.TopIndex &&
                            index < (outputList.TopIndex + itemsPerPage))
                        {
                            // is there a way to refresh just one line
                            // without redrawing the entire listbox?
                            // changing the item value has no effect
                            outputList.Refresh();
                        }
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    // not found, so add it
                    outputList.Items.Add(line);
                }
            }
        }
        /// <summary>
        /// check to see if filter matches string
        /// </summary>
        /// <param name="s">string to check</param>
        /// <returns>true if matches filter</returns>
        bool outputList_ApplyFilter(String s)
        {
            if (filterString == "")
            {
                return true;
            }
            else if (s == "")
            {
                return false;
            }
            else if (Settings_1.Option.FilterUseCase)
            {
                return (s.IndexOf(filterString) != -1);
            }
            else
            {
                string upperString = s.ToUpper();
                string upperFilter = filterString.ToUpper();
                return (upperString.IndexOf(upperFilter) != -1);
            }
        }
        string Friction_time_act = "", Joining_time_act = "", Dwell_time_act = "", Welding_depth_act = "", Friction_press_act = "";
        double Friction_time_act_D = 0, Joining_time_act_D = 0, Dwell_time_act_D = 0, Welding_depth_act_D = 0, Friction_press_act_D = 0;
        public void OnDataReceived(string dataIn)
        {
            
            //Handle multi-threading
            if (InvokeRequired)
            {
                Invoke(new StringDelegate(OnDataReceived), new object[] { dataIn });
                return;
            }

            // pause scrolling to speed up output of multiple lines
            bool saveScrolling = scrolling;
            scrolling = false;

            // if we detect a line terminator, add line to output
            int index;
            while (dataIn.Length > 0 &&
                ((index = dataIn.IndexOf("\r")) != -1
                || (index = dataIn.IndexOf("\n")) != -1)
                )
            {
                String StringIn = dataIn.Substring(0, index);
                dataIn = dataIn.Remove(0, index + 1);

                logFile_writeLine(AddData(StringIn).Str);
                partialLine = null;	// terminate partial line
            }

            // if we have data remaining, add a partial line
            if (dataIn.Length > 0)
            {
                partialLine = AddData(dataIn);
            }
            bool check = false;
            string data_plus = "";
            try
            {
                foreach (Line d in outputList.Items)
                {
                    textBox5.Text = "";
                    data_plus = data_plus + d.Str.Replace("\r", "").Replace("\n", "") + "/";
                    textBox5.Text = data_plus;
                    Regex rx = new Regex(@"^(--------------)[a-zA-Z0-9\/\.\-\+\:\<\> ]+(----------------------------------------\/\/)$");
                    check = rx.IsMatch(textBox5.Text);
                    textBox6.Text = check.ToString();
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
                return;
            }
            if (check)
            {
                //if (textBox6.Text.Split('/').Length > 17)
                //{
                try
                {
                    string[] arryData = textBox5.Text.Split('/');
                    foreach (string tt in arryData)
                    {
                        if (tt.IndexOf("FRICTION TIME") != -1)
                        {
                            Friction_time_act = tt;
                        }
                        if (tt.IndexOf("JOINING TIME") != -1)
                        {
                            Joining_time_act = tt;
                        }
                        if (tt.IndexOf("DWELL TIME") != -1)
                        {
                            Dwell_time_act = tt;
                        }
                        if (tt.IndexOf("WELDING DEPTH") != -1)
                        {
                            Welding_depth_act = tt;
                        }
                        if (tt.IndexOf("FRICTION PRESS") != -1)
                        {
                            Friction_press_act = tt;
                        }
                    }




                    string[] Friction_time_act_arry = Friction_time_act.Split(' ');
                    string[] Joining_time_act_arry = Joining_time_act.Split(' ');
                    string[] Dwell_time_act_arry = Dwell_time_act.Split(' ');
                    string[] Welding_depth_act_arry = Welding_depth_act.Split(' ');
                    string[] Friction_press_act_arry = Friction_press_act.Split(' ');

                    //Friction_time_act_D = Convert.ToDouble(Friction_time_act_arry[Friction_time_act_arry.Length - 2]);
                    //Joining_time_act_D = Convert.ToDouble(Joining_time_act_arry[Joining_time_act_arry.Length - 2]);
                    //Dwell_time_act_D = Convert.ToDouble(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]);
                    //Welding_depth_act_D = Convert.ToDouble(Welding_depth_act_arry[Welding_depth_act_arry.Length - 2]);

                    if (Friction_time_act_arry.Length > 2)
                    {
                        if (isNumber(Friction_time_act_arry[Friction_time_act_arry.Length - 2]))
                            Friction_time_act_D = Convert.ToDouble(Friction_time_act_arry[Friction_time_act_arry.Length - 2]);
                    }

                    if (Joining_time_act_arry.Length > 2)
                    {
                        if (isNumber(Joining_time_act_arry[Joining_time_act_arry.Length - 2]))
                            Joining_time_act_D = Convert.ToDouble(Joining_time_act_arry[Joining_time_act_arry.Length - 2]);
                    }

                    if (Dwell_time_act_arry.Length > 2)
                    {
                        if (isNumber(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]))
                            Dwell_time_act_D = Convert.ToDouble(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]);
                    }

                    if (Welding_depth_act_arry.Length > 2)
                    {
                        if (isNumber(Welding_depth_act_arry[Welding_depth_act_arry.Length - 3]))
                            Welding_depth_act_D = Convert.ToDouble(Welding_depth_act_arry[Welding_depth_act_arry.Length - 3]);
                    }

                    if (Friction_press_act_arry.Length > 2)
                    {
                        if (isNumber(Friction_press_act_arry[Friction_press_act_arry.Length - 2]))
                            Friction_press_act_D = Convert.ToDouble(Friction_press_act_arry[Friction_press_act_arry.Length - 2]);
                    }
                    //}
                }
                catch (Exception eee)
                {
                    MessageBox.Show(eee.Message);
                    return;
                }
                
                    if (Friction_time_act_D != 0 && Joining_time_act_D != 0 && Dwell_time_act_D != 0 && Welding_depth_act_D != 0 && Friction_press_act_D != 0)
                    {
                        string goodFlag = "";
                        try
                        {
                            double WeldingTime = Friction_time_act_D + Joining_time_act_D + Dwell_time_act_D;
                            goodFlag = "";
                            if (Convert.ToDouble(txt_Weld_time_row.Text.Trim()) > WeldingTime)
                            {
                                //WeldingTime 불량, 최저 시간보다 작습니다.
                                goodFlag = "N";

                            }
                            else if (Convert.ToDouble(txt_Weld_time_higt.Text.Trim()) < WeldingTime)
                            {
                                //WeldingTime 불량, 최고 시간보다 큽니다.
                                goodFlag = "N";

                            }
                            else if (Convert.ToDouble(txt_Weld_depth_row.Text.Trim()) > Welding_depth_act_D)
                            {
                                //WeldingDepth 불량, 최저값 보다 작습니다.
                                goodFlag = "N";

                            }
                            else if (Convert.ToDouble(txt_Weld_depth_higt.Text.Trim()) < Welding_depth_act_D)
                            {
                                //WeldingDepth 불량, 최고값 보다 큽니다.
                                goodFlag = "N";

                            }
                            else
                            {
                                goodFlag = "Y";
                            }

                            STR_DATA = "";
                        }
                        catch (Exception eeee)
                        {
                            MessageBox.Show("eeee"+eeee.Message);
                            return;
                        }

                        //
                        try
                        {

                            string chk = "";
                            chk = save_data(txt_reading.Text, Friction_time_act_D, Joining_time_act_D, Dwell_time_act_D, Welding_depth_act_D, Friction_press_act_D, goodFlag);
                            txt_reading.Text = "";

                            if (goodFlag.Equals("Y"))
                            {
                                if (chk.Equals("Y"))
                                {

                                    sqty_cnt.Text = "" + (int.Parse(sqty_cnt.Text) + 1);
                                    PpCard_Success.TopLevel = true;
                                    PpCard_Success.TopMost = true;
                                    PpCard_Success.Visible = true;
                                    PpCard_Success.set_text("융착 합격", 5);
                                    PpCard_Success.BackColor = Color.RoyalBlue;
                                    textBox5.Text = "";
                                    log_write();
                                    if (int.Parse(sqty_cnt.Text.Trim()) >= int.Parse(btn_carrier_sqty.Text))
                                    {
                                        //대차수량
                                        string pd_lot_no = work_input_save_virtual("D001", mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code.Trim()
                                                , btn_carrier_sqty.Text.Trim(), txt_datetime.Text, "","", "Y");


                                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                        txt_datetime.Text = r_start;
                                        set_현재고_최대생산가능수량();

                                       
                                            // 식별표 출력
                                            //it_chart_print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no);
                                            Func_Mobis_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no,2);
                                            PpCard_Success10.TopLevel = true;
                                            PpCard_Success10.TopMost = true;
                                            PpCard_Success10.Visible = true;
                                            PpCard_Success10.set_text2(11);
                                       
                                        
                                        /*
                                        string hh = DateTime.Now.Hour.ToString();
                                        string mm = DateTime.Now.Minute.ToString();
                                        if (hh.Length == 1)
                                        {
                                            hh = "0" + hh;
                                        }
                                        if (mm.Length == 1)
                                        {
                                            mm = "0" + mm;
                                        }
                                        string time = hh + mm;


                                        if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                                        {
                                            txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(sqty_cnt.Text));
                                            sqty_cnt.Text = "0";
                                        }
                                        else
                                        {
                                            txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(sqty_cnt.Text));
                                            sqty_cnt.Text = "0";
                                        }*/
                                    }
                                    
                                    
                                }
                            }
                            else if (goodFlag.Equals("N"))
                            {
                                if (chk.Equals("Y"))
                                {
                                    if (DialogResult.OK == MessageBox.Show("불량 데이터가 수신 되었습니다.\r\n불량으로 등록 하시겠습니까?", "불량 등록 여부", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                                    {
                                        int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                                        if (SUB_SAVE.fail_input_data(Properties.Settings.Default.SITE_CODE.ToString(), "B18", mo_snumb, vw_snumb, str_wc_code))
                                        {
                                            txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                                        }
                                        PpCard_Success.TopLevel = true;
                                        PpCard_Success.TopMost = true;
                                        PpCard_Success.Visible = true;
                                        PpCard_Success.set_text("융착 불합격", 5);
                                        PpCard_Success.BackColor = Color.Red;
                                    }
                                }
                            }

                            

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("등록 실패 : " + ex.Message);
                        }
                        finally
                        {
                            
                            outputList.Items.Clear();
                        }
                    }
            }
            // restore scrolling
            scrolling = saveScrolling;
            outputList_Scroll();
        }
        public bool isNumber(string strValue)
        {
            strValue = strValue.Replace(" ", "");
            if (strValue == null || strValue.Length < 1)
                return false;
            Regex rx = new Regex(@"^[0-9]+\.*[0-9]*$");
            return rx.IsMatch(strValue);
        }
        private string save_data(string barcode, double Friction_time_act_D, double Joining_time_act_D, double Dwell_time_act_D, double Welding_depth_act_D, double Friction_press_act_D, string good_gubn)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("U_SP_Y_BARCODE_CS", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BARCODE", barcode);
            cmd.Parameters.AddWithValue("@IT_SCODE", txt_IT_SCODE.Text.Trim());
            cmd.Parameters.AddWithValue("@FRICTION_TIME", Friction_time_act_D);
            cmd.Parameters.AddWithValue("@JOINING_TIME", Joining_time_act_D);
            cmd.Parameters.AddWithValue("@DWELL_TIME", Dwell_time_act_D);
            cmd.Parameters.AddWithValue("@WELDING_DEPTH", Welding_depth_act_D);
            cmd.Parameters.AddWithValue("@FRICTION_PRESS", Friction_press_act_D);
            cmd.Parameters.AddWithValue("@GOOD_GUBN", good_gubn);
            cmd.Parameters.AddWithValue("@SITE_CODE", "D001");
            cmd.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
            cmd.Parameters.AddWithValue("@PRDT_ITEM", txt_IT_SCODE.Text.Trim());
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);

            conn.Open();
            string chk = "";
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    chk = reader["CHECK_YN"].ToString();
                }
                reader.Close();
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return chk;
        }
        protected override void OnClosed(EventArgs e)
        {
            CommPort com = CommPort.Instance;
            com.Close();

            base.OnClosed(e);
        }
        /*
        #region Swing-u 함수(Notify)
        void Swing_NotifyParameterChanged(SwingLibrary.SwingParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else { 
                            //radioButton_ac_multi.Checked = true;
                        }
                            break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing.GetVersionHW();
                        //label_version_fw.Text = Swing.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing.GetTagCount();
                        int ui_count = dsmListView_ivt.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                //rdbBCD.Text = "B1D";
                                //rdbBCD.Enabled = false;
                                //rdbENC.Enabled = false;
                                 
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                //rdbBCD.Text = "B2D";
                                //rdbBCD.Enabled = false;
                                //rdbENC.Enabled = false;
                                 
                                break;
                            default:
                                //rdbBCD.Enabled = false;
                                //rdbENC.Enabled = false;
                                 
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID(found_tag_uid, found_tag_index);
                }

                if (Swing.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'), "B");

        }
        
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }

        void Swing_NotifyButtonEvent(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        //Thread key_event = new Thread(Swing_FnKeyFired);
                        //key_event.Start();
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        //label_key_read_click.BackColor = Color.Red;
                        //label_key_read_click.ForeColor = Color.White;
                        //label_key_read_click.Text = "Clicked";
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        
                        //label_key_read_click.BackColor = Color.LightGray;
                        //label_key_read_click.ForeColor = Color.DarkGray;
                        //label_key_read_click.Text = "Released";
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no="";
                        rec_it_scode = "";
                        rec_card_no = "";
                        
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport(data.Trim('\0'));
            else
                Swing_ParseTagReport(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        //label_key_read_click.BackColor = Color.Red;
                        //label_key_read_click.ForeColor = Color.White;
                        //label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        //labelProgress.Text = "Reading.!!!";
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        
                        //label_key_read_click.BackColor = Color.LightGray;
                        //label_key_read_click.ForeColor = Color.DarkGray;
                        //label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        //labelProgress.Text = "Stand By.!!!";
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수
        private object locker = new object();
        private bool key = false;
        private void SyncTagList()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt.Items[i].SubItems[2].Text; }));

                lock (locker) key = false;
                Swing.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker) if (key) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
            }));

            Swing.ReportTagList();
        }
        
        private void Swing_ParseTagReport(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID(data, 0, mode);
            }
        }
        
        private void Swing_ParseMemoryReadReport(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);

                    if (dsmListView_ivt.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID(data, 0);
            }
        }
        private void UpdateUID(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();

                    saveDataRow_PP(itemString[3]);
                    //saveDataRow_DC_S(itemString[3]);
                    //txtIn_Carrier_no.Text = in_carrier_no;
                    txtIt_scode.Text = rec_it_scode;
                    txtCard_no.Text = rec_card_no;
                    
                    if (in_carrier_no == sel_carrier_no) { 
                        txtIn_Carrier_no.Text = in_carrier_no;
                    }
                    if (rec_it_scode == sel_it_scode) { 
                        txtIt_scode.Text = rec_it_scode;
                    }
                    if (rec_card_no == sel_card_no) { 
                        txtCard_no.Text = rec_card_no;
                    }
                    
                    Swing.SetBuzzerVolume(SwingLibrary.SwingAPI.BuzzerVolume.MUTE);
                    if (!string.IsNullOrWhiteSpace(txtIt_scode.Text) || !string.IsNullOrWhiteSpace(txtCard_no.Text))
                    {
                        //if (sel_carrier_no == in_carrier_no && sel_it_scode == rec_it_scode && sel_card_no == rec_card_no)
                        //{
                        //대차가 작업장에 있는지 체크 (품목,PP시리얼,대차코드)
                        ////
                        DataRow[] Dr = DataGridViewDT.Select("PRDT_ITEM='" + txtIt_scode.Text + "' OR CARD_NO='" + rec_card_no + "'");

                        if (Dr.Length > 0)
                        {
                            Swing.InventoryStop();
                            //작업장으로 이동

                            MOVE_FUNC.move_insert(Dr[0][5].ToString(), Dr[0][4].ToString(), Dr[0][3].ToString(), float.Parse(Dr[0][10].ToString())
                                , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());

                            Success_Form.TopLevel = true;
                            Success_Form.TopMost = true;
                            Success_Form.Visible = true;
                            Success_Form.set_text(Dr[0][3].ToString().Trim());
                            timer1.Start();

                            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            dataGridView1.DataSource = DataGridViewDT;
                            dataGridView1.CurrentCell = null;

                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";
                            sel_carrier_no = "";
                            sel_it_scode = "";
                            sel_card_no = "";
                            //Swing.InventoryStop();
                            //Swing.TagListClear();
                            //dsmListView_ivt.Items.Clear();
                            //ddc_ivt.DigitText = "00000";

                            Swing.InventoryStart();
                            if (dataGridView1.Rows.Count == 0)
                            {
                                txtNext_Carrier_no.Text = "";
                            }
                            else
                            {
                                //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                                sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                                sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                                //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                            }
                        }
                        else
                        {
                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";

                            //Swing.TagListClear();
                            //dsmListView_ivt.Items.Clear();
                            //ddc_ivt.DigitText = "00000";
                        }
                        Swing.SetBuzzerVolume(SwingLibrary.SwingAPI.BuzzerVolume.MAX);
                    }                    
                }
            }));

            if (new_item)
            {
                
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
                 
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        
        

        public void saveDataRow_PP(string str)
        {
            
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {
                        //dataGridView1.Rows[0].Cells["PP_SERNO"].Value.ToString();
                        //dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString();
                        //lot_no = dataGridView1.Rows[0].Cells["LOT_NO"].Value.ToString();
                        
                        //dr["SITE_CODE"] = strTag[0];
                        //dr["WC_CODE"] = strTag[1];
                        rec_card_no = strTag[2].Trim();
                        rec_it_scode = strTag[3].Trim();
                        f_mm_sqty = float.Parse(strTag[4]);
                    }
                }
            }
            
        }
        public void saveDataRow_DC_S(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    in_carrier_no = strTags[1].Trim();                    
                }
            }

        }
        public void saveDataRow_DC_S2(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("DC"))
                {
                    if (CHECK_FUNC.carrier_check(strTags[1].Trim()))
                    {
                        if (txtOut_carrier_no.Text.Trim() == "")
                        {
                            if (wc_group.Equals("PLR"))
                            {
                                if (strTags[1].Substring(0, 1).Equals("P"))
                                {
                                    out_carrier_no = strTags[1].Trim();
                                    txtOut_carrier_no.Text = out_carrier_no;
                                    //MessageBox.Show(out_carrier_no);
                                    if (btn_on_off.Text.Trim().Equals("On"))
                                    {
                                        Swing2.InventoryStop();
                                        Swing2.TagListClear();
                                        dsmListView_ivt2.Items.Clear();
                                        ddc_ivt2.DigitText = "00000";
                                    }
                                }
                                else
                                {
                                    if (CHECK_FUNC.Match_carrier_no(txt_IT_SCODE.Text.Trim(), strTags[1].Trim()))//대차 번호 가 맞는지 체크 
                                    {
                                        out_carrier_no = strTags[1].Trim();
                                        txtOut_carrier_no.Text = out_carrier_no;
                                        //MessageBox.Show(out_carrier_no);
                                        if (btn_on_off.Text.Trim().Equals("On"))
                                        {
                                            Swing2.InventoryStop();
                                            Swing2.TagListClear();
                                            dsmListView_ivt2.Items.Clear();
                                            ddc_ivt2.DigitText = "00000";
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("품목에 해당하는 대차가 아닙니다.");
                                    }
                                }
                            }
                            else
                            {
                                if (CHECK_FUNC.Match_carrier_no(txt_IT_SCODE.Text.Trim(), strTags[1].Trim()))//대차 번호 가 맞는지 체크 
                                {
                                    out_carrier_no = strTags[1].Trim();
                                    txtOut_carrier_no.Text = out_carrier_no;
                                    //MessageBox.Show(out_carrier_no);
                                    if (btn_on_off.Text.Trim().Equals("On"))
                                    {
                                        Swing2.InventoryStop();
                                        Swing2.TagListClear();
                                        dsmListView_ivt2.Items.Clear();
                                        ddc_ivt2.DigitText = "00000";
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        saveDataRow_DC_S(str);
                        txtIn_Carrier_no.Text = in_carrier_no;
                        if (!string.IsNullOrWhiteSpace(txtIn_Carrier_no.Text) || !string.IsNullOrWhiteSpace(txtIt_scode.Text) || !string.IsNullOrWhiteSpace(txtCard_no.Text))
                        {
                            //if (sel_carrier_no == in_carrier_no && sel_it_scode == rec_it_scode && sel_card_no == rec_card_no)
                            //{
                            //대차가 작업장에 있는지 체크 (품목,PP시리얼,대차코드)
                            ////
                            DataRow[] Dr = DataGridViewDT.Select("CARRIER_NO='" + in_carrier_no + "' OR CARD_NO='" + rec_card_no + "'");

                            if (Dr.Length > 0)
                            {
                                Swing2.InventoryStop();
                                //작업장으로 이동

                                MOVE_FUNC.move_insert(Dr[0][5].ToString(), Dr[0][4].ToString(), Dr[0][3].ToString(), float.Parse(Dr[0][10].ToString())
                                    , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());

                                //Success_Form Success_Form = new Success_Form();
                                //Success_Form.carrier_no = Dr[0][3].ToString();
                                //Success_Form.Show();
                                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                                dataGridView1.DataSource = DataGridViewDT;
                                dataGridView1.CurrentCell = null;

                                txtIn_Carrier_no.Text = "";
                                txtIt_scode.Text = "";
                                txtCard_no.Text = "";
                                in_carrier_no = "";
                                rec_it_scode = "";
                                rec_card_no = "";
                                sel_carrier_no = "";
                                sel_it_scode = "";
                                sel_card_no = "";
                                Swing2.InventoryStop();
                                Swing2.TagListClear();
                                dsmListView_ivt2.Items.Clear();
                                ddc_ivt2.DigitText = "00000";

                                Swing2.InventoryStart();
                                if (dataGridView1.Rows.Count == 0)
                                {
                                    txtNext_Carrier_no.Text = "";
                                }
                                else
                                {
                                    //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                                    sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                                    sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                                    //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                                }
                            }
                            else
                            {
                                txtIn_Carrier_no.Text = "";
                                txtIt_scode.Text = "";
                                txtCard_no.Text = "";
                                in_carrier_no = "";
                                rec_it_scode = "";
                                rec_card_no = "";

                                //Swing.TagListClear();
                                //dsmListView_ivt.Items.Clear();
                                //ddc_ivt.DigitText = "00000";
                            }
                            
                        }   
                    }
                    
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                    item = dsmListView_ivt.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt.BeginUpdate();
                    dsmListView_ivt.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt.Invoke(new EventHandler(delegate
                {
                    ddc_ivt.DigitText = string.Format("{0:00000}", dsmListView_ivt.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt.Items.Count > 0)
                {
                 //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening(object sender, CancelEventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove(object sender, EventArgs e)
        {
            if (listView_target_list.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list.SelectedIndices[0];
            ListViewItem item = listView_target_list.Items[idx];

            listView_target_list.BeginUpdate();

            listView_target_list.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list.Items.Count; i++)
            {
                listView_target_list.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list.EndUpdate();
        }

        private void button_com_open_Click(object sender, EventArgs e)
        {
            try
            {
                //Swing.ConnectionOpen(comboBox_ports.SelectedItem.ToString());
                Swing.ConnectionOpen(comboBox_ports.SelectedValue.ToString(), 5);

                if (Swing.IsOpen)
                {   
                    
                    Properties.Settings.Default.ComPortName = Swing.PortName;
                    Properties.Settings.Default.Save();
                    Swing.InventoryStop();
                    Swing.TagListClear();
                    dsmListView_ivt.Items.Clear();
                    ddc_ivt.DigitText = "00000";
                    txtIn_Carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";
                    Swing.SetRFPower(27);
                    Swing.ReportAllInformation();

                    
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
              
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }

        private void button_com_close_Click(object sender, EventArgs e)
        {
            if (Swing.ConnectionClose())
            {
              
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports.Enabled = !Swing.IsOpen;
            button_com_open.Enabled = !Swing.IsOpen;
            button_com_close.Enabled = Swing.IsOpen;
            //checkBox_dongle.Enabled = !Swing.IsOpen;
        }
        #endregion
        */
        #region Swing-u 함수2(Notify)
        void Swing_NotifyParameterChanged2(SwingLibrary.SwingParameter parameterType)
        {
            
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingParameter.BatteryStatus:
                        //label_battery_volt.Text = string.Format("Volts: {0:F3} [V]", Swing2.GetBatteryVolt());
                        //dDC_Battery.DigitText = string.Format("{0:000}", Swing2.GetBatteryRate());
                        break;
                    case SwingLibrary.SwingParameter.BuzzerVolume:
                        switch (Swing2.GetBuzzerVolume())
                        {
                            case SwingLibrary.SwingAPI.BuzzerVolume.MAX:
                                //radioButton_vol_max.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MIN:
                                //radioButton_vol_min.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.MUTE:
                                //radioButton_vol_mute.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.NORMAL:
                                //radioButton_vol_normal.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.BuzzerVolume.VIBRATION:
                                //radioButton_vol_vib.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ChargingStatus:
                        switch (Swing2.GetChargeMode())
                        {
                            case SwingLibrary.SwingAPI.ChargingMode.CHARGING:
                                //radioButton_btr_charging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.DISCHARGING:
                                //radioButton_btr_discharging.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.ChargingMode.FULL:
                                //radioButton_btr_full.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.ContinuousMode:
                        if (Swing2.GetContinuous() == SwingLibrary.SwingAPI.ContinuousMode.SINGLE)
                        {
                            //radioButton_ac_single.Checked = true;
                        }
                        else
                        {
                            //radioButton_ac_multi.Checked = true;
                        }
                        break;
                    case SwingLibrary.SwingParameter.FindStepUnit:
                        int int_unit = Swing2.GetFindStepUnit();
                        //textBox_find_unit.Text = int_unit.ToString();
                        break;
                    case SwingLibrary.SwingParameter.FindThreshold:
                        int int_th = Swing2.GetFindThreshold();
                        //textBox_find_threshold.Text = int_th.ToString();
                        break;
                    case SwingLibrary.SwingParameter.InventoryMode:
                        comboBox_inventory_mode.SelectedIndex = (int)Swing2.GetInventoryMode();
                        //label_inventory_mode.Text = comboBox_inventory_mode.SelectedValue.ToString();
                        break;
                    case SwingLibrary.SwingParameter.RFPower:
                        int atten = Swing2.GetRFPower();
                        //comboBox_rfpwr.SelectedIndex = atten;
                        break;
                    case SwingLibrary.SwingParameter.TagReportMode:
                        switch (Swing2.GetTagReportMode())
                        {
                            case SwingLibrary.SwingAPI.TagReportMode.ALWAYS:
                                //radioButton_bz_always.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.TagReportMode.TRIGGER:
                                //radioButton_bz_trigger.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.Version:
                        //label_version_hw.Text = Swing2.GetVersionHW();
                        //label_version_fw.Text = Swing2.GetVersionFW();
                        break;
                    case SwingLibrary.SwingParameter.TagCount:
                        int swing_count = Swing2.GetTagCount();
                        int ui_count = dsmListView_ivt2.Items.Count;
                        if (ui_count != swing_count)
                        {
                            new Thread(SyncTagList2).Start();
                        }
                        break;
                    case SwingLibrary.SwingParameter.PowerOff:
                        //Thread poff_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //poff_thread.Start(true);
                        break;
                    case SwingLibrary.SwingParameter.PowerDown:
                        //Thread pdown_thread = new Thread(new ParameterizedThreadStart(CloseThreadFunction));
                        //pdown_thread.Start(false);
                        break;
                    case SwingLibrary.SwingParameter.LCDControlMode:
                        switch (Swing2.GetLCDControlMode())
                        {
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_COUNT:
                                //radioButton_lcd_host_count.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.HOST_TEXT:
                                //radioButton_lcd_host_text.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.LCDControlMode.SWING:
                                //radioButton_lcd_swingU.Checked = true;
                                break;
                            default:
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.InventoryTimeout:
                        int timeout = Swing2.GetInventoryTimeout();
                        //textBox_inventory_timeout.Text = timeout.ToString();
                        break;
                    case SwingLibrary.SwingParameter.SwingMode:
                        switch (Swing2.GetSwingMode())
                        {
                            case SwingLibrary.SwingAPI.SwingMode.RFID:
                                //rdbRFID.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.BCD:
                                //rdbBCD.Checked = true;
                                break;
                            case SwingLibrary.SwingAPI.SwingMode.ENC:
                                //rdbENC.Checked = true;
                                break;
                        }
                        break;
                    case SwingLibrary.SwingParameter.BarcodeType:
                        switch (Swing2.GetBarcodeType())
                        {
                            case SwingLibrary.SwingAPI.BarcodeType.B1D:
                                /*rdbBCD.Text = "B1D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            case SwingLibrary.SwingAPI.BarcodeType.B2D:
                                /*rdbBCD.Text = "B2D";
                                rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                            default:
                                /*rdbBCD.Enabled = false;
                                rdbENC.Enabled = false;
                                 */
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }));
        }

        void Swing_NotifyTagFound2(string data)
        {
            string[] datas = data.Split(',');
            try
            {
                ulong found_tag_index = Convert.ToUInt32(datas[0]);
                string found_tag_uid = datas[1];

                if (found_tag_index == 0)
                {
                    //non-matched
                }
                else if (found_tag_index == 99999)
                {
                    //wildcard-matched
                    UpdateUID2(found_tag_uid, found_tag_index);
                }
                else
                {
                    UpdateUID2(found_tag_uid, found_tag_index);
                }

                if (Swing2.GetInventoryMode() == SwingLibrary.SwingAPI.InventoryMode.SEARCH_SINGLE
                    && found_tag_index == 1)
                {
                    //string msg = string.Format("Index: {0:D5}\r\nUID: {1}", found_tag_index, found_tag_uid);
                    //MessageBox.Show(msg, "Single Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Swing2.SetInventoryMode(SwingLibrary.SwingAPI.InventoryMode.INVENTORY_NORMAL);
                    Swing2.SetRFPower(0);
                }
            }
            catch { }
        }
        void Swing_NotifyInventoryBCD2(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport2(data.Trim('\0'));
            else
                Swing_ParseTagReport2(data.Trim('\0'), "B");

        }
        /*
        void Swing_NotifyError(string data)
        {
            lock (locker) key = true;
            this.Invoke(new EventHandler(delegate
            {
                rTB_error.Clear();
                if (data.Equals("No Error"))
                {
                    Utils.AddText(rTB_error, Color.Blue, "Memory access success");
                }
                else
                {
                    Utils.AddText(rTB_error, Color.Red, string.Format("Error: {0}", data));
                }
            }));
        }*/

        void Swing_NotifyButtonEvent2(SwingLibrary.ButtonEvent buttonType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (buttonType)
                {
                    case SwingLibrary.ButtonEvent.FN:
                        /*Thread key_event = new Thread(Swing_FnKeyFired);
                        key_event.Start();*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTART:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";*/
                        break;
                    case SwingLibrary.ButtonEvent.READSTOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";*/
                        break;
                    case SwingLibrary.ButtonEvent.TAGLISTCLEAR:
                        dsmListView_ivt2.Items.Clear();
                        ddc_ivt2.DigitText = "00000";
                        //txtOut_carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        //out_carrier_no = "";
                        rec_it_scode = "";
                        rec_card_no = "";
                        //Tag_DT_DC.Clear();
                        Tag_DT_PP.Clear();
                        //po_DT.Clear();
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyStatus2(SwingLibrary.SwingStatusParameter parameterType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (parameterType)
                {
                    case SwingLibrary.SwingStatusParameter.BatteryWarning:
                        MessageBox.Show("Please charge the battery.!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    default:
                        break;
                }
            }));
        }
        void Swing_NotifyInventory2(string data)
        {
            if (data.Contains("M"))
                Swing_ParseMemoryReadReport2(data.Trim('\0'));
            else
                Swing_ParseTagReport2(data.Trim('\0'));

        }
        void Swing_NotifyReadEvent2(SwingLibrary.ReadEvent readType)
        {
            this.Invoke(new EventHandler(delegate
            {
                switch (readType)
                {
                    case SwingLibrary.ReadEvent.START:
                        /*label_key_read_click.BackColor = Color.Red;
                        label_key_read_click.ForeColor = Color.White;
                        label_key_read_click.Text = "Clicked";
                        //this.pictureBox4.Image = Properties.Resources.Reading;
                        labelProgress.Text = "Reading.!!!";*/
                        break;
                    case SwingLibrary.ReadEvent.STOP:
                        /*
                        label_key_read_click.BackColor = Color.LightGray;
                        label_key_read_click.ForeColor = Color.DarkGray;
                        label_key_read_click.Text = "Released";
                        //this.pictureBox4.Image = Properties.Resources.StandBy;
                        labelProgress.Text = "Stand By.!!!";*/
                        break;
                    default:
                        break;
                }
            }));
        }
        #endregion
        #region Swing-u 기본함수2
        private object locker2 = new object();
        private bool key2 = false;
        private void SyncTagList2()
        {
            int count = 0;
            string uid = string.Empty;

            this.Invoke(new EventHandler(delegate { count = dsmListView_ivt2.Items.Count; }));

            for (int i = 0; i < count; i++)
            {
                this.Invoke(new EventHandler(delegate { uid = dsmListView_ivt2.Items[i].SubItems[2].Text; }));

                lock (locker2) key2 = false;
                Swing2.TagListAdd(uid);
                while (true)
                {
                    Thread.Sleep(0);
                    lock (locker2) if (key2) break;
                }
            }

            this.Invoke(new EventHandler(delegate
            {
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
            }));

            Swing2.ReportTagList();
        }

        private void Swing_ParseTagReport2(string data, string mode)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing2.GetRFPower();
                //double txp_watt = Math.Pow(10, ((30 - Swing2.GetRFPower()) / 10)) / 1000;
                //double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                //double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID2(datas[0], rssi, meter, mode);
            }
            else
            {
                UpdateUID2(data, 0, mode);
            }
        }

        private void Swing_ParseMemoryReadReport2(string data)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess2(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void Swing_ParseMemoryReadReport2(string data, string mode)
        {
            this.Invoke(new EventHandler(delegate
            {
                string[] datas = data.Split('M');
                UpdateUIDforAccess2(datas[0]);
                //rTB_read.Clear();
                //Utils.AddText(rTB_read, Color.Gray, "UID: ");
                //Utils.AddText(rTB_read, Color.Blue, string.Format("{0}", datas[0]));
                if (datas[1].Length > 0)
                {
                    int dlength, offset, counts = 0;

                    dlength = datas[1].Length / 2;
                    //offset = int.Parse(textBox_BlockOffset.Text.Trim());
                    //counts = int.Parse(textBox_BlockCount.Text.Trim());

                    //Utils.AddText(rTB_read, FontStyle.Regular, Color.Gray, "\r\nDATA: ");
                    //Utils.AddText(rTB_read, Color.OrangeRed, datas[1]);
                }
            }));
        }
        private void UpdateUIDforAccess2(string UID)
        {
            ListViewItem item = null;

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item == null)
                {
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:00000}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();

                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);

                    if (dsmListView_ivt2.Items.Count > 0)
                    {
                        //btnExport.Enabled = true;
                    }
                }
            }));
        }
        private void Swing_ParseTagReport2(string data)
        {
            if (data.Contains(","))
            {
                string[] datas = data.Split(',');
                double rssi = Convert.ToDouble(datas[1]);
                double txp_dbm = 30 - Swing2.GetRFPower();
                double txp_watt = Math.Pow(10, ((30 - Swing2.GetRFPower()) / 10)) / 1000;
                double bs_watt = Math.Pow(10, (rssi / 10)) / 1000;
                double meter_squre = Math.Sqrt((bs_watt / txp_watt));
                //double meter = Math.Sqrt(meter_squre);

                //Console.WriteLine("txp: {0}, rssi: {1}, m^2: {2}", txp_watt, bs_watt, meter_squre);

                double meter = Math.Sqrt(0.4 / (txp_dbm - rssi - 130));

                UpdateUID2(datas[0], rssi, meter);
            }
            else
            {
                UpdateUID2(data, 0);
            }
        }
        private void UpdateUID2(string UID, double RSSI, double meter)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //btnExport.Enabled = true;
                }
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="RSSI"></param>
        /// <param name="meter"></param>
        /// <param name="mode">RFID/BARCODE구분</param>
        private void UpdateUID2(string UID, double RSSI, double meter, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                    item.SubItems[5].Text = string.Format("{0}", RSSI);
                    item.SubItems[6].Text = string.Format("{0:F3}", meter);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";
                    itemString[5] = string.Format("{0}", RSSI);
                    itemString[6] = string.Format("{0:F3}", meter);

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));
        }
        private void UpdateUID2(string UID, ulong tag_index)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID.Trim(), true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);//tag_index
                    itemString[1] = "R";
                    itemString[2] = UID;
                    itemString[3] = Utils.HexToASCII(UID);
                    itemString[4] = "1";
                    saveDataRow_PP(itemString[3]);
                    //saveDataRow_DC_S(itemString[3]);
                    //txtIn_Carrier_no.Text = in_carrier_no;

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();


                    //리딩



                    txtIt_scode.Text = rec_it_scode;
                    txtCard_no.Text = rec_card_no;
                    /*
                    if (in_carrier_no == sel_carrier_no) { 
                        txtIn_Carrier_no.Text = in_carrier_no;
                    }
                    if (rec_it_scode == sel_it_scode) { 
                        txtIt_scode.Text = rec_it_scode;
                    }
                    if (rec_card_no == sel_card_no) { 
                        txtCard_no.Text = rec_card_no;
                    }
                    */
                    
                    if (!string.IsNullOrWhiteSpace(txtIt_scode.Text) || !string.IsNullOrWhiteSpace(txtCard_no.Text))
                    {
                        //if (sel_carrier_no == in_carrier_no && sel_it_scode == rec_it_scode && sel_card_no == rec_card_no)
                        //{
                        //대차가 작업장에 있는지 체크 (품목,PP시리얼,대차코드)
                        ////
                        DataRow[] Dr = DataGridViewDT.Select("PRDT_ITEM='" + txtIt_scode.Text + "' AND CARD_NO='" + rec_card_no + "'");

                        if (Dr.Length > 0)
                        {
                            
                            //작업장으로 이동

                            MOVE_FUNC.move_insert_NEW(Dr[0][4].ToString(), Dr[0][3].ToString(), "", float.Parse(Dr[0][9].ToString())
                                , lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            Swing2.InventoryStop();
                            PpCard_reading.TopLevel = false;
                            PpCard_reading.TopMost = false;
                            PpCard_reading.Visible = false;
                            PpCard_reading_chk = false;

                            Swing2.TagListClear();
                            dsmListView_ivt2.Items.Clear();
                            ddc_ivt2.DigitText = "00000";
                            Tag_DT_PP.Rows.Clear();
                            Tag_DT_DC.Rows.Clear();

                            Success_Form.TopLevel = true;
                            Success_Form.TopMost = true;
                            Success_Form.Visible = true;
                            Success_Form.set_text(Dr[0][3].ToString().Trim());
                            timer1.Start();

                            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                            dataGridView1.DataSource = DataGridViewDT;
                            dataGridView1.CurrentCell = null;

                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";
                            sel_carrier_no = "";
                            sel_it_scode = "";
                            sel_card_no = "";
                            
                            //
                            //Swing.TagListClear();
                            //dsmListView_ivt.Items.Clear();
                            //ddc_ivt.DigitText = "00000";
                            
                            if (dataGridView1.Rows.Count == 0)
                            {
                                txtNext_Carrier_no.Text = "";
                            }
                            else
                            {
                                //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                                sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                                sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                                //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                            }
                            
                            return;
                        }
                        else
                        {
                            txtIn_Carrier_no.Text = "";
                            txtIt_scode.Text = "";
                            txtCard_no.Text = "";
                            in_carrier_no = "";
                            rec_it_scode = "";
                            rec_card_no = "";

                            Swing2.TagListClear();
                            dsmListView_ivt2.Items.Clear();
                            ddc_ivt2.DigitText = "00000";
                            Tag_DT_PP.Rows.Clear();
                            Tag_DT_DC.Rows.Clear();
                            out_carrier_no = "";
                            txtOut_carrier_no.Text = "";
                        }
                    }
                    ///////////////////////////////////////////////////////////////////////////////
                    //PP카드 row add
                    /*
                    DataRow dr = saveDataRow_PP2(itemString[3]);

                    if (dr != null)
                    {
                        if (btn_mix_toggle.Text.Trim().Equals("Off"))
                        {
                            Tag_DT_PP.Rows.Add(dr);
                            Tag_DT_PP.DefaultView.Sort = "CARD_NO ASC";
                        }
                        else if (btn_mix_toggle.Text.Trim().Equals("On"))
                        {
                            DataRow[] drr = Tag_DT_PP_MIX.Select("IT_SCODE='" + dr["IT_SCODE"].ToString() + "'");

                            if (drr.Length == 0)
                            {
                                //if (작업장에서 생산계획이 있는품목인지 체크)
                                if (CHECK_FUNC.Mix_work_plan_check_item(dr["IT_SCODE"].ToString()))
                                {
                                    Tag_DT_PP_MIX.Rows.Add(dr);
                                }

                            }
                        }

                    }
                    */
                    /*if (txtOut_carrier_no.Enabled)
                    {
                        saveDataRow_DC_S2(itemString[3]);
                    }*/
                }
            }));

            if (new_item)
            {

                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));

            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UID"></param>
        /// <param name="tag_index"></param>
        /// <param name="mode">RFID, BARCODE 구분</param>
        private void UpdateUID2(string UID, ulong tag_index, string mode)
        {
            bool new_item = false;
            ListViewItem item = null;

            dsmListView_ivt2.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                    item = dsmListView_ivt2.FindItemWithText(UID, true, 0, false);

                if (item != null)
                {
                    item.SubItems[4].Text = Convert.ToString(Convert.ToInt32(item.SubItems[4].Text) + 1);
                }
                else
                {
                    new_item = true;
                    String[] itemString = new String[dsmListView_ivt2.Columns.Count];
                    itemString[0] = string.Format("{0:D5}", dsmListView_ivt2.Items.Count + 1);//tag_index
                    itemString[1] = mode;
                    itemString[2] = UID;

                    if (mode.Equals("R"))
                        itemString[3] = Utils.HexToASCII(UID);
                    else
                        itemString[3] = UID;

                    itemString[4] = "1";

                    dsmListView_ivt2.BeginUpdate();
                    dsmListView_ivt2.Items.Add(new ListViewItem(itemString));
                    dsmListView_ivt2.EndUpdate();
                }
            }));

            if (new_item)
            {
                ddc_ivt2.Invoke(new EventHandler(delegate
                {
                    ddc_ivt2.DigitText = string.Format("{0:00000}", dsmListView_ivt2.Items.Count);
                }));
            }

            this.Invoke(new EventHandler(delegate
            {
                if (dsmListView_ivt2.Items.Count > 0)
                {
                    //   btnExport.Enabled = true;
                }
            }));

        }
        void remove_menu_Opening2(object sender, CancelEventArgs e)
        {
            if (listView_target_list2.SelectedIndices.Count <= 0) e.Cancel = true;
        }
        void target_remove2(object sender, EventArgs e)
        {
            if (listView_target_list2.SelectedIndices.Count <= 0) return;

            int idx = listView_target_list2.SelectedIndices[0];
            ListViewItem item = listView_target_list2.Items[idx];

            listView_target_list2.BeginUpdate();

            listView_target_list2.Items.RemoveAt(idx);
            for (int i = idx; i < listView_target_list2.Items.Count; i++)
            {
                listView_target_list2.Items[i].SubItems[0].Text = string.Format("{0:D3}", i + 1);
            }

            listView_target_list2.EndUpdate();
        }

        private void button_com_open_Click2(object sender, EventArgs e)
        {
            try
            {
                //Swing2.ConnectionOpen(comboBox_ports2.SelectedItem.ToString());
                Swing2.ConnectionOpen(comboBox_ports2.SelectedValue.ToString(), 5);

                if (Swing2.IsOpen)
                {
                    
                    Properties.Settings.Default.ComPortName = Swing2.PortName;
                    Properties.Settings.Default.Save();
                    Swing2.InventoryStop();
                    Swing2.TagListClear();
                    dsmListView_ivt2.Items.Clear();
                    ddc_ivt2.DigitText = "00000";
                    txtOut_carrier_no.Text = "";
                    txtIt_scode.Text = "";
                    txtCard_no.Text = "";
                    in_carrier_no = "";
                    rec_it_scode = "";
                    rec_card_no = "";

                    Swing2.SetRFPower(27);
                    Swing2.ReportAllInformation();
                    work_plan_popup_paint();
                    //po_release_popup();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

            comboBox_ports2.Enabled = !Swing2.IsOpen;
            button_com_open2.Enabled = !Swing2.IsOpen;
            button_com_close2.Enabled = Swing2.IsOpen;
            //checkBox_dongle.Enabled = !Swing2.IsOpen;
            txt_reading.Focus();
        }

        private void button_com_close_Click2(object sender, EventArgs e)
        {
            if (Swing2.ConnectionClose())
            {
                
            }
            else
            {
                MessageBox.Show("Error on closing", "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            comboBox_ports2.Enabled = !Swing2.IsOpen;
            button_com_open2.Enabled = !Swing2.IsOpen;
            button_com_close2.Enabled = Swing2.IsOpen;
            //checkBox_dongle.Enabled = !Swing2.IsOpen;
            txt_reading.Focus();
        }
        #endregion

        ContextMenuStrip remove_menu2;
        ContextMenuStrip remove_menu;
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                sqty_arry[0] = "0";
                sqty_arry[1] = "0";
                PpCard_reading.Show();
                PpCard_reading.Visible = false;
                PpCard_Success10.Show();
                PpCard_Success10.Visible = false;
                Success_Form.Show();
                Success_Form.Visible = false;
                
                lab_carrier_sqty.Visible = true;
                btn_carrier_sqty.Visible = true;
                
                /*
                if (wc_group.Equals("PFR"))
                {
                    btn_mix_toggle.Visible = true;
                    lbl혼적.Visible = true;
                }*/
                GET_DATA.get_work_time_master();
                night_time_start = GET_DATA.night_time_start;
                day_time_start = GET_DATA.day_time_start;
                
                //timer_now.Start();
                //GET_DATA.SYSTEMTIME stime = new GET_DATA.SYSTEMTIME();
                //stime = GET_DATA.GetTime();
                //N_timer.Text = stime.wYear.ToString() + "-" + stime.wMonth + "-" + stime.wDay + " " + stime.wHour + ":" + stime.wMinute + ":" + stime.wSecond;
                
                GET_DATA.get_timer_master(wc_group);
                timer_po_start.Interval = int.Parse(GET_DATA.po_timer) * 1000;
                timer_pp_start.Interval = int.Parse(GET_DATA.pp_timer) * 1000;

                lueWc_code.Properties.DataSource = GET_DATA.WccodeSelect_DropDown(wc_group);
                lueWc_code.Properties.DisplayMember = "WC_NAME";
                lueWc_code.Properties.ValueMember = "WC_CODE";
                //lookUpEdit1.SelectionStart = 0;
                //lueWc_code.ItemIndex = 0;
                if (regKey.GetValue("WC_CODE") == null)
                {
                    regKey.SetValue("WC_CODE", "");
                }
                else
                {
                    if (regKey.GetValue("WC_CODE").ToString() != "")
                    {

                        lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                    }
                }

                //SWING-U
                this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
                SendMessage((int)dsmListView_ivt.Handle, 0x1000 + 54, 0x00010000, 0x00010000);
               
                /*
                remove_menu = new ContextMenuStrip();
                ToolStripMenuItem item = new ToolStripMenuItem("Remove");
                item.Click += new EventHandler(target_remove);
                remove_menu.Items.Add(item);

                remove_menu.Opening += new CancelEventHandler(remove_menu_Opening);

                listView_target_list.ContextMenuStrip = remove_menu;*/

                remove_menu2 = new ContextMenuStrip();
                ToolStripMenuItem item2 = new ToolStripMenuItem("Remove");
                item2.Click += new EventHandler(target_remove2);
                remove_menu2.Items.Add(item2);

                remove_menu2.Opening += new CancelEventHandler(remove_menu_Opening2);

                listView_target_list2.ContextMenuStrip = remove_menu2;
                Tag_DT_PP.Columns.Add("SITE_CODE", typeof(string));
                Tag_DT_PP.Columns.Add("WC_CODE", typeof(string));
                Tag_DT_PP.Columns.Add("CARD_NO", typeof(string));
                Tag_DT_PP.Columns.Add("IT_SCODE", typeof(string));
                Tag_DT_PP.Columns.Add("SIZE", typeof(string));
                Tag_DT_PP.Columns.Add("CHECK_YN", typeof(string));

                Tag_DT_PP_MIX.Columns.Add("SITE_CODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("WC_CODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("CARD_NO", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("IT_SCODE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("SIZE", typeof(string));
                Tag_DT_PP_MIX.Columns.Add("CHECK_YN", typeof(string));

                po_DT.Columns.Add("MO_SNUMB", typeof(string));
                po_DT.Columns.Add("PO_IT_SCODE", typeof(string));
                po_DT.Columns.Add("PO_IT_SNAME", typeof(string));
                po_DT.Columns.Add("PO_ME_SCODE", typeof(string));
                
                DataGridViewDT.Columns.Add("ROWNUM", typeof(string));//0
                DataGridViewDT.Columns.Add("VW_SNUMB", typeof(string));//1
                DataGridViewDT.Columns.Add("CARRIER_DATE", typeof(string));//2
                //DataGridViewDT.Columns.Add("CARRIER_NO", typeof(string));//3
                DataGridViewDT.Columns.Add("CARD_NO", typeof(string));//4
                DataGridViewDT.Columns.Add("PRDT_ITEM", typeof(string));//5
                DataGridViewDT.Columns.Add("LOT_DATE", typeof(string));//6
                DataGridViewDT.Columns.Add("HARFTYPE", typeof(string));//7
                DataGridViewDT.Columns.Add("LOT_NO", typeof(string));//8
                DataGridViewDT.Columns.Add("CHECK_YN", typeof(string));//9
                DataGridViewDT.Columns.Add("GOOD_QTY", typeof(string));//10
                DataGridViewDT.Columns.Add("HH", typeof(string));//11


                PpCard_Success.Show();
                PpCard_Success.Visible = false;
                listBox1.Items.Add("--------------  00642791 ---------------\n");
                listBox1.Items.Add("DATE: 29.06.15\n");
                listBox1.Items.Add("TIME: 09:22\n");
                listBox1.Items.Add("TOOL NR.  :  1\n");
                listBox1.Items.Add("TOOL NAME : OP 1 LHD AIRDUCT\n");
                listBox1.Items.Add("\n");
                listBox1.Items.Add("MODE: RELATIVE WELDING DEPTH\n");
                listBox1.Items.Add("                   REF          ACT\n");
                listBox1.Items.Add("FRICTION TIME      3.0 sec       3.0 sec\n");
                listBox1.Items.Add("JOINING TIME       5.0 sec       2.2 sec\n");
                listBox1.Items.Add("DWELL TIME         3.0 sec       3.0 sec\n");
                listBox1.Items.Add("FRICTION PRESS.     78 bar        71 bar\n");
                listBox1.Items.Add("JOINING PRESS.      78 bar        76 bar\n");
                listBox1.Items.Add("DWELL PRESS.        78 bar        76 bar\n");
                listBox1.Items.Add("WELDING DEPTH     1.10 mm     + 1.10 mm\n");
                listBox1.Items.Add("AMPLITUDE         3.50 mm       3.50 mm\n");
                listBox1.Items.Add("----------------------------------------\n");
                txt_reading.Focus();

                worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message+ " : (1)");
            }

        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                //작업표준서 가져오기
                timer_now.Stop();
                imageSlider1.Images.Clear();
                string ftp_str = AdjustDir(txt_IT_SCODE.Text.Trim());

                ftpUtil.FTPDirectioryCheck(ftp_str,"OUT");

                work_image = ftpUtil.get_file_list(ftp_str, "OUT");

                for (int i = 0; i < work_image.Length; i++)
                {
                    if (work_image[i] != null)
                        imageSlider1.Images.Add(work_image[i]);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                timer_now.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        public string AdjustDir(string path)
        {
            return ((path.StartsWith("/")) ? "" : "/").ToString() + path;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Swing2.InventoryStop();
            Swing2.ConnectionClose();
            parentForm.Visible = true;
        }

        public void saveDataRow_PP(string str)
        {

            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {
                        //dataGridView1.Rows[0].Cells["PP_SERNO"].Value.ToString();
                        //dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString();
                        //lot_no = dataGridView1.Rows[0].Cells["LOT_NO"].Value.ToString();

                        //dr["SITE_CODE"] = strTag[0];
                        //dr["WC_CODE"] = strTag[1];
                        rec_card_no = strTag[2].Trim();
                        rec_it_scode = strTag[3].Trim();
                        f_mm_sqty = float.Parse(strTag[4]);
                    }
                }
            }

        }
        //PP카드 dataRow 저장
        public DataRow saveDataRow_PP2(string str)
        {
            DataRow dr = null;
            string[] strTags = str.Split('*');
            if (strTags.Length == 2)
            {
                if (strTags[0].Equals("PP"))
                {
                    string[] strTag = strTags[1].Split('/');

                    if (strTag.Length == 5)
                    {
                        //if (str_wc_code.Trim() == strTag[1].Trim())
                        //{
                        if (btn_mix_toggle.Text.Trim().Equals("Off"))
                        {

                            dr = Tag_DT_PP.NewRow();
                            dr["SITE_CODE"] = strTag[0];
                            dr["WC_CODE"] = strTag[1];
                            dr["CARD_NO"] = strTag[2];
                            dr["IT_SCODE"] = strTag[3];
                            if (btn_carrier_sqty.Visible)
                            {
                                dr["SIZE"] = btn_carrier_sqty.Text.Trim();
                            }
                            else
                            {
                                dr["SIZE"] = strTag[4];
                            }

                            if (dr["SIZE"].ToString().Trim().Equals("0"))
                            {
                                Sqty_popup Sqty_popup = new Sqty_popup();
                                Sqty_popup.sqty = "0";
                                if (Sqty_popup.ShowDialog() == DialogResult.OK)
                                {
                                    dr["SIZE"] = Sqty_popup.sqty;
                                }

                            }
                            if (PpCard_reading_chk)
                            {
                                if (pp_card_check2(strTag[3], strTag[2], strTag[1]))
                                {

                                    if (btn_on_off.Text.Trim().Equals("On"))//자동일때 실적 바로 등록
                                    {
                                        Swing2.InventoryStop();

                                        //foreach (DataRow Po_dr in po_DT.Rows)
                                        //{txt_IT_SCODE
                                        //if (Po_dr["PO_IT_SCODE"].ToString().Trim().Equals(strTag[3]))
                                        if (txt_IT_SCODE.Text.Trim().Equals(strTag[3]))
                                        {

                                            string pd_lot_no = work_input_save_virtual(strTag[0].ToString(), mo_snumb, strTag[3].ToString(), str_wc_code.Trim()
                                                , dr["SIZE"].ToString(), txt_datetime.Text, strTag[2].ToString(), out_carrier_no, "Y");
                                            
                                            dr["CHECK_YN"] = "생산완료";
                                            PpCard_reading.TopLevel = false;
                                            PpCard_reading.TopMost = false;
                                            PpCard_reading.Visible = false;
                                            PpCard_reading_chk = false;
                                            Swing2.TagListClear();
                                            dsmListView_ivt2.Items.Clear();
                                            ddc_ivt2.DigitText = "00000";
                                            Tag_DT_PP.Rows.Clear();
                                            Tag_DT_DC.Rows.Clear();
                                            out_carrier_no = "";
                                            txtOut_carrier_no.Text = "";
                                            r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                                            txt_datetime.Text = r_start;
                                            set_현재고_최대생산가능수량();
                                            

                                                PpCard_Success10.TopLevel = true;
                                                PpCard_Success10.TopMost = true;
                                                PpCard_Success10.Visible = true;
                                                PpCard_Success10.set_text(strTag[2].ToString().Trim(), 11);

                                            

                                        }
                                        else
                                        {
                                            Swing2.InventoryStop();
                                            Swing2.TagListClear();
                                            dsmListView_ivt2.Items.Clear();
                                            ddc_ivt2.DigitText = "00000";
                                            Tag_DT_PP.Rows.Clear();
                                            PpCard_reading.TopLevel = false;
                                            PpCard_reading.TopMost = false;
                                            PpCard_reading.Visible = false;
                                            PpCard_reading_chk = false;
                                            MessageBox.Show("PP카드의 품목과 \n선택된 생산계획의 품목이 틀립니다.");
                                        }
                                        //}

                                    }
                                    else
                                    {
                                        dr["CHECK_YN"] = "생산가능";
                                    }
                                }
                                else
                                {
                                    if (btn_on_off.Text.Trim().Equals("On"))
                                    {
                                        MessageBox.Show("생산완료된 카드 입니다.");
                                    }
                                    dr["CHECK_YN"] = "생산완료";
                                }
                            }
                            else
                            {
                                Swing2.InventoryStop();
                                Swing2.TagListClear();
                                dsmListView_ivt2.Items.Clear();
                                ddc_ivt2.DigitText = "00000";
                            }
                        }
                        else if (btn_mix_toggle.Text.Trim().Equals("On"))
                        {
                            dr = Tag_DT_PP_MIX.NewRow();
                            dr["SITE_CODE"] = strTag[0];
                            dr["WC_CODE"] = strTag[1];
                            dr["CARD_NO"] = strTag[2];
                            dr["IT_SCODE"] = strTag[3];
                            dr["SIZE"] = 42;
                        }
                    }
                }
            }
            txt_reading.Focus();
            return dr;
        }

        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check2(string prdt_item, string pp_serno,string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + pp_serno + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIn_Carrier_no.Text = "";
            txtIt_scode.Text = "";
            txtCard_no.Text = "";
            in_carrier_no = "";
            rec_it_scode = "";
            rec_card_no = "";
            sel_carrier_no="";
            sel_it_scode ="";
            sel_card_no = "";
            //Swing.InventoryStop();
            //Swing.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";

            //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
            //Swing.InventoryStart(); 
        }

        private void btn_po_release_Click(object sender, EventArgs e)
        {
            
            work_plan_popup_paint();
            //po_release_popup();
        }
        string[] sqty_arry = new string[2];
        

        public void work_plan_popup_paint()
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                //if (Swing2.IsOpen)
                if (true)
                {

                    Po_release_search_paint_dev Work_plan_search = new Po_release_search_paint_dev();
                    Work_plan_search.wc_group = wc_group;
                    Work_plan_search.wc_code = str_wc_code;
                    if (Work_plan_search.ShowDialog() == DialogResult.OK)
                    {
                        formClear2();
                        txt_IT_SCODE.Text = Work_plan_search.IT_SCODE_str;
                        //작업표준서 들고오기
                        worker.RunWorkerAsync();
                        txt_IT_SNAME.Text = Work_plan_search.IT_SNAME_str;
                        txt_IT_MODEL.Text = Work_plan_search.IT_MODEL_str;
                        txt_MO_SQUTY.Text = Work_plan_search.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        PP_SITE_CODE_str = Work_plan_search.SITE_CODE_str;
                        me_scode = Work_plan_search.ME_SCODE_str;
                        max_sqty = Work_plan_search.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        carrier_yn = "Y";

                        mo_snumb = Work_plan_search.MO_SNUMB_str;
                        btn_carrier_sqty.Text = Work_plan_search.IT_PKQTY_str;

                        //양품수량 불량수량 들고오기
                        GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        txt_good_qty.Text = GET_DATA.good_qty;
                        txt_fail_qty.Text = GET_DATA.fail_qty;

                        sqty_cnt.Text = GET_DATA.get_weight_cnt_NEW_0(mo_snumb, str_wc_code);

                        DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                        dataGridView1.DataSource = DataGridViewDT;
                        dataGridView1.CurrentCell = null;
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no = "";
                        rec_it_scode = "";
                        rec_card_no = "";
                        sel_carrier_no = "";
                        sel_it_scode = "";
                        sel_card_no = "";
                        Swing2.InventoryStop();
                        Swing2.TagListClear();
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        set_현재고_최대생산가능수량();
                        shiftwork();//작업유형 가져오기
                        sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString());
                        txt_day_sqty.Text = sqty_arry[0];
                        txt_night_sqty.Text = sqty_arry[1];
                        txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                        set_worker_info();//작업자 팝업
                        SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");
                        if (dataGridView1.Rows.Count > 0)
                        {
                            //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                            //Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);

                            //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();


                        }
                        Swing2.InventoryStop();
                        //84711a7100wkl
                        timer_re_carrier.Stop();
                        timer_re_carrier.Start();


                        lueWc_code.Enabled = false;

                    }

                }
                txt_reading.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }/*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }


        //클리어
        public void formClear()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_fail_qty.Text = null;
            txtIn_Carrier_no.Text = null;
            in_carrier_no = "";
            mo_snumb = "";
            Swing2.InventoryStop();
            Swing2.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

        }
        public void formClear2()
        {
            mo_snumb = null;
            txt_datetime.Text = null;
            txt_IT_SCODE.Text = null;
            txt_IT_SNAME.Text = null;
            txt_IT_MODEL.Text = null;
            txt_MO_SQUTY.Text = null;
            txt_good_qty.Text = null;
            txt_fail_qty.Text = null;
            txtOut_carrier_no.Text = null;
            out_carrier_no = "";
            mo_snumb = "";
            Swing2.InventoryStop();
            Swing2.TagListClear();
            dsmListView_ivt2.Items.Clear();
            ddc_ivt2.DigitText = "00000";
            PP_WC_CODE_str = "";
            PP_IT_SCODE_str = "";
            CARD_NO_str = "";
            PP_SIZE_str = "";
            PP_SITE_CODE_str = "";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();

        }
        //비가동 등록
        private void btn_dt_input_Click(object sender, EventArgs e)
        {
            try
            {
                Dt_Input Dt_Input = new Dt_Input();
                Dt_Input.wc_group = wc_group;
                //PP_SITE_CODE_str,mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime,str_wc_code
                Dt_Input.PP_SITE_CODE_str = PP_SITE_CODE_str;
                Dt_Input.mo_snumb = mo_snumb;
                Dt_Input.str_wc_code = str_wc_code;
                if (Dt_Input.ShowDialog() == DialogResult.OK)
                {
                    SUB_SAVE.dt_input_data(PP_SITE_CODE_str, mo_snumb, Dt_Input.dt_code, Dt_Input.dt_stime, Dt_Input.dt_etime, str_wc_code);

                }
            }
            catch { }
            finally { txt_reading.Focus(); }
        }

        


        private void btn_fail_input_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mo_snumb))
                {
                    MessageBox.Show("생산계획을 선택해주세요");
                }
                else
                {
                    this.Cursor = Cursors.WaitCursor;
                    Fail_Input Fail_Input = new Fail_Input();
                    Fail_Input.wc_group = wc_group;
                    if (Fail_Input.ShowDialog() == DialogResult.OK)
                    {

                        //불량 등록
                        int vw_snumb = GET_DATA.maxIN_SERNO(mo_snumb, str_wc_code);
                        if (SUB_SAVE.fail_input_data(PP_SITE_CODE_str, Fail_Input.lost_code, mo_snumb, vw_snumb, str_wc_code))
                        {
                            txt_fail_qty.Text = (float.Parse(txt_fail_qty.Text) + 1).ToString();
                        }
                    }
                    this.Cursor = Cursors.Default;
                    txt_reading.Focus();
                }
            }
            catch { }
            finally { txt_reading.Focus(); }
        }


        private void btn_pp_card_search_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("작업지시를 선택해주세요");
                txt_reading.Focus();
                return;
            }
            if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
            {
                MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                txt_reading.Focus();
                return;
            }
            Swing2.TagListClear();
            dsmListView_ivt2.Items.Clear();
            ddc_ivt2.DigitText = "00000";
            Tag_DT_PP.Rows.Clear();
            Tag_DT_DC.Rows.Clear();
            Tag_DT_PP_MIX.Rows.Clear();            
            
            
            //Swing.InventoryStart();
            Swing2.InventoryStart();
            
            Swing2.TagListClear();
            dsmListView_ivt.Items.Clear();
            ddc_ivt.DigitText = "00000";

            Tag_DT_PP.Rows.Clear();
            PpCard_reading.TopLevel = true;
            PpCard_reading.TopMost = true;
            PpCard_reading.Visible = true;
            PpCard_reading_chk = true;
            
            
        }
        bool PpCard_reading_chk = false;

        public string transfer = "NO";
        //가실적 등록 
        

        public string work_input_save_virtual(string site_code, string rsrv_no, string prdt_item, string wc_code, string good_qty, string r_start, string card_no, string carrier_no, string carrier_yn)
        {
            string strCon;
            string lot_no = "";
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            
            SqlDataReader reader = null;

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_WORK_INPUT_VIRTUAL_PM", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);

            //작업지시번호
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //품목코드
            cmd.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);

            //작업장코드
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            
            //양품
            cmd.Parameters.AddWithValue("@GOOD_QTY", float.Parse(good_qty));

            //불량
            cmd.Parameters.AddWithValue("@FAIL_QTY", 0);

            //시작시간
            cmd.Parameters.AddWithValue("@R_START", r_start);

            //pp 시리얼번호
            cmd.Parameters.AddWithValue("@CARD_NO", card_no);

            //대차 번호
            cmd.Parameters.AddWithValue("@CARRIER_NO", "");

            cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));

            cmd.Parameters.AddWithValue("@CARRIER_YN", carrier_yn);

            cmd.Parameters.AddWithValue("@WORKER", str_worker);

            //cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);

            //cmd.Parameters.AddWithValue("@QR_CODE", createQrCode(pm_snumb));

            //커넥션오픈 실행
            conn.Open();

            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lot_no = reader["PD_LOT_NO"].ToString();
                }
                //양품수량 증가

                txt_good_qty.Text = "" + (int.Parse(txt_good_qty.Text) + int.Parse(good_qty));
                string hh = DateTime.Now.Hour.ToString();
                string mm = DateTime.Now.Minute.ToString();
                if (hh.Length == 1)
                {
                    hh = "0" + hh;
                }
                if (mm.Length == 1)
                {
                    mm = "0" + mm;
                }
                string time = hh + mm;


                if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                {
                    txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(good_qty));
                    sqty_cnt.Text = "0";
                }
                else
                {
                    txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(good_qty));
                    sqty_cnt.Text = "0";
                }
                
                //공정이동표 출력
                //print_process_mov(pm_snumb);
            }

            catch (Exception e)
            {
                
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                reader.Close();
                reader.Dispose();
                conn.Close();
            }
            return lot_no;
            
        }
        public void Manually_Weight_save()
        {
            if (string.IsNullOrWhiteSpace(mo_snumb))
            {
                MessageBox.Show("작업지시를 선택해주세요");
                return;
            }
            if (CHECK_FUNC.po_release_new_check(mo_snumb).Equals("Y"))
            {
                MessageBox.Show("종료된 작업지시 입니다. \n작업지시를 다시 선택해 주세요");
                return;
            }
            //용기 수량 체크

            //if (txt_weight_cnt.Text.Trim() == btn_carrier_sqty.Text.Trim())
            //{
            //    MessageBox.Show("대차에 수량이 가득 찼습니다. \n 수동으로 수량을 입력 할 수 없습니다.");
            //    return;
            //}

            //else
            //{
            Injection_Sqty_Popup Injection_Sqty_Popup = new Injection_Sqty_Popup();
            Injection_Sqty_Popup.base_sqty = btn_carrier_sqty.Text.Trim();
            Injection_Sqty_Popup.sqty = sqty_cnt.Text.Trim();
            if (Injection_Sqty_Popup.ShowDialog() == DialogResult.OK)
            {
                string gubn = "Y";
                int max_num = int.Parse(Injection_Sqty_Popup.sqty.Trim());

                //가실적 번호 무조건 0으로 저장 나중에 실적을 등록 하게 되면 가실적 등록 번호를 수량만큼 업데이트
                int vw_snumb = 0;
                for (int i = 0; i < max_num; i++)
                {
                    
                    if (gubn.Equals("Y"))
                    {
                        SqlTransaction tran = null;
                        string strCon;
                        strCon = DK_Tablet.Properties.Settings.Default.SQL_DKQT;

                        SqlConnection conn = new SqlConnection(strCon);
                        conn.Open();
                        tran = conn.BeginTransaction();
                        SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_SAVE_WEIGHT", conn);
                        da.SelectCommand.Transaction = tran;

                        da.SelectCommand.CommandType = CommandType.StoredProcedure;

                        da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", "D001");
                        da.SelectCommand.Parameters.AddWithValue("@RSRV_NO", mo_snumb);
                        da.SelectCommand.Parameters.AddWithValue("@PRDT_ITEM", txt_IT_SCODE.Text.Trim());
                        da.SelectCommand.Parameters.AddWithValue("@WC_CODE", str_wc_code);
                        da.SelectCommand.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);
                        da.SelectCommand.Parameters.AddWithValue("@IN_WEIGHT", 0);
                        da.SelectCommand.Parameters.AddWithValue("@CHECK_YN", gubn);


                        DataSet ds = new DataSet();
                        try
                        {
                            da.Fill(ds, "WEIGHT_CHART");
                            if (gubn.Trim().Equals("Y"))
                            {
                                try
                                {

                                    tran.Commit();
                                    sqty_cnt.Text = (int.Parse(sqty_cnt.Text.Trim()) + 1).ToString();


                                }
                                catch (Exception ex)
                                {
                                    tran.Rollback();
                                    conn.Close();

                                    MessageBox.Show(ex.StackTrace + ", " + ex.Message);
                                    return;
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + " : (0)");
                        }
                        finally
                        {
                            conn.Close();
                        }

                    }

                }

                if (int.Parse(sqty_cnt.Text.Trim()) >= int.Parse(btn_carrier_sqty.Text))
                {
                    //대차수량
                    string pd_lot_no = work_input_save_virtual("D001", mo_snumb, txt_IT_SCODE.Text.Trim(), str_wc_code.Trim()
                            , btn_carrier_sqty.Text.Trim(), txt_datetime.Text, "", "", "Y");


                    r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                    txt_datetime.Text = r_start;
                    set_현재고_최대생산가능수량();

                    
                        // 식별표 출력
                        //it_chart_print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no);
                        Func_Mobis_Print.Print(txt_IT_SCODE.Text.Trim(), int.Parse(btn_carrier_sqty.Text.Trim()), pd_lot_no, 2);
                        PpCard_Success10.TopLevel = true;
                        PpCard_Success10.TopMost = true;
                        PpCard_Success10.Visible = true;
                        PpCard_Success10.set_text2(11);
                    

                    /*
                    string hh = DateTime.Now.Hour.ToString();
                    string mm = DateTime.Now.Minute.ToString();
                    if (hh.Length == 1)
                    {
                        hh = "0" + hh;
                    }
                    if (mm.Length == 1)
                    {
                        mm = "0" + mm;
                    }
                    string time = hh + mm;


                    if (int.Parse(time) >= 1850 || int.Parse(time) < 750)
                    {
                        txt_night_sqty.Text = "" + (int.Parse(txt_night_sqty.Text) + int.Parse(sqty_cnt.Text));
                        sqty_cnt.Text = "0";
                    }
                    else
                    {
                        txt_day_sqty.Text = "" + (int.Parse(txt_day_sqty.Text) + int.Parse(sqty_cnt.Text));
                        sqty_cnt.Text = "0";
                    }*/
                }
            }
        }
        private byte[] createQrCode(string data)
        {
            byte[] qr_code = null;
            Image qr_code_img;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            try
            {
                qrCodeEncoder.QRCodeScale = Convert.ToInt16(4);
                qrCodeEncoder.QRCodeVersion = 4;
                qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                qr_code_img = qrCodeEncoder.Encode(data);

                ImageConverter converter = new ImageConverter();
                qr_code = (byte[])converter.ConvertTo(qr_code_img, typeof(byte[]));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return qr_code;
        }
        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            
            regKey.SetValue("WC_CODE", str_wc_code);
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            
        }

        private void SubMain_Shown(object sender, EventArgs e)
        {
            parentForm.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_po_start_Tick(object sender, EventArgs e)
        {
            //Swing.InventoryStop();
            Swing2.InventoryStop();
            timer_po_start.Stop();
        }

        private void timer_pp_start_Tick(object sender, EventArgs e)
        {
            //Swing.InventoryStop();
            Swing2.InventoryStop();
            timer_pp_start.Stop();
        }

        private void Btn_Auto_Connection_Click(object sender, EventArgs e)
        {
            
            string[] arryPort_power_out = null;
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.Trim()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                arryPort_power_out = GET_DATA.get_connection_port(lueWc_code.GetColumnValue("WC_CODE").ToString(), "OUT");
                //Swing2.ConnectionOpen(comboBox_ports2.SelectedItem.ToString());
                Swing2.ConnectionOpen(arryPort_power_out[0], 25);

                if (Swing2.IsOpen)
                {
                    
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Com-Port Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }

            comboBox_ports2.Enabled = !Swing2.IsOpen;
            button_com_open2.Enabled = !Swing2.IsOpen;
            button_com_close2.Enabled = Swing2.IsOpen;
            if (Swing2.IsOpen)
            {
                
                //Properties.Settings.Default.ComPortName = Swing.PortName;
                //Properties.Settings.Default.Save();
                //Swing.InventoryStop();
                //Swing.TagListClear();
                dsmListView_ivt.Items.Clear();
                ddc_ivt.DigitText = "00000";
                txtIn_Carrier_no.Text = "";
                txtIt_scode.Text = "";
                txtCard_no.Text = "";
                in_carrier_no = "";
                rec_it_scode = "";
                rec_card_no = "";
                //Swing.SetRFPower(30 - int.Parse(arryPort_power_in[1]));
                //Swing.ReportAllInformation();

                
                Properties.Settings.Default.ComPortName = Swing2.PortName;
                Properties.Settings.Default.Save();
                Swing2.InventoryStop();
                Swing2.TagListClear();
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
                txtOut_carrier_no.Text = "";
                txtIt_scode.Text = "";
                txtCard_no.Text = "";
                in_carrier_no = "";
                rec_it_scode = "";
                rec_card_no = "";
                
                Swing2.SetRFPower(30 - int.Parse(arryPort_power_out[1]));
                Swing2.ReportAllInformation();
                work_plan_popup_paint();
                //po_release_popup();
            }
            else
            {

            }
        }

        private void btn_meterial_search_Click(object sender, EventArgs e)
        {
            try
            {
                metarial_search metarial_search = new metarial_search();
                metarial_search.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();

                if (metarial_search.ShowDialog() == DialogResult.OK)
                {
                }
            }
            catch { }
            finally { txt_reading.Focus(); }
            
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                dataGridView1.DataSource = DataGridViewDT;
                dataGridView1.CurrentCell = null;
                if (dataGridView1.Rows.Count == 0)
                {
                    txtNext_Carrier_no.Text = "";
                }
                else
                {
                    //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                    sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                    sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                    //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                }
            }
            txt_reading.Focus();
        }

        private void timer_now_Tick(object sender, EventArgs e)
        {

        }

        private void timer_re_carrier_Tick(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                dataGridView1.DataSource = DataGridViewDT;
                dataGridView1.CurrentCell = null;
                if (dataGridView1.Rows.Count == 0)
                {
                    txtNext_Carrier_no.Text = "";
                }
                else
                {
                    //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                    sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                    sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                    //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();
                }
            }
        }

        public void set_현재고_최대생산가능수량()
        {
            lbl_now_sqty.Text = GET_DATA.get_now_sqty(txt_IT_SCODE.Text.Trim());
            //txt_MO_SQUTY.Text = (int.Parse(max_sqty) - int.Parse(lbl_now_sqty.Text)).ToString();
        }

        private void txtOut_carrier_no_Click(object sender, EventArgs e)
        {
            DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
            txtOut_carrier_no.Text = "";
            out_carrier_no = "";
            txtIt_scode.Text = "";
            txtCard_no.Text = "";
            rec_it_scode = "";
            rec_card_no = "";
            Tag_DT_DC.Clear();
            Tag_DT_PP.Clear();
            
                Swing2.InventoryStop();
                Swing2.TagListClear();
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
                Swing2.InventoryStart();
                timer_pp_start.Start();

                
        }

        private void btn_carrier_sqty_Click(object sender, EventArgs e)
        {
            KeyPad KeyPad = new KeyPad();
            if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                btn_carrier_sqty.Text = KeyPad.txt_value;
            }
            /*
            if (btn_carrier_sqty.Text.Trim().Equals("32"))
            {
                btn_carrier_sqty.Text = "40";
            }
            else
            {
                btn_carrier_sqty.Text = "32";
            }*/
        }


        //작업유형 가져오기
        public void shiftwork()
        {
            string[] shiftwork = CHECK_FUNC.server_get_shiftwork();
            sw_code = shiftwork[0];
            txtDayAndNight.Text = shiftwork[1];
        }

        DataTable worker_Dt = new DataTable();
        //작업자 불러오기
        public void set_worker_info()
        {
            Worker_Popup Worker_Popup = new Worker_Popup();
            //Worker_Popup.sw_code = sw_code;
            Worker_Popup.str_wc_group = wc_group;
            Worker_Popup.select_worker_dt = select_worker_dt;

            if (Worker_Popup.ShowDialog() == DialogResult.OK)
            {
                select_worker_dt = null;
                select_worker_dt = new DataTable();
                select_worker_dt.Columns.Add("SERNO", typeof(string));
                select_worker_dt.Columns.Add("WCR_CODE", typeof(string));
                select_worker_dt.Columns.Add("WCR_NAME", typeof(string));
                select_worker_dt.Columns.Add("CHK", typeof(string));
                select_worker_dt = Worker_Popup.select_worker_dt;

                //작업 화면에 표시 해주기 위한 변수
                if (select_worker_dt.Rows.Count > 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString() + " 외 " + (select_worker_dt.Rows.Count - 1).ToString() + "명";
                }
                else if (select_worker_dt.Rows.Count == 1)
                {
                    btn_worker_info.Text = select_worker_dt.Rows[0]["WCR_NAME"].ToString();
                }
                else
                {
                    btn_worker_info.Text = "";
                }

                //DB에 저장 하기 위한 변수
                str_worker = "";
                DataRow[] drr = select_worker_dt.Select();
                for (int i = 0; i < drr.Length; i++)
                {
                    if (i == drr.Length - 1)
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString();
                    }
                    else
                    {
                        str_worker += drr[i]["WCR_NAME"].ToString() + ",";
                    }
                }

            }
        }

        private void btn_worker_info_Click(object sender, EventArgs e)
        {
            set_worker_info();
        }

        private void btn_on_off_Click(object sender, EventArgs e)
        {
            if (btn_on_off.Text.Trim().Equals("On"))
            {
                btn_on_off.Text = "Off";
            }
            else
            {
                btn_on_off.Text = "On";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }
        
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
            {
                MessageBox.Show("생산 계획을 선택해 주세요");
                return;
            }
            Injection_Carrier_KeyPad Injection_Carrier_KeyPad = new Injection_Carrier_KeyPad();
            Injection_Carrier_KeyPad.it_scode = txt_IT_SCODE.Text.Trim();
            Injection_Carrier_KeyPad.wc_group = wc_group;
            if (Injection_Carrier_KeyPad.ShowDialog() == DialogResult.OK)
            {
                transfer = Injection_Carrier_KeyPad.transfer;
                out_carrier_no = Injection_Carrier_KeyPad.txt_value;
                txtOut_carrier_no.Text = out_carrier_no;
                Swing2.InventoryStop();
                Swing2.TagListClear();
                dsmListView_ivt2.Items.Clear();
                ddc_ivt2.DigitText = "00000";
            }
        }

        private void btn_fail_search_Click(object sender, EventArgs e)
        {
            fail_search_popup fail_search_popup = new fail_search_popup();
            fail_search_popup.wc_group = wc_group;
            fail_search_popup.wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            fail_search_popup.wc_name = lueWc_code.GetColumnValue("WC_NAME").ToString();
            fail_search_popup.Show();
        }

        private void btn_day_work_Click(object sender, EventArgs e)
        {
            try { 
            Day_Night_Sqty_Popup Day_Night_Sqty_Popup = new Day_Night_Sqty_Popup();
            Day_Night_Sqty_Popup.wc_code = str_wc_code;
            Day_Night_Sqty_Popup.Show();
            }
            catch { }
            finally { txt_reading.Focus(); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //PpCard_Success10
            PpCard_Success10.TopLevel = true;
            PpCard_Success10.TopMost = true;
            PpCard_Success10.Visible = true;
            PpCard_Success10.set_text("P001",11);
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            PpCard_reading.TopLevel = false;
            PpCard_reading.TopMost = false;
            PpCard_reading.Visible = false;
            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            timer1.Stop();
        }

        private void btn_mix_toggle_Click(object sender, EventArgs e)
        {
            if (btn_mix_toggle.Text.Trim().Equals("Off"))
            {
                btn_mix_toggle.Text = "On";
            }
            else
            {
                btn_mix_toggle.Text = "Off";
            }
        }

        private void btn_po_release_Insert_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                if (true)
                {

                    Po_release_Insert_dev Po_release_Insert_dev = new Po_release_Insert_dev();
                    Po_release_Insert_dev.wc_group = wc_group;
                    Po_release_Insert_dev.wc_code = str_wc_code;
                    if (Po_release_Insert_dev.ShowDialog() == DialogResult.OK)
                    {
                        formClear2();
                        txt_IT_SCODE.Text = Po_release_Insert_dev.IT_SCODE_str;
                        txt_IT_SNAME.Text = Po_release_Insert_dev.IT_SNAME_str;
                        txt_IT_MODEL.Text = Po_release_Insert_dev.IT_MODEL_str;
                        txt_MO_SQUTY.Text = Po_release_Insert_dev.MO_SQUTY_str;
                        //str_wc_code = Work_plan_search.WC_CODE_str;
                        mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                        PP_SITE_CODE_str = Po_release_Insert_dev.SITE_CODE_str;
                        me_scode = Po_release_Insert_dev.ME_SCODE_str;
                        max_sqty = Po_release_Insert_dev.MAX_SQTY_str;
                        //txt_MO_SQUTY.Text = max_sqty;
                        //시작시간 서버에서 시간 들고오기(Format : 2014-01-01 10:30)
                        r_start = CHECK_FUNC.server_get_datetime(mo_snumb, str_wc_code);
                        txt_datetime.Text = r_start;
                        carrier_yn = "Y";

                        mo_snumb = Po_release_Insert_dev.MO_SNUMB_str;
                        btn_carrier_sqty.Text = Po_release_Insert_dev.IT_PKQTY_str;

                        //양품수량 불량수량 들고오기
                        GET_DATA.get_good_fail(mo_snumb, str_wc_code);
                        txt_good_qty.Text = GET_DATA.good_qty;
                        txt_fail_qty.Text = GET_DATA.fail_qty;

                        DataGridViewDT = GET_DATA.getDataGridView(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString().Trim());
                        dataGridView1.DataSource = DataGridViewDT;
                        dataGridView1.CurrentCell = null;
                        txtIn_Carrier_no.Text = "";
                        txtIt_scode.Text = "";
                        txtCard_no.Text = "";
                        in_carrier_no = "";
                        rec_it_scode = "";
                        rec_card_no = "";
                        sel_carrier_no = "";
                        sel_it_scode = "";
                        sel_card_no = "";
                        Swing2.InventoryStop();
                        Swing2.TagListClear();
                        dsmListView_ivt.Items.Clear();
                        ddc_ivt.DigitText = "00000";
                        set_현재고_최대생산가능수량();
                        shiftwork();//작업유형 가져오기
                        sqty_arry = GET_DATA.get_day_night_sqty(txt_IT_SCODE.Text.Trim(), lueWc_code.GetColumnValue("WC_CODE").ToString());
                        txt_day_sqty.Text = sqty_arry[0];
                        txt_night_sqty.Text = sqty_arry[1];
                        txt_good_qty.Text = (int.Parse(sqty_arry[0]) + int.Parse(sqty_arry[1])).ToString();
                        set_worker_info();//작업자 팝업
                        SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "1");
                        if (dataGridView1.Rows.Count > 0)
                        {
                            //sel_carrier_no = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString().Trim();//대차코드
                            sel_it_scode = dataGridView1.Rows[0].Cells["PRDT_ITEM"].Value.ToString().Trim();//품목코드
                            sel_card_no = dataGridView1.Rows[0].Cells["CARD_NO"].Value.ToString().Trim();//PP 카드 번호
                            //Swing.SetContinuous(SwingLibrary.SwingAPI.ContinuousMode.CONTINUOUS);

                            //txtNext_Carrier_no.Text = dataGridView1.Rows[0].Cells["CARRIER_NO"].Value.ToString();


                        }
                        Swing2.InventoryStop();
                        //84711a7100wkl
                        timer_re_carrier.Stop();
                        timer_re_carrier.Start();

                        lueWc_code.Enabled = false;


                    }

                }
                txt_reading.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 : " + ex.Message);
            }
        }

        private void timer_now_Tick_1(object sender, EventArgs e)
        {
            imageSlider1.SlideNext();
        }

        private void btn_remove_reading_Click(object sender, EventArgs e)
        {
            txt_reading.Text = "";
            txt_reading.Focus();
        }
        Font monoFont;
        private void button6_Click(object sender, EventArgs e)
        {
            TopMost = false;

            Form2 form2 = new Form2();
            form2.ShowDialog();

            TopMost = Settings_1.Option.StayOnTop;
            Font = Settings_1.Option.MonoFont ? monoFont : origFont;
            txt_reading.Focus();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            outputList_ClearAll();
            txt_reading.Focus();
        }
        /// <summary>
        /// clear the output window
        /// </summary>
        void outputList_ClearAll()
        {
            lines.Clear();
            partialLine = null;

            outputList.Items.Clear();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string data = "";
            foreach (string d in listBox1.Items)
            {
                data = data + d + "\n";
                Regex rx = new Regex(@"(--------------)[a-zA-Z0-9\/\.\-\+\: \n]+(----------------------------------------\n)$");
                bool check = rx.IsMatch(data);
                string Friction_time_act = "", Joining_time_act = "", Dwell_time_act = "", Welding_depth_act = "";
                double Friction_time_act_D = 0, Joining_time_act_D = 0, Dwell_time_act_D = 0, Welding_depth_act_D = 0;
                if (!check)
                {

                    //MessageBox.Show(data);
                }
                else
                {
                    string[] arryData = data.Substring(data.IndexOf("FRICTION TIME"), data.Length - data.IndexOf("FRICTION TIME")).Split('\n');
                    foreach (string tt in arryData)
                    {
                        if (tt.IndexOf("FRICTION TIME") != -1)
                        {
                            Friction_time_act = tt;
                        }
                        else if (tt.IndexOf("JOINING TIME") != -1)
                        {
                            Joining_time_act = tt;
                        }
                        else if (tt.IndexOf("DWELL TIME") != -1)
                        {
                            Dwell_time_act = tt;
                        }
                        else if (tt.IndexOf("WELDING DEPTH") != -1)
                        {
                            Welding_depth_act = tt;
                        }
                    }
                    string[] Friction_time_act_arry = Friction_time_act.Split(' ');
                    string[] Joining_time_act_arry = Joining_time_act.Split(' ');
                    string[] Dwell_time_act_arry = Dwell_time_act.Split(' ');
                    string[] Welding_depth_act_arry = Welding_depth_act.Split(' ');

                    Friction_time_act_D = Convert.ToDouble(Friction_time_act_arry[Friction_time_act_arry.Length - 2]);
                    Joining_time_act_D = Convert.ToDouble(Joining_time_act_arry[Joining_time_act_arry.Length - 2]);
                    Dwell_time_act_D = Convert.ToDouble(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]);
                    Welding_depth_act_D = Convert.ToDouble(Welding_depth_act_arry[Welding_depth_act_arry.Length - 2]);

                    MessageBox.Show(Friction_time_act_D.ToString());
                    MessageBox.Show(Joining_time_act_D.ToString());
                    MessageBox.Show(Dwell_time_act_D.ToString());
                    MessageBox.Show(Welding_depth_act_D.ToString());
                    return;

                }

            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            string data = "";
            foreach (string tt in listBox1.Items)
            {
                data = data + tt;
            }
            OnDataReceived(data);
            txt_reading.Focus();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            scrolling = !scrolling;
            outputList_Scroll();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.RestoreDirectory = false;
            dialog.Title = "Select a file";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String text = System.IO.File.ReadAllText(dialog.FileName);

                CommPort com = CommPort.Instance;
                com.Send(text);

                if (Settings_1.Option.LocalEcho)
                {
                    outputList_Add("SendFile " + dialog.FileName + "," +
                        text.Length.ToString() + " byte(s)\n", sentColor);
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string command = textBox3.Text;
            textBox3.Text = "";
            textBox3.Focus();

            if (command.Length > 0)
            {
                CommPort com = CommPort.Instance;
                com.Send(command);

                if (Settings_1.Option.LocalEcho)
                {
                    outputList_Add(command + "\n", sentColor);
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //textBox4
            Regex rx = new Regex(@"(<ACK>)[a-zA-Z0-9\/\.\-\+\:]+(<ETX>)$");
            bool check = rx.IsMatch(textBox4.Text.Trim());

            string str = "";
            str = textBox4.Text.Trim().Replace("<ACK>", "").Replace("<ETX>", "");

            string[] arrStr = str.Split('/');

            foreach (string st in arrStr)
            {
                listBox1.Items.Add(st);
                listBox2.Items.Add(st);
            }
            
            listBox2.Items.Clear();
        }

        private void label47_Click(object sender, EventArgs e)
        {
            if (splitContainer1.Visible)
            {
                splitContainer1.Visible = false;
            }
            else
            {
                splitContainer1.Visible = true;
            }
        }

        private void txt_reading_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {

                string check_yn = barcode_chk(txt_reading.Text.Trim());
                if (check_yn.Equals("N"))
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("레이저 가공 OK", 5);
                    PpCard_Success.BackColor = Color.RoyalBlue;
                    txt_reading.Focus();
                }
                else if (check_yn.Equals("Y"))
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("중복 NG", 5);
                    PpCard_Success.BackColor = Color.Red;
                    txt_reading.Text = "";
                    txt_reading.Focus();
                    return;
                }
                else
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("레이저 미가공 NG", 5);
                    PpCard_Success.BackColor = Color.Red;
                    txt_reading.Text = "";
                    txt_reading.Focus();
                    return;
                }
            }
        }
        private string barcode_chk(string barcode)
        {
            string chk = "";
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("SELECT CHECK_GUBN FROM L_BARCODE_CHK WHERE L_BARCODE=@BARCODE", conn);

            cmd.Parameters.AddWithValue("@BARCODE", barcode);

            conn.Open();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    chk = reader["CHECK_GUBN"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return chk;
        }
        private void txt_reading_Leave(object sender, EventArgs e)
        {
            txt_reading.BackColor = Color.DarkRed;
        }

        private void txt_reading_Enter(object sender, EventArgs e)
        {
            txt_reading.BackColor = Color.SkyBlue;
        }
        public void     log_write()
        {


            //DateTime dt = new DateTime();
            string date = DateTime.Now.ToString("yyyyMMdd");
            string dt_str = DateTime.Now.ToString();
            dt_str = dt_str.Replace(" ", "");
            dt_str = dt_str.Replace(":", "'");
            string desktop = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            DirectoryInfo di = new DirectoryInfo(desktop + @"\RECEIVE_DATA");
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!di.Exists)
            {
                di.Create();
            }
            DirectoryInfo dir = new DirectoryInfo(desktop + @"\RECEIVE_DATA\" + date);
            // 해당 경로에 해당 하는 폴더가 없으면 만들어 줌 
            if (!dir.Exists)
            {
                dir.Create();
            }

            string file_path = desktop + @"\RECEIVE_DATA\" + date + @"\";
            file_path += dt_str + ".txt";

            /*

            if (!File.Exists(file_path))
            {
                File.Create(file_path);
                
            }
            */
            StreamWriter tw = new StreamWriter(file_path);
            foreach (var item in listBox1.Items)
            {
                tw.WriteLine(item.ToString());
            }
            tw.WriteLine("\r\n");
            tw.Close();
            tw.Dispose();

            /*
            if (Settings_1.Option.LogFileName != "")
            {
                Stream myStream = File.Open(Settings_1.Option.LogFileName,
                    FileMode.Append, FileAccess.Write, FileShare.Read);
                
                    StreamWriter myWriter = new StreamWriter(myStream, Encoding.UTF8);
                    myWriter.WriteLine(str);
                    myWriter.Close();
               
            }*/
            /*
            string ftpfile_full_path = ftpUtil.getFtpFile_Full_Path();
            DateTime Date = DateTime.Now;
            // WELD_1 은 융착장비 
            string ftp_str = AdjustDir("WELD_1" + "/" + Date.ToShortDateString());

            ftpUtil.FTPDirectioryCheck(ftp_str);

            string ext = Path.GetExtension(file_path.Substring(file_path.LastIndexOf("\\") + 1)).ToLower().Replace(".", "");
            string filename = file_path.Substring(file_path.LastIndexOf("\\") + 1).Replace(ext, "").Substring(0, file_path.Substring(file_path.LastIndexOf("\\") + 1).Replace(ext, "").Length - 1);

            try
            {
                this.Cursor = Cursors.WaitCursor;
                ftpUtil.Upload(file_path, "WORK_IMAGE" + "/" + "WELD_1" + "/" + Date.ToShortDateString() + "/" + filename + "." + ext);
                //getFileData();
                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR : " + ex.Message);
            }*/


        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            CommPort com = CommPort.Instance;
            if (com.IsOpen)
            {
                com.Close();
            }
            else
            {
                com.Open();
            }
            outputList.Focus();
        }

        private void button13_Click(object sender, EventArgs e)
        {

            Regex rx = new Regex(@"^(--------------)[a-zA-Z0-9\/\.\-\+\: ]+(----------------------------------------\/\/)$");
            //Regex rx = new Regex(@"^[0-9]+\.*[0-9]*$");
            bool check = rx.IsMatch(textBox7.Text);
            MessageBox.Show(check.ToString());
            
            //string[] arry = textBox7.Text.Split('/');
            ////MessageBox.Show(arry[arry.Length - 2]);
            
            
            //    foreach (string tt in arry)
            //    {
            //        if (tt.IndexOf("FRICTION TIME") != -1)
            //        {
            //            Friction_time_act = tt;
            //        }
            //        if (tt.IndexOf("JOINING TIME") != -1)
            //        {
            //            Joining_time_act = tt;
            //        }
            //        if (tt.IndexOf("DWELL TIME") != -1)
            //        {
            //            Dwell_time_act = tt;
            //        }
            //        if (tt.IndexOf("WELDING DEPTH") != -1)
            //        {
            //            Welding_depth_act = tt;
            //        }
            //        if (tt.IndexOf("FRICTION PRESS") != -1)
            //        {
            //            Friction_press_act = tt;
            //        }
            //    }
            //    MessageBox.Show(Friction_time_act.ToString());
            //    MessageBox.Show(Joining_time_act.ToString());
            //    MessageBox.Show(Dwell_time_act.ToString());
            //    MessageBox.Show(Welding_depth_act.ToString());
            //    MessageBox.Show(Friction_press_act.ToString());

            //    string[] Friction_time_act_arry = Friction_time_act.Split(' ');
            //    string[] Joining_time_act_arry = Joining_time_act.Split(' ');
            //    string[] Dwell_time_act_arry = Dwell_time_act.Split(' ');
            //    string[] Welding_depth_act_arry = Welding_depth_act.Split(' ');
            //    string[] Friction_press_act_arry = Friction_press_act.Split(' ');

            //    //Friction_time_act_D = Convert.ToDouble(Friction_time_act_arry[Friction_time_act_arry.Length - 2]);
            //    //Joining_time_act_D = Convert.ToDouble(Joining_time_act_arry[Joining_time_act_arry.Length - 2]);
            //    //Dwell_time_act_D = Convert.ToDouble(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]);
            //    //Welding_depth_act_D = Convert.ToDouble(Welding_depth_act_arry[Welding_depth_act_arry.Length - 2]);

            //    if (Friction_time_act_arry.Length > 2)
            //    {
            //        if (isNumber(Friction_time_act_arry[Friction_time_act_arry.Length - 2]))
            //            Friction_time_act_D = Convert.ToDouble(Friction_time_act_arry[Friction_time_act_arry.Length - 2]);
            //    }

            //    if (Joining_time_act_arry.Length > 2)
            //    {
            //        if (isNumber(Joining_time_act_arry[Joining_time_act_arry.Length - 2]))
            //            Joining_time_act_D = Convert.ToDouble(Joining_time_act_arry[Joining_time_act_arry.Length - 2]);
            //    }

            //    if (Dwell_time_act_arry.Length > 2)
            //    {
            //        if (isNumber(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]))
            //            Dwell_time_act_D = Convert.ToDouble(Dwell_time_act_arry[Dwell_time_act_arry.Length - 2]);
            //    }

            //    if (Welding_depth_act_arry.Length > 2)
            //    {
            //        if (isNumber(Welding_depth_act_arry[Welding_depth_act_arry.Length - 2]))
            //            Welding_depth_act_D = Convert.ToDouble(Welding_depth_act_arry[Welding_depth_act_arry.Length - 2]);
            //    }

            //    if (Friction_press_act_arry.Length > 2)
            //    {
            //        if (isNumber(Friction_press_act_arry[Friction_press_act_arry.Length - 2]))
            //            Friction_press_act_D = Convert.ToDouble(Friction_press_act_arry[Friction_press_act_arry.Length - 2]);
            //    }
            //    MessageBox.Show(Friction_time_act_D.ToString());
            //    MessageBox.Show(Joining_time_act_D.ToString());
            //    MessageBox.Show(Dwell_time_act_D.ToString());
            //    MessageBox.Show(Welding_depth_act_D.ToString());
            //    MessageBox.Show(Friction_press_act_D.ToString());
        }

        private void btn_Weld_search_Click(object sender, EventArgs e)
        {
            Lot_Trace_LY Lot_Trace_LY = new Lot_Trace_LY();
            Lot_Trace_LY.Show();
        }

        private void btn_Manually_sqty_Click(object sender, EventArgs e)
        {
            Manually_Weight_save();
        }

        private void Label_Text_Click(object sender, EventArgs e)
        {
            TextEdit TE = (TextEdit)sender;

            KeyPad KeyPad = new KeyPad();
            if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TE.Text = KeyPad.txt_value;
            }
        }

        private void btn_re_print_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txt_IT_SCODE.Text.Trim()))
                {
                    MessageBox.Show("작업지시를 선택해 주세요");
                    return;
                }
                Func_Mobis_Print.re_Print(txt_IT_SCODE.Text.Trim(), 2);
            }
            catch
            {

            }
            finally
            {

            }
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(str_wc_code.ToString().Trim()))
            {
                MessageBox.Show("작업장을 선택 해 주새요");
                txt_reading.Focus();
                return;
            }
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(parentForm);
            FME_InsResultReg.wc_code = str_wc_code.ToString().Trim();
            FME_InsResultReg.Show();
            txt_reading.Focus();
        }

        private void btn_program_finish_Click(object sender, EventArgs e)
        {
            Finish_Popup Finish_Popup = new Finish_Popup();
            if (Finish_Popup.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Finish_Popup.btn_state.Equals("0"))
                {

                }
                else if (Finish_Popup.btn_state.Equals("1"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    Application.Exit();
                }
                else if (Finish_Popup.btn_state.Equals("2"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, txt_IT_SCODE.Text.Trim(), "", "6");
                    System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");

                }
            }
        }
    }
}
