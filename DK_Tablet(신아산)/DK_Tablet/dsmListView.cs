﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class dsmListView : System.Windows.Forms.ListView
    {
        public dsmListView()
        {
            InitializeComponent();

            //Activate double buffering
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);

            //Enable the OnNotifyMessage event so we get a chance to filter out 
            // Windows messages before they get to the form's WndProc
            this.SetStyle(ControlStyles.EnableNotifyMessage, true);
        }

        public dsmListView(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnNotifyMessage(System.Windows.Forms.Message m)
        {
            //Filter out the WM_ERASEBKGND message
            if (m.Msg != 0x14)
            {
                base.OnNotifyMessage(m);
            }
        }

        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            // 방향 초기화
            for (int i = 0; i < this.Columns.Count; i++)
            {
                this.Columns[i].Text = this.Columns[i].Text.Replace(" △", "");
                this.Columns[i].Text = this.Columns[i].Text.Replace(" ▽", "");
            }

            // DESC
            if (this.Sorting == SortOrder.Ascending || this.Sorting == SortOrder.None)
            {
                this.ListViewItemSorter = new ListViewItemComparer(e.Column, "desc");
                this.Sorting = SortOrder.Descending;
                this.Columns[e.Column].Text = this.Columns[e.Column].Text + " ▽";
            }
            // ASC
            else
            {
                this.ListViewItemSorter = new ListViewItemComparer(e.Column, "asc");
                this.Sorting = SortOrder.Ascending;
                this.Columns[e.Column].Text = this.Columns[e.Column].Text + " △";
            }
            this.Sort();

            // 컬럼 갯수가 변경되는 구조라면 sorter를 null 처리하여야 함
            this.ListViewItemSorter = null;

            base.OnColumnClick(e);
        }
    }

    public class ListViewItemComparer : IComparer
    {
        private int col;
        public string sort = "asc";
        public ListViewItemComparer()
        {
            col = 0;
        }

        /// <summary>
        /// 컬럼과 정렬 기준(asc, desc)을 사용하여 정렬 함.
        /// </summary>
        /// <param name="column">몇 번째 컬럼인지를 나타냄.</param>
        /// <param name="sort">정렬 방법을 나타냄. Ex) asc, desc</param>
        public ListViewItemComparer(int column, string sort)
        {
            col = column;
            this.sort = sort;
        }

        public int Compare(object x, object y)
        {
            if (sort == "asc")
                return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
            else
                return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
        }


        // 숫자의 정확한 sort를 하기 위해서는 
        // http://www.codeproject.com/KB/recipes/csnsort.aspx 사용하자.
        // using ns; 선언 필요
        /*
        public int Compare(object x, object y)
        {
            if (sort == "asc")
                return StringLogicalComparer.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
            else
                return StringLogicalComparer.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
        }
        */
    }
}

