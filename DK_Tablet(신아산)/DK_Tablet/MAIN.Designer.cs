﻿namespace DK_Tablet
{
    partial class MAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_main_injection = new System.Windows.Forms.Button();
            this.btn_main_laser = new System.Windows.Forms.Button();
            this.btn_main_paint = new System.Windows.Forms.Button();
            this.btn_main_assey = new System.Windows.Forms.Button();
            this.btn_main_metarial_move = new System.Windows.Forms.Button();
            this.btn_main_shipment = new System.Windows.Forms.Button();
            this.btn_main_assey_c = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_inspection = new System.Windows.Forms.Button();
            this.btn_main_Instatement = new System.Windows.Forms.Button();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.tableLayoutPanel_main = new System.Windows.Forms.TableLayoutPanel();
            this.btn_shipment = new System.Windows.Forms.Button();
            this.btn_inlogistics = new System.Windows.Forms.Button();
            this.btn_production = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btn_quality = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel_production = new System.Windows.Forms.TableLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_main_assey_c_2 = new System.Windows.Forms.Button();
            this.btn_injection_carrier = new System.Windows.Forms.Button();
            this.btn_main_assey_c_3 = new System.Windows.Forms.Button();
            this.btn_main_laser_hi = new System.Windows.Forms.Button();
            this.btn_injection_7 = new System.Windows.Forms.Button();
            this.btn_main_laser_JA_YB = new System.Windows.Forms.Button();
            this.btn_main_assey_JA_YB = new System.Windows.Forms.Button();
            this.btn_main_injection_Lot = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.tableLayoutPanel_logistics = new System.Windows.Forms.TableLayoutPanel();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tableLayoutPanel_shipment = new System.Windows.Forms.TableLayoutPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.btn_main_site_move = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel_quality = new System.Windows.Forms.TableLayoutPanel();
            this.btn_Loading = new System.Windows.Forms.Button();
            this.btn_FME_InsResult = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.btn_fail = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_back = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel_production.SuspendLayout();
            this.tableLayoutPanel_logistics.SuspendLayout();
            this.tableLayoutPanel_shipment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel_quality.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_main_injection
            // 
            this.btn_main_injection.BackgroundImage = global::DK_Tablet.Properties.Resources.product1;
            this.btn_main_injection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_injection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_injection.FlatAppearance.BorderSize = 0;
            this.btn_main_injection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_injection.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_injection.Location = new System.Drawing.Point(8, 188);
            this.btn_main_injection.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_injection.Name = "btn_main_injection";
            this.btn_main_injection.Size = new System.Drawing.Size(86, 81);
            this.btn_main_injection.TabIndex = 0;
            this.btn_main_injection.Text = "사출";
            this.btn_main_injection.UseVisualStyleBackColor = true;
            this.btn_main_injection.Click += new System.EventHandler(this.btn_main_injection_Click);
            // 
            // btn_main_laser
            // 
            this.btn_main_laser.BackgroundImage = global::DK_Tablet.Properties.Resources.product2;
            this.btn_main_laser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_laser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_laser.FlatAppearance.BorderSize = 0;
            this.btn_main_laser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_laser.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_laser.Location = new System.Drawing.Point(94, 188);
            this.btn_main_laser.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_laser.Name = "btn_main_laser";
            this.btn_main_laser.Size = new System.Drawing.Size(86, 81);
            this.btn_main_laser.TabIndex = 0;
            this.btn_main_laser.Text = "레이저";
            this.btn_main_laser.UseVisualStyleBackColor = true;
            this.btn_main_laser.Click += new System.EventHandler(this.btn_main_laser_Click);
            // 
            // btn_main_paint
            // 
            this.btn_main_paint.BackgroundImage = global::DK_Tablet.Properties.Resources.product4;
            this.btn_main_paint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_paint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_paint.FlatAppearance.BorderSize = 0;
            this.btn_main_paint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_paint.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_paint.Location = new System.Drawing.Point(266, 188);
            this.btn_main_paint.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_paint.Name = "btn_main_paint";
            this.btn_main_paint.Size = new System.Drawing.Size(86, 81);
            this.btn_main_paint.TabIndex = 0;
            this.btn_main_paint.Text = "도장";
            this.btn_main_paint.UseVisualStyleBackColor = true;
            this.btn_main_paint.Click += new System.EventHandler(this.btn_main_paint_Click);
            // 
            // btn_main_assey
            // 
            this.btn_main_assey.BackgroundImage = global::DK_Tablet.Properties.Resources.product3;
            this.btn_main_assey.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_assey.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_assey.FlatAppearance.BorderSize = 0;
            this.btn_main_assey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_assey.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_assey.Location = new System.Drawing.Point(180, 188);
            this.btn_main_assey.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_assey.Name = "btn_main_assey";
            this.btn_main_assey.Size = new System.Drawing.Size(86, 81);
            this.btn_main_assey.TabIndex = 0;
            this.btn_main_assey.Text = "조립";
            this.btn_main_assey.UseVisualStyleBackColor = true;
            this.btn_main_assey.Click += new System.EventHandler(this.btn_main_assey_Click);
            // 
            // btn_main_metarial_move
            // 
            this.btn_main_metarial_move.BackgroundImage = global::DK_Tablet.Properties.Resources.logistics1;
            this.btn_main_metarial_move.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_metarial_move.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_metarial_move.FlatAppearance.BorderSize = 0;
            this.btn_main_metarial_move.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_metarial_move.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_main_metarial_move.Location = new System.Drawing.Point(128, 238);
            this.btn_main_metarial_move.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_main_metarial_move.Name = "btn_main_metarial_move";
            this.btn_main_metarial_move.Size = new System.Drawing.Size(99, 83);
            this.btn_main_metarial_move.TabIndex = 0;
            this.btn_main_metarial_move.Text = "자재수급";
            this.btn_main_metarial_move.UseVisualStyleBackColor = true;
            this.btn_main_metarial_move.Click += new System.EventHandler(this.btn_main_metarial_move_Click);
            // 
            // btn_main_shipment
            // 
            this.btn_main_shipment.BackgroundImage = global::DK_Tablet.Properties.Resources.shipment1;
            this.btn_main_shipment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_shipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_shipment.FlatAppearance.BorderSize = 0;
            this.btn_main_shipment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_shipment.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_main_shipment.Location = new System.Drawing.Point(65, 230);
            this.btn_main_shipment.Margin = new System.Windows.Forms.Padding(45, 30, 45, 30);
            this.btn_main_shipment.Name = "btn_main_shipment";
            this.btn_main_shipment.Size = new System.Drawing.Size(40, 37);
            this.btn_main_shipment.TabIndex = 0;
            this.btn_main_shipment.Text = "출하\r\n(PP 카드)";
            this.btn_main_shipment.UseVisualStyleBackColor = true;
            this.btn_main_shipment.Click += new System.EventHandler(this.btn_main_shipment_Click);
            // 
            // btn_main_assey_c
            // 
            this.btn_main_assey_c.BackgroundImage = global::DK_Tablet.Properties.Resources.product5;
            this.btn_main_assey_c.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_assey_c.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_assey_c.FlatAppearance.BorderSize = 0;
            this.btn_main_assey_c.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_assey_c.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_assey_c.Location = new System.Drawing.Point(352, 188);
            this.btn_main_assey_c.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_assey_c.Name = "btn_main_assey_c";
            this.btn_main_assey_c.Size = new System.Drawing.Size(90, 81);
            this.btn_main_assey_c.TabIndex = 0;
            this.btn_main_assey_c.Text = "완성품";
            this.btn_main_assey_c.UseVisualStyleBackColor = true;
            this.btn_main_assey_c.Click += new System.EventHandler(this.btn_main_assey_c_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::DK_Tablet.Properties.Resources._0ff2;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.Location = new System.Drawing.Point(1226, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 148);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_inspection
            // 
            this.btn_inspection.BackgroundImage = global::DK_Tablet.Properties.Resources.logistics1;
            this.btn_inspection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_inspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_inspection.FlatAppearance.BorderSize = 0;
            this.btn_inspection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_inspection.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_inspection.Location = new System.Drawing.Point(233, 238);
            this.btn_inspection.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(99, 83);
            this.btn_inspection.TabIndex = 0;
            this.btn_inspection.Text = "재고실사";
            this.btn_inspection.UseVisualStyleBackColor = true;
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // btn_main_Instatement
            // 
            this.btn_main_Instatement.BackgroundImage = global::DK_Tablet.Properties.Resources.logistics11;
            this.btn_main_Instatement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_Instatement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_Instatement.FlatAppearance.BorderSize = 0;
            this.btn_main_Instatement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_Instatement.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_main_Instatement.Location = new System.Drawing.Point(23, 238);
            this.btn_main_Instatement.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_main_Instatement.Name = "btn_main_Instatement";
            this.btn_main_Instatement.Size = new System.Drawing.Size(99, 83);
            this.btn_main_Instatement.TabIndex = 0;
            this.btn_main_Instatement.Text = "입고";
            this.btn_main_Instatement.UseVisualStyleBackColor = true;
            this.btn_main_Instatement.Click += new System.EventHandler(this.btn_main_Instatement_Click);
            // 
            // tableLayoutPanel_main
            // 
            this.tableLayoutPanel_main.ColumnCount = 4;
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel_main.Controls.Add(this.btn_shipment, 2, 1);
            this.tableLayoutPanel_main.Controls.Add(this.btn_inlogistics, 1, 1);
            this.tableLayoutPanel_main.Controls.Add(this.btn_production, 0, 1);
            this.tableLayoutPanel_main.Controls.Add(this.button5, 0, 0);
            this.tableLayoutPanel_main.Controls.Add(this.btn_quality, 3, 1);
            this.tableLayoutPanel_main.Location = new System.Drawing.Point(31, 26);
            this.tableLayoutPanel_main.Name = "tableLayoutPanel_main";
            this.tableLayoutPanel_main.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel_main.RowCount = 2;
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel_main.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel_main.Size = new System.Drawing.Size(334, 443);
            this.tableLayoutPanel_main.TabIndex = 3;
            // 
            // btn_shipment
            // 
            this.btn_shipment.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.btn_shipment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_shipment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_shipment.FlatAppearance.BorderSize = 0;
            this.btn_shipment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_shipment.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_shipment.Location = new System.Drawing.Point(196, 305);
            this.btn_shipment.Margin = new System.Windows.Forms.Padding(30, 105, 30, 105);
            this.btn_shipment.Name = "btn_shipment";
            this.btn_shipment.Size = new System.Drawing.Size(13, 245);
            this.btn_shipment.TabIndex = 5;
            this.btn_shipment.TabStop = false;
            this.btn_shipment.Text = "출  하";
            this.btn_shipment.UseVisualStyleBackColor = true;
            this.btn_shipment.Click += new System.EventHandler(this.btn_production_Click);
            // 
            // btn_inlogistics
            // 
            this.btn_inlogistics.BackgroundImage = global::DK_Tablet.Properties.Resources.main2;
            this.btn_inlogistics.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_inlogistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_inlogistics.FlatAppearance.BorderSize = 0;
            this.btn_inlogistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_inlogistics.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_inlogistics.Location = new System.Drawing.Point(123, 305);
            this.btn_inlogistics.Margin = new System.Windows.Forms.Padding(30, 105, 30, 105);
            this.btn_inlogistics.Name = "btn_inlogistics";
            this.btn_inlogistics.Size = new System.Drawing.Size(13, 245);
            this.btn_inlogistics.TabIndex = 4;
            this.btn_inlogistics.TabStop = false;
            this.btn_inlogistics.Text = "자재관리";
            this.btn_inlogistics.UseVisualStyleBackColor = true;
            this.btn_inlogistics.Click += new System.EventHandler(this.btn_production_Click);
            // 
            // btn_production
            // 
            this.btn_production.BackgroundImage = global::DK_Tablet.Properties.Resources.main1;
            this.btn_production.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_production.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_production.FlatAppearance.BorderSize = 0;
            this.btn_production.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_production.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_production.Location = new System.Drawing.Point(50, 305);
            this.btn_production.Margin = new System.Windows.Forms.Padding(30, 105, 30, 105);
            this.btn_production.Name = "btn_production";
            this.btn_production.Size = new System.Drawing.Size(13, 245);
            this.btn_production.TabIndex = 3;
            this.btn_production.TabStop = false;
            this.btn_production.Text = "생  산";
            this.btn_production.UseVisualStyleBackColor = true;
            this.btn_production.Click += new System.EventHandler(this.btn_production_Click);
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::DK_Tablet.Properties.Resources.header;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel_main.SetColumnSpan(this.button5, 4);
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("맑은 고딕", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button5.Location = new System.Drawing.Point(23, 23);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(288, 174);
            this.button5.TabIndex = 6;
            this.button5.TabStop = false;
            this.button5.Text = "DK통합 생산 시스템";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // btn_quality
            // 
            this.btn_quality.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.btn_quality.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_quality.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_quality.FlatAppearance.BorderSize = 0;
            this.btn_quality.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_quality.Font = new System.Drawing.Font("맑은 고딕", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_quality.Location = new System.Drawing.Point(269, 305);
            this.btn_quality.Margin = new System.Windows.Forms.Padding(30, 105, 30, 105);
            this.btn_quality.Name = "btn_quality";
            this.btn_quality.Size = new System.Drawing.Size(15, 245);
            this.btn_quality.TabIndex = 5;
            this.btn_quality.TabStop = false;
            this.btn_quality.Text = "품  질";
            this.btn_quality.UseVisualStyleBackColor = true;
            this.btn_quality.Click += new System.EventHandler(this.btn_production_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DK_Tablet.Properties.Resources.logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(156, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1064, 148);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel_production
            // 
            this.tableLayoutPanel_production.ColumnCount = 5;
            this.tableLayoutPanel_production.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_production.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_production.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_production.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_production.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_injection, 0, 1);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_assey_c, 4, 1);
            this.tableLayoutPanel_production.Controls.Add(this.button3, 0, 0);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_laser, 1, 1);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_paint, 3, 1);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_assey, 2, 1);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_assey_c_2, 4, 2);
            this.tableLayoutPanel_production.Controls.Add(this.btn_injection_carrier, 0, 2);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_assey_c_3, 2, 2);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_laser_hi, 1, 2);
            this.tableLayoutPanel_production.Controls.Add(this.btn_injection_7, 0, 3);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_laser_JA_YB, 1, 3);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_assey_JA_YB, 4, 3);
            this.tableLayoutPanel_production.Controls.Add(this.btn_main_injection_Lot, 0, 4);
            this.tableLayoutPanel_production.Controls.Add(this.button10, 4, 4);
            this.tableLayoutPanel_production.Location = new System.Drawing.Point(374, 54);
            this.tableLayoutPanel_production.Name = "tableLayoutPanel_production";
            this.tableLayoutPanel_production.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel_production.RowCount = 5;
            this.tableLayoutPanel_production.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel_production.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel_production.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel_production.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel_production.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel_production.Size = new System.Drawing.Size(450, 522);
            this.tableLayoutPanel_production.TabIndex = 4;
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::DK_Tablet.Properties.Resources.header;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel_production.SetColumnSpan(this.button3, 5);
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("맑은 고딕", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button3.Location = new System.Drawing.Point(11, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(428, 174);
            this.button3.TabIndex = 6;
            this.button3.TabStop = false;
            this.button3.Text = "생   산";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btn_main_assey_c_2
            // 
            this.btn_main_assey_c_2.BackgroundImage = global::DK_Tablet.Properties.Resources.product5;
            this.btn_main_assey_c_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_assey_c_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_assey_c_2.FlatAppearance.BorderSize = 0;
            this.btn_main_assey_c_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_assey_c_2.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_assey_c_2.Location = new System.Drawing.Point(352, 269);
            this.btn_main_assey_c_2.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_assey_c_2.Name = "btn_main_assey_c_2";
            this.btn_main_assey_c_2.Size = new System.Drawing.Size(90, 81);
            this.btn_main_assey_c_2.TabIndex = 0;
            this.btn_main_assey_c_2.Text = "완성품\r\n(4,5,6,7,9,AS)";
            this.btn_main_assey_c_2.UseVisualStyleBackColor = true;
            this.btn_main_assey_c_2.Click += new System.EventHandler(this.btn_main_assey_c_2_Click);
            // 
            // btn_injection_carrier
            // 
            this.btn_injection_carrier.BackgroundImage = global::DK_Tablet.Properties.Resources.product1;
            this.btn_injection_carrier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_injection_carrier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_injection_carrier.FlatAppearance.BorderSize = 0;
            this.btn_injection_carrier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_injection_carrier.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_injection_carrier.Location = new System.Drawing.Point(8, 269);
            this.btn_injection_carrier.Margin = new System.Windows.Forms.Padding(0);
            this.btn_injection_carrier.Name = "btn_injection_carrier";
            this.btn_injection_carrier.Size = new System.Drawing.Size(86, 81);
            this.btn_injection_carrier.TabIndex = 0;
            this.btn_injection_carrier.Text = "사출\r\n(케리어)";
            this.btn_injection_carrier.UseVisualStyleBackColor = true;
            this.btn_injection_carrier.Click += new System.EventHandler(this.btn_injection_carrier_Click);
            // 
            // btn_main_assey_c_3
            // 
            this.btn_main_assey_c_3.BackgroundImage = global::DK_Tablet.Properties.Resources.product5;
            this.btn_main_assey_c_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_assey_c_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_assey_c_3.FlatAppearance.BorderSize = 0;
            this.btn_main_assey_c_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_assey_c_3.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_assey_c_3.Location = new System.Drawing.Point(180, 269);
            this.btn_main_assey_c_3.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_assey_c_3.Name = "btn_main_assey_c_3";
            this.btn_main_assey_c_3.Size = new System.Drawing.Size(86, 81);
            this.btn_main_assey_c_3.TabIndex = 0;
            this.btn_main_assey_c_3.Text = "완성품(HI라인)";
            this.btn_main_assey_c_3.UseVisualStyleBackColor = true;
            this.btn_main_assey_c_3.Click += new System.EventHandler(this.btn_main_assey_c_3_Click);
            // 
            // btn_main_laser_hi
            // 
            this.btn_main_laser_hi.BackgroundImage = global::DK_Tablet.Properties.Resources.product2;
            this.btn_main_laser_hi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_laser_hi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_laser_hi.FlatAppearance.BorderSize = 0;
            this.btn_main_laser_hi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_laser_hi.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_laser_hi.Location = new System.Drawing.Point(94, 269);
            this.btn_main_laser_hi.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_laser_hi.Name = "btn_main_laser_hi";
            this.btn_main_laser_hi.Size = new System.Drawing.Size(86, 81);
            this.btn_main_laser_hi.TabIndex = 0;
            this.btn_main_laser_hi.Text = "레이저(HI라인)";
            this.btn_main_laser_hi.UseVisualStyleBackColor = true;
            this.btn_main_laser_hi.Click += new System.EventHandler(this.btn_main_laser_hi_Click);
            // 
            // btn_injection_7
            // 
            this.btn_injection_7.BackgroundImage = global::DK_Tablet.Properties.Resources.product1;
            this.btn_injection_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_injection_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_injection_7.FlatAppearance.BorderSize = 0;
            this.btn_injection_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_injection_7.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_injection_7.Location = new System.Drawing.Point(8, 350);
            this.btn_injection_7.Margin = new System.Windows.Forms.Padding(0);
            this.btn_injection_7.Name = "btn_injection_7";
            this.btn_injection_7.Size = new System.Drawing.Size(86, 81);
            this.btn_injection_7.TabIndex = 0;
            this.btn_injection_7.Text = "사출\r\n(7호기)";
            this.btn_injection_7.UseVisualStyleBackColor = true;
            this.btn_injection_7.Click += new System.EventHandler(this.btn_injection_7_Click);
            // 
            // btn_main_laser_JA_YB
            // 
            this.btn_main_laser_JA_YB.BackgroundImage = global::DK_Tablet.Properties.Resources.product2;
            this.btn_main_laser_JA_YB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_laser_JA_YB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_laser_JA_YB.FlatAppearance.BorderSize = 0;
            this.btn_main_laser_JA_YB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_laser_JA_YB.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_laser_JA_YB.Location = new System.Drawing.Point(94, 350);
            this.btn_main_laser_JA_YB.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_laser_JA_YB.Name = "btn_main_laser_JA_YB";
            this.btn_main_laser_JA_YB.Size = new System.Drawing.Size(86, 81);
            this.btn_main_laser_JA_YB.TabIndex = 0;
            this.btn_main_laser_JA_YB.Text = "레이저(JA,YB)";
            this.btn_main_laser_JA_YB.UseVisualStyleBackColor = true;
            this.btn_main_laser_JA_YB.Click += new System.EventHandler(this.btn_main_laser_JA_YB_Click);
            // 
            // btn_main_assey_JA_YB
            // 
            this.btn_main_assey_JA_YB.BackgroundImage = global::DK_Tablet.Properties.Resources.product5;
            this.btn_main_assey_JA_YB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_assey_JA_YB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_assey_JA_YB.FlatAppearance.BorderSize = 0;
            this.btn_main_assey_JA_YB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_assey_JA_YB.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_assey_JA_YB.Location = new System.Drawing.Point(352, 350);
            this.btn_main_assey_JA_YB.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_assey_JA_YB.Name = "btn_main_assey_JA_YB";
            this.btn_main_assey_JA_YB.Size = new System.Drawing.Size(90, 81);
            this.btn_main_assey_JA_YB.TabIndex = 0;
            this.btn_main_assey_JA_YB.Text = "완성품(JA)";
            this.btn_main_assey_JA_YB.UseVisualStyleBackColor = true;
            this.btn_main_assey_JA_YB.Click += new System.EventHandler(this.btn_main_assey_JA_YB_Click);
            // 
            // btn_main_injection_Lot
            // 
            this.btn_main_injection_Lot.BackgroundImage = global::DK_Tablet.Properties.Resources.product1;
            this.btn_main_injection_Lot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_injection_Lot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_injection_Lot.FlatAppearance.BorderSize = 0;
            this.btn_main_injection_Lot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_injection_Lot.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.btn_main_injection_Lot.Location = new System.Drawing.Point(8, 431);
            this.btn_main_injection_Lot.Margin = new System.Windows.Forms.Padding(0);
            this.btn_main_injection_Lot.Name = "btn_main_injection_Lot";
            this.btn_main_injection_Lot.Size = new System.Drawing.Size(86, 83);
            this.btn_main_injection_Lot.TabIndex = 0;
            this.btn_main_injection_Lot.Text = "사출(JA,YB)";
            this.btn_main_injection_Lot.UseVisualStyleBackColor = true;
            this.btn_main_injection_Lot.Click += new System.EventHandler(this.btn_main_injection_Lot_Click);
            // 
            // button10
            // 
            this.button10.BackgroundImage = global::DK_Tablet.Properties.Resources.product5;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(352, 431);
            this.button10.Margin = new System.Windows.Forms.Padding(0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(90, 83);
            this.button10.TabIndex = 0;
            this.button10.Text = "완성품(YB)";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // tableLayoutPanel_logistics
            // 
            this.tableLayoutPanel_logistics.ColumnCount = 4;
            this.tableLayoutPanel_logistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_logistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_logistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_logistics.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel_logistics.Controls.Add(this.btn_main_metarial_move, 1, 1);
            this.tableLayoutPanel_logistics.Controls.Add(this.btn_main_Instatement, 0, 1);
            this.tableLayoutPanel_logistics.Controls.Add(this.button4, 0, 0);
            this.tableLayoutPanel_logistics.Controls.Add(this.btn_inspection, 2, 1);
            this.tableLayoutPanel_logistics.Controls.Add(this.button7, 3, 1);
            this.tableLayoutPanel_logistics.Location = new System.Drawing.Point(386, 12);
            this.tableLayoutPanel_logistics.Name = "tableLayoutPanel_logistics";
            this.tableLayoutPanel_logistics.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel_logistics.RowCount = 3;
            this.tableLayoutPanel_logistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel_logistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel_logistics.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel_logistics.Size = new System.Drawing.Size(461, 486);
            this.tableLayoutPanel_logistics.TabIndex = 4;
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::DK_Tablet.Properties.Resources.header;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel_logistics.SetColumnSpan(this.button4, 4);
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("맑은 고딕", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button4.Location = new System.Drawing.Point(23, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(415, 174);
            this.button4.TabIndex = 6;
            this.button4.TabStop = false;
            this.button4.Text = "자재관리";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::DK_Tablet.Properties.Resources.logistics1;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button7.Location = new System.Drawing.Point(338, 238);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(100, 83);
            this.button7.TabIndex = 0;
            this.button7.Text = "사급출고";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel_shipment
            // 
            this.tableLayoutPanel_shipment.ColumnCount = 3;
            this.tableLayoutPanel_shipment.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tableLayoutPanel_shipment.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel_shipment.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel_shipment.Controls.Add(this.btn_main_shipment, 0, 1);
            this.tableLayoutPanel_shipment.Controls.Add(this.button6, 0, 0);
            this.tableLayoutPanel_shipment.Controls.Add(this.button8, 1, 1);
            this.tableLayoutPanel_shipment.Controls.Add(this.button9, 2, 1);
            this.tableLayoutPanel_shipment.Controls.Add(this.btn_main_site_move, 0, 2);
            this.tableLayoutPanel_shipment.Location = new System.Drawing.Point(926, 35);
            this.tableLayoutPanel_shipment.Name = "tableLayoutPanel_shipment";
            this.tableLayoutPanel_shipment.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel_shipment.RowCount = 3;
            this.tableLayoutPanel_shipment.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel_shipment.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_shipment.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_shipment.Size = new System.Drawing.Size(431, 415);
            this.tableLayoutPanel_shipment.TabIndex = 4;
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::DK_Tablet.Properties.Resources.header;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel_shipment.SetColumnSpan(this.button6, 3);
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("맑은 고딕", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button6.Location = new System.Drawing.Point(23, 23);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(385, 174);
            this.button6.TabIndex = 6;
            this.button6.TabStop = false;
            this.button6.Text = "출   하";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::DK_Tablet.Properties.Resources.shipment1;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button8.Location = new System.Drawing.Point(195, 230);
            this.button8.Margin = new System.Windows.Forms.Padding(45, 30, 45, 30);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 37);
            this.button8.TabIndex = 0;
            this.button8.Text = "출하\r\n(식별표)";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.BackgroundImage = global::DK_Tablet.Properties.Resources.shipment1;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button9.Location = new System.Drawing.Point(325, 230);
            this.button9.Margin = new System.Windows.Forms.Padding(45, 30, 45, 30);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(41, 37);
            this.button9.TabIndex = 0;
            this.button9.Text = "재고실사";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // btn_main_site_move
            // 
            this.btn_main_site_move.BackgroundImage = global::DK_Tablet.Properties.Resources.shipment1;
            this.btn_main_site_move.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_main_site_move.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_main_site_move.FlatAppearance.BorderSize = 0;
            this.btn_main_site_move.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_main_site_move.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_main_site_move.Location = new System.Drawing.Point(65, 327);
            this.btn_main_site_move.Margin = new System.Windows.Forms.Padding(45, 30, 45, 30);
            this.btn_main_site_move.Name = "btn_main_site_move";
            this.btn_main_site_move.Size = new System.Drawing.Size(40, 38);
            this.btn_main_site_move.TabIndex = 0;
            this.btn_main_site_move.Text = "공장대체";
            this.btn_main_site_move.UseVisualStyleBackColor = true;
            this.btn_main_site_move.Click += new System.EventHandler(this.btn_main_site_move_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel_quality);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel_shipment);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel_production);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel_logistics);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel_main);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel14);
            this.splitContainer1.Size = new System.Drawing.Size(1378, 758);
            this.splitContainer1.SplitterDistance = 560;
            this.splitContainer1.TabIndex = 5;
            this.splitContainer1.TabStop = false;
            // 
            // tableLayoutPanel_quality
            // 
            this.tableLayoutPanel_quality.ColumnCount = 3;
            this.tableLayoutPanel_quality.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_quality.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_quality.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_quality.Controls.Add(this.btn_Loading, 0, 2);
            this.tableLayoutPanel_quality.Controls.Add(this.btn_FME_InsResult, 0, 1);
            this.tableLayoutPanel_quality.Controls.Add(this.button11, 0, 0);
            this.tableLayoutPanel_quality.Controls.Add(this.btn_fail, 1, 1);
            this.tableLayoutPanel_quality.Controls.Add(this.button2, 2, 1);
            this.tableLayoutPanel_quality.Location = new System.Drawing.Point(873, 49);
            this.tableLayoutPanel_quality.Name = "tableLayoutPanel_quality";
            this.tableLayoutPanel_quality.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel_quality.RowCount = 3;
            this.tableLayoutPanel_quality.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.tableLayoutPanel_quality.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_quality.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_quality.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_quality.Size = new System.Drawing.Size(461, 419);
            this.tableLayoutPanel_quality.TabIndex = 6;
            this.tableLayoutPanel_quality.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel_quality_Paint);
            // 
            // btn_Loading
            // 
            this.btn_Loading.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.btn_Loading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Loading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Loading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Loading.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold);
            this.btn_Loading.Location = new System.Drawing.Point(23, 337);
            this.btn_Loading.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_Loading.Name = "btn_Loading";
            this.btn_Loading.Size = new System.Drawing.Size(134, 24);
            this.btn_Loading.TabIndex = 8;
            this.btn_Loading.Text = "button10";
            this.btn_Loading.UseVisualStyleBackColor = true;
            this.btn_Loading.Visible = false;
            // 
            // btn_FME_InsResult
            // 
            this.btn_FME_InsResult.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.btn_FME_InsResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_FME_InsResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_FME_InsResult.FlatAppearance.BorderSize = 0;
            this.btn_FME_InsResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FME_InsResult.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_FME_InsResult.Location = new System.Drawing.Point(23, 238);
            this.btn_FME_InsResult.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_FME_InsResult.Name = "btn_FME_InsResult";
            this.btn_FME_InsResult.Size = new System.Drawing.Size(134, 23);
            this.btn_FME_InsResult.TabIndex = 0;
            this.btn_FME_InsResult.Text = "초중종물 검사등록";
            this.btn_FME_InsResult.UseVisualStyleBackColor = true;
            this.btn_FME_InsResult.Click += new System.EventHandler(this.btn_FME_InsResult_Click_1);
            // 
            // button11
            // 
            this.button11.BackgroundImage = global::DK_Tablet.Properties.Resources.header;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel_quality.SetColumnSpan(this.button11, 3);
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("맑은 고딕", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button11.Location = new System.Drawing.Point(23, 23);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(415, 174);
            this.button11.TabIndex = 6;
            this.button11.TabStop = false;
            this.button11.Text = "품질관리";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // btn_fail
            // 
            this.btn_fail.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.btn_fail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_fail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_fail.FlatAppearance.BorderSize = 0;
            this.btn_fail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fail.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_fail.Location = new System.Drawing.Point(163, 238);
            this.btn_fail.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.btn_fail.Name = "btn_fail";
            this.btn_fail.Size = new System.Drawing.Size(134, 23);
            this.btn_fail.TabIndex = 0;
            this.btn_fail.Text = "불량처리";
            this.btn_fail.UseVisualStyleBackColor = true;
            this.btn_fail.Click += new System.EventHandler(this.btn_fail_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::DK_Tablet.Properties.Resources.main3;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("맑은 고딕", 24.75F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(303, 238);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 38, 3, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "수입검사";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel14.Controls.Add(this.btn_back, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1378, 194);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // btn_back
            // 
            this.btn_back.BackgroundImage = global::DK_Tablet.Properties.Resources.go_back1;
            this.btn_back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_back.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_back.FlatAppearance.BorderSize = 0;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_back.Location = new System.Drawing.Point(23, 23);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(127, 148);
            this.btn_back.TabIndex = 6;
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1378, 758);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MAIN";
            this.Text = "MAIN";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MAIN_FormClosing);
            this.Load += new System.EventHandler(this.MAIN_Load);
            this.tableLayoutPanel_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel_production.ResumeLayout(false);
            this.tableLayoutPanel_logistics.ResumeLayout(false);
            this.tableLayoutPanel_shipment.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel_quality.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_main_injection;
        private System.Windows.Forms.Button btn_main_paint;
        private System.Windows.Forms.Button btn_main_assey;
        private System.Windows.Forms.Button btn_main_laser;
        private System.Windows.Forms.Button btn_main_assey_c;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.Button btn_main_shipment;
        private System.Windows.Forms.Button btn_main_metarial_move;
        private System.Windows.Forms.Button btn_inspection;
        private System.Windows.Forms.Button btn_main_Instatement;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_main;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_production;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_logistics;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_shipment;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Button btn_production;
        private System.Windows.Forms.Button btn_shipment;
        private System.Windows.Forms.Button btn_inlogistics;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button btn_injection_carrier;
        private System.Windows.Forms.Button btn_main_assey_c_2;
        private System.Windows.Forms.Button btn_quality;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btn_main_assey_c_3;
        private System.Windows.Forms.Button btn_main_laser_hi;
        private System.Windows.Forms.Button btn_injection_7;
        private System.Windows.Forms.Button btn_main_site_move;
        private System.Windows.Forms.Button btn_main_laser_JA_YB;
        private System.Windows.Forms.Button btn_main_assey_JA_YB;
        private System.Windows.Forms.Button btn_main_injection_Lot;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_quality;
        private System.Windows.Forms.Button btn_Loading;
        private System.Windows.Forms.Button btn_FME_InsResult;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btn_fail;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button10;
    }
}