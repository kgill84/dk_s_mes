using System;
using System.IO;
using System.IO.Ports;
using System.Collections;
using System.Windows.Forms; // for Application.StartupPath

namespace DK_Tablet
{
    /// <summary>
    /// Persistent settings
    /// </summary>
    public class IP_Settings
    {
        /// <summary> Port settings. </summary>
        public class ip
        {
            public static string ip1 = "127.0.0.10";
            public static string ip2 = "127.0.0.20";
            public static string ip3 = "127.0.0.30";
            public static string ip4 = "127.0.0.40";
            public static string ip5 = "127.0.0.50";
            public static string ip6 = "127.0.0.60";
            public static string ip7 = "127.0.0.70";

            public static string port1 = "1234";
            public static string port2 = "1234";
            public static string port3 = "2004";
            public static string port4 = "1234";
            public static string port5 = "1234";
            public static string port6 = "1234";
            public static string port7 = "1234";
        }

        
        
        
        /// <summary>
        ///   Read the settings from disk. </summary>
        public static void Read()
        {

            IniFile ini = new IniFile(Application.StartupPath + "\\IP_SETTINGS.ini");
            
            ip.ip1 = ini.ReadValue("IP", "IP1", ip.ip1);
            ip.ip1 = ini.ReadValue("IP", "IP2", ip.ip2);
            ip.ip1 = ini.ReadValue("IP", "IP3", ip.ip3);
            ip.ip1 = ini.ReadValue("IP", "IP4", ip.ip4);
            ip.ip1 = ini.ReadValue("IP", "IP5", ip.ip5);
            ip.ip1 = ini.ReadValue("IP", "IP6", ip.ip6);
            ip.ip1 = ini.ReadValue("IP", "IP7", ip.ip7);

            ip.port1 = ini.ReadValue("IP", "PORT1", ip.port1);
            ip.port2 = ini.ReadValue("IP", "PORT2", ip.port2);
            ip.port3 = ini.ReadValue("IP", "PORT3", ip.port3);
            ip.port4 = ini.ReadValue("IP", "PORT4", ip.port4);
            ip.port5 = ini.ReadValue("IP", "PORT5", ip.port5);
            ip.port6 = ini.ReadValue("IP", "PORT6", ip.port6);
            ip.port7 = ini.ReadValue("IP", "PORT7", ip.port7);

            
		}

        /// <summary>
        ///   Write the settings to disk. </summary>
        public static void Write()
        {
            MessageBox.Show(Application.StartupPath + "\\Termie.ini");
            IniFile ini = new IniFile(Application.StartupPath + "\\Termie.ini");
            ini.WriteValue("IP", "IP1", ip.ip1);
            ini.WriteValue("IP", "IP2", ip.ip2);
            ini.WriteValue("IP", "IP3", ip.ip3);
            ini.WriteValue("IP", "IP4", ip.ip4);
            ini.WriteValue("IP", "IP5", ip.ip5);
            ini.WriteValue("IP", "IP6", ip.ip6);
            ini.WriteValue("IP", "IP7", ip.ip7);

            ini.WriteValue("IP", "PORT1", ip.port1);
            ini.WriteValue("IP", "PORT2", ip.port2);
            ini.WriteValue("IP", "PORT3", ip.port3);
            ini.WriteValue("IP", "PORT4", ip.port4);
            ini.WriteValue("IP", "PORT5", ip.port5);
            ini.WriteValue("IP", "PORT6", ip.port6);
            ini.WriteValue("IP", "PORT7", ip.port7);
		}
	}
}
