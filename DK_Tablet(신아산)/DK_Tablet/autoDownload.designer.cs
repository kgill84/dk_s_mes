﻿namespace DK_Tablet
{
    partial class autoDownload
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(autoDownload));
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.LBpercent = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LBlocation = new System.Windows.Forms.Label();
            this.LBfileSize = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 12);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(424, 27);
            this.progressBar1.TabIndex = 0;
            // 
            // LBpercent
            // 
            this.LBpercent.BackColor = System.Drawing.Color.Transparent;
            this.LBpercent.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.LBpercent.Location = new System.Drawing.Point(14, 42);
            this.LBpercent.Name = "LBpercent";
            this.LBpercent.Size = new System.Drawing.Size(70, 21);
            this.LBpercent.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(399, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "프로그램 실행을 위한 설치파일을 다운로드 중입니다.";
            // 
            // LBlocation
            // 
            this.LBlocation.Location = new System.Drawing.Point(14, 69);
            this.LBlocation.Name = "LBlocation";
            this.LBlocation.Size = new System.Drawing.Size(321, 24);
            this.LBlocation.TabIndex = 3;
            this.LBlocation.Click += new System.EventHandler(this.LBlocation_Click);
            // 
            // LBfileSize
            // 
            this.LBfileSize.BackColor = System.Drawing.Color.Transparent;
            this.LBfileSize.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.LBfileSize.Location = new System.Drawing.Point(105, 42);
            this.LBfileSize.Name = "LBfileSize";
            this.LBfileSize.Size = new System.Drawing.Size(89, 21);
            this.LBfileSize.TabIndex = 4;
            // 
            // autoDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(448, 111);
            this.Controls.Add(this.LBfileSize);
            this.Controls.Add(this.LBlocation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LBpercent);
            this.Controls.Add(this.progressBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "autoDownload";
            this.Text = "설치파일 다운로드";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label LBpercent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LBlocation;
        private System.Windows.Forms.Label LBfileSize;
    }
}

