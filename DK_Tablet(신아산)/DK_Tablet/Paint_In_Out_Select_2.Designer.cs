﻿namespace DK_Tablet
{
    partial class Paint_In_Out_Select_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_In = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Out = new DevExpress.XtraEditors.SimpleButton();
            this.btn_In_QR = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btn_In
            // 
            this.btn_In.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_In.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            this.btn_In.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.btn_In.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_In.Appearance.Options.UseBackColor = true;
            this.btn_In.Appearance.Options.UseFont = true;
            this.btn_In.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_In.Location = new System.Drawing.Point(355, 12);
            this.btn_In.Name = "btn_In";
            this.btn_In.Size = new System.Drawing.Size(335, 274);
            this.btn_In.TabIndex = 0;
            this.btn_In.Text = "입구\r\n(PP 카드)";
            this.btn_In.Click += new System.EventHandler(this.btn_In_Click);
            // 
            // btn_Out
            // 
            this.btn_Out.Appearance.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Out.Appearance.BackColor2 = System.Drawing.Color.White;
            this.btn_Out.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.btn_Out.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_Out.Appearance.Options.UseBackColor = true;
            this.btn_Out.Appearance.Options.UseFont = true;
            this.btn_Out.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Out.Location = new System.Drawing.Point(698, 12);
            this.btn_Out.Name = "btn_Out";
            this.btn_Out.Size = new System.Drawing.Size(341, 274);
            this.btn_Out.TabIndex = 1;
            this.btn_Out.Text = "출구";
            this.btn_Out.Click += new System.EventHandler(this.btn_Out_Click);
            // 
            // btn_In_QR
            // 
            this.btn_In_QR.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_In_QR.Appearance.BackColor2 = System.Drawing.Color.PapayaWhip;
            this.btn_In_QR.Appearance.Font = new System.Drawing.Font("Tahoma", 45F);
            this.btn_In_QR.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_In_QR.Appearance.Options.UseBackColor = true;
            this.btn_In_QR.Appearance.Options.UseFont = true;
            this.btn_In_QR.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_In_QR.Location = new System.Drawing.Point(12, 12);
            this.btn_In_QR.Name = "btn_In_QR";
            this.btn_In_QR.Size = new System.Drawing.Size(335, 274);
            this.btn_In_QR.TabIndex = 0;
            this.btn_In_QR.Text = "입구\r\n(QR 코드)";
            this.btn_In_QR.Click += new System.EventHandler(this.btn_In_QR_Click);
            // 
            // Paint_In_Out_Select_2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 299);
            this.Controls.Add(this.btn_In_QR);
            this.Controls.Add(this.btn_In);
            this.Controls.Add(this.btn_Out);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Paint_In_Out_Select_2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paint_In_Out_Select";
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton btn_In;
        public DevExpress.XtraEditors.SimpleButton btn_Out;
        public DevExpress.XtraEditors.SimpleButton btn_In_QR;
    }
}