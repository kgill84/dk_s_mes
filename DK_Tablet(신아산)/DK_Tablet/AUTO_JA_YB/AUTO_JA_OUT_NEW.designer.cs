﻿namespace DK_Tablet
{
    partial class AUTO_JA_OUT_NEW
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.RESULT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer_시계 = new System.Windows.Forms.Timer(this.components);
            this.read_send_timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TEST_검사_in = new System.Windows.Forms.Button();
            this.TEST_검사_out = new System.Windows.Forms.Button();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_검사기 = new System.Windows.Forms.ListBox();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.winsock_검사기 = new MelsecPLC.Winsock();
            this.Refresh_timer_검사기 = new System.Windows.Forms.Timer(this.components);
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.btnClose = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.lbl_now_date = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl8 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.splitContainerControl9 = new DevExpress.XtraEditors.SplitContainerControl();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.REG_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ORDER_NUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_SNAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PLAN_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTO_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GOOD_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAIL_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOZZLE_CNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SPEC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MO_SNUMB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOBOT_INTO_SQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IT_PKQTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.READING_DATA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.S_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NG_DESCR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.message_timer_clear = new System.Windows.Forms.Timer(this.components);
            this.search_re_time = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).BeginInit();
            this.splitContainerControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).BeginInit();
            this.splitContainerControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // RESULT
            // 
            this.RESULT.AppearanceCell.Options.UseTextOptions = true;
            this.RESULT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RESULT.Caption = "OK/NG";
            this.RESULT.FieldName = "RESULT";
            this.RESULT.Name = "RESULT";
            this.RESULT.Visible = true;
            this.RESULT.VisibleIndex = 1;
            this.RESULT.Width = 320;
            // 
            // timer_시계
            // 
            this.timer_시계.Enabled = true;
            this.timer_시계.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // read_send_timer
            // 
            this.read_send_timer.Interval = 500;
            this.read_send_timer.Tick += new System.EventHandler(this.read_send_timer_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.splitContainerControl6);
            this.panel1.Location = new System.Drawing.Point(3, 209);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(195, 385);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_in, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_out, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 303);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(183, 79);
            this.tableLayoutPanel1.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("굴림", 25F);
            this.label6.Location = new System.Drawing.Point(1, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 38);
            this.label6.TabIndex = 40;
            this.label6.Text = "in";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("굴림", 25F);
            this.label9.Location = new System.Drawing.Point(1, 40);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 38);
            this.label9.TabIndex = 40;
            this.label9.Text = "out";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEST_검사_in
            // 
            this.TEST_검사_in.Location = new System.Drawing.Point(95, 4);
            this.TEST_검사_in.Name = "TEST_검사_in";
            this.TEST_검사_in.Size = new System.Drawing.Size(84, 32);
            this.TEST_검사_in.TabIndex = 39;
            this.TEST_검사_in.Text = "검사";
            this.TEST_검사_in.UseVisualStyleBackColor = true;
            this.TEST_검사_in.Click += new System.EventHandler(this.TEST_LASER_in_Click);
            // 
            // TEST_검사_out
            // 
            this.TEST_검사_out.Location = new System.Drawing.Point(95, 43);
            this.TEST_검사_out.Name = "TEST_검사_out";
            this.TEST_검사_out.Size = new System.Drawing.Size(84, 32);
            this.TEST_검사_out.TabIndex = 39;
            this.TEST_검사_out.Text = "검사";
            this.TEST_검사_out.UseVisualStyleBackColor = true;
            this.TEST_검사_out.Click += new System.EventHandler(this.TEST_LASER_out_Click);
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(3, 3);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.list_box_검사기);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton6);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(183, 294);
            this.splitContainerControl6.SplitterPosition = 34;
            this.splitContainerControl6.TabIndex = 6;
            this.splitContainerControl6.Text = "splitContainerControl1";
            // 
            // list_box_검사기
            // 
            this.list_box_검사기.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_검사기.FormattingEnabled = true;
            this.list_box_검사기.ItemHeight = 14;
            this.list_box_검사기.Location = new System.Drawing.Point(0, 0);
            this.list_box_검사기.Name = "list_box_검사기";
            this.list_box_검사기.Size = new System.Drawing.Size(183, 255);
            this.list_box_검사기.TabIndex = 4;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton6.Location = new System.Drawing.Point(0, 0);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(183, 34);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "clear";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // winsock_검사기
            // 
            this.winsock_검사기.LocalPort = 80;
            this.winsock_검사기.RemoteIP = "127.0.0.1";
            this.winsock_검사기.RemotePort = 80;
            this.winsock_검사기.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_검사기);
            // 
            // Refresh_timer_검사기
            // 
            this.Refresh_timer_검사기.Interval = 500;
            this.Refresh_timer_검사기.Tick += new System.EventHandler(this.Refresh_timer_검사기_Tick);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(97, 2);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AutoHeight = false;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(210, 78);
            this.dateEdit1.TabIndex = 38;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(720, 3);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Size = new System.Drawing.Size(348, 46);
            this.textEdit1.TabIndex = 37;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            this.textEdit1.Leave += new System.EventHandler(this.textEdit1_Leave);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1277, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 81);
            this.btnClose.TabIndex = 36;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(93, 83);
            this.label47.TabIndex = 33;
            this.label47.Text = "생산일";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label47.Click += new System.EventHandler(this.label47_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(633, 0);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(82, 83);
            this.btn_po_release.TabIndex = 34;
            this.btn_po_release.Text = "작업\r\n검색";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(313, 3);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(316, 77);
            this.lueWc_code.TabIndex = 35;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // lbl_now_date
            // 
            this.lbl_now_date.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lbl_now_date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_now_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_now_date.Location = new System.Drawing.Point(720, 0);
            this.lbl_now_date.Name = "lbl_now_date";
            this.lbl_now_date.Size = new System.Drawing.Size(347, 28);
            this.lbl_now_date.TabIndex = 40;
            this.lbl_now_date.Text = "labelControl1";
            // 
            // splitContainerControl8
            // 
            this.splitContainerControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl8.Horizontal = false;
            this.splitContainerControl8.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl8.Name = "splitContainerControl8";
            this.splitContainerControl8.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl8.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl8.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl8.Panel1.Controls.Add(this.label47);
            this.splitContainerControl8.Panel1.Controls.Add(this.lueWc_code);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_po_release);
            this.splitContainerControl8.Panel1.Controls.Add(this.dateEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.btnClose);
            this.splitContainerControl8.Panel1.Controls.Add(this.textEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.lbl_now_date);
            this.splitContainerControl8.Panel1.Controls.Add(this.textEdit2);
            this.splitContainerControl8.Panel1.Text = "Panel1";
            this.splitContainerControl8.Panel2.Controls.Add(this.splitContainerControl9);
            this.splitContainerControl8.Panel2.Text = "Panel2";
            this.splitContainerControl8.Size = new System.Drawing.Size(1386, 742);
            this.splitContainerControl8.SplitterPosition = 93;
            this.splitContainerControl8.TabIndex = 41;
            this.splitContainerControl8.Text = "splitContainerControl8";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.LemonChiffon;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton1.Location = new System.Drawing.Point(1208, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(63, 79);
            this.simpleButton1.TabIndex = 43;
            this.simpleButton1.Text = "새로\r\n고침";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 60F);
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(1074, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(128, 88);
            this.labelControl2.TabIndex = 42;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(720, 56);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(347, 36);
            this.labelControl1.TabIndex = 41;
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "";
            this.textEdit2.Location = new System.Drawing.Point(379, 55);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.AutoHeight = false;
            this.textEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit2.Size = new System.Drawing.Size(348, 46);
            this.textEdit2.TabIndex = 37;
            this.textEdit2.Visible = false;
            this.textEdit2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit2_KeyPress);
            // 
            // splitContainerControl9
            // 
            this.splitContainerControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl9.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl9.Horizontal = false;
            this.splitContainerControl9.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl9.Name = "splitContainerControl9";
            this.splitContainerControl9.Panel1.Controls.Add(this.panel1);
            this.splitContainerControl9.Panel1.Controls.Add(this.dataGridView1);
            this.splitContainerControl9.Panel1.Text = "Panel1";
            this.splitContainerControl9.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl9.Panel2.Text = "Panel2";
            this.splitContainerControl9.Size = new System.Drawing.Size(1386, 644);
            this.splitContainerControl9.SplitterPosition = 182;
            this.splitContainerControl9.TabIndex = 40;
            this.splitContainerControl9.Text = "splitContainerControl9";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeight = 50;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.REG_DATE,
            this.ORDER_NUM,
            this.IT_SCODE,
            this.IT_SNAME,
            this.PLAN_SQTY,
            this.INTO_SQTY,
            this.GOOD_SQTY,
            this.FAIL_SQTY,
            this.NOZZLE_CNT,
            this.SPEC,
            this.MO_SNUMB,
            this.LOBOT_INTO_SQTY,
            this.IT_PKQTY,
            this.CNT});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.RowTemplate.Height = 50;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(1386, 457);
            this.dataGridView1.TabIndex = 8;
            // 
            // REG_DATE
            // 
            this.REG_DATE.DataPropertyName = "REG_DATE";
            this.REG_DATE.HeaderText = "날짜";
            this.REG_DATE.Name = "REG_DATE";
            this.REG_DATE.ReadOnly = true;
            this.REG_DATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.REG_DATE.Visible = false;
            // 
            // ORDER_NUM
            // 
            this.ORDER_NUM.DataPropertyName = "ORDER_NUM";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.ORDER_NUM.DefaultCellStyle = dataGridViewCellStyle3;
            this.ORDER_NUM.HeaderText = "작업순서";
            this.ORDER_NUM.Name = "ORDER_NUM";
            this.ORDER_NUM.ReadOnly = true;
            this.ORDER_NUM.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ORDER_NUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ORDER_NUM.Width = 140;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.DataPropertyName = "IT_SCODE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.IT_SCODE.DefaultCellStyle = dataGridViewCellStyle4;
            this.IT_SCODE.HeaderText = "품번";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.ReadOnly = true;
            this.IT_SCODE.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IT_SCODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SCODE.Width = 350;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.DataPropertyName = "IT_SNAME";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.IT_SNAME.DefaultCellStyle = dataGridViewCellStyle5;
            this.IT_SNAME.HeaderText = "품명";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.ReadOnly = true;
            this.IT_SNAME.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IT_SNAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IT_SNAME.Width = 450;
            // 
            // PLAN_SQTY
            // 
            this.PLAN_SQTY.DataPropertyName = "PLAN_SQTY";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.PLAN_SQTY.DefaultCellStyle = dataGridViewCellStyle6;
            this.PLAN_SQTY.HeaderText = "계획량";
            this.PLAN_SQTY.Name = "PLAN_SQTY";
            this.PLAN_SQTY.ReadOnly = true;
            this.PLAN_SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PLAN_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PLAN_SQTY.Width = 120;
            // 
            // INTO_SQTY
            // 
            this.INTO_SQTY.DataPropertyName = "INTO_SQTY";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.INTO_SQTY.DefaultCellStyle = dataGridViewCellStyle7;
            this.INTO_SQTY.HeaderText = "투입수량";
            this.INTO_SQTY.Name = "INTO_SQTY";
            this.INTO_SQTY.ReadOnly = true;
            this.INTO_SQTY.Visible = false;
            // 
            // GOOD_SQTY
            // 
            this.GOOD_SQTY.DataPropertyName = "GOOD_SQTY";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.GOOD_SQTY.DefaultCellStyle = dataGridViewCellStyle8;
            this.GOOD_SQTY.HeaderText = "양품";
            this.GOOD_SQTY.Name = "GOOD_SQTY";
            this.GOOD_SQTY.ReadOnly = true;
            this.GOOD_SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.GOOD_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GOOD_SQTY.Width = 120;
            // 
            // FAIL_SQTY
            // 
            this.FAIL_SQTY.DataPropertyName = "FAIL_SQTY";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold);
            this.FAIL_SQTY.DefaultCellStyle = dataGridViewCellStyle9;
            this.FAIL_SQTY.HeaderText = "불량";
            this.FAIL_SQTY.Name = "FAIL_SQTY";
            this.FAIL_SQTY.ReadOnly = true;
            this.FAIL_SQTY.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FAIL_SQTY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FAIL_SQTY.Width = 120;
            // 
            // NOZZLE_CNT
            // 
            this.NOZZLE_CNT.DataPropertyName = "NOZZLE_CNT";
            this.NOZZLE_CNT.HeaderText = "노즐수량";
            this.NOZZLE_CNT.Name = "NOZZLE_CNT";
            this.NOZZLE_CNT.ReadOnly = true;
            this.NOZZLE_CNT.Visible = false;
            // 
            // SPEC
            // 
            this.SPEC.DataPropertyName = "SPEC";
            this.SPEC.HeaderText = "스펙";
            this.SPEC.Name = "SPEC";
            this.SPEC.ReadOnly = true;
            this.SPEC.Visible = false;
            // 
            // MO_SNUMB
            // 
            this.MO_SNUMB.DataPropertyName = "MO_SNUMB";
            this.MO_SNUMB.HeaderText = "작지번호";
            this.MO_SNUMB.Name = "MO_SNUMB";
            this.MO_SNUMB.ReadOnly = true;
            this.MO_SNUMB.Visible = false;
            // 
            // LOBOT_INTO_SQTY
            // 
            this.LOBOT_INTO_SQTY.DataPropertyName = "LOBOT_INTO_SQTY";
            this.LOBOT_INTO_SQTY.HeaderText = "로봇수량";
            this.LOBOT_INTO_SQTY.Name = "LOBOT_INTO_SQTY";
            this.LOBOT_INTO_SQTY.ReadOnly = true;
            this.LOBOT_INTO_SQTY.Visible = false;
            // 
            // IT_PKQTY
            // 
            this.IT_PKQTY.DataPropertyName = "IT_PKQTY";
            this.IT_PKQTY.HeaderText = "박스수량";
            this.IT_PKQTY.Name = "IT_PKQTY";
            this.IT_PKQTY.ReadOnly = true;
            this.IT_PKQTY.Visible = false;
            // 
            // CNT
            // 
            this.CNT.DataPropertyName = "CNT";
            this.CNT.HeaderText = "CNT";
            this.CNT.Name = "CNT";
            this.CNT.ReadOnly = true;
            this.CNT.Visible = false;
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1386, 182);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.READING_DATA,
            this.S_DATE,
            this.RESULT,
            this.NG_DESCR});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.Column = this.RESULT;
            gridFormatRule1.ColumnApplyTo = this.RESULT;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Salmon;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Expression = "[RESULT] = \'NG\'";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.Column = this.RESULT;
            gridFormatRule2.ColumnApplyTo = this.RESULT;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Expression = "[RESULT] = \'OK\'";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsBehavior.ReadOnly = true;
            this.gridView2.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // READING_DATA
            // 
            this.READING_DATA.Caption = "리딩 데이터";
            this.READING_DATA.FieldName = "READING_DATA";
            this.READING_DATA.Name = "READING_DATA";
            this.READING_DATA.Visible = true;
            this.READING_DATA.VisibleIndex = 0;
            this.READING_DATA.Width = 614;
            // 
            // S_DATE
            // 
            this.S_DATE.Caption = "DATE";
            this.S_DATE.FieldName = "S_DATE";
            this.S_DATE.Name = "S_DATE";
            this.S_DATE.Width = 429;
            // 
            // NG_DESCR
            // 
            this.NG_DESCR.Caption = "NG참조";
            this.NG_DESCR.FieldName = "NG_DESCR";
            this.NG_DESCR.Name = "NG_DESCR";
            this.NG_DESCR.Visible = true;
            this.NG_DESCR.VisibleIndex = 2;
            this.NG_DESCR.Width = 334;
            // 
            // message_timer_clear
            // 
            this.message_timer_clear.Tick += new System.EventHandler(this.message_timer_clear_Tick);
            // 
            // search_re_time
            // 
            this.search_re_time.Interval = 1000;
            this.search_re_time.Tick += new System.EventHandler(this.search_re_time_Tick);
            // 
            // AUTO_JA_OUT_NEW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1386, 742);
            this.Controls.Add(this.splitContainerControl8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_JA_OUT_NEW";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).EndInit();
            this.splitContainerControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).EndInit();
            this.splitContainerControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer_시계;
        private System.Windows.Forms.Timer read_send_timer;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private System.Windows.Forms.ListBox list_box_검사기;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private MelsecPLC.Winsock winsock_검사기;
        private System.Windows.Forms.Timer Refresh_timer_검사기;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btn_po_release;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private DevExpress.XtraEditors.LabelControl lbl_now_date;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl8;
        private System.Windows.Forms.Timer message_timer_clear;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl9;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn READING_DATA;
        private DevExpress.XtraGrid.Columns.GridColumn S_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn RESULT;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button TEST_검사_out;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button TEST_검사_in;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Timer search_re_time;
        private DevExpress.XtraGrid.Columns.GridColumn NG_DESCR;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn REG_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ORDER_NUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_SNAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLAN_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn INTO_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn GOOD_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAIL_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOZZLE_CNT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SPEC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MO_SNUMB;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOBOT_INTO_SQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn IT_PKQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNT;
    }
}

