﻿namespace DK_Tablet
{
    partial class AUTO_JA_OUT
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AUTO_JA_OUT));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.RESULT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer_시계 = new System.Windows.Forms.Timer(this.components);
            this.read_send_timer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_검사기 = new System.Windows.Forms.ListBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_save = new DevExpress.XtraEditors.SimpleButton();
            this.winsock_검사기 = new MelsecPLC.Winsock();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PAB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RH_LH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SSB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PLAN_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GOOD_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FAIL_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SPEC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_PKQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TOTAL_CNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.lbl_now_date = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl8 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.checkBox_가성불량 = new System.Windows.Forms.CheckBox();
            this.btn_program_finish = new DevExpress.XtraEditors.SimpleButton();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.btn_inspection = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl9 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_it_scode = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cnt = new DevExpress.XtraEditors.LabelControl();
            this.lbl_it_pkqty = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fail_sqty = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_good_sqty = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_plan_sqty = new DevExpress.XtraEditors.LabelControl();
            this.lbl_pab = new DevExpress.XtraEditors.LabelControl();
            this.lbl_rh_lh = new DevExpress.XtraEditors.LabelControl();
            this.lbl_ssb = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.READING_DATA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.S_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NG_DESCR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.message_timer_clear = new System.Windows.Forms.Timer(this.components);
            this.timer_send_check = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).BeginInit();
            this.splitContainerControl8.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).BeginInit();
            this.splitContainerControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // RESULT
            // 
            this.RESULT.AppearanceCell.Options.UseTextOptions = true;
            this.RESULT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RESULT.Caption = "OK/NG";
            this.RESULT.FieldName = "RESULT";
            this.RESULT.Name = "RESULT";
            this.RESULT.Visible = true;
            this.RESULT.VisibleIndex = 1;
            this.RESULT.Width = 320;
            // 
            // timer_시계
            // 
            this.timer_시계.Enabled = true;
            this.timer_시계.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // read_send_timer
            // 
            this.read_send_timer.Interval = 1000;
            this.read_send_timer.Tick += new System.EventHandler(this.read_send_timer_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainerControl6);
            this.panel1.Location = new System.Drawing.Point(964, 187);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 303);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(3, 3);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.list_box_검사기);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton3);
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton2);
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton6);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(183, 294);
            this.splitContainerControl6.SplitterPosition = 34;
            this.splitContainerControl6.TabIndex = 6;
            this.splitContainerControl6.Text = "splitContainerControl1";
            // 
            // list_box_검사기
            // 
            this.list_box_검사기.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_검사기.FormattingEnabled = true;
            this.list_box_검사기.ItemHeight = 14;
            this.list_box_검사기.Location = new System.Drawing.Point(0, 0);
            this.list_box_검사기.Name = "list_box_검사기";
            this.list_box_검사기.Size = new System.Drawing.Size(183, 255);
            this.list_box_검사기.TabIndex = 4;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(55, 0);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(49, 34);
            this.simpleButton3.TabIndex = 0;
            this.simpleButton3.Text = "완료1";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(110, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(49, 34);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "완료0";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(0, 0);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(49, 34);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "clear";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // btn_save
            // 
            this.btn_save.Appearance.BackColor = System.Drawing.Color.Gold;
            this.btn_save.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_save.Appearance.Font = new System.Drawing.Font("Tahoma", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_save.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_save.Appearance.Options.UseBackColor = true;
            this.btn_save.Appearance.Options.UseFont = true;
            this.btn_save.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.btn_save, 2);
            this.btn_save.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_save.Location = new System.Drawing.Point(3, 451);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(500, 60);
            this.btn_save.TabIndex = 1;
            this.btn_save.Text = "실적등록";
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // winsock_검사기
            // 
            this.winsock_검사기.LocalPort = 80;
            this.winsock_검사기.RemoteIP = "127.0.0.1";
            this.winsock_검사기.RemotePort = 80;
            this.winsock_검사기.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_검사기);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(97, 2);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AutoHeight = false;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(210, 90);
            this.dateEdit1.TabIndex = 38;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(633, 3);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Size = new System.Drawing.Size(376, 46);
            this.textEdit1.TabIndex = 37;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            this.textEdit1.Leave += new System.EventHandler(this.textEdit1_Leave);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(93, 92);
            this.label47.TabIndex = 33;
            this.label47.Text = "생산일";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label47.Click += new System.EventHandler(this.label47_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(633, 0);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(82, 92);
            this.btn_po_release.TabIndex = 34;
            this.btn_po_release.Text = "작업\r\n검색";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Visible = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(313, 3);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(316, 89);
            this.lueWc_code.TabIndex = 35;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(612, 514);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 18F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IT_SCODE,
            this.IT_SNAME,
            this.PAB,
            this.RH_LH,
            this.SSB,
            this.PLAN_SQTY,
            this.GOOD_SQTY,
            this.FAIL_SQTY,
            this.SPEC,
            this.IT_PKQTY,
            this.TOTAL_CNT,
            this.CNT,
            this.gridColumn1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowColumnResizing = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowRowSizing = true;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView1.OptionsFind.AllowFindPanel = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView1.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView1.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 45;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품목코드";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.OptionsColumn.AllowEdit = false;
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 0;
            this.IT_SCODE.Width = 303;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.OptionsColumn.AllowEdit = false;
            this.IT_SNAME.Width = 260;
            // 
            // PAB
            // 
            this.PAB.AppearanceCell.Options.UseTextOptions = true;
            this.PAB.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PAB.Caption = "PAB";
            this.PAB.FieldName = "PAB";
            this.PAB.Name = "PAB";
            this.PAB.Width = 169;
            // 
            // RH_LH
            // 
            this.RH_LH.AppearanceCell.Options.UseTextOptions = true;
            this.RH_LH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RH_LH.Caption = "R/L";
            this.RH_LH.FieldName = "RH_LH";
            this.RH_LH.Name = "RH_LH";
            this.RH_LH.Width = 178;
            // 
            // SSB
            // 
            this.SSB.AppearanceCell.Options.UseTextOptions = true;
            this.SSB.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SSB.Caption = "SSB";
            this.SSB.FieldName = "SSB";
            this.SSB.Name = "SSB";
            this.SSB.Width = 98;
            // 
            // PLAN_SQTY
            // 
            this.PLAN_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.PLAN_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PLAN_SQTY.Caption = "계획";
            this.PLAN_SQTY.FieldName = "PLAN_SQTY";
            this.PLAN_SQTY.Name = "PLAN_SQTY";
            this.PLAN_SQTY.OptionsColumn.AllowEdit = false;
            this.PLAN_SQTY.Visible = true;
            this.PLAN_SQTY.VisibleIndex = 1;
            this.PLAN_SQTY.Width = 133;
            // 
            // GOOD_SQTY
            // 
            this.GOOD_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.GOOD_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GOOD_SQTY.Caption = "양품";
            this.GOOD_SQTY.FieldName = "GOOD_SQTY";
            this.GOOD_SQTY.Name = "GOOD_SQTY";
            this.GOOD_SQTY.OptionsColumn.AllowEdit = false;
            this.GOOD_SQTY.Visible = true;
            this.GOOD_SQTY.VisibleIndex = 2;
            this.GOOD_SQTY.Width = 127;
            // 
            // FAIL_SQTY
            // 
            this.FAIL_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.FAIL_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FAIL_SQTY.Caption = "불량";
            this.FAIL_SQTY.FieldName = "FAIL_SQTY";
            this.FAIL_SQTY.Name = "FAIL_SQTY";
            this.FAIL_SQTY.OptionsColumn.AllowEdit = false;
            this.FAIL_SQTY.Visible = true;
            this.FAIL_SQTY.VisibleIndex = 3;
            this.FAIL_SQTY.Width = 120;
            // 
            // SPEC
            // 
            this.SPEC.Caption = "사양(상수값)";
            this.SPEC.FieldName = "SPEC";
            this.SPEC.Name = "SPEC";
            this.SPEC.OptionsColumn.AllowEdit = false;
            this.SPEC.Width = 85;
            // 
            // IT_PKQTY
            // 
            this.IT_PKQTY.AppearanceCell.Options.UseTextOptions = true;
            this.IT_PKQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.IT_PKQTY.Caption = "용기";
            this.IT_PKQTY.FieldName = "IT_PKQTY";
            this.IT_PKQTY.Name = "IT_PKQTY";
            this.IT_PKQTY.OptionsColumn.AllowEdit = false;
            this.IT_PKQTY.Width = 95;
            // 
            // TOTAL_CNT
            // 
            this.TOTAL_CNT.AppearanceCell.Options.UseTextOptions = true;
            this.TOTAL_CNT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TOTAL_CNT.Caption = "검사카운트";
            this.TOTAL_CNT.FieldName = "TOTAL_CNT";
            this.TOTAL_CNT.Name = "TOTAL_CNT";
            this.TOTAL_CNT.Visible = true;
            this.TOTAL_CNT.VisibleIndex = 4;
            this.TOTAL_CNT.Width = 161;
            // 
            // CNT
            // 
            this.CNT.AppearanceCell.Options.UseTextOptions = true;
            this.CNT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CNT.Caption = "CNT";
            this.CNT.FieldName = "CNT";
            this.CNT.Name = "CNT";
            this.CNT.OptionsColumn.AllowEdit = false;
            this.CNT.Visible = true;
            this.CNT.VisibleIndex = 5;
            this.CNT.Width = 128;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "출력";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 130;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            serializableAppearanceObject1.BackColor = System.Drawing.Color.YellowGreen;
            serializableAppearanceObject1.BackColor2 = System.Drawing.Color.White;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            serializableAppearanceObject1.Options.UseBackColor = true;
            serializableAppearanceObject1.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "실적등록", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // lbl_now_date
            // 
            this.lbl_now_date.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lbl_now_date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_now_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_now_date.Location = new System.Drawing.Point(720, 0);
            this.lbl_now_date.Name = "lbl_now_date";
            this.lbl_now_date.Size = new System.Drawing.Size(347, 28);
            this.lbl_now_date.TabIndex = 40;
            this.lbl_now_date.Text = "labelControl1";
            // 
            // splitContainerControl8
            // 
            this.splitContainerControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl8.Horizontal = false;
            this.splitContainerControl8.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl8.Name = "splitContainerControl8";
            this.splitContainerControl8.Panel1.Controls.Add(this.simpleButton7);
            this.splitContainerControl8.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl8.Panel1.Controls.Add(this.textEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.simpleButton4);
            this.splitContainerControl8.Panel1.Controls.Add(this.panel2);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_program_finish);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_close);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_inspection);
            this.splitContainerControl8.Panel1.Controls.Add(this.simpleButton1);
            this.splitContainerControl8.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl8.Panel1.Controls.Add(this.label47);
            this.splitContainerControl8.Panel1.Controls.Add(this.lueWc_code);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_po_release);
            this.splitContainerControl8.Panel1.Controls.Add(this.dateEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.lbl_now_date);
            this.splitContainerControl8.Panel1.Text = "Panel1";
            this.splitContainerControl8.Panel2.Controls.Add(this.splitContainerControl9);
            this.splitContainerControl8.Panel2.Text = "Panel2";
            this.splitContainerControl8.Size = new System.Drawing.Size(1378, 780);
            this.splitContainerControl8.SplitterPosition = 93;
            this.splitContainerControl8.TabIndex = 41;
            this.splitContainerControl8.Text = "splitContainerControl8";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.simpleButton7.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton7.Location = new System.Drawing.Point(1411, 6);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(115, 86);
            this.simpleButton7.TabIndex = 105;
            this.simpleButton7.Text = "이력\r\n조회";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(633, 56);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(375, 36);
            this.labelControl1.TabIndex = 41;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.simpleButton4.Location = new System.Drawing.Point(1353, 6);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(52, 86);
            this.simpleButton4.TabIndex = 106;
            this.simpleButton4.Text = "설정";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.checkBox_가성불량);
            this.panel2.Location = new System.Drawing.Point(1243, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(103, 92);
            this.panel2.TabIndex = 40;
            this.panel2.Visible = false;
            // 
            // checkBox_가성불량
            // 
            this.checkBox_가성불량.AutoSize = true;
            this.checkBox_가성불량.Checked = true;
            this.checkBox_가성불량.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_가성불량.Location = new System.Drawing.Point(4, 32);
            this.checkBox_가성불량.Name = "checkBox_가성불량";
            this.checkBox_가성불량.Size = new System.Drawing.Size(98, 18);
            this.checkBox_가성불량.TabIndex = 0;
            this.checkBox_가성불량.Text = "가성불량사용";
            this.checkBox_가성불량.UseVisualStyleBackColor = true;
            // 
            // btn_program_finish
            // 
            this.btn_program_finish.Appearance.BackColor = System.Drawing.Color.Crimson;
            this.btn_program_finish.Appearance.BackColor2 = System.Drawing.Color.PaleVioletRed;
            this.btn_program_finish.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_program_finish.Appearance.Options.UseBackColor = true;
            this.btn_program_finish.Appearance.Options.UseFont = true;
            this.btn_program_finish.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_program_finish.Location = new System.Drawing.Point(1640, 6);
            this.btn_program_finish.Name = "btn_program_finish";
            this.btn_program_finish.Size = new System.Drawing.Size(113, 86);
            this.btn_program_finish.TabIndex = 104;
            this.btn_program_finish.Text = "작업종료";
            this.btn_program_finish.Click += new System.EventHandler(this.btn_program_finish_Click);
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_close.Appearance.BackColor2 = System.Drawing.Color.YellowGreen;
            this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Location = new System.Drawing.Point(1525, 6);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(115, 86);
            this.btn_close.TabIndex = 105;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btn_inspection
            // 
            this.btn_inspection.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_inspection.Appearance.Options.UseFont = true;
            this.btn_inspection.Location = new System.Drawing.Point(1218, 6);
            this.btn_inspection.Name = "btn_inspection";
            this.btn_inspection.Size = new System.Drawing.Size(128, 86);
            this.btn_inspection.TabIndex = 44;
            this.btn_inspection.Text = "초중종품\r\n등록";
            this.btn_inspection.Click += new System.EventHandler(this.btn_inspection_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.LemonChiffon;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton1.Location = new System.Drawing.Point(1149, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(63, 88);
            this.simpleButton1.TabIndex = 43;
            this.simpleButton1.Text = "새로\r\n고침";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 60F);
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(1015, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(128, 88);
            this.labelControl2.TabIndex = 42;
            // 
            // splitContainerControl9
            // 
            this.splitContainerControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl9.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl9.Horizontal = false;
            this.splitContainerControl9.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl9.Name = "splitContainerControl9";
            this.splitContainerControl9.Panel1.Controls.Add(this.panel1);
            this.splitContainerControl9.Panel1.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl9.Panel1.Text = "Panel1";
            this.splitContainerControl9.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl9.Panel2.Text = "Panel2";
            this.splitContainerControl9.Size = new System.Drawing.Size(1378, 682);
            this.splitContainerControl9.SplitterPosition = 163;
            this.splitContainerControl9.TabIndex = 40;
            this.splitContainerControl9.Text = "splitContainerControl9";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1378, 514);
            this.splitContainerControl1.SplitterPosition = 761;
            this.splitContainerControl1.TabIndex = 40;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_it_scode, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_save, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lbl_cnt, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbl_it_pkqty, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbl_fail_sqty, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbl_good_sqty, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_plan_sqty, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_pab, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_rh_lh, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_ssb, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.simpleButton5, 2, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.49953F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.50328F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(761, 514);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbl_it_scode
            // 
            this.lbl_it_scode.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_it_scode.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_it_scode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_it_scode.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_it_scode.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_it_scode, 3);
            this.lbl_it_scode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_it_scode.Location = new System.Drawing.Point(3, 3);
            this.lbl_it_scode.Name = "lbl_it_scode";
            this.lbl_it_scode.Size = new System.Drawing.Size(755, 58);
            this.lbl_it_scode.TabIndex = 0;
            // 
            // lbl_cnt
            // 
            this.lbl_cnt.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl_cnt.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_cnt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_cnt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cnt.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_cnt, 2);
            this.lbl_cnt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_cnt.Location = new System.Drawing.Point(256, 323);
            this.lbl_cnt.Name = "lbl_cnt";
            this.lbl_cnt.Size = new System.Drawing.Size(502, 58);
            this.lbl_cnt.TabIndex = 0;
            // 
            // lbl_it_pkqty
            // 
            this.lbl_it_pkqty.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl_it_pkqty.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_it_pkqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_it_pkqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_it_pkqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_it_pkqty, 2);
            this.lbl_it_pkqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_it_pkqty.Location = new System.Drawing.Point(256, 387);
            this.lbl_it_pkqty.Name = "lbl_it_pkqty";
            this.lbl_it_pkqty.Size = new System.Drawing.Size(502, 58);
            this.lbl_it_pkqty.TabIndex = 0;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl7.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(3, 323);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(247, 58);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "CNT";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl8.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(3, 387);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(247, 58);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "용기";
            // 
            // lbl_fail_sqty
            // 
            this.lbl_fail_sqty.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl_fail_sqty.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_fail_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_fail_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_fail_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_fail_sqty, 2);
            this.lbl_fail_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_fail_sqty.Location = new System.Drawing.Point(256, 259);
            this.lbl_fail_sqty.Name = "lbl_fail_sqty";
            this.lbl_fail_sqty.Size = new System.Drawing.Size(502, 58);
            this.lbl_fail_sqty.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl5.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(3, 259);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(247, 58);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "불량";
            // 
            // lbl_good_sqty
            // 
            this.lbl_good_sqty.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl_good_sqty.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_good_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_good_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_good_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_good_sqty, 2);
            this.lbl_good_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_good_sqty.Location = new System.Drawing.Point(256, 195);
            this.lbl_good_sqty.Name = "lbl_good_sqty";
            this.lbl_good_sqty.Size = new System.Drawing.Size(502, 58);
            this.lbl_good_sqty.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl4.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(3, 195);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(247, 58);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "양품";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(3, 131);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(247, 58);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "계획";
            // 
            // lbl_plan_sqty
            // 
            this.lbl_plan_sqty.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lbl_plan_sqty.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_plan_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_plan_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_plan_sqty.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl_plan_sqty, 2);
            this.lbl_plan_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_plan_sqty.Location = new System.Drawing.Point(256, 131);
            this.lbl_plan_sqty.Name = "lbl_plan_sqty";
            this.lbl_plan_sqty.Size = new System.Drawing.Size(502, 58);
            this.lbl_plan_sqty.TabIndex = 0;
            // 
            // lbl_pab
            // 
            this.lbl_pab.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_pab.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_pab.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_pab.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_pab.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_pab.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.lbl_pab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_pab.Location = new System.Drawing.Point(3, 67);
            this.lbl_pab.Name = "lbl_pab";
            this.lbl_pab.Size = new System.Drawing.Size(247, 58);
            this.lbl_pab.TabIndex = 3;
            // 
            // lbl_rh_lh
            // 
            this.lbl_rh_lh.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_rh_lh.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_rh_lh.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_rh_lh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_rh_lh.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_rh_lh.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.lbl_rh_lh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_rh_lh.Location = new System.Drawing.Point(256, 67);
            this.lbl_rh_lh.Name = "lbl_rh_lh";
            this.lbl_rh_lh.Size = new System.Drawing.Size(247, 58);
            this.lbl_rh_lh.TabIndex = 3;
            // 
            // lbl_ssb
            // 
            this.lbl_ssb.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.lbl_ssb.Appearance.Font = new System.Drawing.Font("굴림", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbl_ssb.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbl_ssb.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_ssb.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_ssb.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.lbl_ssb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_ssb.Location = new System.Drawing.Point(509, 67);
            this.lbl_ssb.Name = "lbl_ssb";
            this.lbl_ssb.Size = new System.Drawing.Size(249, 58);
            this.lbl_ssb.TabIndex = 3;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.simpleButton5.Appearance.BackColor2 = System.Drawing.Color.White;
            this.simpleButton5.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton5.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton5.Location = new System.Drawing.Point(509, 451);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(249, 60);
            this.simpleButton5.TabIndex = 4;
            this.simpleButton5.Text = "식별표\r\n재출력";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1378, 163);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.READING_DATA,
            this.S_DATE,
            this.RESULT,
            this.NG_DESCR});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.Column = this.RESULT;
            gridFormatRule1.ColumnApplyTo = this.RESULT;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Salmon;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Expression = "[RESULT] = \'NG\'";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.Column = this.RESULT;
            gridFormatRule2.ColumnApplyTo = this.RESULT;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Expression = "[RESULT] = \'OK\'";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsBehavior.ReadOnly = true;
            this.gridView2.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // READING_DATA
            // 
            this.READING_DATA.Caption = "리딩 데이터";
            this.READING_DATA.FieldName = "READING_DATA";
            this.READING_DATA.Name = "READING_DATA";
            this.READING_DATA.Visible = true;
            this.READING_DATA.VisibleIndex = 0;
            this.READING_DATA.Width = 614;
            // 
            // S_DATE
            // 
            this.S_DATE.Caption = "DATE";
            this.S_DATE.FieldName = "S_DATE";
            this.S_DATE.Name = "S_DATE";
            this.S_DATE.Width = 429;
            // 
            // NG_DESCR
            // 
            this.NG_DESCR.Caption = "NG참조";
            this.NG_DESCR.FieldName = "NG_DESCR";
            this.NG_DESCR.Name = "NG_DESCR";
            this.NG_DESCR.Visible = true;
            this.NG_DESCR.VisibleIndex = 2;
            this.NG_DESCR.Width = 334;
            // 
            // message_timer_clear
            // 
            this.message_timer_clear.Tick += new System.EventHandler(this.message_timer_clear_Tick);
            // 
            // timer_send_check
            // 
            this.timer_send_check.Interval = 1000;
            this.timer_send_check.Tick += new System.EventHandler(this.timer_send_check_Tick);
            // 
            // AUTO_JA_OUT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1378, 780);
            this.Controls.Add(this.splitContainerControl8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_JA_OUT";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).EndInit();
            this.splitContainerControl8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).EndInit();
            this.splitContainerControl9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer_시계;
        private System.Windows.Forms.Timer read_send_timer;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private System.Windows.Forms.ListBox list_box_검사기;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private MelsecPLC.Winsock winsock_검사기;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btn_po_release;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl lbl_now_date;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn PLAN_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn GOOD_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn FAIL_SQTY;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl8;
        private System.Windows.Forms.Timer message_timer_clear;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl9;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn READING_DATA;
        private DevExpress.XtraGrid.Columns.GridColumn S_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn RESULT;
        private DevExpress.XtraGrid.Columns.GridColumn SPEC;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn IT_PKQTY;
        private DevExpress.XtraGrid.Columns.GridColumn CNT;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraGrid.Columns.GridColumn NG_DESCR;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SimpleButton btn_inspection;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btn_program_finish;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private System.Windows.Forms.Timer timer_send_check;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkBox_가성불량;
        private DevExpress.XtraGrid.Columns.GridColumn PAB;
        private DevExpress.XtraGrid.Columns.GridColumn RH_LH;
        private DevExpress.XtraGrid.Columns.GridColumn SSB;
        private DevExpress.XtraGrid.Columns.GridColumn TOTAL_CNT;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lbl_it_scode;
        private DevExpress.XtraEditors.LabelControl lbl_cnt;
        private DevExpress.XtraEditors.LabelControl lbl_it_pkqty;
        private DevExpress.XtraEditors.SimpleButton btn_save;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lbl_fail_sqty;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbl_good_sqty;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lbl_plan_sqty;
        private DevExpress.XtraEditors.LabelControl lbl_pab;
        private DevExpress.XtraEditors.LabelControl lbl_rh_lh;
        private DevExpress.XtraEditors.LabelControl lbl_ssb;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
    }
}

