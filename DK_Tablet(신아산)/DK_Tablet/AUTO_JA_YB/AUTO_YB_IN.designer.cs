﻿namespace DK_Tablet
{
    partial class AUTO_YB_IN
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.RESULT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.list_box_진동 = new System.Windows.Forms.ListBox();
            this.timer_시계 = new System.Windows.Forms.Timer(this.components);
            this.winsock_진동 = new MelsecPLC.Winsock();
            this.read_send_timer = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_진동 = new System.Windows.Forms.Timer(this.components);
            this.list_box_초음파2 = new System.Windows.Forms.ListBox();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainerControl10 = new DevExpress.XtraEditors.SplitContainerControl();
            this.lbl_f20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_f1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_w4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_w3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_w2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_w1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_OP = new System.Windows.Forms.ListBox();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.label6 = new System.Windows.Forms.Label();
            this.TEST_검사_out = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.TEST_초음파2_out = new System.Windows.Forms.Button();
            this.TEST_검사_in = new System.Windows.Forms.Button();
            this.TEST_진동2_out = new System.Windows.Forms.Button();
            this.TEXT_진동1_out = new System.Windows.Forms.Button();
            this.TEST_CLIP_in = new System.Windows.Forms.Button();
            this.TEXT_초음파1_out = new System.Windows.Forms.Button();
            this.TEST_초음파2_in = new System.Windows.Forms.Button();
            this.TEST_CLIP_out = new System.Windows.Forms.Button();
            this.TEXT_초음파1_in = new System.Windows.Forms.Button();
            this.TEXT_진동1_in = new System.Windows.Forms.Button();
            this.TEST_진동2_in = new System.Windows.Forms.Button();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_레이져컷팅 = new System.Windows.Forms.ListBox();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_초음파1 = new System.Windows.Forms.ListBox();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_클립조립 = new System.Windows.Forms.ListBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.TEST_LASER_in_B = new System.Windows.Forms.Button();
            this.TEST_LASER_in_A = new System.Windows.Forms.Button();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_검사기 = new System.Windows.Forms.ListBox();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.TEST_LASER_out_B = new System.Windows.Forms.Button();
            this.TEST_LASER_out_A = new System.Windows.Forms.Button();
            this.winsock_OP = new MelsecPLC.Winsock();
            this.winsock_레이져컷팅 = new MelsecPLC.Winsock();
            this.Refresh_timer_레이져컷팅 = new System.Windows.Forms.Timer(this.components);
            this.winsock_클립조립 = new MelsecPLC.Winsock();
            this.Refresh_timer_클립조립 = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_OP = new System.Windows.Forms.Timer(this.components);
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.btnClose = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.btn_po_release = new System.Windows.Forms.Button();
            this.lueWc_code = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ORDER_NUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PLAN_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.INTO_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NOZZLE_CNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GOOD_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FAIL_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SPEC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MO_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl_now_date = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl8 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btn_re_reading = new DevExpress.XtraEditors.SimpleButton();
            this.button1 = new System.Windows.Forms.Button();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.splitContainerControl9 = new DevExpress.XtraEditors.SplitContainerControl();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.READING_DATA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.S_DATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.message_timer_clear = new System.Windows.Forms.Timer(this.components);
            this.read_send_time_2 = new System.Windows.Forms.Timer(this.components);
            this.read_send_timer_op = new System.Windows.Forms.Timer(this.components);
            this.timer_지멘스 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl10)).BeginInit();
            this.splitContainerControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).BeginInit();
            this.splitContainerControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).BeginInit();
            this.splitContainerControl9.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // RESULT
            // 
            this.RESULT.AppearanceCell.Options.UseTextOptions = true;
            this.RESULT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RESULT.Caption = "OK/NG";
            this.RESULT.FieldName = "RESULT";
            this.RESULT.Name = "RESULT";
            this.RESULT.Visible = true;
            this.RESULT.VisibleIndex = 1;
            this.RESULT.Width = 125;
            // 
            // list_box_진동
            // 
            this.list_box_진동.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_진동.FormattingEnabled = true;
            this.list_box_진동.ItemHeight = 14;
            this.list_box_진동.Location = new System.Drawing.Point(0, 0);
            this.list_box_진동.Name = "list_box_진동";
            this.list_box_진동.Size = new System.Drawing.Size(157, 255);
            this.list_box_진동.TabIndex = 4;
            // 
            // timer_시계
            // 
            this.timer_시계.Enabled = true;
            this.timer_시계.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // winsock_진동
            // 
            this.winsock_진동.LocalPort = 80;
            this.winsock_진동.RemoteIP = "127.0.0.1";
            this.winsock_진동.RemotePort = 80;
            this.winsock_진동.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_진동융착);
            // 
            // read_send_timer
            // 
            this.read_send_timer.Interval = 200;
            this.read_send_timer.Tick += new System.EventHandler(this.read_send_timer_Tick);
            // 
            // Refresh_timer_진동
            // 
            this.Refresh_timer_진동.Interval = 200;
            this.Refresh_timer_진동.Tick += new System.EventHandler(this.Refresh_timer_진동_Tick);
            // 
            // list_box_초음파2
            // 
            this.list_box_초음파2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_초음파2.FormattingEnabled = true;
            this.list_box_초음파2.ItemHeight = 14;
            this.list_box_초음파2.Location = new System.Drawing.Point(0, 0);
            this.list_box_초음파2.Name = "list_box_초음파2";
            this.list_box_초음파2.Size = new System.Drawing.Size(157, 255);
            this.list_box_초음파2.TabIndex = 4;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(492, 3);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.list_box_진동);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl2.SplitterPosition = 34;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            this.splitContainerControl2.Visible = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton1.Location = new System.Drawing.Point(0, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(157, 34);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "clear";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(655, 3);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.list_box_초음파2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.simpleButton2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl1.SplitterPosition = 34;
            this.splitContainerControl1.TabIndex = 6;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.Visible = false;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton2.Location = new System.Drawing.Point(0, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(157, 34);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "clear";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainerControl10);
            this.panel1.Controls.Add(this.splitContainerControl7);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.splitContainerControl4);
            this.panel1.Controls.Add(this.splitContainerControl5);
            this.panel1.Controls.Add(this.splitContainerControl3);
            this.panel1.Controls.Add(this.splitContainerControl2);
            this.panel1.Controls.Add(this.TEST_LASER_in_B);
            this.panel1.Controls.Add(this.TEST_LASER_in_A);
            this.panel1.Controls.Add(this.splitContainerControl6);
            this.panel1.Controls.Add(this.TEST_LASER_out_B);
            this.panel1.Controls.Add(this.splitContainerControl1);
            this.panel1.Controls.Add(this.TEST_LASER_out_A);
            this.panel1.Location = new System.Drawing.Point(3, 209);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 306);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // splitContainerControl10
            // 
            this.splitContainerControl10.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl10.Location = new System.Drawing.Point(194, 6);
            this.splitContainerControl10.Name = "splitContainerControl10";
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f20);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl24);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f19);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl23);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f18);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl22);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f17);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl21);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f16);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl20);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f15);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl19);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f14);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl18);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f13);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl17);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f12);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl16);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f11);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl15);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f10);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl14);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f9);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl13);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f8);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl12);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f7);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl11);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f6);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl10);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f5);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl9);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f4);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl8);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f3);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl7);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f2);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl6);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_f1);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl5);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_w4);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl4);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_w3);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_w2);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl10.Panel1.Controls.Add(this.lbl_w1);
            this.splitContainerControl10.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl10.Panel1.Text = "Panel1";
            this.splitContainerControl10.Panel2.Controls.Add(this.listBox1);
            this.splitContainerControl10.Panel2.Text = "Panel2";
            this.splitContainerControl10.Size = new System.Drawing.Size(803, 251);
            this.splitContainerControl10.SplitterPosition = 353;
            this.splitContainerControl10.TabIndex = 43;
            this.splitContainerControl10.Text = "splitContainerControl10";
            // 
            // lbl_f20
            // 
            this.lbl_f20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f20.Location = new System.Drawing.Point(381, 223);
            this.lbl_f20.Name = "lbl_f20";
            this.lbl_f20.Size = new System.Drawing.Size(60, 14);
            this.lbl_f20.TabIndex = 33;
            // 
            // labelControl24
            // 
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl24.Location = new System.Drawing.Point(226, 223);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(149, 14);
            this.labelControl24.TabIndex = 54;
            this.labelControl24.Text = "Welding Time Maximum";
            // 
            // lbl_f19
            // 
            this.lbl_f19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f19.Location = new System.Drawing.Point(381, 203);
            this.lbl_f19.Name = "lbl_f19";
            this.lbl_f19.Size = new System.Drawing.Size(60, 14);
            this.lbl_f19.TabIndex = 53;
            // 
            // labelControl23
            // 
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl23.Location = new System.Drawing.Point(226, 203);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(149, 14);
            this.labelControl23.TabIndex = 52;
            this.labelControl23.Text = "Welding Time Minimum";
            // 
            // lbl_f18
            // 
            this.lbl_f18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f18.Location = new System.Drawing.Point(381, 183);
            this.lbl_f18.Name = "lbl_f18";
            this.lbl_f18.Size = new System.Drawing.Size(60, 14);
            this.lbl_f18.TabIndex = 51;
            // 
            // labelControl22
            // 
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl22.Location = new System.Drawing.Point(226, 183);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(149, 14);
            this.labelControl22.TabIndex = 50;
            this.labelControl22.Text = "Welding Depth Maximum";
            // 
            // lbl_f17
            // 
            this.lbl_f17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f17.Location = new System.Drawing.Point(381, 163);
            this.lbl_f17.Name = "lbl_f17";
            this.lbl_f17.Size = new System.Drawing.Size(60, 14);
            this.lbl_f17.TabIndex = 49;
            // 
            // labelControl21
            // 
            this.labelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl21.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl21.Location = new System.Drawing.Point(226, 163);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(149, 14);
            this.labelControl21.TabIndex = 48;
            this.labelControl21.Text = "Welding Depth Minimum";
            // 
            // lbl_f16
            // 
            this.lbl_f16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f16.Location = new System.Drawing.Point(381, 143);
            this.lbl_f16.Name = "lbl_f16";
            this.lbl_f16.Size = new System.Drawing.Size(60, 14);
            this.lbl_f16.TabIndex = 47;
            // 
            // labelControl20
            // 
            this.labelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl20.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl20.Location = new System.Drawing.Point(226, 143);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(149, 14);
            this.labelControl20.TabIndex = 46;
            this.labelControl20.Text = "Dwel Pressure Actual";
            // 
            // lbl_f15
            // 
            this.lbl_f15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f15.Location = new System.Drawing.Point(381, 123);
            this.lbl_f15.Name = "lbl_f15";
            this.lbl_f15.Size = new System.Drawing.Size(60, 14);
            this.lbl_f15.TabIndex = 45;
            // 
            // labelControl19
            // 
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl19.Location = new System.Drawing.Point(226, 123);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(149, 14);
            this.labelControl19.TabIndex = 44;
            this.labelControl19.Text = "Dwel Time Actual";
            // 
            // lbl_f14
            // 
            this.lbl_f14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f14.Location = new System.Drawing.Point(381, 103);
            this.lbl_f14.Name = "lbl_f14";
            this.lbl_f14.Size = new System.Drawing.Size(60, 14);
            this.lbl_f14.TabIndex = 43;
            // 
            // labelControl18
            // 
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl18.Location = new System.Drawing.Point(226, 103);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(149, 14);
            this.labelControl18.TabIndex = 42;
            this.labelControl18.Text = "Dwel Pressure Reference";
            // 
            // lbl_f13
            // 
            this.lbl_f13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f13.Location = new System.Drawing.Point(381, 83);
            this.lbl_f13.Name = "lbl_f13";
            this.lbl_f13.Size = new System.Drawing.Size(60, 14);
            this.lbl_f13.TabIndex = 41;
            // 
            // labelControl17
            // 
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl17.Location = new System.Drawing.Point(226, 83);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(149, 14);
            this.labelControl17.TabIndex = 40;
            this.labelControl17.Text = "Dwel Time Reference";
            // 
            // lbl_f12
            // 
            this.lbl_f12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f12.Location = new System.Drawing.Point(381, 63);
            this.lbl_f12.Name = "lbl_f12";
            this.lbl_f12.Size = new System.Drawing.Size(60, 14);
            this.lbl_f12.TabIndex = 39;
            // 
            // labelControl16
            // 
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl16.Location = new System.Drawing.Point(226, 63);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(149, 14);
            this.labelControl16.TabIndex = 38;
            this.labelControl16.Text = "Joining Pressure Actual";
            // 
            // lbl_f11
            // 
            this.lbl_f11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f11.Location = new System.Drawing.Point(381, 43);
            this.lbl_f11.Name = "lbl_f11";
            this.lbl_f11.Size = new System.Drawing.Size(60, 14);
            this.lbl_f11.TabIndex = 37;
            // 
            // labelControl15
            // 
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl15.Location = new System.Drawing.Point(226, 43);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(149, 14);
            this.labelControl15.TabIndex = 36;
            this.labelControl15.Text = "Joining Time Actual";
            // 
            // lbl_f10
            // 
            this.lbl_f10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f10.Location = new System.Drawing.Point(381, 23);
            this.lbl_f10.Name = "lbl_f10";
            this.lbl_f10.Size = new System.Drawing.Size(60, 14);
            this.lbl_f10.TabIndex = 35;
            // 
            // labelControl14
            // 
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl14.Location = new System.Drawing.Point(226, 23);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(149, 14);
            this.labelControl14.TabIndex = 34;
            this.labelControl14.Text = "Joining Pressure Reference";
            // 
            // lbl_f9
            // 
            this.lbl_f9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f9.Location = new System.Drawing.Point(381, 3);
            this.lbl_f9.Name = "lbl_f9";
            this.lbl_f9.Size = new System.Drawing.Size(60, 14);
            this.lbl_f9.TabIndex = 55;
            // 
            // labelControl13
            // 
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl13.Location = new System.Drawing.Point(226, 3);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(149, 14);
            this.labelControl13.TabIndex = 56;
            this.labelControl13.Text = "Joining Time Reference";
            // 
            // lbl_f8
            // 
            this.lbl_f8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f8.Location = new System.Drawing.Point(160, 223);
            this.lbl_f8.Name = "lbl_f8";
            this.lbl_f8.Size = new System.Drawing.Size(60, 14);
            this.lbl_f8.TabIndex = 9;
            // 
            // labelControl12
            // 
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl12.Location = new System.Drawing.Point(5, 223);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(149, 14);
            this.labelControl12.TabIndex = 30;
            this.labelControl12.Text = "Friction Pressure Actual";
            // 
            // lbl_f7
            // 
            this.lbl_f7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f7.Location = new System.Drawing.Point(160, 203);
            this.lbl_f7.Name = "lbl_f7";
            this.lbl_f7.Size = new System.Drawing.Size(60, 14);
            this.lbl_f7.TabIndex = 29;
            // 
            // labelControl11
            // 
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl11.Location = new System.Drawing.Point(5, 203);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(149, 14);
            this.labelControl11.TabIndex = 28;
            this.labelControl11.Text = "Friction Time Actual";
            // 
            // lbl_f6
            // 
            this.lbl_f6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f6.Location = new System.Drawing.Point(160, 183);
            this.lbl_f6.Name = "lbl_f6";
            this.lbl_f6.Size = new System.Drawing.Size(60, 14);
            this.lbl_f6.TabIndex = 27;
            // 
            // labelControl10
            // 
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl10.Location = new System.Drawing.Point(5, 183);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(149, 14);
            this.labelControl10.TabIndex = 26;
            this.labelControl10.Text = "Friction Pressure Reference";
            // 
            // lbl_f5
            // 
            this.lbl_f5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f5.Location = new System.Drawing.Point(160, 163);
            this.lbl_f5.Name = "lbl_f5";
            this.lbl_f5.Size = new System.Drawing.Size(60, 14);
            this.lbl_f5.TabIndex = 25;
            // 
            // labelControl9
            // 
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl9.Location = new System.Drawing.Point(5, 163);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(149, 14);
            this.labelControl9.TabIndex = 24;
            this.labelControl9.Text = "Friction Time Reference";
            // 
            // lbl_f4
            // 
            this.lbl_f4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f4.Location = new System.Drawing.Point(160, 143);
            this.lbl_f4.Name = "lbl_f4";
            this.lbl_f4.Size = new System.Drawing.Size(60, 14);
            this.lbl_f4.TabIndex = 23;
            // 
            // labelControl8
            // 
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl8.Location = new System.Drawing.Point(5, 143);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(149, 14);
            this.labelControl8.TabIndex = 22;
            this.labelControl8.Text = "Amplitude Actual";
            // 
            // lbl_f3
            // 
            this.lbl_f3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f3.Location = new System.Drawing.Point(160, 123);
            this.lbl_f3.Name = "lbl_f3";
            this.lbl_f3.Size = new System.Drawing.Size(60, 14);
            this.lbl_f3.TabIndex = 21;
            // 
            // labelControl7
            // 
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl7.Location = new System.Drawing.Point(5, 123);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(149, 14);
            this.labelControl7.TabIndex = 20;
            this.labelControl7.Text = "Welding Depth Actual";
            // 
            // lbl_f2
            // 
            this.lbl_f2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f2.Location = new System.Drawing.Point(160, 103);
            this.lbl_f2.Name = "lbl_f2";
            this.lbl_f2.Size = new System.Drawing.Size(60, 14);
            this.lbl_f2.TabIndex = 19;
            // 
            // labelControl6
            // 
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl6.Location = new System.Drawing.Point(5, 103);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(149, 14);
            this.labelControl6.TabIndex = 18;
            this.labelControl6.Text = "Amplitude Reference";
            // 
            // lbl_f1
            // 
            this.lbl_f1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_f1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_f1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_f1.Location = new System.Drawing.Point(160, 83);
            this.lbl_f1.Name = "lbl_f1";
            this.lbl_f1.Size = new System.Drawing.Size(60, 14);
            this.lbl_f1.TabIndex = 17;
            // 
            // labelControl5
            // 
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl5.Location = new System.Drawing.Point(5, 83);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(149, 14);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Welding Depth Reference";
            // 
            // lbl_w4
            // 
            this.lbl_w4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_w4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_w4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_w4.Location = new System.Drawing.Point(160, 63);
            this.lbl_w4.Name = "lbl_w4";
            this.lbl_w4.Size = new System.Drawing.Size(60, 14);
            this.lbl_w4.TabIndex = 15;
            // 
            // labelControl4
            // 
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl4.Location = new System.Drawing.Point(5, 63);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(149, 14);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Welding Mode";
            // 
            // lbl_w3
            // 
            this.lbl_w3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_w3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_w3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_w3.Location = new System.Drawing.Point(160, 43);
            this.lbl_w3.Name = "lbl_w3";
            this.lbl_w3.Size = new System.Drawing.Size(60, 14);
            this.lbl_w3.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl3.Location = new System.Drawing.Point(5, 43);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(149, 14);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "융착 판정 : 불량";
            // 
            // lbl_w2
            // 
            this.lbl_w2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_w2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_w2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_w2.Location = new System.Drawing.Point(160, 23);
            this.lbl_w2.Name = "lbl_w2";
            this.lbl_w2.Size = new System.Drawing.Size(60, 14);
            this.lbl_w2.TabIndex = 11;
            // 
            // labelControl2
            // 
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl2.Location = new System.Drawing.Point(5, 23);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(149, 14);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "융착 판정 : 양품";
            // 
            // lbl_w1
            // 
            this.lbl_w1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_w1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_w1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.lbl_w1.Location = new System.Drawing.Point(160, 3);
            this.lbl_w1.Name = "lbl_w1";
            this.lbl_w1.Size = new System.Drawing.Size(60, 14);
            this.lbl_w1.TabIndex = 31;
            // 
            // labelControl1
            // 
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(5, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(149, 14);
            this.labelControl1.TabIndex = 32;
            this.labelControl1.Text = "Machine Identification No";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 14;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(353, 251);
            this.listBox1.TabIndex = 40;
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl7.Horizontal = false;
            this.splitContainerControl7.Location = new System.Drawing.Point(6, 3);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.Controls.Add(this.list_box_OP);
            this.splitContainerControl7.Panel1.Text = "Panel1";
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton9);
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton8);
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton7);
            this.splitContainerControl7.Panel2.Text = "Panel2";
            this.splitContainerControl7.Size = new System.Drawing.Size(182, 294);
            this.splitContainerControl7.SplitterPosition = 34;
            this.splitContainerControl7.TabIndex = 6;
            this.splitContainerControl7.Text = "splitContainerControl1";
            // 
            // list_box_OP
            // 
            this.list_box_OP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_OP.FormattingEnabled = true;
            this.list_box_OP.ItemHeight = 14;
            this.list_box_OP.Location = new System.Drawing.Point(0, 0);
            this.list_box_OP.Name = "list_box_OP";
            this.list_box_OP.Size = new System.Drawing.Size(182, 255);
            this.list_box_OP.TabIndex = 4;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(54, 0);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(49, 34);
            this.simpleButton9.TabIndex = 0;
            this.simpleButton9.Text = "con\r\nclose";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(3, 0);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(49, 34);
            this.simpleButton8.TabIndex = 0;
            this.simpleButton8.Text = "read";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(105, 0);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(49, 34);
            this.simpleButton7.TabIndex = 0;
            this.simpleButton7.Text = "clear";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Controls.Add(this.simpleButton14, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_out, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_초음파2_out, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_검사_in, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_진동2_out, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_진동1_out, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_CLIP_in, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_초음파1_out, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEST_초음파2_in, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_CLIP_out, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_초음파1_in, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEXT_진동1_in, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.TEST_진동2_in, 5, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(26, 303);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(873, 79);
            this.tableLayoutPanel1.TabIndex = 42;
            this.tableLayoutPanel1.Visible = false;
            // 
            // simpleButton14
            // 
            this.simpleButton14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton14.Location = new System.Drawing.Point(113, 4);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(102, 32);
            this.simpleButton14.TabIndex = 42;
            this.simpleButton14.Text = "알람테스트";
            this.simpleButton14.Click += new System.EventHandler(this.simpleButton14_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("굴림", 25F);
            this.label6.Location = new System.Drawing.Point(1, 1);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 38);
            this.label6.TabIndex = 40;
            this.label6.Text = "in";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEST_검사_out
            // 
            this.TEST_검사_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_검사_out.Location = new System.Drawing.Point(767, 43);
            this.TEST_검사_out.Name = "TEST_검사_out";
            this.TEST_검사_out.Size = new System.Drawing.Size(102, 32);
            this.TEST_검사_out.TabIndex = 39;
            this.TEST_검사_out.Text = "검사";
            this.TEST_검사_out.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("굴림", 25F);
            this.label9.Location = new System.Drawing.Point(1, 40);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 38);
            this.label9.TabIndex = 40;
            this.label9.Text = "out";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TEST_초음파2_out
            // 
            this.TEST_초음파2_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_초음파2_out.Location = new System.Drawing.Point(658, 43);
            this.TEST_초음파2_out.Name = "TEST_초음파2_out";
            this.TEST_초음파2_out.Size = new System.Drawing.Size(102, 32);
            this.TEST_초음파2_out.TabIndex = 39;
            this.TEST_초음파2_out.Text = "초음파융착2";
            this.TEST_초음파2_out.UseVisualStyleBackColor = true;
            this.TEST_초음파2_out.Click += new System.EventHandler(this.TEST_LASER_out_Click);
            // 
            // TEST_검사_in
            // 
            this.TEST_검사_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_검사_in.Location = new System.Drawing.Point(767, 4);
            this.TEST_검사_in.Name = "TEST_검사_in";
            this.TEST_검사_in.Size = new System.Drawing.Size(102, 32);
            this.TEST_검사_in.TabIndex = 39;
            this.TEST_검사_in.Text = "검사";
            this.TEST_검사_in.UseVisualStyleBackColor = true;
            // 
            // TEST_진동2_out
            // 
            this.TEST_진동2_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_진동2_out.Location = new System.Drawing.Point(549, 43);
            this.TEST_진동2_out.Name = "TEST_진동2_out";
            this.TEST_진동2_out.Size = new System.Drawing.Size(102, 32);
            this.TEST_진동2_out.TabIndex = 39;
            this.TEST_진동2_out.Text = "진동융착2";
            this.TEST_진동2_out.UseVisualStyleBackColor = true;
            // 
            // TEXT_진동1_out
            // 
            this.TEXT_진동1_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_진동1_out.Location = new System.Drawing.Point(440, 43);
            this.TEXT_진동1_out.Name = "TEXT_진동1_out";
            this.TEXT_진동1_out.Size = new System.Drawing.Size(102, 32);
            this.TEXT_진동1_out.TabIndex = 39;
            this.TEXT_진동1_out.Text = "진동융착1";
            this.TEXT_진동1_out.UseVisualStyleBackColor = true;
            this.TEXT_진동1_out.Click += new System.EventHandler(this.TEST_LASER_out_Click);
            // 
            // TEST_CLIP_in
            // 
            this.TEST_CLIP_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_CLIP_in.Location = new System.Drawing.Point(222, 4);
            this.TEST_CLIP_in.Name = "TEST_CLIP_in";
            this.TEST_CLIP_in.Size = new System.Drawing.Size(102, 32);
            this.TEST_CLIP_in.TabIndex = 39;
            this.TEST_CLIP_in.Text = "클립조립";
            this.TEST_CLIP_in.UseVisualStyleBackColor = true;
            this.TEST_CLIP_in.Click += new System.EventHandler(this.TEST_LASER_in_Click);
            // 
            // TEXT_초음파1_out
            // 
            this.TEXT_초음파1_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_초음파1_out.Location = new System.Drawing.Point(331, 43);
            this.TEXT_초음파1_out.Name = "TEXT_초음파1_out";
            this.TEXT_초음파1_out.Size = new System.Drawing.Size(102, 32);
            this.TEXT_초음파1_out.TabIndex = 39;
            this.TEXT_초음파1_out.Text = "초음파1";
            this.TEXT_초음파1_out.UseVisualStyleBackColor = true;
            this.TEXT_초음파1_out.Click += new System.EventHandler(this.TEST_LASER_out_Click);
            // 
            // TEST_초음파2_in
            // 
            this.TEST_초음파2_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_초음파2_in.Location = new System.Drawing.Point(658, 4);
            this.TEST_초음파2_in.Name = "TEST_초음파2_in";
            this.TEST_초음파2_in.Size = new System.Drawing.Size(102, 32);
            this.TEST_초음파2_in.TabIndex = 39;
            this.TEST_초음파2_in.Text = "초음파융착2";
            this.TEST_초음파2_in.UseVisualStyleBackColor = true;
            this.TEST_초음파2_in.Click += new System.EventHandler(this.TEST_LASER_in_Click);
            // 
            // TEST_CLIP_out
            // 
            this.TEST_CLIP_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_CLIP_out.Location = new System.Drawing.Point(222, 43);
            this.TEST_CLIP_out.Name = "TEST_CLIP_out";
            this.TEST_CLIP_out.Size = new System.Drawing.Size(102, 32);
            this.TEST_CLIP_out.TabIndex = 39;
            this.TEST_CLIP_out.Text = "클립조립";
            this.TEST_CLIP_out.UseVisualStyleBackColor = true;
            this.TEST_CLIP_out.Click += new System.EventHandler(this.TEST_LASER_out_Click);
            // 
            // TEXT_초음파1_in
            // 
            this.TEXT_초음파1_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_초음파1_in.Location = new System.Drawing.Point(331, 4);
            this.TEXT_초음파1_in.Name = "TEXT_초음파1_in";
            this.TEXT_초음파1_in.Size = new System.Drawing.Size(102, 32);
            this.TEXT_초음파1_in.TabIndex = 39;
            this.TEXT_초음파1_in.Text = "초음파1";
            this.TEXT_초음파1_in.UseVisualStyleBackColor = true;
            this.TEXT_초음파1_in.Click += new System.EventHandler(this.TEST_LASER_in_Click);
            // 
            // TEXT_진동1_in
            // 
            this.TEXT_진동1_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEXT_진동1_in.Location = new System.Drawing.Point(440, 4);
            this.TEXT_진동1_in.Name = "TEXT_진동1_in";
            this.TEXT_진동1_in.Size = new System.Drawing.Size(102, 32);
            this.TEXT_진동1_in.TabIndex = 39;
            this.TEXT_진동1_in.Text = "진동융착1";
            this.TEXT_진동1_in.UseVisualStyleBackColor = true;
            this.TEXT_진동1_in.Click += new System.EventHandler(this.TEST_LASER_in_Click);
            // 
            // TEST_진동2_in
            // 
            this.TEST_진동2_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEST_진동2_in.Location = new System.Drawing.Point(549, 4);
            this.TEST_진동2_in.Name = "TEST_진동2_in";
            this.TEST_진동2_in.Size = new System.Drawing.Size(102, 32);
            this.TEST_진동2_in.TabIndex = 39;
            this.TEST_진동2_in.Text = "진동융착2";
            this.TEST_진동2_in.UseVisualStyleBackColor = true;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(3, 3);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.list_box_레이져컷팅);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton12);
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton13);
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton10);
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton11);
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton4);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.ShowCaption = true;
            this.splitContainerControl4.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl4.SplitterPosition = 34;
            this.splitContainerControl4.TabIndex = 5;
            this.splitContainerControl4.Text = "splitContainerControl2";
            // 
            // list_box_레이져컷팅
            // 
            this.list_box_레이져컷팅.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_레이져컷팅.FormattingEnabled = true;
            this.list_box_레이져컷팅.ItemHeight = 14;
            this.list_box_레이져컷팅.Location = new System.Drawing.Point(0, 0);
            this.list_box_레이져컷팅.Name = "list_box_레이져컷팅";
            this.list_box_레이져컷팅.Size = new System.Drawing.Size(157, 255);
            this.list_box_레이져컷팅.TabIndex = 4;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(121, -3);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(36, 34);
            this.simpleButton12.TabIndex = 0;
            this.simpleButton12.Text = "면 2";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click_1);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Location = new System.Drawing.Point(79, -2);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(36, 34);
            this.simpleButton13.TabIndex = 0;
            this.simpleButton13.Text = "면 1";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(37, -2);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(36, 34);
            this.simpleButton10.TabIndex = 0;
            this.simpleButton10.Text = "리셋";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(363, 1);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(57, 34);
            this.simpleButton11.TabIndex = 0;
            this.simpleButton11.Text = "연결해제";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(0, 0);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(31, 31);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.Text = "clear";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(329, 3);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.list_box_초음파1);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.simpleButton5);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl5.SplitterPosition = 34;
            this.splitContainerControl5.TabIndex = 6;
            this.splitContainerControl5.Text = "splitContainerControl1";
            // 
            // list_box_초음파1
            // 
            this.list_box_초음파1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_초음파1.FormattingEnabled = true;
            this.list_box_초음파1.ItemHeight = 14;
            this.list_box_초음파1.Location = new System.Drawing.Point(0, 0);
            this.list_box_초음파1.Name = "list_box_초음파1";
            this.list_box_초음파1.Size = new System.Drawing.Size(157, 255);
            this.list_box_초음파1.TabIndex = 4;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton5.Location = new System.Drawing.Point(0, 0);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(157, 34);
            this.simpleButton5.TabIndex = 0;
            this.simpleButton5.Text = "clear";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(166, 3);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.list_box_클립조립);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.simpleButton3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl3.SplitterPosition = 34;
            this.splitContainerControl3.TabIndex = 6;
            this.splitContainerControl3.Text = "splitContainerControl1";
            // 
            // list_box_클립조립
            // 
            this.list_box_클립조립.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_클립조립.FormattingEnabled = true;
            this.list_box_클립조립.ItemHeight = 14;
            this.list_box_클립조립.Location = new System.Drawing.Point(0, 0);
            this.list_box_클립조립.Name = "list_box_클립조립";
            this.list_box_클립조립.Size = new System.Drawing.Size(157, 255);
            this.list_box_클립조립.TabIndex = 4;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton3.Location = new System.Drawing.Point(0, 0);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(157, 34);
            this.simpleButton3.TabIndex = 0;
            this.simpleButton3.Text = "clear";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // TEST_LASER_in_B
            // 
            this.TEST_LASER_in_B.Location = new System.Drawing.Point(1027, 307);
            this.TEST_LASER_in_B.Name = "TEST_LASER_in_B";
            this.TEST_LASER_in_B.Size = new System.Drawing.Size(102, 32);
            this.TEST_LASER_in_B.TabIndex = 39;
            this.TEST_LASER_in_B.Text = "레이저컷팅B면";
            this.TEST_LASER_in_B.UseVisualStyleBackColor = true;
            this.TEST_LASER_in_B.Visible = false;
            this.TEST_LASER_in_B.Click += new System.EventHandler(this.TEST_LASER_in_A_Click);
            // 
            // TEST_LASER_in_A
            // 
            this.TEST_LASER_in_A.Location = new System.Drawing.Point(919, 307);
            this.TEST_LASER_in_A.Name = "TEST_LASER_in_A";
            this.TEST_LASER_in_A.Size = new System.Drawing.Size(102, 32);
            this.TEST_LASER_in_A.TabIndex = 39;
            this.TEST_LASER_in_A.Text = "레이저컷팅A면";
            this.TEST_LASER_in_A.UseVisualStyleBackColor = true;
            this.TEST_LASER_in_A.Visible = false;
            this.TEST_LASER_in_A.Click += new System.EventHandler(this.TEST_LASER_in_A_Click);
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(818, 3);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.list_box_검사기);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton6);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl6.SplitterPosition = 34;
            this.splitContainerControl6.TabIndex = 6;
            this.splitContainerControl6.Text = "splitContainerControl1";
            this.splitContainerControl6.Visible = false;
            // 
            // list_box_검사기
            // 
            this.list_box_검사기.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_검사기.FormattingEnabled = true;
            this.list_box_검사기.ItemHeight = 14;
            this.list_box_검사기.Location = new System.Drawing.Point(0, 0);
            this.list_box_검사기.Name = "list_box_검사기";
            this.list_box_검사기.Size = new System.Drawing.Size(157, 255);
            this.list_box_검사기.TabIndex = 4;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton6.Location = new System.Drawing.Point(0, 0);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(157, 34);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "clear";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // TEST_LASER_out_B
            // 
            this.TEST_LASER_out_B.Location = new System.Drawing.Point(1027, 346);
            this.TEST_LASER_out_B.Name = "TEST_LASER_out_B";
            this.TEST_LASER_out_B.Size = new System.Drawing.Size(102, 32);
            this.TEST_LASER_out_B.TabIndex = 39;
            this.TEST_LASER_out_B.Text = "레이저컷팅B면";
            this.TEST_LASER_out_B.UseVisualStyleBackColor = true;
            this.TEST_LASER_out_B.Visible = false;
            this.TEST_LASER_out_B.Click += new System.EventHandler(this.TEST_LASER_in_A_Click);
            // 
            // TEST_LASER_out_A
            // 
            this.TEST_LASER_out_A.Location = new System.Drawing.Point(919, 346);
            this.TEST_LASER_out_A.Name = "TEST_LASER_out_A";
            this.TEST_LASER_out_A.Size = new System.Drawing.Size(102, 32);
            this.TEST_LASER_out_A.TabIndex = 39;
            this.TEST_LASER_out_A.Text = "레이저컷팅A면";
            this.TEST_LASER_out_A.UseVisualStyleBackColor = true;
            this.TEST_LASER_out_A.Visible = false;
            this.TEST_LASER_out_A.Click += new System.EventHandler(this.TEST_LASER_in_A_Click);
            // 
            // winsock_OP
            // 
            this.winsock_OP.LocalPort = 80;
            this.winsock_OP.RemoteIP = "127.0.0.1";
            this.winsock_OP.RemotePort = 80;
            this.winsock_OP.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_OP);
            // 
            // winsock_레이져컷팅
            // 
            this.winsock_레이져컷팅.LocalPort = 80;
            this.winsock_레이져컷팅.RemoteIP = "127.0.0.1";
            this.winsock_레이져컷팅.RemotePort = 80;
            this.winsock_레이져컷팅.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_레이져컷팅);
            // 
            // Refresh_timer_레이져컷팅
            // 
            this.Refresh_timer_레이져컷팅.Interval = 200;
            this.Refresh_timer_레이져컷팅.Tick += new System.EventHandler(this.Refresh_timer_레이져컷팅_Tick);
            // 
            // winsock_클립조립
            // 
            this.winsock_클립조립.LocalPort = 80;
            this.winsock_클립조립.RemoteIP = "127.0.0.1";
            this.winsock_클립조립.RemotePort = 80;
            this.winsock_클립조립.DataArrival += new MelsecPLC.Winsock.DataArrivalEventHandler(this.winsock1_DataArrival_클립조립);
            // 
            // Refresh_timer_클립조립
            // 
            this.Refresh_timer_클립조립.Interval = 200;
            this.Refresh_timer_클립조립.Tick += new System.EventHandler(this.Refresh_timer_클립조립_Tick);
            // 
            // Refresh_timer_OP
            // 
            this.Refresh_timer_OP.Interval = 2500;
            this.Refresh_timer_OP.Tick += new System.EventHandler(this.Refresh_timer_OP_Tick);
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(97, 2);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AutoHeight = false;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(210, 78);
            this.dateEdit1.TabIndex = 38;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(720, 3);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit1.Size = new System.Drawing.Size(348, 46);
            this.textEdit1.TabIndex = 37;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            this.textEdit1.Leave += new System.EventHandler(this.textEdit1_Leave);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Font = new System.Drawing.Font("굴림", 22F);
            this.btnClose.Location = new System.Drawing.Point(1219, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(198, 81);
            this.btnClose.TabIndex = 36;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(0, 0);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(93, 83);
            this.label47.TabIndex = 33;
            this.label47.Text = "생산일";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label47.Click += new System.EventHandler(this.label47_Click);
            // 
            // btn_po_release
            // 
            this.btn_po_release.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_po_release.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.btn_po_release.Location = new System.Drawing.Point(633, 0);
            this.btn_po_release.Margin = new System.Windows.Forms.Padding(1);
            this.btn_po_release.Name = "btn_po_release";
            this.btn_po_release.Size = new System.Drawing.Size(82, 83);
            this.btn_po_release.TabIndex = 34;
            this.btn_po_release.Text = "작업\r\n검색";
            this.btn_po_release.UseVisualStyleBackColor = false;
            this.btn_po_release.Click += new System.EventHandler(this.btn_po_release_Click);
            // 
            // lueWc_code
            // 
            this.lueWc_code.Location = new System.Drawing.Point(313, 3);
            this.lueWc_code.Name = "lueWc_code";
            this.lueWc_code.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 25F);
            this.lueWc_code.Properties.Appearance.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lueWc_code.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 40F);
            this.lueWc_code.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lueWc_code.Properties.AutoHeight = false;
            this.lueWc_code.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            this.lueWc_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueWc_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_CODE", "작업장코드"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("WC_NAME", "작업장명")});
            this.lueWc_code.Properties.DropDownRows = 5;
            this.lueWc_code.Properties.NullText = "";
            this.lueWc_code.Size = new System.Drawing.Size(316, 77);
            this.lueWc_code.TabIndex = 35;
            this.lueWc_code.EditValueChanged += new System.EventHandler(this.lueWc_code_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(987, 644);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ActiveFilterEnabled = false;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 25F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ORDER_NUM,
            this.IT_SCODE,
            this.IT_SNAME,
            this.PLAN_SQTY,
            this.INTO_SQTY,
            this.NOZZLE_CNT,
            this.GOOD_SQTY,
            this.FAIL_SQTY,
            this.SPEC,
            this.MO_SNUMB});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowColumnResizing = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowRowSizing = true;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView1.OptionsFind.AllowFindPanel = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView1.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.gridView1.OptionsNavigation.AutoMoveRowFocus = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.Hidden;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 80;
            // 
            // ORDER_NUM
            // 
            this.ORDER_NUM.AppearanceCell.Options.UseTextOptions = true;
            this.ORDER_NUM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ORDER_NUM.Caption = "작업순서";
            this.ORDER_NUM.FieldName = "ORDER_NUM";
            this.ORDER_NUM.Name = "ORDER_NUM";
            this.ORDER_NUM.Visible = true;
            this.ORDER_NUM.VisibleIndex = 0;
            this.ORDER_NUM.Width = 125;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품목코드";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 1;
            this.IT_SCODE.Width = 274;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품목명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 2;
            this.IT_SNAME.Width = 301;
            // 
            // PLAN_SQTY
            // 
            this.PLAN_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.PLAN_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PLAN_SQTY.Caption = "계획수량";
            this.PLAN_SQTY.FieldName = "PLAN_SQTY";
            this.PLAN_SQTY.Name = "PLAN_SQTY";
            this.PLAN_SQTY.Visible = true;
            this.PLAN_SQTY.VisibleIndex = 3;
            this.PLAN_SQTY.Width = 159;
            // 
            // INTO_SQTY
            // 
            this.INTO_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.INTO_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.INTO_SQTY.Caption = "투입수량";
            this.INTO_SQTY.FieldName = "INTO_SQTY";
            this.INTO_SQTY.Name = "INTO_SQTY";
            this.INTO_SQTY.Visible = true;
            this.INTO_SQTY.VisibleIndex = 4;
            this.INTO_SQTY.Width = 152;
            // 
            // NOZZLE_CNT
            // 
            this.NOZZLE_CNT.Caption = "노즐투입수량";
            this.NOZZLE_CNT.FieldName = "NOZZLE_CNT";
            this.NOZZLE_CNT.Name = "NOZZLE_CNT";
            this.NOZZLE_CNT.Width = 147;
            // 
            // GOOD_SQTY
            // 
            this.GOOD_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.GOOD_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GOOD_SQTY.Caption = "양품수량";
            this.GOOD_SQTY.FieldName = "GOOD_SQTY";
            this.GOOD_SQTY.Name = "GOOD_SQTY";
            this.GOOD_SQTY.Width = 153;
            // 
            // FAIL_SQTY
            // 
            this.FAIL_SQTY.AppearanceCell.Options.UseTextOptions = true;
            this.FAIL_SQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FAIL_SQTY.Caption = "불량수량";
            this.FAIL_SQTY.FieldName = "FAIL_SQTY";
            this.FAIL_SQTY.Name = "FAIL_SQTY";
            this.FAIL_SQTY.Width = 151;
            // 
            // SPEC
            // 
            this.SPEC.Caption = "사양(상수값)";
            this.SPEC.FieldName = "SPEC";
            this.SPEC.Name = "SPEC";
            this.SPEC.Width = 85;
            // 
            // MO_SNUMB
            // 
            this.MO_SNUMB.Caption = "작업지시";
            this.MO_SNUMB.FieldName = "MO_SNUMB";
            this.MO_SNUMB.Name = "MO_SNUMB";
            // 
            // lbl_now_date
            // 
            this.lbl_now_date.Appearance.Font = new System.Drawing.Font("Tahoma", 15F);
            this.lbl_now_date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_now_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_now_date.Location = new System.Drawing.Point(720, 0);
            this.lbl_now_date.Name = "lbl_now_date";
            this.lbl_now_date.Size = new System.Drawing.Size(347, 28);
            this.lbl_now_date.TabIndex = 40;
            this.lbl_now_date.Text = "labelControl1";
            // 
            // splitContainerControl8
            // 
            this.splitContainerControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl8.Horizontal = false;
            this.splitContainerControl8.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl8.Name = "splitContainerControl8";
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_re_reading);
            this.splitContainerControl8.Panel1.Controls.Add(this.label47);
            this.splitContainerControl8.Panel1.Controls.Add(this.lueWc_code);
            this.splitContainerControl8.Panel1.Controls.Add(this.button1);
            this.splitContainerControl8.Panel1.Controls.Add(this.btn_po_release);
            this.splitContainerControl8.Panel1.Controls.Add(this.dateEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.btnClose);
            this.splitContainerControl8.Panel1.Controls.Add(this.textEdit2);
            this.splitContainerControl8.Panel1.Controls.Add(this.textEdit1);
            this.splitContainerControl8.Panel1.Controls.Add(this.lbl_now_date);
            this.splitContainerControl8.Panel1.Text = "Panel1";
            this.splitContainerControl8.Panel2.Controls.Add(this.splitContainerControl9);
            this.splitContainerControl8.Panel2.Text = "Panel2";
            this.splitContainerControl8.Size = new System.Drawing.Size(1378, 742);
            this.splitContainerControl8.SplitterPosition = 93;
            this.splitContainerControl8.TabIndex = 41;
            this.splitContainerControl8.Text = "splitContainerControl8";
            // 
            // btn_re_reading
            // 
            this.btn_re_reading.Appearance.BackColor = System.Drawing.Color.LightCoral;
            this.btn_re_reading.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btn_re_reading.Appearance.Options.UseBackColor = true;
            this.btn_re_reading.Appearance.Options.UseFont = true;
            this.btn_re_reading.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_re_reading.Location = new System.Drawing.Point(1073, 3);
            this.btn_re_reading.Name = "btn_re_reading";
            this.btn_re_reading.Size = new System.Drawing.Size(140, 81);
            this.btn_re_reading.TabIndex = 41;
            this.btn_re_reading.Tag = "OFF";
            this.btn_re_reading.Text = "재리딩OFF";
            this.btn_re_reading.Click += new System.EventHandler(this.btn_re_reading_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(1125, 98);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 83);
            this.button1.TabIndex = 34;
            this.button1.Text = "시\r\n작";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textEdit2
            // 
            this.textEdit2.EditValue = "";
            this.textEdit2.Location = new System.Drawing.Point(719, 52);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.AutoHeight = false;
            this.textEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.textEdit2.Size = new System.Drawing.Size(348, 46);
            this.textEdit2.TabIndex = 37;
            this.textEdit2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit2_KeyPress);
            // 
            // splitContainerControl9
            // 
            this.splitContainerControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl9.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl9.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl9.Name = "splitContainerControl9";
            this.splitContainerControl9.Panel1.Controls.Add(this.toolStrip1);
            this.splitContainerControl9.Panel1.Controls.Add(this.panel1);
            this.splitContainerControl9.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl9.Panel1.Text = "Panel1";
            this.splitContainerControl9.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl9.Panel2.Text = "Panel2";
            this.splitContainerControl9.Size = new System.Drawing.Size(1378, 644);
            this.splitContainerControl9.SplitterPosition = 386;
            this.splitContainerControl9.TabIndex = 40;
            this.splitContainerControl9.Text = "splitContainerControl9";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.toolStripLabel1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 619);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(987, 25);
            this.toolStrip1.TabIndex = 40;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.BackColor = System.Drawing.Color.LawnGreen;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(70, 22);
            this.toolStripLabel2.Text = "연결 상태 : ";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(0, 22);
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(386, 644);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.gridView2.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView2.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 15F);
            this.gridView2.Appearance.Row.Options.UseFont = true;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.READING_DATA,
            this.S_DATE,
            this.RESULT});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            gridFormatRule1.Column = this.RESULT;
            gridFormatRule1.ColumnApplyTo = this.RESULT;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Salmon;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Expression = "[RESULT] = \'NG\'";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.Column = this.RESULT;
            gridFormatRule2.ColumnApplyTo = this.RESULT;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Expression = "[RESULT] = \'OK\'";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsBehavior.ReadOnly = true;
            this.gridView2.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // READING_DATA
            // 
            this.READING_DATA.Caption = "리딩 데이터";
            this.READING_DATA.FieldName = "READING_DATA";
            this.READING_DATA.Name = "READING_DATA";
            this.READING_DATA.Visible = true;
            this.READING_DATA.VisibleIndex = 0;
            this.READING_DATA.Width = 302;
            // 
            // S_DATE
            // 
            this.S_DATE.Caption = "DATE";
            this.S_DATE.FieldName = "S_DATE";
            this.S_DATE.Name = "S_DATE";
            this.S_DATE.Width = 429;
            // 
            // message_timer_clear
            // 
            this.message_timer_clear.Tick += new System.EventHandler(this.message_timer_clear_Tick);
            // 
            // read_send_time_2
            // 
            this.read_send_time_2.Interval = 500;
            this.read_send_time_2.Tick += new System.EventHandler(this.read_send_time_2_Tick);
            // 
            // read_send_timer_op
            // 
            this.read_send_timer_op.Interval = 2500;
            this.read_send_timer_op.Tick += new System.EventHandler(this.read_send_timer_op_Tick);
            // 
            // timer_지멘스
            // 
            this.timer_지멘스.Interval = 5000;
            this.timer_지멘스.Tick += new System.EventHandler(this.timer_지멘스_Tick);
            // 
            // AUTO_YB_IN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1378, 742);
            this.Controls.Add(this.splitContainerControl8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_YB_IN";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl10)).EndInit();
            this.splitContainerControl10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueWc_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).EndInit();
            this.splitContainerControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl9)).EndInit();
            this.splitContainerControl9.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer_시계;
        private MelsecPLC.Winsock winsock_진동;
        private System.Windows.Forms.Timer read_send_timer;
        private System.Windows.Forms.Timer Refresh_timer_진동;
        private System.Windows.Forms.ListBox list_box_진동;
        private System.Windows.Forms.ListBox list_box_초음파2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.Panel panel1;
        private MelsecPLC.Winsock winsock_OP;
        private MelsecPLC.Winsock winsock_레이져컷팅;
        private System.Windows.Forms.Timer Refresh_timer_레이져컷팅;
        private MelsecPLC.Winsock winsock_클립조립;
        private System.Windows.Forms.Timer Refresh_timer_클립조립;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private System.Windows.Forms.ListBox list_box_레이져컷팅;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private System.Windows.Forms.ListBox list_box_초음파1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private System.Windows.Forms.ListBox list_box_클립조립;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private System.Windows.Forms.ListBox list_box_검사기;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private System.Windows.Forms.ListBox list_box_OP;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.Timer Refresh_timer_OP;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btn_po_release;
        private DevExpress.XtraEditors.LookUpEdit lueWc_code;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.LabelControl lbl_now_date;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn PLAN_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn INTO_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn GOOD_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn FAIL_SQTY;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl8;
        private System.Windows.Forms.Timer message_timer_clear;
        private DevExpress.XtraGrid.Columns.GridColumn ORDER_NUM;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl9;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn READING_DATA;
        private DevExpress.XtraGrid.Columns.GridColumn S_DATE;
        private DevExpress.XtraGrid.Columns.GridColumn RESULT;
        private DevExpress.XtraGrid.Columns.GridColumn SPEC;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn MO_SNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn NOZZLE_CNT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button TEST_검사_out;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button TEST_초음파2_out;
        private System.Windows.Forms.Button TEST_검사_in;
        private System.Windows.Forms.Button TEST_진동2_out;
        private System.Windows.Forms.Button TEST_LASER_in_A;
        private System.Windows.Forms.Button TEXT_진동1_out;
        private System.Windows.Forms.Button TEST_CLIP_in;
        private System.Windows.Forms.Button TEXT_초음파1_out;
        private System.Windows.Forms.Button TEST_초음파2_in;
        private System.Windows.Forms.Button TEST_CLIP_out;
        private System.Windows.Forms.Button TEXT_초음파1_in;
        private System.Windows.Forms.Button TEST_LASER_out_A;
        private System.Windows.Forms.Button TEXT_진동1_in;
        private System.Windows.Forms.Button TEST_진동2_in;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button TEST_LASER_in_B;
        private System.Windows.Forms.Button TEST_LASER_out_B;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private System.Windows.Forms.Timer read_send_time_2;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private System.Windows.Forms.Timer read_send_timer_op;
        private DevExpress.XtraEditors.SimpleButton btn_re_reading;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl10;
        private DevExpress.XtraEditors.LabelControl lbl_f20;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl lbl_f19;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl lbl_f18;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl lbl_f17;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl lbl_f16;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl lbl_f15;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lbl_f14;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl lbl_f13;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl lbl_f12;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl lbl_f11;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lbl_f10;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl lbl_f9;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lbl_f8;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl lbl_f7;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl lbl_f6;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl lbl_f5;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lbl_f4;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl lbl_f3;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lbl_f2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl lbl_f1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbl_w4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lbl_w3;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lbl_w2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl lbl_w1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.Timer timer_지멘스;
    }
}

