﻿namespace DK_Tablet.AUTO_JA_YB
{
    partial class AUTO_FAIL_POPUP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.ASSY_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ASSY_BARCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PRDT_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.LASER_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHILD_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHILD_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TOTAL_INS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FINAL_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.R_DATE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHANGE_LOT_NO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.btn_cancle = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ng = new DevExpress.XtraEditors.SimpleButton();
            this.btn_ok = new DevExpress.XtraEditors.SimpleButton();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_op_code = new DevExpress.XtraEditors.LabelControl();
            this.lbl_ok_ng = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.splitContainerControl1.Appearance.Options.UseBackColor = true;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(7, 7);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.btn_cancle);
            this.splitContainerControl1.Panel2.Controls.Add(this.btn_ng);
            this.splitContainerControl1.Panel2.Controls.Add(this.btn_ok);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1312, 231);
            this.splitContainerControl1.SplitterPosition = 80;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(872, 146);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.advBandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.advBandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 14.25F);
            this.advBandedGridView1.Appearance.Row.Options.UseFont = true;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand6,
            this.gridBand3});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.R_DATE,
            this.ASSY_ITEM,
            this.ASSY_BARCODE,
            this.CHILD_ITEM,
            this.CHILD_LOT,
            this.CHANGE_LOT_NO,
            this.PRDT_ITEM,
            this.LASER_LOT,
            this.TOTAL_INS,
            this.FINAL_GUBN});
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.ShowBands = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            // 
            // ASSY_ITEM
            // 
            this.ASSY_ITEM.Caption = "완성품 품번";
            this.ASSY_ITEM.FieldName = "ASSY_ITEM";
            this.ASSY_ITEM.Name = "ASSY_ITEM";
            this.ASSY_ITEM.OptionsColumn.AllowEdit = false;
            this.ASSY_ITEM.OptionsColumn.ReadOnly = true;
            this.ASSY_ITEM.Visible = true;
            this.ASSY_ITEM.Width = 297;
            // 
            // ASSY_BARCODE
            // 
            this.ASSY_BARCODE.Caption = "완제품로트";
            this.ASSY_BARCODE.FieldName = "ASSY_BARCODE";
            this.ASSY_BARCODE.Name = "ASSY_BARCODE";
            this.ASSY_BARCODE.RowIndex = 1;
            this.ASSY_BARCODE.Visible = true;
            this.ASSY_BARCODE.Width = 297;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.Caption = "레이저 품번";
            this.PRDT_ITEM.FieldName = "PRDT_ITEM";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.Visible = true;
            this.PRDT_ITEM.Width = 285;
            // 
            // LASER_LOT
            // 
            this.LASER_LOT.Caption = "레이저로트";
            this.LASER_LOT.FieldName = "LASER_LOT";
            this.LASER_LOT.Name = "LASER_LOT";
            this.LASER_LOT.RowIndex = 1;
            this.LASER_LOT.Visible = true;
            this.LASER_LOT.Width = 285;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.Caption = "사출품";
            this.CHILD_ITEM.FieldName = "CHILD_ITEM";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.OptionsColumn.ReadOnly = true;
            this.CHILD_ITEM.Visible = true;
            this.CHILD_ITEM.Width = 287;
            // 
            // CHILD_LOT
            // 
            this.CHILD_LOT.Caption = "사출LOT";
            this.CHILD_LOT.FieldName = "CHILD_LOT";
            this.CHILD_LOT.Name = "CHILD_LOT";
            this.CHILD_LOT.OptionsColumn.ReadOnly = true;
            this.CHILD_LOT.RowIndex = 1;
            this.CHILD_LOT.Visible = true;
            this.CHILD_LOT.Width = 287;
            // 
            // TOTAL_INS
            // 
            this.TOTAL_INS.AppearanceCell.Options.UseTextOptions = true;
            this.TOTAL_INS.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TOTAL_INS.Caption = "최종";
            this.TOTAL_INS.FieldName = "TOTAL_INS";
            this.TOTAL_INS.Name = "TOTAL_INS";
            this.TOTAL_INS.Width = 139;
            // 
            // FINAL_GUBN
            // 
            this.FINAL_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.FINAL_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FINAL_GUBN.Caption = "처리구분";
            this.FINAL_GUBN.FieldName = "FINAL_GUBN";
            this.FINAL_GUBN.Name = "FINAL_GUBN";
            this.FINAL_GUBN.Width = 139;
            // 
            // R_DATE
            // 
            this.R_DATE.Caption = "불량발생일";
            this.R_DATE.FieldName = "R_DATE";
            this.R_DATE.Name = "R_DATE";
            this.R_DATE.OptionsColumn.AllowEdit = false;
            this.R_DATE.OptionsColumn.ReadOnly = true;
            // 
            // CHANGE_LOT_NO
            // 
            this.CHANGE_LOT_NO.Caption = "사출변환LOT";
            this.CHANGE_LOT_NO.FieldName = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Name = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Visible = true;
            // 
            // repositoryItemButtonEdit1
            // 
            serializableAppearanceObject2.BackColor = System.Drawing.Color.LightBlue;
            serializableAppearanceObject2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            serializableAppearanceObject2.Options.UseBackColor = true;
            serializableAppearanceObject2.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "불량처리", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ReadOnly = true;
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // btn_cancle
            // 
            this.btn_cancle.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.btn_cancle.Appearance.Options.UseFont = true;
            this.btn_cancle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_cancle.Location = new System.Drawing.Point(763, 1);
            this.btn_cancle.Name = "btn_cancle";
            this.btn_cancle.Size = new System.Drawing.Size(206, 66);
            this.btn_cancle.TabIndex = 0;
            this.btn_cancle.Text = "취소";
            this.btn_cancle.Click += new System.EventHandler(this.btn_cancle_Click);
            // 
            // btn_ng
            // 
            this.btn_ng.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.btn_ng.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.btn_ng.Appearance.Options.UseBackColor = true;
            this.btn_ng.Appearance.Options.UseFont = true;
            this.btn_ng.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_ng.Location = new System.Drawing.Point(551, 1);
            this.btn_ng.Name = "btn_ng";
            this.btn_ng.Size = new System.Drawing.Size(206, 66);
            this.btn_ng.TabIndex = 0;
            this.btn_ng.Text = "폐기";
            this.btn_ng.Click += new System.EventHandler(this.btn_ng_Click);
            // 
            // btn_ok
            // 
            this.btn_ok.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            this.btn_ok.Appearance.Font = new System.Drawing.Font("Tahoma", 40F);
            this.btn_ok.Appearance.Options.UseBackColor = true;
            this.btn_ok.Appearance.Options.UseFont = true;
            this.btn_ok.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_ok.Location = new System.Drawing.Point(339, 1);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(206, 66);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "재작업";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Columns.Add(this.ASSY_ITEM);
            this.gridBand2.Columns.Add(this.ASSY_BARCODE);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 297;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Columns.Add(this.PRDT_ITEM);
            this.gridBand6.Columns.Add(this.LASER_LOT);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 285;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Columns.Add(this.CHILD_ITEM);
            this.gridBand3.Columns.Add(this.CHILD_LOT);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 287;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1312, 146);
            this.splitContainerControl2.SplitterPosition = 872;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lbl_op_code, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_ok_ng, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(435, 146);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lbl_op_code
            // 
            this.lbl_op_code.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.lbl_op_code.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lbl_op_code.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_op_code.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_op_code.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_op_code.Location = new System.Drawing.Point(3, 3);
            this.lbl_op_code.Name = "lbl_op_code";
            this.lbl_op_code.Size = new System.Drawing.Size(429, 67);
            this.lbl_op_code.TabIndex = 0;
            // 
            // lbl_ok_ng
            // 
            this.lbl_ok_ng.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.lbl_ok_ng.Appearance.Font = new System.Drawing.Font("Tahoma", 35F);
            this.lbl_ok_ng.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_ok_ng.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_ok_ng.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_ok_ng.Location = new System.Drawing.Point(3, 76);
            this.lbl_ok_ng.Name = "lbl_ok_ng";
            this.lbl_ok_ng.Size = new System.Drawing.Size(429, 67);
            this.lbl_ok_ng.TabIndex = 0;
            // 
            // AUTO_FAIL_POPUP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(1326, 245);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_FAIL_POPUP";
            this.Padding = new System.Windows.Forms.Padding(7);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AUTO_FAIL_POPUP";
            this.Load += new System.EventHandler(this.AUTO_FAIL_POPUP_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btn_cancle;
        private DevExpress.XtraEditors.SimpleButton btn_ng;
        private DevExpress.XtraEditors.SimpleButton btn_ok;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_BARCODE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LASER_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PRDT_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TOTAL_INS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FINAL_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn R_DATE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHANGE_LOT_NO;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl lbl_op_code;
        private DevExpress.XtraEditors.LabelControl lbl_ok_ng;
    }
}