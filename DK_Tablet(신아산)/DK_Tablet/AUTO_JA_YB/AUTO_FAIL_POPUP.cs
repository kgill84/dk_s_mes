﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    public partial class AUTO_FAIL_POPUP : Form
    {
        public string txt_title="";
        public DataTable dt = new DataTable();
        public AUTO_FAIL_POPUP()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            
            DialogResult = DialogResult.Yes;
        }

        private void btn_ng_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void btn_cancle_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void AUTO_FAIL_POPUP_Load(object sender, EventArgs e)
        {
            gridControl1.DataSource= dt;

            foreach(DataRow dr in dt.Rows)
            {
                lbl_ok_ng.Text = dr["TOTAL_INS"].ToString();
                lbl_op_code.Text = dr["FINAL_GUBN"].ToString();
            }
        }
    }
}
