﻿using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    public partial class AUTO_FAIL_REG : Form
    {
        MAIN parent_form;
        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();
        DataTable FAIL_DT = new DataTable();
        public AUTO_FAIL_REG(MAIN form)
        {
            parent_form = form;
            InitializeComponent();
        }
        private void AUTO_FAIL_REG_Load(object sender, EventArgs e)
        {
            textEdit1.Focus();
            FAIL_DT.Columns.Add("R_DATE",typeof(string));
            FAIL_DT.Columns.Add("IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_LOT_NO", typeof(string));
            FAIL_DT.Columns.Add("CHANGE_LOT_NO", typeof(string));
            FAIL_DT.Columns.Add("NG_DESCR", typeof(string));
            FAIL_DT.Columns.Add("NG_OP", typeof(string));
            Sdate.DateTime = DateTime.Now;
            gridControl1.DataSource = FAIL_DT;
        }
        

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                string barcode = textEdit1.Text.Trim();
                string[] arrBarcode = barcode.Split('*');
                if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                {
                    string read_it_scode = arrBarcode[0].ToString();
                    string read_lot = arrBarcode[1].ToString();
                    string[] arr_lot = read_lot.Split('/');
                    if (arr_lot.Length.Equals(4))
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("R_DATE", typeof(string));
                        dt.Columns.Add("IT_SCODE", typeof(string));
                        dt.Columns.Add("CHILD_IT_SCODE", typeof(string));
                        dt.Columns.Add("CHILD_LOT_NO", typeof(string));
                        dt.Columns.Add("CHANGE_LOT_NO", typeof(string));
                        dt.Columns.Add("NG_DESCR", typeof(string));
                        dt.Columns.Add("NG_OP", typeof(string));
                        
                        dt= get_fail_data(read_it_scode, read_lot);
                        AUTO_FAIL_POPUP AUTO_FAIL_POPUP = new AUTO_FAIL_POPUP();
                        AUTO_FAIL_POPUP.dt = dt;
                        DialogResult dr = AUTO_FAIL_POPUP.ShowDialog();
                        if (dr == System.Windows.Forms.DialogResult.Yes)
                        {
                            //불량 처리 하는 SP
                            OK_NG_save(read_it_scode, read_lot, "OK");
                            //DataRow[] drr = FAIL_DT.Select("CHANGE_LOT_NO='" + change_lot_no + "'");
                            //FAIL_DT.Rows.Remove(drr[0]);
                        }
                        else if (dr == System.Windows.Forms.DialogResult.No)
                        {
                            OK_NG_save(read_it_scode, read_lot, "NG");
                        }
                        //if (dt.Rows.Count > 0)
                        //{

                        //    if (string.IsNullOrWhiteSpace(dt.Rows[0]["CHANGE_LOT_NO"].ToString()))
                        //    {

                        //    }
                        //    else
                        //    {
                        //        DataRow[] drr = FAIL_DT.Select("CHANGE_LOT_NO='" + dt.Rows[0]["CHANGE_LOT_NO"].ToString() + "'");
                        //        if (drr.Length < 1)
                        //        {
                        //            string ng_descr = "";
                        //            if (string.IsNullOrWhiteSpace(dt.Rows[0]["NG_DESCR"].ToString()))
                        //            {
                        //                ng_descr = "강제 NG";
                        //            }
                        //            else
                        //            {
                        //                ng_descr = dt.Rows[0]["NG_DESCR"].ToString();
                        //            }
                        //            DataRow dr = FAIL_DT.NewRow();
                        //            dr["R_DATE"] = dt.Rows[0]["R_DATE"].ToString();
                        //            dr["IT_SCODE"] = dt.Rows[0]["IT_SCODE"].ToString();
                        //            dr["CHILD_IT_SCODE"] = dt.Rows[0]["CHILD_IT_SCODE"].ToString();
                        //            dr["CHILD_LOT_NO"] = dt.Rows[0]["CHILD_LOT_NO"].ToString();
                        //            dr["CHANGE_LOT_NO"] = dt.Rows[0]["CHANGE_LOT_NO"].ToString();
                        //            dr["NG_DESCR"] = ng_descr;
                        //            dr["NG_OP"] = dt.Rows[0]["NG_OP"].ToString();
                        //            FAIL_DT.Rows.Add(dr);
                        //        }
                        //    }
                        //}

                    }
                }
                else
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("잘못된 QR 코드 입니다.", 5);
                    PpCard_Success.BackColor = Color.Salmon;
                }
                textEdit1.Focus();
                textEdit1.Text = "";
            }
            
        }

        

        //불량 처리 데이터 가져오기
        public DataTable get_fail_data(string child_it_scode, string child_lot_no)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            //string sql = "USP_AUTO_GET_FAIL_DATA";
            string sql = "USP_JA_LOT_SEARCH_NG_BELT_READING_NEW";

            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;

            da.SelectCommand.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            da.SelectCommand.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);

            DataTable dt = null;
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "NG_BELT_READING");
                dt = ds.Tables["NG_BELT_READING"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        
        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //string change_lot_no = "", ng_op = ""; ;
            //change_lot_no = gridView1.GetFocusedRowCellValue("CHANGE_LOT_NO").ToString();
            //ng_op = gridView1.GetFocusedRowCellValue("NG_OP").ToString();
            //AUTO_FAIL_POPUP AUTO_FAIL_POPUP = new AUTO_FAIL_POPUP();
            //AUTO_FAIL_POPUP.txt_title = "불량처리";
            //DialogResult dialog = AUTO_FAIL_POPUP.ShowDialog();
            //if (dialog == System.Windows.Forms.DialogResult.Yes)
            //{
            //    //불량 처리 하는 SP
            //    OK_NG_save(change_lot_no, "OK", ng_op);
            //    DataRow[] drr = FAIL_DT.Select("CHANGE_LOT_NO='" + change_lot_no + "'");
            //    FAIL_DT.Rows.Remove(drr[0]);
                
            //}
            //else if (dialog == System.Windows.Forms.DialogResult.No)
            //{
            //    OK_NG_save(change_lot_no, "NG", ng_op);
                
            //}
            //else
            //{
                
            //}
            //textEdit1.Focus();
        }

        public string OK_NG_save(string child_item,string child_lot,string ok_ng)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string send_lot = "";
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_OK_NG_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@CHILD_ITEM", child_item);
            cmd.Parameters.AddWithValue("@CHILD_LOT", child_lot);
            cmd.Parameters.AddWithValue("@OK_NG", ok_ng);


            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return send_lot;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            FAIL_DT.Rows.Clear();
            textEdit1.Focus();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //완료
            getData_Y();
            textEdit1.Focus();
        }

        private void getData_N()
        {
            splashScreenManager1.ShowWaitForm();
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);

            SqlDataAdapter da = new SqlDataAdapter("USP_JA_LOT_SEARCH_NG_BELT", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@SDATE", Sdate.DateTime.ToString("yyyyMMdd"));


            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "SHIPMENT");

                gridControl1.DataSource = ds.Tables["SHIPMENT"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
                splashScreenManager1.CloseWaitForm();
            }
        }
        private void getData_Y()
        {
            splashScreenManager1.ShowWaitForm();
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);

            SqlDataAdapter da = new SqlDataAdapter("USP_JA_LOT_SEARCH_NG_BELT_Y", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@SDATE", Sdate.DateTime.ToString("yyyyMMdd"));


            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "SHIPMENT");

                gridControl1.DataSource = ds.Tables["SHIPMENT"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
                splashScreenManager1.CloseWaitForm();
            }
        }
        private void simpleButton2_Click(object sender, EventArgs e)
        {
            getData_N();
            textEdit1.Focus();
        }

        private void advBandedGridView1_MouseWheel(object sender, MouseEventArgs e)
        {
            advBandedGridView1.CloseEditor();
        }

    }
}
