﻿using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    public partial class AUTO_OUT_INS_FAIL_SEARCH : Form
    {

        DataTable FAIL_DT = new DataTable();
        public string car_code { get; set; }
        public AUTO_OUT_INS_FAIL_SEARCH()
        {
            InitializeComponent();
        }
        private void AUTO_FAIL_REG_Load(object sender, EventArgs e)
        {
            FAIL_DT.Columns.Add("R_DATE",typeof(string));
            FAIL_DT.Columns.Add("IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_LOT_NO", typeof(string));
            FAIL_DT.Columns.Add("CHANGE_LOT_NO", typeof(string));
            FAIL_DT.Columns.Add("NG_DESCR", typeof(string));
            FAIL_DT.Columns.Add("NG_OP", typeof(string));
            Sdate.DateTime = DateTime.Now;
            gridControl1.DataSource = FAIL_DT;
        }
        

        

        //불량 처리 데이터 가져오기
        public DataTable get_fail_data(string child_it_scode, string child_lot_no)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            //string sql = "USP_AUTO_GET_FAIL_DATA";
            string sql = "USP_JA_LOT_SEARCH_NG_BELT_READING";

            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;

            da.SelectCommand.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            da.SelectCommand.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);

            DataTable dt = null;
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "NG_BELT_READING");
                dt = ds.Tables["NG_BELT_READING"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }


        public string OK_NG_save(string child_item,string child_lot,string ok_ng)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string send_lot = "";
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_OK_NG_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@CHILD_ITEM", child_item);
            cmd.Parameters.AddWithValue("@CHILD_LOT", child_lot);
            cmd.Parameters.AddWithValue("@OK_NG", ok_ng);


            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return send_lot;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void getData_N()
        {
            splashScreenManager1.ShowWaitForm();
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlConnection conn = new SqlConnection(strCon);
            string sql = "";

            if (car_code.Trim().Equals("JA"))
            {
                sql = "USP_JA_LOT_SEARCH_OUT_INS";
            }
            else
            {
                sql = "USP_YB_LOT_SEARCH_OUT_INS";
            }


            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@SDATE", Sdate.DateTime.ToString("yyyyMMdd"));


            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "SHIPMENT");

                gridControl1.DataSource = ds.Tables["SHIPMENT"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
                splashScreenManager1.CloseWaitForm();
            }
        }
        

        private void advBandedGridView1_MouseWheel(object sender, MouseEventArgs e)
        {
            advBandedGridView1.CloseEditor();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            getData_N();
        }

    }
}
