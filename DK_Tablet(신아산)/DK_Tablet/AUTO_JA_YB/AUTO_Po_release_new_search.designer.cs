﻿namespace DK_Tablet
{
    partial class AUTO_Po_release_new_search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            this.MO_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.SITE_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CARRIER_YN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MAX_SQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NUM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.MO_SNUMB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_MODEL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SDATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SAFTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ALC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ME_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_PKQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.D_WEIGHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MATCH_GUBN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.A_WEIGHT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.REMAIN_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WC_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl_sdate = new DevExpress.XtraEditors.LabelControl();
            this.btn_next_date = new DevExpress.XtraEditors.SimpleButton();
            this.btn_prev_date = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.btn_day_night = new DevExpress.XtraEditors.SimpleButton();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // MO_SQUTY
            // 
            this.MO_SQUTY.AppearanceCell.Options.UseTextOptions = true;
            this.MO_SQUTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MO_SQUTY.Caption = "계획수량";
            this.MO_SQUTY.ColumnEdit = this.repositoryItemTextEdit2;
            this.MO_SQUTY.FieldName = "MO_SQUTY";
            this.MO_SQUTY.Name = "MO_SQUTY";
            this.MO_SQUTY.Visible = true;
            this.MO_SQUTY.VisibleIndex = 3;
            this.MO_SQUTY.Width = 472;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.Click += new System.EventHandler(this.repositoryItemTextEdit2_Click);
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(842, 575);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 70);
            this.btn_done.TabIndex = 1;
            this.btn_done.Text = "확정";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1010, 575);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 70);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(13, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1149, 6);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(251, 41);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "작업등록";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(13, 76);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.gridControl1.Size = new System.Drawing.Size(1149, 492);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.SITE_CODE,
            this.CARRIER_YN,
            this.MAX_SQTY,
            this.NUM,
            this.MO_SNUMB,
            this.IT_SCODE,
            this.IT_SNAME,
            this.IT_MODEL,
            this.MO_SQUTY,
            this.SDATE,
            this.IT_SAFTY,
            this.ALC_CODE,
            this.ME_SCODE,
            this.IT_PKQTY,
            this.D_WEIGHT,
            this.MATCH_GUBN,
            this.A_WEIGHT,
            this.REMAIN_SQUTY,
            this.WC_SCODE});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.MO_SQUTY;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.SkyBlue;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Expression = "[MO_SQUTY] > 0";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsMenu.ShowAutoFilterRowItem = false;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            // 
            // SITE_CODE
            // 
            this.SITE_CODE.Caption = "공장코드";
            this.SITE_CODE.FieldName = "SITE_CODE";
            this.SITE_CODE.Name = "SITE_CODE";
            this.SITE_CODE.OptionsColumn.AllowEdit = false;
            this.SITE_CODE.OptionsColumn.ReadOnly = true;
            // 
            // CARRIER_YN
            // 
            this.CARRIER_YN.Caption = "CARRIER_YN";
            this.CARRIER_YN.FieldName = "CARRIER_YN";
            this.CARRIER_YN.Name = "CARRIER_YN";
            this.CARRIER_YN.OptionsColumn.AllowEdit = false;
            this.CARRIER_YN.OptionsColumn.ReadOnly = true;
            // 
            // MAX_SQTY
            // 
            this.MAX_SQTY.Caption = "최대적재수량";
            this.MAX_SQTY.FieldName = "MAX_SQTY";
            this.MAX_SQTY.Name = "MAX_SQTY";
            this.MAX_SQTY.OptionsColumn.AllowEdit = false;
            this.MAX_SQTY.OptionsColumn.ReadOnly = true;
            // 
            // NUM
            // 
            this.NUM.AppearanceCell.Options.UseTextOptions = true;
            this.NUM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NUM.Caption = "작업순서";
            this.NUM.ColumnEdit = this.repositoryItemTextEdit1;
            this.NUM.FieldName = "NUM";
            this.NUM.Name = "NUM";
            this.NUM.Visible = true;
            this.NUM.VisibleIndex = 0;
            this.NUM.Width = 172;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.Click += new System.EventHandler(this.repositoryItemTextEdit1_Click);
            // 
            // MO_SNUMB
            // 
            this.MO_SNUMB.Caption = "작업지시번호";
            this.MO_SNUMB.FieldName = "MO_SNUMB";
            this.MO_SNUMB.Name = "MO_SNUMB";
            this.MO_SNUMB.OptionsColumn.AllowEdit = false;
            this.MO_SNUMB.OptionsColumn.ReadOnly = true;
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품번";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.OptionsColumn.AllowEdit = false;
            this.IT_SCODE.OptionsColumn.ReadOnly = true;
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 1;
            this.IT_SCODE.Width = 405;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.OptionsColumn.AllowEdit = false;
            this.IT_SNAME.OptionsColumn.ReadOnly = true;
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 2;
            this.IT_SNAME.Width = 583;
            // 
            // IT_MODEL
            // 
            this.IT_MODEL.Caption = "모델";
            this.IT_MODEL.FieldName = "IT_MODEL";
            this.IT_MODEL.Name = "IT_MODEL";
            this.IT_MODEL.OptionsColumn.AllowEdit = false;
            this.IT_MODEL.OptionsColumn.ReadOnly = true;
            this.IT_MODEL.Width = 138;
            // 
            // SDATE
            // 
            this.SDATE.Caption = "시작일";
            this.SDATE.FieldName = "SDATE";
            this.SDATE.Name = "SDATE";
            this.SDATE.OptionsColumn.AllowEdit = false;
            this.SDATE.OptionsColumn.ReadOnly = true;
            // 
            // IT_SAFTY
            // 
            this.IT_SAFTY.Caption = "안전재고";
            this.IT_SAFTY.FieldName = "IT_SAFTY";
            this.IT_SAFTY.Name = "IT_SAFTY";
            this.IT_SAFTY.OptionsColumn.AllowEdit = false;
            this.IT_SAFTY.OptionsColumn.ReadOnly = true;
            // 
            // ALC_CODE
            // 
            this.ALC_CODE.Caption = "ALC_CODE";
            this.ALC_CODE.FieldName = "ALC_CODE";
            this.ALC_CODE.Name = "ALC_CODE";
            this.ALC_CODE.OptionsColumn.AllowEdit = false;
            this.ALC_CODE.OptionsColumn.ReadOnly = true;
            this.ALC_CODE.Width = 138;
            // 
            // ME_SCODE
            // 
            this.ME_SCODE.Caption = "자재유형코드";
            this.ME_SCODE.FieldName = "ME_SCODE";
            this.ME_SCODE.Name = "ME_SCODE";
            this.ME_SCODE.OptionsColumn.AllowEdit = false;
            this.ME_SCODE.OptionsColumn.ReadOnly = true;
            // 
            // IT_PKQTY
            // 
            this.IT_PKQTY.Caption = "포장수량";
            this.IT_PKQTY.FieldName = "IT_PKQTY";
            this.IT_PKQTY.Name = "IT_PKQTY";
            this.IT_PKQTY.OptionsColumn.AllowEdit = false;
            this.IT_PKQTY.OptionsColumn.ReadOnly = true;
            // 
            // D_WEIGHT
            // 
            this.D_WEIGHT.Caption = "표준편차";
            this.D_WEIGHT.FieldName = "D_WEIGHT";
            this.D_WEIGHT.Name = "D_WEIGHT";
            this.D_WEIGHT.OptionsColumn.AllowEdit = false;
            this.D_WEIGHT.OptionsColumn.ReadOnly = true;
            // 
            // MATCH_GUBN
            // 
            this.MATCH_GUBN.Caption = "매칭구분";
            this.MATCH_GUBN.FieldName = "MATCH_GUBN";
            this.MATCH_GUBN.Name = "MATCH_GUBN";
            this.MATCH_GUBN.OptionsColumn.AllowEdit = false;
            this.MATCH_GUBN.OptionsColumn.ReadOnly = true;
            // 
            // A_WEIGHT
            // 
            this.A_WEIGHT.Caption = "정미중량";
            this.A_WEIGHT.FieldName = "A_WEIGHT";
            this.A_WEIGHT.Name = "A_WEIGHT";
            this.A_WEIGHT.OptionsColumn.AllowEdit = false;
            this.A_WEIGHT.OptionsColumn.ReadOnly = true;
            // 
            // REMAIN_SQUTY
            // 
            this.REMAIN_SQUTY.Caption = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.FieldName = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.Name = "REMAIN_SQUTY";
            this.REMAIN_SQUTY.OptionsColumn.AllowEdit = false;
            this.REMAIN_SQUTY.OptionsColumn.ReadOnly = true;
            // 
            // WC_SCODE
            // 
            this.WC_SCODE.Caption = "WC_SCODE";
            this.WC_SCODE.FieldName = "WC_SCODE";
            this.WC_SCODE.Name = "WC_SCODE";
            this.WC_SCODE.OptionsColumn.AllowEdit = false;
            this.WC_SCODE.OptionsColumn.ReadOnly = true;
            // 
            // lbl_sdate
            // 
            this.lbl_sdate.Appearance.Font = new System.Drawing.Font("Tahoma", 20F);
            this.lbl_sdate.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbl_sdate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_sdate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_sdate.Location = new System.Drawing.Point(354, 14);
            this.lbl_sdate.Name = "lbl_sdate";
            this.lbl_sdate.Size = new System.Drawing.Size(193, 42);
            this.lbl_sdate.TabIndex = 54;
            // 
            // btn_next_date
            // 
            this.btn_next_date.Location = new System.Drawing.Point(553, 3);
            this.btn_next_date.Name = "btn_next_date";
            this.btn_next_date.Size = new System.Drawing.Size(78, 57);
            this.btn_next_date.TabIndex = 74;
            this.btn_next_date.Text = ">>";
            this.btn_next_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // btn_prev_date
            // 
            this.btn_prev_date.Location = new System.Drawing.Point(270, 3);
            this.btn_prev_date.Name = "btn_prev_date";
            this.btn_prev_date.Size = new System.Drawing.Size(78, 57);
            this.btn_prev_date.TabIndex = 75;
            this.btn_prev_date.Text = "<<";
            this.btn_prev_date.Click += new System.EventHandler(this.btn_date_Click);
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(885, 12);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(277, 44);
            this.progressBarControl1.TabIndex = 76;
            // 
            // btn_day_night
            // 
            this.btn_day_night.Appearance.BackColor = System.Drawing.Color.Yellow;
            this.btn_day_night.Appearance.BackColor2 = System.Drawing.Color.Chocolate;
            this.btn_day_night.Appearance.Font = new System.Drawing.Font("Tahoma", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_day_night.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_day_night.Appearance.Options.UseBackColor = true;
            this.btn_day_night.Appearance.Options.UseFont = true;
            this.btn_day_night.Location = new System.Drawing.Point(653, 3);
            this.btn_day_night.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.btn_day_night.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btn_day_night.Name = "btn_day_night";
            this.btn_day_night.Size = new System.Drawing.Size(132, 57);
            this.btn_day_night.TabIndex = 78;
            this.btn_day_night.Text = "주간";
            this.btn_day_night.Click += new System.EventHandler(this.btn_day_night_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(13, 575);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(162, 70);
            this.button1.TabIndex = 1;
            this.button1.Text = "긴급작지\r\n생성";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AUTO_Po_release_new_search
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 659);
            this.Controls.Add(this.btn_day_night);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.btn_next_date);
            this.Controls.Add(this.btn_prev_date);
            this.Controls.Add(this.lbl_sdate);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_done);
            this.Name = "AUTO_Po_release_new_search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "작업지시현황";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn SITE_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn CARRIER_YN;
        private DevExpress.XtraGrid.Columns.GridColumn MAX_SQTY;
        private DevExpress.XtraGrid.Columns.GridColumn NUM;
        private DevExpress.XtraGrid.Columns.GridColumn MO_SNUMB;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn IT_MODEL;
        private DevExpress.XtraGrid.Columns.GridColumn MO_SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn SDATE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SAFTY;
        private DevExpress.XtraGrid.Columns.GridColumn ALC_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn ME_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_PKQTY;
        private DevExpress.XtraGrid.Columns.GridColumn D_WEIGHT;
        private DevExpress.XtraGrid.Columns.GridColumn MATCH_GUBN;
        private DevExpress.XtraGrid.Columns.GridColumn A_WEIGHT;
        private DevExpress.XtraGrid.Columns.GridColumn REMAIN_SQUTY;
        private DevExpress.XtraGrid.Columns.GridColumn WC_SCODE;
        private DevExpress.XtraEditors.LabelControl lbl_sdate;
        private DevExpress.XtraEditors.SimpleButton btn_next_date;
        private DevExpress.XtraEditors.SimpleButton btn_prev_date;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraEditors.SimpleButton btn_day_night;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.Button button1;
    }
}