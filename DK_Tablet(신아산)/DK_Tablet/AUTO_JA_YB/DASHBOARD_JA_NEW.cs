﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Runtime.InteropServices;
using DK_Tablet.Popup;
using System.IO.Ports;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet;
using LibUsbDotNet.Main;





namespace DK_Tablet
{

    


    public partial class DASHBOARD_JA_NEW : Form
    {

        /// 여기까지 uio.dll을 사용하기 위한 선언부
        public string site_code = "";
        public string str_con = "";

       
        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();

        MAIN parent_form;
        public DASHBOARD_JA_NEW(MAIN form)//생성자
        {
            
            parent_form = form;
            
            CheckForIllegalCrossThreadCalls = false;
            
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)//LOAD 이벤트
        {
           

            this.str_con = Properties.Settings.Default.SQL_DKQT.ToString();
            this.site_code = Properties.Settings.Default.SITE_CODE.ToString();

            Refresh_timer_레이져컷팅.Enabled = true;
            Refresh_timer_레이져컷팅.Start();

            Refresh_timer_클립조립.Enabled = true;
            Refresh_timer_클립조립.Start();

            Refresh_timer_진동.Enabled = true;
            Refresh_timer_진동.Start();

            Refresh_timer_검사기.Enabled = true;
            Refresh_timer_검사기.Start();

            Refresh_timer_초음파융착1.Enabled = true;
            Refresh_timer_초음파융착1.Start();

            Refresh_timer_초음파융착2.Enabled = true;
            Refresh_timer_초음파융착2.Start();

           

            /*
            Refresh_timer_OP.Enabled = true;
            Refresh_timer_OP.Start();
            */
            계획실적_timer.Start();
        }


        private void timer1_Tick(object sender, EventArgs e)//현재시간
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }


        private void Refresh_timer_레이져컷팅_Tick(object sender, EventArgs e)//새로고침 함수
        {
            string[] 레이져컷팅_data = new string[2];

            레이져컷팅_data = get_Equip_In_Out_Data("A1");

            if (레이져컷팅_data[0].ToString().Equals("1"))
            {
                lbl_A1.Text = "작동중";
                lbl_A1.ForeColor = Color.Lime;
                lbl_A1_2.Text = 레이져컷팅_data[1].ToString();
                lbl_A1_2.ForeColor = Color.Lime;
            }
            else if (레이져컷팅_data[0].ToString().Equals("2"))
            {
                lbl_A1.Text = "OK";
                lbl_A1.ForeColor = Color.DeepSkyBlue;
                lbl_A1_2.Text = 레이져컷팅_data[1].ToString();
                lbl_A1_2.ForeColor = Color.DeepSkyBlue;
            }
            else if (레이져컷팅_data[0].ToString().Equals("3"))
            {
                lbl_A1.Text = "NG";
                lbl_A1.ForeColor = Color.Red;
                lbl_A1_2.Text = 레이져컷팅_data[1].ToString();
                lbl_A1_2.ForeColor = Color.Red;
            }
            else if (레이져컷팅_data[0].ToString().Equals("4"))
            {
                lbl_A1.Text = "대기중";
                lbl_A1.ForeColor = Color.DarkGray;
                lbl_A1_2.Text = "";
            }

        }
        private void OK_timer_레이져컷팅_Tick(object sender, EventArgs e)//ok 타이머 5초 셋팅
        {
            lbl_A1.Text = "대기중";
            lbl_A1.ForeColor = Color.DarkGray;
            OK_timer_레이져컷팅.Stop();
        }





        private void Refresh_timer_클립조립_Tick(object sender, EventArgs e)//새로고침 함수
        {
            string[] 클립조립_data = new string[2];

            클립조립_data = get_Equip_In_Out_Data("B1");

            if (클립조립_data[0].ToString().Equals("1"))
            {
                lbl_B1.Text = "작동중";
                lbl_B1.ForeColor = Color.Lime;
                lbl_B1_2.Text = 클립조립_data[1].ToString();
                lbl_B1_2.ForeColor = Color.Lime;
            }
            else if (클립조립_data[0].ToString().Equals("2"))
            {
                lbl_B1.Text = "OK";
                lbl_B1.ForeColor = Color.DeepSkyBlue;
                lbl_B1_2.Text = 클립조립_data[1].ToString();
                lbl_B1_2.ForeColor = Color.DeepSkyBlue;
            }
            else if (클립조립_data[0].ToString().Equals("3"))
            {
                lbl_B1.Text = "NG";
                lbl_B1.ForeColor = Color.Red;
                lbl_B1_2.Text = 클립조립_data[1].ToString();
                lbl_B1_2.ForeColor = Color.Red;
            }
            else if (클립조립_data[0].ToString().Equals("4"))
            {
                lbl_B1.Text = "대기중";
                lbl_B1.ForeColor = Color.DarkGray;
                lbl_B1_2.Text = "";
            }

        }
        private void OK_timer_클립조립_Tick(object sender, EventArgs e)//ok 타이머 5초 셋팅
        {
            lbl_B1.Text = "대기중";
            lbl_B1.ForeColor = Color.DarkGray;
            OK_timer_클립조립.Stop();
        }





        private void OK_timer_초음파1_Tick(object sender, EventArgs e)//ok 타이머 5초셋팅
        {
            lbl_C1.Text = "대기중";
            lbl_C1.ForeColor = Color.DarkGray;
            OK_timer_초음파1.Stop();
        }


        private void Refresh_timer_진동_Tick(object sender, EventArgs e)//새로고침 함수
        {
            string[] 진동_data = new string[2];

            진동_data = get_Equip_In_Out_Data("E1");

            if (진동_data[0].ToString().Equals("1"))
            {
                lbl_E1.Text = "작동중";
                lbl_E1.ForeColor = Color.Lime;
                lbl_E1_2.Text = 진동_data[1].ToString();
                lbl_E1_2.ForeColor = Color.Lime;

            }
            else if (진동_data[0].ToString().Equals("2"))
            {
                lbl_E1.Text = "OK";
                lbl_E1.ForeColor = Color.DeepSkyBlue;

                lbl_E1_2.Text = 진동_data[1].ToString();
                lbl_E1_2.ForeColor = Color.DeepSkyBlue;

            }
            else if (진동_data[0].ToString().Equals("3"))
            {
                lbl_E1.Text = "NG";
                lbl_E1.ForeColor = Color.Red;

                lbl_E1_2.Text = 진동_data[1].ToString();
                lbl_E1_2.ForeColor = Color.Red;
            }
            else if (진동_data[0].ToString().Equals("4"))
            {
                lbl_E1.Text = "대기중";
                lbl_E1.ForeColor = Color.DarkGray;
                lbl_E1_2.Text = "";
            }
        }
        private void OK_timer_진동_Tick(object sender, EventArgs e)//ok 타이머 5초 셋팅
        {
            lbl_E1.Text = "대기중";
            lbl_E1.ForeColor = Color.DarkGray;
            OK_timer_진동.Stop();
        }


        private void Refresh_timer_검사기_Tick(object sender, EventArgs e)//새로고침 함수
        {
            string[] 검사기_data = new string[2];

            검사기_data = get_Equip_In_Out_Data("F1");

            if (검사기_data[0].ToString().Equals("1"))
            {
                lbl_F1.Text = "작동중";
                lbl_F1.ForeColor = Color.Lime;
                lbl_F1_2.Text = 검사기_data[1].ToString();
                lbl_F1_2.ForeColor = Color.Lime;
            }
            else if (검사기_data[0].ToString().Equals("2"))
            {
                lbl_F1.Text = "OK";
                lbl_F1.ForeColor = Color.DeepSkyBlue;

                lbl_F1_2.Text = 검사기_data[1].ToString();
                lbl_F1_2.ForeColor = Color.DeepSkyBlue;
            }
            else if (검사기_data[0].ToString().Equals("3"))
            {
                lbl_F1.Text = "NG";
                lbl_F1.ForeColor = Color.Red;
                lbl_F1_2.Text = 검사기_data[1].ToString();
                lbl_F1_2.ForeColor = Color.Red;
            }
            else if (검사기_data[0].ToString().Equals("4"))
            {
                lbl_F1.Text = "대기중";
                lbl_F1.ForeColor = Color.DarkGray;
                lbl_F1_2.Text = "";
            }
        }
        private void OK_timer_검사기_Tick(object sender, EventArgs e)//ok 타이머 5초 셋팅
        {
            lbl_F1.Text = "대기중";
            lbl_F1.ForeColor = Color.DarkGray;
            OK_timer_검사기.Stop();
        }



        private void 계획실적_timer_Tick(object sender, EventArgs e)//타이머로 읽을 명령어 송신 0.1초마다
        {
            string[] 계획_data = new string[3];
            계획_data = get_계획_실적();

            lbl_plan_sqty.Text = 계획_data[0].ToString();
            lbl_good_sqty.Text = 계획_data[1].ToString();
            lbl_fail_sqty.Text = 계획_data[2].ToString();
        }



        private void labelControl1_Click(object sender, EventArgs e)//패널(받는 데이터) 숨기기/보이기
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            }
        }

        #region clear
        private void simpleButton4_Click(object sender, EventArgs e)//레이져 컷팅 clear
        {
            list_box_레이져컷팅.Items.Clear();
        }
        private void simpleButton3_Click(object sender, EventArgs e)//클립조립 clear
        {
            list_box_클립조립.Items.Clear();
        }
        private void simpleButton5_Click(object sender, EventArgs e)//초음파1 clear
        {
            list_box_초음파1.Items.Clear();
        }
        private void simpleButton1_Click(object sender, EventArgs e)//진동 clear
        {
            list_box_진동.Items.Clear();
        }
        private void simpleButton2_Click(object sender, EventArgs e)//초음파2 clear
        {
            list_box_초음파2.Items.Clear();
        }
        private void simpleButton6_Click(object sender, EventArgs e)//검사기 clear
        {
            list_box_검사기.Items.Clear();
        }
        private void simpleButton7_Click(object sender, EventArgs e)//op clear
        {
            list_box_OP.Items.Clear();
        }
        #endregion



        private void Refresh_timer_초음파융착1_Tick(object sender, EventArgs e)
        {
            string[] 초음파융착1_data = new string[2];

            초음파융착1_data = get_Equip_In_Out_Data("C1");

            if (초음파융착1_data[0].ToString().Equals("1"))
            {
                lbl_C1.Text = "작동중";
                lbl_C1.ForeColor = Color.Lime;
                lbl_C1_2.Text = 초음파융착1_data[1].ToString();
                lbl_C1_2.ForeColor = Color.Lime;
            }
            else if (초음파융착1_data[0].ToString().Equals("2"))
            {
                lbl_C1.Text = "OK";
                lbl_C1.ForeColor = Color.DeepSkyBlue;
                lbl_C1_2.Text = 초음파융착1_data[1].ToString();
                lbl_C1_2.ForeColor = Color.DeepSkyBlue;
            }
            else if (초음파융착1_data[0].ToString().Equals("3"))
            {
                lbl_C1.Text = "NG";
                lbl_C1.ForeColor = Color.Red;
                lbl_C1_2.Text = 초음파융착1_data[1].ToString();
                lbl_C1_2.ForeColor = Color.Red;
            }
            else if (초음파융착1_data[0].ToString().Equals("4"))
            {
                lbl_C1.Text = "대기중";
                lbl_C1.ForeColor = Color.DarkGray;
                lbl_C1_2.Text = "";
            }

        }

        private void Refresh_timer_초음파융착2_Tick(object sender, EventArgs e)
        {
            string[] 초음파융착2_data = new string[2];


            초음파융착2_data = get_Equip_In_Out_Data("C2");

            if (초음파융착2_data[0].ToString().Equals("1"))
            {
                lbl_C2.Text = "작동중";
                lbl_C2.ForeColor = Color.Lime;

                lbl_C2_2.Text = 초음파융착2_data[1].ToString();
                lbl_C2_2.ForeColor = Color.Lime;

            }
            else if (초음파융착2_data[0].ToString().Equals("2"))
            {
                lbl_C2.Text = "OK";
                lbl_C2.ForeColor = Color.DeepSkyBlue;
                lbl_C2_2.Text = 초음파융착2_data[1].ToString();
                lbl_C2_2.ForeColor = Color.DeepSkyBlue;

            }
            else if (초음파융착2_data[0].ToString().Equals("3"))
            {
                lbl_C2.Text = "NG";
                lbl_C2.ForeColor = Color.Red;
                lbl_C2_2.Text = 초음파융착2_data[1].ToString();
                lbl_C2_2.ForeColor = Color.Red;
            }
            else if (초음파융착2_data[0].ToString().Equals("4"))
            {
                lbl_C2.Text = "대기중";
                lbl_C2.ForeColor = Color.DarkGray;
                lbl_C2_2.Text = "";
            }
        }

        private string[] get_Equip_In_Out_Data(string equip_code)
        {
            string[] result = new string[2];

            SqlConnection conn = new SqlConnection(str_con);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("USP_GET_EQUIP_IN_OUT_DATA", conn);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
                cmd.Parameters.AddWithValue("@OP_CODE", equip_code);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    //MessageBox.Show(reader[0].ToString().Trim());

                    // MessageBox.Show(reader[1].ToString().Trim());

                    result[0] = reader[0].ToString().Trim();
                    result[1] = reader[1].ToString().Trim();
                }

                //MessageBox.Show(result[0].ToString());

                //MessageBox.Show(result[1].ToString());
            }
            catch (Exception e)
            {

                //MessageBox.Show(e.Message + e.StackTrace);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }

        // bool result  = usb_in_request(selectID);

        private string[] get_계획_실적()
        {
            string[] result = new string[3];
            result[0] = "0";
            result[1] = "0";
            result[2] = "0";

            SqlConnection conn = new SqlConnection(str_con);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("USP_GET_PLAN_GOOD_FAIL", conn);


                cmd.CommandType = CommandType.StoredProcedure;


                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    //MessageBox.Show(reader[0].ToString().Trim());

                    // MessageBox.Show(reader[1].ToString().Trim());

                    result[0] = reader[0].ToString().Trim();
                    result[1] = reader[1].ToString().Trim();
                    result[2] = reader[2].ToString().Trim();
                }

                //MessageBox.Show(result[0].ToString());

                //MessageBox.Show(result[1].ToString());
            }
            catch (Exception e)
            {

                //MessageBox.Show(e.Message + e.StackTrace);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    loc = 1;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        usb_io_output(selectID, 0, -1, 0, 0, 0);
        //        usb_io_output(selectID, 0, -4, 0, 0, 0);
        //        getBI(blink, -4);
        //    }
        //    catch
        //    {

        //    }
        //}

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    loc = 2;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button5_Click(object sender, EventArgs e)
        //{
        //    loc = 3;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button7_Click(object sender, EventArgs e)
        //{
        //    loc = 4;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    loc = -2;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button6_Click(object sender, EventArgs e)
        //{
        //    loc = -3;
        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button8_Click(object sender, EventArgs e)
        //{
        //    loc = -4;

        //    usb_io_output(selectID, 0, loc, 0, 0, 0);
        //}

        //private void button10_Click(object sender, EventArgs e)
        //{
        //    getBI(blink, loc);
        //}
        //private void getBI(int blink, int loc)
        //{

        //    usb_io_output(selectID, blink, loc, 0, 0, 0);
        //}

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //불량 처리 로직 추가
                string barcode = textEdit1.Text.Trim();
                string[] arrBarcode = barcode.Split('*');
                if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                {
                    string read_it_scode = arrBarcode[0].ToString();
                    string read_lot = arrBarcode[1].ToString();
                    string[] arr_lot = read_lot.Split('/');
                    if (arr_lot.Length.Equals(4))
                    {
                        check_save_read_data(read_it_scode, read_lot);
                    }
                }
                textEdit1.Text = "";


            }
        }
        public void check_save_read_data(string child_it_scode, string child_lot_no)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT_OUT_MIDDLE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@READ_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@READ_LOT_NO", child_lot_no);


            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {

            if (e.Message.Trim().Equals("OK"))
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("불량등록 OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
            }
            else
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        //private void timer1_Tick_1(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        set_usb_events(this.Handle.ToInt32());  // USB로부터 입력 패킷이 수신 되었을 때 WM_INPUT 이벤트가 발생하로록 설정
        //        usb_io_output(selectID, 0, 4, 0, 0, 0);
        //    }
        //    catch
        //    {

        //    }
        //}

        //private void btn_device_no_load_Click(object sender, EventArgs e)
        //{

        //    loadUSB();

        //}



        //USB 연결/해제시 호출됨

        //private void OnDeviceNotifyEvent(object sender, DeviceNotifyEventArgs e)
        //{

        //    set_usb_events(this.Handle.ToInt32());

        //    usb_io_output(selectID, 0, 4, 0, 0, 0);
        //    getBI(blink, 4);

        //    if (e.Device != null)
        //    {

        //    }

        //}



        //USB device를 read 한다

        //private void loadUSB()
        //{

        //    try
        //    {
        //        //finder 방법은 시리얼, vendorID, productID 등의 여러가지 방법이 있다.
        //        mUsbDeviceFinder = new UsbDeviceFinder("0001");
        //        //mUsbDeviceFinder = new UsbDeviceFinder("a5dcbf10-6530-11d2-901f-00c04fb951ed");

        //        mUsbDevice = UsbDevice.OpenUsbDevice(mUsbDeviceFinder);

        //        if (mUsbDevice == null)
        //        {
        //            return;
        //        }
        //        IUsbDevice wholeUsbDevice = mUsbDevice as IUsbDevice;
                
        //        if (!ReferenceEquals(wholeUsbDevice, null))
        //        {
        //            wholeUsbDevice.SetConfiguration(1);
        //            wholeUsbDevice.ClaimInterface(0);
        //        }
        //        UsbEndpointReader reader = mUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01);

        //    }
        //    catch (Exception e)
        //    {
        //    }



        //}


    }
    
}
