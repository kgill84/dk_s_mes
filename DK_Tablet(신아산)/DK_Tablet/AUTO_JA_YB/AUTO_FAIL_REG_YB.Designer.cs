﻿namespace DK_Tablet.AUTO_JA_YB
{
    partial class AUTO_FAIL_REG_YB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AUTO_FAIL_REG_YB));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression1 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression2 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.Sdate = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Refresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.ASSY_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ASSY_BARCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PRDT_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.LASER_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHILD_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHILD_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.A1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.A1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.B1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.B1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.C1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.C1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.E1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.E1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.C2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.C2_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.D1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.D1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.F1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.F1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TOTAL_INS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FINAL_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.R_DATE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHANGE_LOT_NO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.REWORK_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.E2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.E2_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1308, 711);
            this.splitContainerControl1.SplitterPosition = 42;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.btn_close);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1308, 42);
            this.splitContainerControl3.SplitterPosition = 173;
            this.splitContainerControl3.TabIndex = 2;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(1130, 42);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "YB 자동화라인 종합진행 현황";
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.LemonChiffon;
            this.btn_close.Appearance.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_close.Location = new System.Drawing.Point(0, 0);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(173, 42);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl5);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1308, 664);
            this.splitContainerControl2.SplitterPosition = 42;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.textEdit1);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1308, 42);
            this.splitContainerControl4.SplitterPosition = 182;
            this.splitContainerControl4.TabIndex = 1;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.DarkGray;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(0, 0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(182, 42);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "리딩▶";
            // 
            // textEdit1
            // 
            this.textEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(0, 0);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Size = new System.Drawing.Size(1121, 42);
            this.textEdit1.TabIndex = 0;
            this.textEdit1.Enter += new System.EventHandler(this.textEdit1_Enter);
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            this.textEdit1.Leave += new System.EventHandler(this.textEdit1_Leave);
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.splitContainerControl6);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1308, 617);
            this.splitContainerControl5.SplitterPosition = 34;
            this.splitContainerControl5.TabIndex = 1;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Appearance.BackColor = System.Drawing.Color.LavenderBlush;
            this.splitContainerControl6.Appearance.Options.UseBackColor = true;
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.Sdate);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton2);
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainerControl6.Panel2.Controls.Add(this.btn_Refresh);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(1308, 34);
            this.splitContainerControl6.SplitterPosition = 183;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // Sdate
            // 
            this.Sdate.EditValue = null;
            this.Sdate.Location = new System.Drawing.Point(3, 8);
            this.Sdate.Name = "Sdate";
            this.Sdate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sdate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sdate.Size = new System.Drawing.Size(179, 20);
            this.Sdate.TabIndex = 0;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton2.Location = new System.Drawing.Point(252, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(115, 30);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "미완료";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton1.Location = new System.Drawing.Point(131, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(115, 30);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "완료";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Appearance.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.Appearance.Options.UseFont = true;
            this.btn_Refresh.Image = ((System.Drawing.Image)(resources.GetObject("btn_Refresh.Image")));
            this.btn_Refresh.Location = new System.Drawing.Point(10, 3);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(115, 30);
            this.btn_Refresh.TabIndex = 0;
            this.btn_Refresh.Text = "새로고침";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1308, 578);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.advBandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.advBandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 14F);
            this.advBandedGridView1.Appearance.Row.Options.UseFont = true;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand6,
            this.gridBand3,
            this.gridBand8,
            this.gridBand7,
            this.gridBand9,
            this.gridBand10,
            this.gridBand1,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.R_DATE,
            this.ASSY_ITEM,
            this.ASSY_BARCODE,
            this.CHILD_ITEM,
            this.CHILD_LOT,
            this.CHANGE_LOT_NO,
            this.PRDT_ITEM,
            this.LASER_LOT,
            this.A1,
            this.B1,
            this.C1,
            this.E1,
            this.E2,
            this.C2,
            this.D1,
            this.F1,
            this.TOTAL_INS,
            this.A1_GUBN,
            this.B1_GUBN,
            this.C1_GUBN,
            this.E1_GUBN,
            this.E2_GUBN,
            this.C2_GUBN,
            this.D1_GUBN,
            this.F1_GUBN,
            this.FINAL_GUBN,
            this.REWORK_GUBN});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Salmon;
            formatConditionRuleExpression1.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression1.Expression = "[REWORK_GUBN] = \'X\'";
            gridFormatRule1.Rule = formatConditionRuleExpression1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            formatConditionRuleExpression2.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression2.Expression = "[REWORK_GUBN] = \'Y\' And ([FINAL_GUBN] = \'재작업\' Or [A1_GUBN] = \'재작업\' Or [B1_GUBN] =" +
    " \'재작업\' Or [C1_GUBN] = \'재작업\' Or [C2_GUBN] = \'재작업\' Or [D1_GUBN] = \'재작업\' Or [E1_GUB" +
    "N] = \'재작업\' Or [F1_GUBN] = \'재검사\')";
            gridFormatRule2.Rule = formatConditionRuleExpression2;
            this.advBandedGridView1.FormatRules.Add(gridFormatRule1);
            this.advBandedGridView1.FormatRules.Add(gridFormatRule2);
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsBehavior.ReadOnly = true;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.ShowBands = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.advBandedGridView1_MouseWheel);
            // 
            // ASSY_ITEM
            // 
            this.ASSY_ITEM.Caption = "완성품 품번";
            this.ASSY_ITEM.FieldName = "ASSY_ITEM";
            this.ASSY_ITEM.Name = "ASSY_ITEM";
            this.ASSY_ITEM.OptionsColumn.AllowEdit = false;
            this.ASSY_ITEM.OptionsColumn.ReadOnly = true;
            this.ASSY_ITEM.Visible = true;
            this.ASSY_ITEM.Width = 185;
            // 
            // ASSY_BARCODE
            // 
            this.ASSY_BARCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ASSY_BARCODE.AppearanceCell.Options.UseFont = true;
            this.ASSY_BARCODE.Caption = "완제품로트";
            this.ASSY_BARCODE.FieldName = "ASSY_BARCODE";
            this.ASSY_BARCODE.Name = "ASSY_BARCODE";
            this.ASSY_BARCODE.RowIndex = 1;
            this.ASSY_BARCODE.Visible = true;
            this.ASSY_BARCODE.Width = 185;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.Caption = "레이저 품번";
            this.PRDT_ITEM.FieldName = "PRDT_ITEM";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.Visible = true;
            this.PRDT_ITEM.Width = 199;
            // 
            // LASER_LOT
            // 
            this.LASER_LOT.Caption = "레이저로트";
            this.LASER_LOT.FieldName = "LASER_LOT";
            this.LASER_LOT.Name = "LASER_LOT";
            this.LASER_LOT.RowIndex = 1;
            this.LASER_LOT.Visible = true;
            this.LASER_LOT.Width = 199;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.Caption = "사출품";
            this.CHILD_ITEM.FieldName = "CHILD_ITEM";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.OptionsColumn.ReadOnly = true;
            this.CHILD_ITEM.Visible = true;
            this.CHILD_ITEM.Width = 192;
            // 
            // CHILD_LOT
            // 
            this.CHILD_LOT.Caption = "사출LOT";
            this.CHILD_LOT.FieldName = "CHILD_LOT";
            this.CHILD_LOT.Name = "CHILD_LOT";
            this.CHILD_LOT.OptionsColumn.ReadOnly = true;
            this.CHILD_LOT.RowIndex = 1;
            this.CHILD_LOT.Visible = true;
            this.CHILD_LOT.Width = 192;
            // 
            // A1
            // 
            this.A1.AppearanceCell.Options.UseTextOptions = true;
            this.A1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.A1.Caption = "레이저컷팅";
            this.A1.FieldName = "A1";
            this.A1.Name = "A1";
            this.A1.Visible = true;
            // 
            // A1_GUBN
            // 
            this.A1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.A1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.A1_GUBN.Caption = "처리구분A";
            this.A1_GUBN.FieldName = "A1_GUBN";
            this.A1_GUBN.Name = "A1_GUBN";
            this.A1_GUBN.RowIndex = 1;
            this.A1_GUBN.Visible = true;
            // 
            // B1
            // 
            this.B1.AppearanceCell.Options.UseTextOptions = true;
            this.B1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.B1.Caption = "클립조립";
            this.B1.FieldName = "B1";
            this.B1.Name = "B1";
            this.B1.Visible = true;
            // 
            // B1_GUBN
            // 
            this.B1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.B1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.B1_GUBN.Caption = "처리구분B";
            this.B1_GUBN.FieldName = "B1_GUBN";
            this.B1_GUBN.Name = "B1_GUBN";
            this.B1_GUBN.RowIndex = 1;
            this.B1_GUBN.Visible = true;
            // 
            // C1
            // 
            this.C1.AppearanceCell.Options.UseTextOptions = true;
            this.C1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.C1.Caption = "초음파융착1";
            this.C1.FieldName = "C1";
            this.C1.Name = "C1";
            this.C1.Visible = true;
            // 
            // C1_GUBN
            // 
            this.C1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.C1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.C1_GUBN.Caption = "처리구분C1";
            this.C1_GUBN.FieldName = "C1_GUBN";
            this.C1_GUBN.Name = "C1_GUBN";
            this.C1_GUBN.RowIndex = 1;
            this.C1_GUBN.Visible = true;
            // 
            // E1
            // 
            this.E1.AppearanceCell.Options.UseTextOptions = true;
            this.E1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.E1.Caption = "진동융착";
            this.E1.FieldName = "E1";
            this.E1.Name = "E1";
            this.E1.Visible = true;
            // 
            // E1_GUBN
            // 
            this.E1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.E1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.E1_GUBN.Caption = "처리구분E1";
            this.E1_GUBN.FieldName = "E1_GUBN";
            this.E1_GUBN.Name = "E1_GUBN";
            this.E1_GUBN.RowIndex = 1;
            this.E1_GUBN.Visible = true;
            // 
            // C2
            // 
            this.C2.AppearanceCell.Options.UseTextOptions = true;
            this.C2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.C2.Caption = "초음파융착2";
            this.C2.FieldName = "C2";
            this.C2.Name = "C2";
            this.C2.Visible = true;
            // 
            // C2_GUBN
            // 
            this.C2_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.C2_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.C2_GUBN.Caption = "처리구분C2";
            this.C2_GUBN.FieldName = "C2_GUBN";
            this.C2_GUBN.Name = "C2_GUBN";
            this.C2_GUBN.RowIndex = 1;
            this.C2_GUBN.Visible = true;
            // 
            // D1
            // 
            this.D1.AppearanceCell.Options.UseTextOptions = true;
            this.D1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.D1.Caption = "터치업";
            this.D1.FieldName = "D1";
            this.D1.Name = "D1";
            this.D1.Visible = true;
            // 
            // D1_GUBN
            // 
            this.D1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.D1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.D1_GUBN.Caption = "처리구분D1";
            this.D1_GUBN.FieldName = "D1_GUBN";
            this.D1_GUBN.Name = "D1_GUBN";
            this.D1_GUBN.RowIndex = 1;
            this.D1_GUBN.Visible = true;
            // 
            // F1
            // 
            this.F1.AppearanceCell.Options.UseTextOptions = true;
            this.F1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.F1.Caption = "검사";
            this.F1.FieldName = "F1";
            this.F1.Name = "F1";
            this.F1.Visible = true;
            // 
            // F1_GUBN
            // 
            this.F1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.F1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.F1_GUBN.Caption = "처리구분F1";
            this.F1_GUBN.FieldName = "F1_GUBN";
            this.F1_GUBN.Name = "F1_GUBN";
            this.F1_GUBN.RowIndex = 1;
            this.F1_GUBN.Visible = true;
            // 
            // TOTAL_INS
            // 
            this.TOTAL_INS.AppearanceCell.Options.UseTextOptions = true;
            this.TOTAL_INS.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TOTAL_INS.Caption = "최종";
            this.TOTAL_INS.FieldName = "TOTAL_INS";
            this.TOTAL_INS.Name = "TOTAL_INS";
            this.TOTAL_INS.Visible = true;
            // 
            // FINAL_GUBN
            // 
            this.FINAL_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.FINAL_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FINAL_GUBN.Caption = "처리구분";
            this.FINAL_GUBN.FieldName = "FINAL_GUBN";
            this.FINAL_GUBN.Name = "FINAL_GUBN";
            this.FINAL_GUBN.RowIndex = 1;
            this.FINAL_GUBN.Visible = true;
            // 
            // R_DATE
            // 
            this.R_DATE.Caption = "불량발생일";
            this.R_DATE.FieldName = "R_DATE";
            this.R_DATE.Name = "R_DATE";
            this.R_DATE.OptionsColumn.AllowEdit = false;
            this.R_DATE.OptionsColumn.ReadOnly = true;
            // 
            // CHANGE_LOT_NO
            // 
            this.CHANGE_LOT_NO.Caption = "사출변환LOT";
            this.CHANGE_LOT_NO.FieldName = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Name = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Visible = true;
            // 
            // REWORK_GUBN
            // 
            this.REWORK_GUBN.Caption = "재작업구분";
            this.REWORK_GUBN.FieldName = "REWORK_GUBN";
            this.REWORK_GUBN.Name = "REWORK_GUBN";
            // 
            // repositoryItemButtonEdit1
            // 
            serializableAppearanceObject1.BackColor = System.Drawing.Color.LightBlue;
            serializableAppearanceObject1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            serializableAppearanceObject1.Options.UseBackColor = true;
            serializableAppearanceObject1.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "불량처리", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ReadOnly = true;
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // E2
            // 
            this.E2.Caption = "진동융착2";
            this.E2.FieldName = "E2";
            this.E2.Name = "E2";
            this.E2.Visible = true;
            // 
            // E2_GUBN
            // 
            this.E2_GUBN.Caption = "처리구분E2";
            this.E2_GUBN.FieldName = "E2_GUBN";
            this.E2_GUBN.Name = "E2_GUBN";
            this.E2_GUBN.RowIndex = 1;
            this.E2_GUBN.Visible = true;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Columns.Add(this.ASSY_ITEM);
            this.gridBand2.Columns.Add(this.ASSY_BARCODE);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 185;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Columns.Add(this.PRDT_ITEM);
            this.gridBand6.Columns.Add(this.LASER_LOT);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 199;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Columns.Add(this.CHILD_ITEM);
            this.gridBand3.Columns.Add(this.CHILD_LOT);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 192;
            // 
            // gridBand8
            // 
            this.gridBand8.Caption = "gridBand8";
            this.gridBand8.Columns.Add(this.A1);
            this.gridBand8.Columns.Add(this.A1_GUBN);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 3;
            this.gridBand8.Width = 75;
            // 
            // gridBand7
            // 
            this.gridBand7.Caption = "gridBand7";
            this.gridBand7.Columns.Add(this.B1);
            this.gridBand7.Columns.Add(this.B1_GUBN);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 4;
            this.gridBand7.Width = 75;
            // 
            // gridBand9
            // 
            this.gridBand9.Caption = "gridBand9";
            this.gridBand9.Columns.Add(this.C1);
            this.gridBand9.Columns.Add(this.C1_GUBN);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 5;
            this.gridBand9.Width = 75;
            // 
            // gridBand10
            // 
            this.gridBand10.Caption = "gridBand10";
            this.gridBand10.Columns.Add(this.E1);
            this.gridBand10.Columns.Add(this.E1_GUBN);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 6;
            this.gridBand10.Width = 75;
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Columns.Add(this.E2);
            this.gridBand1.Columns.Add(this.E2_GUBN);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 7;
            this.gridBand1.Width = 75;
            // 
            // gridBand11
            // 
            this.gridBand11.Caption = "gridBand11";
            this.gridBand11.Columns.Add(this.C2);
            this.gridBand11.Columns.Add(this.C2_GUBN);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 8;
            this.gridBand11.Width = 75;
            // 
            // gridBand12
            // 
            this.gridBand12.Caption = "gridBand12";
            this.gridBand12.Columns.Add(this.D1);
            this.gridBand12.Columns.Add(this.D1_GUBN);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 9;
            this.gridBand12.Width = 75;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Columns.Add(this.F1);
            this.gridBand13.Columns.Add(this.F1_GUBN);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 10;
            this.gridBand13.Width = 75;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "gridBand14";
            this.gridBand14.Columns.Add(this.TOTAL_INS);
            this.gridBand14.Columns.Add(this.FINAL_GUBN);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 11;
            this.gridBand14.Width = 75;
            // 
            // AUTO_FAIL_REG_YB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1308, 711);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AUTO_FAIL_REG_YB";
            this.Text = "AUTO_FAIL_REG";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.AUTO_FAIL_REG_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.SimpleButton btn_Refresh;
        private DevExpress.XtraEditors.DateEdit Sdate;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_BARCODE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LASER_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PRDT_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn A1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn B1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn C1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn E1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn C2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn D1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn F1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TOTAL_INS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn R_DATE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHANGE_LOT_NO;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn A1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn B1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn C1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn E1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn C2_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn D1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn F1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FINAL_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn REWORK_GUBN;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn E2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn E2_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
    }
}