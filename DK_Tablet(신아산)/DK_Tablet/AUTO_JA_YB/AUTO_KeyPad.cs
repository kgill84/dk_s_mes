﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DK_Tablet.Popup
{
    public partial class AUTO_KeyPad : Form
    {
        public string txt_value { get; set; }
        public float int_value { get; set; }
        public AUTO_KeyPad()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string btn_tag = btn.Tag.ToString();
            if (btn_tag == "0" || btn_tag == "1" || btn_tag == "2" || btn_tag == "3" || btn_tag == "4" || btn_tag == "5" || btn_tag == "6" || btn_tag == "7" || btn_tag == "8" || btn_tag == "9")
            {
                if (txtDigit.EditValue.ToString() == "0")
                {
                    txtDigit.EditValue = "";
                }
                bool set = Regex.IsMatch(txtDigit.EditValue.ToString(), @"^[0-9]+[.][0]");
                if (set)
                {
                    txtDigit.EditValue = txtDigit.EditValue.ToString().Substring(0, txtDigit.EditValue.ToString().Length - 1);
                }
                txtDigit.EditValue = txtDigit.EditValue + btn_tag;
            }
            if (btn_tag == ".0")
            {
                txtDigit.EditValue = txtDigit.EditValue + btn_tag;
            }
            if (btn_tag == "back")
            {
                txtDigit.EditValue = txtDigit.EditValue.ToString().Substring(0, txtDigit.EditValue.ToString().Length - 1);
                if (txtDigit.EditValue.ToString() == "")
                {
                    txtDigit.EditValue = "0";
                }
            }

        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            txt_value = txtDigit.EditValue.ToString();
            int_value = float.Parse(txtDigit.EditValue.ToString());
            DialogResult = DialogResult.OK;
        }

        private void KeyPad_Load(object sender, EventArgs e)
        {
            txtDigit.EditValue = txt_value;
        }
    }
}
