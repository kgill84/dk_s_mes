﻿using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    public partial class AUTO_REWORK_REG : Form
    {
        MAIN parent_form;
        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();
        DataTable FAIL_DT = new DataTable();
        public AUTO_REWORK_REG(MAIN form)
        {
            parent_form = form;
            InitializeComponent();
        }
        private void AUTO_FAIL_REG_Load(object sender, EventArgs e)
        {
            textEdit1.Focus();
            FAIL_DT.Columns.Add("R_DATE",typeof(string));
            FAIL_DT.Columns.Add("IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_IT_SCODE", typeof(string));
            FAIL_DT.Columns.Add("CHILD_LOT_NO", typeof(string));
            FAIL_DT.Columns.Add("CHANGE_LOT_NO", typeof(string));            
            FAIL_DT.Columns.Add("REWORK_OP", typeof(string));

            gridControl1.DataSource = FAIL_DT;




            REWORK_LookUpEdit1.DataSource = OP_CODE_DropDown("JA");
            REWORK_LookUpEdit1.DisplayMember = "OP_NAME";
            REWORK_LookUpEdit1.ValueMember = "OP_CODE";
        }
        public DataTable OP_CODE_DropDown(string CAR_TYPE)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_AUTO_LOOKUP_EDIT_GET_OP";
            //strQury = "";
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("CAR_TYPE",CAR_TYPE);

            try
            {
                ds = new DataSet();

                da.Fill(ds, "LOOKUP_EDIT_GET_OP");
                dt = ds.Tables["LOOKUP_EDIT_GET_OP"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                string barcode = textEdit1.Text.Trim();
                string[] arrBarcode = barcode.Split('*');
                if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                {
                    string read_it_scode = arrBarcode[0].ToString();
                    string read_lot = arrBarcode[1].ToString();
                    string[] arr_lot = read_lot.Split('/');
                    if (arr_lot.Length.Equals(4))
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("R_DATE", typeof(string));
                        dt.Columns.Add("IT_SCODE", typeof(string));
                        dt.Columns.Add("CHILD_IT_SCODE", typeof(string));
                        dt.Columns.Add("CHILD_LOT_NO", typeof(string));
                        dt.Columns.Add("CHANGE_LOT_NO", typeof(string));                        
                        dt.Columns.Add("REWORK_OP", typeof(string));
                        
                        dt= get_fail_data(read_it_scode, read_lot);
                        if (dt.Rows.Count > 0)
                        {

                            if (string.IsNullOrWhiteSpace(dt.Rows[0]["CHANGE_LOT_NO"].ToString()))
                            {

                            }
                            else
                            {
                                DataRow[] drr = FAIL_DT.Select("CHANGE_LOT_NO='" + dt.Rows[0]["CHANGE_LOT_NO"].ToString() + "'");
                                if (drr.Length < 1)
                                {
                                    
                                    
                                    DataRow dr = FAIL_DT.NewRow();
                                    dr["R_DATE"] = dt.Rows[0]["R_DATE"].ToString();
                                    dr["IT_SCODE"] = dt.Rows[0]["IT_SCODE"].ToString();
                                    dr["CHILD_IT_SCODE"] = dt.Rows[0]["CHILD_IT_SCODE"].ToString();
                                    dr["CHILD_LOT_NO"] = dt.Rows[0]["CHILD_LOT_NO"].ToString();
                                    dr["CHANGE_LOT_NO"] = dt.Rows[0]["CHANGE_LOT_NO"].ToString();

                                    dr["REWORK_OP"] = "";
                                    FAIL_DT.Rows.Add(dr);
                                }
                            }
                        }

                    }
                }
                else
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("잘못된 QR 코드 입니다.", 5);
                    PpCard_Success.BackColor = Color.Salmon;
                }
                textEdit1.Focus();
                textEdit1.Text = "";
            }
            
        }

        

        //불량 처리 데이터 가져오기
        public DataTable get_fail_data(string child_it_scode, string child_lot_no)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "USP_AUTO_GET_REWORK_DATA";

            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;

            da.SelectCommand.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            da.SelectCommand.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);

            DataTable dt = null;
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "SEARCH_REWORK");
                dt = ds.Tables["SEARCH_REWORK"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            string[] str_arr = e.Message.Trim().Split('/');
            if (e.Message.Trim().Substring(0,2).Equals("OK"))
            {
                string message_str = "";
                message_str = e.Message.Trim().Substring(e.Message.Trim().IndexOf("/") + 1, e.Message.Trim().Length - 3);
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("OK : "+message_str, 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
            }
            else
            {
                
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }
        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

            string change_lot_no = "", rework_op = ""; ;
            change_lot_no = gridView1.GetFocusedRowCellValue("CHANGE_LOT_NO").ToString();
            rework_op = gridView1.GetFocusedRowCellValue("REWORK_OP").ToString();
            if (string.IsNullOrWhiteSpace(rework_op))
            {
                MessageBox.Show("재작업공정을 선택해 주세요");
                return;
            }
            AUTO_REWORK_POPUP_2 AUTO_REWORK_POPUP_2 = new AUTO_REWORK_POPUP_2();

            DialogResult dialog = AUTO_REWORK_POPUP_2.ShowDialog();
            if (dialog == System.Windows.Forms.DialogResult.Yes)
            {
                //불량 처리 하는 SP
                REWORK_save(change_lot_no, "OK", rework_op);
                DataRow[] drr = FAIL_DT.Select("CHANGE_LOT_NO='" + change_lot_no + "'");
                FAIL_DT.Rows.Remove(drr[0]);

            }
            else if (dialog == System.Windows.Forms.DialogResult.No)
            {
                REWORK_save(change_lot_no, "NG", rework_op);

            }
            else
            {

            }
            textEdit1.Focus();
        }

        public string REWORK_save(string change_lot_no,string ok_ng,string on_op)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string send_lot = "";
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_REWORK_OK_NG_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@CHANGE_LOT_NO", change_lot_no);
            cmd.Parameters.AddWithValue("@OK_NG", ok_ng);
            cmd.Parameters.AddWithValue("@REWORK_OP", on_op);


            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return send_lot;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            FAIL_DT.Rows.Clear();
            textEdit1.Focus();
        }

    }
}
