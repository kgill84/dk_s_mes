﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Runtime.InteropServices;
using DK_Tablet.Popup;
using System.IO.Ports;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet;
using LibUsbDotNet.Main;





namespace DK_Tablet
{

    


    public partial class DASHBOARD_JA_NEW_2 : Form
    {

        /// 여기까지 uio.dll을 사용하기 위한 선언부
        public string site_code = "";
        public string str_con = "";

        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();

        MAIN parent_form;
        public DASHBOARD_JA_NEW_2(MAIN form)//생성자
        {
            
            parent_form = form;
           
            CheckForIllegalCrossThreadCalls = false;
           
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)//LOAD 이벤트
        {
            
            this.str_con = Properties.Settings.Default.SQL_DKQT.ToString();
            this.site_code = Properties.Settings.Default.SITE_CODE.ToString();
            
            계획실적_timer.Start();
        }


        private void timer1_Tick(object sender, EventArgs e)//현재시간
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }





        private void 계획실적_timer_Tick(object sender, EventArgs e)//타이머로 읽을 명령어 송신 0.1초마다
        {
            string[] 계획_data = new string[3];
            계획_data = get_계획_실적();

            lbl_plan_sqty.Text = 계획_data[0].ToString();
            lbl_good_sqty.Text = 계획_data[1].ToString();
            lbl_fail_sqty.Text = 계획_data[2].ToString();
        }




        // bool result  = usb_in_request(selectID);

        private string[] get_계획_실적()
        {
            string[] result = new string[3];
            result[0] = "0";
            result[1] = "0";
            result[2] = "0";

            SqlConnection conn = new SqlConnection(str_con);

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("USP_GET_PLAN_GOOD_FAIL", conn);


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("IT_MODEL", "JA");

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    //MessageBox.Show(reader[0].ToString().Trim());

                    // MessageBox.Show(reader[1].ToString().Trim());

                    result[0] = reader[0].ToString().Trim();
                    result[1] = reader[1].ToString().Trim();
                    result[2] = reader[2].ToString().Trim();
                }

                //MessageBox.Show(result[0].ToString());

                //MessageBox.Show(result[1].ToString());
            }
            catch (Exception e)
            {

                //MessageBox.Show(e.Message + e.StackTrace);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }
       


        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //불량 처리 로직 추가
                string barcode = textEdit1.Text.Trim();
                string[] arrBarcode = barcode.Split('*');
                if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                {
                    string read_it_scode = arrBarcode[0].ToString();
                    string read_lot = arrBarcode[1].ToString();
                    string[] arr_lot = read_lot.Split('/');
                    if (arr_lot.Length.Equals(4))
                    {
                        check_save_read_data(read_it_scode, read_lot);
                    }
                }
                textEdit1.Text = "";


            }
        }
        public void check_save_read_data(string child_it_scode, string child_lot_no)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT_OUT_MIDDLE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@READ_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@READ_LOT_NO", child_lot_no);


            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {

            if (e.Message.Trim().Equals("OK"))
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("불량등록 OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
            }
            else
            {
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
