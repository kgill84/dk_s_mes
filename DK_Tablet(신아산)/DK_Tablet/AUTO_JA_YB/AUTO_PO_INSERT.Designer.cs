﻿namespace DK_Tablet.AUTO_JA_YB
{
    partial class AUTO_PO_INSERT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IT_SCODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_SNAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IT_MODEL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ALC_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MO_SQUTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_done
            // 
            this.btn_done.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_done.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_done.Location = new System.Drawing.Point(842, 575);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(162, 70);
            this.btn_done.TabIndex = 1;
            this.btn_done.Text = "등록";
            this.btn_done.UseVisualStyleBackColor = false;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_close.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.btn_close.Location = new System.Drawing.Point(1010, 575);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(152, 70);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::DK_Tablet.Properties.Resources.headerbar;
            this.pictureBox1.Location = new System.Drawing.Point(13, 63);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1149, 6);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("굴림", 20F, System.Drawing.FontStyle.Bold);
            this.button3.Image = global::DK_Tablet.Properties.Resources.simbol;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(13, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(336, 41);
            this.button3.TabIndex = 2;
            this.button3.TabStop = false;
            this.button3.Text = "긴급작업지시 등록";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(13, 76);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1149, 492);
            this.gridControl1.TabIndex = 53;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.gridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 20F);
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IT_SCODE,
            this.IT_SNAME,
            this.IT_MODEL,
            this.ALC_CODE,
            this.MO_SQUTY});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.RowHeight = 60;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // IT_SCODE
            // 
            this.IT_SCODE.Caption = "품번";
            this.IT_SCODE.FieldName = "IT_SCODE";
            this.IT_SCODE.Name = "IT_SCODE";
            this.IT_SCODE.Visible = true;
            this.IT_SCODE.VisibleIndex = 1;
            this.IT_SCODE.Width = 414;
            // 
            // IT_SNAME
            // 
            this.IT_SNAME.Caption = "품명";
            this.IT_SNAME.FieldName = "IT_SNAME";
            this.IT_SNAME.Name = "IT_SNAME";
            this.IT_SNAME.Visible = true;
            this.IT_SNAME.VisibleIndex = 2;
            this.IT_SNAME.Width = 168;
            // 
            // IT_MODEL
            // 
            this.IT_MODEL.Caption = "모델";
            this.IT_MODEL.FieldName = "IT_MODEL";
            this.IT_MODEL.Name = "IT_MODEL";
            this.IT_MODEL.Visible = true;
            this.IT_MODEL.VisibleIndex = 3;
            this.IT_MODEL.Width = 179;
            // 
            // ALC_CODE
            // 
            this.ALC_CODE.Caption = "ALC_CODE";
            this.ALC_CODE.FieldName = "ALC_CODE";
            this.ALC_CODE.Name = "ALC_CODE";
            this.ALC_CODE.Visible = true;
            this.ALC_CODE.VisibleIndex = 0;
            this.ALC_CODE.Width = 215;
            // 
            // MO_SQUTY
            // 
            this.MO_SQUTY.AppearanceCell.BackColor = System.Drawing.Color.Yellow;
            this.MO_SQUTY.AppearanceCell.BackColor2 = System.Drawing.Color.White;
            this.MO_SQUTY.AppearanceCell.Options.UseBackColor = true;
            this.MO_SQUTY.AppearanceCell.Options.UseTextOptions = true;
            this.MO_SQUTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.MO_SQUTY.Caption = "작지수량";
            this.MO_SQUTY.FieldName = "MO_SQUTY";
            this.MO_SQUTY.Name = "MO_SQUTY";
            this.MO_SQUTY.Visible = true;
            this.MO_SQUTY.VisibleIndex = 4;
            this.MO_SQUTY.Width = 155;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(885, 12);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Properties.ShowTitle = true;
            this.progressBarControl1.Size = new System.Drawing.Size(277, 44);
            this.progressBarControl1.TabIndex = 76;
            // 
            // AUTO_PO_INSERT
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 659);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_done);
            this.Name = "AUTO_PO_INSERT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "작업지시현황";
            this.Load += new System.EventHandler(this.po_realese_search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SCODE;
        private DevExpress.XtraGrid.Columns.GridColumn IT_SNAME;
        private DevExpress.XtraGrid.Columns.GridColumn IT_MODEL;
        private DevExpress.XtraGrid.Columns.GridColumn ALC_CODE;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private DevExpress.XtraGrid.Columns.GridColumn MO_SQUTY;
    }
}