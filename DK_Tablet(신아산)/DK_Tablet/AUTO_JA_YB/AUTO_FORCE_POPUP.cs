﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    public partial class AUTO_FORCE_POPUP : Form
    {
        public string str_title = "";
        public AUTO_FORCE_POPUP()
        {
            InitializeComponent();
        }

        private void AUTO_REWORK_POPUP_Load(object sender, EventArgs e)
        {
            lblTitle.Text = str_title;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
