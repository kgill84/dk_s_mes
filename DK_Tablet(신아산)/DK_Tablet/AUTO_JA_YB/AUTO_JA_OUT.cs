﻿using DK_Tablet.AUTO_JA_YB;
using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using DK_Tablet.PRINT;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class AUTO_JA_OUT : Form
    {
        bool 완료전송_check = false;
        bool 검사기_toggle = false;
        bool start_bool = false;
        string ip_검사기 = "172.168.100.50";
        SUB_SAVE SUB_SAVE = new SUB_SAVE();

        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        DataTable SELECT_DT = new DataTable();
        public string str_wc_code = "";//작업장
        //public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();
        private AUTO_Success_Form Success_Form = new AUTO_Success_Form();
        Func_Mobis_Print Func_Mobis_Print = new Func_Mobis_Print();
        

        string po_sdate = "";
        string day_night = "";
        string REWORK_FLAG = "";

        MAIN parent_form;
        public AUTO_JA_OUT(MAIN form)//생성자
        {
            parent_form = form;
            CheckForIllegalCrossThreadCalls = false;
            parent_form.Visible = false;
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)//LOAD 이벤트
        {
            SELECT_DT.Columns.Add("SPEC",typeof(string));
            SELECT_DT.Columns.Add("IT_SCODE", typeof(string));
            SELECT_DT.Columns.Add("IT_SNAME", typeof(string));
            SELECT_DT.Columns.Add("PAB", typeof(string));
            SELECT_DT.Columns.Add("RH_LH", typeof(string));
            SELECT_DT.Columns.Add("SSB", typeof(string));
            SELECT_DT.Columns.Add("PLAN_SQTY", typeof(int));
            SELECT_DT.Columns.Add("GOOD_SQTY", typeof(int));
            SELECT_DT.Columns.Add("FAIL_SQTY", typeof(int));
            SELECT_DT.Columns.Add("TOTAL_CNT", typeof(int));
            SELECT_DT.Columns.Add("IT_PKQTY", typeof(int));
            SELECT_DT.Columns.Add("CNT", typeof(int));

            
            reading_data.Columns.Add("READING_DATA");
            reading_data.Columns.Add("S_DATE");
            reading_data.Columns.Add("RESULT");
            reading_data.Columns.Add("NG_DESCR");

            gridControl2.DataSource = reading_data;

            EQUIP.Add("A1", "OFF");
            EQUIP.Add("B1", "OFF");
            EQUIP.Add("C1", "OFF");
            EQUIP.Add("E1", "OFF");
            //EQUIP.Add("E2", "OFF");
            EQUIP.Add("C2", "OFF");
            EQUIP.Add("F1", "OFF");

            dateEdit1.DateTime = DateTime.Now;
            Success_Form.Show();
            Success_Form.Visible = false;
            lueWc_code.Properties.DataSource = AUTO_GET_DATA.WccodeSelect_DropDown("PFR");
            lueWc_code.Properties.DisplayMember = "WC_NAME";
            lueWc_code.Properties.ValueMember = "WC_CODE";
            //lookUpEdit1.SelectionStart = 0;
            //lueWc_code.ItemIndex = 0;
            if (regKey.GetValue("WC_CODE") == null)
            {
                regKey.SetValue("WC_CODE", "");
            }
            else
            {
                if (regKey.GetValue("WC_CODE").ToString() != "")
                {

                    lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                }
            }

            
            winsock1Connect_검사기();
            SELECT_DT = GET_PLAN_ACCEPT_NEW();
            gridControl1.DataSource = SELECT_DT;
            start_bool = true;

            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            new Thread(new ThreadStart(write_send_검사기_완료신호1)).Start();
            //Thread.Sleep(1000);
            start_bool = true;
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }

            new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start(); 
        }


        private void timer1_Tick(object sender, EventArgs e)//현재시간
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }


       

        #region 검사기(MELSEC)
        string 검사기_data;
        private void winsock1Connect_검사기()//통신연결
        {
            try
            {
                if (winsock_검사기.GetState.ToString() != "Connected")
                {

                    winsock_검사기.LocalPort = 3001;

                    winsock_검사기.RemoteIP = ip_검사기;

                    winsock_검사기.RemotePort = 3000;

                    winsock_검사기.Connect();

                }
            }
            catch (Exception ex)
            {

            }
        }
        private void winsock1_DataArrival_검사기(MelsecPLC.Winsock sender, int BytesTotal)//받은데이터 처리
        {
            //String s = String.Empty;
            //winsock1.GetData(ref s);
            //textBox2.Text = s.ToString();
            byte[] bt = new byte[1024];
            winsock_검사기.GetData(ref bt);
            string recive_str = "";


            for (int i = 0; i < bt.Length; i++)
            {
                if (i == 9 || i == 10)
                {
                    if (!bt[i].ToString("X2").Equals("00"))
                    {
                        //오류
                        recive_str = "Error";
                        list_box_검사기.Items.Add(recive_str);
                        outputList_Scroll();
                        return;
                    }
                }
                if (i > 10)
                {
                    //recive_str += bt[i].ToString("X2");
                    recive_str += string.Format("{0} ", bt[i]);
                }
            }

            recive_str = recive_str.Trim();
            검사기_data = recive_str;

            if (!string.IsNullOrWhiteSpace(검사기_data))
            {
                string[] deciValuesSplit = 검사기_data.Split(' ');
                string StringOut = "";
                string recieved_msg = "";
                foreach (string deci in deciValuesSplit)
                {                    
                    int a = int.Parse(deci);
                    if (a < 32 || a >= 127)
                    {
                        
                    }
                    else
                    {
                        if (a == 32)
                        {
                            recieved_msg += " ";
                        }
                        else
                        {
                            StringOut = StringOut + a;
                            recieved_msg += ((char)a).ToString();
                        }
                    }                    
                }
                if (!reading_flag)
                {
                    string barcode = labelControl1.Text.Trim();
                    string[] arrBarcode = barcode.Split('*');
                    if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                    {
                        read_it_scode = arrBarcode[0].ToString();
                        read_lot = arrBarcode[1].ToString();
                        if (recieved_msg.Substring(2, 1).Equals("0") && !검사기_toggle)//진입
                        {
                            검사기_toggle = true;                            
                            Equip_in_out_insert(recieved_msg, read_it_scode, read_lot);
                            list_box_검사기.Items.Add("진입 : "+recieved_msg);
                            outputList_Scroll();
                        }
                        else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("0") && 검사기_toggle)//ok 완료
                        {
                            검사기_toggle = false;
                            recieved_msg_finish = recieved_msg;
                            read_send_timer.Stop();
                            list_box_검사기.Items.Add("OK : "+recieved_msg);
                            outputList_Scroll();
                            검사기_바코드_리딩();

                        }
                        else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("1") && 검사기_toggle)//ng 완료
                        {
                            검사기_toggle = false;
                            recieved_msg_finish = recieved_msg;
                            read_send_timer.Stop();
                            list_box_검사기.Items.Add("NG : " + recieved_msg);
                            outputList_Scroll();



                            Equip_in_out_insert(recieved_msg, read_it_scode, read_lot);
                            
                            
                            //list_box_검사기.Items.Add(recieved_msg);
                            //outputList_Scroll();
                            
                            
                            //if (winsock_검사기.GetState.ToString() != "Connected")
                            //{
                            //    winsock1Connect_검사기();
                            //}
                            //new Thread(new ThreadStart(write_send_검사기_완료신호)).Start(); 
                            //Thread.Sleep(1000);
                            //if (winsock_검사기.GetState.ToString() != "Connected")
                            //{
                            //    winsock1Connect_검사기();
                            //}
                            //new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start(); 
                        }
                    }
                    list_box_검사기.Items.Add(recieved_msg);
                    outputList_Scroll();
                    
                }
                else
                {
                    if (start_bool)
                    {
                        start_bool = false;
                    }
                    else
                    {
                        if (가성불량OK)
                        {
                            Equip_in_out_insert("F10S", read_it_scode, read_lot);
                            Equip_in_out_insert("F11S00", read_it_scode, read_lot, recieved_msg);
                            가성불량OK = false;

                        }
                        else
                        {
                            Equip_in_out_insert(recieved_msg_finish, read_it_scode, read_lot, recieved_msg);
                            list_box_검사기.Items.Add("바코드 : " + recieved_msg);
                            outputList_Scroll();

                        }
                        완료전송_check = true;
                    }
                    
                    //if (winsock_검사기.GetState.ToString() != "Connected")
                    //{
                    //    winsock1Connect_검사기();
                    //}
                    //new Thread(new ThreadStart(write_send_검사기_완료신호)).Start(); 
                    //Thread.Sleep(1000);
                    //if (winsock_검사기.GetState.ToString() != "Connected")
                    //{
                    //    winsock1Connect_검사기();
                    //}
                    //new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start();

                    
                }
            }

        }
        string recieved_msg_finish = "";
        string read_it_scode="", read_lot = "";
        bool reading_flag = false;
        public void read_send_검사기()//읽기 명령어
        {
            reading_flag = false;
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            byte[] cmd2 = new byte[21];
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0x7B; cmd2[16] = 0x07; cmd2[17] = 0x00; //선두 디바이스 6C 07
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x03; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte  0F

            winsock_검사기.Send(cmd2);
        }

        public void 검사기_바코드_리딩()//바코드읽기
        {
            reading_flag = true;
            start_bool = false;
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            byte[] cmd2 = new byte[21];
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0x6C; cmd2[16] = 0x07; cmd2[17] = 0x00; //선두 디바이스 
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x0F; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte  

            winsock_검사기.Send(cmd2);

        }

        
        #endregion


        private void read_send_timer_Tick(object sender, EventArgs e)//타이머로 읽을 명령어 송신 0.1초마다
        {  
            read_send_검사기();
        }
        void outputList_Scroll()//들어오는 데이터 listBox 자동 스크롤
        {
            

            int itemsPerPage_검사기 = (int)(list_box_검사기.Height / list_box_검사기.ItemHeight);
            list_box_검사기.TopIndex = list_box_검사기.Items.Count - itemsPerPage_검사기;

        }

        #region clear
        
        private void simpleButton6_Click(object sender, EventArgs e)//검사기 clear
        {
            list_box_검사기.Items.Clear();
        }
        
        #endregion

        #region 데이터 저장
        Dictionary<string, string> EQUIP = new Dictionary<string, string>();
        public void Equip_in_out_insert(string data, string child_it_scode,string child_lot_no)//설비에서 들어오는데이터 inout 저장
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT_OUT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_inout);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            string 장비번호 = data.Substring(0, 2);

            string 상태값 = data.Substring(2, 1); // 0:in , 1:out
            string 전송구분 = data.Substring(3, 1);// S:전송 , R:재전송
            string OK_NG = "X"; // 0:OK , 1:NG
            string 검사유무 = "X"; // 0:무 , 1:유
            int 블럭수 = 0;
            string ins_data = "";

            if (상태값.Equals("1"))//out 일때
            {
                OK_NG = data.Substring(4, 1);
                검사유무 = data.Substring(5, 1);
                if (검사유무.Equals("1"))
                {
                    블럭수 = int.Parse(data.Substring(6, 2));

                    ins_data = data.Substring(8, data.Length - 8);


                }

            }
            //IN 일때
            if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                return;
            }
            else if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                EQUIP[장비번호] = "RUN";
            }

            //OUT 일때
            if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                return;
            }
            else if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                EQUIP[장비번호] = "OFF";
            }

            DataTable ins_dt = new DataTable();
            ins_dt.Columns.Add("INS_DATA", typeof(string));
            for (int i = 0; i < 블럭수; i++)
            {
                int ins_data_length = int.Parse(ins_data.Substring(0, 2));
                DataRow dr = ins_dt.NewRow();
                dr["INS_DATA"] = ins_data.Substring(2, ins_data_length);
                ins_dt.Rows.Add(dr);
                ins_data = ins_data.Substring(2 + ins_data_length, ins_data.Length - (2 + ins_data_length));
            }
           
            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@IT_SCODE", "");
            cmd.Parameters.AddWithValue("@OP_CODE", 장비번호);
            cmd.Parameters.AddWithValue("@TOTAL_INS", OK_NG);
            cmd.Parameters.AddWithValue("@IN_OUT_FLAG", 상태값);
            cmd.Parameters.AddWithValue("@TVP", ins_dt);
            cmd.Parameters.AddWithValue("@RSRV_NO", "");
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);
            cmd.Parameters.AddWithValue("@READ_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@READ_LOT_NO", child_lot_no);


            try
            {

                cmd.ExecuteNonQuery();

                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void Equip_in_out_insert(string data, string child_it_scode, string child_lot_no,string assy_lot)//설비에서 들어오는데이터 inout 저장
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT_OUT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_inout);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            string 장비번호 = data.Substring(0, 2);

            string 상태값 = data.Substring(2, 1); // 0:in , 1:out
            string 전송구분 = data.Substring(3, 1);// S:전송 , R:재전송
            string OK_NG = "X"; // 0:OK , 1:NG
            string 검사유무 = "X"; // 0:무 , 1:유
            int 블럭수 = 0;
            string ins_data = "";

            if (상태값.Equals("1"))//out 일때
            {
                OK_NG = data.Substring(4, 1);
                검사유무 = data.Substring(5, 1);
                if (검사유무.Equals("1"))
                {
                    블럭수 = int.Parse(data.Substring(6, 2));

                    ins_data = data.Substring(8, data.Length - 8);


                }

            }
            //IN 일때
            if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                return;
            }
            else if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                EQUIP[장비번호] = "RUN";
            }

            //OUT 일때
            if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                return;
            }
            else if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                EQUIP[장비번호] = "OFF";
            }

            DataTable ins_dt = new DataTable();
            ins_dt.Columns.Add("INS_DATA", typeof(string));
            for (int i = 0; i < 블럭수; i++)
            {
                int ins_data_length = int.Parse(ins_data.Substring(0, 2));
                DataRow dr = ins_dt.NewRow();
                dr["INS_DATA"] = ins_data.Substring(2, ins_data_length);
                ins_dt.Rows.Add(dr);
                ins_data = ins_data.Substring(2 + ins_data_length, ins_data.Length - (2 + ins_data_length));
            }

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@IT_SCODE", "");
            cmd.Parameters.AddWithValue("@OP_CODE", 장비번호);
            cmd.Parameters.AddWithValue("@TOTAL_INS", OK_NG);
            cmd.Parameters.AddWithValue("@IN_OUT_FLAG", 상태값);
            cmd.Parameters.AddWithValue("@TVP", ins_dt);
            cmd.Parameters.AddWithValue("@RSRV_NO", auto_mo_snumb);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);
            cmd.Parameters.AddWithValue("@READ_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@READ_LOT_NO", child_lot_no);
            cmd.Parameters.AddWithValue("@ASSY_LOT_NO", assy_lot);
            cmd.Parameters.AddWithValue("@REWORK_FLAG", REWORK_FLAG);

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
            }
            finally
            {
                conn.Close();
                //formClear();                
            }            
        }

        private void get_reading_mo_snumb(string child_it_scode, string child_lot_no)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "USP_AUTO_GET_RSRV_ITEM";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("CHILD_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("CHILD_LOT_NO", child_lot_no);
            SqlDataReader reader;
            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    auto_mo_snumb = reader["RSRV_NO"].ToString().Trim();
                    auto_it_scode = reader["IT_SCODE"].ToString().Trim();
                    send_spec = reader["SPEC"].ToString().Trim();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)//리딩시 OK 일때 READING_DATA INSERT
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                labelControl2.Text = "";
                labelControl2.BackColor = Color.White;
                
                //if (string.IsNullOrWhiteSpace(auto_it_scode))
                //{
                //    PpCard_Success.TopLevel = true;
                //    PpCard_Success.TopMost = true;
                //    PpCard_Success.Visible = true;
                //    PpCard_Success.set_text("작업할 생산품이 없습니다.", 5);
                //    PpCard_Success.BackColor = Color.Salmon;
                //    textEdit1.Text = "";
                //    return;
                //}
                try
                {

                    string barcode = textEdit1.Text.Trim();
                    string[] arrBarcode = barcode.Split('*');
                    if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                    {
                        if (winsock_검사기.GetState.ToString() != "Connected")
                        {
                            winsock1Connect_검사기();
                        }
                        read_it_scode = arrBarcode[0].ToString();
                        read_lot = arrBarcode[1].ToString();
                        get_reading_mo_snumb(read_it_scode, read_lot);
                        
                        if (string.IsNullOrWhiteSpace(auto_mo_snumb))
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("투입되지 않은 제품입니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;
                            textEdit1.Text = "";
                            return;
                        }
                        string[] arr_lot = read_lot.Split('/');                        
                        if (arr_lot.Length.Equals(4))
                        {

                            //리딩된 사출 QR 코드로 레이져공정을 거쳐서 왔는지 체크 (해당 로뜨로 레이져 품목 가져오기)
                            //
                            string laser_it_scode = AUTO_GET_DATA.check_PAB(read_it_scode, read_lot);
                            string check_bom_it_scode = "";
                            //BOM 체크(이종체크)
                            if (!string.IsNullOrWhiteSpace(laser_it_scode))
                            {
                                check_bom_it_scode = laser_it_scode;
                            }
                            else
                            {
                                check_bom_it_scode = read_it_scode;
                            }
                            DataTable dt = AUTO_GET_DATA.bom_chk(auto_it_scode, "N");//완성품품목으로 bom 정전개
                            DataRow[] dr = dt.Select("IT_SCODE='" + check_bom_it_scode + "'");//리딩된 사출품번이 bom에 있는지 체크
                            if (dr.Length > 0)
                            {                                
                                REWORK_FLAG = "NONE";
                                check_save_read_data(auto_it_scode, read_it_scode, read_lot);
                                read_send_timer.Start();
                                timer_send_check.Start();
                            }
                            else
                            {
                                PpCard_Success.TopLevel = true;
                                PpCard_Success.TopMost = true;
                                PpCard_Success.Visible = true;
                                PpCard_Success.set_text("해당 제품이 아닙니다.", 5);
                                PpCard_Success.BackColor = Color.Red;
                            }
                            
                        }
                        
                        //사용되지 않았으면 체크하는 테이블에 저장 하면서 카운트
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                }

            }

        }
        //public string 
        private void check_save_read_data(string it_scode, string child_it_scode, string child_lot_no)//리딩시 체크
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_IN_READ_DATA_SAVE_OUT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_리딩시체크);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@RSRV_NO", auto_mo_snumb);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);
            cmd.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);
            

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        
        
        #endregion

        private void label47_Click(object sender, EventArgs e)
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
                
                
            }
            else
            {
                panel1.Visible = true;
                panel1.BringToFront();
                
            }
        }

        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            regKey.SetValue("WC_CODE", str_wc_code);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            winsock_검사기.Close();
            parent_form.Visible = true;
            this.Close();
        }
        DataTable reading_data = new DataTable();
        string auto_it_scode = "",auto_mo_snumb="";
        
        string send_spec = "";


        //public void auto_it_scode_get()//자동으로 다음품목 가져오기(리딩시 체크를 위해)
        //{
        //    /*자동으로 다음 품목 가져오기*/
        //    send_spec="";
        //    //리딩시 체크해야될 작지번호 가져오기
        //    foreach (DataRow w_dr in work_plan.Rows)
        //    {
        //        if (int.Parse(w_dr["PLAN_SQTY"].ToString()) > ((int.Parse(w_dr["GOOD_SQTY"].ToString()) + int.Parse(w_dr["FAIL_SQTY"].ToString()))))
        //        {
        //            next_it_scode = w_dr["IT_SCODE"].ToString().Trim();
        //            next_mo_snumb = w_dr["MO_SNUMB"].ToString().Trim();
        //            send_spec = w_dr["SPEC"].ToString().Trim();
        //            break;
        //        }

        //    }
        //    if (auto_it_scode != next_it_scode)
        //    {
        //        auto_it_scode = next_it_scode;
                
        //    }
        //    if (auto_mo_snumb != next_mo_snumb)
        //    {
        //        auto_mo_snumb = next_mo_snumb;
        //    }
            
            
        //}
        private void write_send_검사기(string spec)//쓰기 명령어 작업지시 변경시 한번쓰기 D50번지 
        {
            
            //
            byte[] bt_spec = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A
                                 ,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14
                                 ,0x15,0x16,0x17,0x18 };
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0xD0;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = bt_spec[int.Parse(spec)];
            cmd2[22] = 0x00;
            winsock_검사기.Send(cmd2);
        }
        private void write_send_검사기_완료신호1()//쓰기 명령어 작업지시 변경시 한번쓰기 D50번지 
        {

            //
            
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            Thread.Sleep(100);
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd_완료신호1 = new byte[23];
            cmd_완료신호1[0] = 0x50;
            cmd_완료신호1[1] = 0x00;

            cmd_완료신호1[2] = 0x00;

            cmd_완료신호1[3] = 0xFF;

            cmd_완료신호1[4] = 0xFF;
            cmd_완료신호1[5] = 0x03;

            cmd_완료신호1[6] = 0x00;

            cmd_완료신호1[7] = 0x0E;
            cmd_완료신호1[8] = 0x00;

            cmd_완료신호1[9] = 0x10;
            cmd_완료신호1[10] = 0x00;

            cmd_완료신호1[11] = 0x01;
            cmd_완료신호1[12] = 0x14;

            cmd_완료신호1[13] = 0x00;
            cmd_완료신호1[14] = 0x00;

            cmd_완료신호1[15] = 0xDF;
            cmd_완료신호1[16] = 0x07;
            cmd_완료신호1[17] = 0x00;

            cmd_완료신호1[18] = 0xA8;

            cmd_완료신호1[19] = 0x01;
            cmd_완료신호1[20] = 0x00;

            cmd_완료신호1[21] = 0x01; cmd_완료신호1[22] = 0x00;
            winsock_검사기.Send(cmd_완료신호1);
        }
        private void write_send_검사기_완료신호0()//쓰기 명령어 작업지시 변경시 한번쓰기 D50번지 
        {

            //

            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                winsock1Connect_검사기();
            }
            Thread.Sleep(100);
            if (winsock_검사기.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd완료신호0 = new byte[23];
            cmd완료신호0[0] = 0x50;
            cmd완료신호0[1] = 0x00;

            cmd완료신호0[2] = 0x00;

            cmd완료신호0[3] = 0xFF;

            cmd완료신호0[4] = 0xFF;
            cmd완료신호0[5] = 0x03;

            cmd완료신호0[6] = 0x00;

            cmd완료신호0[7] = 0x0E;
            cmd완료신호0[8] = 0x00;

            cmd완료신호0[9] = 0x10;
            cmd완료신호0[10] = 0x00;

            cmd완료신호0[11] = 0x01;
            cmd완료신호0[12] = 0x14;

            cmd완료신호0[13] = 0x00;
            cmd완료신호0[14] = 0x00;

            cmd완료신호0[15] = 0xDF;
            cmd완료신호0[16] = 0x07;
            cmd완료신호0[17] = 0x00;

            cmd완료신호0[18] = 0xA8;

            cmd완료신호0[19] = 0x01;
            cmd완료신호0[20] = 0x00;

            cmd완료신호0[21] = 0x00; cmd완료신호0[22] = 0x00;
            winsock_검사기.Send(cmd완료신호0);
        }
        public string str2hex(string strData)
        {
            string resultHex = string.Empty;
            byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

            foreach (byte byteStr in arr_byteStr)
                resultHex += string.Format("{0:X2}", byteStr);

            return resultHex;
        }
        

        private void conn_InfoMessage_리딩시체크(object sender, SqlInfoMessageEventArgs e)
        {
            DataRow drr = reading_data.NewRow();
            DataRow[] dr = SELECT_DT.Select("IT_SCODE='" + auto_it_scode+"'");
            if (dr.Length > 0)
            {
                lbl_it_scode.Text = dr[0]["IT_SCODE"].ToString();
                lbl_pab.Text = dr[0]["PAB"].ToString();
                lbl_rh_lh.Text = dr[0]["RH_LH"].ToString();
                lbl_ssb.Text = dr[0]["SSB"].ToString();
                lbl_plan_sqty.Text = dr[0]["PLAN_SQTY"].ToString();
                lbl_good_sqty.Text = dr[0]["GOOD_SQTY"].ToString();
                lbl_fail_sqty.Text = dr[0]["FAIL_SQTY"].ToString();
                lbl_cnt.Text = dr[0]["CNT"].ToString();
                lbl_it_pkqty.Text = dr[0]["IT_PKQTY"].ToString();
            }
            if (e.Message.Trim().Substring(0, 2).Equals("OK"))
            {
                
                if (reading_data.Rows.Count > 10)
                {
                    reading_data.Rows.Clear();
                }
                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "OK";
                drr["NG_DESCR"] = "";

                //데이터 보내주기 D5000번
                write_send_검사기(send_spec);
                
                reading_data.Rows.Add(drr);                
                
                labelControl1.Text = textEdit1.Text.Trim();
                
                textEdit1.Text = "";
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("제품투입 OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
                
            }
            else if (e.Message.Trim().Substring(0, 2).Equals("RE") || e.Message.Trim().Substring(0, 2).Equals("NG"))
            {
                if (e.Message.Trim().Substring(0, 2).Equals("RE"))
                    REWORK_FLAG = "REWORK";
                else if (e.Message.Trim().Substring(0, 2).Equals("NG"))
                    REWORK_FLAG = "NG";
                if (reading_data.Rows.Count > 10)
                {
                    reading_data.Rows.Clear();
                }
                string message_str = "";
                message_str = e.Message.Trim().Substring(e.Message.Trim().IndexOf("/") + 1, e.Message.Trim().Length - 3);

                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "OK";
                drr["NG_DESCR"] = message_str;

                
                AUTO_REWORK_POPUP AUTO_REWORK_POPUP = new AUTO_REWORK_POPUP();
                AUTO_REWORK_POPUP.str_title = message_str;
                if (AUTO_REWORK_POPUP.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //재작업 플래그

                    reading_data.Rows.Add(drr);     
                    write_send_검사기(send_spec);
                    labelControl1.Text = textEdit1.Text.Trim();

                    textEdit1.Text = "";
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("제품투입 OK", 5);
                    PpCard_Success.BackColor = Color.RoyalBlue;
                    

                }
                else
                {
                    textEdit1.Text = "";
                }

            }
            else if (e.Message.Trim().Substring(0, 2).Equals("OP"))
            {
                REWORK_FLAG = "NONE";
                string message_str = "";
                message_str = e.Message.Trim().Substring(e.Message.Trim().IndexOf("/") + 1, e.Message.Trim().Length - 3);
                if (reading_data.Rows.Count > 10)
                {
                    reading_data.Rows.Clear();
                }
                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "OK";
                drr["NG_DESCR"] = message_str;
                AUTO_FORCE_POPUP AUTO_FORCE_POPUP = new AUTO_FORCE_POPUP();
                AUTO_FORCE_POPUP.str_title = message_str;
                if (AUTO_FORCE_POPUP.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //재작업 플래그
                    reading_data.Rows.Add(drr);
                    write_send_검사기(send_spec);
                    labelControl1.Text = textEdit1.Text.Trim();

                    textEdit1.Text = "";
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("제품투입 OK", 5);
                    PpCard_Success.BackColor = Color.RoyalBlue;
                    
                }
                else
                {
                    textEdit1.Text = "";
                }
            }
            else
            {
                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "NG";
                drr["NG_DESCR"] = "";
                reading_data.Rows.Add(drr);
                
                textEdit1.Text = "";
                //auto_it_scode_get_LOBOT();
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
            }
        }
        bool 가성불량OK = false;
        private void conn_InfoMessage_inout(object sender, SqlInfoMessageEventArgs e)
        {
            
            if (e.Message.Trim().Equals("OK"))
            {

                new Thread(new ThreadStart(reflesh_search)).Start();

                if (winsock_검사기.GetState.ToString() != "Connected")
                {
                    winsock1Connect_검사기();
                }
                //new Thread(new ThreadStart(write_send_검사기_완료신호)).Start();
                //new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start(); 
                
                labelControl1.Text = "";
                //read_send_timer.Start();
                //PpCard_Success.TopLevel = true;
                //PpCard_Success.TopMost = true;
                //PpCard_Success.Visible = true;
                //PpCard_Success.set_text("검사 OK", 5);
                //PpCard_Success.BackColor = Color.RoyalBlue;
                labelControl2.Text = "OK";
                labelControl2.BackColor = Color.RoyalBlue;
            }
            else if (e.Message.Trim().Equals("NG"))
            {
                string barcode = textEdit1.Text.Trim();
                string[] arrBarcode = barcode.Split('*');
                if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                {
                    if (winsock_검사기.GetState.ToString() != "Connected")
                    {
                        winsock1Connect_검사기();
                    }
                    read_it_scode = arrBarcode[0].ToString();
                    read_lot = arrBarcode[1].ToString();
                    get_reading_mo_snumb(read_it_scode, read_lot);
                }
                
                new Thread(new ThreadStart(reflesh_search)).Start();
                
                if (winsock_검사기.GetState.ToString() != "Connected")
                {
                    winsock1Connect_검사기();
                }
                if (checkBox_가성불량.Checked)
                {
                    AUTO_OUT_NG_POPUP AUTO_OUT_NG_POPUP = new AUTO_OUT_NG_POPUP();
                    
                    if (AUTO_OUT_NG_POPUP.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        REWORK_FLAG = "NG";
                        //reading_flag = false;

                        가성불량OK = true;
                        검사기_바코드_리딩();

                        labelControl1.Text = "";
                        textEdit1.Text = "";
                        완료전송_check = true;
                    }
                    else
                    {
                        labelControl1.Text = "";
                        textEdit1.Text = "";
                        labelControl2.Text = "NG";
                        labelControl2.BackColor = Color.Salmon;
                        완료전송_check = true;
                    }
                }
                else
                {
                    labelControl1.Text = "";
                    textEdit1.Text = "";
                    labelControl2.Text = "NG";
                    labelControl2.BackColor = Color.Salmon;
                    완료전송_check = true;
                }
                
                
                
            }
            
        }
        
        
        private void message_timer_clear_Tick(object sender, EventArgs e)
        {

            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            message_timer_clear.Stop();
        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void btn_po_release_Click(object sender, EventArgs e)
        {
            work_plan_popup_paint();
        }
        public void work_plan_popup_paint()//작업확정
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                AUTO_Po_release_new_search_OUT AUTO_Po_release_new_search_OUT = new AUTO_Po_release_new_search_OUT();
                AUTO_Po_release_new_search_OUT.wc_group = "PFR";
                AUTO_Po_release_new_search_OUT.wc_code = str_wc_code;
                AUTO_Po_release_new_search_OUT.car_code = "JA";
                if (AUTO_Po_release_new_search_OUT.ShowDialog() == DialogResult.OK)
                {
                    //확정된 작업지시 가져오기
                    po_sdate = AUTO_Po_release_new_search_OUT.po_sdate;
                    day_night = AUTO_Po_release_new_search_OUT.day_night;
                    gridControl1.DataSource = GET_PLAN_ACCEPT(po_sdate, day_night);
                    
                }
                textEdit1.Focus();
                //자동으로 다음생산품 가져오기
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }    
            /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }
        public DataTable GET_PLAN_ACCEPT(string po_sdate,string day_night)//작업확정된거 불러오기
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_AUTO_GET_PLAN";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.Parameters.AddWithValue("@REG_DATE", po_sdate);
            da.SelectCommand.Parameters.AddWithValue("@DAY_NIGHT", day_night);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public DataTable GET_PLAN_ACCEPT_NEW()//작업확정된거 불러오기
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_AUTO_GET_PLAN_NEW";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.Parameters.AddWithValue("@CAR_CODE","JA");
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_GET_PLAN_NEW");
                dt = ds.Tables["AUTO_GET_PLAN_NEW"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //Thread.Sleep(1000);
            //gridControl1.DataSource = null;
            //gridControl1.RefreshDataSource();
            //gridControl1.DataSource = GET_PLAN_ACCEPT(po_sdate, day_night);
            //gridControl1.RefreshDataSource();
            
            new Thread(new ThreadStart(reflesh_search)).Start();


            textEdit1.Focus();


            //Thread t3 = new Thread(() => test(10, 20, 30));
            //t3.Start();
            
        }

        delegate void _d_set_gridControl(DataTable dt);        

        private void reflesh_search()
        {
            SELECT_DT = GET_PLAN_ACCEPT_NEW();
            //gridControl1.DataSource = SELECT_DT;
            //gridControl1.RefreshDataSource();

            this.Invoke(new _d_set_gridControl(set_gridControl), new object[] { SELECT_DT });
            
            //if (!string.IsNullOrWhiteSpace(auto_it_scode))
            //{
            //    DataRow[] dr = SELECT_DT.Select("IT_SCODE='" + auto_it_scode + "'");
            //    if (dr.Length > 0)
            //    {
            //        lbl_it_scode.Text = dr[0]["IT_SCODE"].ToString();
            //        lbl_pab.Text = dr[0]["PAB"].ToString();
            //        lbl_rh_lh.Text = dr[0]["RH_LH"].ToString();
            //        lbl_ssb.Text = dr[0]["SSB"].ToString();
            //        lbl_plan_sqty.Text = dr[0]["PLAN_SQTY"].ToString();
            //        lbl_good_sqty.Text = dr[0]["GOOD_SQTY"].ToString();
            //        lbl_fail_sqty.Text = dr[0]["FAIL_SQTY"].ToString();
            //        lbl_cnt.Text = dr[0]["CNT"].ToString();
            //        lbl_it_pkqty.Text = dr[0]["IT_PKQTY"].ToString();
            //    }
            //    //if (int.Parse(lbl_it_pkqty.Text.Trim()) <= int.Parse(lbl_cnt.Text.Trim()))
            //    //{
            //    //    if (save_work_input(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim())))
            //    //    {
            //    //        new Thread(new ThreadStart(reflesh_search)).Start();
            //    //        //모비스 식별표 출력
            //    //        Thread print_thread = new Thread(()
            //    //            => Func_Mobis_Print.Print_AUTO(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim()), "", 2));
            //    //        print_thread.Start();
                        
            //    //    }
            //    //}
                
            //    //timer_work_input.Start();
            //}
            textEdit1.Focus();
        }
        private void set_gridControl(DataTable dt)
        {
            gridControl1.DataSource = dt;
            if (!string.IsNullOrWhiteSpace(lbl_it_scode.Text.Trim()))
            {
                DataRow[] dr = SELECT_DT.Select("IT_SCODE='" + lbl_it_scode.Text.Trim() + "'");
                if (dr.Length > 0)
                {
                    lbl_it_scode.Text = dr[0]["IT_SCODE"].ToString();
                    lbl_pab.Text = dr[0]["PAB"].ToString();
                    lbl_rh_lh.Text = dr[0]["RH_LH"].ToString();
                    lbl_ssb.Text = dr[0]["SSB"].ToString();
                    lbl_plan_sqty.Text = dr[0]["PLAN_SQTY"].ToString();
                    lbl_good_sqty.Text = dr[0]["GOOD_SQTY"].ToString();
                    lbl_fail_sqty.Text = dr[0]["FAIL_SQTY"].ToString();
                    lbl_cnt.Text = dr[0]["CNT"].ToString();
                    lbl_it_pkqty.Text = dr[0]["IT_PKQTY"].ToString();
                }
                //if (int.Parse(lbl_it_pkqty.Text.Trim()) <= int.Parse(lbl_cnt.Text.Trim()))
                //{
                //    if (save_work_input(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim())))
                //    {
                //        new Thread(new ThreadStart(reflesh_search)).Start();
                //        //모비스 식별표 출력
                //        Thread print_thread = new Thread(()
                //            => Func_Mobis_Print.Print_AUTO(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim()), "", 2));
                //        print_thread.Start();

                //    }
                //}

                //timer_work_input.Start();
            }
        }
        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //if (int.Parse(gridView1.GetFocusedRowCellValue("CNT").ToString())==0)
            //{
            //    MessageBox.Show("카운트 수량이 '0' 이면\r\n실적을 등록 할 수 없습니다.");
            //    return;
            //}
            //if (int.Parse(gridView1.GetFocusedRowCellValue("IT_PKQTY").ToString()) > int.Parse(gridView1.GetFocusedRowCellValue("CNT").ToString()))
            //{
            //    if (DialogResult.OK == MessageBox.Show("카운트 수량이 용기수량보다 작습니다.\r\n카운트 수량 만큼 실적 등록 하시겠습니까?","알림",MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
            //    {
            //        if (save_work_input(gridView1.GetFocusedRowCellValue("IT_SCODE").ToString(), int.Parse(gridView1.GetFocusedRowCellValue("CNT").ToString())))
            //        {
            //            new Thread(new ThreadStart(reflesh_search)).Start();
            //            //모비스 식별표 출력
            //            //Thread print_thread = new Thread(()
            //            //    => Func_Mobis_Print.Print(gridView1.GetFocusedRowCellValue("IT_SCODE").ToString(), int.Parse(gridView1.GetFocusedRowCellValue("IT_PKQTY").ToString()), "", 2));
            //            //print_thread.Start();
            //            //textEdit1.Focus();
            //        }
            //    }
            //    else
            //    {
            //        return;
            //    }
            //}
            ////실적 등록
            //if (save_work_input(gridView1.GetFocusedRowCellValue("IT_SCODE").ToString(), int.Parse(gridView1.GetFocusedRowCellValue("IT_PKQTY").ToString())))
            //{
            //    new Thread(new ThreadStart(reflesh_search)).Start();
            //    //모비스 식별표 출력
            //    //Thread print_thread = new Thread(()
            //    //    => Func_Mobis_Print.Print(gridView1.GetFocusedRowCellValue("IT_SCODE").ToString(), int.Parse(gridView1.GetFocusedRowCellValue("IT_PKQTY").ToString()), "", 2));
            //    //print_thread.Start();
            //    //textEdit1.Focus();

            //}
            
        }
        DataTable DT = new DataTable();
        //실적등록
        public bool save_work_input(string it_scode,int it_pkqty)
        {
            
            bool result = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "USP_AUTO_GOOD_SQTY_SAVE";

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("WC_CODE", str_wc_code);
            cmd.Parameters.AddWithValue("IT_PKQTY", it_pkqty);

            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();                
                trans.Commit();
                result = true;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return result;
        }


        private void btn_inspection_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(str_wc_code.ToString().Trim()))
            {
                MessageBox.Show("작업장을 선택 해 주새요");
                return;
            }
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(parent_form);
            FME_InsResultReg.wc_code = str_wc_code.ToString().Trim();
            FME_InsResultReg.Show();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(write_send_검사기_완료신호1)).Start(); 
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start(); 
        }

        private void btn_program_finish_Click(object sender, EventArgs e)
        {
            Finish_Popup Finish_Popup = new Finish_Popup();
            if (Finish_Popup.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Finish_Popup.btn_state.Equals("0"))
                {

                }
                else if (Finish_Popup.btn_state.Equals("1"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, "", "", "6");
                    Application.Exit();
                }
                else if (Finish_Popup.btn_state.Equals("2"))
                {
                    SUB_SAVE.작업시작시간_업데이트(str_wc_code, "", "", "6");
                    System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");

                }
            }
        }

        private void timer_send_check_Tick(object sender, EventArgs e)
        {
            if (완료전송_check)
            {
                if (winsock_검사기.GetState.ToString() != "Connected")
                {
                    winsock1Connect_검사기();
                }
                new Thread(new ThreadStart(write_send_검사기_완료신호1)).Start();
                //Thread.Sleep(1000);
                if (winsock_검사기.GetState.ToString() != "Connected")
                {
                    winsock1Connect_검사기();
                }

                new Thread(new ThreadStart(write_send_검사기_완료신호0)).Start(); 

            }
            완료전송_check = false;
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Func_Kia_Print.Print("84610G6150REC", 24, "", 1);
        }

        private void simpleButton4_Click_1(object sender, EventArgs e)
        {
            if (panel2.Visible)
            {
                panel2.Visible = false;
            }
            else
            {
                panel2.Visible = true;
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(lbl_it_scode.Text.Trim()))
                {
                    MessageBox.Show("사출 QR 코드를 읽어주세요");
                    return;
                }
                if (int.Parse(lbl_cnt.Text.Trim()) == 0)
                {
                    MessageBox.Show("카운트 수량이 '0' 이면\r\n실적을 등록 할 수 없습니다.");
                    return;
                }
                if (int.Parse(lbl_it_pkqty.Text.Trim()) > int.Parse(lbl_cnt.Text.Trim()))
                {
                    if (DialogResult.OK == MessageBox.Show("카운트 수량이 용기수량보다 작습니다.\r\n카운트 수량 만큼 실적 등록 하시겠습니까?", "알림", MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
                    {
                        int work_good = int.Parse(lbl_cnt.Text.Trim());
                        if (save_work_input(lbl_it_scode.Text.Trim(), work_good))
                        {
                            new Thread(new ThreadStart(reflesh_search)).Start();
                            //모비스 식별표 출력
                            Thread print_thread = new Thread(()
                                => Func_Mobis_Print.Print_AUTO(lbl_it_scode.Text.Trim(), work_good, "", 2));
                            print_thread.Start();
                            textEdit1.Focus();
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                //실적 등록
                else if (save_work_input(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim())))
                {
                    new Thread(new ThreadStart(reflesh_search)).Start();
                    //모비스 식별표 출력
                    Thread print_thread = new Thread(()
                        => Func_Mobis_Print.Print_AUTO(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim()), "", 2));
                    print_thread.Start();
                    textEdit1.Focus();

                }
            }
            catch
            {

            }
            finally
            {
                textEdit1.Focus();
            }
            

            
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(lbl_it_scode.Text.Trim()))
            {
                MessageBox.Show("사출 QR 코드를 읽어주세요");
                return;
            }

            Thread print_thread = new Thread(()
                        => Func_Mobis_Print.Print(lbl_it_scode.Text.Trim(), int.Parse(lbl_it_pkqty.Text.Trim()), "", 2));
            print_thread.Start();
            textEdit1.Focus();
        }

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            try
            {
                AUTO_OUT_INS_FAIL_SEARCH AUTO_OUT_INS_FAIL_SEARCH = new AUTO_OUT_INS_FAIL_SEARCH();
                AUTO_OUT_INS_FAIL_SEARCH.car_code = "JA";
                AUTO_OUT_INS_FAIL_SEARCH.Show();
            }
            catch
            {

            }
            
        }


    }
}
