﻿using LibUsbDotNet;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    delegate void _d_set_connection_gridControl1_yb(DataTable dt);
    delegate void _d_set_connection_gridControl2_yb(DataTable dt);
    public partial class AUTO_YB_CONNECTION_SEARCH : Form
    {
        
        /// 여기까지 uio.dll을 사용하기 위한 선언부
        public string site_code = "";
        public string str_con = "";

        

        MAIN parent_form;
        public AUTO_YB_CONNECTION_SEARCH(MAIN form)
        {
            MessageFilter uioMf = new MessageFilter();
            this.parent_form = form;
            
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {  
            this.Close();
        }

        public DataTable get_connection_cnt_search()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_CONNECTION_CNT_SEARCH_PLAN_YB";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
                   
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        private void AUTO_JA_NOZZLE_SEARCH_Load(object sender, EventArgs e)
        {
            
            new Thread(new ThreadStart(reflesh_search)).Start();
            labelControl3.Text = (timer1.Interval / 1000).ToString();
            timer3.Start();
           
            timer1.Start();
        }
       
        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            timer3.Stop();
            reflesh_search();
            timer3.Start();
        }
        private void reflesh_search()
        {
            this.Invoke(new _d_set_connection_gridControl1_yb(set_gridcontrol1), new object[] { get_connection_cnt_search() });

            this.Invoke(new _d_set_connection_gridControl2_yb(set_gridcontrol2), new object[] { get_현재진행() });
            labelControl3.Text = (timer1.Interval / 1000).ToString();
        }
        public void set_gridcontrol1(DataTable dt)
        {
            gridControl1.DataSource = dt;
        }
        public void set_gridcontrol2(DataTable dt)
        {
            gridControl2.DataSource = dt;
        }
        private DataTable get_현재진행()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_CONNECTION_CNT_SEARCH_PLAN_INC_YB";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            //da.SelectCommand.Parameters.AddWithValue("REG_DATE", dateEdit1.DateTime.ToString("yyyyMMdd"));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(reflesh_search)).Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            labelControl3.Text = (int.Parse(labelControl3.Text) - 1).ToString();
            btn_Refresh.Text = "새로고침(" + labelControl3.Text + ")";
            if (labelControl3.Text.Trim().Equals("0"))
            {
                reflesh_search();
            }
        }

    }
    
}
