﻿namespace DK_Tablet
{
    partial class DASHBOARD_JA_NEW
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_A1_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_A1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_B1_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_B1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_C1_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_C1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_E1_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_E1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_C2 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_C2_2 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_F1_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_F1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_plan_sqty = new DevExpress.XtraEditors.LabelControl();
            this.lbl_good_sqty = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fail_sqty = new DevExpress.XtraEditors.LabelControl();
            this.button10 = new System.Windows.Forms.Button();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.button2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.button9 = new System.Windows.Forms.Button();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_now_date = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.list_box_진동 = new System.Windows.Forms.ListBox();
            this.timer_시계 = new System.Windows.Forms.Timer(this.components);
            this.winsock_진동 = new MelsecPLC.Winsock();
            this.계획실적_timer = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_진동 = new System.Windows.Forms.Timer(this.components);
            this.OK_timer_진동 = new System.Windows.Forms.Timer(this.components);
            this.list_box_초음파2 = new System.Windows.Forms.ListBox();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.OK_timer_초음파2 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_레이져컷팅 = new System.Windows.Forms.ListBox();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_초음파1 = new System.Windows.Forms.ListBox();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_클립조립 = new System.Windows.Forms.ListBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_OP = new System.Windows.Forms.ListBox();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.list_box_검사기 = new System.Windows.Forms.ListBox();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.winsock_OP = new MelsecPLC.Winsock();
            this.winsock_레이져컷팅 = new MelsecPLC.Winsock();
            this.Refresh_timer_레이져컷팅 = new System.Windows.Forms.Timer(this.components);
            this.OK_timer_레이져컷팅 = new System.Windows.Forms.Timer(this.components);
            this.winsock_클립조립 = new MelsecPLC.Winsock();
            this.Refresh_timer_클립조립 = new System.Windows.Forms.Timer(this.components);
            this.OK_timer_클립조립 = new System.Windows.Forms.Timer(this.components);
            this.OK_timer_초음파1 = new System.Windows.Forms.Timer(this.components);
            this.winsock_검사기 = new MelsecPLC.Winsock();
            this.Refresh_timer_검사기 = new System.Windows.Forms.Timer(this.components);
            this.OK_timer_검사기 = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_OP = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_초음파융착1 = new System.Windows.Forms.Timer(this.components);
            this.Refresh_timer_초음파융착2 = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.tableLayoutPanel1);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.lbl_now_date);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(434, 415, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1166, 636);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.38461F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.38461F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.38461F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.38461F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.tableLayoutPanel1.Controls.Add(this.panelControl1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelControl2, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelControl3, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelControl4, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit2, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit3, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit4, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit5, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.pictureEdit6, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelControl6, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelControl7, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelControl8, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.button10, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textEdit1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_close, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button9, 8, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 149);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.69274F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.84916F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.4581F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1142, 475);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl1.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.tableLayoutPanel2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(90, 3);
            this.panelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(169, 206);
            this.panelControl1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lbl_A1_2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lbl_A1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(163, 200);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lbl_A1_2
            // 
            this.lbl_A1_2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_A1_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_A1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_A1_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_A1_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_A1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_A1_2.Location = new System.Drawing.Point(3, 135);
            this.lbl_A1_2.Name = "lbl_A1_2";
            this.lbl_A1_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_A1_2.TabIndex = 7;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(3, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(157, 60);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "레이저컷팅";
            // 
            // lbl_A1
            // 
            this.lbl_A1.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_A1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_A1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_A1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_A1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_A1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_A1.Location = new System.Drawing.Point(3, 69);
            this.lbl_A1.Name = "lbl_A1";
            this.lbl_A1.Size = new System.Drawing.Size(157, 136);
            this.lbl_A1.TabIndex = 5;
            this.lbl_A1.Text = "대기중";
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl2.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl2.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Controls.Add(this.tableLayoutPanel3);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(352, 3);
            this.panelControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl2.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(169, 206);
            this.panelControl2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.lbl_B1_2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelControl5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbl_B1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(163, 200);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lbl_B1_2
            // 
            this.lbl_B1_2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_B1_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_B1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_B1_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_B1_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_B1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_B1_2.Location = new System.Drawing.Point(3, 135);
            this.lbl_B1_2.Name = "lbl_B1_2";
            this.lbl_B1_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_B1_2.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(3, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(157, 60);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "클립조립";
            // 
            // lbl_B1
            // 
            this.lbl_B1.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_B1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_B1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_B1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_B1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_B1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_B1.Location = new System.Drawing.Point(3, 69);
            this.lbl_B1.Name = "lbl_B1";
            this.lbl_B1.Size = new System.Drawing.Size(157, 136);
            this.lbl_B1.TabIndex = 5;
            this.lbl_B1.Text = "대기중";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl3.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl3.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.Controls.Add(this.tableLayoutPanel4);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(614, 3);
            this.panelControl3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(169, 206);
            this.panelControl3.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.lbl_C1_2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.labelControl7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lbl_C1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(163, 200);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // lbl_C1_2
            // 
            this.lbl_C1_2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_C1_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_C1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_C1_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_C1_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_C1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_C1_2.Location = new System.Drawing.Point(3, 135);
            this.lbl_C1_2.Name = "lbl_C1_2";
            this.lbl_C1_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_C1_2.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(3, 3);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(157, 60);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "초음파융착1";
            // 
            // lbl_C1
            // 
            this.lbl_C1.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_C1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_C1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_C1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_C1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_C1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_C1.Location = new System.Drawing.Point(3, 69);
            this.lbl_C1.Name = "lbl_C1";
            this.lbl_C1.Size = new System.Drawing.Size(157, 136);
            this.lbl_C1.TabIndex = 5;
            this.lbl_C1.Text = "대기중";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl4.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl4.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.tableLayoutPanel5);
            this.panelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl4.Location = new System.Drawing.Point(876, 3);
            this.panelControl4.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl4.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(169, 206);
            this.panelControl4.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.lbl_E1_2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.labelControl9, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lbl_E1, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(163, 200);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // lbl_E1_2
            // 
            this.lbl_E1_2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_E1_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_E1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_E1_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_E1_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_E1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_E1_2.Location = new System.Drawing.Point(3, 135);
            this.lbl_E1_2.Name = "lbl_E1_2";
            this.lbl_E1_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_E1_2.TabIndex = 7;
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(3, 3);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(157, 60);
            this.labelControl9.TabIndex = 5;
            this.labelControl9.Text = "진동융착1";
            // 
            // lbl_E1
            // 
            this.lbl_E1.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_E1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_E1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_E1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_E1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_E1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_E1.Location = new System.Drawing.Point(3, 69);
            this.lbl_E1.Name = "lbl_E1";
            this.lbl_E1.Size = new System.Drawing.Size(157, 136);
            this.lbl_E1.TabIndex = 5;
            this.lbl_E1.Text = "대기중";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow;
            this.pictureEdit1.Location = new System.Drawing.Point(265, 3);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(81, 206);
            this.pictureEdit1.TabIndex = 1;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit2.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow;
            this.pictureEdit2.Location = new System.Drawing.Point(527, 3);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(81, 206);
            this.pictureEdit2.TabIndex = 1;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit3.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow;
            this.pictureEdit3.Location = new System.Drawing.Point(789, 3);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit3.Size = new System.Drawing.Size(81, 206);
            this.pictureEdit3.TabIndex = 1;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit4.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow_bottom;
            this.pictureEdit4.Location = new System.Drawing.Point(876, 215);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit4.Size = new System.Drawing.Size(169, 55);
            this.pictureEdit4.TabIndex = 1;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit5.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow_left;
            this.pictureEdit5.Location = new System.Drawing.Point(789, 276);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit5.Size = new System.Drawing.Size(81, 196);
            this.pictureEdit5.TabIndex = 1;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureEdit6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit6.EditValue = global::DK_Tablet.Properties.Resources.Forward_Arrow_left;
            this.pictureEdit6.Location = new System.Drawing.Point(527, 276);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit6.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit6.Size = new System.Drawing.Size(81, 196);
            this.pictureEdit6.TabIndex = 1;
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl6.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl6.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl6.Appearance.Options.UseBackColor = true;
            this.panelControl6.Controls.Add(this.tableLayoutPanel7);
            this.panelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl6.Location = new System.Drawing.Point(876, 276);
            this.panelControl6.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl6.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(169, 196);
            this.panelControl6.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.labelControl13, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.lbl_C2, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.lbl_C2_2, 0, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(163, 190);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // labelControl13
            // 
            this.labelControl13.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl13.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl13.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl13.Location = new System.Drawing.Point(3, 3);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(157, 57);
            this.labelControl13.TabIndex = 5;
            this.labelControl13.Text = "초음파융착2";
            // 
            // lbl_C2
            // 
            this.lbl_C2.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_C2.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_C2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_C2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_C2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_C2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_C2.Location = new System.Drawing.Point(3, 66);
            this.lbl_C2.Name = "lbl_C2";
            this.lbl_C2.Size = new System.Drawing.Size(157, 136);
            this.lbl_C2.TabIndex = 5;
            this.lbl_C2.Text = "대기중";
            // 
            // lbl_C2_2
            // 
            this.lbl_C2_2.Appearance.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_C2_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_C2_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_C2_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_C2_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_C2_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_C2_2.Location = new System.Drawing.Point(3, 129);
            this.lbl_C2_2.Name = "lbl_C2_2";
            this.lbl_C2_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_C2_2.TabIndex = 6;
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl7.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl7.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.Controls.Add(this.tableLayoutPanel8);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl7.Location = new System.Drawing.Point(614, 276);
            this.panelControl7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl7.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(169, 196);
            this.panelControl7.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.lbl_F1_2, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.labelControl15, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lbl_F1, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(163, 190);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // lbl_F1_2
            // 
            this.lbl_F1_2.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_F1_2.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_F1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_F1_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_F1_2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_F1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_F1_2.Location = new System.Drawing.Point(3, 129);
            this.lbl_F1_2.Name = "lbl_F1_2";
            this.lbl_F1_2.Size = new System.Drawing.Size(157, 0);
            this.lbl_F1_2.TabIndex = 7;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl15.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl15.Location = new System.Drawing.Point(3, 3);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(157, 57);
            this.labelControl15.TabIndex = 5;
            this.labelControl15.Text = "검사";
            // 
            // lbl_F1
            // 
            this.lbl_F1.Appearance.Font = new System.Drawing.Font("Tahoma", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_F1.Appearance.ForeColor = System.Drawing.Color.DarkGray;
            this.lbl_F1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_F1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.lbl_F1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_F1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_F1.Location = new System.Drawing.Point(3, 66);
            this.lbl_F1.Name = "lbl_F1";
            this.lbl_F1.Size = new System.Drawing.Size(157, 136);
            this.lbl_F1.TabIndex = 5;
            this.lbl_F1.Text = "대기중";
            // 
            // panelControl8
            // 
            this.panelControl8.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelControl8.Appearance.BackColor2 = System.Drawing.Color.Black;
            this.panelControl8.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.panelControl8.Appearance.Options.UseBackColor = true;
            this.panelControl8.Controls.Add(this.tableLayoutPanel9);
            this.panelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl8.Location = new System.Drawing.Point(352, 276);
            this.panelControl8.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.panelControl8.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(169, 196);
            this.panelControl8.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.labelControl17, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.labelControl2, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.labelControl18, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.labelControl19, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.lbl_plan_sqty, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.lbl_good_sqty, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.lbl_fail_sqty, 1, 3);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 4;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(163, 190);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.White;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl17.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel9.SetColumnSpan(this.labelControl17, 2);
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(3, 3);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(157, 41);
            this.labelControl17.TabIndex = 5;
            this.labelControl17.Text = "금일실적";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Turquoise;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(3, 50);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 41);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "계획량";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl18.Location = new System.Drawing.Point(3, 97);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(75, 41);
            this.labelControl18.TabIndex = 6;
            this.labelControl18.Text = "양품";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl19.Location = new System.Drawing.Point(3, 144);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(75, 43);
            this.labelControl19.TabIndex = 6;
            this.labelControl19.Text = "불량";
            // 
            // lbl_plan_sqty
            // 
            this.lbl_plan_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.lbl_plan_sqty.Appearance.ForeColor = System.Drawing.Color.Turquoise;
            this.lbl_plan_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_plan_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_plan_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_plan_sqty.Location = new System.Drawing.Point(84, 50);
            this.lbl_plan_sqty.Name = "lbl_plan_sqty";
            this.lbl_plan_sqty.Size = new System.Drawing.Size(76, 41);
            this.lbl_plan_sqty.TabIndex = 6;
            this.lbl_plan_sqty.Text = "0";
            // 
            // lbl_good_sqty
            // 
            this.lbl_good_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.lbl_good_sqty.Appearance.ForeColor = System.Drawing.Color.Lime;
            this.lbl_good_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_good_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_good_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_good_sqty.Location = new System.Drawing.Point(84, 97);
            this.lbl_good_sqty.Name = "lbl_good_sqty";
            this.lbl_good_sqty.Size = new System.Drawing.Size(76, 41);
            this.lbl_good_sqty.TabIndex = 6;
            this.lbl_good_sqty.Text = "0";
            // 
            // lbl_fail_sqty
            // 
            this.lbl_fail_sqty.Appearance.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold);
            this.lbl_fail_sqty.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lbl_fail_sqty.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_fail_sqty.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_fail_sqty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_fail_sqty.Location = new System.Drawing.Point(84, 144);
            this.lbl_fail_sqty.Name = "lbl_fail_sqty";
            this.lbl_fail_sqty.Size = new System.Drawing.Size(76, 43);
            this.lbl_fail_sqty.TabIndex = 6;
            this.lbl_fail_sqty.Text = "0";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(3, 215);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(81, 55);
            this.button10.TabIndex = 11;
            this.button10.Text = "깝빡이";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            // 
            // textEdit1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textEdit1, 6);
            this.textEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit1.EditValue = "";
            this.textEdit1.Location = new System.Drawing.Point(90, 215);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Black;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 24.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.textEdit1.Properties.AutoHeight = false;
            this.textEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.textEdit1.Size = new System.Drawing.Size(780, 55);
            this.textEdit1.TabIndex = 12;
            this.textEdit1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textEdit1_KeyPress);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.Location = new System.Drawing.Point(1051, 403);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 69);
            this.button2.TabIndex = 3;
            this.button2.Text = "알람중지";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.button8, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.button7, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.button6, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.button5, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.button4, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.button3, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(1051, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 8;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(88, 196);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(3, 171);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(82, 22);
            this.button8.TabIndex = 9;
            this.button8.Text = "종료";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(3, 147);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 18);
            this.button7.TabIndex = 8;
            this.button7.Text = "녹색";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(3, 123);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(82, 18);
            this.button6.TabIndex = 7;
            this.button6.Text = "종료";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(3, 99);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(82, 18);
            this.button5.TabIndex = 6;
            this.button5.Text = "노랑";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(3, 75);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 18);
            this.button4.TabIndex = 5;
            this.button4.Text = "종료";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(3, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 18);
            this.button3.TabIndex = 4;
            this.button3.Text = "빨강";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 18);
            this.button1.TabIndex = 2;
            this.button1.Text = "경광등";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn_close.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.btn_close.Appearance.BackColor2 = System.Drawing.Color.White;
            this.btn_close.Appearance.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Location = new System.Drawing.Point(3, 403);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(81, 69);
            this.btn_close.TabIndex = 13;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // button9
            // 
            this.button9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button9.Location = new System.Drawing.Point(1056, 215);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 55);
            this.button9.TabIndex = 14;
            this.button9.Text = "비상등";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Visible = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 55F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.DarkOrange;
            this.labelControl1.Location = new System.Drawing.Point(332, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(502, 89);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "JA 자동화 라인";
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // lbl_now_date
            // 
            this.lbl_now_date.Appearance.Font = new System.Drawing.Font("Tahoma", 25F, System.Drawing.FontStyle.Bold);
            this.lbl_now_date.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_now_date.Location = new System.Drawing.Point(471, 105);
            this.lbl_now_date.Name = "lbl_now_date";
            this.lbl_now_date.Size = new System.Drawing.Size(223, 40);
            this.lbl_now_date.StyleController = this.layoutControl1;
            this.lbl_now_date.TabIndex = 5;
            this.lbl_now_date.Text = "labelControl2";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1166, 636);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lbl_now_date;
            this.layoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 93);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1146, 44);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tableLayoutPanel1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1146, 479);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl1;
            this.layoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1146, 93);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // list_box_진동
            // 
            this.list_box_진동.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_진동.FormattingEnabled = true;
            this.list_box_진동.ItemHeight = 14;
            this.list_box_진동.Location = new System.Drawing.Point(0, 0);
            this.list_box_진동.Name = "list_box_진동";
            this.list_box_진동.Size = new System.Drawing.Size(157, 255);
            this.list_box_진동.TabIndex = 4;
            // 
            // timer_시계
            // 
            this.timer_시계.Enabled = true;
            this.timer_시계.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // winsock_진동
            // 
            this.winsock_진동.LocalPort = 80;
            this.winsock_진동.RemoteIP = "127.0.0.1";
            this.winsock_진동.RemotePort = 80;
            // 
            // 계획실적_timer
            // 
            this.계획실적_timer.Interval = 5000;
            this.계획실적_timer.Tick += new System.EventHandler(this.계획실적_timer_Tick);
            // 
            // Refresh_timer_진동
            // 
            this.Refresh_timer_진동.Interval = 1000;
            this.Refresh_timer_진동.Tick += new System.EventHandler(this.Refresh_timer_진동_Tick);
            // 
            // OK_timer_진동
            // 
            this.OK_timer_진동.Interval = 5000;
            this.OK_timer_진동.Tick += new System.EventHandler(this.OK_timer_진동_Tick);
            // 
            // list_box_초음파2
            // 
            this.list_box_초음파2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_초음파2.FormattingEnabled = true;
            this.list_box_초음파2.ItemHeight = 14;
            this.list_box_초음파2.Location = new System.Drawing.Point(0, 0);
            this.list_box_초음파2.Name = "list_box_초음파2";
            this.list_box_초음파2.Size = new System.Drawing.Size(157, 255);
            this.list_box_초음파2.TabIndex = 4;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(492, 3);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.list_box_진동);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl2.SplitterPosition = 34;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton1.Location = new System.Drawing.Point(0, 0);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(157, 34);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "clear";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(655, 3);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.list_box_초음파2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.simpleButton2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl1.SplitterPosition = 34;
            this.splitContainerControl1.TabIndex = 6;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton2.Location = new System.Drawing.Point(0, 0);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(157, 34);
            this.simpleButton2.TabIndex = 0;
            this.simpleButton2.Text = "clear";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // OK_timer_초음파2
            // 
            this.OK_timer_초음파2.Interval = 5000;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainerControl4);
            this.panel1.Controls.Add(this.splitContainerControl5);
            this.panel1.Controls.Add(this.splitContainerControl3);
            this.panel1.Controls.Add(this.splitContainerControl2);
            this.panel1.Controls.Add(this.splitContainerControl7);
            this.panel1.Controls.Add(this.splitContainerControl6);
            this.panel1.Controls.Add(this.splitContainerControl1);
            this.panel1.Location = new System.Drawing.Point(12, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1154, 307);
            this.panel1.TabIndex = 7;
            this.panel1.Visible = false;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(3, 3);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.list_box_레이져컷팅);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.simpleButton4);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.ShowCaption = true;
            this.splitContainerControl4.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl4.SplitterPosition = 34;
            this.splitContainerControl4.TabIndex = 5;
            this.splitContainerControl4.Text = "splitContainerControl2";
            // 
            // list_box_레이져컷팅
            // 
            this.list_box_레이져컷팅.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_레이져컷팅.FormattingEnabled = true;
            this.list_box_레이져컷팅.ItemHeight = 14;
            this.list_box_레이져컷팅.Location = new System.Drawing.Point(0, 0);
            this.list_box_레이져컷팅.Name = "list_box_레이져컷팅";
            this.list_box_레이져컷팅.Size = new System.Drawing.Size(157, 255);
            this.list_box_레이져컷팅.TabIndex = 4;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton4.Location = new System.Drawing.Point(0, 0);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(157, 34);
            this.simpleButton4.TabIndex = 0;
            this.simpleButton4.Text = "clear";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(329, 3);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.list_box_초음파1);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.simpleButton5);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl5.SplitterPosition = 34;
            this.splitContainerControl5.TabIndex = 6;
            this.splitContainerControl5.Text = "splitContainerControl1";
            // 
            // list_box_초음파1
            // 
            this.list_box_초음파1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_초음파1.FormattingEnabled = true;
            this.list_box_초음파1.ItemHeight = 14;
            this.list_box_초음파1.Location = new System.Drawing.Point(0, 0);
            this.list_box_초음파1.Name = "list_box_초음파1";
            this.list_box_초음파1.Size = new System.Drawing.Size(157, 255);
            this.list_box_초음파1.TabIndex = 4;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton5.Location = new System.Drawing.Point(0, 0);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(157, 34);
            this.simpleButton5.TabIndex = 0;
            this.simpleButton5.Text = "clear";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(166, 3);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.list_box_클립조립);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.simpleButton3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl3.SplitterPosition = 34;
            this.splitContainerControl3.TabIndex = 6;
            this.splitContainerControl3.Text = "splitContainerControl1";
            // 
            // list_box_클립조립
            // 
            this.list_box_클립조립.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_클립조립.FormattingEnabled = true;
            this.list_box_클립조립.ItemHeight = 14;
            this.list_box_클립조립.Location = new System.Drawing.Point(0, 0);
            this.list_box_클립조립.Name = "list_box_클립조립";
            this.list_box_클립조립.Size = new System.Drawing.Size(157, 255);
            this.list_box_클립조립.TabIndex = 4;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton3.Location = new System.Drawing.Point(0, 0);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(157, 34);
            this.simpleButton3.TabIndex = 0;
            this.simpleButton3.Text = "clear";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl7.Horizontal = false;
            this.splitContainerControl7.Location = new System.Drawing.Point(981, 3);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.Controls.Add(this.list_box_OP);
            this.splitContainerControl7.Panel1.Text = "Panel1";
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton9);
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton8);
            this.splitContainerControl7.Panel2.Controls.Add(this.simpleButton7);
            this.splitContainerControl7.Panel2.Text = "Panel2";
            this.splitContainerControl7.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl7.SplitterPosition = 34;
            this.splitContainerControl7.TabIndex = 6;
            this.splitContainerControl7.Text = "splitContainerControl1";
            // 
            // list_box_OP
            // 
            this.list_box_OP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_OP.FormattingEnabled = true;
            this.list_box_OP.ItemHeight = 14;
            this.list_box_OP.Location = new System.Drawing.Point(0, 0);
            this.list_box_OP.Name = "list_box_OP";
            this.list_box_OP.Size = new System.Drawing.Size(157, 255);
            this.list_box_OP.TabIndex = 4;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(54, 0);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(49, 34);
            this.simpleButton9.TabIndex = 0;
            this.simpleButton9.Text = "write";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(3, 0);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(49, 34);
            this.simpleButton8.TabIndex = 0;
            this.simpleButton8.Text = "read";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(105, 0);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(49, 34);
            this.simpleButton7.TabIndex = 0;
            this.simpleButton7.Text = "clear";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(818, 3);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.list_box_검사기);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton6);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(157, 294);
            this.splitContainerControl6.SplitterPosition = 34;
            this.splitContainerControl6.TabIndex = 6;
            this.splitContainerControl6.Text = "splitContainerControl1";
            // 
            // list_box_검사기
            // 
            this.list_box_검사기.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_box_검사기.FormattingEnabled = true;
            this.list_box_검사기.ItemHeight = 14;
            this.list_box_검사기.Location = new System.Drawing.Point(0, 0);
            this.list_box_검사기.Name = "list_box_검사기";
            this.list_box_검사기.Size = new System.Drawing.Size(157, 255);
            this.list_box_검사기.TabIndex = 4;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleButton6.Location = new System.Drawing.Point(0, 0);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(157, 34);
            this.simpleButton6.TabIndex = 0;
            this.simpleButton6.Text = "clear";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // winsock_OP
            // 
            this.winsock_OP.LocalPort = 80;
            this.winsock_OP.RemoteIP = "127.0.0.1";
            this.winsock_OP.RemotePort = 80;
            // 
            // winsock_레이져컷팅
            // 
            this.winsock_레이져컷팅.LocalPort = 80;
            this.winsock_레이져컷팅.RemoteIP = "127.0.0.1";
            this.winsock_레이져컷팅.RemotePort = 80;
            // 
            // Refresh_timer_레이져컷팅
            // 
            this.Refresh_timer_레이져컷팅.Interval = 1000;
            this.Refresh_timer_레이져컷팅.Tick += new System.EventHandler(this.Refresh_timer_레이져컷팅_Tick);
            // 
            // OK_timer_레이져컷팅
            // 
            this.OK_timer_레이져컷팅.Interval = 5000;
            this.OK_timer_레이져컷팅.Tick += new System.EventHandler(this.OK_timer_레이져컷팅_Tick);
            // 
            // winsock_클립조립
            // 
            this.winsock_클립조립.LocalPort = 80;
            this.winsock_클립조립.RemoteIP = "127.0.0.1";
            this.winsock_클립조립.RemotePort = 80;
            // 
            // Refresh_timer_클립조립
            // 
            this.Refresh_timer_클립조립.Interval = 1000;
            this.Refresh_timer_클립조립.Tick += new System.EventHandler(this.Refresh_timer_클립조립_Tick);
            // 
            // OK_timer_클립조립
            // 
            this.OK_timer_클립조립.Interval = 5000;
            this.OK_timer_클립조립.Tick += new System.EventHandler(this.OK_timer_클립조립_Tick);
            // 
            // OK_timer_초음파1
            // 
            this.OK_timer_초음파1.Interval = 5000;
            this.OK_timer_초음파1.Tick += new System.EventHandler(this.OK_timer_초음파1_Tick);
            // 
            // winsock_검사기
            // 
            this.winsock_검사기.LocalPort = 80;
            this.winsock_검사기.RemoteIP = "127.0.0.1";
            this.winsock_검사기.RemotePort = 80;
            // 
            // Refresh_timer_검사기
            // 
            this.Refresh_timer_검사기.Interval = 1000;
            this.Refresh_timer_검사기.Tick += new System.EventHandler(this.Refresh_timer_검사기_Tick);
            // 
            // OK_timer_검사기
            // 
            this.OK_timer_검사기.Interval = 5000;
            this.OK_timer_검사기.Tick += new System.EventHandler(this.OK_timer_검사기_Tick);
            // 
            // Refresh_timer_초음파융착1
            // 
            this.Refresh_timer_초음파융착1.Interval = 1000;
            this.Refresh_timer_초음파융착1.Tick += new System.EventHandler(this.Refresh_timer_초음파융착1_Tick);
            // 
            // Refresh_timer_초음파융착2
            // 
            this.Refresh_timer_초음파융착2.Interval = 1000;
            this.Refresh_timer_초음파융착2.Tick += new System.EventHandler(this.Refresh_timer_초음파융착2_Tick);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            // 
            // DASHBOARD_JA_NEW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1166, 636);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DASHBOARD_JA_NEW";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lbl_now_date;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lbl_A1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbl_B1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl lbl_C1;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl lbl_E1;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl lbl_C2;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl lbl_F1;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private System.Windows.Forms.Timer timer_시계;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl lbl_plan_sqty;
        private DevExpress.XtraEditors.LabelControl lbl_good_sqty;
        private DevExpress.XtraEditors.LabelControl lbl_fail_sqty;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private MelsecPLC.Winsock winsock_진동;
        private System.Windows.Forms.Timer 계획실적_timer;
        private System.Windows.Forms.Timer Refresh_timer_진동;
        private System.Windows.Forms.Timer OK_timer_진동;
        private System.Windows.Forms.ListBox list_box_진동;
        private System.Windows.Forms.ListBox list_box_초음파2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private System.Windows.Forms.Timer OK_timer_초음파2;
        private System.Windows.Forms.Panel panel1;
        private MelsecPLC.Winsock winsock_OP;
        private MelsecPLC.Winsock winsock_레이져컷팅;
        private System.Windows.Forms.Timer Refresh_timer_레이져컷팅;
        private System.Windows.Forms.Timer OK_timer_레이져컷팅;
        private MelsecPLC.Winsock winsock_클립조립;
        private System.Windows.Forms.Timer Refresh_timer_클립조립;
        private System.Windows.Forms.Timer OK_timer_클립조립;
        private System.Windows.Forms.Timer OK_timer_초음파1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private System.Windows.Forms.ListBox list_box_레이져컷팅;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private System.Windows.Forms.ListBox list_box_초음파1;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private System.Windows.Forms.ListBox list_box_클립조립;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private System.Windows.Forms.ListBox list_box_검사기;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private MelsecPLC.Winsock winsock_검사기;
        private System.Windows.Forms.Timer Refresh_timer_검사기;
        private System.Windows.Forms.Timer OK_timer_검사기;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private System.Windows.Forms.ListBox list_box_OP;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.Timer Refresh_timer_OP;
        private System.Windows.Forms.Timer Refresh_timer_초음파융착1;
        private System.Windows.Forms.Timer Refresh_timer_초음파융착2;
        private DevExpress.XtraEditors.LabelControl lbl_A1_2;
        private DevExpress.XtraEditors.LabelControl lbl_B1_2;
        private DevExpress.XtraEditors.LabelControl lbl_C1_2;
        private DevExpress.XtraEditors.LabelControl lbl_E1_2;
        private DevExpress.XtraEditors.LabelControl lbl_C2_2;
        private DevExpress.XtraEditors.LabelControl lbl_F1_2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button10;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Timer timer1;
    }
}

