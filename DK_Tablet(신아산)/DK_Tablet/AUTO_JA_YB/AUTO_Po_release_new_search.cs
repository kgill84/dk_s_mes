﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DK_Tablet.Popup;
using DK_Tablet.AUTO_JA_YB;

namespace DK_Tablet
{
    public partial class AUTO_Po_release_new_search : DevExpress.XtraEditors.XtraForm
    {
        

        public string wc_group { get; set; }
        public string wc_code { get; set; }
        public string car_code { get; set; }
        DataTable po_release_DT = new DataTable();
        DataTable it_model_DT = new DataTable();
        public string po_sdate = "";
        public string day_night = "";
        public AUTO_Po_release_new_search()
        {
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        int days = 0;
        private BackgroundWorker worker=new BackgroundWorker();
        private void po_realese_search_Load(object sender, EventArgs e)
        {
            po_release_DT.Columns.Add("NUM", typeof(string));
            po_release_DT.Columns.Add("MO_SNUMB", typeof(string));
            po_release_DT.Columns.Add("SDATE", typeof(string));
            po_release_DT.Columns.Add("IT_SCODE", typeof(string));
            po_release_DT.Columns.Add("IT_SNAME", typeof(string));
            po_release_DT.Columns.Add("IT_MODEL", typeof(string));
            po_release_DT.Columns.Add("MO_SQUTY", typeof(string));
            po_release_DT.Columns.Add("IT_SAFTY", typeof(string));
            po_release_DT.Columns.Add("ALC_CODE", typeof(string));
            po_release_DT.Columns.Add("ME_SCODE", typeof(string));
            po_release_DT.Columns.Add("IT_PKQTY", typeof(string));
            po_release_DT.Columns.Add("D_WEIGHT", typeof(string));
            po_release_DT.Columns.Add("MATCH_GUBN", typeof(string));
            po_release_DT.Columns.Add("A_WEIGHT", typeof(string));
            po_release_DT.Columns.Add("CARRIER_YN", typeof(string));
            po_release_DT.Columns.Add("MAX_SQTY", typeof(string));
            po_release_DT.Columns.Add("REMAIN_SQUTY", typeof(string));
            po_release_DT.Columns.Add("WC_SCODE", typeof(string));

            it_model_DT.Columns.Add("IT_MODEL", typeof(string));
            DateTime dt = DateTime.Now;
            if(dt.Hour>=0 && dt.Hour<8)
            {
                days = days - 1;
            }
            date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
            lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");

            
            //worker.WorkerReportsProgress = true;
            //worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            
            
            //getdata();
            worker.RunWorkerAsync();
        }
        
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                progressBarControl1.EditValue = 0;
                
                po_release_DT = getdata();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // 에러가 있는지 체크
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error");
                return;
            }
            try
            {
                gridControl1.DataSource = po_release_DT;
                
                if (po_release_DT.Rows.Count > 0)
                {
                    string sdate = gridView1.GetRowCellValue(0, "SDATE").ToString();
                    lbl_sdate.Text = sdate.Substring(0, 4) + "-" + sdate.Substring(4, 2) + "-" + sdate.Substring(6, 2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요 " + ex.Message);
            }
        }      
        string date_reg = "";
        private void btn_date_Click(object sender, EventArgs e)
        {
            try
            {
                po_release_DT.Rows.Clear();
                

                if (sender == btn_prev_date)
                {
                    days = days - 1;
                }
                else if (sender == btn_next_date)
                {
                    days = days + 1;
                }
                date_reg = DateTime.Now.AddDays(days).ToString("yyyyMMdd");
                lbl_sdate.Text = DateTime.Now.AddDays(days).ToString("yyyy-MM-dd");
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
            
        }
        
        private DataTable getdata()
        {
            
            DataTable dt = new DataTable();


            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_JA_YB_PO_RELEASE", conn);
            try
            {
                conn.StateChange += new StateChangeEventHandler(_sqlConnection_StateChange);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                //da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
                da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
                da.SelectCommand.Parameters.AddWithValue("@IT_MODEL", car_code);
                da.SelectCommand.Parameters.AddWithValue("@SDATE", date_reg);
                da.SelectCommand.Parameters.AddWithValue("DAY_NIGHT", btn_day_night.Text.Trim());
                //jason

                DataSet ds = new DataSet();
                da.Fill(ds, "PO_RELEASE_NEW");

                dt = ds.Tables["PO_RELEASE_NEW"];

                //gridControl1.DataSource = dt;
                //gridControl1.Refresh();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
            return dt;
        }
        void _sqlConnection_StateChange(object sender, StateChangeEventArgs e)
        {
            try
            {
                if (e.CurrentState.ToString().Equals("Open"))
                    progressBarControl1.EditValue = 50;
                else
                {
                    progressBarControl1.EditValue = 100;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /*
        private void ColorChange()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                //MessageBox.Show(row.Cells["합계"].Value.ToString());
                if (2 == int.Parse(row.Cells["GUBN"].Value.ToString()))
                {
                    row.DefaultCellStyle.BackColor = Color.Red;

                }
            }
        }
        */
        private void btn_done_Click(object sender, EventArgs e)
        {   
            try
            {
                DataTable dt = po_release_DT;
                foreach (DataRow dr in dt.Rows)
                {
                    if (string.IsNullOrWhiteSpace(dr["NUM"].ToString()))
                    {
                        MessageBox.Show("작업순서가 입력 되지 않은 항목이 있습니다.\r\n 작업순서를 확인해주세요");
                        return;
                    }
                    DataRow[] drr = dt.Select("NUM = '"+dr["NUM"].ToString()+"' AND MO_SQUTY<>0");
                    if (drr.Length > 1)
                    {
                        MessageBox.Show("중복된 순번이 있습니다. 작업순서를 다시 입력해주세요");
                        return;
                    }
                    
                }
                if (MessageBox.Show("수량을 확정 하시겠습니까?", "", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {   
                    DataTable SAVE_DT = new DataTable();
                    SAVE_DT.Columns.Add("REG_DATE",typeof(string));
                    SAVE_DT.Columns.Add("ORDER_NUM",typeof(int));
                    SAVE_DT.Columns.Add("IT_SCODE", typeof(string));
                    SAVE_DT.Columns.Add("IT_SNAME", typeof(string));
                    SAVE_DT.Columns.Add("PLAN_SQTY", typeof(int));
                    SAVE_DT.Columns.Add("MO_SNUMB", typeof(string));
                    SAVE_DT.Columns.Add("DAY_NIGHT", typeof(string));

                    foreach (DataRow dr in po_release_DT.Rows)
                    {
                        //if (int.Parse(dr["MO_SQUTY"].ToString()) > 0)
                        //{
                            DataRow drr = SAVE_DT.NewRow();
                            drr["REG_DATE"] = dr["SDATE"].ToString();
                            drr["ORDER_NUM"] = int.Parse(dr["NUM"].ToString());
                            drr["IT_SCODE"] = dr["IT_SCODE"].ToString();
                            drr["IT_SNAME"] = dr["IT_SNAME"].ToString();
                            drr["PLAN_SQTY"] = int.Parse(dr["MO_SQUTY"].ToString());
                        drr["MO_SNUMB"] = dr["MO_SNUMB"].ToString();
                        drr["DAY_NIGHT"] = btn_day_night.Text.Trim();
                        
                            SAVE_DT.Rows.Add(drr);
                        //}
                    }
                    work_input_save_virtual_IM(SAVE_DT);
                    
                    MessageBox.Show("저장");
                    po_sdate = date_reg;
                    day_night = btn_day_night.Text.Trim();

                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("취소");
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("ERROR : "+ex.Message);
            }
        }
        public void work_input_save_virtual_IM(DataTable DT)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;            
            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            
            //커맨드
            SqlCommand cmd =
                    new SqlCommand("USP_AUTO_PLAN_SAVE", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            
            cmd.Parameters.AddWithValue("@TVP", DT);
            
            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                
                trans.Commit();
            }

            catch (Exception e)
            {
                trans.Rollback();
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }
        private void btn_day_night_Click(object sender, EventArgs e)
        {
            try
            {
                if (btn_day_night.Text.Equals("주간"))
                {
                    btn_day_night.Text = "야간";
                }
                else
                {
                    btn_day_night.Text = "주간";
                }
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해 주세요 " + ex.Message);
            }
        }

        private void repositoryItemTextEdit1_Click(object sender, EventArgs e)
        {
            AUTO_KeyPad KeyPad = new AUTO_KeyPad();
            KeyPad.txt_value = gridView1.GetFocusedRowCellValue(NUM).ToString();
            if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gridView1.SetFocusedRowCellValue(NUM, KeyPad.txt_value);
            }
        }

        private void repositoryItemTextEdit2_Click(object sender, EventArgs e)
        {
            AUTO_KeyPad KeyPad = new AUTO_KeyPad();
            KeyPad.txt_value = gridView1.GetFocusedRowCellValue(MO_SQUTY).ToString();
            if (KeyPad.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                gridView1.SetFocusedRowCellValue(MO_SQUTY, KeyPad.txt_value);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AUTO_PO_INSERT AUTO_PO_INSERT = new AUTO_PO_INSERT();
            AUTO_PO_INSERT.wc_code = wc_code;
            AUTO_PO_INSERT.date_re = date_reg;
            AUTO_PO_INSERT.IT_MODEL_str = car_code;
            if (AUTO_PO_INSERT.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                worker.RunWorkerAsync();
            }
            else
            {

            }
        }

    }
}
