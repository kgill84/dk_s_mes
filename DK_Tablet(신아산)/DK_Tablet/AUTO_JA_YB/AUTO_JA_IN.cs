﻿using DK_Tablet.FUNCTION;
using DK_Tablet.Popup;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class AUTO_JA_IN : Form
    { 
        bool 레이져컷팅_toggle = false;
        bool 레이져컷팅_toggle_B = false;
        bool 클립조립_toggle = false;
        bool 초음파1_toggle = false;
        bool 진동융착_toggle = false;
        bool 초음파2_toggle = false;
        
        string ip_레이져컷팅 = "172.168.100.10";
        string ip_클립조립 = "172.168.100.20";
        string ip_초음파융착1 = "172.168.100.30";
        string ip_진동융착 = "172.168.100.40";
        string ip_초음파융착2 = "172.168.100.31";
        
        string ip_Op_Pannel = "172.168.100.60";
        Socket clientsock_초음파1; //LS 초음파1
        Socket clientsock_초음파2; //LS 초음파2
        // A buffer of transmitted
        byte[] MsgSendBuff_초음파1 = new byte[39];//LS PLC 보낼 데이터 변수
        byte[] MsgSendBuff_초음파2 = new byte[39];//LS PLC 보낼 데이터 변수
        // A buffer of received  
        byte[] MsgRecvBuff_초음파1 = new byte[512];//LS PLC 받는 데이터 변수
        byte[] MsgRecvBuff_초음파2 = new byte[512];//LS PLC 받는 데이터 변수

        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);//레지스트리 키 
        
        public string str_wc_code = "";//작업장
        //public string mo_snumb = "", r_start = "";//작업계획번호와 작업시작시간
        AUTO_PpCard_Success PpCard_Success = new AUTO_PpCard_Success();
        private AUTO_Success_Form Success_Form = new AUTO_Success_Form();
        DataTable work_plan = new DataTable();
        string final_ok_ng = "";
        
        string final_nozzle_cnt = "";
        string final_connection_cnt = "";
        string final_connection_spec = "";

        string po_sdate = "";
        string day_night = "";

        MAIN parent_form;
        public AUTO_JA_IN(MAIN form)//생성자
        {
            parent_form = form;
            CheckForIllegalCrossThreadCalls = false;
            parent_form.Visible = false;

            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)//LOAD 이벤트
        {
            work_plan.Columns.Add("ORDER_NUM");
            work_plan.Columns.Add("IT_SCODE");
            work_plan.Columns.Add("IT_SNAME");
            work_plan.Columns.Add("PLAN_SQTY");
            work_plan.Columns.Add("INTO_SQTY");
            work_plan.Columns.Add("GOOD_SQTY");
            work_plan.Columns.Add("FAIL_SQTY");
            work_plan.Columns.Add("NOZZLE_CNT");

            reading_data.Columns.Add("READING_DATA");
            reading_data.Columns.Add("S_DATE");
            reading_data.Columns.Add("RESULT");

            gridControl2.DataSource = reading_data;

            EQUIP.Add("A1_A", "OFF");
            EQUIP.Add("A1_B", "OFF");
            EQUIP.Add("B1", "OFF");
            EQUIP.Add("C1", "OFF");
            EQUIP.Add("E1", "OFF");
            //EQUIP.Add("E2", "OFF");
            EQUIP.Add("C2", "OFF");
            EQUIP.Add("F1", "OFF");

            dateEdit1.DateTime = DateTime.Now;
            Success_Form.Show();
            Success_Form.Visible = false;
            lueWc_code.Properties.DataSource = AUTO_GET_DATA.WccodeSelect_DropDown("PFR");
            lueWc_code.Properties.DisplayMember = "WC_NAME";
            lueWc_code.Properties.ValueMember = "WC_CODE";
            //lookUpEdit1.SelectionStart = 0;
            //lueWc_code.ItemIndex = 0;
            if (regKey.GetValue("WC_CODE") == null)
            {
                regKey.SetValue("WC_CODE", "");
            }
            else
            {
                if (regKey.GetValue("WC_CODE").ToString() != "")
                {

                    lueWc_code.ItemIndex = lueWc_code.Properties.GetDataSourceRowIndex("WC_CODE", regKey.GetValue("WC_CODE"));
                }
            }

            winsock1Connect_OP();
            //winsock1Connect_레이져컷팅();
            //winsock1Connect_클립조립();
            //winsock_Connect_초음파융착1();            
            //winsock1Connect_진동융착();
            //winsock_Connect_초음파융착2();
            //winsock1Connect_검사기();

            //Refresh_timer_레이져컷팅.Enabled = true;
            //Refresh_timer_레이져컷팅.Start();

            //Refresh_timer_클립조립.Enabled = true;
            //Refresh_timer_클립조립.Start();

            //Refresh_timer_진동.Enabled = true;
            //Refresh_timer_진동.Start();

            //Refresh_timer_검사기.Enabled = true;
            //Refresh_timer_검사기.Start();

            Refresh_timer_OP.Enabled = true;
            Refresh_timer_OP.Start();

            //read_send_timer.Start();
            //read_send_time_2.Start();
            read_send_timer_op.Start();
            
        }


        private void timer1_Tick(object sender, EventArgs e)//현재시간
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }


        #region op판넬 연결/송수신
        string op_data;
        private void winsock1Connect_OP()//통신연결
        {
            try
            {
                if (winsock_OP.GetState.ToString() != "Connected")
                {

                    winsock_OP.LocalPort = 3001;

                    winsock_OP.RemoteIP = ip_Op_Pannel;

                    winsock_OP.RemotePort = 3000;

                    winsock_OP.Connect();

                }
            }
            catch (Exception ex)
            {

            }
        }
        bool read_flag = false;
        string recived_lot_no = "";
        private void winsock1_DataArrival_OP(MelsecPLC.Winsock sender, int BytesTotal)//받은데이터 처리
        {
            //String s = String.Empty;
            //winsock1.GetData(ref s);
            //textBox2.Text = s.ToString();
            byte[] bt = new byte[512];
            winsock_OP.GetData(ref bt);
            //winsock_OP.Listen();
            //string recive_str = "";

            //for (int i = 0; i < bt.Length; i++)
            //{
            //    if (i == 9 || i == 10)
            //    {
            //        if (!bt[i].ToString("X2").Equals("00"))
            //        {
            //            //오류
            //            recive_str = "Error";
            //            return;
            //        }
            //    }
            //    if (i > 10)
            //    {
            //        //recive_str += bt[i].ToString("X2");
            //        recive_str += string.Format("{0} ", bt[i]);
            //    }
            //}

            //recive_str = recive_str.Trim();

            //MessageBox.Show(recive_str);
            Thread my_t1 = new Thread(() => receive_data_op(bt));
            my_t1.Start();
            if (read_flag)
            {
                int check = Convert.ToInt32((bt[8].ToString("X2") + bt[7].ToString("X2")), 16);
                //list_box_OP.Items.Add(check.ToString());
                final_ok_ng = "";
                final_nozzle_cnt = "";
                recived_lot_no="";
                final_connection_spec="";
                final_connection_cnt = "";
                string 레이져_ok_ng = "";
                string 클립_ok_ng = "";
                string 초음파1_ok_ng = "";
                string 진동_ok_ng = "";
                string 초음파2_ok_ng = "";

                if (check == 68)
                {
                    final_ok_ng = bt[12].ToString("X2") + bt[11].ToString("X2"); //OK:1,NG:2
                    final_nozzle_cnt = bt[72].ToString("X2") + bt[71].ToString("X2");
                    final_connection_spec = bt[74].ToString("X2") + bt[73].ToString("X2");
                    final_connection_cnt = bt[76].ToString("X2") + bt[75].ToString("X2");

                    레이져_ok_ng = bt[34].ToString("X2") + bt[33].ToString("X2");
                    클립_ok_ng = bt[36].ToString("X2") + bt[35].ToString("X2");
                    초음파1_ok_ng = bt[38].ToString("X2") + bt[37].ToString("X2");
                    진동_ok_ng = bt[40].ToString("X2") + bt[39].ToString("X2");
                    초음파2_ok_ng = bt[46].ToString("X2") + bt[45].ToString("X2");

                    final_ok_ng = Convert.ToInt32(final_ok_ng, 16).ToString();
                    final_nozzle_cnt = Convert.ToInt32(final_nozzle_cnt, 16).ToString();

                    final_connection_spec = Convert.ToInt32(final_connection_spec, 16).ToString();
                    final_connection_cnt = Convert.ToInt32(final_connection_cnt, 16).ToString();


                    레이져_ok_ng = Convert.ToInt32(레이져_ok_ng, 16).ToString();
                    클립_ok_ng = Convert.ToInt32(클립_ok_ng, 16).ToString();
                    초음파1_ok_ng = Convert.ToInt32(초음파1_ok_ng, 16).ToString();
                    진동_ok_ng = Convert.ToInt32(진동_ok_ng, 16).ToString();
                    초음파2_ok_ng = Convert.ToInt32(초음파2_ok_ng, 16).ToString();

                    if (final_ok_ng.Equals("2") || final_ok_ng.Equals("1"))
                    {
                        //24 20 16 12 11 00 02
                        string recievd_spec = "";
                        string recievd_spec2 = "";
                        if (bt[51].ToString("X2").ToString().Length.Equals(1))
                        {
                            recievd_spec = "0" + bt[51].ToString("X2").ToString();
                        }
                        else
                        {
                            recievd_spec = bt[51].ToString("X2").ToString();
                        }
                        if (bt[52].ToString("X2").ToString().Length.Equals(1))
                        {
                            recievd_spec2 = "0" + bt[52].ToString("X2").ToString();
                        }
                        else
                        {
                            recievd_spec2 = bt[52].ToString("X2").ToString();
                        }
                        string year1 = "";
                        string year2 = "";
                        if (bt[53].ToString("X2").ToString().Length.Equals(1))
                        {
                            year1 = "0" + bt[53].ToString("X2").ToString();
                        }
                        else
                        {
                            year1 = bt[53].ToString("X2").ToString();
                        }
                        if (bt[54].ToString("X2").ToString().Length.Equals(1))
                        {
                            year2 = "0" + bt[54].ToString("X2").ToString();
                        }
                        else
                        {
                            year2 = bt[54].ToString("X2").ToString();
                        }
                        string month = "";
                        if (bt[55].ToString("X2").ToString().Length.Equals(1))
                        {
                            month = "0" + bt[55].ToString("X2").ToString();
                        }
                        else
                        {
                            month = bt[55].ToString("X2").ToString();
                        }
                        string day = "";
                        if (bt[56].ToString("X2").ToString().Length.Equals(1))
                        {
                            day = "0" + bt[56].ToString("X2").ToString();
                        }
                        else
                        {
                            day = bt[56].ToString("X2").ToString();
                        }
                        string seq1 = "";
                        if (bt[57].ToString("X2").ToString().Length.Equals(1))
                        {
                            seq1 = "0" + bt[57].ToString("X2").ToString();
                        }
                        else
                        {
                            seq1 = bt[57].ToString("X2").ToString();
                        } 
                        string seq2 = "";
                        if (bt[58].ToString("X2").ToString().Length.Equals(1))
                        {
                            seq2 = "0" + bt[58].ToString("X2").ToString();
                        }
                        else
                        {
                            seq2 = bt[58].ToString("X2").ToString();
                        }
                        recived_lot_no = recievd_spec +recievd_spec2 + year1 + year2 + month + day + seq1 + seq2;
                        NG_DataRecieved_save(recived_lot_no, 레이져_ok_ng, 클립_ok_ng, 초음파1_ok_ng, 진동_ok_ng, 초음파2_ok_ng, final_ok_ng);//NG 데이터 수신 완료 READING_DATA 완료 주기 
                        //OK/NG 수신후 CLEAR 해주기 D5110번지 부터 15워드
                        write_send_OP_SEND_LOT_CLEAR();//로뜨와 설비데이터 리셋
                        if (final_ok_ng.Equals("2"))
                        {
                            Thread my_alarm_push = new Thread(() => alarm_post_send());
                            my_alarm_push.Start();
                        }
                    }
                }
                op_data = final_ok_ng + "/" + final_nozzle_cnt + "/" + final_connection_spec + "/" + final_connection_cnt + "/" + recived_lot_no;
                if (final_ok_ng.Equals("1") || final_ok_ng.Equals("2"))//1:ok , 2:ng
                {
                    //5100번지 0으로 리셋 (전체 합부 판정)
                    write_send_OP_SEND_OK_CLEAR();
                }
                
                if (final_nozzle_cnt.Equals("1"))
                {
                    //5130번지 0으로 리셋 (노즐 카운트)
                    write_send_OP_SEND_NOZZLE_CLEAR();
                    노즐_투입수량증가();
                }
                if (!final_connection_cnt.Equals("0"))
                {
                    //if (!final_connection_spec.Equals("0"))
                    //{
                        //5131~5132번지 0으로 리셋 (커넥션 카운트)
                        write_send_OP_SEND_커넥션_CLEAR();
                        커넥션_투입수량증가();
                    //}
                }
            }
        }
        public void read_send_OP()//읽기 명령어
        {
            read_flag = true;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            Thread.Sleep(100);
            byte[] cmd2 = new byte[21];
            
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0xEC; cmd2[16] = 0x13; cmd2[17] = 0x00; //선두 디바이스
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x21; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte

            winsock_OP.Send(cmd2);
        }
        private void Refresh_timer_OP_Tick(object sender, EventArgs e)//새로고침 함수
        {
            
            if (!string.IsNullOrEmpty(op_data))
            {
                //string[] deciValuesSplit = op_data.Split(' ');
                //string StringOut = "";
                //string recieved_msg = "";
                //foreach (string deci in deciValuesSplit)
                //{
                //    //    // Convert the number expressed in base-16 to an integer.
                //    //int value = Convert.ToInt32(hex, 16);
                //    //    // Get the character corresponding to the integral value.
                //    //string stringValue = Char.ConvertFromUtf32(hex);
                //    //MessageBox.Show(deci);
                //    int a = int.Parse(deci);
                //    if (a <= 32 || a >= 127)
                //    {
                //        //StringOut = StringOut + "<" + charNames[a] + ">";
                //        //Uglier "Termite" style
                //        //StringOut = StringOut + String.Format("[{0:X2}]", (int)c);
                //    }
                //    else
                //    {
                //        StringOut = StringOut + a;
                //        recieved_msg += ((char)a).ToString();
                //    }

                //    //MessageBox.Show(Convert.ToInt32(a.ToString(),16).ToString());
                //    //MessageBox.Show(string.Format("hexadecimal value = {0}, int value = {1}, char value = {2} or {3}",
                //    //                    hex, value, stringValue, charValue));
                //}
                list_box_OP.Items.Add(op_data);
                outputList_Scroll();
                
            }
        }
        public void write_send_OP(string spec,string send_lot)//쓰기 명령어 작업지시 변경시 한번쓰기 D50번지 
        {
            read_flag = false;
            //
            byte[] bt_spec = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A
                                 ,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14
                                 ,0x15,0x16,0x17,0x18 };
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            Thread.Sleep(100);
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x32;
            cmd2[16] = 0x00;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = bt_spec[int.Parse(spec)];
            cmd2[22] = 0x00;

            winsock_OP.Send(cmd2);
            //사양전송 데이터 DB 저장
            Thread my_spec_send = new Thread(() => receive_data_op_SPEC(cmd2, send_lot,"사양전송"));
            my_spec_send.Start();
        }
        public void write_send_OP_reading_OK(string send_lot)//쓰기 명령어 리딩 ok 시 D5000번지에 쓰기
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x88;
            cmd2[16] = 0x13;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x01;
            cmd2[22] = 0x00;
            winsock_OP.Send(cmd2);
            Thread my_spec_send = new Thread(() => receive_data_op_SPEC(cmd2, send_lot, "OK전송"));
            my_spec_send.Start();
        }

        public void write_send_Lot_Send_data(string send_lot)//쓰기 명령어 lot 보내주기
        {
            //24 20 16 12 11 00 02
            byte[] by_send = new byte[8];
            if (send_lot.Length.Equals(16))
            {
                by_send[0] = Convert.ToByte(send_lot.Substring(0, 2), 16);
                by_send[1] = Convert.ToByte(send_lot.Substring(2, 2), 16);
                by_send[2] = Convert.ToByte(send_lot.Substring(4, 2), 16);
                by_send[3] = Convert.ToByte(send_lot.Substring(6, 2), 16);
                by_send[4] = Convert.ToByte(send_lot.Substring(8, 2), 16);
                by_send[5] = Convert.ToByte(send_lot.Substring(10, 2), 16);
                by_send[6] = Convert.ToByte(send_lot.Substring(12, 2), 16);
                by_send[7] = Convert.ToByte(send_lot.Substring(14, 2), 16);
            }
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[31];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x16;//0E
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x46;
            cmd2[16] = 0x00;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x05;
            cmd2[20] = 0x00;

            cmd2[21] = by_send[0];
            cmd2[22] = by_send[1];
            //////////before
            cmd2[23] = by_send[2];
            cmd2[24] = by_send[3];

            cmd2[25] = by_send[4];
            cmd2[26] = by_send[5];

            cmd2[27] = by_send[6];
            cmd2[28] = by_send[7];

            cmd2[29] = 0x00;
            cmd2[30] = 0x00;
            winsock_OP.Send(cmd2);
            Thread my_spec_send = new Thread(() => receive_data_op_SPEC(cmd2, send_lot, "LOT전송"));
            my_spec_send.Start();
        }
        public void write_send_OP_SEND_OK_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5100번지 
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0xEC;
            cmd2[16] = 0x13;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;
            cmd2[22] = 0x00;
            winsock_OP.Send(cmd2);
        }

        public void write_send_OP_SEND_NOZZLE_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5131번지 
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x0A;
            cmd2[16] = 0x14;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;
            cmd2[22] = 0x00;
            winsock_OP.Send(cmd2);
        }
        public void write_send_OP_SEND_커넥션_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5131,5132번지 
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[25];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x10;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x0B;
            cmd2[16] = 0x14;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x02;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;
            cmd2[22] = 0x00;
            cmd2[23] = 0x00;
            cmd2[24] = 0x00;
            winsock_OP.Send(cmd2);
        }
        public void write_send_OP_SEND_LOT_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5110번지 부터 15워드
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[51];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x2A;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0xF6;
            cmd2[16] = 0x13;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x0F;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;//5110
            cmd2[22] = 0x00;//
            cmd2[23] = 0x00;//5111
            cmd2[24] = 0x00;//
            cmd2[25] = 0x00;//5112
            cmd2[26] = 0x00;//
            cmd2[27] = 0x00;//5113
            cmd2[28] = 0x00;//
            cmd2[29] = 0x00;//5114
            cmd2[30] = 0x00;//
            cmd2[31] = 0x00;//5115
            cmd2[32] = 0x00;//
            cmd2[33] = 0x00;//5116
            cmd2[34] = 0x00;//
            cmd2[35] = 0x00;//5117
            cmd2[36] = 0x00;//
            cmd2[37] = 0x00;//5118
            cmd2[38] = 0x00;//
            cmd2[39] = 0x00;//5119
            cmd2[40] = 0x00;//
            cmd2[41] = 0x00;//5120
            cmd2[42] = 0x00;//
            cmd2[43] = 0x00;//5121
            cmd2[44] = 0x00;//
            cmd2[45] = 0x00;//5122
            cmd2[46] = 0x00;//
            cmd2[47] = 0x00;//5123
            cmd2[48] = 0x00;//
            cmd2[49] = 0x00;//5124
            cmd2[50] = 0x00;//

            winsock_OP.Send(cmd2);
        }
        
        private void simpleButton8_Click(object sender, EventArgs e)//read 버튼
        {
            read_send_OP();
        }
        private void simpleButton9_Click(object sender, EventArgs e)//write 버튼
        {
            winsock_OP.Close();
        }
        #endregion
        
        #region 레이져컷팅(MELSEC)
        string 레이져컷팅_data;
        private void winsock1Connect_레이져컷팅()//통신연결
        {
            try
            {
                if (winsock_레이져컷팅.GetState.ToString() != "Connected")
                {

                    winsock_레이져컷팅.LocalPort = 3001;

                    winsock_레이져컷팅.RemoteIP = ip_레이져컷팅;

                    winsock_레이져컷팅.RemotePort = 3000;

                    winsock_레이져컷팅.Connect();

                }
            }
            catch (Exception ex)
            {

            }
        }
        string received_data_A = "";
        string received_data_B = "";
        private void winsock1_DataArrival_레이져컷팅(MelsecPLC.Winsock sender, int BytesTotal)//받은데이터 처리
        {
            //String s = String.Empty;
            //winsock1.GetData(ref s);
            //textBox2.Text = s.ToString();
            byte[] bt = new byte[256];
            winsock_레이져컷팅.GetData(ref bt);
            

            int check = Convert.ToInt32((bt[8].ToString("X2") + bt[7].ToString("X2")), 16);
            
            string laser_in="A10S";            
            string laser_ok = "A11S00";
            string laser_ng = "A11S10";
            string str = "";
            //foreach (byte btt in bt)
            //{
            //    str += btt.ToString("X2");
            //}

            if (check == 6)
            {
                received_data_A = bt[12].ToString("X2") + bt[11].ToString("X2"); //
                received_data_B = bt[14].ToString("X2") + bt[13].ToString("X2"); //

                received_data_A = Convert.ToInt32(received_data_A, 16).ToString();
                received_data_B = Convert.ToInt32(received_data_B, 16).ToString();
                if (received_data_A.Trim().Equals("0"))
                {

                }
                else if (received_data_A.Trim().Equals("1") && !레이져컷팅_toggle)
                {
                    레이져컷팅_toggle = true;
                    Equip_in_out_insert_레이져(laser_in, "","A");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);  
                    //레이져컷팅_A면초기화();
                    
                }
                else if (received_data_A.Trim().Equals("2") && 레이져컷팅_toggle)
                {
                    레이져컷팅_toggle = false;
                    Equip_in_out_insert_레이져(laser_ok, "", "A");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);
                    //레이져컷팅_A면초기화();
                    
                }
                else if (received_data_A.Trim().Equals("3") && 레이져컷팅_toggle)
                {
                    레이져컷팅_toggle = false;
                    Equip_in_out_insert_레이져(laser_ng, "", "A");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);
                    //레이져컷팅_A면초기화();
                    
                }
                if (received_data_B.Trim().Equals("0"))
                {

                }
                else if (received_data_B.Trim().Equals("1") && !레이져컷팅_toggle_B)
                {
                    레이져컷팅_toggle_B = true;
                    Equip_in_out_insert_레이져(laser_in, "", "B");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);
                    //레이져컷팅_B면초기화();
                    
                }
                else if (received_data_B.Trim().Equals("2") && 레이져컷팅_toggle_B)
                {
                    레이져컷팅_toggle_B = false;
                    Equip_in_out_insert_레이져(laser_ok, "", "B");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);
                    //레이져컷팅_B면초기화();
                    
                }
                else if (received_data_B.Trim().Equals("3") && 레이져컷팅_toggle_B)
                {
                    레이져컷팅_toggle_B = false;
                    Equip_in_out_insert_레이져(laser_ng, "", "B");
                    winsock_레이져컷팅.Close();
                    //Thread.Sleep(100);
                    //레이져컷팅_B면초기화();
                    
                }


            }
                레이져컷팅_data = "A면:" + received_data_A + "/B면:" + received_data_B;
                list_box_레이져컷팅.Items.Add(레이져컷팅_data);
                //list_box_레이져컷팅.Items.Add(str);
                outputList_Scroll();

                
                
        }
        public void read_send_레이져컷팅()//읽기 명령어(20161215 수정 1800번지,1801번지)
        {
            
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                winsock_레이져컷팅.Connect();
            }
            Thread.Sleep(100);
            byte[] cmd2 = new byte[21];
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0x08; cmd2[16] = 0x07; cmd2[17] = 0x00; //선두 디바이스
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x02; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte

            winsock_레이져컷팅.Send(cmd2);
        }
        private void Refresh_timer_레이져컷팅_Tick(object sender, EventArgs e)//새로고침 함수
        {
            if (!string.IsNullOrEmpty(레이져컷팅_data))
            {
                

            }
        }
        
        #endregion

        #region 클립조립(MELSEC)
        string 클립조립_data;
        private void winsock1Connect_클립조립()//통신연결
        {
            try
            {
                if (winsock_클립조립.GetState.ToString() != "Connected")
                {

                    winsock_클립조립.LocalPort = 3001;

                    winsock_클립조립.RemoteIP = ip_클립조립;

                    winsock_클립조립.RemotePort = 3000;

                    winsock_클립조립.Connect();

                }
            }
            catch (Exception ex)
            {

            }
        }
        private void winsock1_DataArrival_클립조립(MelsecPLC.Winsock sender, int BytesTotal)//받은데이터 처리
        {
            //String s = String.Empty;
            //winsock1.GetData(ref s);
            //textBox2.Text = s.ToString();
            byte[] bt = new byte[1024];
            winsock_클립조립.GetData(ref bt);
            string recive_str = "";

            for (int i = 0; i < bt.Length; i++)
            {
                if (i == 9 || i == 10)
                {
                    if (!bt[i].ToString("X2").Equals("00"))
                    {
                        //오류
                        recive_str = "Error";
                        return;
                    }
                }
                if (i > 10)
                {
                    //recive_str += bt[i].ToString("X2");
                    recive_str += string.Format("{0} ", bt[i]);
                }
            }

            recive_str = recive_str.Trim();


            클립조립_data = recive_str;
        }
        public void read_send_클립조립()//읽기 명령어
        {
            if (winsock_클립조립.GetState.ToString() != "Connected")
            {
                winsock1Connect_클립조립();
            }
            byte[] cmd2 = new byte[21];
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0x6C; cmd2[16] = 0x07; cmd2[17] = 0x00; //선두 디바이스
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x03; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte

            winsock_클립조립.Send(cmd2);
        }
        private void Refresh_timer_클립조립_Tick(object sender, EventArgs e)//새로고침 함수
        {
            if (!string.IsNullOrEmpty(클립조립_data))
            {
                string[] deciValuesSplit = 클립조립_data.Split(' ');
                string StringOut = "";
                string recieved_msg = "";
                foreach (string deci in deciValuesSplit)
                {
                    //    // Convert the number expressed in base-16 to an integer.
                    //int value = Convert.ToInt32(hex, 16);
                    //    // Get the character corresponding to the integral value.
                    //string stringValue = Char.ConvertFromUtf32(hex);
                    //MessageBox.Show(deci);
                    int a = int.Parse(deci);
                    if (a <= 32 || a >= 127)
                    {
                        //StringOut = StringOut + "<" + charNames[a] + ">";
                        //Uglier "Termite" style
                        //StringOut = StringOut + String.Format("[{0:X2}]", (int)c);
                    }
                    else
                    {
                        StringOut = StringOut + a;
                        recieved_msg += ((char)a).ToString();
                    }

                    //MessageBox.Show(Convert.ToInt32(a.ToString(),16).ToString());
                    //MessageBox.Show(string.Format("hexadecimal value = {0}, int value = {1}, char value = {2} or {3}",
                    //                    hex, value, stringValue, charValue));
                }

                if (recieved_msg.Substring(2, 1).Equals("0") && !클립조립_toggle)
                {
                    //lbl_B1.Text = "작동중";
                    //lbl_B1.ForeColor = Color.Lime;
                    클립조립_toggle = true;
                    
                    Equip_in_out_insert(recieved_msg, "");
                }
                else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("0") && 클립조립_toggle)
                {
                    //lbl_B1.Text = "OK";
                    //lbl_B1.ForeColor = Color.DeepSkyBlue;
                    클립조립_toggle = false;
                    
                    Equip_in_out_insert(recieved_msg, "");

                }
                else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("1") && 클립조립_toggle)
                {
                    //lbl_B1.Text = "NG";
                    //lbl_B1.ForeColor = Color.Red;
                    클립조립_toggle = false;
                    
                    Equip_in_out_insert(recieved_msg, "");

                }

                list_box_클립조립.Items.Add(recieved_msg);
                outputList_Scroll();
            }
        }
        
        #endregion

        #region 초음파융착1(LS)
        private void winsock_Connect_초음파융착1()//통신 연결
        {

            IPAddress peerIP = IPAddress.Parse(ip_초음파융착1);
            // There are IP or port information in the localEP
            IPEndPoint peerEP = new IPEndPoint(peerIP, 2004);

            // Client socket memory allocation
            clientsock_초음파1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                // Send the signal for connect with server 
                // This application will be run asynchronously by this function.
                // If the client accept the socekt, this function called a ConnectCallback function.
                clientsock_초음파1.BeginConnect(ip_초음파융착1, 2004, new AsyncCallback(ConnectCallback_초음파융착1), clientsock_초음파1);



            }
            catch (SocketException er)
            {
                // If the socket exception is occurrence, the application display a error message.  
                //MessageBox.Show(er.Message);
            }
        }
        public void ConnectCallback_초음파융착1(IAsyncResult ar)//통신연결 콜백
        {
            try
            {
                // If the client socket connect with the server, return the socket
                clientsock_초음파1 = (Socket)ar.AsyncState;
                clientsock_초음파1.EndConnect(ar);
                // If the client transmitted the messages, this function called a Callback_ReceiveMsg function.
                clientsock_초음파1.BeginReceive(MsgRecvBuff_초음파1, 0, MsgRecvBuff_초음파1.Length, SocketFlags.None, new AsyncCallback(CallBack_ReceiveMsg_초음파융착1), clientsock_초음파1);
            }
            catch (SocketException er)
            {
                //MessageBox.Show(er.Message);
            }

        }
        public void CallBack_ReceiveMsg_초음파융착1(IAsyncResult ar)//받은 데이터 처리
        {

            int length;
            // A variable of decimal message
            String MsgRecvStr = null;
            // A variable of hex message
            String MsgRecvHexStr = null;

            try
            {
                // If the socket close, return the 0 value 
                length = clientsock_초음파1.EndReceive(ar);

                if (length > 0)
                {

                    // display with decimal data
                    MsgRecvStr = Encoding.Default.GetString(MsgRecvBuff_초음파1, 0, MsgRecvBuff_초음파1.Length);

                    // display with hex data
                    MsgRecvHexStr = ToHexString(MsgRecvStr, "Recv");

                    //this.ConnText.AppendText("Received Data : " + MsgRecvHexStr + " | " + MsgRecvStr + "\n");
                    //this.ConnText.ScrollToCaret();

                    // The socket can continuedly wait the receive message because of this function 
                    clientsock_초음파1.BeginReceive(MsgRecvBuff_초음파1, 0, MsgRecvBuff_초음파1.Length, SocketFlags.None, new AsyncCallback(CallBack_ReceiveMsg_초음파융착1), clientsock_초음파1);
                    string[] charNames = { "NUL", "SOH", "STX", "ETX", "EOT",
				                            "ENQ", "ACK", "BEL", "BS", "HT", "LF", "VT", "FF", "CR", "SO", "SI",
				                            "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB", "CAN", "EM", "SUB",
				                            "ESC", "FS", "GS", "RS", "US", "Space"};
                    string recieved_data = "";

                    foreach (byte bb in MsgRecvBuff_초음파1)
                    {
                        //MessageBox.Show(char.ConvertFromUtf32(bb));
                        recieved_data += string.Format("{0} ", bb);
                        //recieved_data += string.Format("{0} ", bb.ToString("X2"));
                    }
                    recieved_data = recieved_data.Trim();
                    string[] deciValuesSplit = recieved_data.Split(' ');
                    string StringOut = "";
                    string recieved_msg = "";
                    for (int i = 32; i < deciValuesSplit.Length;i++ )
                    {
                        int a = int.Parse(deciValuesSplit[i]);
                        if (a <= 32 || a >= 127)
                        {

                        }
                        else
                        {
                            StringOut = StringOut + a;
                            recieved_msg += ((char)a).ToString();
                        }
                    }
                    if (recieved_msg.Substring(2, 1).Equals("0") && !초음파1_toggle)
                    {
                        //lbl_C1.Text = "작동중";
                        //lbl_C1.ForeColor = Color.Lime;
                        초음파1_toggle = true;
                       
                        Equip_in_out_insert(recieved_msg, "");
                    }
                    else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("0") && 초음파1_toggle)
                    {
                        //lbl_C1.Text = "OK";
                        //lbl_C1.ForeColor = Color.DeepSkyBlue;
                        초음파1_toggle = false;
                        
                        Equip_in_out_insert(recieved_msg, "");

                    }
                    else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("1") && 초음파1_toggle)
                    {
                        //lbl_C1.Text = "NG";
                        //lbl_C1.ForeColor = Color.Red;
                        초음파1_toggle = false;
                        
                        Equip_in_out_insert(recieved_msg, "");

                    }
                    list_box_초음파1.Items.Add(recieved_msg);
                    outputList_Scroll();
                }
                else
                {
                    // If the socket close, return the 0 value 
                    // Then, socket close.

                    clientsock_초음파1.Close();
                    
                }

            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }
        public void CallBack_SendMsg_초음파융착1(IAsyncResult ar)//보낸 데이터 콜백
        {
            int length;
            // A variable of decimal message
            string MsgSendStr = null;
            // A variable of hex message
            string MsgSendHexStr = null;

            // get the send message 
            clientsock_초음파1 = (Socket)ar.AsyncState;
            // Get the send message's length
            length = clientsock_초음파1.EndSend(ar);
            // Encoding the byte to string.
            MsgSendStr = Encoding.Default.GetString(MsgSendBuff_초음파1, 0, length);

            // Display with hex data
            MsgSendHexStr = ToHexString(MsgSendStr, "Send");

        }        
        private void read_send_초음파융착1()//연속읽기 명령어
        {
            if (!clientsock_초음파1.Connected)
            {
                winsock_Connect_초음파융착1();    
                return;
            }
            int sendLength;
            int i = 0;

            try
            {
                sendLength = MsgSendBuff_초음파1.Length;



                byte[] header = { 0x4C, 0x53, 0x49, 0x53, 0x2D, 0x58, 0x47, 0x54, 0x00, 0x00
                                ,0x00, 0x00
                                ,0xA0
                                ,0x33
                                ,0x00,0x01
                                ,0x13,0x00
                                ,0x00
                                ,0x00};

                //header
                int header_idx = 0;
                foreach (byte hex_byte in header)
                {
                    MsgSendBuff_초음파1[i] = hex_byte;
                    i++;
                }


                //명령어 부분(연속읽기)
                MsgSendBuff_초음파1[20] = 0x54;//명령어
                MsgSendBuff_초음파1[21] = 0x00;
                MsgSendBuff_초음파1[22] = 0x14;//데이터 타입
                MsgSendBuff_초음파1[23] = 0x00;
                MsgSendBuff_초음파1[24] = 0x00;//예약영역
                MsgSendBuff_초음파1[25] = 0x00;
                MsgSendBuff_초음파1[26] = 0x01;//블록수
                MsgSendBuff_초음파1[27] = 0x00;
                MsgSendBuff_초음파1[28] = 0x07;//변수길이
                MsgSendBuff_초음파1[29] = 0x00;
                MsgSendBuff_초음파1[30] = Convert.ToByte('%'); //0x25
                MsgSendBuff_초음파1[31] = Convert.ToByte('D'); //0x4D
                MsgSendBuff_초음파1[32] = Convert.ToByte('B'); //0x57
                MsgSendBuff_초음파1[33] = Convert.ToByte('3'); //0x36
                MsgSendBuff_초음파1[34] = Convert.ToByte('8'); //0x30
                MsgSendBuff_초음파1[35] = Convert.ToByte('0'); //0x30
                MsgSendBuff_초음파1[36] = Convert.ToByte('0'); //0x30                
                MsgSendBuff_초음파1[37] = 0x12;
                MsgSendBuff_초음파1[38] = 0x00;


                clientsock_초음파1.BeginSend(MsgSendBuff_초음파1, 0, sendLength, SocketFlags.None, new AsyncCallback(CallBack_SendMsg_초음파융착1), clientsock_초음파1);
            }
            // If the socket exception is occurrence, the application display a error message.  
            catch (SocketException se)
            {
                MessageBox.Show(se.ErrorCode + ";" + se.Message);
                Environment.Exit(se.ErrorCode);
            }
        }
        
        #endregion

        #region 진동융착(MELSEC)
        string 진동_data;
        private void winsock1Connect_진동융착()//통신연결
        {
            try
            {
                if (winsock_진동.GetState.ToString() != "Connected")
                {

                    winsock_진동.LocalPort = 3001;

                    winsock_진동.RemoteIP = ip_진동융착;

                    winsock_진동.RemotePort = 3000;

                    winsock_진동.Connect();

                }
            }
            catch (Exception ex)
            {

            }
        }
        private void winsock1_DataArrival_진동융착(MelsecPLC.Winsock sender, int BytesTotal)//받은데이터 처리
        {
            //String s = String.Empty;
            //winsock1.GetData(ref s);
            //textBox2.Text = s.ToString();
            byte[] bt = new byte[1024];
            winsock_진동.GetData(ref bt);
            string recive_str = "";

            for (int i = 0; i < bt.Length; i++)
            {
                if (i == 9 || i == 10)
                {
                    if (!bt[i].ToString("X2").Equals("00"))
                    {
                        //오류
                        recive_str = "Error";
                        return;
                    }
                }
                if (i > 10)
                {
                    //recive_str += bt[i].ToString("X2");
                    recive_str += string.Format("{0} ", bt[i]);
                }
            }

            recive_str = recive_str.Trim();


            진동_data = recive_str;
        }        
        public void read_send_진동융착()//읽기 명령어
        {
            if (winsock_진동.GetState.ToString() != "Connected")
            {
                winsock1Connect_진동융착();
            }
            byte[] cmd2 = new byte[21];
            cmd2[0] = 0x50; cmd2[1] = 0x00; //서브 헤더
            cmd2[2] = 0x00; //네트워크 번호
            cmd2[3] = 0xFF; //PLC 번호
            cmd2[4] = 0xFF; cmd2[5] = 0x03;//요구상대 I/O 번호
            cmd2[6] = 0x00; //국번호
            cmd2[7] = 0x0C; cmd2[8] = 0x00; //요구데이터 길이
            cmd2[9] = 0x10; cmd2[10] = 0x00; //CPU 감시 타이머
            cmd2[11] = 0x01; cmd2[12] = 0x04; //커멘트
            cmd2[13] = 0x00; cmd2[14] = 0x00; //서브 커멘드
            cmd2[15] = 0x6C; cmd2[16] = 0x07; cmd2[17] = 0x00; //선두 디바이스
            cmd2[18] = 0xA8;  //디바이스 코드
            cmd2[19] = 0x03; cmd2[20] = 0x00;//디바이스 점 수 3word = 12byte

            winsock_진동.Send(cmd2);
        }        
        private void Refresh_timer_진동_Tick(object sender, EventArgs e)//새로고침 함수
        {
            if (!string.IsNullOrEmpty(진동_data))
            {
                string[] deciValuesSplit = 진동_data.Split(' ');
                string StringOut = "";
                string recieved_msg = "";
                foreach (string deci in deciValuesSplit)
                {
                    //    // Convert the number expressed in base-16 to an integer.
                    //int value = Convert.ToInt32(hex, 16);
                    //    // Get the character corresponding to the integral value.
                    //string stringValue = Char.ConvertFromUtf32(hex);
                    //MessageBox.Show(deci);
                    int a = int.Parse(deci);
                    if (a <= 32 || a >= 127)
                    {
                        //StringOut = StringOut + "<" + charNames[a] + ">";
                        //Uglier "Termite" style
                        //StringOut = StringOut + String.Format("[{0:X2}]", (int)c);
                    }
                    else
                    {
                        StringOut = StringOut + a;
                        recieved_msg += ((char)a).ToString();
                    }

                    //MessageBox.Show(Convert.ToInt32(a.ToString(),16).ToString());
                    //MessageBox.Show(string.Format("hexadecimal value = {0}, int value = {1}, char value = {2} or {3}",
                    //                    hex, value, stringValue, charValue));
                }

                if (recieved_msg.Substring(2, 1).Equals("0") && !진동융착_toggle)
                {
                    //lbl_E1.Text = "작동중";
                    //lbl_E1.ForeColor = Color.Lime;
                    진동융착_toggle = true;
                    
                    Equip_in_out_insert(recieved_msg, "");
                }
                else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("0") && 진동융착_toggle)
                {
                    //lbl_E1.Text = "OK";
                    //lbl_E1.ForeColor = Color.DeepSkyBlue;
                    진동융착_toggle = false;
                    
                    Equip_in_out_insert(recieved_msg, "");

                }
                else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("1") && 진동융착_toggle)
                {
                    //lbl_E1.Text = "NG";
                    //lbl_E1.ForeColor = Color.Red;
                    진동융착_toggle = false;
                    
                    Equip_in_out_insert(recieved_msg, "");

                }

                list_box_진동.Items.Add(recieved_msg);
                outputList_Scroll();
            }
        }
        
        #endregion

        #region 초음파융착2(LS)
        private void winsock_Connect_초음파융착2()//통신 연결
        {

            IPAddress peerIP = IPAddress.Parse(ip_초음파융착2);
            // There are IP or port information in the localEP
            IPEndPoint peerEP = new IPEndPoint(peerIP, 2004);

            // Client socket memory allocation
            clientsock_초음파2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                // Send the signal for connect with server 
                // This application will be run asynchronously by this function.
                // If the client accept the socekt, this function called a ConnectCallback function.
                clientsock_초음파2.BeginConnect(ip_초음파융착2, 2004, new AsyncCallback(ConnectCallback_초음파융착2), clientsock_초음파2);

                

            }
            catch (SocketException er)
            {
                // If the socket exception is occurrence, the application display a error message.  
                //MessageBox.Show(er.Message);
            }
        }
        public void ConnectCallback_초음파융착2(IAsyncResult ar)//통신연결 콜백
        {
            try
            {
                // If the client socket connect with the server, return the socket
                clientsock_초음파2 = (Socket)ar.AsyncState;
                clientsock_초음파2.EndConnect(ar);
                // If the client transmitted the messages, this function called a Callback_ReceiveMsg function.
                clientsock_초음파2.BeginReceive(MsgRecvBuff_초음파2, 0, MsgRecvBuff_초음파2.Length, SocketFlags.None, new AsyncCallback(CallBack_ReceiveMsg_초음파융착2), clientsock_초음파2);
            }
            catch (SocketException er)
            {
                //MessageBox.Show(er.Message);
            }

        }
        public void CallBack_ReceiveMsg_초음파융착2(IAsyncResult ar)//받은 데이터 처리
        {

            int length;
            // A variable of decimal message
            String MsgRecvStr = null;
            // A variable of hex message
            String MsgRecvHexStr = null;

            try
            {
                // If the socket close, return the 0 value 
                length = clientsock_초음파2.EndReceive(ar);

                if (length > 0)
                {

                    // display with decimal data
                    MsgRecvStr = Encoding.Default.GetString(MsgRecvBuff_초음파2, 0, MsgRecvBuff_초음파2.Length);

                    // display with hex data
                    MsgRecvHexStr = ToHexString(MsgRecvStr, "Recv");

                    //this.ConnText.AppendText("Received Data : " + MsgRecvHexStr + " | " + MsgRecvStr + "\n");
                    //this.ConnText.ScrollToCaret();

                    // The socket can continuedly wait the receive message because of this function 
                    clientsock_초음파2.BeginReceive(MsgRecvBuff_초음파2, 0, MsgRecvBuff_초음파2.Length, SocketFlags.None, new AsyncCallback(CallBack_ReceiveMsg_초음파융착2), clientsock_초음파2);
                    string[] charNames = { "NUL", "SOH", "STX", "ETX", "EOT",
				                            "ENQ", "ACK", "BEL", "BS", "HT", "LF", "VT", "FF", "CR", "SO", "SI",
				                            "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB", "CAN", "EM", "SUB",
				                            "ESC", "FS", "GS", "RS", "US", "Space"};
                    string recieved_data = "";

                    foreach (byte bb in MsgRecvBuff_초음파2)
                    {
                        //MessageBox.Show(char.ConvertFromUtf32(bb));
                        recieved_data += string.Format("{0} ", bb);
                        //recieved_data += string.Format("{0} ", bb.ToString("X2"));
                    }
                    recieved_data = recieved_data.Trim();
                    string[] deciValuesSplit = recieved_data.Split(' ');
                    string StringOut = "";
                    string recieved_msg = "";
                    for (int i = 32; i < deciValuesSplit.Length;i++ )
                    {
                        int a = int.Parse(deciValuesSplit[i]);
                        if (a <= 32 || a >= 127)
                        {

                        }
                        else
                        {
                            StringOut = StringOut + a;
                            recieved_msg += ((char)a).ToString();
                        }
                    }
                    if (recieved_msg.Substring(2, 1).Equals("0") && !초음파2_toggle)
                    {
                        //lbl_C2.Text = "작동중";
                        //lbl_C2.ForeColor = Color.Lime;
                        초음파2_toggle = true;
                       
                        Equip_in_out_insert(recieved_msg, "");
                    }
                    else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("0") && 초음파2_toggle)
                    {
                        //lbl_C2.Text = "OK";
                        //lbl_C2.ForeColor = Color.DeepSkyBlue;
                        초음파2_toggle = false;
                       
                        Equip_in_out_insert(recieved_msg, "");

                    }
                    else if (recieved_msg.Substring(2, 1).Equals("1") && recieved_msg.Substring(4, 1).Equals("1") && 초음파2_toggle)
                    {
                        //lbl_C2.Text = "NG";
                        //lbl_C2.ForeColor = Color.Red;
                        초음파2_toggle = false;
                        
                        Equip_in_out_insert(recieved_msg, "");

                    }
                    list_box_초음파2.Items.Add(recieved_msg);
                    outputList_Scroll();
                }
                else
                {
                    // If the socket close, return the 0 value 
                    // Then, socket close.

                    clientsock_초음파2.Close();
                }

            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }
        public void CallBack_SendMsg_초음파융착2(IAsyncResult ar)//보낸 데이터 콜백
        {
            int length;
            // A variable of decimal message
            string MsgSendStr = null;
            // A variable of hex message
            string MsgSendHexStr = null;

            // get the send message 
            clientsock_초음파2 = (Socket)ar.AsyncState;
            // Get the send message's length
            length = clientsock_초음파2.EndSend(ar);
            // Encoding the byte to string.
            MsgSendStr = Encoding.Default.GetString(MsgSendBuff_초음파2, 0, length);

            // Display with hex data
            MsgSendHexStr = ToHexString(MsgSendStr, "Send");

        }
        public string ToHexString(string MsgStr, string chk)//hex 변환
        {
            string MsgHexStr = null;


            if (chk == "Recv")
            {
                for (int i = 0; i < MsgStr.Length; i++)
                {
                    MsgHexStr += MsgRecvBuff_초음파2[i].ToString("X2") + " ";
                }
            }
            else if (chk == "Send")
            {
                for (int i = 0; i <= MsgStr.Length; i++)
                {
                    MsgHexStr += MsgSendBuff_초음파2[i].ToString("X2") + " ";

                }
            }

            return MsgHexStr;
        }
        private void read_send_초음파융착2()//연속읽기 명령어
        {
            if (!clientsock_초음파2.Connected)
            {
                winsock_Connect_초음파융착2();    
                return;
            }
            int sendLength;
            int i = 0;

            try
            {
                sendLength = MsgSendBuff_초음파2.Length;



                byte[] header = { 0x4C, 0x53, 0x49, 0x53, 0x2D, 0x58, 0x47, 0x54, 0x00, 0x00
                                ,0x00, 0x00
                                ,0xA0
                                ,0x33
                                ,0x00,0x01
                                ,0x13,0x00
                                ,0x00
                                ,0x00};

                //header
                int header_idx = 0;
                foreach (byte hex_byte in header)
                {
                    MsgSendBuff_초음파2[i] = hex_byte;
                    i++;
                }


                //명령어 부분(연속읽기)
                MsgSendBuff_초음파2[20] = 0x54;//명령어
                MsgSendBuff_초음파2[21] = 0x00;
                MsgSendBuff_초음파2[22] = 0x14;//데이터 타입
                MsgSendBuff_초음파2[23] = 0x00;
                MsgSendBuff_초음파2[24] = 0x00;//예약영역
                MsgSendBuff_초음파2[25] = 0x00;
                MsgSendBuff_초음파2[26] = 0x01;//블록수
                MsgSendBuff_초음파2[27] = 0x00;
                MsgSendBuff_초음파2[28] = 0x07;//변수길이
                MsgSendBuff_초음파2[29] = 0x00;
                MsgSendBuff_초음파2[30] = Convert.ToByte('%'); //0x25
                MsgSendBuff_초음파2[31] = Convert.ToByte('D'); //0x4D
                MsgSendBuff_초음파2[32] = Convert.ToByte('B'); //0x57
                MsgSendBuff_초음파2[33] = Convert.ToByte('3'); //0x36
                MsgSendBuff_초음파2[34] = Convert.ToByte('8'); //0x30
                MsgSendBuff_초음파2[35] = Convert.ToByte('0'); //0x30
                MsgSendBuff_초음파2[36] = Convert.ToByte('0'); //0x30                
                MsgSendBuff_초음파2[37] = 0x12;
                MsgSendBuff_초음파2[38] = 0x00;


                clientsock_초음파2.BeginSend(MsgSendBuff_초음파2, 0, sendLength, SocketFlags.None, new AsyncCallback(CallBack_SendMsg_초음파융착2), clientsock_초음파2);
            }
            // If the socket exception is occurrence, the application display a error message.  
            catch (SocketException se)
            {
                MessageBox.Show(se.ErrorCode + ";" + se.Message);
                Environment.Exit(se.ErrorCode);
            }
        }
        
        #endregion


        private void read_send_timer_Tick(object sender, EventArgs e)//타이머로 읽을 명령어 송신 0.1초마다
        {
            
            //read_send_레이져컷팅();
            //read_send_클립조립();
            //read_send_초음파융착1();
            //read_send_진동융착();
            //read_send_초음파융착2();
            
        }
        void outputList_Scroll()//들어오는 데이터 listBox 자동 스크롤
        {
            //if (scrolling)
            //{
            int itemsPerPage_레이져컷팅 = (int)(list_box_레이져컷팅.Height / list_box_레이져컷팅.ItemHeight);
            list_box_레이져컷팅.TopIndex = list_box_레이져컷팅.Items.Count - itemsPerPage_레이져컷팅;

            int itemsPerPage_클립조립 = (int)(list_box_클립조립.Height / list_box_클립조립.ItemHeight);
            list_box_클립조립.TopIndex = list_box_클립조립.Items.Count - itemsPerPage_클립조립;

            int itemsPerPage_초음파1 = (int)(list_box_초음파1.Height / list_box_초음파1.ItemHeight);
            list_box_초음파1.TopIndex = list_box_초음파1.Items.Count - itemsPerPage_초음파1;

            int itemsPerPage_진동 = (int)(list_box_진동.Height / list_box_진동.ItemHeight);
            list_box_진동.TopIndex = list_box_진동.Items.Count - itemsPerPage_진동;

            int itemsPerPage_초음파2 = (int)(list_box_초음파2.Height / list_box_초음파2.ItemHeight);
            list_box_초음파2.TopIndex = list_box_초음파2.Items.Count - itemsPerPage_초음파2;

            int itemsPerPage_OP = (int)(list_box_OP.Height / list_box_OP.ItemHeight);
            list_box_OP.TopIndex = list_box_OP.Items.Count - itemsPerPage_OP;

            //}
        }

        

        #region 테스트 code
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    byte[] bt = new byte[1024];

        //    bt[0] = 0x45;
        //    bt[1] = 0x31;
        //    bt[2] = 0x31;
        //    bt[3] = 0x53;
        //    bt[4] = 0x30;
        //    bt[5] = 0x30;
        //    string recive_str = "";

        //    for (int i = 0; i < bt.Length; i++)
        //    {
        //        //recive_str += bt[i].ToString("X2");
        //        recive_str += string.Format("{0} ", bt[i]);
        //    }
        //    recive_str = recive_str.Trim();
        //    Jig01 = recive_str;
        //}
        //private void button2_Click(object sender, EventArgs e)
        //{
        //    byte[] bt = new byte[1024];

        //    bt[0] = 0x45;
        //    bt[1] = 0x31;
        //    bt[2] = 0x30;
        //    bt[3] = 0x53;

        //    string recive_str = "";

        //    for (int i = 0; i < bt.Length; i++)
        //    {
        //        //recive_str += bt[i].ToString("X2");
        //        recive_str += string.Format("{0} ", bt[i]);
        //    }
        //    recive_str = recive_str.Trim();

        //    Jig01 = recive_str;
        //}
        //private void button3_Click(object sender, EventArgs e)
        //{
        //    byte[] bt = new byte[1024];

        //    bt[0] = 0x45;
        //    bt[1] = 0x31;
        //    bt[2] = 0x31;
        //    bt[3] = 0x53;
        //    bt[4] = 0x31;
        //    bt[5] = 0x30;
        //    string recive_str = "";

        //    for (int i = 0; i < bt.Length; i++)
        //    {
        //        //recive_str += bt[i].ToString("X2");
        //        recive_str += string.Format("{0} ", bt[i]);
        //    }
        //    recive_str = recive_str.Trim();
        //    Jig01 = recive_str;
        //}
        #endregion

        private void labelControl1_Click(object sender, EventArgs e)//패널(받는 데이터) 숨기기/보이기
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
            }
            else
            {
                panel1.Visible = true;
            }
        }

        #region clear
        private void simpleButton4_Click(object sender, EventArgs e)//레이져 컷팅 clear
        {
            list_box_레이져컷팅.Items.Clear();
        }
        private void simpleButton3_Click(object sender, EventArgs e)//클립조립 clear
        {
            list_box_클립조립.Items.Clear();
        }
        private void simpleButton5_Click(object sender, EventArgs e)//초음파1 clear
        {
            list_box_초음파1.Items.Clear();
        }
        private void simpleButton1_Click(object sender, EventArgs e)//진동 clear
        {
            list_box_진동.Items.Clear();
        }
        private void simpleButton2_Click(object sender, EventArgs e)//초음파2 clear
        {
            list_box_초음파2.Items.Clear();
        }
        private void simpleButton6_Click(object sender, EventArgs e)//검사기 clear
        {
            
        }
        private void simpleButton7_Click(object sender, EventArgs e)//op clear
        {
            list_box_OP.Items.Clear();
        }
        #endregion

        #region 데이터 저장
        Dictionary<string, string> EQUIP = new Dictionary<string, string>();
        public void Equip_in_out_insert(string data, string child_lot_no)//설비에서 들어오는데이터 inout 저장
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            string 장비번호 = data.Substring(0, 2);

            string 상태값 = data.Substring(2, 1); // 0:in , 1:out
            string 전송구분 = data.Substring(3, 1);// S:전송 , R:재전송
            string OK_NG = "X"; // 0:OK , 1:NG
            string 검사유무 = "X"; // 0:무 , 1:유
            int 블럭수 = 0;
            string ins_data = "";

            if (상태값.Equals("1"))//out 일때
            {
                OK_NG = data.Substring(4, 1);
                검사유무 = data.Substring(5, 1);
                if (검사유무.Equals("1"))
                {
                    블럭수 = int.Parse(data.Substring(6, 2));

                    ins_data = data.Substring(8, data.Length - 8);


                }

            }
            //IN 일때
            if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                return;
            }
            else if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                EQUIP[장비번호] = "RUN";
            }

            //OUT 일때
            if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                return;
            }
            else if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                EQUIP[장비번호] = "OFF";
            }

            DataTable ins_dt = new DataTable();
            ins_dt.Columns.Add("INS_DATA", typeof(string));
            for (int i = 0; i < 블럭수; i++)
            {
                int ins_data_length = int.Parse(ins_data.Substring(0, 2));
                DataRow dr = ins_dt.NewRow();
                dr["INS_DATA"] = ins_data.Substring(2, ins_data_length);
                ins_dt.Rows.Add(dr);
                ins_data = ins_data.Substring(2 + ins_data_length, ins_data.Length - (2 + ins_data_length));
            }
           
            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@IT_SCODE", "");
            cmd.Parameters.AddWithValue("@OP_CODE", 장비번호);
            cmd.Parameters.AddWithValue("@TOTAL_INS", OK_NG);
            cmd.Parameters.AddWithValue("@IN_OUT_FLAG", 상태값);
            cmd.Parameters.AddWithValue("@TVP", ins_dt);
            cmd.Parameters.AddWithValue("@RSRV_NO", "");
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);


            try
            {

                cmd.ExecuteNonQuery();

                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        public void Equip_in_out_insert_레이져(string data, string child_lot_no,string a_b)//설비에서 들어오는데이터 inout 저장
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_MES_AUTOMATION_IN_OUT_INSERT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            string 장비번호 = "";
            if (a_b.Equals("A")) 
            {
                장비번호 = "A1_A";
            }
            else if (a_b.Equals("B"))
            {
                장비번호 = "A1_B";
            }
                

            string 상태값 = data.Substring(2, 1); // 0:in , 1:out
            string 전송구분 = data.Substring(3, 1);// S:전송 , R:재전송
            string OK_NG = "X"; // 0:OK , 1:NG
            string 검사유무 = "X"; // 0:무 , 1:유
            int 블럭수 = 0;
            string ins_data = "";

            if (상태값.Equals("1"))//out 일때
            {
                OK_NG = data.Substring(4, 1);
                검사유무 = data.Substring(5, 1);
                if (검사유무.Equals("1"))
                {
                    블럭수 = int.Parse(data.Substring(6, 2));

                    ins_data = data.Substring(8, data.Length - 8);


                }

            }
            //IN 일때
            if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                return;
            }
            else if (상태값.Equals("0") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                EQUIP[장비번호] = "RUN";
            }

            //OUT 일때
            if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("OFF"))
            {
                return;
            }
            else if (상태값.Equals("1") && EQUIP[장비번호].ToString().Equals("RUN"))
            {
                EQUIP[장비번호] = "OFF";
            }

            DataTable ins_dt = new DataTable();
            ins_dt.Columns.Add("INS_DATA", typeof(string));
            for (int i = 0; i < 블럭수; i++)
            {
                int ins_data_length = int.Parse(ins_data.Substring(0, 2));
                DataRow dr = ins_dt.NewRow();
                dr["INS_DATA"] = ins_data.Substring(2, ins_data_length);
                ins_dt.Rows.Add(dr);
                ins_data = ins_data.Substring(2 + ins_data_length, ins_data.Length - (2 + ins_data_length));
            }

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@IT_SCODE", "");
            cmd.Parameters.AddWithValue("@OP_CODE", "A1");
            cmd.Parameters.AddWithValue("@TOTAL_INS", OK_NG);
            cmd.Parameters.AddWithValue("@IN_OUT_FLAG", 상태값);
            cmd.Parameters.AddWithValue("@TVP", ins_dt);
            cmd.Parameters.AddWithValue("@RSRV_NO", "");
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);

            
            try
            {

                cmd.ExecuteNonQuery();

                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        private void textEdit1_KeyPress(object sender, KeyPressEventArgs e)//리딩시 OK 일때 READING_DATA INSERT
        {

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {

                if (string.IsNullOrWhiteSpace(auto_it_scode))
                {
                    PpCard_Success.TopLevel = true;
                    PpCard_Success.TopMost = true;
                    PpCard_Success.Visible = true;
                    PpCard_Success.set_text("작업할 생산품이 없습니다.", 5);
                    PpCard_Success.BackColor = Color.Salmon;
                    textEdit1.Text = "";
                    return;
                }
                try
                {

                    string barcode = textEdit1.Text.Trim();
                    string[] arrBarcode = barcode.Split('*');
                    if (arrBarcode.Length.Equals(2))//사출qr코드 유형이 맞는지 체크
                    {
                        string read_it_scode = arrBarcode[0].ToString();
                        string read_lot = arrBarcode[1].ToString();
                        string[] arr_lot = read_lot.Split('/');
                        if (arr_lot.Length.Equals(4))
                        {

                            //리딩된 사출 QR 코드로 레이져공정을 거쳐서 왔는지 체크
                            string laser_it_scode = AUTO_GET_DATA.check_PAB(read_it_scode, read_lot);
                            string check_bom_it_scode = "";
                            //BOM 체크(이종체크)
                            if (!string.IsNullOrWhiteSpace(laser_it_scode))
                            {
                                check_bom_it_scode = laser_it_scode;
                            }
                            else
                            {
                                check_bom_it_scode = read_it_scode;
                            }
                            DataTable dt = AUTO_GET_DATA.bom_chk(auto_it_scode, "Y");//완성품품목으로 bom 정전개
                            DataRow[] dr = dt.Select("IT_SCODE='" + check_bom_it_scode + "'");//리딩된 사출품번이 bom에 있는지 체크
                            if (dr.Length > 0)
                            {
                                /*
                                 * read 사출 qr 코드가 사용 되었는지 체크 NG/진행중/완료 인지 체크 (테이블 : READING_DATA)
                                 * 
                                 * 완료되었으면 메세지 띄워주고 return
                                 * NG : 재투입
                                 * 진행중이면 return
                                */
                                string send_lot = check_save_read_data(auto_it_scode, read_it_scode, read_lot);
                                //MessageBox.Show("완료");
                                //LOT 보내주기
                            }
                            else
                            {
                                PpCard_Success.TopLevel = true;
                                PpCard_Success.TopMost = true;
                                PpCard_Success.Visible = true;
                                PpCard_Success.set_text("해당 제품이 아닙니다.", 5);
                                PpCard_Success.BackColor = Color.Red;

                            }
                        }
                        //사용되지 않았으면 체크하는 테이블에 저장 하면서 카운트
                    }
                    else
                        if (barcode.Substring(0, 2).Equals("PM"))
                        {
                            bool check = false;
                            DataTable dt = AUTO_GET_DATA.bom_chk("", "Y");
                            string child_it_scode = AUTO_GET_DATA.get_pm_child_code(barcode);
                            DataRow[] dr = dt.Select("IT_SCODE='" + child_it_scode + "'");
                            if (dr.Length > 0)
                            {

                                if (AUTO_GET_DATA.get_pm_CHECK(barcode))
                                {


                                    check = AUTO_GET_DATA.move_insert_NEW_pm(barcode, str_wc_code);
                                    if (check)
                                    {
                                        Success_Form.TopLevel = true;
                                        Success_Form.TopMost = true;
                                        Success_Form.Visible = true;
                                        Success_Form.set_text(barcode);
                                        message_timer_clear.Start();
                                    }
                                    else
                                    {
                                        PpCard_Success.TopLevel = true;
                                        PpCard_Success.TopMost = true;
                                        PpCard_Success.Visible = true;
                                        PpCard_Success.set_text("NG", 5);
                                        PpCard_Success.BackColor = Color.Red;

                                    }
                                }
                                else
                                {
                                    PpCard_Success.TopLevel = true;
                                    PpCard_Success.TopMost = true;
                                    PpCard_Success.Visible = true;
                                    PpCard_Success.set_text("완료된 이동표 입니다.", 5);
                                    PpCard_Success.BackColor = Color.Salmon;
                                }
                            }
                            else
                            {
                                PpCard_Success.TopLevel = true;
                                PpCard_Success.TopMost = true;
                                PpCard_Success.Visible = true;
                                PpCard_Success.set_text("해당하는 제품이 아닙니다.", 5);
                                PpCard_Success.BackColor = Color.Salmon;

                            }
                            textEdit1.Text = "";
                        }
                        else
                        {
                            PpCard_Success.TopLevel = true;
                            PpCard_Success.TopMost = true;
                            PpCard_Success.Visible = true;
                            PpCard_Success.set_text("잘못된 이동표 입니다.", 5);
                            PpCard_Success.BackColor = Color.Salmon;
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    textEdit1.Text = "";
                    textEdit1.Focus();
                    
                }
            }
        }
        public string check_save_read_data(string it_scode, string child_it_scode, string child_lot_no)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string send_lot = "";
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_IN_READ_DATA_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            cmd.Parameters.AddWithValue("@RSRV_NO", auto_mo_snumb);
            cmd.Parameters.AddWithValue("@IT_SCODE", auto_it_scode);
            cmd.Parameters.AddWithValue("@CHILD_LOT_NO", child_lot_no);
            cmd.Parameters.AddWithValue("@CHILD_IT_SCODE", child_it_scode);
            cmd.Parameters.AddWithValue("@WC_CODE", str_wc_code);
            cmd.Parameters.AddWithValue("@RE_READING", btn_re_reading.Tag.ToString());

            try
            {
                SqlDataReader reader =  cmd.ExecuteReader();
                while (reader.Read())
                {
                    send_lot = reader["SEND_LOT"].ToString();
                }
                reader.Close();
                reader.Dispose();
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return send_lot;
        }
        //NG 데이터 수신후 완료 구분 주기
        public string NG_DataRecieved_save(string change_lot_no, string 레이져_ok_ng, string 클립_ok_ng, string 초음파1_ok_ng, string 진동_ok_ng, string 초음파2_ok_ng,string ok_ng)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string send_lot = "";
            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_IN_NG_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage);
            //conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@CHANGE_LOT_NO", change_lot_no);
            cmd.Parameters.AddWithValue("@LASER_OK_NG", 레이져_ok_ng);
            cmd.Parameters.AddWithValue("@CLIP_OK_NG", 클립_ok_ng);
            cmd.Parameters.AddWithValue("@CHO_1", 초음파1_ok_ng);
            cmd.Parameters.AddWithValue("@JIN_1", 진동_ok_ng);
            cmd.Parameters.AddWithValue("@CHO_2", 초음파2_ok_ng);
            cmd.Parameters.AddWithValue("@OK_NG", ok_ng);
            

            try
            {
                cmd.ExecuteNonQuery();
                
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return send_lot;
        }
        public void 투입수량증가()
        {
            if (string.IsNullOrWhiteSpace(auto_mo_snumb))
            {
                return;
            }
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_PLAN_JAYB_INTO_SQTY", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_투입수량);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@MO_SNUMB", auto_mo_snumb);

            try
            {
                cmd.ExecuteNonQuery();
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
       

        public void 노즐_투입수량증가()
        {
            
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_PLAN_JAYB_INTO_SQTY_NOZZLE_NEW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_노즐투입수량);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }


        public void 커넥션_투입수량증가()
        {
            
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_PLAN_JAYB_INTO_SQTY_CONECTION_NEW", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_노즐투입수량);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
        }
        #endregion

        private void label47_Click(object sender, EventArgs e)
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
                
                
            }
            else
            {
                panel1.Visible = true;
                panel1.BringToFront();
                
            }
        }

        private void lueWc_code_EditValueChanged(object sender, EventArgs e)
        {
            str_wc_code = lueWc_code.GetColumnValue("WC_CODE").ToString();
            regKey.SetValue("WC_CODE", str_wc_code);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //read_send_timer.Stop();
            read_send_timer_op.Stop();
            
            //winsock_레이져컷팅.Close();
            //winsock_클립조립.Close();

            //clientsock_초음파1.Dispose();
            //clientsock_초음파1.Close();
            //winsock_진동.Close();
            //clientsock_초음파2.Dispose();
            //clientsock_초음파2.Close();
            winsock_OP.Close();
            parent_form.Visible = true;
            this.Close();
        }
        DataTable reading_data = new DataTable();
        string auto_it_scode = "",auto_mo_snumb="";
        string next_it_scode = "",next_mo_snumb="";
        string nozzle_auto_it_scode = "", nozzle_auto_mo_snumb = "";
        string nozzle_next_it_scode = "", nozzle_next_mo_snumb = "";
        string send_spec = "";

        public void auto_it_scode_get()//자동으로 다음품목 가져오기(리딩시 체크를 위해)
        {
            /*자동으로 다음 품목 가져오기*/
            
            //리딩시 체크해야될 작지번호 가져오기
            foreach (DataRow w_dr in work_plan.Rows)
            {
                if (int.Parse(w_dr["PLAN_SQTY"].ToString()) > int.Parse(w_dr["INTO_SQTY"].ToString()))
                {
                    next_it_scode = w_dr["IT_SCODE"].ToString().Trim();
                    next_mo_snumb = w_dr["MO_SNUMB"].ToString().Trim();
                    send_spec = w_dr["SPEC"].ToString().Trim();
                    break;
                }

            }
            if (auto_it_scode != next_it_scode)
            {
                auto_it_scode = next_it_scode;
                
            }
            if (auto_mo_snumb != next_mo_snumb)
            {
                auto_mo_snumb = next_mo_snumb;
            }
            
            
        }

        public void auto_it_scode_get_NOZZLE()//자동으로 다음품목 가져오기(사양 자동전송)
        {
            /*자동으로 다음 품목 가져오기*/
            
            //리딩시 체크해야될 작지번호 가져오기
            foreach (DataRow w_dr in work_plan.Rows)
            {
                if (int.Parse(w_dr["PLAN_SQTY"].ToString()) > int.Parse(w_dr["NOZZLE_CNT"].ToString()))
                {
                    nozzle_next_it_scode = w_dr["IT_SCODE"].ToString().Trim();
                    nozzle_next_mo_snumb = w_dr["MO_SNUMB"].ToString().Trim();
            
                    break;
                }

            }
            if (nozzle_auto_it_scode != nozzle_next_it_scode)
            {
                nozzle_auto_it_scode = nozzle_next_it_scode;
                
            }
            if (nozzle_auto_mo_snumb != nozzle_next_mo_snumb)
            {
                nozzle_auto_mo_snumb = nozzle_next_mo_snumb;
            }
            
        }
        public string str2hex(string strData)
        {
            string resultHex = string.Empty;
            byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

            foreach (byte byteStr in arr_byteStr)
                resultHex += string.Format("{0:X2}", byteStr);

            return resultHex;
        }
        

        private void conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            DataRow drr = reading_data.NewRow();
            auto_it_scode_get();
            if (e.Message.Trim().Substring(0,2).Equals("OK"))
            {
                if (winsock_OP.GetState.ToString() != "Connected")
                {
                    winsock1Connect_OP();
                }
                string send_lot_no = "";
                send_lot_no = e.Message.Trim().Substring(e.Message.Trim().IndexOf("/") + 1, e.Message.Trim().Length - 3);
                if (reading_data.Rows.Count > 10)
                {
                    reading_data.Rows.Clear();
                }
                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "OK";
                //사양 보내주기
                string send_spec_re = "";
                if (send_lot_no.Substring(0, 2).Equals("99"))
                {
                    send_spec_re = "04";
                }
                else
                {
                    send_spec_re = send_lot_no.Substring(0, 2);
                }

                write_send_OP(send_spec_re, send_lot_no);

                Thread.Sleep(100);

                write_send_Lot_Send_data(send_lot_no);

                Thread.Sleep(100);
                //OK 데이터 보내주기
                write_send_OP_reading_OK(send_lot_no);
                
                //투입수량 증가
                if (btn_re_reading.Tag.ToString().Trim().Equals("OFF"))
                {
                    투입수량증가();
                }
                
                reading_data.Rows.Add(drr);
                auto_it_scode_get();
                

                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text("OK", 5);
                PpCard_Success.BackColor = Color.RoyalBlue;
                btn_re_reading.Appearance.BackColor = Color.LightCoral;
                btn_re_reading.Tag = "OFF";
                btn_re_reading.Text = "재리딩OFF";
            }
            else
            {
                drr["READING_DATA"] = textEdit1.Text.Trim();
                drr["S_DATE"] = DateTime.Now.ToString("yyyyMMdd HH:mm:ss:fff");
                drr["RESULT"] = "NG";
                
                reading_data.Rows.Add(drr);
                auto_it_scode_get();
                
                PpCard_Success.TopLevel = true;
                PpCard_Success.TopMost = true;
                PpCard_Success.Visible = true;
                PpCard_Success.set_text(e.Message, 5);
                PpCard_Success.BackColor = Color.Salmon;
                btn_re_reading.Appearance.BackColor = Color.LightCoral;
                btn_re_reading.Tag = "OFF";
                btn_re_reading.Text = "재리딩OFF";
            }
        }
        
        private void conn_InfoMessage_투입수량(object sender, SqlInfoMessageEventArgs e)
        { 
            
            DataRow[] dr = work_plan.Select("MO_SNUMB='"+auto_mo_snumb+"'");
            int update_row_index = work_plan.Rows.IndexOf(dr[0]);
            gridView1.SetRowCellValue(update_row_index, INTO_SQTY, e.Message);
        
        }
        private void conn_InfoMessage_노즐투입수량(object sender, SqlInfoMessageEventArgs e)
        {

            DataRow[] dr = work_plan.Select("MO_SNUMB='" + nozzle_auto_mo_snumb + "'");
            int update_row_index = work_plan.Rows.IndexOf(dr[0]);
            gridView1.SetRowCellValue(update_row_index, NOZZLE_CNT, e.Message);
            auto_it_scode_get_NOZZLE();

        }
        private void message_timer_clear_Tick(object sender, EventArgs e)
        {

            Success_Form.TopLevel = false;
            Success_Form.TopMost = false;
            Success_Form.Visible = false;
            message_timer_clear.Stop();
        }

        private void textEdit1_Enter(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.SkyBlue;
        }

        private void textEdit1_Leave(object sender, EventArgs e)
        {
            textEdit1.BackColor = Color.DarkRed;
        }

        private void btn_po_release_Click(object sender, EventArgs e)
        {            
            work_plan_popup_paint();
        }
        public void work_plan_popup_paint()//작업확정
        {
            if (string.IsNullOrWhiteSpace(lueWc_code.Text.ToString()))
            {
                MessageBox.Show("작업장을 선택해주세요");
                return;
            }
            try
            {
                AUTO_Po_release_new_search AUTO_Po_release_new_search = new AUTO_Po_release_new_search();
                AUTO_Po_release_new_search.wc_group = "PFR";
                AUTO_Po_release_new_search.wc_code = str_wc_code;
                AUTO_Po_release_new_search.car_code = "JA";
                if (AUTO_Po_release_new_search.ShowDialog() == DialogResult.OK)
                {
                    //확정된 작업지시 가져오기
                    po_sdate = AUTO_Po_release_new_search.po_sdate;
                    day_night = AUTO_Po_release_new_search.day_night;
                    work_plan = GET_PLAN_ACCEPT(po_sdate, day_night,"JA");
                    gridControl1.DataSource = work_plan;
                }
                textEdit1.Focus();
                //자동으로 다음생산품 가져오기
                auto_it_scode_get();
                auto_it_scode_get_NOZZLE();
            }
            catch (Exception ex)
            {
                MessageBox.Show("다시 시도해주세요");
            }    
            /*
            else
            {
                MessageBox.Show("리더기를 연결해 주세요");
            }*/
        }
        public DataTable GET_PLAN_ACCEPT(string po_sdate,string day_night,string car_code)//작업확정된거 불러오기
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_AUTO_GET_PLAN";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.Parameters.AddWithValue("REG_DATE", po_sdate);
            da.SelectCommand.Parameters.AddWithValue("DAY_NIGHT", day_night);
            da.SelectCommand.Parameters.AddWithValue("CAR_CODE", car_code);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        private void textEdit2_KeyPress(object sender, KeyPressEventArgs e)//TEST
        {
            //if (e.KeyChar == Convert.ToChar(Keys.Enter))
            //{
            //    check_save_read_data(auto_it_scode, textEdit2.Text, textEdit2.Text);
            //}

        }

        private void TEST_LASER_in_Click(object sender, EventArgs e)
        {
            string equip_laser_str_in = "A10S";
            string equip_clip_str_in = "B10S";
            string equip_초음파1_str_in = "C10S";
            string equip_초음파2_str_in = "C20S";
            string equip_진동1_str_in = "E10S";
            
            string equip_검사_str_in = "F10S";

            if (sender == TEST_LASER_in_A)
            {
                //insert into 
                Equip_in_out_insert(equip_laser_str_in, "");

            }
            else if (sender == TEST_CLIP_in)
            {
                Equip_in_out_insert(equip_clip_str_in, "");
            }
            else if (sender == TEXT_초음파1_in)
            {
                Equip_in_out_insert(equip_초음파1_str_in, "");
            }
            else if (sender == TEXT_진동1_in)
            {
                Equip_in_out_insert(equip_진동1_str_in, "");
            }
            else if (sender == TEST_초음파2_in)
            {
                Equip_in_out_insert(equip_초음파2_str_in, "");
            }
            else if (sender == TEST_검사_in)
            {
                Equip_in_out_insert(equip_검사_str_in, "");
            }
        }

        private void TEST_LASER_out_Click(object sender, EventArgs e)
        {

            string equip_laser_str_out = "A11S00";
            //equip_laser_str_out = "A11S010305#01OK05#02OK07#0310.1";
            string equip_clip_str_out = "B11S10";
            string equip_초음파1_str_out = "C11S00";
            string equip_초음파2_str_out = "C21S00";
            string equip_진동1_str_out = "E11S00";
            
            string equip_검사_str_out = "F11S00";

            if (sender == TEST_LASER_out_A)
            {
                //insert into 
                Equip_in_out_insert(equip_laser_str_out, "");
            }
            else if (sender == TEST_CLIP_out)
            {
                Equip_in_out_insert(equip_clip_str_out, "");
            }
            else if (sender == TEXT_초음파1_out)
            {
                Equip_in_out_insert(equip_초음파1_str_out, "");

            }
            else if (sender == TEXT_진동1_out)
            {
                Equip_in_out_insert(equip_진동1_str_out, "");
            }            
            else if (sender == TEST_초음파2_out)
            {
                Equip_in_out_insert(equip_초음파2_str_out, "");
            }
            else if (sender == TEST_검사_out)
            {
                Equip_in_out_insert(equip_검사_str_out, "");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //레이져컷팅_초기화();
            클립_초기화();
            융착_초기화();
            Refresh_timer_레이져컷팅.Enabled = true;
            Refresh_timer_레이져컷팅.Start();

            Refresh_timer_클립조립.Enabled = true;
            Refresh_timer_클립조립.Start();

            Refresh_timer_진동.Enabled = true;
            Refresh_timer_진동.Start();

            //Refresh_timer_검사기.Enabled = true;
            //Refresh_timer_검사기.Start();

            Refresh_timer_OP.Enabled = true;
            Refresh_timer_OP.Start();

            read_send_timer.Start();
        }

        public void 융착_초기화()
        {
            if (winsock_진동.GetState.ToString() != "Connected")
            {
                winsock1Connect_진동융착();
            }
            if (winsock_진동.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }
            byte[] cmd2 = new byte[27];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x12;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x6C;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x03;
            cmd2[20] = 0x00;

            cmd2[21] = 0x45; cmd2[22] = 0x31;
            cmd2[23] = 0x31; cmd2[24] = 0x53;
            cmd2[25] = 0x30; cmd2[26] = 0x30;
            winsock_진동.Send(cmd2);
        }

        public void 레이져컷팅_A면초기화()
        {
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                winsock_레이져컷팅.Connect();
            }
            Thread.Sleep(100);
            
            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;             
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x08;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00; cmd2[22] = 0x00;
            winsock_레이져컷팅.Send(cmd2);
        }
        public void 레이져컷팅_A면1()
        {
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                winsock_레이져컷팅.Connect();
            }
            Thread.Sleep(100);
            
            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x08;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x01; cmd2[22] = 0x00;
            winsock_레이져컷팅.Send(cmd2);
        }

        public void 레이져컷팅_A면2()
        {
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                winsock_레이져컷팅.Connect();
            }
            Thread.Sleep(100);
            
            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x08;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x02; cmd2[22] = 0x00;
            winsock_레이져컷팅.Send(cmd2);
        }
        public void 레이져컷팅_B면초기화()
        {
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                winsock1Connect_레이져컷팅();
            }
            if (winsock_레이져컷팅.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }
            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x09;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00; cmd2[22] = 0x00;
            winsock_레이져컷팅.Send(cmd2);
        }
        public void 클립_초기화()
        {
            if (winsock_클립조립.GetState.ToString() != "Connected")
            {
                winsock1Connect_클립조립();
            }
            if (winsock_클립조립.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }
            byte[] cmd2 = new byte[27];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x12;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x6C;
            cmd2[16] = 0x07;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x03;
            cmd2[20] = 0x00;

            cmd2[21] = 0x42; cmd2[22] = 0x31;
            cmd2[23] = 0x31; cmd2[24] = 0x53;
            cmd2[25] = 0x30; cmd2[26] = 0x30;
            winsock_클립조립.Send(cmd2);
        }

        private void TEST_LASER_in_A_Click(object sender, EventArgs e)
        {
            string equip_laser_str_in = "A10S";
            string equip_laser_str_out = "A11S00";

            if (sender == TEST_LASER_in_A)
            {
                //insert into 
                Equip_in_out_insert_레이져(equip_laser_str_in, "","A");

            }
            if (sender == TEST_LASER_in_B)
            {
                //insert into 
                Equip_in_out_insert_레이져(equip_laser_str_in, "", "B");

            }
            if (sender == TEST_LASER_out_A)
            {
                //insert into 
                Equip_in_out_insert_레이져(equip_laser_str_out, "", "A");

            }
            if (sender == TEST_LASER_out_B)
            {
                //insert into 
                Equip_in_out_insert_레이져(equip_laser_str_out, "", "B");

            }
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            winsock_레이져컷팅.Close();
            Thread.Sleep(100);
            레이져컷팅_A면초기화();
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            winsock_레이져컷팅.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            winsock_레이져컷팅.Close();
            Thread.Sleep(100);
            read_send_레이져컷팅();
        }

        private void read_send_time_2_Tick(object sender, EventArgs e)
        {
            //winsock_레이져컷팅.Close();
            //Thread.Sleep(100);
            //read_send_레이져컷팅();
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            winsock_레이져컷팅.Close();
            Thread.Sleep(100);
            레이져컷팅_A면1();
        }

        private void simpleButton12_Click_1(object sender, EventArgs e)
        {
            winsock_레이져컷팅.Close();
            Thread.Sleep(100);
            레이져컷팅_A면2();
        }

        
        private void receive_data_op(byte[] bt)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_OP_RECEIVE_DATA_SAVE", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.InfoMessage += new SqlInfoMessageEventHandler(conn_InfoMessage_노즐투입수량);
            conn.FireInfoMessageEventOnUserErrors = true;
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@DATA", bt);

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }

        }
        //사양전송 데이터 DB 저장
        private void receive_data_op_SPEC(byte[] bt,string send_lot,string gubn)
        {

            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_OP_RECEIVE_DATA_SAVE_SPEC", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;
            cmd.Parameters.AddWithValue("@SEND_LOT", send_lot);
            cmd.Parameters.AddWithValue("@GUBN", gubn);
            cmd.Parameters.AddWithValue("@DATA", bt);

            try
            {
                cmd.ExecuteNonQuery();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }

        }
        private void read_send_timer_op_Tick(object sender, EventArgs e)
        {
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            //사양1 클리어
            사양1_CLEAR();
            Thread.Sleep(100);
            //사양2 클리어
            사양2_CLEAR();
            Thread.Sleep(100);
            read_send_OP();
        }

        private void btn_re_reading_Click(object sender, EventArgs e)
        {
            if(btn_re_reading.Tag.ToString().Equals("OFF"))
            {
                btn_re_reading.Appearance.BackColor = Color.SkyBlue;
                btn_re_reading.Tag = "ON";
                btn_re_reading.Text = "재리딩ON";

            }
            else if (btn_re_reading.Tag.ToString().Equals("ON"))
            {
                btn_re_reading.Appearance.BackColor = Color.LightCoral;
                btn_re_reading.Tag = "OFF";
                btn_re_reading.Text = "재리딩OFF";
            }
            textEdit1.Focus();
            
        }

        private void alarm_post_send()
        {
            try
            {
                StringBuilder postParams = new StringBuilder();
                postParams.Append("CAR_CODE=JA");
                //postParams.Append("&pw=" + textPW.Text);

                Encoding encoding = Encoding.UTF8;
                byte[] result = encoding.GetBytes(postParams.ToString());

                // 타겟이 되는 웹페이지 URL
                
                string Url = @"http://192.168.101.42:8001/DK_APP_PUSH_SERVER/push_yb.jsp";

                HttpWebRequest wReqFirst = (HttpWebRequest)WebRequest.Create(Url);

                // HttpWebRequest 오브젝트 설정
                wReqFirst.Method = "POST";
                wReqFirst.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                wReqFirst.ContentLength = result.Length;

                Stream postDataStream = wReqFirst.GetRequestStream();
                postDataStream.Write(result, 0, result.Length);

                postDataStream.Close();

                HttpWebResponse wRespFirst = (HttpWebResponse)wReqFirst.GetResponse();

                // Response의 결과를 스트림을 생성합니다.
                Stream respPostStream = wRespFirst.GetResponseStream();
                StreamReader readerPost = new StreamReader(respPostStream, Encoding.Default);

                // 생성한 스트림으로부터 string으로 변환합니다.
                string resultPost = readerPost.ReadToEnd();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            Thread my_alarm = new Thread(()=> alarm_post_send());
            my_alarm.Start();
        }

        public void 사양1_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5131,5132번지 
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0xF6;
            cmd2[16] = 0x13;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;
            cmd2[22] = 0x00;

            winsock_OP.Send(cmd2);
        }
        public void 사양2_CLEAR()//OK/NG 수신후 CLEAR 해주기 D5131,5132번지 
        {
            read_flag = false;
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                winsock1Connect_OP();
            }
            if (winsock_OP.GetState.ToString() != "Connected")
            {
                MessageBox.Show("Not Connected");
                return;
            }


            byte[] cmd2 = new byte[23];
            cmd2[0] = 0x50;
            cmd2[1] = 0x00;

            cmd2[2] = 0x00;

            cmd2[3] = 0xFF;

            cmd2[4] = 0xFF;
            cmd2[5] = 0x03;

            cmd2[6] = 0x00;

            cmd2[7] = 0x0E;
            cmd2[8] = 0x00;

            cmd2[9] = 0x10;
            cmd2[10] = 0x00;

            cmd2[11] = 0x01;
            cmd2[12] = 0x14;

            cmd2[13] = 0x00;
            cmd2[14] = 0x00;

            cmd2[15] = 0x0B;
            cmd2[16] = 0x14;
            cmd2[17] = 0x00;

            cmd2[18] = 0xA8;

            cmd2[19] = 0x01;
            cmd2[20] = 0x00;

            cmd2[21] = 0x00;
            cmd2[22] = 0x00;

            winsock_OP.Send(cmd2);
        }
    }
}
