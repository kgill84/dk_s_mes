﻿using LibUsbDotNet;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet.Main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DK_Tablet.AUTO_JA_YB
{
    delegate void _d_set_nozzle_gridControl1_yb(DataTable dt);
    delegate void _d_set_nozzle_gridControl2_yb(DataTable dt);
    public partial class AUTO_YB_NOZZLE_SEARCH : Form
    {
        public static IDeviceNotifier mUsbDeviceNotifier = DeviceNotifier.OpenDeviceNotifier();
        public static UsbDeviceFinder mUsbDeviceFinder;
        public static UsbDevice mUsbDevice;



        SerialPort mSerialPort;


        [DllImport("uio.dll")]
        private static extern int usb_io_init(int pID);
        [DllImport("uio.dll")]
        private static extern void set_usb_events(int hWnd);
        [DllImport("uio.dll")]
        private static extern void get_usb_input(int lParam, ref USB_INPUT uInput);
        [DllImport("uio.dll")]
        private static extern bool usb_io_output(int pID, int cmd, int io1, int io2, int io3, int io4);
        [DllImport("uio.dll")]
        private static extern bool usb_io_reset(int pID);
        [DllImport("uio.dll")]
        private static extern bool usb_in_request(int pID);
        /// 여기까지 uio.dll을 사용하기 위한 선언부
        public string site_code = "";
        public string str_con = "";

        public int selectID = Convert.ToInt32("261", 16);
        public int loc = 0;
        public int blink = 5;

        MAIN parent_form;
        public AUTO_YB_NOZZLE_SEARCH(MAIN form)
        {
            MessageFilter uioMf = new MessageFilter();
            this.parent_form = form;
            uioMf.UIO_FORM_yb = this;
            CheckForIllegalCrossThreadCalls = false;
            mUsbDeviceNotifier.OnDeviceNotify += OnDeviceNotifyEvent;
            InitializeComponent();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            usb_io_output(selectID, 0, -1, 0, 0, 0);
            usb_io_output(selectID, 0, -2, 0, 0, 0);
            usb_io_output(selectID, 0, -4, 0, 0, 0);
            this.Close();
        }

        public DataTable get_nozzle_cnt_search()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_NOZZLE_CNT_SEARCH_PLAN_YB";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
                      
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        private void AUTO_JA_NOZZLE_SEARCH_Load(object sender, EventArgs e)
        {
            try
            {
                set_usb_events(this.Handle.ToInt32());  // USB로부터 입력 패킷이 수신 되었을 때 WM_INPUT 이벤트가 발생하로록 설정
            }
            catch
            {

            }
            new Thread(new ThreadStart(reflesh_search)).Start();
            
            usb_io_output(selectID, 0, 4, 0, 0, 0);
            getBI(blink, 4);
            labelControl3.Text = (timer1.Interval / 1000).ToString();
            timer2.Start();
            //timer1.Start();

        }
        private void getBI(int blink, int loc)
        {

            usb_io_output(selectID, blink, loc, 0, 0, 0);
        }
        private void OnDeviceNotifyEvent(object sender, DeviceNotifyEventArgs e)
        {

            set_usb_events(this.Handle.ToInt32());

            usb_io_output(selectID, 0, 4, 0, 0, 0);
            getBI(blink, 4);

            if (e.Device != null)
            {

            }

        }

        //USB device를 read 한다

        private void loadUSB()
        {

            try
            {
                //finder 방법은 시리얼, vendorID, productID 등의 여러가지 방법이 있다.
                mUsbDeviceFinder = new UsbDeviceFinder("0001");
                //mUsbDeviceFinder = new UsbDeviceFinder("a5dcbf10-6530-11d2-901f-00c04fb951ed");

                mUsbDevice = UsbDevice.OpenUsbDevice(mUsbDeviceFinder);

                if (mUsbDevice == null)
                {
                    return;
                }
                IUsbDevice wholeUsbDevice = mUsbDevice as IUsbDevice;

                if (!ReferenceEquals(wholeUsbDevice, null))
                {
                    wholeUsbDevice.SetConfiguration(1);
                    wholeUsbDevice.ClaimInterface(0);
                }
                UsbEndpointReader reader = mUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01);

            }
            catch (Exception e)
            {
            }



        }

        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            reflesh_search();
            timer2.Stop();
            timer2.Start();
        }
        private void reflesh_search()
        {
            this.Invoke(new _d_set_nozzle_gridControl1_yb(set_gridcontrol1), new object[] { get_nozzle_cnt_search() });

            this.Invoke(new _d_set_nozzle_gridControl2_yb(set_gridcontrol2), new object[] { get_현재진행() });
            labelControl3.Text = (timer1.Interval / 1000).ToString();
        }
        public void set_gridcontrol1(DataTable dt)
        {
            gridControl1.DataSource = dt;

        }
        public void set_gridcontrol2(DataTable dt)
        {
            gridControl2.DataSource = dt;

        }
        private DataTable get_현재진행()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_NOZZLE_CNT_SEARCH_PLAN_INC_YB";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            //da.SelectCommand.Parameters.AddWithValue("REG_DATE", dateEdit1.DateTime.ToString("yyyyMMdd"));
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "AUTO_PLAN_GET");
                dt = ds.Tables["AUTO_PLAN_GET"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            new Thread(new ThreadStart(reflesh_search)).Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                set_usb_events(this.Handle.ToInt32());  // USB로부터 입력 패킷이 수신 되었을 때 WM_INPUT 이벤트가 발생하로록 설정
                usb_io_output(selectID, 0, 4, 0, 0, 0);
                labelControl3.Text = (int.Parse(labelControl3.Text) - 1).ToString();
                btn_Refresh.Text = "새로고침(" + labelControl3.Text + ")";
                if (labelControl3.Text.Trim().Equals("0"))
                {
                    reflesh_search();
                }
            }
            catch
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                usb_io_output(selectID, 0, -1, 0, 0, 0);
                usb_io_output(selectID, 0, -4, 0, 0, 0);
                getBI(blink, -4);
            }
            catch
            {

            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            usb_io_output(selectID, 0, -4, 0, 0, 0);

            getBI(blink, 1);
            usb_io_output(selectID, 0, 2, 0, 0, 0);
            getBI(blink, 2);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            lbl_now_date.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
    
}
