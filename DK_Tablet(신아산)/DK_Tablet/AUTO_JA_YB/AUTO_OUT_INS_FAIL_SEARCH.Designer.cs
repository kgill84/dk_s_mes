﻿namespace DK_Tablet.AUTO_JA_YB
{
    partial class AUTO_OUT_INS_FAIL_SEARCH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression3 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleExpression formatConditionRuleExpression4 = new DevExpress.XtraEditors.FormatConditionRuleExpression();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn_close = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.Sdate = new DevExpress.XtraEditors.DateEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.advBandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ASSY_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ASSY_BARCODE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PRDT_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.LASER_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CHILD_ITEM = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHILD_LOT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.F1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.F1_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.TOTAL_INS = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FINAL_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.R_DATE = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CHANGE_LOT_NO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.REWORK_GUBN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::DK_Tablet.DisplayForm.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1127, 711);
            this.splitContainerControl1.SplitterPosition = 42;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.btn_close);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1127, 42);
            this.splitContainerControl3.SplitterPosition = 173;
            this.splitContainerControl3.TabIndex = 2;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.labelControl1.Appearance.BackColor2 = System.Drawing.Color.White;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(949, 42);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "검사 이력";
            // 
            // btn_close
            // 
            this.btn_close.Appearance.BackColor = System.Drawing.Color.LemonChiffon;
            this.btn_close.Appearance.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_close.Appearance.Options.UseBackColor = true;
            this.btn_close.Appearance.Options.UseFont = true;
            this.btn_close.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_close.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_close.Location = new System.Drawing.Point(0, 0);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(173, 42);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "닫기";
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.splitContainerControl6);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1127, 664);
            this.splitContainerControl2.SplitterPosition = 42;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Appearance.BackColor = System.Drawing.Color.LavenderBlush;
            this.splitContainerControl6.Appearance.Options.UseBackColor = true;
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.Sdate);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.simpleButton1);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(1127, 42);
            this.splitContainerControl6.SplitterPosition = 183;
            this.splitContainerControl6.TabIndex = 1;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // Sdate
            // 
            this.Sdate.EditValue = null;
            this.Sdate.Location = new System.Drawing.Point(3, 8);
            this.Sdate.Name = "Sdate";
            this.Sdate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sdate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Sdate.Size = new System.Drawing.Size(179, 20);
            this.Sdate.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.LightSteelBlue;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Tahoma", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.simpleButton1.Location = new System.Drawing.Point(0, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(115, 30);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "검색";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.advBandedGridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1127, 617);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.advBandedGridView1});
            // 
            // advBandedGridView1
            // 
            this.advBandedGridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold);
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.advBandedGridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.advBandedGridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.advBandedGridView1.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 14F);
            this.advBandedGridView1.Appearance.Row.Options.UseFont = true;
            this.advBandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand6,
            this.gridBand3,
            this.gridBand13,
            this.gridBand14});
            this.advBandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.R_DATE,
            this.ASSY_ITEM,
            this.ASSY_BARCODE,
            this.CHILD_ITEM,
            this.CHILD_LOT,
            this.CHANGE_LOT_NO,
            this.PRDT_ITEM,
            this.LASER_LOT,
            this.F1,
            this.TOTAL_INS,
            this.F1_GUBN,
            this.FINAL_GUBN,
            this.REWORK_GUBN});
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleExpression3.Appearance.BackColor = System.Drawing.Color.Salmon;
            formatConditionRuleExpression3.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression3.Expression = "[REWORK_GUBN] = \'X\'";
            gridFormatRule3.Rule = formatConditionRuleExpression3;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Name = "Format1";
            formatConditionRuleExpression4.Appearance.BackColor = System.Drawing.Color.YellowGreen;
            formatConditionRuleExpression4.Appearance.Options.UseBackColor = true;
            formatConditionRuleExpression4.Expression = "[REWORK_GUBN] = \'Y\' And ([FINAL_GUBN] = \'재작업\' Or [A1_GUBN] = \'재작업\' Or [B1_GUBN] =" +
    " \'재작업\' Or [C1_GUBN] = \'재작업\' Or [C2_GUBN] = \'재작업\' Or [D1_GUBN] = \'재작업\' Or [E1_GUB" +
    "N] = \'재작업\' Or [F1_GUBN] = \'재검사\')";
            gridFormatRule4.Rule = formatConditionRuleExpression4;
            this.advBandedGridView1.FormatRules.Add(gridFormatRule3);
            this.advBandedGridView1.FormatRules.Add(gridFormatRule4);
            this.advBandedGridView1.GridControl = this.gridControl1;
            this.advBandedGridView1.Name = "advBandedGridView1";
            this.advBandedGridView1.OptionsBehavior.ReadOnly = true;
            this.advBandedGridView1.OptionsView.ColumnAutoWidth = true;
            this.advBandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.advBandedGridView1.OptionsView.ShowBands = false;
            this.advBandedGridView1.OptionsView.ShowGroupPanel = false;
            this.advBandedGridView1.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.advBandedGridView1_MouseWheel);
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Columns.Add(this.ASSY_ITEM);
            this.gridBand2.Columns.Add(this.ASSY_BARCODE);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 185;
            // 
            // ASSY_ITEM
            // 
            this.ASSY_ITEM.Caption = "완성품 품번";
            this.ASSY_ITEM.FieldName = "ASSY_ITEM";
            this.ASSY_ITEM.Name = "ASSY_ITEM";
            this.ASSY_ITEM.OptionsColumn.AllowEdit = false;
            this.ASSY_ITEM.OptionsColumn.ReadOnly = true;
            this.ASSY_ITEM.Visible = true;
            this.ASSY_ITEM.Width = 185;
            // 
            // ASSY_BARCODE
            // 
            this.ASSY_BARCODE.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 12F);
            this.ASSY_BARCODE.AppearanceCell.Options.UseFont = true;
            this.ASSY_BARCODE.Caption = "완제품로트";
            this.ASSY_BARCODE.FieldName = "ASSY_BARCODE";
            this.ASSY_BARCODE.Name = "ASSY_BARCODE";
            this.ASSY_BARCODE.RowIndex = 1;
            this.ASSY_BARCODE.Visible = true;
            this.ASSY_BARCODE.Width = 185;
            // 
            // gridBand6
            // 
            this.gridBand6.Caption = "gridBand6";
            this.gridBand6.Columns.Add(this.PRDT_ITEM);
            this.gridBand6.Columns.Add(this.LASER_LOT);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 199;
            // 
            // PRDT_ITEM
            // 
            this.PRDT_ITEM.Caption = "레이저 품번";
            this.PRDT_ITEM.FieldName = "PRDT_ITEM";
            this.PRDT_ITEM.Name = "PRDT_ITEM";
            this.PRDT_ITEM.Visible = true;
            this.PRDT_ITEM.Width = 199;
            // 
            // LASER_LOT
            // 
            this.LASER_LOT.Caption = "레이저로트";
            this.LASER_LOT.FieldName = "LASER_LOT";
            this.LASER_LOT.Name = "LASER_LOT";
            this.LASER_LOT.RowIndex = 1;
            this.LASER_LOT.Visible = true;
            this.LASER_LOT.Width = 199;
            // 
            // gridBand3
            // 
            this.gridBand3.Caption = "gridBand3";
            this.gridBand3.Columns.Add(this.CHILD_ITEM);
            this.gridBand3.Columns.Add(this.CHILD_LOT);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 192;
            // 
            // CHILD_ITEM
            // 
            this.CHILD_ITEM.Caption = "사출품";
            this.CHILD_ITEM.FieldName = "CHILD_ITEM";
            this.CHILD_ITEM.Name = "CHILD_ITEM";
            this.CHILD_ITEM.OptionsColumn.ReadOnly = true;
            this.CHILD_ITEM.Visible = true;
            this.CHILD_ITEM.Width = 192;
            // 
            // CHILD_LOT
            // 
            this.CHILD_LOT.Caption = "사출LOT";
            this.CHILD_LOT.FieldName = "CHILD_LOT";
            this.CHILD_LOT.Name = "CHILD_LOT";
            this.CHILD_LOT.OptionsColumn.ReadOnly = true;
            this.CHILD_LOT.RowIndex = 1;
            this.CHILD_LOT.Visible = true;
            this.CHILD_LOT.Width = 192;
            // 
            // gridBand13
            // 
            this.gridBand13.Caption = "gridBand13";
            this.gridBand13.Columns.Add(this.F1);
            this.gridBand13.Columns.Add(this.F1_GUBN);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 3;
            this.gridBand13.Width = 134;
            // 
            // F1
            // 
            this.F1.AppearanceCell.Options.UseTextOptions = true;
            this.F1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.F1.Caption = "검사";
            this.F1.FieldName = "F1";
            this.F1.Name = "F1";
            this.F1.Visible = true;
            this.F1.Width = 134;
            // 
            // F1_GUBN
            // 
            this.F1_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.F1_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.F1_GUBN.Caption = "처리구분F1";
            this.F1_GUBN.FieldName = "F1_GUBN";
            this.F1_GUBN.Name = "F1_GUBN";
            this.F1_GUBN.RowIndex = 1;
            this.F1_GUBN.Visible = true;
            this.F1_GUBN.Width = 134;
            // 
            // gridBand14
            // 
            this.gridBand14.Caption = "gridBand14";
            this.gridBand14.Columns.Add(this.TOTAL_INS);
            this.gridBand14.Columns.Add(this.FINAL_GUBN);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 4;
            this.gridBand14.Width = 120;
            // 
            // TOTAL_INS
            // 
            this.TOTAL_INS.AppearanceCell.Options.UseTextOptions = true;
            this.TOTAL_INS.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TOTAL_INS.Caption = "최종";
            this.TOTAL_INS.FieldName = "TOTAL_INS";
            this.TOTAL_INS.Name = "TOTAL_INS";
            this.TOTAL_INS.Visible = true;
            this.TOTAL_INS.Width = 120;
            // 
            // FINAL_GUBN
            // 
            this.FINAL_GUBN.AppearanceCell.Options.UseTextOptions = true;
            this.FINAL_GUBN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FINAL_GUBN.Caption = "처리구분";
            this.FINAL_GUBN.FieldName = "FINAL_GUBN";
            this.FINAL_GUBN.Name = "FINAL_GUBN";
            this.FINAL_GUBN.RowIndex = 1;
            this.FINAL_GUBN.Visible = true;
            this.FINAL_GUBN.Width = 120;
            // 
            // R_DATE
            // 
            this.R_DATE.Caption = "불량발생일";
            this.R_DATE.FieldName = "R_DATE";
            this.R_DATE.Name = "R_DATE";
            this.R_DATE.OptionsColumn.AllowEdit = false;
            this.R_DATE.OptionsColumn.ReadOnly = true;
            // 
            // CHANGE_LOT_NO
            // 
            this.CHANGE_LOT_NO.Caption = "사출변환LOT";
            this.CHANGE_LOT_NO.FieldName = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Name = "CHANGE_LOT_NO";
            this.CHANGE_LOT_NO.Visible = true;
            // 
            // REWORK_GUBN
            // 
            this.REWORK_GUBN.Caption = "재작업구분";
            this.REWORK_GUBN.FieldName = "REWORK_GUBN";
            this.REWORK_GUBN.Name = "REWORK_GUBN";
            // 
            // repositoryItemButtonEdit1
            // 
            serializableAppearanceObject2.BackColor = System.Drawing.Color.LightBlue;
            serializableAppearanceObject2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            serializableAppearanceObject2.Options.UseBackColor = true;
            serializableAppearanceObject2.Options.UseFont = true;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "불량처리", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.repositoryItemButtonEdit1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.ReadOnly = true;
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // AUTO_OUT_INS_FAIL_SEARCH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1127, 711);
            this.Controls.Add(this.splitContainerControl1);
            this.Name = "AUTO_OUT_INS_FAIL_SEARCH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "검사이력";
            this.Load += new System.EventHandler(this.AUTO_FAIL_REG_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Sdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.advBandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.SimpleButton btn_close;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.DateEdit Sdate;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView advBandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ASSY_BARCODE;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PRDT_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LASER_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_ITEM;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHILD_LOT;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn F1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn F1_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TOTAL_INS;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FINAL_GUBN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn R_DATE;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CHANGE_LOT_NO;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn REWORK_GUBN;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
    }
}