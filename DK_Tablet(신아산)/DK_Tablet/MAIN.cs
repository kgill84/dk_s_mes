﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DK_Tablet.FUNCTION;
using Microsoft.Win32;
using DevExpress.XtraEditors;
using System.Runtime.InteropServices;
using DK_Tablet.AUTO_JA_YB;
namespace DK_Tablet
{
    public partial class MAIN : Form
    {

        [DllImport("Sensapi.dll")]
        private static extern int IsNetworkAlive(ref int dwFlags);
        private int NETWORK_LAN = 0X01;
        private int NETWORK_WAN = 0X02;
        private int NETWORK_AOL = 0X04;
        GET_DATA GET_DATA = new GET_DATA();
        
        Paint_In_Out_Select Paint_In_Out_Select = new DK_Tablet.Paint_In_Out_Select();
        Paint_In_Out_Select_2 Paint_In_Out_Select_2 = new Paint_In_Out_Select_2();
        AUTO_display_Select AUTO_display_Select = new AUTO_display_Select();
        RegistryKey regKey = Registry.CurrentUser.CreateSubKey("DONGUKTablet", RegistryKeyPermissionCheck.ReadWriteSubTree);
        [DllImport("Kernel32.dll")]
        public static extern void SetSystemTime([In] SystemTime st);


        [StructLayout(LayoutKind.Sequential)]
        public class SystemTime
        {
            public ushort year;
            public ushort month;
            public ushort dayofweek;
            public ushort day;
            public ushort hour;
            public ushort minute;
            public ushort second;
            public ushort milliseconds;
        }
        public MAIN()
        {
            InitializeComponent();
        }

        private void btn_main_injection_Click(object sender, EventArgs e)
        {
            

            //사출            
            
                Button btn = (Button)sender;
                Injection  Injection = new Injection(this);
                Injection.wc_group = "PIM01";
                Injection.Show();

                //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
                
                    
        }

        private void btn_main_paint_Click(object sender, EventArgs e)
        {
            //도장

            if (Paint_In_Out_Select.ShowDialog() == DialogResult.OK)
            {
                if (Paint_In_Out_Select.select_in_out.Equals("IN"))
                {
                    
                    Paint_In Paint_In = new Paint_In(this);
                    Paint_In.wc_group = "PDJ";
                    Paint_In.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PDJ", RegistryValueKind.String);
                    
                }
                else if (Paint_In_Out_Select.select_in_out.Equals("OUT"))
                {
                                 
                    Paint_Out Paint_Out = new Paint_Out(this);
                    Paint_Out.wc_group = "PDJ";
                    Paint_Out.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PDJ", RegistryValueKind.String);
                                   
                }
            }

        }

        private void btn_main_assey_Click(object sender, EventArgs e)
        {
            //조립

            if (Paint_In_Out_Select.ShowDialog() == DialogResult.OK)
            {
                if (Paint_In_Out_Select.select_in_out.Equals("IN"))
                {
                                     
                    Paint_In Paint_In = new Paint_In(this);
                    Paint_In.wc_group = "PAR";
                    Paint_In.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PAR", RegistryValueKind.String);
                                   
                }
                else if (Paint_In_Out_Select.select_in_out.Equals("OUT"))
                {
                                    
                    Paint_Out Paint_Out = new Paint_Out(this);
                    Paint_Out.wc_group = "PAR";
                    Paint_Out.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PAR", RegistryValueKind.String);
                                     
                }
            }

        }


        private void btn_main_laser_Click(object sender, EventArgs e)
        {
            
            //레이저
            
                Button btn = (Button)sender;
                SubMain SubMain = new SubMain(this);
                SubMain.wc_group = "PLR";
                SubMain.Show();
                regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
                        
        }

        private void btn_main_assey_c_Click(object sender, EventArgs e)
        {

            if (Paint_In_Out_Select_2.ShowDialog() == DialogResult.OK)
            {
                if (Paint_In_Out_Select_2.select_in_out.Equals("IN_QR"))
                {

                    Work_Center_In Work_Center_In = new Work_Center_In(this);
                    Work_Center_In.wc_group = "PFR";
                    Work_Center_In.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PFR", RegistryValueKind.String);

                }
                else if (Paint_In_Out_Select_2.select_in_out.Equals("IN_PP"))
                {
                                        
                    Paint_In Paint_In = new Paint_In(this);
                    Paint_In.wc_group = "PFR";
                    Paint_In.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PFR", RegistryValueKind.String);
                                     
                }
                else if (Paint_In_Out_Select_2.select_in_out.Equals("OUT"))
                {
                                   
                    Paint_Out Paint_Out = new Paint_Out(this);
                    Paint_Out.wc_group = "PFR";
                    Paint_Out.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);
                                      
                }
            }

        }

        private void MAIN_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("프로그램이 종료 됩니다.","알림", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes) {
                this.Close();
            }
            
        }

        private void btn_main_metarial_move_Click(object sender, EventArgs e)
        {
            
            /*Button btn = (Button)sender;
            Metarial_Move_TEST Metarial_Move_TEST = new Metarial_Move_TEST(this);
            Metarial_Move_TEST.wc_group = "%";
            Metarial_Move_TEST.Show();*/
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
            Button btn = (Button)sender;
            Metarial_Move_2 Metarial_Move_2 = new Metarial_Move_2(this);
            Metarial_Move_2.wc_group = "%";
            Metarial_Move_2.Show();
        }

        private void btn_inspection_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Inspection Inspection = new Inspection(this);
            Inspection.Show();
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
        }

        private void btn_main_Instatement_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Instatement Instatement = new Instatement(this);
            Instatement.Show();
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
        }
        private void btn_injection_carrier_Click(object sender, EventArgs e)
        {
            
                
                Button btn = (Button)sender;
                Injection_carrier Injection_carrier = new Injection_carrier(this);
                Injection_carrier.wc_group = "PIM01";
                Injection_carrier.Show();

                //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
                   
        }
        public void timesync(string LSysdate)
        {            
            if (!string.IsNullOrWhiteSpace(LSysdate))
            {
                string l_datetime = DateTime.Parse(LSysdate).AddHours(-9).ToString("yyyy-MM-dd HH:mm:ss");

                DateTime sysTime = new DateTime(Convert.ToInt32(l_datetime.Substring(0, 4).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(5, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(8, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(11, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(14, 2).ToString()),
                                                Convert.ToInt32(l_datetime.Substring(17, 2).ToString()));

                SystemTime sTime = new SystemTime();
                sTime.year = Convert.ToUInt16(sysTime.Year);
                sTime.month = Convert.ToUInt16(sysTime.Month);
                sTime.dayofweek = Convert.ToUInt16(sysTime.DayOfWeek);
                sTime.day = Convert.ToUInt16(sysTime.Day);
                sTime.hour = Convert.ToUInt16(sysTime.Hour);
                sTime.minute = Convert.ToUInt16(sysTime.Minute);
                sTime.second = Convert.ToUInt16(sysTime.Second);
                sTime.milliseconds = Convert.ToUInt16(sysTime.Millisecond);

                
                SetSystemTime(sTime);
            }
        }
        private void MAIN_Load(object sender, EventArgs e)
        {

            timer1.Start();
            
            tableLayoutPanel_main.Dock = DockStyle.Fill;
            tableLayoutPanel_production.Dock = DockStyle.Fill;
            tableLayoutPanel_logistics.Dock = DockStyle.Fill;
            tableLayoutPanel_shipment.Dock = DockStyle.Fill;
            tableLayoutPanel_quality.Dock = DockStyle.Fill;
            tableLayoutPanel_main.Visible = true;
            tableLayoutPanel_production.Visible = false;
            tableLayoutPanel_logistics.Visible = false;
            tableLayoutPanel_shipment.Visible = false;
            tableLayoutPanel_quality.Visible = false;

            if (INTERNET_CHECK.IsConnectedToInternet())
            {
                timesync(GET_DATA.maxTimeSync());
            }
            else
            {
                MessageBox.Show("인터넷이 불안정 합니다. 연결 상태를 체크 해주세요");
            }
            /*
            try
            {
                if (regKey.GetValue("MAIN") == null)
                {
                    regKey.SetValue("MAIN", "");
                }


                if (regKey.GetValue("WC_GROUP") == null)
                {
                    regKey.SetValue("WC_GROUP", "");
                }

                if (regKey != null)
                {
                    // 체크 키 없으면 생성
                    if (regKey.GetValue("WC_CODE") == null)
                    {
                        regKey.SetValue("WC_CODE", "");
                    }
                    if (regKey.GetValue("COM_PORT") == null)
                    {
                        regKey.SetValue("COM_PORT", "");
                    }
                    if (regKey.GetValue("MAIN").ToString() != "")
                    {
                        if (regKey.GetValue("MAIN").ToString() == "btn_production")
                        {
                            btn_production.PerformClick();
                        }
                        else if (regKey.GetValue("MAIN").ToString() == "btn_inlogistics")
                        {
                            btn_inlogistics.PerformClick();
                        }
                        else if (regKey.GetValue("MAIN").ToString() == "btn_shipment")
                        {
                            btn_shipment.PerformClick();
                        }
                    }
                    // 체크 키 N 이면
                    if (regKey.GetValue("WC_GROUP").ToString() != "")
                    {

                        if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_injection")
                        {
                            btn_main_injection.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_paint")
                        {
                            btn_main_paint.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_assey")
                        {
                            btn_main_assey.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_laser")
                        {
                            btn_main_laser.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_assey_c")
                        {
                            btn_main_assey_c.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_assey_c_2")
                        {
                            btn_main_assey_c_2.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_Instatement")
                        {
                            btn_main_Instatement.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_metarial_move")
                        {
                            btn_main_metarial_move.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_main_shipment")
                        {
                            btn_main_shipment.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_inspection")
                        {
                            btn_inspection.PerformClick();
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_In_PDJ")
                        {
                            Paint_In Paint_In = new Paint_In(this);
                            Paint_In.wc_group = "PDJ";
                            Paint_In.Show();
                            regKey.SetValue("WC_GROUP", "btn_In_PDJ", RegistryValueKind.String);
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_Out_PDJ")
                        {
                            Paint_Out Paint_Out = new Paint_Out(this);
                            Paint_Out.wc_group = "PDJ";
                            Paint_Out.Show();
                            regKey.SetValue("WC_GROUP", "btn_Out_PDJ", RegistryValueKind.String);
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_In_PFR")
                        {
                            Paint_In Paint_In = new Paint_In(this);
                            Paint_In.wc_group = "PFR";
                            Paint_In.Show();
                            regKey.SetValue("WC_GROUP", "btn_In_PFR", RegistryValueKind.String);
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_Out_PFR")
                        {
                            Paint_Out Paint_Out = new Paint_Out(this);
                            Paint_Out.wc_group = "PFR";
                            Paint_Out.Show();
                            regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_In_PAR")
                        {
                            Paint_In Paint_In = new Paint_In(this);
                            Paint_In.wc_group = "PAR";
                            Paint_In.Show();
                            regKey.SetValue("WC_GROUP", "btn_In_PAR", RegistryValueKind.String);
                        }
                        else if (regKey.GetValue("WC_GROUP").ToString() == "btn_Out_PAR")
                        {
                            Paint_Out Paint_Out = new Paint_Out(this);
                            Paint_Out.wc_group = "PAR";
                            Paint_Out.Show();
                            regKey.SetValue("WC_GROUP", "btn_Out_PAR", RegistryValueKind.String);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
             */
        }

        private void btn_production_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Name == "btn_production")
            {
                tableLayoutPanel_main.Visible = false;
                tableLayoutPanel_production.Visible = true;
                tableLayoutPanel_logistics.Visible = false;
                tableLayoutPanel_shipment.Visible = false;
                tableLayoutPanel_quality.Visible = false;

            }
            else if (btn.Name == "btn_inlogistics")
            {
                tableLayoutPanel_main.Visible = false;
                tableLayoutPanel_production.Visible = false;
                tableLayoutPanel_logistics.Visible = true;
                tableLayoutPanel_shipment.Visible = false;
                tableLayoutPanel_quality.Visible = false;
            }
            else if (btn.Name == "btn_shipment")
            {
                tableLayoutPanel_main.Visible = false;
                tableLayoutPanel_production.Visible = false;
                tableLayoutPanel_logistics.Visible = false;
                tableLayoutPanel_shipment.Visible = true;
                tableLayoutPanel_quality.Visible = false;
            }
            else if (btn.Name == "btn_quality")
            {
                tableLayoutPanel_main.Visible = false;
                tableLayoutPanel_production.Visible = false;
                tableLayoutPanel_logistics.Visible = false;
                tableLayoutPanel_shipment.Visible = false;
                tableLayoutPanel_quality.Visible = true;
            }
            btn_back.Visible = true;
            regKey.SetValue("MAIN", btn.Name, RegistryValueKind.String);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tableLayoutPanel_main.Visible = true;
            tableLayoutPanel_production.Visible = false;
            tableLayoutPanel_logistics.Visible = false;
            tableLayoutPanel_shipment.Visible = false;
            tableLayoutPanel_quality.Visible = false;
            btn_back.Visible = false;
        }

        private void btn_main_shipment_Click(object sender, EventArgs e)
        {            
            Button btn = (Button)sender;
            //Shipment Shipment = new Shipment(this);
            //Shipment.wc_group = "%";
            //Shipment.Show();
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);            
        }

        private void btn_main_assey_c_2_Click(object sender, EventArgs e)
        {
            
                Button btn = (Button)sender;
                SubMain_Assy SubMain_Assy = new SubMain_Assy(this);
                SubMain_Assy.wc_group = "PFR";
                SubMain_Assy.Show();
                //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
                      
        }

        private void btn_Loading_Click(object sender, EventArgs e)
        {
            /*if (INTERNET_CHECK.IsConnectedToInternet())
            {

                Loading Loading = new Loading(this);

                Loading.Show();

            }
            else
            {
                MessageBox.Show("인터넷이 불안정 합니다. 연결 상태를 체크 해주세요");
            }*/
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!IsNetConnection(ref NETWORK_LAN))
            {
                timer1.Stop();
                DialogResult result = MyMessageBox.ShowBox("인터넷이 불안정 합니다.", "알림 !!!!!!!!",5);
                if (result == DialogResult.Retry)
                {
                    timer1.Start();
                }
                else if (result == DialogResult.Cancel)
                {
                    this.Close();
                }
            }            
        }

        private bool IsNetConnection(ref int dwFlags)
        {            
            bool _int_status = Convert.ToBoolean(IsNetworkAlive(ref dwFlags));
            return _int_status;
        }


        private void btn_main_assey_c_3_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            SubMain_Y SubMain_Y = new SubMain_Y(this);
            SubMain_Y.wc_group = "PFR";
            SubMain_Y.Show();
        }

        private void btn_main_laser_hi_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //SubMain_L SubMain_L = new SubMain_L(this);
            //SubMain_L.wc_group = "PLR";
            //SubMain_L.Show();
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
            SubMain_L SubMain_L = new SubMain_L(this);
            SubMain_L.wc_group = "PLR";
            SubMain_L.Show();
            regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            /*Shipment_식별표 Shipment_식별표 = new Shipment_식별표(this);
            Shipment_식별표.wc_group = "%";
            Shipment_식별표.Show();*/
            Shipment_New Shipment_New = new Shipment_New(this);
            Shipment_New.wc_group = "%";
            Shipment_New.Show();
        }

        private void btn_FME_InsResult_Click(object sender, EventArgs e)
        {
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(this);
            FME_InsResultReg.Show();
        }


        private void btn_injection_7_Click(object sender, EventArgs e)
        {
            injection_7 injection_7 = new injection_7(this);
            injection_7.wc_group = "PIM01";
            injection_7.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_main_site_move_Click(object sender, EventArgs e)
        {
            Site_Move Site_Move = new Site_Move(this);
            Site_Move.wc_group = "%";
            Site_Move.Show();
        }

        private void btn_main_laser_JA_YB_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            //SubMain_L SubMain_L = new SubMain_L(this);
            //SubMain_L.wc_group = "PLR";
            //SubMain_L.Show();
            //regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
            Laser_JA_YB Laser_JA_YB = new Laser_JA_YB(this);
            Laser_JA_YB.wc_group = "PLR";
            Laser_JA_YB.Show();
            regKey.SetValue("WC_GROUP", btn.Name, RegistryValueKind.String);
        }

        private void btn_main_assey_JA_YB_Click(object sender, EventArgs e)
        {
            //AUTO_display_Select
            if (AUTO_display_Select.ShowDialog() == DialogResult.OK)
            {
                if (AUTO_display_Select.select_in_out.Equals("IN"))
                {

                    AUTO_JA_IN AUTO_JA_IN = new AUTO_JA_IN(this);
                    AUTO_JA_IN.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("OUT"))
                {

                    AUTO_JA_OUT AUTO_JA_OUT = new AUTO_JA_OUT(this);
                    AUTO_JA_OUT.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("DISPLAY"))
                {

                    DASHBOARD_JA_NEW_2 DASHBOARD_JA_NEW_2 = new DASHBOARD_JA_NEW_2(this);
                    DASHBOARD_JA_NEW_2.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("FAIL_INPUT"))
                {

                    AUTO_FAIL_REG AUTO_FAIL_REG = new AUTO_FAIL_REG(this);
                    AUTO_FAIL_REG.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("REWORK"))
                {

                    AUTO_REWORK_REG AUTO_REWORK_REG = new AUTO_REWORK_REG(this);
                    AUTO_REWORK_REG.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);
                    
                }
                else if (AUTO_display_Select.select_in_out.Equals("NOZZLE"))
                {

                    AUTO_JA_NOZZLE_SEARCH AUTO_JA_NOZZLE_SEARCH = new AUTO_JA_NOZZLE_SEARCH(this);
                    AUTO_JA_NOZZLE_SEARCH.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);
                    
                }
                else if (AUTO_display_Select.select_in_out.Equals("CONNECTION"))
                {

                    AUTO_JA_CONNECTION_SEARCH AUTO_JA_CONNECTION_SEARCH = new AUTO_JA_CONNECTION_SEARCH(this);
                    AUTO_JA_CONNECTION_SEARCH.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);
                  
                }
            }
        }

        private void btn_main_injection_Lot_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Injection_Lot Injection_Lot = new Injection_Lot(this);
            Injection_Lot.wc_group = "PIM01";
            Injection_Lot.Show();
        }

        private void btn_FME_InsResult_Click_1(object sender, EventArgs e)
        {
            FME_InsResultReg FME_InsResultReg = new FME_InsResultReg(this);
            FME_InsResultReg.Show();
        }

        private void tableLayoutPanel_quality_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_fail_Click(object sender, EventArgs e)
        {
            Fail_Receip Fail_Receip = new Fail_Receip(this);
            Fail_Receip.Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Fail_input_qr Fail_input_qr = new Fail_input_qr();
            Fail_input_qr.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //AUTO_display_Select
            if (AUTO_display_Select.ShowDialog() == DialogResult.OK)
            {
                if (AUTO_display_Select.select_in_out.Equals("IN"))
                {

                    AUTO_YB_IN AUTO_YB_IN = new AUTO_YB_IN(this);
                    AUTO_YB_IN.Show();
                    //regKey.SetValue("WC_GROUP", "btn_In_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("OUT"))
                {

                    AUTO_YB_OUT AUTO_YB_OUT = new AUTO_YB_OUT(this);
                    AUTO_YB_OUT.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("DISPLAY"))
                {

                    DASHBOARD_YB_NEW_2 DASHBOARD_YB_NEW_2 = new DASHBOARD_YB_NEW_2(this);
                    DASHBOARD_YB_NEW_2.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("FAIL_INPUT"))
                {

                    AUTO_FAIL_REG_YB AUTO_FAIL_REG_YB = new AUTO_FAIL_REG_YB(this);
                    AUTO_FAIL_REG_YB.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("REWORK"))
                {

                    AUTO_REWORK_REG_YB AUTO_REWORK_REG_YB = new AUTO_REWORK_REG_YB(this);
                    AUTO_REWORK_REG_YB.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("NOZZLE"))
                {

                    AUTO_YB_NOZZLE_SEARCH AUTO_YB_NOZZLE_SEARCH = new AUTO_YB_NOZZLE_SEARCH(this);
                    AUTO_YB_NOZZLE_SEARCH.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
                else if (AUTO_display_Select.select_in_out.Equals("CONNECTION"))
                {

                    AUTO_YB_CONNECTION_SEARCH AUTO_YB_CONNECTION_SEARCH = new AUTO_YB_CONNECTION_SEARCH(this);
                    AUTO_YB_CONNECTION_SEARCH.Show();
                    //regKey.SetValue("WC_GROUP", "btn_Out_PFR", RegistryValueKind.String);

                }
            }
        }


    }
}
