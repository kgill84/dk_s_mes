﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.FUNCTION
{
    class AUTO_GET_DATA
    {
        public static DataTable WccodeSelect_DropDown(string wc_group)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT WC_CODE,WC_NAME FROM WORK_CENTER WHERE WKAREA_CODE like '" + wc_group + "'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_CENTER");
                dt = ds.Tables["WORK_CENTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public static DataTable bom_chk(string parent_item, string lv_check)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "USP_BOM_SEARCH_NEW";

            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", parent_item);
            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            da.SelectCommand.Parameters.AddWithValue("@BOM_TYPE", "MF");
            da.SelectCommand.Parameters.AddWithValue("@LV_CHECK", lv_check);
            DataTable dt = null;
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "BOM_SEARCH");
                dt = ds.Tables["BOM_SEARCH"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }

        public static string get_pm_child_code(string pm_snumb)
        {
            string child_it_scode = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT TOP 1 PRDT_ITEM FROM  PROCESS_MOV WHERE PM_SNUMB = '" + pm_snumb + "' ";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    child_it_scode = reader["PRDT_ITEM"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return child_it_scode;
        }


        public static bool get_pm_CHECK(string pm_snumb)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT TOP 1 CHECK_YN FROM  PROCESS_MOV WHERE PM_SNUMB = '" + pm_snumb + "' ";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public static bool move_insert_NEW_pm(string pm_snumb, string wc_code)
        {
            bool check = false;
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();

            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_NEW_PM", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;


            mov_cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);
            mov_cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                //이동표 완료 주기
                string update_pp_sql = "UPDATE PROCESS_MOV SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE PM_SNUMB='" + pm_snumb + "' AND CHECK_YN='N'";
                SqlCommand update_pp_cmd = new SqlCommand(update_pp_sql, conn);
                update_pp_cmd.CommandType = CommandType.Text;


                update_pp_cmd.Transaction = trans;
                update_pp_cmd.ExecuteNonQuery();

                //공대차로 만듦
                /*
                string update_carrier_sql = "UPDATE CARRIER_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";
                SqlCommand update_carrier_cmd = new SqlCommand(update_carrier_sql, conn);
                update_carrier_cmd.CommandType = CommandType.Text;

                update_carrier_cmd.Transaction = trans;
                update_carrier_cmd.ExecuteNonQuery();
                */
                trans.Commit();
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //PAB 인지 체크 
        public static string check_PAB(string it_scode, string lot_no)//리딩시 OK 일때 READING_DATA INSERT
        {
            string strCon;
            string get_pab_it_scode = "";
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd = new SqlCommand("USP_AUTO_PAB_CHECK", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            conn.Open();
            SqlTransaction tran = conn.BeginTransaction();
            cmd.Transaction = tran;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@LOT_NO", lot_no);


            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    get_pab_it_scode = reader[0].ToString();
                }
                reader.Close();
                reader.Dispose();

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();

            }
            finally
            {
                conn.Close();
                //formClear();
            }
            return get_pab_it_scode;
        }



        
    }
}
