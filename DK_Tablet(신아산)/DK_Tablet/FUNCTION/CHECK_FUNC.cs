﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.FUNCTION
{
    class CHECK_FUNC
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public static bool IsConnectedToInternet()
        {
            int Desc;
            bool result = InternetGetConnectedState(out Desc, 0);
            Console.WriteLine(Desc);
            Console.WriteLine(Convert.ToString(Desc, 16));
            return result;
        }

        
        public bool check_bom_hd(string lot_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT COUNT(*)AS NUM FROM LOT_NO_HD WHERE LOT_NO = '" + lot_no + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public bool check_in_reading_det(string lot_no, string it_scode)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT COUNT(*)AS NUM FROM LOT_NO_DET WHERE LOT_NO = '" + lot_no + "' AND IT_SCODE = '" + it_scode + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public Font AutoFontSize(LabelControl label, String text)
        {
            Font ft;
            Graphics gp;
            SizeF sz;
            Single Faktor, FaktorX, FaktorY;
            gp = label.CreateGraphics();
            sz = gp.MeasureString(text, label.Font);
            gp.Dispose();

            FaktorX = (label.Width) / sz.Width;
            FaktorY = (label.Height) / sz.Height;
            if (FaktorX > FaktorY)
                Faktor = FaktorY;
            else
                Faktor = FaktorX;
            ft = label.Font;

            return new Font(ft.Name, ft.SizeInPoints * (Faktor) - 1);
        }
        //작업지시 완료여부 체크
        public string po_release_new_check(string mo_snumb)
        {
            string end_check = "N";
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT END_CHECK FROM PO_RELEASE_NEW WHERE MO_SNUMB='" + mo_snumb + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();

                end_check = sr["END_CHECK"].ToString().Trim();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return end_check;
        }
        public string server_get_datetime(string mo_snumb,string wc_code)
        {
            string strCon;
            string datetime = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "if exists (select * from work_input_new_virtual where RSRV_NO='"+mo_snumb+"' AND wc_code='"+wc_code+"')"
                + " select TOP 1 r_END from work_input_new_virtual where RSRV_NO='"+mo_snumb+"' AND wc_code='" + wc_code + "' ORDER BY R_END DESC"
                +" else"
                +" SELECT CONVERT(CHAR(10), GETDATE(), 23) +' '+ SUBSTRING(Convert(varchar(8),Getdate(),108),1,5)AS TIME";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        datetime = reader[0].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return datetime;
        }

        //pp카드 체크
        public bool pp_card_check(string prdt_item, string card_no, string carrier_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N' AND CARRIER_NO='" + carrier_no + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        //pp 카드 선택시 pp_card 테이블에 check_yn 구분이 n 이 있는지 체크 n이 있으면 대기장에 있는 PP 카드이므로 실적등록 불가
        public bool pp_card_check2(string prdt_item, string card_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from PP_CARD_REG where PRDT_ITEM='" + prdt_item + "' AND CARD_NO='" + card_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        //대차 체크
        public bool carrier_check(string carrier_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from CARRIER_REG WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        //qr_code 읽었을때 체크 해야됨(20150226)진행중
        public bool qr_loc_document(string lot_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() == "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public int max_vw_snumb(string rsrv_no, string wc_code)
        {
            int vw_snumb = 0;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT ISNULL(MAX(VW_SNUMB),0)AS VW_SNUMB FROM WORK_INPUT_NEW_VIRTUAL WHERE RSRV_NO = '" + rsrv_no + "' AND WC_CODE = '" + wc_code + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["VW_SNUMB"].ToString().Trim() == "0")
                {
                    vw_snumb = 0;
                }
                else
                {
                    vw_snumb = int.Parse(sr["VW_SNUMB"].ToString().Trim());
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return vw_snumb;

        }
        //N값인지 체크 
        public bool cancle_MOVE_check(string rsrv_no, int vw_snumb,string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT CHECK_YN FROM CARRIER_REG WHERE RSRV_NO='" + rsrv_no + "' AND WC_CODE='" + wc_code + "' AND VW_SNUMB=" + vw_snumb + "";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["CHECK_YN"].ToString().Trim() == "N")
                {
                    check = false;
                }
                else
                {
                    check = true;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        //품목에 대한 성형조건이 이전에 저장된 것이 있는지 체크
        public bool inj_condi_check(string it_scode)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "select COUNT(*)AS NUM from INJ_CONDI where IT_SCODE='" + it_scode + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }


        //주야간 가지고 오기
        public string[] server_get_shiftwork()
        {
            string strCon;
            string[] arry_str = new string[2];
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SP_TABLET_GET_SHIFTWORK_CS";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =new SqlCommand(sql, conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    arry_str[0] = reader["SW_CODE"].ToString().Trim();
                    arry_str[1] = reader["SW_NAME"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return arry_str;
        }

        public bool Match_carrier_no(string it_scode,string carrier_no)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "DECLARE @IT_MODEL VARCHAR(5)SET @IT_MODEL =''"
                        + " SET @IT_MODEL =(select substring(IT_MODEL,1,3) from it_master where it_scode='" + it_scode + "')"
                        + " SELECT COUNT(*)NUM FROM CARRIER_MASTER WHERE SUBSTRING(CAR_NAME,1,3)=@IT_MODEL AND DC_GUBN=SUBSTRING('" + carrier_no + "',1,4)";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public bool Mix_work_plan_check_item(string it_scode)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT COUNT(*) AS NUM FROM WORK_PLAN WHERE END_CHECK='N' AND IT_SCODE ='" + it_scode + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public string Mix_get_work_plan_wp_snumb(string it_scode)
        {
            string wp_snumb = "";
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT WP_SNUMB FROM WORK_PLAN WHERE END_CHECK='N' AND IT_SCODE ='" + it_scode + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();

                wp_snumb = sr["WP_SNUMB"].ToString().Trim();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return wp_snumb;
        }

        public string Mix_get_work_plan_me_scode(string it_scode)
        {
            string  me_scode = "";
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT ME_SCODE FROM IT_MASTER WHERE IT_SCODE='" + it_scode + "'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();

                me_scode = sr["ME_SCODE"].ToString().Trim();
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return me_scode;
        }
        //해당 공장에 품목 있는지 검사
        public bool chkSiteIt(string site_code, string it_scode)
        {
            SqlConnection conn = new SqlConnection();
            string sql = "SELECT COUNT(IT_SCODE) FROM IT_MASTER WHERE IT_SCODE = @IT_SCODE";
            switch (site_code)
            {
                case "D001":
                    conn.ConnectionString = Properties.Settings.Default.SQL_DKQT;
                    break;
                case "K001":
                    conn.ConnectionString = Properties.Settings.Default.SQL_TKQT;
                    break;
                case "U001":
                    conn.ConnectionString = Properties.Settings.Default.SQL_TUST;
                    break;
                case "A001":
                    conn.ConnectionString = Properties.Settings.Default.SQL_TAQT;
                    break;
                default:
                    conn.ConnectionString = Properties.Settings.Default.SQL_DKQT;
                    break;
            }
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
                return int.Parse(cmd.ExecuteScalar().ToString().Trim()) > 0;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
    }
}
