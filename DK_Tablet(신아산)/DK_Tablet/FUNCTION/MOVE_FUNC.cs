﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.FUNCTION
{
    class MOVE_FUNC
    {
        public void move_insert(string it_scode, string pm_snumb, float mm_sqty, string wc_code)
        {
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();
            string sql_cmd = "";
            sql_cmd = "SP_TABLET_LASER_IN";
            string mm_snumb = SaveMaxNumber();
            SqlCommand history_cmd = new SqlCommand(sql_cmd, conn);
            history_cmd.CommandType = CommandType.StoredProcedure;
            history_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//등록번호
            history_cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));//등록일            
            history_cmd.Parameters.AddWithValue("@WC_CODE", wc_code);//작업장

            history_cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//품목
            history_cmd.Parameters.AddWithValue("@MM_SQTY", mm_sqty);//수량
            history_cmd.Parameters.AddWithValue("@MM_DESC", "생산이동");//적요
            history_cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);//PP카드

            history_cmd.Transaction = trans;



            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            mov_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//이동 이력 등록번호            

            mov_cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);//이동 이력 등록번호    
            mov_cmd.Transaction = trans;

            try
            {
                history_cmd.ExecuteNonQuery();
                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                //PP카드 공카드로 만듦
                string update_pp_sql = "UPDATE PROCESS_MOV SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE PM_SNUMB='" + pm_snumb + "' AND PRDT_ITEM='" + it_scode + "' AND CHECK_YN='N'";
                SqlCommand update_pp_cmd = new SqlCommand(update_pp_sql, conn);
                update_pp_cmd.CommandType = CommandType.Text;


                update_pp_cmd.Transaction = trans;
                update_pp_cmd.ExecuteNonQuery();

                //공대차로 만듦
                /*
                string update_carrier_sql = "UPDATE CARRIER_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";
                SqlCommand update_carrier_cmd = new SqlCommand(update_carrier_sql, conn);
                update_carrier_cmd.CommandType = CommandType.Text;

                update_carrier_cmd.Transaction = trans;
                update_carrier_cmd.ExecuteNonQuery();
                */
                trans.Commit();


            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        public bool move_insert_shipment_NEW(string sh_snumb, DataTable dtable, string to_loc)
        {
            string conStr;
            bool check = false;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            //string mm_snumb = SaveMaxNumber();
            DataTable DT = new DataTable();
            DT.Columns.Add("QR_SDATE", typeof(string));
            DT.Columns.Add("QR_IT_SCODE", typeof(string));
            DT.Columns.Add("QR_SQTY", typeof(string));
            DT.Columns.Add("INPUT_SQTY", typeof(string));
            DT.Columns.Add("QR_SEQ", typeof(string));
            DT.Columns.Add("QR_LOT_NO", typeof(string));
            DT.Columns.Add("QR_CHECK_YN", typeof(string));
            DT.Columns.Add("QR_LOC_CODE", typeof(string));
            DT.Columns.Add("QR_MM_RDATE", typeof(string));
            DT.Columns.Add("ME_SCODE", typeof(string));

            DataRow[] dr = dtable.Select("CHECK_YN='Y'");
            for (int i = 0; i < dr.Length; i++)
            {
                DataRow drr = DT.NewRow();
                drr["QR_SDATE"] = dr[i]["QR_SDATE"].ToString();
                drr["QR_IT_SCODE"] = dr[i]["QR_IT_SCODE"].ToString();
                drr["QR_SQTY"] = dr[i]["QR_SQTY"].ToString();
                drr["INPUT_SQTY"] = dr[i]["INPUT_SQTY"].ToString();
                drr["QR_SEQ"] = dr[i]["QR_SEQ"].ToString();
                drr["QR_LOT_NO"] = dr[i]["QR_LOT_NO"].ToString();
                drr["QR_CHECK_YN"] = dr[i]["QR_CHECK_YN"].ToString();
                drr["QR_LOC_CODE"] = dr[i]["QR_LOC_CODE"].ToString();
                drr["QR_MM_RDATE"] = dr[i]["QR_MM_RDATE"].ToString();
                drr["ME_SCODE"] = dr[i]["ME_SCODE"].ToString();
                DT.Rows.Add(drr);
            }

            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_SHIPMENT_1", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;
            mov_cmd.CommandTimeout = 0;

            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);//출하 등록번호   
            mov_cmd.Parameters.AddWithValue("@CARD_NO", "");//PP카드
            mov_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            mov_cmd.Parameters.AddWithValue("@TVP", DT);//
            mov_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//
            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                trans.Commit();
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }


        public bool move_insert_shipment_NEW_LOT_DEL(string sh_snumb, DataTable dtable, string to_loc)
        {
            string conStr;
            bool check = false;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            //string mm_snumb = SaveMaxNumber();
            DataTable DT = new DataTable();
            DT.Columns.Add("QR_SDATE", typeof(string));
            DT.Columns.Add("QR_IT_SCODE", typeof(string));
            DT.Columns.Add("QR_SQTY", typeof(string));
            DT.Columns.Add("INPUT_SQTY", typeof(string));
            DT.Columns.Add("QR_SEQ", typeof(string));
            DT.Columns.Add("QR_LOT_NO", typeof(string));
            DT.Columns.Add("QR_CHECK_YN", typeof(string));
            DT.Columns.Add("QR_LOC_CODE", typeof(string));
            DT.Columns.Add("QR_MM_RDATE", typeof(string));
            DT.Columns.Add("ME_SCODE", typeof(string));


            foreach (DataRow dr in dtable.Rows)
            {
                DataRow drr = DT.NewRow();
                drr["QR_SDATE"] = dr["QR_SDATE"].ToString();
                drr["QR_IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                drr["QR_SQTY"] = dr["QR_SQTY"].ToString();
                drr["INPUT_SQTY"] = dr["INPUT_SQTY"].ToString();
                drr["QR_SEQ"] = dr["QR_SEQ"].ToString();
                drr["QR_LOT_NO"] = dr["QR_LOT_NO"].ToString();
                drr["QR_CHECK_YN"] = dr["QR_CHECK_YN"].ToString();
                drr["QR_LOC_CODE"] = dr["QR_LOC_CODE"].ToString();
                drr["QR_MM_RDATE"] = dr["QR_MM_RDATE"].ToString();
                drr["ME_SCODE"] = dr["ME_SCODE"].ToString();
                DT.Rows.Add(drr);
            }
            
            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_SHIPMENT_1_LOT_DEL_NEW", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;
            mov_cmd.CommandTimeout = 0;

            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);//출하 등록번호   
            mov_cmd.Parameters.AddWithValue("@CARD_NO", "");//PP카드
            mov_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            mov_cmd.Parameters.AddWithValue("@TVP", DT);//
            mov_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//
            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                trans.Commit();
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public void move_insert_NEW(string it_scode, string card_no, string carrier_no, float mm_sqty, string wc_code)
        {
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();


            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_NEW_LOT", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            mov_cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//이동 이력 등록번호                           
            mov_cmd.Parameters.AddWithValue("@CARD_NO", card_no);//이동 이력 등록번호   
            mov_cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            mov_cmd.Parameters.AddWithValue("@SQTY", mm_sqty);
            mov_cmd.Transaction = trans;

            try
            {
                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                //PP카드 공카드로 만듦
                string update_pp_sql = "UPDATE PP_CARD_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARD_NO='" + card_no + "' AND PRDT_ITEM='" + it_scode + "' AND CHECK_YN='N'";
                SqlCommand update_pp_cmd = new SqlCommand(update_pp_sql, conn);
                update_pp_cmd.CommandType = CommandType.Text;

                update_pp_cmd.Transaction = trans;
                update_pp_cmd.ExecuteNonQuery();

                //공대차로 만듦
                /*
                string update_carrier_sql = "UPDATE CARRIER_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";
                SqlCommand update_carrier_cmd = new SqlCommand(update_carrier_sql, conn);
                update_carrier_cmd.CommandType = CommandType.Text;

                update_carrier_cmd.Transaction = trans;
                update_carrier_cmd.ExecuteNonQuery();
                */
                trans.Commit();


            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        //공정이동표로 이동
        public bool move_insert_NEW_pm(string pm_snumb, string wc_code)
        {
            bool check = false;
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();

            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_NEW_PM", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            
            mov_cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);
            mov_cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            
            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                //이동표 완료 주기
                string update_pp_sql = "UPDATE PROCESS_MOV SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE PM_SNUMB='" + pm_snumb + "' AND CHECK_YN='N'";
                SqlCommand update_pp_cmd = new SqlCommand(update_pp_sql, conn);
                update_pp_cmd.CommandType = CommandType.Text;


                update_pp_cmd.Transaction = trans;
                update_pp_cmd.ExecuteNonQuery();

                //공대차로 만듦
                /*
                string update_carrier_sql = "UPDATE CARRIER_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";
                SqlCommand update_carrier_cmd = new SqlCommand(update_carrier_sql, conn);
                update_carrier_cmd.CommandType = CommandType.Text;

                update_carrier_cmd.Transaction = trans;
                update_carrier_cmd.ExecuteNonQuery();
                */
                trans.Commit();
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //작업장 location 으로 이동 
        public void move_insert(string it_scode, string card_no, string carrier_no, float mm_sqty,string wc_code)
        {
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();

            trans = conn.BeginTransaction();
            string sql_cmd = "";
            sql_cmd = "SP_TABLET_LASER_IN";
            string mm_snumb = SaveMaxNumber();
            SqlCommand history_cmd = new SqlCommand(sql_cmd, conn);
            history_cmd.CommandType = CommandType.StoredProcedure;
            history_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//등록번호
            history_cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));//등록일            
            history_cmd.Parameters.AddWithValue("@WC_CODE", wc_code);//작업장
            
            history_cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//품목
            history_cmd.Parameters.AddWithValue("@MM_SQTY", mm_sqty);//수량
            history_cmd.Parameters.AddWithValue("@MM_DESC", "생산이동");//적요
            history_cmd.Parameters.AddWithValue("@CARD_NO", card_no);//PP카드
            history_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            history_cmd.Transaction = trans;



            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            mov_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//이동 이력 등록번호            
            mov_cmd.Parameters.AddWithValue("@CARRIER_NO", carrier_no);//이동 이력 등록번호    
            mov_cmd.Parameters.AddWithValue("@CARD_NO", card_no);//이동 이력 등록번호    
            mov_cmd.Transaction = trans;

            try
            {
                history_cmd.ExecuteNonQuery();
                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time ="";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;
                
                //PP카드 공카드로 만듦
                string update_pp_sql = "UPDATE PP_CARD_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARD_NO='" + card_no + "' AND PRDT_ITEM='" + it_scode + "' AND CHECK_YN='N'";
                SqlCommand update_pp_cmd = new SqlCommand(update_pp_sql, conn);
                update_pp_cmd.CommandType = CommandType.Text;

                update_pp_cmd.Transaction = trans;
                update_pp_cmd.ExecuteNonQuery();

                //공대차로 만듦
                /*
                string update_carrier_sql = "UPDATE CARRIER_REG SET CHECK_YN='Y',OUT_TIME='" + input_time + "' WHERE CARRIER_NO='" + carrier_no + "' AND CHECK_YN='N'";
                SqlCommand update_carrier_cmd = new SqlCommand(update_carrier_sql, conn);
                update_carrier_cmd.CommandType = CommandType.Text;

                update_carrier_cmd.Transaction = trans;
                update_carrier_cmd.ExecuteNonQuery();
                */
                trans.Commit();
                

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        //이동 등록번호 채번
        private string SaveMaxNumber()
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_GET_MOVE_HISTORY_MAXNUM_NEW", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@MAX_NUM", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string maxnum = paramMaxNum.Value.ToString();
                return maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }

        }


        public bool move_insert_shipment_NEW_NULTI_LOC(string sh_snumb, DataTable dtable,DataTable sh_hd,DataTable sh_det)
        {
            string conStr;
            bool check = false;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            //string mm_snumb = SaveMaxNumber();
            DataTable DT = new DataTable();
            DT.Columns.Add("QR_IT_SCODE", typeof(string));
            DT.Columns.Add("QR_SQTY", typeof(string));
            DT.Columns.Add("QR_LOC_CODE", typeof(string));


            foreach (DataRow dr in dtable.Rows)
            {
                DataRow drr = DT.NewRow();
                drr["QR_IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                drr["QR_SQTY"] = dr["QR_SQTY"].ToString();
                drr["QR_LOC_CODE"] = dr["QR_LOC_CODE"].ToString();
                DT.Rows.Add(drr);
            }

            //이동전표 등록/ 출하 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_SHIPMENT_NEW_MULTI_LOC_2", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;
            mov_cmd.CommandTimeout = 0;

            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);//출하 등록번호   
            mov_cmd.Parameters.AddWithValue("@TVP", DT);//
            mov_cmd.Parameters.AddWithValue("@HD_TVP", sh_hd);//
            mov_cmd.Parameters.AddWithValue("@DET_TVP", sh_det);//
            //mov_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//
            mov_cmd.Transaction = trans;

            try
            {

                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                trans.Commit();
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        //공장대체
        public bool move_insert_site_move(string sh_snumb, DataTable dtable)
        {
            string conStr;
            bool check = false;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);
            SqlConnection conn_D = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlConnection conn_A = new SqlConnection(Properties.Settings.Default.SQL_TAQT);
            SqlConnection conn_U = new SqlConnection(Properties.Settings.Default.SQL_TUST);
            SqlConnection conn_K = new SqlConnection(Properties.Settings.Default.SQL_TKQT);
            SqlTransaction trans;
            SqlTransaction trans_D;
            SqlTransaction trans_A;
            SqlTransaction trans_U;
            SqlTransaction trans_K;

            conn.Open();
            conn_D.Open();
            conn_A.Open();
            conn_U.Open();
            conn_K.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            trans_D = conn_D.BeginTransaction();
            trans_A = conn_A.BeginTransaction();
            trans_U = conn_U.BeginTransaction();
            trans_K = conn_K.BeginTransaction();
            //string mm_snumb = SaveMaxNumber();
            DataTable DT = new DataTable();
            DT.Columns.Add("IT_SCODE", typeof(string));
            DT.Columns.Add("SERNO", typeof(int));
            DT.Columns.Add("MM_SQTY", typeof(string));
            DT.Columns.Add("FROM_SITE", typeof(string));
            DT.Columns.Add("FROM_LOC", typeof(string));
            DT.Columns.Add("TO_SITE", typeof(string));            
            DT.Columns.Add("TO_LOC", typeof(string));
            DT.Columns.Add("LOC_OUT_SNUMB", typeof(string));
            DT.Columns.Add("LOC_IN_SNUMB", typeof(string));
            DT.Columns.Add("MM_OUT_SNUMB", typeof(string));
            DT.Columns.Add("MM_IN_SNUMB", typeof(string));
            DT.Columns.Add("MOVE_COST", typeof(float));


            
            string loc_out_snumb = "", loc_in_snumb = "", mm_out_snumb = "", mm_in_snumb = ""; 
            int serno = 1;
            string bf_loc = "", af_loc = "", bf_site = "", af_site = "";

            foreach (DataRow dr in dtable.Rows)
            {
                if (serno == 1)
                {
                    mm_out_snumb = GET_DATA.mm_OUT_NewNumber(Properties.Settings.Default.SITE_CODE.ToString().Trim());
                    loc_out_snumb = GET_DATA.loc_OUT_NewNumber(Properties.Settings.Default.SITE_CODE.ToString().Trim());
                }
                //bf_site = dr["FROM_SITE_CODE"].ToString();
                //bf_loc = dr["FROM_LOC_CODE"].ToString();
                af_site = dr["TO_SITE_CODE"].ToString();
                //af_loc = dr["TO_LOC_CODE"].ToString();
                if (bf_site != af_site)
                {
                    mm_in_snumb = GET_DATA.mm_IN_NewNumber(af_site);
                    loc_in_snumb = GET_DATA.loc_IN_NewNumber(af_site);
                }
                //if (bf_loc != af_loc)
                //{
                //    //로케이션, 통합 입출고 전표 번호 채번
                //    loc_out_snumb = GET_DATA.loc_OUT_NewNumber(Properties.Settings.Default.SITE_CODE.ToString().Trim());
                //    loc_in_snumb = GET_DATA.loc_IN_NewNumber(af_site);
                //}   

                serno = 1;
                DataRow drr = DT.NewRow();
                drr["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                drr["SERNO"] = serno;
                drr["MM_SQTY"] = dr["QR_SQTY"].ToString();
                drr["FROM_SITE"] = dr["FROM_SITE_CODE"].ToString();
                drr["FROM_LOC"] = dr["FROM_LOC_CODE"].ToString();
                drr["TO_SITE"] = dr["TO_SITE_CODE"].ToString();
                drr["TO_LOC"] = dr["TO_LOC_CODE"].ToString();
                drr["LOC_OUT_SNUMB"] = loc_out_snumb;
                drr["LOC_IN_SNUMB"] = loc_in_snumb;
                drr["MM_OUT_SNUMB"] = mm_out_snumb;
                drr["MM_IN_SNUMB"] = mm_in_snumb;
                drr["MOVE_COST"] = float.Parse(dr["MOVE_COST"].ToString());

                DT.Rows.Add(drr);
                //}
                //else
                //{
                //    DataRow drr = DT.NewRow();
                //    drr["IT_SCODE"] = dr["QR_IT_SCODE"].ToString();
                //    drr["SERNO"] = serno;
                //    drr["MM_SQTY"] = dr["QR_SQTY"].ToString();
                //    drr["FROM_SITE"] = dr["FROM_SITE_CODE"].ToString();
                //    drr["FROM_LOC"] = dr["FROM_LOC_CODE"].ToString();    
                //    drr["TO_SITE"] = dr["TO_SITE_CODE"].ToString();      
                //    drr["TO_LOC"] = dr["TO_LOC_CODE"].ToString();                                  
                //    drr["LOC_OUT_SNUMB"] = loc_out_snumb;
                //    drr["LOC_IN_SNUMB"] = loc_in_snumb;
                //    drr["MM_OUT_SNUMB"] = mm_out_snumb;
                //    drr["MM_IN_SNUMB"] = mm_in_snumb;
                //    drr["MOVE_COST"] = float.Parse(dr["MOVE_COST"].ToString());
                //    DT.Rows.Add(drr);
                //}
                serno = serno + 1;
                bf_site = af_site;
                //bf_loc = af_loc;
            }
            
            // 로케이션 출고, 통합 출고 전표 등록
            SqlCommand mov_cmd = new SqlCommand("USP_TABLET_INSERT_MOVE_OUT_REG_FINAL", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;
            mov_cmd.CommandTimeout = 0;                        
            mov_cmd.Parameters.AddWithValue("@MM_SKIND", "2");
            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
            mov_cmd.Parameters.AddWithValue("@TVP", DT);//
            //mov_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//
            mov_cmd.Transaction = trans;
            DataRow[] dr_arry_D = DT.Select("TO_SITE='D001'"); //신아산
            DataRow[] dr_arry_A = DT.Select("TO_SITE='A001'"); //아산
            DataRow[] dr_arry_U = DT.Select("TO_SITE='U001'");//울산
            DataRow[] dr_arry_K = DT.Select("TO_SITE='K001'");//경주
            SqlCommand mov_cmd_D = new SqlCommand("USP_TABLET_INSERT_MOVE_OUT_REG_FINAL", conn_D);
            SqlCommand mov_cmd_A = new SqlCommand("USP_TABLET_INSERT_MOVE_OUT_REG_FINAL", conn_A);
            SqlCommand mov_cmd_U = new SqlCommand("USP_TABLET_INSERT_MOVE_OUT_REG_FINAL", conn_U);
            SqlCommand mov_cmd_K = new SqlCommand("USP_TABLET_INSERT_MOVE_OUT_REG_FINAL", conn_K);
            
            if (dr_arry_D.Length > 0)
            {
                
                DataTable DT_D = new DataTable();
                DT_D.Columns.Add("IT_SCODE", typeof(string));
                DT_D.Columns.Add("SERNO", typeof(int));
                DT_D.Columns.Add("MM_SQTY", typeof(string));
                DT_D.Columns.Add("FROM_SITE", typeof(string));
                DT_D.Columns.Add("FROM_LOC", typeof(string));
                DT_D.Columns.Add("TO_SITE", typeof(string));
                DT_D.Columns.Add("TO_LOC", typeof(string));
                DT_D.Columns.Add("LOC_OUT_SNUMB", typeof(string));
                DT_D.Columns.Add("LOC_IN_SNUMB", typeof(string));
                DT_D.Columns.Add("MM_OUT_SNUMB", typeof(string));
                DT_D.Columns.Add("MM_IN_SNUMB", typeof(string));
                DT_D.Columns.Add("MOVE_COST", typeof(float));
                foreach (DataRow drr in dr_arry_D)
                {
                    DataRow add_dr = DT_D.NewRow();
                    add_dr["IT_SCODE"] = drr["IT_SCODE"].ToString();
                    add_dr["SERNO"] = drr["SERNO"].ToString();
                    add_dr["MM_SQTY"] = drr["MM_SQTY"].ToString();
                    add_dr["FROM_SITE"] = drr["FROM_SITE"].ToString();
                    add_dr["FROM_LOC"] = drr["FROM_LOC"].ToString();
                    add_dr["TO_SITE"] = drr["TO_SITE"].ToString();
                    add_dr["TO_LOC"] = drr["TO_LOC"].ToString();
                    add_dr["LOC_OUT_SNUMB"] = drr["LOC_OUT_SNUMB"].ToString();
                    add_dr["LOC_IN_SNUMB"] = drr["LOC_IN_SNUMB"].ToString();
                    add_dr["MM_OUT_SNUMB"] = drr["MM_OUT_SNUMB"].ToString();
                    add_dr["MM_IN_SNUMB"] = drr["MM_IN_SNUMB"].ToString();
                    add_dr["MOVE_COST"] = drr["MOVE_COST"].ToString();
                    DT_D.Rows.Add(add_dr);
                }
                
                mov_cmd_D.CommandType = CommandType.StoredProcedure;
                mov_cmd_D.CommandTimeout = 0;                
                mov_cmd_D.Parameters.AddWithValue("@MM_SKIND", "1");
                mov_cmd_D.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
                mov_cmd_D.Parameters.AddWithValue("@TVP", DT_D);//
                mov_cmd_D.Transaction = trans_D;
            }
            if (dr_arry_A.Length > 0)
            {
                DataTable DT_A = new DataTable();
                DT_A.Columns.Add("IT_SCODE", typeof(string));
                DT_A.Columns.Add("SERNO", typeof(int));
                DT_A.Columns.Add("MM_SQTY", typeof(string));
                DT_A.Columns.Add("FROM_SITE", typeof(string));
                DT_A.Columns.Add("FROM_LOC", typeof(string));
                DT_A.Columns.Add("TO_SITE", typeof(string));
                DT_A.Columns.Add("TO_LOC", typeof(string));
                DT_A.Columns.Add("LOC_OUT_SNUMB", typeof(string));
                DT_A.Columns.Add("LOC_IN_SNUMB", typeof(string));
                DT_A.Columns.Add("MM_OUT_SNUMB", typeof(string));
                DT_A.Columns.Add("MM_IN_SNUMB", typeof(string));
                DT_A.Columns.Add("MOVE_COST", typeof(float));
                foreach (DataRow drr in dr_arry_A)
                {
                    DataRow add_dr = DT_A.NewRow();
                    add_dr["IT_SCODE"] = drr["IT_SCODE"].ToString();
                    add_dr["SERNO"] = drr["SERNO"].ToString();
                    add_dr["MM_SQTY"] = drr["MM_SQTY"].ToString();
                    add_dr["FROM_SITE"] = drr["FROM_SITE"].ToString();
                    add_dr["FROM_LOC"] = drr["FROM_LOC"].ToString();
                    add_dr["TO_SITE"] = drr["TO_SITE"].ToString();
                    add_dr["TO_LOC"] = drr["TO_LOC"].ToString();
                    add_dr["LOC_OUT_SNUMB"] = drr["LOC_OUT_SNUMB"].ToString();
                    add_dr["LOC_IN_SNUMB"] = drr["LOC_IN_SNUMB"].ToString();
                    add_dr["MM_OUT_SNUMB"] = drr["MM_OUT_SNUMB"].ToString();
                    add_dr["MM_IN_SNUMB"] = drr["MM_IN_SNUMB"].ToString();
                    add_dr["MOVE_COST"] = drr["MOVE_COST"].ToString();
                    DT_A.Rows.Add(add_dr);
                }
                
                mov_cmd_A.CommandType = CommandType.StoredProcedure;
                mov_cmd_A.CommandTimeout = 0;
                mov_cmd_A.Parameters.AddWithValue("@MM_SKIND", "1");
                mov_cmd_A.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
                mov_cmd_A.Parameters.AddWithValue("@TVP", DT_A);//
                mov_cmd_A.Transaction = trans_A;
            }
            if (dr_arry_U.Length > 0)
            {
                
                DataTable DT_U = new DataTable();
                DT_U.Columns.Add("IT_SCODE", typeof(string));
                DT_U.Columns.Add("SERNO", typeof(int));
                DT_U.Columns.Add("MM_SQTY", typeof(string));
                DT_U.Columns.Add("FROM_SITE", typeof(string));
                DT_U.Columns.Add("FROM_LOC", typeof(string));
                DT_U.Columns.Add("TO_SITE", typeof(string));
                DT_U.Columns.Add("TO_LOC", typeof(string));
                DT_U.Columns.Add("LOC_OUT_SNUMB", typeof(string));
                DT_U.Columns.Add("LOC_IN_SNUMB", typeof(string));
                DT_U.Columns.Add("MM_OUT_SNUMB", typeof(string));
                DT_U.Columns.Add("MM_IN_SNUMB", typeof(string));
                DT_U.Columns.Add("MOVE_COST", typeof(float));
                foreach (DataRow drr in dr_arry_U)
                {
                    DataRow add_dr = DT_U.NewRow();
                    add_dr["IT_SCODE"] = drr["IT_SCODE"].ToString();
                    add_dr["SERNO"] = drr["SERNO"].ToString();
                    add_dr["MM_SQTY"] = drr["MM_SQTY"].ToString();
                    add_dr["FROM_SITE"] = drr["FROM_SITE"].ToString();
                    add_dr["FROM_LOC"] = drr["FROM_LOC"].ToString();
                    add_dr["TO_SITE"] = drr["TO_SITE"].ToString();
                    add_dr["TO_LOC"] = drr["TO_LOC"].ToString();
                    add_dr["LOC_OUT_SNUMB"] = drr["LOC_OUT_SNUMB"].ToString();
                    add_dr["LOC_IN_SNUMB"] = drr["LOC_IN_SNUMB"].ToString();
                    add_dr["MM_OUT_SNUMB"] = drr["MM_OUT_SNUMB"].ToString();
                    add_dr["MM_IN_SNUMB"] = drr["MM_IN_SNUMB"].ToString();
                    add_dr["MOVE_COST"] = drr["MOVE_COST"].ToString();
                    DT_U.Rows.Add(add_dr);
                }
                mov_cmd_U.CommandType = CommandType.StoredProcedure;
                mov_cmd_U.CommandTimeout = 0;
                mov_cmd_U.Parameters.AddWithValue("@MM_SKIND", "1");
                mov_cmd_U.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
                mov_cmd_U.Parameters.AddWithValue("@TVP", DT_U);//
                mov_cmd_U.Transaction = trans_U;
            }
            if (dr_arry_K.Length > 0)
            {
                DataTable DT_K = new DataTable();
                DT_K.Columns.Add("IT_SCODE", typeof(string));
                DT_K.Columns.Add("SERNO", typeof(int));
                DT_K.Columns.Add("MM_SQTY", typeof(string));
                DT_K.Columns.Add("FROM_SITE", typeof(string));
                DT_K.Columns.Add("FROM_LOC", typeof(string));
                DT_K.Columns.Add("TO_SITE", typeof(string));
                DT_K.Columns.Add("TO_LOC", typeof(string));
                DT_K.Columns.Add("LOC_OUT_SNUMB", typeof(string));
                DT_K.Columns.Add("LOC_IN_SNUMB", typeof(string));
                DT_K.Columns.Add("MM_OUT_SNUMB", typeof(string));
                DT_K.Columns.Add("MM_IN_SNUMB", typeof(string));
                DT_K.Columns.Add("MOVE_COST", typeof(float));
                foreach (DataRow drr in dr_arry_K)
                {
                    DataRow add_dr = DT_K.NewRow();
                    add_dr["IT_SCODE"] = drr["IT_SCODE"].ToString();
                    add_dr["SERNO"] = drr["SERNO"].ToString();
                    add_dr["MM_SQTY"] = drr["MM_SQTY"].ToString();
                    add_dr["FROM_SITE"] = drr["FROM_SITE"].ToString();
                    add_dr["FROM_LOC"] = drr["FROM_LOC"].ToString();
                    add_dr["TO_SITE"] = drr["TO_SITE"].ToString();
                    add_dr["TO_LOC"] = drr["TO_LOC"].ToString();
                    add_dr["LOC_OUT_SNUMB"] = drr["LOC_OUT_SNUMB"].ToString();
                    add_dr["LOC_IN_SNUMB"] = drr["LOC_IN_SNUMB"].ToString();
                    add_dr["MM_OUT_SNUMB"] = drr["MM_OUT_SNUMB"].ToString();
                    add_dr["MM_IN_SNUMB"] = drr["MM_IN_SNUMB"].ToString();
                    add_dr["MOVE_COST"] = drr["MOVE_COST"].ToString();
                    DT_K.Rows.Add(add_dr);
                }
                mov_cmd_K.CommandType = CommandType.StoredProcedure;
                mov_cmd_K.CommandTimeout = 0;
                mov_cmd_K.Parameters.AddWithValue("@MM_SKIND", "1");
                mov_cmd_K.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
                mov_cmd_K.Parameters.AddWithValue("@TVP", DT_K);//
                mov_cmd_K.Transaction = trans_K;
            }
            try
            {
                
                mov_cmd.ExecuteNonQuery();
                if (dr_arry_D.Length > 0)
                {
                    mov_cmd_D.ExecuteNonQuery();
                }
                if (dr_arry_A.Length > 0)
                {
                    mov_cmd_A.ExecuteNonQuery();
                }
                if (dr_arry_U.Length > 0)
                {
                    mov_cmd_U.ExecuteNonQuery();
                }
                if (dr_arry_K.Length > 0)
                {
                    mov_cmd_K.ExecuteNonQuery();
                }
                
                trans.Commit();

                if (dr_arry_D.Length > 0)
                {
                    trans_D.Commit();
                }
                if (dr_arry_A.Length > 0)
                {
                    trans_A.Commit();
                }
                if (dr_arry_U.Length > 0)
                {
                    trans_U.Commit();
                }
                if (dr_arry_K.Length > 0)
                {
                    trans_K.Commit();
                }
                check = true;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                if (dr_arry_D.Length > 0)
                {
                    trans_D.Rollback();
                }
                if (dr_arry_A.Length > 0)
                {
                    trans_A.Rollback();
                }
                if (dr_arry_U.Length > 0)
                {
                    trans_U.Rollback();
                }
                if (dr_arry_K.Length > 0)
                {
                    trans_K.Rollback();
                }
                MessageBox.Show("Error (전표발행): " + ex.Message);
            }
            finally
            {
                conn.Close();
                conn_D.Close();
                conn_A.Close();
                conn_U.Close();
                conn_K.Close();
            }
            return check;
        }
        //출하 -> 사외창고 이동 PP카드 리딩
        public void move_insert_shipment(string it_scode, string card_no, string carrier_no, float remain_sqty, float mm_sqty, string to_loc, string sh_snumb)
        {
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            string sql_cmd = "";
            sql_cmd = "SP_TABLET_SHIPMENT_MOVE";
            string mm_snumb = SaveMaxNumber();
            SqlCommand history_cmd = new SqlCommand(sql_cmd, conn);
            history_cmd.CommandType = CommandType.StoredProcedure;
            history_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//등록번호
            history_cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString("yyyyMMdd"));//등록일            
            history_cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//품목
            
            history_cmd.Parameters.AddWithValue("@MM_SQTY", mm_sqty);//수량
            history_cmd.Parameters.AddWithValue("@MM_DESC", "사외창고 이동");//적요
            history_cmd.Parameters.AddWithValue("@CARD_NO", card_no);//PP카드
            history_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            history_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//대차번호

            history_cmd.Transaction = trans;

            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_SHIPMENT", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            mov_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//이동 이력 등록번호            
            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);//이동 이력 등록번호   
            mov_cmd.Parameters.AddWithValue("@CARD_NO", card_no);//PP카드
            mov_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            mov_cmd.Transaction = trans;

            try
            {
                history_cmd.ExecuteNonQuery();
                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;
                
                trans.Commit();


            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        //출하 -> 사외창고 이동 식별표 리딩
        public void move_insert_shipment_식별표(string it_scode, string seq, string lot_no, float remain_sqty, float mm_sqty, string to_loc, string sh_snumb)
        {
            string conStr;
            conStr = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(conStr);

            SqlTransaction trans;

            conn.Open();
            //출하지시 등록

            trans = conn.BeginTransaction();
            string sql_cmd = "";
            sql_cmd = "SP_TABLET_SHIPMENT_MOVE_NEW";
            string mm_snumb = SaveMaxNumber();
            SqlCommand history_cmd = new SqlCommand(sql_cmd, conn);
            history_cmd.CommandType = CommandType.StoredProcedure;
            history_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//등록번호
            history_cmd.Parameters.AddWithValue("@MM_RDATE", DateTime.Now.ToString());//등록일            
            history_cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);//품목

            history_cmd.Parameters.AddWithValue("@MM_SQTY", mm_sqty);//수량
            history_cmd.Parameters.AddWithValue("@MM_DESC", "사외창고 이동");//적요
            history_cmd.Parameters.AddWithValue("@QR_SEQ", seq);//PP카드
            history_cmd.Parameters.AddWithValue("@QR_LOT_NO", lot_no);//대차번호
            history_cmd.Parameters.AddWithValue("@TO_LOC", to_loc);//대차번호

            history_cmd.Transaction = trans;

            //이동전표 등록
            SqlCommand mov_cmd = new SqlCommand("SP_INSERT_LOC_DOCUMENT_SHIPMENT", conn);
            mov_cmd.CommandType = CommandType.StoredProcedure;

            mov_cmd.Parameters.AddWithValue("@MM_SNUMB", mm_snumb);//이동 이력 등록번호            
            mov_cmd.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);//이동 이력 등록번호   
            mov_cmd.Parameters.AddWithValue("@CARD_NO", "");//PP카드
            mov_cmd.Parameters.AddWithValue("@CARRIER_NO", "");//대차번호
            mov_cmd.Transaction = trans;

            try
            {
                history_cmd.ExecuteNonQuery();
                mov_cmd.ExecuteNonQuery();
                DateTime dt = new DateTime();
                dt = DateTime.Now;
                string input_time = "";
                string hh_str = "";
                string mm_str = "";
                if (dt.Hour.ToString().Length == 1)
                {
                    hh_str = "0" + dt.Hour.ToString();
                }
                else
                {
                    hh_str = dt.Hour.ToString();
                }
                if (dt.Minute.ToString().Length == 1)
                {
                    mm_str = "0" + dt.Minute.ToString();
                }
                else
                {
                    mm_str = dt.Minute.ToString();
                }
                input_time = hh_str + mm_str;

                trans.Commit();


            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("Error : " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        
    }
}
