﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using ThoughtWorks.QRCode.Codec;

namespace DK_Tablet.FUNCTION
{
    class SUB_SAVE
    {
        public bool fail_input_data_commit(string site_code, string it_scode, string lost_code, string wc_code, string fail_sqty, string fail_person)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;

            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_FAIL_COMMIT", conn);

            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            cmd.Parameters.AddWithValue("@LOST_SCODE", lost_code);

            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            cmd.Parameters.AddWithValue("@FAIL_SQTY", fail_sqty);

            cmd.Parameters.AddWithValue("@FAIL_PERSON", fail_person);




            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                check = true;
                MessageBox.Show("등록 완료");
                //불량수량 증가


            }

            catch (Exception e)
            {
                trans.Rollback();
                check = false;
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        
        //불량 테이블 저장
        public bool fail_input_data(string site_code,string lost_code, string rsrv_no, int vw_snumb,string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_FAIL", conn);
            
            cmd.CommandType = CommandType.StoredProcedure;

            
            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);
            cmd.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);
            cmd.Parameters.AddWithValue("@LOST_SCODE", lost_code);
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            
            
            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                check = true;
                MessageBox.Show("등록 완료");
                //불량수량 증가
                

            }

            catch (Exception e)
            {
                trans.Rollback();
                check = false;
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public bool fail_input_data_7(string site_code, string lost_code, string rsrv_no, int vw_snumb, string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;

            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_FAIL", conn);

            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramSITE_CODE =
                    new SqlParameter("@SITE_CODE", SqlDbType.VarChar, 4);
            paramSITE_CODE.Value = site_code;
            cmd.Parameters.Add(paramSITE_CODE);

            SqlParameter paramRSRV_NO =
                    new SqlParameter("@RSRV_NO", SqlDbType.VarChar, 8);
            paramRSRV_NO.Value = rsrv_no;
            cmd.Parameters.Add(paramRSRV_NO);


            SqlParameter paramVW_SNUMB =
                    new SqlParameter("@VW_SNUMB", SqlDbType.Int, 4);
            paramVW_SNUMB.Value = vw_snumb;
            cmd.Parameters.Add(paramVW_SNUMB);


            SqlParameter paramLOST_SCODE =
                    new SqlParameter("@LOST_SCODE", SqlDbType.VarChar, 10);
            paramLOST_SCODE.Value = lost_code;
            cmd.Parameters.Add(paramLOST_SCODE);


            SqlParameter paramWC_CODE =
                    new SqlParameter("@WC_CODE", SqlDbType.VarChar, 4);
            paramWC_CODE.Value = wc_code;
            cmd.Parameters.Add(paramWC_CODE);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                check = true;
                
                //불량수량 증가


            }

            catch (Exception e)
            {
                trans.Rollback();
                check = false;
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return check;
        }



        public bool fail_input_data_new(string site_code, string lost_code, string rsrv_no, int vw_snumb, string wc_code, string fail_sqty)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            SqlTransaction trans;
            

            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_FAIL1", conn);

            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.AddWithValue("@SITE_CODE", site_code);


            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);


            cmd.Parameters.AddWithValue("@VW_SNUMB", vw_snumb);


            cmd.Parameters.AddWithValue("@LOST_SCODE", lost_code);


            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            cmd.Parameters.AddWithValue("@FAIL_SQTY", fail_sqty);

            //커넥션오픈 실행
            conn.Open();
            trans = conn.BeginTransaction();
            cmd.Transaction = trans;
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();
                check = true;

                //불량수량 증가


            }

            catch (Exception e)
            {
                trans.Rollback();
                check = false;
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //아산 ACS(DM라인) 조립 입구에서 사출품(QR 코드)리딩시 LOT 연결 해제 및 불량 재고 처리
        public void material_fail_QR_reading(string lot_no, string it_scode, string wc_code, string rsrv_no)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("USP_METARIAL_FAIL_READING", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            cmd.Parameters.AddWithValue("@LOT_NO", lot_no);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            cmd.Parameters.AddWithValue("@RSRV_NO", rsrv_no);

            //커넥션오픈 실행
            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();
            }

            catch (Exception e)
            {
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
        }
        //비가동 등록
        public void dt_input_data(string site_code,string rsrv_no, string dt_code, string dt_stime, string dt_etime,string wc_code)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_TABLET_SAVE_DOWNTIME", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //공장코드
            SqlParameter paramSITE_CODE =
                    new SqlParameter("@SITE_CODE", SqlDbType.VarChar, 4);
            paramSITE_CODE.Value = "D001";
            cmd.Parameters.Add(paramSITE_CODE);

            //작업지시번호
            SqlParameter paramRSRV_NO =
                    new SqlParameter("@RSRV_NO", SqlDbType.VarChar, 8);
            paramRSRV_NO.Value = rsrv_no;
            cmd.Parameters.Add(paramRSRV_NO);

            //비가동 원인 코드
            SqlParameter paramDT_CODE =
                    new SqlParameter("@DT_CODE", SqlDbType.VarChar, 10);
            paramDT_CODE.Value = dt_code;
            cmd.Parameters.Add(paramDT_CODE);

            //시작시간
            SqlParameter paramDT_STIME =
                    new SqlParameter("@DT_STIME", SqlDbType.VarChar, 20);
            paramDT_STIME.Value = dt_stime;
            cmd.Parameters.Add(paramDT_STIME);

            //종료시간
            SqlParameter paramDT_ETIME =
                    new SqlParameter("@DT_ETIME", SqlDbType.VarChar, 20);
            paramDT_ETIME.Value = dt_etime;
            cmd.Parameters.Add(paramDT_ETIME);

            SqlParameter paramWC_CODE =
                    new SqlParameter("@WC_CODE", SqlDbType.VarChar, 4);
            paramWC_CODE.Value = wc_code;
            cmd.Parameters.Add(paramWC_CODE);

            //커넥션오픈 실행
            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();
            }

            catch (Exception e)
            {
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
        }
        //비가동 등록(종료시간 업데이트
        public void dt_input_data_update(string site_code, string rsrv_no, string dt_code, string dt_stime, string wc_code)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            DateTime dt = new DateTime();
            dt = DateTime.Now;

            SqlConnection conn = new SqlConnection(strCon);
            string sql = "";

            //sql = "UPDATE DT_REG_VIRTUAL SET DT_ETIME='" + dt.ToString("yyyy-MM-dd HH:mm") + "' WHERE WC_CODE='" + wc_code + "' AND DT_STIME='" + dt_stime + "'";
            sql = "SP_DT_REG_VIRTUAL_UPDATE";
            //커맨드
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DT_ETIME",dt.ToString("yyyy-MM-dd HH:mm:ss"));
            cmd.Parameters.AddWithValue("@DT_STIME",dt_stime.Trim());
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //커넥션오픈 실행
            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();
                
            }

            catch (Exception e)
            {
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }
        }
        public bool createQrCode(string ic_snumb, string qr_code)
        {
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);

            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            try
            {
                int scale = Convert.ToInt16(4);
                qrCodeEncoder.QRCodeScale = scale;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid size!" + ex.Message);
                return false;
            }
            try
            {

                qrCodeEncoder.QRCodeVersion = 3;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid version !"+ex.Message);
            }

            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;


            Image image;
            String data = qr_code;
            image = qrCodeEncoder.Encode(data);


            SqlCommand cmd = null;
            string query = "";
            List<SqlParameter> paramArr = new List<SqlParameter>();
            //파라메터 선언

            if (image == null)
            {
                SqlParameter param = new SqlParameter("QR_CODE", SqlDbType.Image);
                param.Value = DBNull.Value;
                paramArr.Add(param);
            }
            else
            {
                byte[] bImage = ImageToByte(image);
                paramArr.Add(new SqlParameter("@QR_CODE", bImage));
            }
            paramArr.Add(new SqlParameter("@IC_SNUMB", ic_snumb));

            //파라메터 선언 끝
            try
            {
                conn.Open();


                query = "UPDATE IT_CHART_NEW SET QR_IMAGE=@QR_CODE WHERE IC_SNUMB=@IC_SNUMB";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddRange(paramArr.ToArray());
                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            return true;
        }
        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public bool createQrCode_KIA(string ic_snumb, string qr_code)
        {
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);

            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            try
            {
                int scale = Convert.ToInt16(4);
                qrCodeEncoder.QRCodeScale = scale;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid size!"+ex.Message);
                return false;
            }
            try
            {

                qrCodeEncoder.QRCodeVersion = 3;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid version !"+ex.Message);
            }

            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;


            Image image;
            String data = qr_code;
            image = qrCodeEncoder.Encode(data);


            SqlCommand cmd = null;
            string query = "";
            List<SqlParameter> paramArr = new List<SqlParameter>();
            //파라메터 선언

            if (image == null)
            {
                SqlParameter param = new SqlParameter("QR_CODE", SqlDbType.Image);
                param.Value = DBNull.Value;
                paramArr.Add(param);
            }
            else
            {
                byte[] bImage = ImageToByte(image);
                paramArr.Add(new SqlParameter("@QR_CODE", bImage));
            }
            paramArr.Add(new SqlParameter("@IC_SNUMB", ic_snumb));

            //파라메터 선언 끝
            try
            {
                conn.Open();


                query = "UPDATE IT_CHART_KIA SET QR_IMAGE=@QR_CODE WHERE IC_SNUMB=@IC_SNUMB";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddRange(paramArr.ToArray());
                cmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            return true;
        }

        public bool lot_connect(DataTable dt, string parent_item, string wc_code)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);


            SqlCommand cmd =
                    new SqlCommand("USP_LOT_CONNECT_CS", conn);

            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.AddWithValue("@TVP", dt);
            cmd.Parameters.AddWithValue("@PARENT_ITEM", parent_item);
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);

            //커넥션오픈 실행
            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();
                check = true;
            }

            catch (Exception ex)
            {
                check = false;
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //작업 시간 업데이트
        public void 작업시작시간_업데이트(string wc_code,string it_scode,string dt_code, string gubn)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            DateTime dt = new DateTime();
            dt = DateTime.Now;

            SqlConnection conn = new SqlConnection(strCon);
            string sql = "";
            sql = "USP_WORK_PANNEL_UPDATE";
            //커맨드
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WC_CODE",wc_code);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@DT_CODE", dt_code);
            cmd.Parameters.AddWithValue("@GUBN", gubn);
            //커넥션오픈 실행
            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();

            }

            catch (Exception e)
            {
                MessageBox.Show("등록 실패 : " + e.Message);

            }
            finally
            {
                conn.Close();
            }

        }
    }
}
