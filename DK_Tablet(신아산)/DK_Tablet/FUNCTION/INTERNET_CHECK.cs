﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace DK_Tablet.FUNCTION
{
    class INTERNET_CHECK
    {
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState( out int Description, int ReservedValue ) ;

        //Creating a function that uses the API function...
        public static bool IsConnectedToInternet( )
            {
            int Desc ;
            return InternetGetConnectedState( out Desc, 0 ) ;
        }

        
        

    }
    public delegate void stCallBackDelegate();
    public class ScheduledTimer
    {
        private Timer _timer;
        public ScheduledTimer() { }

        public static TimeSpan GetDueTime(TimeSpan A, TimeSpan B) 
        {
            if (A < B)
            {
                return B.Subtract(A);
            }
            else
            {
                return new TimeSpan(24, 0, 0).Subtract(B.Subtract(A));
            }
        }


        public void SetTime(TimeSpan _time, stCallBackDelegate callback) 
        {
            if (this._timer != null)
            {
                // Change 매서드 사용 가능.
                this._timer = null;
            }
            TimeSpan Now = DateTime.Now.TimeOfDay;
            TimeSpan DueTime = GetDueTime(Now, _time);

            this._timer = new Timer(new TimerCallback(delegate(object _callback)
            {
                ((stCallBackDelegate)_callback)();
            }), callback, DueTime, new TimeSpan(24, 0, 0));
        }


    }
}
