﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.FUNCTION
{
    class GET_DATA
    {
        [System.Runtime.InteropServices.DllImport("coredll.dll")]
        private extern static void GetSystemTime(ref SYSTEMTIME lpSystemTime);
        public string good_qty { get; set; }
        public string fail_qty { get; set; }
        public string po_timer { get; set; }
        public string pp_timer { get; set; }
        public string day_time_start { get; set; }
        public string night_time_start { get; set; }
        public struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;
        }
        public SYSTEMTIME GetTime()
        {
            // Call the native GetSystemTime method
            // with the defined structure.
            SYSTEMTIME stime = new SYSTEMTIME();
            GetSystemTime(ref stime);

            // Show the current time.      
            return stime;

        }
        public string getPlant(string site_code)
        {
            string PLANT = "";
            if (site_code.Equals("D001"))
            {
                PLANT = "DKQT";
            }
            else if (site_code.Equals("A001"))
            {
                PLANT = "TAQT";
            }
            else if (site_code.Equals("U001"))
            {
                PLANT = "TUST";
            }
            else if (site_code.Equals("K001"))
            {
                PLANT = "TKQT";
            }
            return PLANT;

        }
        public DataTable SHIP_IT_MASTER()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT RTRIM(IT_SCODE) AS IT_SCODE, IT_SNAME, IT_PKQTY,ME_SCODE FROM IT_MASTER";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable bom_chk(string parent_item, string lv_check)
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "USP_BOM_SEARCH_NEW";

            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", parent_item);
            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE.ToString());
            da.SelectCommand.Parameters.AddWithValue("@BOM_TYPE", "MF");
            da.SelectCommand.Parameters.AddWithValue("@LV_CHECK", lv_check);
            DataTable dt = null;
            DataSet ds = null;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "BOM_SEARCH");
                dt = ds.Tables["BOM_SEARCH"];
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
        public DataTable SHIP_MOVE_ITEM_MASTER()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT RTRIM(A.IT_SCODE)AS IT_SCODE,RTRIM(B.IT_SNAME)AS IT_SNAME, B.IT_PKQTY,B.ME_SCODE FROM SITE_MOVE_REG A "
                    + " LEFT JOIN IT_MASTER B ON A.IT_SCODE = B.IT_SCODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public string getWc_code_to_loc_code(string wc_code)
        {
            string loc_code = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT LOC_CODE FROM WORK_CENTER WHERE WC_CODE='" + wc_code + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    loc_code = reader[0].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return loc_code;
        }
        public string get_pm_child_code(string pm_snumb)
        {
            string child_it_scode = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT TOP 1 PRDT_ITEM FROM  PROCESS_MOV WHERE PM_SNUMB = '" + pm_snumb + "' ";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    child_it_scode = reader["PRDT_ITEM"].ToString();                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return child_it_scode;
        }
        public bool get_pm_CHECK(string pm_snumb)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT TOP 1 CHECK_YN FROM  PROCESS_MOV WHERE PM_SNUMB = '" + pm_snumb + "' ";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public string get_weight_cnt_NEW_0(string rsrv_no, string wc_code)
        {
            string weight_cnt = "0";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "DECLARE @VW_SNUMB VARCHAR(5)";
            sql = sql + " SET @VW_SNUMB = '0'";
            sql = sql + " SELECT COUNT(*)AS NUM FROM INJECTION_WEIGHT WHERE RSRV_NO='" + rsrv_no + "' AND VW_SNUMB=@VW_SNUMB AND WC_CODE='" + wc_code + "' AND CHECK_YN='Y'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["NUM"].ToString().Trim() != "")
                    {
                        weight_cnt = reader["NUM"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return weight_cnt;
        }
        //ITEM 의 하위 자재중 반제품만 가져오기
        public DataTable get_bom_child_item(string parent_item)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT A.CHILD_ITEM,''AS LOT_NO, 'N'AS CHECK_YN FROM BOM_ITEM A"
                        + " LEFT JOIN IT_MASTER B ON A.CHILD_ITEM = B.IT_SCODE"
                        + " WHERE A.PARENT_ITEM ='" + parent_item + "' AND A.BOM_TYPE='MF' AND B.ME_SCODE='30'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "CHILD_ITEM");
                dt = ds.Tables["CHILD_ITEM"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public string get_it_chart_lot_no(string it_scode, string seq, string lot_no)
        {
            string pd_lot_no = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT PD_LOT_NO FROM IT_CHART_NEW WHERE IT_SCODE='" + it_scode + "' AND LOT_NO='" + lot_no + "' AND IC_SEQNO='" + seq + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    pd_lot_no = reader["PD_LOT_NO"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return pd_lot_no;
        }
        public bool 식별표완료체크(string str_it_scode, string str_seq, string str_lot)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT ISNULL(CHECK_YN,'N')AS CHECK_YN FROM IT_CHART_NEW WHERE IT_SCODE='" + str_it_scode + "' AND LOT_NO='" + str_lot + "' AND IC_SEQNO='" + str_seq + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Trim().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public bool 현대식별표완료체크(string str_it_scode, string str_seq, string date_str)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT ISNULL(CHECK_YN,'N')AS CHECK_YN FROM HD_IT_CHART WHERE IT_SCODE='" + str_it_scode + "' AND PDATE_CODE='" + date_str + "' AND IC_SEQNO='" + str_seq + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Trim().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public bool 기아식별표완료체크(string str_it_scode, string str_seq, string str_lot)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT ISNULL(CHECK_YN,'N')AS CHECK_YN FROM KIA_IT_CHART WHERE IT_SCODE='" + str_it_scode + "' AND LOT_NO='" + str_lot + "' AND SERNO='" + str_seq + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Trim().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public bool 외주완제품_체크(string it_scode)
        {

            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT ME_SCODE FROM IT_MASTER WHERE IT_SCODE='" + it_scode + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["ME_SCODE"].ToString().Trim().Equals("70"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public bool 외주완제품_식별표완료체크(string str_it_scode, string str_seq, string str_lot)
        {
            bool check = false;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT ISNULL(CHECK_YN,'N')AS CHECK_YN FROM IT_CHART WHERE IT_SCODE='" + str_it_scode + "' AND LOT_NO='" + str_lot + "' AND IC_SEQNO='" + str_seq + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["CHECK_YN"].ToString().Trim().Equals("N"))
                    {
                        check = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        public void 식별표구분자업데이트(DataTable DT_it_chart_update)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlTransaction trans;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            trans = conn.BeginTransaction();//트랜잭션 시작
            string insertStr = "";
            insertStr = "SP_IT_CHART_CHECK_UPDATE_CS";

            //da.InsertCommand.Parameters.Clear();
            SqlCommand cmd = new SqlCommand(insertStr, conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Transaction = trans;
            cmd.Parameters.AddWithValue("@TVP", DT_it_chart_update);
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("전송 실패 : " + ex.Message);
            }
        }
        public void 식별표구분자업데이트_HD(DataTable DT_it_chart_update)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlTransaction trans;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            trans = conn.BeginTransaction();//트랜잭션 시작
            string insertStr = "";
            insertStr = "SP_IT_CHART_CHECK_UPDATE_CS_HD";

            //da.InsertCommand.Parameters.Clear();
            SqlCommand cmd = new SqlCommand(insertStr, conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Transaction = trans;
            cmd.Parameters.AddWithValue("@TVP", DT_it_chart_update);
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("전송 실패 : " + ex.Message);
            }
        }
        public void 식별표구분자업데이트_KIA(DataTable DT_it_chart_update)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            SqlTransaction trans;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            trans = conn.BeginTransaction();//트랜잭션 시작
            string insertStr = "";
            insertStr = "SP_IT_CHART_CHECK_UPDATE_CS_KIA";

            //da.InsertCommand.Parameters.Clear();
            SqlCommand cmd = new SqlCommand(insertStr, conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Transaction = trans;
            cmd.Parameters.AddWithValue("@TVP", DT_it_chart_update);
            try
            {
                cmd.ExecuteNonQuery();
                trans.Commit();

            }
            catch (Exception ex)
            {
                trans.Rollback();
                MessageBox.Show("전송 실패 : " + ex.Message);
            }
        }
        public string get_pm_snumb()
        {
            string pm_snumb = "";
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "DECLARE @PM_SNUMB VARCHAR(10)"
                + " INSERT INTO PROCESS_MOV_GETNUM DEFAULT VALUES"
                + " SET @PM_SNUMB = SCOPE_IDENTITY()"
                + " SET @PM_SNUMB = 'PM'+(SELECT SUBSTRING('10000000'+@PM_SNUMB,Len(@PM_SNUMB)+1,LEN('10000000'+@PM_SNUMB)))"
                + " SELECT @PM_SNUMB AS PM_SNUMB";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    pm_snumb = reader["PM_SNUMB"].ToString().Trim();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return pm_snumb;
        }
        public DataTable LoccodeSelect_DropDown_move()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "select A.LOC_CODE,B.LOC_NAME from work_center A"
                        + " LEFT JOIN MANUFACTURE_LOC_MASTER B ON A.LOC_CODE=B.LOC_CODE"
                        + " LEFT JOIN (SELECT DISTINCT WASUB_CODE,SERNO FROM WORK_AREA)C ON A.WC_CODE=C.WASUB_CODE"
                        + " WHERE A.LOC_CODE IS NOT NULL ORDER BY C.SERNO,A.LOC_CODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable LoccodeSelect_DropDown_inspection()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "select A.LOC_CODE,A.LOC_NAME from MANUFACTURE_LOC_MASTER A"
                        +" WHERE A.LOC_CODE IS NOT NULL AND GUBN ='IN' ORDER BY A.LOC_CODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //양품,불량 수량 가져오기
        public void get_good_fail(string mo_snumb, string wc_code)
        {
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "DECLARE @GOOD_QTY INT DECLARE @FAIL_QTY INT SET @GOOD_QTY=0 SET @FAIL_QTY=0"
                + " SET @GOOD_QTY = (SELECT ISNULL(SUM(GOOD_QTY),0)AS GOOD_QTY FROM WORK_INPUT_NEW_VIRTUAL WHERE RSRV_NO='" + mo_snumb + "' AND WC_CODE='" + wc_code + "')"
                + " SET @FAIL_QTY =(SELECT ISNULL(SUM(FAIL_QTY),0)AS FAIL_QTY FROM FAIL_INPUT_NEW where RSRV_NO='" + mo_snumb + "' AND WC_CODE='" + wc_code + "')"
                + " SELECT @GOOD_QTY AS GOOD_QTY,@FAIL_QTY AS FAIL_QTY";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        good_qty = reader["GOOD_QTY"].ToString();
                        fail_qty = reader["FAIL_QTY"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
        //대기장에 있는 PP_CARD 검색 PP_CARD_REG 테이블
        public DataTable getDataGridView(string prdt_item, string wc_code)
        {
            DataTable dt = new DataTable();
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_GET_LOCATION_SET_DATA_LOT";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //품목코드로 가지고 오기 
            da.SelectCommand.Parameters.AddWithValue("@PRDT_ITEM", prdt_item);
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds, "laser_search");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            dt = ds.Tables["laser_search"];
            return dt;
        }

        //자재유형 불러오기
        public DataTable MescodeSelect_DropDown()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_GET_COMB_ME_SCODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "ME_MASTER");
                dt = ds.Tables["ME_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //작업장 불러오기
        public DataTable WccodeSelect_DropDown(string wc_group)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT WC_CODE,WC_NAME FROM WORK_CENTER WHERE WKAREA_CODE like '" + wc_group + "'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_CENTER");
                dt = ds.Tables["WORK_CENTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable Print_type_DropDown()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_PRINT_TYPE_GET";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "PRINT_TYPE");
                dt = ds.Tables["PRINT_TYPE"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public DataTable OUT_LoccodeSelect_DropDownTOit_scode(string it_scode)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;
            string db_con = "";
            string site_name = "";
            if (Properties.Settings.Default.SITE_CODE.ToString().Equals("D001"))
            {
                db_con = "";
                site_name = "신아산";
            }
            else if (Properties.Settings.Default.SITE_CODE.ToString().Equals("A001"))
            {
                db_con = "TAQT.MIS_TAQT.DBO.";
                site_name = "아산";
            }
            else if (Properties.Settings.Default.SITE_CODE.ToString().Equals("U001"))
            {
                db_con = "TUST.MIS_TUST.DBO.";
                site_name = "울산";
            }
            else if (Properties.Settings.Default.SITE_CODE.ToString().Equals("K001"))
            {
                db_con = "TKQT.MIS_TKQT.DBO.";
                site_name = "경주";
            }
            strQury = "SELECT DISTINCT F.LOC_CODE, C.LOC_NAME FROM "
                    + " (SELECT CASE WHEN ISNULL(B.CUS_IT_CODE,'') ='' THEN A.IT_SCODE "
                    + " ELSE B.IT_SCODE END F_IT_SCODE, LOC_CODE "
                    + " FROM (SELECT REPLACE(REPLACE(IT_SCODE,'-',''),' ','')IT_SCODE, LOC_CODE,IT_SCODE AS CUS_ITEM FROM D1_DELIVERY_INFO "
                    + " WHERE plant='" + site_name + "' AND ISNULL(LOC_CODE,'')<>'' AND IT_SCODE='" + it_scode + "'"
                    + " ) A "
                    + " LEFT JOIN " + db_con + "ITEM_CONVERSION_D1D9 B ON A.IT_SCODE= B.CUS_IT_CODE)F "
                    + " LEFT JOIN " + db_con + "MANUFACTURE_LOC_MASTER C ON F.LOC_CODE =C.LOC_CODE ";
                    

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable 공장대체품목_LOCATION_정보(string it_scode)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "USP_GET_SITE_MOVE_TO_ITEM";


            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE",it_scode);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //작업장 불러오기
        public DataTable WccodeSelect_DropDown_move(string wc_group)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_GET_COMB_WC_CODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //품목코드로 가지고 오기 
            da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_CENTER");
                dt = ds.Tables["WORK_CENTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable WccodeSelect_DropDown_move_SUBUL(string wc_group)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT '' AS WC_CODE, '' AS WC_NAME UNION SELECT WC_CODE,WC_NAME FROM WORK_CENTER";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            //품목코드로 가지고 오기 
            
            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_CENTER");
                dt = ds.Tables["WORK_CENTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public DataTable loc_codeSelect_DropDown_move_SUBUL()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT '' AS LOC_CODE, '' AS LOC_NAME UNION select DISTINCT a.loc_code,b.loc_name from work_center a"
                    +" left join MANUFACTURE_LOC_MASTER b on a.loc_code = b.LOC_CODE WHERE B.WC_YN='Y'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            //품목코드로 가지고 오기 

            try
            {
                ds = new DataSet();

                da.Fill(ds, "LOCATION");
                dt = ds.Tables["LOCATION"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //LOCATION 불러오기
        public DataTable LoccodeSelect_DropDown()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT LOC_CODE,LOC_NAME FROM MANUFACTURE_LOC_MASTER";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //사외 LOCATION 가져오기
        public DataTable OUT_LoccodeSelect_DropDown(string sale_gubn)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT DISTINCT A.LOC_CODE,A.LOC_NAME FROM MANUFACTURE_LOC_MASTER A"
                     +" WHERE A.LOC_NAME IS NOT NULL AND A.GUBN='OUT'"
                     + " AND SALE_GUBN ='" + sale_gubn + "'";
            //strQury = "";
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public DataTable OUT_LoccodeSelect_DropDown()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT DISTINCT A.LOC_CODE,A.LOC_NAME FROM MANUFACTURE_LOC_MASTER A"
                     + " WHERE A.LOC_NAME IS NOT NULL AND A.GUBN='OUT'";             
            //strQury = "";
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable shipment_sh_stime_get(string loc_code,string sh_odate)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT DISTINCT A.SH_SNUMB, SH_STIME FROM SHIPMENT_HD A"
                    + " LEFT JOIN SHIPMENT_DET B ON A.SH_SNUMB = B.SH_SNUMB"
                    + " WHERE B.LOC_CODE ='" + loc_code + "' AND A.SH_ODATE='" + sh_odate + "'"
                    + " AND ISNULL(A.CANCLE_GUBN,'N')='N' AND SH_STIME IS NOT NULL";
            //strQury = "";
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        public DataTable OUT_Loc_carSelect_DropDown()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT A.DV_CODE, A.DV_NAME,B.LC_CODE,B.LC_NAME FROM DRIVER_MASTER A"
                    +" LEFT JOIN LOGISTIC_COMPANY B ON A.LC_CODE=B.LC_CODE";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;

            try
            {
                ds = new DataSet();

                da.Fill(ds, "MANUFACTURE_LOC_MASTER");
                dt = ds.Tables["MANUFACTURE_LOC_MASTER"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        //가실적 테이블 MAX(IN_SERNO)+1 들고오기
        public int maxIN_SERNO(string rsrv_no, string wc_code)
        {
            int in_serno = 0;
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "select ISNULL(MAX(VW_SNUMB),0)+1 from WORK_INPUT_NEW_VIRTUAL WHERE RSRV_NO='" + rsrv_no + "' and WC_CODE='" + wc_code + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    in_serno = int.Parse(reader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return in_serno;
        }
        public string get_loc_code(string wc_code)
        {
            string loc_code = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT LOC_CODE FROM WORK_CENTER WHERE WC_CODE='" + wc_code + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    loc_code = reader["LOC_CODE"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return loc_code;
        }

        public void get_timer_master(string wc_group)
        {
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT GUBN,TIMER FROM TABLET_TIMER_MASTER WHERE WC_GROUP='" + wc_group + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["GUBN"].ToString().Trim() == "PO")
                    {
                        po_timer = reader["TIMER"].ToString();
                    }
                    else if (reader["GUBN"].ToString().Trim() == "PP")
                    {
                        pp_timer = reader["TIMER"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }


        public string[] get_connection_port(string wc_code, string inout)
        {
            string strCon;
            string[] str = new string[2];
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT PORT_NO,POWER FROM AUTO_CONNECT_PORT_MASTER WHERE WC_CODE='" + wc_code + "' AND IN_OUT='" + inout + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        str[0] = reader["PORT_NO"].ToString();
                        str[1] = reader["POWER"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return str;
        }
        //중량 테이블에 등록 되어 있는 양품 개수 들고 오기
        public string get_weight_cnt(string rsrv_no, string wc_code)
        {
            string weight_cnt = "0";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "DECLARE @VW_SNUMB VARCHAR(5)";
            sql = sql + " SET @VW_SNUMB = (SELECT ISNULL(MAX(VW_SNUMB),0)+1 FROM WORK_INPUT_NEW_VIRTUAL WHERE RSRV_NO='" + rsrv_no + "' AND WC_CODE='" + wc_code + "')";
            sql = sql + " SELECT COUNT(*)AS NUM FROM INJECTION_WEIGHT WHERE RSRV_NO='" + rsrv_no + "' AND VW_SNUMB=@VW_SNUMB AND WC_CODE='" + wc_code + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["NUM"].ToString().Trim() != "")
                    {
                        weight_cnt = reader["NUM"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return weight_cnt;
        }
        public string get_weight_cnt_LOT(string rsrv_no, string wc_code)
        {
            string weight_cnt = "0";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "DECLARE @VW_SNUMB VARCHAR(5)";
            sql = sql + " SET @VW_SNUMB = (SELECT ISNULL(MAX(VW_SNUMB),0)+1 FROM WORK_INPUT_NEW_VIRTUAL WHERE RSRV_NO='" + rsrv_no + "' AND WC_CODE='" + wc_code + "')";
            sql = sql + " SELECT COUNT(*)AS NUM FROM INJECTION_WEIGHT WHERE RSRV_NO='" + rsrv_no + "' AND VW_SNUMB=@VW_SNUMB AND WC_CODE='" + wc_code + "' AND ISNULL(LOT_NO,'') <> ''";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["NUM"].ToString().Trim() != "")
                    {
                        weight_cnt = reader["NUM"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return weight_cnt;
        }
        //주야간 시작 시간 가져오기

        public void get_work_time_master()
        {
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT START_TIME,GUBN FROM WORK_TIME_MASTER";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["GUBN"].ToString().Trim() == "1")
                    {
                        day_time_start = reader["START_TIME"].ToString();
                    }
                    else if (reader["GUBN"].ToString().Trim() == "2")
                    {
                        night_time_start = reader["START_TIME"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }


        public DataTable get_miss_sqty(string wc_code, string me_scode, string tag)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_ERP_BOM_REMAIN_SQTY";
            if (tag.Trim().Equals("0"))
            {
                me_scode = "";
            }
            else
            {
                wc_code = "";
            }
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();
            DateTime date = DateTime.Now;
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@BOM_TYPE", "MF");
            da.SelectCommand.Parameters.AddWithValue("@BOM_REVNO", "0001");
            da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", "D001");
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@ME_SCODE", me_scode);

            try
            {
                ds = new DataSet();

                da.Fill(ds, "BOM_REMAIN_SQTY");
                dt = ds.Tables["BOM_REMAIN_SQTY"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;
        }
        public DataTable get_miss_sqty_new(string loc_code, string me_scode, string tag)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_QUANTITY_MATARIAL_1";
            if (tag.Trim().Equals("0"))
            {
                me_scode = "";
            }
            else
            {
                loc_code = "";
            }
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();
            DateTime date = DateTime.Now;
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@SDATE", date.ToString("yyyyMMdd"));
            da.SelectCommand.Parameters.AddWithValue("@LOC_CODE", loc_code);
            

            try
            {
                ds = new DataSet();

                da.Fill(ds, "BOM_REMAIN_SQTY");
                dt = ds.Tables["BOM_REMAIN_SQTY"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;
        }
        //입고 거래 명세서 qr 리딩시 데이터 불러오기
        public DataTable get_po_cnfm(string tr_snumb)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;


            strQury = "USP_TABLET_GET_PO_CNFM";

            SqlConnection conn = new SqlConnection(strConn);
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.Parameters.AddWithValue("@TR_SNUMB", tr_snumb);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                da.Fill(dt);
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;
        }
        //거래명세서가 입고 완료된 명세서인지 체크
        public int 명세서_완료_체크(string tr_snumb)
        {
            int check = 0;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT COUNT(*)AS CNT FROM PO_CNFM A  WHERE A.TR_SNUMB='" + tr_snumb + "' AND A.SLIO_YN='Y'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();

                check = int.Parse(sr["CNT"].ToString().Trim());
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }
        //현재고 불러오기
        public string get_now_sqty(string it_scode)
        {
            string strCon;
            string now_sqty = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";
            SqlDataReader reader = null;
            sql = "SELECT ";
            sql = sql + " (select ISNULL(SUM(MM_SQTY),0) from loc_document where mm_skind='1' and it_scode='" + it_scode.Trim() + "')-";
            sql = sql + " (select ISNULL(SUM(MM_SQTY),0) from loc_document where mm_skind='2' and it_scode='" + it_scode.Trim() + "')as SQTY";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    now_sqty = reader["SQTY"].ToString().Trim();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                reader.Close();
                reader.Dispose();
                conn.Close();
            }
            return now_sqty;
        }

        //서버 클라이언트 시간 동기화 
        public string maxTimeSync()
        {
            string time_sync = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "select getdate()";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    time_sync = reader[0].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n시간 동기화 작업 실패\n인터넷 연결이 원할하지 않습니다.");
            }
            finally
            {
                conn.Close();
            }
            return time_sync;
        }
        //yyyy-MM-dd HH:mm:ss
        public string get_time_now()
        {
            string time_sync = "";
            string strCon;

            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "select convert(varchar(20),getdate(),120)";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    time_sync = reader[0].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n시간 동기화 작업 실패\n인터넷 연결이 원할하지 않습니다.");
            }
            finally
            {
                conn.Close();
            }
            return time_sync;
        }
        //출하지시 GET 'N'
        public DataTable get_shipment_select()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_SHIPMENT_SELECT";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                ds = new DataSet();

                da.Fill(ds, "SHIPMENT_SELECT");
                dt = ds.Tables["SHIPMENT_SELECT"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;
        }

        public DataTable getDataGridView_ship(string sh_snumb)
        {
            DataTable dt = new DataTable();
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_GET_SHIPMENT_DET_CS";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //품목코드로 가지고 오기 
            da.SelectCommand.Parameters.AddWithValue("@SH_SNUMB", sh_snumb);
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds, "SHIPMENT_DET");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            dt = ds.Tables["SHIPMENT_DET"];
            return dt;
        }


        public DataTable Get_Shipment_Select(DataTable dt_it)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ROWNUM", typeof(string));
            dt.Columns.Add("RSRV_NO", typeof(string));
            dt.Columns.Add("VW_SNUMB", typeof(string));
            dt.Columns.Add("PP_DATE", typeof(string));
            dt.Columns.Add("PRDT_ITEM", typeof(string));
            dt.Columns.Add("CARRIER_NO", typeof(string));
            dt.Columns.Add("CARD_NO", typeof(string));            
            dt.Columns.Add("MM_SQTY", typeof(string));
            dt.Columns.Add("SQTY", typeof(string));
            dt.Columns.Add("CHECK_YN", typeof(string));

            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_GET_SHIPMENT_SELECT_CS";

            SqlConnection conn = new SqlConnection(strConn);



            SqlCommand cmd = new SqlCommand();

            cmd = new SqlCommand(strQury, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //품목코드로 가지고 오기 
            foreach (DataRow dr in dt_it.Rows)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.AddWithValue("@IT_SCODE", dr["IT_SCODE"].ToString());

                try
                {
                    SqlDataReader reader = null;
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DataRow dr_D = dt.NewRow();
                        dr_D["ROWNUM"] = reader["ROWNUM"].ToString().Trim();
                        dr_D["RSRV_NO"] = reader["RSRV_NO"].ToString().Trim();
                        dr_D["VW_SNUMB"] = reader["VW_SNUMB"].ToString().Trim();
                        dr_D["PP_DATE"] = reader["PP_DATE"].ToString().Trim();
                        dr_D["CARRIER_NO"] = reader["CARRIER_NO"].ToString().Trim();
                        dr_D["CARD_NO"] = reader["CARD_NO"].ToString().Trim();
                        dr_D["PRDT_ITEM"] = reader["PRDT_ITEM"].ToString().Trim();
                        dr_D["MM_SQTY"] = reader["MM_SQTY"].ToString().Trim();
                        dr_D["SQTY"] = reader["SQTY"].ToString().Trim();
                        dr_D["CHECK_YN"] = reader["CHECK_YN"].ToString().Trim();

                        dt.Rows.Add(dr_D);
                    }
                    reader.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            return dt;
        }
        public DataRow Get_Shipment_Select_NEW(string it_scode,string card_no,DataTable DT)
        {
            DataRow dr_D=null;
            

            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_TABLET_GET_SHIPMENT_CARD_CS";

            SqlConnection conn = new SqlConnection(strConn);



            SqlCommand cmd = new SqlCommand();

            cmd = new SqlCommand(strQury, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            cmd.Parameters.AddWithValue("@CARD_NO", card_no);
            
            
              
            try
            {
                SqlDataReader reader = null;
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    dr_D = DT.NewRow();
                    
                    dr_D["RSRV_NO"] = reader["RSRV_NO"].ToString().Trim();
                    dr_D["VW_SNUMB"] = reader["VW_SNUMB"].ToString().Trim();
                    dr_D["PP_DATE"] = reader["PP_DATE"].ToString().Trim();
                    dr_D["CARRIER_NO"] = reader["CARRIER_NO"].ToString().Trim();
                    dr_D["CARD_NO"] = reader["CARD_NO"].ToString().Trim();
                    dr_D["PRDT_ITEM"] = reader["PRDT_ITEM"].ToString().Trim();
                    dr_D["MM_SQTY"] = reader["MM_SQTY"].ToString().Trim();
                    dr_D["SQTY"] = reader["SQTY"].ToString().Trim();
                    dr_D["CHECK_YN"] = "Y";

                    
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            return dr_D;
        }
        //작업자 불러오기
        public DataTable Worker_info_set(string wc_code)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            //strQury = "SELECT WCR_CODE,WCR_NAME,''AS CHK FROM WORKER_INFO WHERE WC_CODE=@WC_CODE AND SW_CODE=@SW_CODE";
            strQury = "USP_GET_WORK_RESOURCE";
            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            //da.SelectCommand.Parameters.AddWithValue("@WC_CODE", sw_code);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "WORK_RESOURCE");
                dt = ds.Tables["WORK_RESOURCE"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }


        public string getIt_model(string it_scode)
        {
            string strCon;
            string it_model = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT IT_MODEL FROM IT_MASTER WHERE IT_SCODE='" + it_scode + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        it_model = reader[0].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return it_model;
        }
        //getPrj_code
        public string getPrj_code(string it_scode)
        {
            string strCon;
            string prj_code = "";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT PRJ_CODE FROM IT_MASTER WHERE IT_SCODE='" + it_scode + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        prj_code = reader[0].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return prj_code;
        }
        public DataTable GetFail_Dt(string wc_code, int month_idx, int year)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_WC_CODE_FAIL_DAY";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@MONTH_IDX", month_idx);
            da.SelectCommand.Parameters.AddWithValue("@YEAR_IDX", year);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "FAIL_DAY");
                dt = ds.Tables["FAIL_DAY"];
            }
            catch
            {
                conn.Close();
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public DataTable GetFail_Dt_DETAIL(string wc_code, int month_idx, string wc_group, int year)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_WC_CODE_FAIL_DAY_DETAIL";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@MONTH_IDX", month_idx);
            da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
            da.SelectCommand.Parameters.AddWithValue("@YEAR_IDX", year);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "FAIL_DAY_DETAIL");
                dt = ds.Tables["FAIL_DAY_DETAIL"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }

        public string[] get_day_night_sqty(string it_scode,string wc_code)
        {
            string strCon;
            string[] sqty = new string[2];
            sqty[0] = "0";
            sqty[1] = "0";
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "GET_DAY_NIGHT_SQTY";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WC_CODE", wc_code);
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);
            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["GUBN"].ToString().Trim() == "1")
                    {
                        sqty[0] = reader["QTY"].ToString().Trim();
                    }
                    else
                    {
                        sqty[1] = reader["QTY"].ToString().Trim();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return sqty;
        }

        public DataTable GetDataDay_Sqty(string date, string wc_code, string gubn)
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SP_DAY_NIGHT_SQTY_POPUP";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);            
            da.SelectCommand.Parameters.AddWithValue("@GUBN", gubn);
            da.SelectCommand.Parameters.AddWithValue("@DATE", date);
            try
            {
                ds = new DataSet();

                da.Fill(ds, "FAIL_DAY_DETAIL");
                dt = ds.Tables["FAIL_DAY_DETAIL"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
        #region 사이트 코드로 컨넥션 정보 가져와서 로케이션전표와 통합창고 전표 채번
        public static string loc_OUT_NewNumber(string site_code)
        {

            string strCon;
            strCon = site_code_to_site_conn(site_code);

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_GET_MOVE_INOUT_REG_MAXNUM_NEW", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@MAX_NUM", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string loc_maxnum = paramMaxNum.Value.ToString();
                return loc_maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }

        }
        public static string loc_IN_NewNumber(string site_code)
        {
            string strCon;
            strCon = site_code_to_site_conn(site_code);

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_GET_MOVE_INOUT_REG_MAXNUM_NEW", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@MAX_NUM", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string loc_maxnum = paramMaxNum.Value.ToString();
                return loc_maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }

        }


        public static string mm_OUT_NewNumber(string site_code)
        {
            string strCon;
            strCon = site_code_to_site_conn(site_code);

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_SAVE_MAXNUM", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@MAX_NUM", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string loc_maxnum = paramMaxNum.Value.ToString();
                return loc_maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }

        }

        public static string mm_IN_NewNumber(string site_code)
        {

            string strCon;
            strCon = site_code_to_site_conn(site_code);

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();

            SqlCommand cmd = new SqlCommand("SP_SAVE_MAXNUM", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            SqlParameter paramMaxNum = new SqlParameter("@MAX_NUM", SqlDbType.Char, 8);
            //sp 에서 output 변수 설정
            paramMaxNum.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramMaxNum);

            try
            {
                cmd.ExecuteNonQuery();
                string loc_maxnum = paramMaxNum.Value.ToString();
                return loc_maxnum;
            }
            catch (Exception ex)
            {

                MessageBox.Show("ERROR : " + ex.Message);
                return "error";
            }

            finally
            {
                conn.Close();
            }

        }
        public static string site_code_to_site_conn(string site_code)
        {
            string str_conn = "";
            if (site_code.Trim().Equals("D001"))
            {
                str_conn = Properties.Settings.Default.SQL_DKQT;
            }
            else if (site_code.Trim().Equals("A001"))
            {
                str_conn = Properties.Settings.Default.SQL_TAQT;
            }
            else if (site_code.Trim().Equals("U001"))
            {
                str_conn = Properties.Settings.Default.SQL_TUST;
            }
            else if (site_code.Trim().Equals("K001"))
            {
                str_conn = Properties.Settings.Default.SQL_TKQT;
            }
            return str_conn;
        }
        #endregion
    }
    public class GET_AUTO
    {
        public string IT_SCODE_str = "";//dataGridView1.Rows[cxtl].Cells["IT_SCODE"].Value.ToString().Trim();            
        public string IT_SNAME_str = "";// dataGridView1.Rows[cxtl].Cells["IT_SNAME"].Value.ToString().Trim();            
        public string IT_MODEL_str = "";//dataGridView1.Rows[cxtl].Cells["IT_MODEL"].Value.ToString().Trim();            
        public string MO_SQUTY_str = "";//dataGridView1.Rows[cxtl].Cells["WP_SQUTY"].Value.ToString().Trim();            
        public string MO_SNUMB_str = "";// dataGridView1.Rows[cxtl].Cells["WP_SNUMB"].Value.ToString().Trim();            
        //WC_CODE_str = dataGridView1.Rows[cxtl].Cells["WC_SCODE"].Value.ToString().Trim();
        public string SITE_CODE_str = "";//"D001";//dataGridView1.Rows[cxtl].Cells["SITE_CODE"].Value.ToString().Trim();            
        public string ALC_CODE_str = "";//dataGridView1.Rows[cxtl].Cells["ALC_CODE"].Value.ToString().Trim();            
        public string ME_SCODE_str = "";//dataGridView1.Rows[cxtl].Cells["ME_SCODE"].Value.ToString().Trim();            
        public string IT_PKQTY_str = "";//dataGridView1.Rows[cxtl].Cells["IT_PKQTY"].Value.ToString().Trim();            
        public string D_WEIGHT_str = "";//dataGridView1.Rows[cxtl].Cells["D_WEIGHT"].Value.ToString().Trim();            
        public string A_WEIGHT_str = "";// dataGridView1.Rows[cxtl].Cells["A_WEIGHT"].Value.ToString().Trim();            
        public string MATCH_GUBN_str = "";//dataGridView1.Rows[cxtl].Cells["MATCH_GUBN"].Value.ToString().Trim();            
        public string CARRIER_YN_str = "";//dataGridView1.Rows[cxtl].Cells["CARRIER_YN"].Value.ToString().Trim();            
        public string MAX_SQTY_str = "";//dataGridView1.Rows[cxtl].Cells["MAX_SQTY"].Value.ToString().Trim();   
        public string wc_group { get; set; }
        public string wc_code { get; set; }
        public string it_scode { get; set; }
        public void get생산계획()
        {

            SqlConnection conn = new SqlConnection(Properties.Settings.Default.SQL_DKQT);
            SqlDataAdapter da = new SqlDataAdapter("SP_TABLET_WORK_PLAN_SEARCH_AUTO", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            da.SelectCommand.Parameters.AddWithValue("@WC_GROUP", wc_group);
            da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@IT_SCODE", it_scode);
            try
            {
                DataSet ds = new DataSet();
                da.Fill(ds, "wc_group_search");

                DataTable dt = ds.Tables["wc_group_search"];


                IT_SCODE_str = dt.Rows[0]["IT_SCODE"].ToString();
                IT_SNAME_str = dt.Rows[0]["IT_SNAME"].ToString();
                IT_MODEL_str = dt.Rows[0]["IT_MODEL"].ToString();
                MO_SQUTY_str = dt.Rows[0]["WP_SQUTY"].ToString();
                MO_SNUMB_str = dt.Rows[0]["WP_SNUMB"].ToString();

                SITE_CODE_str = "D001";
                ALC_CODE_str = dt.Rows[0]["ALC_CODE"].ToString();
                ME_SCODE_str = dt.Rows[0]["ME_SCODE"].ToString();
                IT_PKQTY_str = dt.Rows[0]["IT_PKQTY"].ToString();
                D_WEIGHT_str = dt.Rows[0]["D_WEIGHT"].ToString();
                A_WEIGHT_str = dt.Rows[0]["A_WEIGHT"].ToString();
                MATCH_GUBN_str = dt.Rows[0]["MATCH_GUBN"].ToString();
                CARRIER_YN_str = dt.Rows[0]["CARRIER_YN"].ToString();
                MAX_SQTY_str = dt.Rows[0]["MAX_SQTY"].ToString();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                //ColorChange();

            }
        }

        public bool 생산계획_품목체크()
        {
            bool check = false;
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            conn.Open();
            string sql = "SELECT count(*)NUM FROM WORK_PLAN WHERE WORK_AREA='"+wc_group+"' AND END_CHECK='N' AND WC_SCODE='"+wc_code+"' AND IT_SCODE='"+it_scode+"'";

            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader sr;
            try
            {
                sr = cmd.ExecuteReader();
                sr.Read();
                if (sr["NUM"].ToString().Trim() != "0")
                {
                    check = true;
                }
                else
                {
                    check = false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return check;
        }

        public DataTable GetLoding_data()
        {
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT * FROM ";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = null;
            DataSet ds = null;
            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            /*da.SelectCommand.Parameters.AddWithValue("@WC_CODE", wc_code);
            da.SelectCommand.Parameters.AddWithValue("@GUBN", gubn);
            da.SelectCommand.Parameters.AddWithValue("@DATE", date);*/
            try
            {
                ds = new DataSet();

                da.Fill(ds, "FAIL_DAY_DETAIL");
                dt = ds.Tables["FAIL_DAY_DETAIL"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            return dt;

        }
    }
}