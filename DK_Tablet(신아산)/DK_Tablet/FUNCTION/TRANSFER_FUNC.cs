﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.FUNCTION
{
    class TRANSFER_FUNC
    {
        public DataTable get_Transfer(string carrier_no)
        {
            DataTable dt = new DataTable();
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT SITE_CODE,RSRV_NO,PRDT_ITEM,VW_SNUMB,WC_CODE,SYSIN_DATE,MANIN_DATE,R_START,R_END,GOOD_SQTY"
                +",ISNULL(FAIL_SQTY,0)AS FAIL_SQTY,REMAIN_SQTY,MM_RDATE"
                +",LOT_NO,CARRIER_NO,CARD_NO,CARRIER_YN,END_CHECK FROM WORK_INPUT_TRANSFER WHERE CARRIER_NO='" + carrier_no + "' AND END_CHECK='N'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            //품목코드로 가지고 오기 
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds, "transfer_search");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            dt = ds.Tables["transfer_search"];
            return dt;
        }
        //품목정보의 포장수량 가지고 오기
        public string get_it_pkqty(string it_scode)
        {
            string it_pkqty="0";
            string strCon;            
            strCon = Properties.Settings.Default.SQL_DKQT;
            string sql = "";

            sql = "SELECT IT_PKQTY FROM IT_MASTER WHERE IT_SCODE='" + it_scode + "'";

            SqlConnection conn = new SqlConnection(strCon);
            SqlCommand cmd =
                    new SqlCommand(sql, conn);

            conn.Open();

            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString().Trim() != "")
                    {
                        it_pkqty = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return it_pkqty;
        }

        public DataTable get_Transfer_it_scode(string it_scode)
        {
            DataTable dt = new DataTable();
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT SITE_CODE,RSRV_NO,PRDT_ITEM,VW_SNUMB,WC_CODE,SYSIN_DATE,MANIN_DATE,R_START,R_END,GOOD_SQTY"
                + ",ISNULL(FAIL_SQTY,0)AS FAIL_SQTY,REMAIN_SQTY,MM_RDATE"
                + ",LOT_NO,CARRIER_NO,CARD_NO,CARRIER_YN,END_CHECK FROM WORK_INPUT_TRANSFER WHERE PRDT_ITEM='" + it_scode + "' AND END_CHECK='N'";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            //품목코드로 가지고 오기 
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds, "transfer_search");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            dt = ds.Tables["transfer_search"];
            return dt;
        }

        public DataTable Loading_pp_card_get(string it_scode)
        {
            DataTable dt = new DataTable();
            string strConn;
            string strQury;
            strConn = Properties.Settings.Default.SQL_DKQT;

            strQury = "SELECT * FROM PP_CARD_REG A WHERE A.CHECK_YN='N' AND CARRIER_NO =''";

            SqlConnection conn = new SqlConnection(strConn);

            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter();

            da.SelectCommand = new SqlCommand(strQury, conn);
            da.SelectCommand.CommandType = CommandType.Text;
            //품목코드로 가지고 오기 
            DataSet ds = new DataSet();
            try
            {

                da.Fill(ds, "Loading_pp_search");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }


            dt = ds.Tables["Loading_pp_search"];
            return dt;
        }
    }
}
