﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet.UC_Control
{
    public partial class RoundButton : UserControl
    {
        public AdvButton.RoundButton roundButton1;
        public Color cr { get; set; }
        //private System.ComponentModel.IContainer components;
        public RoundButton()
        {
            InitializeComponent();
            
        }

        private void RoundButton_Load(object sender, EventArgs e)
        {
            this.components = new System.ComponentModel.Container();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RoundButton));
            this.roundButton1 = new AdvButton.RoundButton();
            this.SuspendLayout();
            // 
            // roundButton1
            // 
            this.roundButton1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.roundButton1.ColorGradient = ((System.Byte)(2));
            this.roundButton1.ColorStepGradient = ((System.Byte)(2));
            this.roundButton1.FadeOut = false;
            this.roundButton1.HoverColor = System.Drawing.SystemColors.ControlDark;
            this.roundButton1.ImageIndex = 0;
            this.roundButton1.Location = new System.Drawing.Point(0, 0);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(25, 25);
            this.roundButton1.TabIndex = 0;
            this.roundButton1.Dock = DockStyle.Fill;
            this.roundButton1.Margin = new System.Windows.Forms.Padding(1);
            this.roundButton1.Cr = cr;
            //this.roundButton1.ForeColor = Color.DimGray;
            

            //this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            //this.ClientSize = new System.Drawing.Size(168, 104);
            //this.Controls.Add(this.roundButton2);
            this.Controls.Add(this.roundButton1);
            this.ResumeLayout(false);
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        if (components != null)
        //        {
        //            components.Dispose();
        //        }
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
