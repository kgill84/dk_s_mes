﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.Net;
using System.Threading;


namespace DK_Tablet
{
    public partial class autoDownload : Form
    {
        public autoDownload()
        {
            InitializeComponent();
        }

        string programName = "";
        string downURL = "";
        string fileName = "";



        private void Form1_Load(object sender, EventArgs e)
        {
            
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(
                                   @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            //레지스트리 등록 할때
            if (registryKey.GetValue("DK 테블릿") == null)
            {
                //registryKey.SetValue("DK 테블릿", Application.ExecutablePath.ToString());
            }
            else 
            { 
                registryKey.DeleteValue("DK 테블릿");
            }
            /*
            if (Environment.Is64BitOperatingSystem == true)
            {
                programName = "SAP Crystal Reports runtime engine for .NET Framework 4 (64-bit)";
                downURL = "http://downloads.businessobjects.com/akdlm/crnetruntime/clickonce/CRRuntime_64bit_13_0.msi";
                fileName = "CRRuntime_64bit_13_0.msi";
            }
            else
            {*/
                programName = "SAP Crystal Reports runtime engine for .NET Framework 4 (32-bit)";
                downURL = "http://downloads.businessobjects.com/akdlm/crnetruntime/clickonce/CRRuntime_32bit_13_0.msi";
                fileName = "CRRuntime_32bit_13_0.msi";
            //}
            LBlocation.Text = "다운로드 위치 : C:\\Users\\Public\\" + fileName;
        }

        //설치되었는지 체크
        public bool CheckInstall()
        {
            bool Chk = false;

            string displayName;

            string registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            RegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;

                    if (displayName != null && displayName.Contains(programName))
                    {
                        Chk = true;
                    }
                }                
                key.Close();
            }
            return Chk;
        }

        // 프로그래스바 
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                double bytesIn = double.Parse(e.BytesReceived.ToString());
                double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                double percentage = bytesIn / totalBytes * 100;
                LBpercent.Text = int.Parse(Math.Truncate(percentage).ToString()) + "%";
                LBfileSize.Text = totalBytes.ToString();
                progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
            });
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                MessageBox.Show("Download completed!");

                ProcessStartInfo startinfo = new ProcessStartInfo();
                startinfo.FileName = fileName;
                startinfo.WorkingDirectory = "C:\\Users\\Public\\";
                Process.Start(startinfo);
                this.Close();
            });
        }

        public void install()
        {
            //다운로드 받기
            Thread bgThead = new Thread(() =>
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                client.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                client.DownloadFileAsync(new Uri(downURL), @"C:\Users\Public\" + fileName);
            });
            bgThead.Start();

            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!CheckInstall())
            {
                if (DialogResult.OK == MessageBox.Show("프로그램 설치", "확인", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                {
                    install();
                }
                else
                {
                    Close();
                }
            }
            else
            {
                this.Visible = false;
                MAIN main = new MAIN();
                main.Show();

            }
        }

        private void LBlocation_Click(object sender, EventArgs e)
        {

        }
               
    }
}
