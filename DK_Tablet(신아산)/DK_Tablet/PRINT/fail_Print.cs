﻿using DataMatrix.net;
using DevExpress.Office.Utils;
using DevExpress.XtraReports.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;

namespace DK_Tablet.PRINT
{
    class fail_Print
    {

        public static void Print(string pro_regno, String luePro)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("USP_FAIL_PRO_PRINT_PC_SCODE", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);

            cmd.Parameters.AddWithValue("@PRO_REGNO", pro_regno);



            SqlDataReader reader;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                int i = 0;
                // string[] pc_scode = new string[0];
                ArrayList pc_Scode = new ArrayList();
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    pc_Scode.Add(reader["PC_SCODE"].ToString());
                    i++;
                }
                //trans.Commit();
                reader.Close();
                //양품수량 증가

                Main_Fail_Report mainDoc = new Main_Fail_Report();
                mainDoc.CreateDocument();
                mainDoc.Pages.Clear();

                for (i = 0; i < pc_Scode.Count; i++)
                {
                    int maxRowCount = 10;

                    SqlCommand countCmd = new SqlCommand("USP_FAIL_PRO_PRINT_COUNT", conn);
                    countCmd.CommandType = CommandType.StoredProcedure;
                    countCmd.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);
                    countCmd.Parameters.AddWithValue("@PRO_REGNO", pro_regno);
                    countCmd.Parameters.AddWithValue("@PC_SCODE", pc_Scode[i]);

                    int totalCount = Convert.ToInt32(countCmd.ExecuteScalar());

                    int pageCount = totalCount / maxRowCount + (totalCount % maxRowCount == 0 ? 0 : 1);
                    int start = 0, end = 0;

                    for (int idx = 0; idx < pageCount; idx++)
                    {
                        start = ((idx + 1) * maxRowCount) - (maxRowCount - 1);
                        end = (idx + 1) * maxRowCount;
                        SqlDataAdapter da = new SqlDataAdapter("USP_FAIL_PRO_PRINT", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.AddWithValue("@SITE_CODE", Properties.Settings.Default.SITE_CODE);
                        da.SelectCommand.Parameters.AddWithValue("@PRO_REGNO", pro_regno);
                        da.SelectCommand.Parameters.AddWithValue("@PC_SCODE", pc_Scode[i]);
                        da.SelectCommand.Parameters.AddWithValue("@START", start);
                        da.SelectCommand.Parameters.AddWithValue("@END", end);

                        DataSet ds = new DataSet();
                        ds = new DataSet();
                        da.Fill(ds, "FAIL_PROCESSING");

                        Main_Fail_Report doc = new Main_Fail_Report(luePro);
                        Fail_Re_Print report = new Fail_Re_Print();

                        report.DataSource = ds;

                        doc.report1.ReportSource = report;
                        doc.report2.ReportSource = report;

                        doc.CreateDocument();
                        mainDoc.Pages.AddRange(doc.Pages);

                    }

                }
                mainDoc.PrintingSystem.ContinuousPageNumbering = true;
                mainDoc.ShowPrintMarginsWarning = false;
                mainDoc.ShowPrintStatusDialog = false;

                mainDoc.ExportToPdf("c:\\Test.pdf");

                ReportPrintTool ReportPrintTool = new ReportPrintTool(mainDoc);
                ReportPrintTool.PrintingSystem.ShowMarginsWarning = false;
                //ReportPrintTool.ShowPreviewDialog();
                ReportPrintTool.Print();

                mainDoc.Dispose();

            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }


    }
}
