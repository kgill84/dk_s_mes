﻿using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;

namespace DK_Tablet.PRINT
{
    public partial class qr_doc_viewer : DevExpress.XtraEditors.XtraForm
    {
        public string strCon { get; set; }
        public string me_scode { get; set; }
        public string wc_code { get; set; }
        public string sdate { get; set; }
        public string edate { get; set; }
        public string site_code { get; set; }

        public qr_doc_viewer()
        {
            InitializeComponent();
        }

        private void documentViewer1_Load(object sender, EventArgs e)
        {
            SplashScreenManager.ShowDefaultWaitForm();
            try
            {
                /*
                string strCon;
                strCon = Properties.Settings.Default.sqlconString;

                SqlConnection conn = new SqlConnection(strCon);

                SqlDataAdapter da = new SqlDataAdapter("SELECT IT_SCODE,IT_SNAME, ALC_CODE, '20150901', '0001' AS SER_NO,RTRIM(IT_SCODE)+'201509010001'AS LOT_NO"
                + " FROM IT_MASTER WHERE IT_SCODE LIKE '84510B8000NBC%'", conn);
                da.SelectCommand.CommandType = CommandType.Text;



                DataSet ds = new DataSet();
                da.Fill(ds, "ALC_CODE_data");*/
                Mobis_Print Mobis_Print = new Mobis_Print();
                //Alc_code_Print.DataSource = ds;
                //Alc_code_Print.PrinterName = "PRINT_A";
                //Alc_code_Print.ShowPrintMarginsWarning = false;
                //Alc_code_Print.ShowPrintStatusDialog = false;
                documentViewer1.DocumentSource = Mobis_Print;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                SplashScreenManager.CloseDefaultWaitForm();
            }
        }

    }
}
