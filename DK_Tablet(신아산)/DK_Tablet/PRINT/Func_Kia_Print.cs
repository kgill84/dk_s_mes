﻿using DataMatrix.net;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ThoughtWorks.QRCode.Codec;

namespace DK_Tablet.PRINT
{
    class Func_Kia_Print
    {
        
        public static void Print(string it_scode, int sqty, string pd_lotno,int print_sqty)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SP_KIA_IT_CHART_INSERT_TABLET", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            cmd.Parameters.AddWithValue("@SQUTY", sqty);

            cmd.Parameters.AddWithValue("@PD_LOT_NO", pd_lotno);

            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_KIA_QR_PRINT_NEW", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("CAR_CODE", typeof(string));
                    DT.Columns.Add("ALC_CODE", typeof(string));
                    DT.Columns.Add("PLNT_CODE", typeof(string));
                    DT.Columns.Add("SHIPP_CODE", typeof(string));
                    DT.Columns.Add("AREA_CODE", typeof(string));
                    DT.Columns.Add("PROC_CODE", typeof(string));
                    DT.Columns.Add("IC_PDATE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("SQTY_NO", typeof(string));
                    DT.Columns.Add("TEL_NO", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LH_RH", typeof(string));
                    DT.Columns.Add("PKQTY", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();
                        string temp = "               ";
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["CAR_CODE"] = reader1["CAR_CODE"].ToString();
                        dr["ALC_CODE"] = reader1["ALC_CODE"].ToString();
                        dr["PLNT_CODE"] = reader1["PLNT_CODE"].ToString();
                        dr["SHIPP_CODE"] = reader1["SHIPP_CODE"].ToString();
                        dr["AREA_CODE"] = reader1["AREA_CODE"].ToString();
                        dr["PROC_CODE"] = reader1["PROC_CODE"].ToString();
                        dr["IC_PDATE"] = reader1["IC_PDATE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["SQTY_NO"] = reader1["SQTY_NO"].ToString();
                        dr["TEL_NO"] = reader1["TEL_NO"].ToString().Replace("<br>",@", ");
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = "U678 " + reader1["IT_SCODE"].ToString().Trim().Replace("-", "") +
                            temp.Substring(reader1["IT_SCODE"].ToString().Replace("-", "").Trim().Length, temp.Length - reader1["IT_SCODE"].ToString().Replace("-", "").Trim().Length)
                            + "00000".Substring(reader1["IC_SQTY"].ToString().Length, "00000".Length - reader1["IC_SQTY"].ToString().Length) + reader1["IC_SQTY"].ToString();
                        dr["LH_RH"] = reader1["LH_RH"].ToString();
                        dr["PKQTY"] = reader1["PKQTY"].ToString();
                        //dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());

                        dr["QR_CODE_IMAGE"] = createQr_Code_basic(reader1["QR_CODE"].ToString());

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("KIA_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Kia_Print Kia_Print = new Kia_Print();
                    Kia_Print.DataSource = ds;
                    //Alc_code_Print.PrinterName = "PRINT_A";
                    Kia_Print.ShowPrintMarginsWarning = false;
                    Kia_Print.ShowPrintStatusDialog = false;
                    

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Mobis_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Kia_Print);
                    
                    for (int i = 0; i < print_sqty;i++ )
                    {
                        ReportPrintTool.Print();
                    }
                    
                    
                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }
        public static byte[] createQr_Code_basic(string data)
        {
            byte[] byte_arry = null;
            string gs1Code = data;
            DmtxImageEncoder encoder = new DmtxImageEncoder();
            DmtxImageEncoderOptions options = new DmtxImageEncoderOptions();
            options.ModuleSize = 8;
            options.MarginSize = 30;
            options.BackColor = Color.White;
            options.ForeColor = Color.Black;
            options.Scheme = DmtxScheme.DmtxSchemeAsciiGS1;
            Bitmap encodedBitmap = encoder.EncodeImage(gs1Code, options);


            //byte[] barcodeInBytes = encodedBitmap.
            //MemoryStream ms = new MemoryStream(barcodeInBytes);
            Image returnImage = encodedBitmap;
            ImageConverter _imageConverter = new ImageConverter();
            byte_arry = (byte[])_imageConverter.ConvertTo(returnImage, typeof(byte[]));
            return byte_arry;
        }
        public void Print_외주(string it_scode, int sqty, string pd_lotno, int print_sqty)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SP_MOBIS_IT_CHART_INSERT_TABLET_외주", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            cmd.Parameters.AddWithValue("@SQTY", sqty);

            cmd.Parameters.AddWithValue("@PD_LOT_NO", pd_lotno);


            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_MOBIS_QR_PRINT_NEW", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SDATE", typeof(string));
                    DT.Columns.Add("IC_DEUDT", typeof(string));
                    DT.Columns.Add("IC_PTIME", typeof(string));
                    DT.Columns.Add("T_CAR_NAME", typeof(string));
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("IC_SEQNO", typeof(string));
                    DT.Columns.Add("T_ALC_CODE", typeof(string));
                    DT.Columns.Add("T_SITE_CODE", typeof(string));
                    DT.Columns.Add("T_SHIP_CODE", typeof(string));
                    DT.Columns.Add("T_PC_SCODE", typeof(string));
                    DT.Columns.Add("T_AREA", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));
                    DT.Columns.Add("T_INSPECT_TYPE", typeof(string));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();

                        dr["IC_SDATE"] = reader1["IC_SDATE"].ToString();
                        dr["IC_DEUDT"] = reader1["IC_DEUDT"].ToString();
                        dr["IC_PTIME"] = reader1["IC_PTIME"].ToString();
                        dr["T_CAR_NAME"] = reader1["T_CAR_NAME"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["IC_SEQNO"] = reader1["IC_SEQNO"].ToString();
                        dr["T_ALC_CODE"] = reader1["T_ALC_CODE"].ToString();
                        dr["T_SITE_CODE"] = reader1["T_SITE_CODE"].ToString();
                        dr["T_SHIP_CODE"] = reader1["T_SHIP_CODE"].ToString();
                        dr["T_PC_SCODE"] = reader1["T_PC_SCODE"].ToString();
                        dr["T_AREA"] = reader1["T_AREA"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());
                        dr["T_INSPECT_TYPE"] = reader1["T_INSPECT_TYPE"].ToString();

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("MOBIS_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Mobis_Print Mobis_Print = new Mobis_Print();
                    Mobis_Print.DataSource = ds;
                    Mobis_Print.PrinterName = "PRINT_A";
                    Mobis_Print.ShowPrintMarginsWarning = false;
                    Mobis_Print.ShowPrintStatusDialog = false;

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Mobis_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Mobis_Print);
                    for (int i = 0; i < print_sqty; i++)
                    {
                        ReportPrintTool.Print();
                    }


                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }
        private static byte[] createQrCode_injection(string data)
        {
            byte[] qr_code = null;
            Image qr_code_img;
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;

            try
            {
                qrCodeEncoder.QRCodeScale = Convert.ToInt16(4);
                qrCodeEncoder.QRCodeVersion = 4;
                qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                qr_code_img = qrCodeEncoder.Encode(data);

                ImageConverter converter = new ImageConverter();
                qr_code = (byte[])converter.ConvertTo(qr_code_img, typeof(byte[]));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return qr_code;
        }

        public void Print_C(string it_scode, int sqty, string pd_lotno)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SP_MOBIS_IT_CHART_INSERT_TABLET_C", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            cmd.Parameters.AddWithValue("@SQTY", sqty);

            cmd.Parameters.AddWithValue("@PD_LOT_NO", pd_lotno);


            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_MOBIS_QR_PRINT_NEW", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SDATE", typeof(string));
                    DT.Columns.Add("IC_DEUDT", typeof(string));
                    DT.Columns.Add("IC_PTIME", typeof(string));
                    DT.Columns.Add("T_CAR_NAME", typeof(string));
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("IC_SEQNO", typeof(string));
                    DT.Columns.Add("T_ALC_CODE", typeof(string));
                    DT.Columns.Add("T_SITE_CODE", typeof(string));
                    DT.Columns.Add("T_SHIP_CODE", typeof(string));
                    DT.Columns.Add("T_PC_SCODE", typeof(string));
                    DT.Columns.Add("T_AREA", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));
                    DT.Columns.Add("T_INSPECT_TYPE", typeof(string));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();

                        dr["IC_SDATE"] = reader1["IC_SDATE"].ToString();
                        dr["IC_DEUDT"] = reader1["IC_DEUDT"].ToString();
                        dr["IC_PTIME"] = reader1["IC_PTIME"].ToString();
                        dr["T_CAR_NAME"] = reader1["T_CAR_NAME"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["IC_SEQNO"] = reader1["IC_SEQNO"].ToString();
                        dr["T_ALC_CODE"] = reader1["T_ALC_CODE"].ToString();
                        dr["T_SITE_CODE"] = reader1["T_SITE_CODE"].ToString();
                        dr["T_SHIP_CODE"] = reader1["T_SHIP_CODE"].ToString();
                        dr["T_PC_SCODE"] = reader1["T_PC_SCODE"].ToString();
                        dr["T_AREA"] = reader1["T_AREA"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());
                        dr["T_INSPECT_TYPE"] = reader1["T_INSPECT_TYPE"].ToString();

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("MOBIS_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Mobis_Print_C Mobis_Print_C = new Mobis_Print_C();
                    Mobis_Print_C.DataSource = ds;
                    Mobis_Print_C.PrinterName = "PRINT_A";
                    Mobis_Print_C.ShowPrintMarginsWarning = false;
                    Mobis_Print_C.ShowPrintStatusDialog = false;

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Mobis_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Mobis_Print_C);
                    ReportPrintTool.Print();
                    ReportPrintTool.Print();
                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }
        public void Print_C_외주(string it_scode, int sqty, string pd_lotno)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SP_MOBIS_IT_CHART_INSERT_TABLET_C_외주", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            cmd.Parameters.AddWithValue("@SQTY", sqty);

            cmd.Parameters.AddWithValue("@PD_LOT_NO", pd_lotno);


            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_MOBIS_QR_PRINT_NEW_외주", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SDATE", typeof(string));
                    DT.Columns.Add("IC_DEUDT", typeof(string));
                    DT.Columns.Add("IC_PTIME", typeof(string));
                    DT.Columns.Add("T_CAR_NAME", typeof(string));
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("IC_SEQNO", typeof(string));
                    DT.Columns.Add("T_ALC_CODE", typeof(string));
                    DT.Columns.Add("T_SITE_CODE", typeof(string));
                    DT.Columns.Add("T_SHIP_CODE", typeof(string));
                    DT.Columns.Add("T_PC_SCODE", typeof(string));
                    DT.Columns.Add("T_AREA", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));
                    DT.Columns.Add("T_INSPECT_TYPE", typeof(string));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();

                        dr["IC_SDATE"] = reader1["IC_SDATE"].ToString();
                        dr["IC_DEUDT"] = reader1["IC_DEUDT"].ToString();
                        dr["IC_PTIME"] = reader1["IC_PTIME"].ToString();
                        dr["T_CAR_NAME"] = reader1["T_CAR_NAME"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["IC_SEQNO"] = reader1["IC_SEQNO"].ToString();
                        dr["T_ALC_CODE"] = reader1["T_ALC_CODE"].ToString();
                        dr["T_SITE_CODE"] = reader1["T_SITE_CODE"].ToString();
                        dr["T_SHIP_CODE"] = reader1["T_SHIP_CODE"].ToString();
                        dr["T_PC_SCODE"] = reader1["T_PC_SCODE"].ToString();
                        dr["T_AREA"] = reader1["T_AREA"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());
                        dr["T_INSPECT_TYPE"] = reader1["T_INSPECT_TYPE"].ToString();

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("MOBIS_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Mobis_Print_C Mobis_Print_C = new Mobis_Print_C();
                    Mobis_Print_C.DataSource = ds;
                    Mobis_Print_C.PrinterName = "PRINT_A";
                    Mobis_Print_C.ShowPrintMarginsWarning = false;
                    Mobis_Print_C.ShowPrintStatusDialog = false;

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Mobis_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Mobis_Print_C);
                    ReportPrintTool.Print();
                    ReportPrintTool.Print();
                }


            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }
        public void Print_ACS(string it_scode, int sqty, string pm_snumb)
        {

            /*
            It_Chart_print_Form It_Chart_print_Form = new It_Chart_print_Form();
            It_Chart_print_Form.it_scode = "847101W000HU";
            It_Chart_print_Form.sqty = "5";
            It_Chart_print_Form.Show();
             */
            string strCon;
            strCon = DK_Tablet.Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);
            //SqlTransaction trans;

            //커맨드
            SqlCommand cmd =
                    new SqlCommand("SP_IT_CHART_INSERT_TABLET_NEW_ACS", conn);
            //커맨드 타입
            cmd.CommandType = CommandType.StoredProcedure;

            //품목코드
            cmd.Parameters.AddWithValue("@IT_SCODE", it_scode);

            //수량
            cmd.Parameters.AddWithValue("@SQTY", sqty);

            //lot 번호 
            cmd.Parameters.AddWithValue("@PM_SNUMB", pm_snumb);

            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_MOBIS_QR_PRINT_NEW", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IC_SDATE", typeof(string));
                    DT.Columns.Add("IC_DEUDT", typeof(string));
                    DT.Columns.Add("IC_PTIME", typeof(string));
                    DT.Columns.Add("T_CAR_NAME", typeof(string));
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("IC_SEQNO", typeof(string));
                    DT.Columns.Add("T_ALC_CODE", typeof(string));
                    DT.Columns.Add("T_SITE_CODE", typeof(string));
                    DT.Columns.Add("T_SHIP_CODE", typeof(string));
                    DT.Columns.Add("T_PC_SCODE", typeof(string));
                    DT.Columns.Add("T_AREA", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));
                    DT.Columns.Add("T_INSPECT_TYPE", typeof(string));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();

                        dr["IC_SDATE"] = reader1["IC_SDATE"].ToString();
                        dr["IC_DEUDT"] = reader1["IC_DEUDT"].ToString();
                        dr["IC_PTIME"] = reader1["IC_PTIME"].ToString();
                        dr["T_CAR_NAME"] = reader1["T_CAR_NAME"].ToString();
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = reader1["QR_CODE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["IC_SEQNO"] = reader1["IC_SEQNO"].ToString();
                        dr["T_ALC_CODE"] = reader1["T_ALC_CODE"].ToString();
                        dr["T_SITE_CODE"] = reader1["T_SITE_CODE"].ToString();
                        dr["T_SHIP_CODE"] = reader1["T_SHIP_CODE"].ToString();
                        dr["T_PC_SCODE"] = reader1["T_PC_SCODE"].ToString();
                        dr["T_AREA"] = reader1["T_AREA"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());
                        dr["T_INSPECT_TYPE"] = reader1["T_INSPECT_TYPE"].ToString();

                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("MOBIS_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Mobis_Print_C Mobis_Print_C = new Mobis_Print_C();
                    Mobis_Print_C.DataSource = ds;
                    Mobis_Print_C.PrinterName = "PRINT_A";
                    Mobis_Print_C.ShowPrintMarginsWarning = false;
                    Mobis_Print_C.ShowPrintStatusDialog = false;

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Mobis_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Mobis_Print_C);
                    ReportPrintTool.Print();
                    ReportPrintTool.Print();
                }

            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }

        public static void re_Print(string it_scode,int print_sqty)
        {
            string strCon;
            strCon = Properties.Settings.Default.SQL_DKQT;

            SqlConnection conn = new SqlConnection(strCon);

            SqlCommand cmd =
                    new SqlCommand("SELECT MAX(IC_SNUMB)AS IC_SNUMB FROM KIA_IT_CHART WHERE IT_SCODE='" + it_scode + "'", conn);

            cmd.CommandType = CommandType.Text;

            SqlDataReader reader;
            SqlDataReader reader1;
            //커넥션오픈 실행
            conn.Open();
            //trans = conn.BeginTransaction();
            //cmd.Transaction = trans;
            try
            {
                string ic_snumb = "", qr_code = "";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ic_snumb = reader["IC_SNUMB"].ToString();
                }
                //trans.Commit();
                reader.Close();

                //양품수량 증가

                if (ic_snumb != "")
                {

                    SqlCommand cmd_select =
                    new SqlCommand("SP_KIA_QR_PRINT_NEW", conn);
                    cmd_select.CommandType = CommandType.StoredProcedure;
                    cmd_select.Parameters.AddWithValue("@IC_SNUMB", ic_snumb);


                    reader1 = cmd_select.ExecuteReader();
                    DataTable DT = new DataTable();
                    DT.Columns.Add("IT_SCODE", typeof(string));
                    DT.Columns.Add("IT_SNAME", typeof(string));
                    DT.Columns.Add("CAR_CODE", typeof(string));
                    DT.Columns.Add("ALC_CODE", typeof(string));
                    DT.Columns.Add("PLNT_CODE", typeof(string));
                    DT.Columns.Add("SHIPP_CODE", typeof(string));
                    DT.Columns.Add("AREA_CODE", typeof(string));
                    DT.Columns.Add("PROC_CODE", typeof(string));
                    DT.Columns.Add("IC_PDATE", typeof(string));
                    DT.Columns.Add("LOT_NO", typeof(string));
                    DT.Columns.Add("SQTY_NO", typeof(string));
                    DT.Columns.Add("TEL_NO", typeof(string));
                    DT.Columns.Add("IC_SQTY", typeof(string));
                    DT.Columns.Add("QR_CODE", typeof(string));
                    DT.Columns.Add("LH_RH", typeof(string));
                    DT.Columns.Add("PKQTY", typeof(string));
                    DT.Columns.Add("QR_CODE_IMAGE", typeof(byte[]));



                    while (reader1.Read())
                    {
                        DataRow dr = DT.NewRow();
                        string temp = "               ";
                        dr["IT_SCODE"] = reader1["IT_SCODE"].ToString();
                        dr["IT_SNAME"] = reader1["IT_SNAME"].ToString();
                        dr["CAR_CODE"] = reader1["CAR_CODE"].ToString();
                        dr["ALC_CODE"] = reader1["ALC_CODE"].ToString();
                        dr["PLNT_CODE"] = reader1["PLNT_CODE"].ToString();
                        dr["SHIPP_CODE"] = reader1["SHIPP_CODE"].ToString();
                        dr["AREA_CODE"] = reader1["AREA_CODE"].ToString();
                        dr["PROC_CODE"] = reader1["PROC_CODE"].ToString();
                        dr["IC_PDATE"] = reader1["IC_PDATE"].ToString();
                        dr["LOT_NO"] = reader1["LOT_NO"].ToString();
                        dr["SQTY_NO"] = reader1["SQTY_NO"].ToString();
                        dr["TEL_NO"] = reader1["TEL_NO"].ToString().Replace("<br>", @", ");
                        dr["IC_SQTY"] = reader1["IC_SQTY"].ToString();
                        dr["QR_CODE"] = "U678 " + reader1["IT_SCODE"].ToString().Trim().Replace("-", "") +
                            temp.Substring(reader1["IT_SCODE"].ToString().Replace("-", "").Trim().Length, temp.Length - reader1["IT_SCODE"].ToString().Replace("-", "").Trim().Length)
                            + "00000".Substring(reader1["IC_SQTY"].ToString().Length, "00000".Length - reader1["IC_SQTY"].ToString().Length) + reader1["IC_SQTY"].ToString();
                        dr["LH_RH"] = reader1["LH_RH"].ToString();
                        dr["PKQTY"] = reader1["PKQTY"].ToString();
                        //dr["QR_CODE_IMAGE"] = createQrCode_injection(reader1["QR_CODE"].ToString());

                        dr["QR_CODE_IMAGE"] = createQr_Code_basic(reader1["QR_CODE"].ToString());


                        DT.Rows.Add(dr);
                    }
                    DataSet ds = new DataSet("KIA_IT_CHART");
                    ds.Tables.Add(DT);
                    reader1.Close();

                    Kia_Print Kia_Print = new Kia_Print();
                    Kia_Print.DataSource = ds;
                    //Alc_code_Print.PrinterName = "PRINT_A";
                    Kia_Print.ShowPrintMarginsWarning = false;
                    Kia_Print.ShowPrintStatusDialog = false;
                    Kia_Print.Landscape = true;

                    //qr_doc_viewer qr_doc_viewer = new qr_doc_viewer();
                    //qr_doc_viewer.documentViewer1.DocumentSource = Kia_Print;
                    //qr_doc_viewer.ShowDialog();
                    ReportPrintTool ReportPrintTool = new ReportPrintTool(Kia_Print);
                    
                    for (int i = 0; i < print_sqty; i++)
                    {
                        ReportPrintTool.Print();
                    }     
                    
                }
                else
                {
                    MessageBox.Show("데이터가 없습니다.");
                }

            }

            catch (Exception ex)
            {
                //trans.Rollback();
                MessageBox.Show("등록 실패 : " + ex.Message);

            }
            finally
            {
                conn.Close();

            }
        }
    }
}
