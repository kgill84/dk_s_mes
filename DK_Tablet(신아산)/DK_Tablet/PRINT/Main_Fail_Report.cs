﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace DK_Tablet.PRINT
{
    public partial class Main_Fail_Report : DevExpress.XtraReports.UI.XtraReport
    {
        public Main_Fail_Report()
        {
            InitializeComponent();
        }

        public Main_Fail_Report(String luePro)
        {
            InitializeComponent();
            xrTableCell10.Text = luePro;
            xrTableCell4.Text = luePro;
        }
    }
}
