﻿using DK_Tablet.Popup;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;


namespace DK_Tablet
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //string mtxName = "DK_MES";
            //Mutex mtx = new Mutex(true, mtxName);

            //// 1초 동안 뮤텍스를 획득하려 대기  
            //TimeSpan tsWait = new TimeSpan(0, 0, 1);
            //bool success = mtx.WaitOne(tsWait);

            //// 실패하면 프로그램 종료  
            //if (!success)
            //{
            //    MessageBox.Show("이미실행중입니다.");
            //    return;

            //}              
            //뮤텍스를 사용하여 프로그램 중복 실행 방지
            //string guid = "AAD27BE7-3AFC-4A3F-852C-A20A97805933";
            //bool bnew;
            //Mutex mutex = new Mutex(true, guid, out bnew);
            //if (bnew)
            //{
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new autoDownload());
                //mutex.ReleaseMutex();
            //}
            //else
            //{
            //    MessageBox.Show("프로그램이 실행중입니다.");
            //    Application.Exit();
            //}  
            
        }
    }
}
