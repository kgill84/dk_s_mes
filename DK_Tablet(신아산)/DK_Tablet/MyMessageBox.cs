using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace DK_Tablet
{
    public partial class MyMessageBox : DevExpress.XtraEditors.XtraForm
    {

        static MyMessageBox newMessageBox;
        public Timer msgTimer;
        
        static DialogResult DR;

        int disposeFormTimer; 

        public MyMessageBox()
        {
            DR = DialogResult.Retry;
            disposeFormTimer = 30;
            InitializeComponent();
        }

        public static DialogResult ShowBox(string txtMessage)
        {
            newMessageBox = new MyMessageBox();
            newMessageBox.lblMessage.Text = txtMessage;
            newMessageBox.ShowDialog();
            DR = DialogResult.OK;
            return DR;
        }

        public static DialogResult ShowBox(string txtMessage, string txtTitle)
        {
            newMessageBox = new MyMessageBox();
            newMessageBox.lblTitle.Text = txtTitle;
            newMessageBox.lblMessage.Text = txtMessage;
            newMessageBox.ShowDialog();
            DR = DialogResult.OK;
            return DR;
        }
        public static DialogResult ShowBox(string txtMessage, string txtTitle, int timers)
        {
            newMessageBox = new MyMessageBox();
            newMessageBox.lblTitle.Text = txtTitle;
            newMessageBox.lblMessage.Text = txtMessage;
            newMessageBox.disposeFormTimer = timers;
            newMessageBox.ShowDialog();
            
            return DR;
        }
        public static DialogResult ShowBox(string txtMessage, string txtTitle,string txtlbl, int timers)
        {
            newMessageBox = new MyMessageBox();
            newMessageBox.panelControl1.Appearance.BackColor = System.Drawing.Color.BlueViolet;
            newMessageBox.lblTitle.Text = txtTitle;
            newMessageBox.lblMessage.Text = txtMessage;
            newMessageBox.disposeFormTimer = timers;
            newMessageBox.labelControl1.Text = txtlbl;
            newMessageBox.ShowDialog();

            return DR;
        } 
        private void MyMessageBox_Load(object sender, EventArgs e)
        {
            
            newMessageBox.lblTimer.Text = disposeFormTimer.ToString();
            msgTimer = new Timer();
            msgTimer.Interval = 1000;
            msgTimer.Enabled = true;
            msgTimer.Start();
            msgTimer.Tick += new System.EventHandler(this.timer_tick); 
        }

        private void MyMessageBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics mGraphics = e.Graphics;
            Pen pen1 = new Pen(Color.FromArgb(96, 155, 173), 1);
            
            Rectangle Area1 = new Rectangle(0, 0, this.Width - 1, this.Height - 1);
            LinearGradientBrush LGB = new LinearGradientBrush(Area1, Color.FromArgb(96, 155, 173), Color.FromArgb(245, 251, 251), LinearGradientMode.Vertical);
            mGraphics.FillRectangle(LGB, Area1);
            mGraphics.DrawRectangle(pen1, Area1);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            newMessageBox.msgTimer.Stop();
            newMessageBox.msgTimer.Dispose();
            DR = DialogResult.OK;
            newMessageBox.Dispose();
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            newMessageBox.msgTimer.Stop();
            newMessageBox.msgTimer.Dispose();
            DR = DialogResult.Cancel;
            newMessageBox.Dispose();
        }

        private void timer_tick(object sender, EventArgs e)
        {
            disposeFormTimer--;

            if (disposeFormTimer >= 0)
            {
                newMessageBox.lblTimer.Text = disposeFormTimer.ToString();
            }
            else
            {
                DR = DialogResult.Retry;
                newMessageBox.msgTimer.Stop();
                newMessageBox.msgTimer.Dispose();
                newMessageBox.Dispose();
            }
        }
    }
}