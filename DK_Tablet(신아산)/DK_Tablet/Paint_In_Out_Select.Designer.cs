﻿namespace DK_Tablet
{
    partial class Paint_In_Out_Select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_In = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Out = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_In);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btn_Out);
            this.splitContainer1.Size = new System.Drawing.Size(1046, 430);
            this.splitContainer1.SplitterDistance = 518;
            this.splitContainer1.TabIndex = 0;
            // 
            // btn_In
            // 
            this.btn_In.Appearance.BackColor = System.Drawing.Color.White;
            this.btn_In.Appearance.BackColor2 = System.Drawing.Color.LightSalmon;
            this.btn_In.Appearance.Font = new System.Drawing.Font("Tahoma", 65F);
            this.btn_In.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_In.Appearance.Options.UseBackColor = true;
            this.btn_In.Appearance.Options.UseFont = true;
            this.btn_In.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_In.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_In.Location = new System.Drawing.Point(0, 0);
            this.btn_In.Name = "btn_In";
            this.btn_In.Size = new System.Drawing.Size(518, 430);
            this.btn_In.TabIndex = 0;
            this.btn_In.Text = "입구";
            this.btn_In.Click += new System.EventHandler(this.btn_In_Click);
            // 
            // btn_Out
            // 
            this.btn_Out.Appearance.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_Out.Appearance.BackColor2 = System.Drawing.Color.White;
            this.btn_Out.Appearance.Font = new System.Drawing.Font("Tahoma", 65F);
            this.btn_Out.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.btn_Out.Appearance.Options.UseBackColor = true;
            this.btn_Out.Appearance.Options.UseFont = true;
            this.btn_Out.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.btn_Out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_Out.Location = new System.Drawing.Point(0, 0);
            this.btn_Out.Name = "btn_Out";
            this.btn_Out.Size = new System.Drawing.Size(524, 430);
            this.btn_Out.TabIndex = 1;
            this.btn_Out.Text = "출구";
            this.btn_Out.Click += new System.EventHandler(this.btn_Out_Click);
            // 
            // Paint_In_Out_Select
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 430);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Paint_In_Out_Select";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Paint_In_Out_Select";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        public DevExpress.XtraEditors.SimpleButton btn_In;
        public DevExpress.XtraEditors.SimpleButton btn_Out;
    }
}